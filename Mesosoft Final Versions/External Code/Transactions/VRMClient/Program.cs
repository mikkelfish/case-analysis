//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Transactions;
using System.Diagnostics;

using VolatileResourceManager;
using IDesign.System.Collections.Transactional;

namespace VRMClient
{
    static class Program
    {
        [Serializable]
        class testing
        {
            public int Val { get; set; }
        }

        static Program()
        {
         
        }

        static void Main()
        {
            //MyClass obj = new MyClass();
            //obj.MyMethod();

            Transactional<string> text = new Transactional<string>("Some initial value");
            //text.Value = "Demonstrating non-transactional access";

            //using (TransactionScope scope1 = new TransactionScope())
            //{
            //    text.Value = "Transactional access";

            //    scope1.Complete();
            //}
            //Debug.Assert(text.Value == "Transactional access");

            Transactional<int> number = new Transactional<int>(1);

            //using (TransactionScope scope2 = new TransactionScope())
            //{
            //    number.Value = 2;
            //    scope2.Complete();
            //}
            //int i = number.Value;
            //Debug.Assert(i == 2);

            //using (TransactionScope scope3 = new TransactionScope())
            //{
            //    text.Value = "Transactional access";
            //    scope3.Complete();
            //}
            //Debug.Assert(text.Value == "Transactional access");


            //text.Value = "Some initial value";

            //using (TransactionScope scope4 = new TransactionScope())
            //{
            //    text.Value = "This access will be rolled back because there is no call to scope3.Complete()";
            //}
            //Debug.Assert(text.Value == "Some initial value");

            //using (TransactionScope scope5 = new TransactionScope())
            //{
            //    text.Value = "Both the text and the number below will be set as part of the transaction";
            //    number.Value = 3;
            //    scope5.Complete();
            //}
            //Debug.Assert(number.Value == 3);
            //Debug.Assert(text.Value == "Both the text and the number below will be set as part of the transaction");


            //text.Value = "Some initial value";

            try
            {
                using (TransactionScope scope6 = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(50)))
                {
                    number.Value = 4;
                    text.Value = "First transaction acquires lock here";

                    using (TransactionScope scope7 = new TransactionScope())
                    {
                        text.Value = "Second transaction blocks, first transaction aborts, second commits";
                        scope7.Complete();
                    }
                   // scope6.Complete();
                }
            }
            catch (TransactionAbortedException e)
            {
                MessageBox.Show(e.Message, "scope6");
            }
            Debug.Assert(number.Value == 3);
            Debug.Assert(text.Value == "Second transaction blocks, first transaction aborts, second commits");

            //TransactionalArray<int> array1 = new TransactionalArray<int>(4);

            //array1[0] = 1;
            //array1[1] = 2;
            //array1[2] = 3;
            //array1[3] = 4;

            //using (TransactionScope scope8 = new TransactionScope())
            //{
            //    array1[0] = 11;
            //    array1[1] = 22;
            //    array1[2] = 33;
            //    array1[3] = 44;
            //    scope8.Complete();
            //}
            //Debug.Assert(array1[2] == 33);

            //TransactionalArray<int> array2 = new TransactionalArray<int>(4);
            //using (TransactionScope scope9 = new TransactionScope())
            //{
            //    array2[0] = 1;
            //    array2[1] = 2;
            //    array2[2] = 3;
            //    array2[3] = 4;
            //}
            //Debug.Assert(array2[2] == 0);

            //using (TransactionScope scope10 = new TransactionScope())
            //{
            //    text.Value = "Demonstrating sharing a lock with a nested scope";
            //    using (TransactionScope scope11 = new TransactionScope())
            //    {
            //        text.Value = "Set inside the nested scope";
            //        scope11.Complete();
            //    }
            //    scope10.Complete();
            //}
            //Debug.Assert(text.Value == "Set inside the nested scope");

            //TransactionalQueue<int> messageQueue = new TransactionalQueue<int>();
            //messageQueue.Enqueue(1);
            //using (TransactionScope scope11 = new TransactionScope())
            //{
            //    messageQueue.Enqueue(2);
            //    messageQueue.Enqueue(3);
            //    messageQueue.Enqueue(4);
            //    messageQueue.Enqueue(5);
            //    Debug.Assert(messageQueue.Dequeue() == 1);
            //}
            //Debug.Assert(messageQueue.Dequeue() == 1);
            //Debug.Assert(messageQueue.Count == 0);


            //TransactionalDictionary<int, string> dictionary = new TransactionalDictionary<int, string>();
            //dictionary.Add(1, "A");
            //using (TransactionScope scope11 = new TransactionScope())
            //{
            //    dictionary.Add(2, "B");
            //    dictionary.Add(3, "C");
            //    dictionary.Add(4, "D");
            //    Debug.Assert(dictionary[1] == "A");
            //    scope11.Complete();
            //}
            //Debug.Assert(dictionary[3] == "C");


            //TransactionalList<string> list = new TransactionalList<string>();
            //list.Add("a");
            //list.Add("b");
            //using (TransactionScope scope12 = new TransactionScope())
            //{
            //    list.Add("c");
            //    list.Remove("b");
            //    scope12.Complete();
            //}
            //string[] letters = list.ToArray();

            
        }
    }

    class MyClass
    {
        Transactional<int> m_Number = new Transactional<int>(3);
        public void MyMethod()
        {
            //TransactionalList<int> test = new TransactionalList<int>();
            //for (int n = 0; n < 9000006; n++)
            //{
            //    test.Add(new Transactional<int>(n));
            //}
            //using (TransactionScope scope13 = new TransactionScope())
            //{
            //    test.Add(new Transactional<int>(5));
            //    test[0] = new Transactional<int>(203);
            //    test[1] = 150;
            //    //scope13.Complete();
            //}
            //MessageBox.Show("Done");
        }
    }
}