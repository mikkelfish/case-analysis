﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VolatileResourceManager
{
    /// <summary>
    /// Applied to IEnlismentNotification objects that handle their own enlistment through the ambient <see cref="System.Transactions.Transaction.Current"/>
    /// property. This tells any resource managers that they shouldn't enlist the object.
    /// </summary>
    /// <remarks>
    /// The default constructor sets EnlistsSelf to true.
    /// </remarks>
    [global::System.AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class SelfEnlistmentAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets whether the object handles its own enlistment through amibent transactions. By default, true.
        /// </summary>
        public bool EnlistsSelf { get; set; }

        /// <summary>
        /// Public constructor. Sets EnlistsSelf to true.
        /// </summary>
        public SelfEnlistmentAttribute()
        {
            this.EnlistsSelf = true;
        }
    }
}
