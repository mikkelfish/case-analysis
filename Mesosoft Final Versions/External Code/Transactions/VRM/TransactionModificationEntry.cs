﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VolatileResourceManager
{
    public class TransactionModificationEntry<T, C>
    {
        public T Key { get; set; }
        public C Value { get; set; }
    }
}
