﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using MesosoftCore.XAML;
using System.ComponentModel;
using System.IO;
using System.Text;

namespace TestMesosoftCore
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public class testclass
        {
            public int TestInt { get; set; }
            public string TestString { get; set; }
        }

        private class testclass<T>
        {
            public T Value { get; set; }
            public string TestString { get; set; }

            public class whee<U>
            {
                public string Whee { get; set; }
                public U Val { get; set; }
            }
        }

        public class testclass<T, C>
        {
            public T Value { get; set; }

            private Dictionary<T, C> dictionary = new Dictionary<T, C>();
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]            
            public Dictionary<T, C> Dictionary
            {
                get
                {
                    return this.dictionary;
                }
            }

            public class whee<U>
            {
                public string Whee { get; set; }
                public U Val { get; set; }
            }
        }

        public class testclassdict<T, C> : Dictionary<T,C>
        {
            public T Value { get; set; }

            private Dictionary<T, C> dictionary = new Dictionary<T, C>();
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
            public Dictionary<T, C> Dictionary
            {
                get
                {
                    return this.dictionary;
                }
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //Passed test
            Dictionary<string, int> one = new Dictionary<string, int>();
            one.Add("set", 1);
            string xaml = XAMLUtilities.Save(one);
            using(MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            {
                object t = XAMLUtilities.Load(stream);
                int s = 0;
            }

            testclass<int, double>.whee<testclass<string, double>> whee = new testclass<int, double>.whee<testclass<string, double>>() { Whee = "TWo" };
            whee.Val = new testclass<string, double>();
            whee.Val.Dictionary.Add("this is a test", 5.3);
            xaml = XAMLUtilities.Save(whee);
            using (MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            {
                object t = XAMLUtilities.Load(stream);
                int s = 0;
            }

            //Passed test
            testclass<double> two = new testclass<double>() { Value = 0.5, TestString = "Were" };
            xaml = XAMLUtilities.Save(two);
            using (MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            {
                object t = XAMLUtilities.Load(stream);
                int s = 0;
            }

            //Passed test
            testclass<string, double> three = new testclass<string, double>() { Value = "Testing" };
            three.Dictionary.Add("oops", 1);
            three.Dictionary.Add("testing", 2);
            xaml = XAMLUtilities.Save(three);
            using (MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            {
                object t = XAMLUtilities.Load(stream);
                int s = 0;
            }

            //Passed test
            List<testclass> testList = new List<testclass>();
            testList.Add(new testclass() { TestInt = 2 });
            testList.Add(new testclass() { TestString = "235" });
            testList.Add(new testclass());
            testList.Add(new testclass() { TestString = "35", TestInt = 5 });
            xaml = XAMLUtilities.Save(testList);
            using (MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            {
                object t = XAMLUtilities.Load(stream);
                int s = 0;
            }

            //Passed test
            testclass<testclass> doubleTest = new testclass<testclass>();
            doubleTest.Value = new testclass() { TestInt = 5 };
            xaml = XAMLUtilities.Save(doubleTest);
            using (MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            {
                object t = XAMLUtilities.Load(stream);
                int s = 0;
            }

            //Passed test
            testclass<testclass<testclass>, double> superTest = new testclass<testclass<testclass>, double>();
            superTest.Dictionary.Add(doubleTest, 5);
            xaml = XAMLUtilities.Save(superTest);
            using (MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            {
                object t = XAMLUtilities.Load(stream);
                int s = 0;
            }

            //testclassdict<testclass<testclass>, testclass> newTest = new testclassdict<testclass<testclass>, testclass>();
            //newTest.Value = new testclass<testclass>() { TestString = "testing2552" };
            //newTest.Add(doubleTest, one);
            //xaml = XAMLUtilities.Save(newTest);
            //using (MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            //{
            //    object t = XAMLUtilities.Load(stream);
            //    int s = 0;
            //}

            Queue<double> queue = new Queue<double>();
            queue.Enqueue(0);
            queue.Enqueue(2);
            queue.Enqueue(5);
            queue.Enqueue(10);
            xaml = XAMLUtilities.Save(queue);
            using (MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            {
                object t = XAMLUtilities.Load(stream);
                int s = 0;
            }

            Stack<string> stack = new Stack<string>();
            stack.Push("tes");
            stack.Push("23");
            stack.Push("last");
            xaml = XAMLUtilities.Save(stack);
            using (MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            {
                object t = XAMLUtilities.Load(stream);
                int s = 0;
            }

            LinkedList<testclass<int>> list = new LinkedList<testclass<int>>();
            LinkedListNode<testclass<int>> node = list.AddFirst(new testclass<int>() { Value = 1 });
            node = list.AddAfter(node, new testclass<int>() { Value = 1 });
            node = list.AddAfter(node, new testclass<int>() { Value = 1 });
            xaml = XAMLUtilities.Save(list);
            using (MemoryStream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(xaml)))
            {
                object t = XAMLUtilities.Load(stream);
                int s = 0;
            }


        }
    }
}
