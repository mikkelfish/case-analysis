﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Development
{
    /// <summary>
    /// Enumeration representing the current status of documentation.
    /// </summary>
    public enum DocumentedStage 
    { 
        /// <summary>
        /// No documentation
        /// </summary>
        None, 
        /// <summary>
        /// Internal documentation completed and accepted.
        /// </summary>
        InternalAccepted, 
        /// <summary>
        /// Internal documentation completed and ready for review.
        /// </summary>
        InternalForReview,
        /// <summary>
        /// Documentation completed in flow chart only.
        /// </summary>
        FlowChartOnly,
        /// <summary>
        /// Public documentation completed and ready for review.
        /// </summary>
        PublicForReview,
        /// <summary>
        /// Public documentation completely accepted. Once this stage is reached, any 
        /// change will trigger an alert that requests explanation.
        /// </summary>
        PublicAccepted 
    };

    /// <summary>
    /// Enumeration for the different stages of development.
    /// </summary>
    public enum DevelopmentStage
    {
        /// <summary>
        /// General announcement that the code has a lot of work to be done. Structure may change completely.
        /// </summary>
        UnderDevelopment,
        /// <summary>
        /// As far as the developer is concerned, the code is done. It just needs to be checked out.
        /// </summary>
        ForReview,
        /// <summary>
        /// Reviewer has accepted code. Has not been tested.
        /// </summary>
        AcceptedNeedsTesting,
        /// <summary>
        /// Either developer, reviewer or tester has discovered a bug. The bug has yet to be fixed.
        /// </summary>
        BugFixing,
        /// <summary>
        /// Either developer, reviewer, or tester has noticed that there could be further optimization of algorithms. This has yet to be completed.
        /// </summary>
        NeedsOptimization,
        /// <summary>
        /// Either developer, reviewer, or tester feels that there should be more exception handling.
        /// </summary>
        NeedsExceptionHandling,
        /// <summary>
        /// The code is too complex and needs to be refactored and/or simplified.
        /// </summary>
        NeedsSimplification,
        /// <summary>
        /// The code is completely good as far as everyone is concerned.
        /// </summary>
        Accepted

    }

    /// <summary>
    /// Give a Mesosoft Development attribute the proper priority for this problem/request for input.
    /// </summary>
    public enum DevelopmentPriority
    {
        /// <summary>
        /// This should be used for all bugs that are a problem whenever the function is called
        /// or implementation that needs to be done to get the object to work
        /// </summary>
        Critical,
        /// <summary>
        /// This should be used when you want to mark where there should be exception handling 
        /// but you don't want to do it yet or it isn't decided how to handle it.
        /// </summary>
        Exception,
        /// <summary>
        /// This should be used primarily for optimization possibilities, but also for bugs 
        /// that appear in rare situations
        /// </summary>
        Low,
        /// <summary>
        /// This should be used only if you don't know whether something is correct or whether
        /// something can be done, and want someone else to look at it.
        /// </summary>
        Glance
    }

}
