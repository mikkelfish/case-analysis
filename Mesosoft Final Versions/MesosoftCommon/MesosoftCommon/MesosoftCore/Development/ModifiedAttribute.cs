﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Development
{
    /// <summary>
    /// Apply when you make a modification to an existing item that has been accepted in a prior form.
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public class ModifiedAttribute : DevelopmentAttribute
    {
        //The author that made the modification.
        public ModifiedAttribute(string authorName)
            : base(authorName)
        {

        }
    }
}
