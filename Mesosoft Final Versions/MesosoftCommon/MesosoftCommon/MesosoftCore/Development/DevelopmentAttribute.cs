﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Development
{
    /// <summary>
    /// The base for all Mesosoft Development Attributes. These attributes will help the automated
    /// management system keep track of changes, bugs and TODOs.
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
    public abstract class DevelopmentAttribute : Attribute
    {
        /// <summary>
        /// Gets the name of the author.
        /// </summary>
        public string Author { get; private set; }
        
        /// <summary>
        /// Gets or sets the date of the change.
        /// </summary>
        public string Date { get; set; }
        
        /// <summary>
        /// Gets or sets optional comments about the change.
        /// </summary>
        public string Comments { get; set; }
        
        /// <summary>
        /// Gets or sets the version that the change applies to.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// The current status of documentation for the change.
        /// </summary>
        public DocumentedStage DocumentedStage { get; set; }

        /// <summary>
        /// Gets or sets a description of the current development stage.
        /// </summary>
        public DevelopmentStage DevelopmentStage { get; set; }

        /// <summary>
        /// The public constructor for DevelopmentAttributes
        /// </summary>
        /// <param name="authorName">The name of the author of the attribute.</param>
        public DevelopmentAttribute(string authorName)
        {
            this.Author = authorName;
        }
    }
}
