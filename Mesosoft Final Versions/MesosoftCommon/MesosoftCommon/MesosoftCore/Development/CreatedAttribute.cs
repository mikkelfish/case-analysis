﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Development
{
    /// <summary>
    /// Apply when creating an item and using the Mesosoft Development System.
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public class CreatedAttribute : DevelopmentAttribute
    {
        /// <summary>
        /// Public constructor for the CreatedAttribute.
        /// </summary>
        /// <param name="author">The name of the author of the attribute.</param>
        public CreatedAttribute(string author)
            : base(author)
        {

        }
    }
}
