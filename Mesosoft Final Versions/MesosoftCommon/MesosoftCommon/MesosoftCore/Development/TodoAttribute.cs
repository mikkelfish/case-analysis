﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Development
{
    /// <summary>
    /// Attribute applied to targets that need implementation or testing.
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public class TodoAttribute : DevelopmentAttribute
    {
        /// <summary>
        /// Gets or sets whether the issue has been resolved.
        /// </summary>
        /// <remarks>People should remember to mark things as completed when the issue is resolved. Attributes
        /// will only be removed on version review.</remarks>
        public bool Completed { get; set; }

        /// <summary>
        /// Gets or sets the person that addressed the issue.
        /// </summary>
        public string CompletedBy { get; set; }

        /// <summary>
        /// Gets or sets the date that the issue was addressed.
        /// </summary>
        public string CompletionDate { get; set; }

        /// <summary>
        /// Gets the priority of the request.
        /// </summary>
        public DevelopmentPriority Priority { get; private set; }

        /// <summary>
        /// Public constructor for Todo Attributes.
        /// </summary>
        /// <param name="author">The name of the attribute author.</param>
        /// <param name="priority">The priority of the Todo request.</param>
        public TodoAttribute(string author, DevelopmentPriority priority)
            : base(author)
        {
            this.Priority = priority;
        }


    }
}
