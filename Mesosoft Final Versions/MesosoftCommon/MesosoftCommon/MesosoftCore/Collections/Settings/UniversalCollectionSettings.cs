﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Collections
{
    /// <summary>
    /// Base class for implementation that the <see cref="MesosoftCore.Collections.IUniversalCollection"/> uses in order to manage its backing source.
    /// </summary>
    public abstract class UniversalCollectionSettings
    {
        private Uri uri;
        /// <summary>
        /// The location of the backing source. If the implementation uses a nonstandard storage method, this property might not comply with accepted standards.
        /// </summary>
        public Uri URI
        {
            get { return uri; }
            set { uri = value; }
        }

        private Type backingType;
        /// <summary>
        /// The type of the <see cref="MesosoftCore.Collections.IUniversalCollectionImplementation"/> implementation that the settings object belongs to.
        /// </summary>
        public Type BackingType
        {
            get { return backingType; }
        }

        private bool updateSourceOnExit;
        /// <summary>
        /// Gets or sets whether the implementation should save when the program exits.
        /// </summary>
        public bool UpdateSourceOnExit
        {
            get { return updateSourceOnExit; }
            set { updateSourceOnExit = value; }
        }

        /// <summary>
        /// The public constructor, takes the type of the backing implementation.
        /// </summary>
        /// <param name="backingType">The type of the backing implementation.</param>
        public UniversalCollectionSettings(Type backingType)
        {
            this.backingType = backingType;
        }
    }
}
