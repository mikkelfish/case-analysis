﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using MesosoftCore.Tracking.Transactions;
using MesosoftCore.Development;
using System.Collections.Specialized;
using System.ComponentModel;
using MesosoftCore.Tracking.TracksChanges;
using System.Diagnostics;

namespace MesosoftCore.Collections
{
    public abstract class UniversalCollection<T> : IUniversalCollection<T>
    {

        private TransactionalLock transLock = new TransactionalLock();
        private Transaction transaction;
        private DefaultTracking trackingObject;

        protected virtual IUniversalCollectionImplementation<T> Implementation { get; set; }

        public UniversalCollection()
        {
            this.trackingObject = new DefaultTracking(this);
        }

        #region IUniversalCollection Members

        public UniversalCollectionSettings Settings
        {
            get { return this.Implementation.Settings; }
        }

        public bool UpdateSourceOnExit
        {
            get
            {
                return this.Settings.UpdateSourceOnExit;
            }
            set
            {
                this.Settings.UpdateSourceOnExit = value;
            }
        }

        public bool IsVolatile
        {
            get { return this.Implementation.IsVolatile; }
        }

        public bool InTransaction
        {
            get;
            private set;
        }

        public bool CanPersist
        {
            get { return this.Implementation.CanPersist; }
        }

        [Todo("Mikkel", DevelopmentPriority.Critical, Comments = "Put some sort of locking on this so the collection can't change while it's writing.")]
        public virtual void UpdateSource()
        {
            if (this.InTransaction)
            {
                throw new Exception("Cannot update source while in transaction. End the transaction to update the source.");
            }

            if (this.IsTracking)
            {
                throw new Exception("Cannot update source while in tracking mode. End tracking mode to update the source.");
            }

            this.Implementation.UpdateSource();
        }

        [Todo("Mikkel", DevelopmentPriority.Critical, Comments="Put some sort of locking on this so the collection can't change while it's writing.")]
        public void UpdateSource(UniversalCollectionSettings settings)
        {
            if (this.InTransaction)
            {
                throw new Exception("Cannot update source while in transaction. End the transaction to update the source.");
            }

            if (this.IsTracking)
            {
                throw new Exception("Cannot update source while in tracking mode. End tracking mode to update the source.");
            }

            this.Implementation.UpdateSource(settings);
        }

        public Guid Enlist()
        {
            //Can't start a transaction while tracking
            if (this.IsTracking)
            {
                throw new InvalidOperationException("Cannot begin transaction while in Tracking Mode.");
            }

            if (Transaction.Current == null)
            {
                throw new InvalidOperationException("There is no current ambient transaction.");
            }

            //Inform the locking manager that a transaction is in progress
            this.transLock.Lock();

            Guid toRet = Guid.NewGuid();

            //Get the current transaction
            Debug.Assert(this.transaction == null);
            this.transaction = Transaction.Current;
            Debug.Assert(this.transaction.TransactionInformation.Status == TransactionStatus.Active);

            //Pick the right enlistment option based on the Implementation
            //This ensures that failure will be dealt with properly 
            //See the System.Transaction documentation
            if (this.IsVolatile) Transaction.Current.EnlistVolatile(this, EnlistmentOptions.None);
            else Transaction.Current.EnlistDurable(toRet, this, EnlistmentOptions.None);
                        
            //Tell the Implementation it should prepare for the transaction
            this.Implementation.StartTransaction();
            this.InTransaction = true;

            return toRet;
        }


        #endregion

        #region INotifyCollectionChanged Members

        public event System.Collections.Specialized.NotifyCollectionChangedEventHandler CollectionChanged;

        protected void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion

        #region IQueryable Members

        public Type ElementType
        {
            get { return typeof(T); }
        }

        public System.Linq.Expressions.Expression Expression
        {
            get { return System.Linq.Expressions.Expression.Constant(this.Implementation.AsQueryable<T>()); }
        }

        public IQueryProvider Provider
        {
            get { return this; }
        }

        #endregion

        #region IEnumerable Members

        public System.Collections.IEnumerator GetEnumerator()
        {
            return this.Implementation.GetEnumerator();
        }

        #endregion

        #region IQueryProvider Members

        public IQueryable<TElement> CreateQuery<TElement>(System.Linq.Expressions.Expression expression)
        {
            IQueryable<TElement> ele = this.Implementation.AsQueryable<T>().Provider.CreateQuery<TElement>(expression);
            return ele;
        }

        public IQueryable CreateQuery(System.Linq.Expressions.Expression expression)
        {
            if (this.Implementation is IQueryable)
            {
                return this.CreateQuery<T>(expression);
            }

            return this.Implementation.AsQueryable();
        }

        public TResult Execute<TResult>(System.Linq.Expressions.Expression expression)
        {
            return this.Implementation.AsQueryable<T>().Provider.Execute<TResult>(expression);            
        }

        public object Execute(System.Linq.Expressions.Expression expression)
        {
            return this.Execute<T>(expression);
        }

        #endregion

        #region IEnlistmentNotification Members

        void IEnlistmentNotification.Commit(System.Transactions.Enlistment enlistment)
        {
            this.Implementation.Commit();
            this.transLock.Unlock();
            enlistment.Done();
        }

        void IEnlistmentNotification.InDoubt(System.Transactions.Enlistment enlistment)
        {
            this.Implementation.InDoubt();
            this.transLock.Unlock();
            enlistment.Done();
        }

        void IEnlistmentNotification.Prepare(System.Transactions.PreparingEnlistment preparingEnlistment)
        {
            bool ok = this.Implementation.Prepare();
            if (!ok)
            {
                preparingEnlistment.ForceRollback();
            }
            else preparingEnlistment.Done();
        }

        void IEnlistmentNotification.Rollback(System.Transactions.Enlistment enlistment)
        {
            this.Implementation.Rollback();
            this.transLock.Unlock();
            enlistment.Done();
        }

        #endregion

        #region ITracksChanges Members

        public bool StartTracking()
        {
            return this.trackingObject.StartTracking();
        }

        public bool Undo()
        {
            return this.trackingObject.Undo();
        }

        public bool Redo()
        {
            return this.trackingObject.Redo();
        }

        public void StopTracking()
        {
           this.trackingObject.EndTracking();
        }

        public bool IsTracking
        {
            get { return this.trackingObject.IsActive; }
        }

        public event MesosoftCore.Tracking.PreviewUndoRedoTrackingEventHandler PreviewTrackingEvent
        {
            add
            {
                this.trackingObject.PreviewTrackingEvent += value;
            }
            remove
            {
                this.trackingObject.PreviewTrackingEvent -= value;
            }
        }


        public event MesosoftCore.Tracking.UndoRedoTrackingEventHandler TrackingEvent
        {
            add
            {
                this.trackingObject.TrackingEvent += value;
            }
            remove
            {
                this.trackingObject.TrackingEvent += value;
            }
        }

        #endregion

        #region ICollection<T> Members

        public void Add(T item)
        {
            this.Implementation.Add(item);
            this.trackingObject.RegisterObject(item);
            if (this.IsTracking) this.trackingObject.AddedItems(new object[] { item });
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }


        public void Clear()
        {
            List<T> removedItems = new List<T>();
            foreach (T item in this.Implementation)
            {
                this.trackingObject.UnregisterObject(item);
                removedItems.Add(item);
            }
            if (this.IsTracking) this.trackingObject.RemovedItems(removedItems.Select<T, object>(t => t ).ToArray());
            this.Implementation.Clear();
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public bool Contains(T item)
        {
            return this.Implementation.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            this.Implementation.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return (this.Implementation as ICollection<T>).Count; }
        }

        public bool IsReadOnly
        {
            get { return this.Implementation.IsReadOnly; }
        }

        public bool Remove(T item)
        {
            bool success = this.Implementation.Remove(item);
            if (!success) return false;
            this.trackingObject.UnregisterObject(item);
            if(this.IsTracking) this.trackingObject.RemovedItems(new object[] { item });
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
            return true;
            
        }

        #endregion

        #region IEnumerable<T> Members

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return this.Implementation.GetEnumerator();
        }

        #endregion


        #region ICollection Members

        public void CopyTo(Array array, int index)
        {
            this.Implementation.CopyTo(array, index);
        }

        public bool IsSynchronized
        {
            get { return this.Implementation.IsSynchronized; }
        }

        public object SyncRoot
        {
            get { return this.Implementation.SyncRoot; }
        }

        #endregion
    }
}
