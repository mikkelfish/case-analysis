﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Transactions;
using System.Collections;

namespace MesosoftCore.Collections
{
    /// <summary>
    /// Interface that supports collections that satisfy the Mesosoft Universal Collection engine implementation.
    /// </summary>
    /// <remarks>
    /// UniversalCollection is the catch all collection that provides all functionality a person could ever want. 
    /// This includes:
    /// <list type="bullet">
    /// <item>
    /// <description>
    /// Backing store blind operation. The list can be stored anywhere and the user doesn't have to care. The backing store 
    /// is IUniversalCollectionImplementation.
    /// </description>
    /// </item>
    /// <item>
    /// <description>
    /// Most backing stores are capable of persistance.
    /// </description>
    /// </item>    
    /// <item>
    /// <description>
    /// Undo/Redo capabilities by implementing <see cref="Tracking.ITracksChanges" />.
    /// </description>
    /// </item>
    /// <item>
    /// <description>
    /// Transaction capabilities through <see cref="IEnlistmentNotification"/>.
    /// </description>
    /// </item>
    /// <item>
    /// <description>
    /// LINQ support by implementing both <see cref="IQueryable"/> and <see cref="IQueryProvider"/>.
    /// </description>
    /// </item>
    /// <item>
    /// <description>
    /// Is bindable since it impelements <see cref="INotifyCollectionChanged"/>.
    /// </description>
    /// </item>
    /// </list>
    /// Here is a diagram of the UniversalCollection process.
    /// <img src="../UniversalCollection.jpg" />
    /// </remarks>
    public interface IUniversalCollection : ICollection, INotifyCollectionChanged, IQueryable, IQueryProvider, IEnlistmentNotification, Tracking.ITracksChanges
    {
        /// <summary>
        /// Gets or sets whether the collection manager should have the collection persist when the program exits.
        /// </summary>
        bool UpdateSourceOnExit { get; set; }
        
        /// <summary>
        /// Gets whether the collection is volatile. If so it means that there is information stored only in memory that may be lost on failure.
        /// It is recommended that collections be nonvolatile if they are operating in a critical environment.
        /// </summary>
        bool IsVolatile { get; }
        
        /// <summary>
        /// Gets whether the collection is in the middle of a transaction. A transaction is a set of commands that have to all executive successfully, or else
        /// the entire command set is rolled back.
        /// </summary>
        bool InTransaction { get; }

        /// <summary>
        /// Gets whether the implementation support persistance.
        /// </summary>
        bool CanPersist { get; }

        /// <summary>
        /// Updates the source by persisting using the default <see cref="UniversalCollectionSettings" />. This will move volatile information to a long-term state.
        /// </summary>
        void UpdateSource();

        /// <summary>
        /// Persists the collection using non-default <see cref="UniversalCollectionSettings" />. This is good for creating a copy of the collection at a particular location
        /// or making a backup.
        /// </summary>
        /// <param name="settings">Settings that dictate how to store the list.</param>
        void UpdateSource(UniversalCollectionSettings settings);

        /// <summary>
        /// Gets the settings that control the backing store.
        /// </summary>
        UniversalCollectionSettings Settings { get; }

        /// <summary>
        /// Enlist the collection in the current ambient transaction.
        /// </summary>
        Guid Enlist();

    }
}
