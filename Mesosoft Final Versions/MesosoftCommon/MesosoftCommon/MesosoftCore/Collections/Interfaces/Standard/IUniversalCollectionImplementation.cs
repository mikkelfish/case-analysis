﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCore.Delegates;
using System.Collections;

namespace MesosoftCore.Collections
{
    /// <summary>
    /// Provides methods for the backing store for <see cref="IUniversalCollection"/> collections.
    /// </summary>
    /// <remarks>
    /// This interface allows the UniversalCollection to be agnostic about how the data in its collection is actually stored.
    /// </remarks>
    public interface IUniversalCollectionImplementation : ICollection
    {
        /// <summary>
        /// Called when a transaction is committed.
        /// </summary>
        void Commit();
        /// <summary>
        ///   Called when a transaction is rolled back.
        /// </summary>
        void Rollback();
        /// <summary>
        /// Called when a transaction is ended.
        /// </summary>
        /// <returns>  Whether the operation is successful, or whether there should be a rollback.</returns>
        bool Prepare();
        /// <summary>
        /// Called when a transaction is aborted, and then restarted at a different time.
        /// </summary>
        void InDoubt();
        /// <summary>
        ///   Called when a transaction is started.
        /// </summary>
        void StartTransaction();
        /// <summary>
        /// Called when a transaction is ended, but before the prepare function is called.
        /// </summary>
        void EndTransaction();

        /// <summary>
        /// Notifies the implementation that it should store its contents to a permanent location.  This function uses the default settings.
        /// </summary>
        void UpdateSource();
        /// <summary>
        /// Notifies implementation that it should store its contents to a location specified by the passed settings.
        /// </summary>
        /// <param name="settings">Description of the target location.</param>
        void UpdateSource(UniversalCollectionSettings settings);

        /// <summary>
        /// Notifies implementation that it should load its contents from its default setting.
        /// </summary>
        void LoadFromSource();
        /// <summary>
        /// Notifies implementation that it should load its contents from a location specified by the passed settings.
        /// </summary>
        /// <param name="settings">Description of the location that the collection should load from.</param>
        void LoadFromSource(UniversalCollectionSettings settings);

        /// <summary>
        /// Gets whether the implementation can persist to a permanent location.
        /// </summary>
        bool CanPersist { get; }

        /// <summary>
        /// Event used to report an error if there is a problem persisting or in the transaction.
        /// </summary>
        event ReportExceptionHandler Error;
        /// <summary>
        /// Default settings used by the implementation.
        /// </summary>
        UniversalCollectionSettings Settings { get; set; }

        /// <summary>
        /// Gets whether the implementation stores (even if just temporarily) information in memory only.
        /// </summary>
        bool IsVolatile { get; }
    }
}
