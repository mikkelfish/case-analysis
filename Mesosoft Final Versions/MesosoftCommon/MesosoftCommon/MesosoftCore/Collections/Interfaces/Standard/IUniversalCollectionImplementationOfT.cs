﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Collections
{
    /// <summary>
    /// Provides methods for the backing store for <see cref="IUniversalCollection"/> collections.
    /// </summary>
    /// <remarks>
    /// This interface allows the UniversalCollection to be agnostic about how the data in its collection is actually stored.
    /// </remarks>
    /// <typeparam name="T">The collection type.</typeparam>    
    public interface IUniversalCollectionImplementation<T> : IUniversalCollectionImplementation, ICollection<T>
    {
    }
}
