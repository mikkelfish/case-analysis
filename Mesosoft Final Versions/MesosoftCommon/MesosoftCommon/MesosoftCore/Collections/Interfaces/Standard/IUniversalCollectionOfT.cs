﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MesosoftCore.Development;

namespace MesosoftCore.Collections
{
    /// <summary>
    /// A generic implementation of ICollection, an Interface that supports collections that satisfy the Mesosoft Universal Collection engine implementation.
    /// </summary>
    /// <typeparam name="T">Type can be of any class. However, if it supports INotifyPropertyChanging and INotifyPropertyChanged then it will have better support the Undo/Redo functionality. </typeparam>
    /// <remarks>
    /// UniversalCollection is the catch all collection that provides all functionality a person could ever want. 
    /// This includes:
    /// <list type="bullet">
    /// <item>
    /// <description>
    /// Backing store blind operation. The list can be stored anywhere and the user doesn't have to care. The backing store 
    /// is IUniversalCollectionImplementation.
    /// </description>
    /// </item>
    /// <item>
    /// <description>
    /// Most backing stores are capable of persistance.
    /// </description>
    /// </item>    
    /// <item>
    /// <description>
    /// Undo/Redo capabilities by implementing <see cref="Tracking.ITracksChanges" />.
    /// </description>
    /// </item>
    /// <item>
    /// <description>
    /// Transaction capabilities through <see cref="System.Transactions.IEnlistmentNotification"/>.
    /// </description>
    /// </item>
    /// <item>
    /// <description>
    /// LINQ support by implementing both <see cref="IQueryable"/> and <see cref="IQueryProvider"/>.
    /// </description>
    /// </item>
    /// <item>
    /// <description>
    /// Is bindable since it impelements <see cref="System.Collections.Specialized.INotifyCollectionChanged"/>.
    /// </description>
    /// </item>
    /// </list>
    /// Here is a diagram of the UniversalCollection process.
    /// <img src="../UniversalCollection.jpg" />
    /// </remarks>
    [Created("Mikkel", DocumentedStage = DocumentedStage.None, DevelopmentStage = DevelopmentStage.UnderDevelopment, Version = "0.0")]
    public interface IUniversalCollection<T> : IUniversalCollection, ICollection<T>, IQueryable<T>
    {

    }
}
