﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace MesosoftCore.Collections.Interfaces.List
{
    /// <summary>
    /// A <see cref="IUniversalCollectionImplementation"/>that supports indexing.
    /// </summary>
    public interface IUniversalListCollectionImplemenation : IUniversalCollectionImplementation, IList
    {
    }
}
