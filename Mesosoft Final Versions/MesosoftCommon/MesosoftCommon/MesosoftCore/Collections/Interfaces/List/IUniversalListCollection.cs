﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace MesosoftCore.Collections
{
    /// <summary>
    /// A <see cref="IUniversalCollection"/> that supports indexing.
    /// </summary>
    public interface IUniversalListCollection : IUniversalCollection, IList
    {
    }
}
