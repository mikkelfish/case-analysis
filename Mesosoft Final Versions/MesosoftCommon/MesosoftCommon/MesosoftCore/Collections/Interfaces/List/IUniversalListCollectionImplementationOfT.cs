﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Collections.Interfaces.List
{
    /// <summary>
    /// A <see cref="IUniversalCollectionImplementation{T}"/> that supports indexing.
    /// </summary>
    /// <typeparam name="T">Can be of any class type, but if it supports <see cref="System.ComponentModel.INotifyPropertyChanged"/> and <see cref="System.ComponentModel.INotifyPropertyChanging"/>then it will have better undo/redo capabilities.</typeparam>
    public interface IUniversalListCollectionImplementation<T> : IUniversalCollectionImplementation<T>, IList<T>
    {
    }
}
