﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Inspection
{
    /// <summary>
    /// Reports the arguments that were passed to the object on creation. 
    /// At present, this allows for the XAML writer to write the object even if it doesn't have a default constructor and also
    /// for it to be copied. 
    /// </summary>
    /// <example>
    /// <code>
    /// public class Test : IConstructorArgsProvider
    /// {
    ///     public object[] ConstructionArgs {get ;}
    /// 
    ///     public Test(int width)
    ///     {
    ///         this.ConstructionArgs = new object[]{width};
    ///     }
    ///     
    ///     public Test(string name, int width)
    ///     {
    ///         this.ConstructionArgs = new object[]{name, width};
    ///     }
    /// }
    /// </code>
    /// </example>
    public interface IConstructorArgsProvider
    {
        /// <summary>
        /// Gets the construction arguments that were passed to the object on creation.
        /// </summary>
        object[] ConstructionArgs { get; }
    }
}
