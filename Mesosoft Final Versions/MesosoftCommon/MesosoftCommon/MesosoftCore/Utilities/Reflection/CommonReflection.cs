﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using MesosoftCore.Development;

namespace MesosoftCore.Utilities.Reflection
{
    public static class CommonReflection
    {
        //Stores generic MethodInfos so they don't need to keep being reformed.
        private static Dictionary<string, MethodInfo> compiledMethods = new Dictionary<string, MethodInfo>();

        /// <summary>
        /// Run a generic function. 
        /// </summary>
        /// <param name="functionName">The method name.</param>
        /// <param name="parameterTypes">The generic parameter types.</param>
        /// <param name="obj">The object that calls the function.</param>
        /// <param name="args">Method arguments.</param>
        /// <returns>The result of the method.</returns>
        /// <exception cref="ArgumentNullException">Either obj or parameterTypes is null.</exception>
        /// <exception cref="InvalidOperationException">The functionName cannot be found in the object's type.</exception>
        [Created("Mikkel", DocumentedStage=DocumentedStage.InternalForReview)]
        public static object RunGenericMethod(string functionName, Type[] parameterTypes, object obj, object[] args)
        {
            if (obj == null) throw new ArgumentNullException("Object cannot be null.");
            if (parameterTypes == null) throw new ArgumentNullException("Parameter types cannot be null.");

            //Get the method to use.
            MethodInfo info = obj.GetType().GetMethod(functionName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            if (info == null) throw new InvalidOperationException("The function " + functionName + " was not found in type " + obj.GetType());
            return RunGenericMethod(info, parameterTypes, obj, args);
        }

        /// <summary>
        /// Run a generic function. 
        /// </summary>
        /// <param name="function">The method to invoke.</param>
        /// <param name="parameterTypes">The generic parameter types.</param>
        /// <param name="obj">The object that calls the function.</param>
        /// <param name="args">Method arguments.</param>
        /// <returns>The result of the method.</returns>
        /// <exception cref="ArgumentNullException">Either function, obj, or parameterTypes is null.</exception>
        /// <exception cref="InvalidOperationException">The functionName cannot be found in the object's type.</exception>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview)]       
        public static object RunGenericMethod(MethodInfo function, Type[] parameterTypes, object obj, object[] args)
        {
            if (obj == null) throw new ArgumentNullException("Object cannot be null.");
            if (parameterTypes == null) throw new ArgumentNullException("Parameter types cannot be null.");
            if (function == null) throw new ArgumentNullException("Function cannot be null.");

            MethodInfo info = null;
            //Get the key to look in the compiled methods dictionary.
            string key = function.ReflectedType.FullName + "." + function.Name;
            foreach (Type type in parameterTypes)
            {
                key += " " + type.FullName;
            }

            if (compiledMethods.ContainsKey(key))
            {
                info = compiledMethods[key];
            }
            else 
            {
                //Make the generic method and add it to the compiled methods dictionary.
                info = function.MakeGenericMethod(parameterTypes);
                compiledMethods.Add(key, info);
            }

            return info.Invoke(obj, args);
        }
    }
}
