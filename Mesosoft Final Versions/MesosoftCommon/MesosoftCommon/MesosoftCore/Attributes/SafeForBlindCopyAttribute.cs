﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCore.Development;

namespace MesosoftCore.Attributes
{
    /// <summary>
    /// Attribute signifies that the object can have a successful copy just by creating an object 
    /// with the default constructor (or if object is IConstructorProvider a non-default constructor)
    /// and setting the properties. This means that there are no side effects
    /// from calling functions, etc.
    /// </summary>
    /// <remarks>
    /// It's up to the developer to properly apply this attribute. Refer to the examples of classes where it 
    /// can and cannot be used.
    /// </remarks>
    /// <example>
    /// The following class should have the attribute applied. Notice it just has properties and no members
    /// that store things because of side effects.
    /// <code>
    /// [SafeForBlindCopy]
    /// public class SafeTestClass
    /// {
    ///     public int TestInt{get;set;}
    ///     public string TestString{get;set;}
    ///     
    ///     public int DoSomething()
    ///     {
    ///     
    ///     }
    /// }
    /// </code>
    /// The following class should NOT have the attribute.
    /// <code>
    /// <![CDATA[    
    /// public class WrongTestClass
    /// {
    ///     List<object> storeResults = new List<object();
    ///     
    ///     public int TestInt{get;set}
    ///     
    ///     public void SideEffect(object obj)
    ///     {
    ///         //This line stores some instance information that is not public and is not valid
    ///         //from execution to execution. This means that the class is not safe for a blind copy.
    ///         storeResults.Add(obj.DoSomethingExtension());
    ///     }
    /// }
    /// ]]>
    /// </code>
    /// There can be some side effects if they occur in the constructor and the class is an <see cref="IConstructorProvider"/>
    /// <code>
    /// [SafeForBlindCopyAttribute]
    /// public class ConstructorTestClass : IConstructorProvider
    /// {
    ///     public object[] ConstructorArgs{get;set;} //IConstructorProvider implementation
    ///     
    ///     private int someCalculation;
    ///     public ConstructorTestClass(int firstNumber)
    ///     {
    ///         this.ConstructorArgs = new object[]{firstNumber}; //Store the arguments so they can be saved
    ///         this.someCalculation = firstNumber + 150; //Even though this is private instance information, it's in the constructor
    ///     }
    /// }
    /// </code>
    /// </example>
    [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
    [global::System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class SafeForBlindCopyAttribute : Attribute
    {

    }
}
