﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Delegates
{
    /// <summary>
    /// Notifies listeners when there is an exception thrown.
    /// </summary>
    /// <param name="sender">Object exception was generated in.</param>
    /// <param name="ex">The exception.</param>
    public delegate void ReportExceptionHandler(object sender, Exception ex);
}
