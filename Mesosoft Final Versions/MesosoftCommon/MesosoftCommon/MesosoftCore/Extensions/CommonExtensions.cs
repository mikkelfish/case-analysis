﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.IO;
using MesosoftCore.Attributes;
using MesosoftCore.Inspection;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using MesosoftCore.Development;

namespace MesosoftCore.Extensions
{
    /// <summary>
    /// Contains common extension methods
    /// </summary>
    public static class CommonExtensions
    {
        /// <summary>
        /// Make a best-guess complete copy of the object. This means that if the object is serializable, then it will use
        /// serialization to memory to create the copy. If the object is <see cref="ICloneable"/>, it will call <see cref="ICloneable.Clone"/>. Otherwise, it must
        /// have the <see cref="SafeForBlindCopyAttribute"/> or it will throw an exception.
        /// </summary>
        /// <param name="source">The object to copy.</param>
        /// <param name="guaranteeExact">Whether the method will accept <see cref="ICloneable"/> as a copy source.</param>
        /// <returns>A best-guess complete copy of the object.</returns>
        /// <remarks>If guaranteeExact is true, then the function will attempt to create a copy through serialization or if that fails, a blind copy.
        /// It will not use <see cref="ICloneable.Clone"/> because that may be a shallow copy and will throw an exception if it cannot serialize or use a blind copy.
        /// Otherwise, it will attempt to serialize and then call <see cref="ICloneable.Clone"/> before it tries a blind copy.</remarks>
        /// <exception cref="NotSupportedException">Thrown if the object is not serializable, cloneable or <see cref="SafeForBlindCopyAttribute"/> if guaranteeExact is false.
        /// Otherwise, thrown if not serializable or <see cref="SafeForBlindCopyAttribute"/>.</exception>
        /// <exception cref="Exception">Thrown if constructor not found during a <see cref="SafeForBlindCopyAttribute"/> action.</exception>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
        public static object CreateCopy(this object source, bool guaranteeExact)
        {
            return CreateCopy(source, guaranteeExact);
        }


        /// <summary>
        /// Make a best-guess complete copy of the object. This means that if the object is serializable, then it will use
        /// serialization to memory to create the copy. If the object is <see cref="ICloneable"/>, it will call <see cref="ICloneable.Clone"/>. Otherwise, it must
        /// have the <see cref="SafeForBlindCopyAttribute"/> or it will throw an exception.
        /// </summary>
        /// <typeparam name="T">Type of object to copy. No constraints.</typeparam>
        /// <param name="source">The object to copy.</param>
        /// <param name="guaranteeExact">Whether the method will accept <see cref="ICloneable"/> as a copy source.</param>
        /// <returns>A best-guess complete copy of the object.</returns>
        /// <remarks>If guaranteeExact is true, then the function will attempt to create a copy through serialization or if that fails, a blind copy.
        /// It will not use <see cref="ICloneable.Clone"/> because that may be a shallow copy and will throw an exception if it cannot serialize or use a blind copy.
        /// Otherwise, it will attempt to serialize and then call <see cref="ICloneable.Clone"/> before it tries a blind copy.</remarks>
        /// <exception cref="NotSupportedException">Thrown if the object is not serializable, cloneable or <see cref="SafeForBlindCopyAttribute"/> if guaranteeExact is false.
        /// Otherwise, thrown if not serializable or <see cref="SafeForBlindCopyAttribute"/>.</exception>
        /// <exception cref="Exception">Thrown if constructor not found during a <see cref="SafeForBlindCopyAttribute"/> action.</exception>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
        public static T CreateCopy<T>(this T source, bool guaranteeExact)
        {
            //If object is null then return default
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            //If the object is serializable this is the best guarantee that it will be complete
            if (typeof(T).IsSerializable)
            {
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new MemoryStream();
                using (stream)
                {
                    formatter.Serialize(stream, source);
                    stream.Seek(0, SeekOrigin.Begin);
                    object clone = formatter.Deserialize(stream);
                    return (T)clone;
                }
            }
            else if (!guaranteeExact && source is ICloneable)
            {
                //If the object is ICloneable then it might be a shallow or deep copy but it's still better than nothing
                return (T)(source as ICloneable).Clone();
            }

            //See if it is safe for a blind copy
            SafeForBlindCopyAttribute attr = source.GetAttribute<SafeForBlindCopyAttribute>(false);
            if (attr == null)
            {
                throw new NotSupportedException("Could not copy object. It isn't serializable, cloneable or safe for blind copy."); ;
            }

            //A blind copy is simply creating an instance of the object (with the same passed constructor arguments if 
            //provided) and then going through each property and setting the new object's property values to the old one.
            object copy = null;

            //If object provides constructor arguments then use a non-default constructor
            if (source is IConstructorArgsProvider)
            {
                IConstructorArgsProvider provider = source as IConstructorArgsProvider;
                Type[] consTypes = Type.GetTypeArray(provider.ConstructionArgs);
                ConstructorInfo cons = provider.GetType().GetConstructor(consTypes);
                if (cons == null)
                    throw new Exception("Could not copy object. Constructor with custom arguments not found.");
                copy = cons.Invoke(provider.ConstructionArgs);
            }
            else
            {
                ConstructorInfo cons = source.GetType().GetConstructor(Type.EmptyTypes);
                if (cons == null)
                    throw new Exception("Could not copy object. Default constructor not found.");
                copy = cons.Invoke(null);
            }

            //Get each public property and set the copy's values.
            PropertyInfo[] props = copy.GetType().GetProperties();
            foreach (PropertyInfo prop in props)
            {
                if (!prop.CanRead || !prop.CanWrite) continue;
                ParameterInfo[] paras = prop.GetIndexParameters();
                if (paras.Length == 0) continue;

                prop.SetValue(copy, prop.GetValue(source, null), null);
            }

            return (T)copy;
        }
    }
}
