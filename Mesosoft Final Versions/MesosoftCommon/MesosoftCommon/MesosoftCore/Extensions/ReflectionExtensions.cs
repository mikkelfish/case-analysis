﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCore.Development;

namespace MesosoftCore.Extensions
{
    /// <summary>
    /// Extension methods that primarily have to do with reflection
    /// </summary>
    public static class ReflectionExtensions
    {
        /// <summary>
        /// Generic function to get a class attribute. If found, it will return only the first attribute found.
        /// </summary>
        /// <typeparam name="TAttribute">The type of attribute to search for.</typeparam>
        /// <param name="type">The type to search.</param>
        /// <param name="inherit">Whether to search the inheritance chain.</param>
        /// <returns>Null if attribute not found, or first attribute.</returns>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")] 
        public static TAttribute GetAttribute<TAttribute>(this Type type, bool inherit) where TAttribute : Attribute
        {
            return (TAttribute)GetAttribute(type, typeof(TAttribute), inherit);
        }

        /// <summary>
        /// Generic function to get a class attribute. If found, it will return only the first attribute found.
        /// </summary>
        /// <typeparam name="TAttribute">The type of attribute to search for.</typeparam>
        /// <param name="obj">An object to search.</param>
        /// <param name="inherit">Whether to search the inheritance chain.</param>
        /// <returns>Null if attribute not found, or first attribute.</returns>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")] 
        public static TAttribute GetAttribute<TAttribute>(this object obj, bool inherit) where TAttribute : Attribute
        {
            return (TAttribute)GetAttribute(obj.GetType(), typeof(TAttribute), inherit);
        }

        /// <summary>
        /// Generic function to get a class attribute. If found, it will return only the first attribute found.
        /// </summary>
        /// <param name="type">The type to search.</param>
        /// <param name="attributeType">The type of attribute to look for.</param>
        /// <param name="inherit">Whether to search the inheritance chain.</param>
        /// <returns>Null if attribute not found, or first attribute.</returns>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]       
        public static object GetAttribute(this Type type, Type attributeType, bool inherit)
        {
            object[] attrs = type.GetCustomAttributes(attributeType, inherit);
            if (attrs == null || attrs.Length == 0) return null;
            return attrs[0];
        }

        /// <summary>
        /// Generic function to get a class attribute. If found, it will return only the first attribute found.
        /// </summary>
        /// <param name="obj">An object to search.</param>
        /// <param name="attributeType">The type of attribute to look for.</param>
        /// <param name="inherit">Whether to search the inheritance chain.</param>
        /// <returns>Null if attribute not found, or first attribute.</returns>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")] 
        public static object GetAttribute(this object obj, Type attributeType, bool inherit)
        {
            return GetAttribute(obj.GetType(), attributeType, inherit);
        }

    }
}
