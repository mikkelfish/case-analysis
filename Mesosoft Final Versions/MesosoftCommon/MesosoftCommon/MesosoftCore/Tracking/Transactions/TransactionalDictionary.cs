//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using System.Diagnostics;
using MesosoftCore.Development;

namespace MesosoftCore.Tracking.Transactions
{
    /// <summary>
    /// Transactional array. See <see cref="Transactional{T}"/> for complete information.
    /// </summary>
    [Created("Mikkel", DocumentedStage = DocumentedStage.PublicForReview | DocumentedStage.InternalForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
   public class TransactionalDictionary<K,T> : TransactionalCollection<Dictionary<K,T>,KeyValuePair<K,T>>,IDictionary<K,T>
   {
       /// <summary>
       /// 
       /// </summary>
      public TransactionalDictionary() : this(0)
      {}

      /// <summary>
      /// 
      /// </summary>
      public TransactionalDictionary(IDictionary<K,T> dictionary) : base(new Dictionary<K,T>(dictionary))
      {}

      /// <summary>
      /// 
      /// </summary>
      public TransactionalDictionary(int capacity) : base(new Dictionary<K,T>(capacity))
      {}

      /// <summary>
      /// 
      /// </summary>
      public int Count
      {
         get
         {
            return Value.Count;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public bool ContainsKey(K key)
      {
         return Value.ContainsKey(key);
      }

      /// <summary>
      /// 
      /// </summary>
      public ICollection<K> Keys
      {
         get
         {
            return Value.Keys;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public ICollection<T> Values
      {
         get
         {
            return Value.Values;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public void Clear()
      {
         Value.Clear();
      }
      void ICollection<KeyValuePair<K,T>>.Add(KeyValuePair<K,T> item)
      {
         (Value as ICollection<KeyValuePair<K,T>>).Add(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public T this[K key]
      {
         get
         {
            return Value[key];
         }
         set
         {
            Value[key] = value;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public void Add(K key,T item)
      {
         Value.Add(key,item);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool ContainsValue(T item)
      {
         return Value.ContainsValue(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool TryGetValue(K key,out T value)
      {
         return Value.TryGetValue(key,out value);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool Remove(K key)
      {
         return Value.Remove(key);
      }
      bool ICollection<KeyValuePair<K,T>>.Contains(KeyValuePair<K,T> item)
      {
         return (Value as ICollection<KeyValuePair<K,T>>).Contains(item);
      }
      void ICollection<KeyValuePair<K,T>>.CopyTo(KeyValuePair<K,T>[] array,int arrayIndex)
      {
         (Value as ICollection<KeyValuePair<K,T>>).CopyTo(array,arrayIndex);
      }
      bool ICollection<KeyValuePair<K,T>>.Remove(KeyValuePair<K,T> item)
      {
         return (Value as ICollection<KeyValuePair<K,T>>).Remove(item);
      }
      bool ICollection<KeyValuePair<K,T>>.IsReadOnly
      {
         get
         {
            return (Value as ICollection<KeyValuePair<K,T>>).IsReadOnly;
         }
      }
      Dictionary<K,T>.Enumerator GetEnumerator()
      {
         return Value.GetEnumerator();
      }

      /// <summary>
      /// 
      /// </summary>
      public IEqualityComparer<K> Comparer
      {
         get
         {
            return Value.Comparer;
         }
      }
   }
}

