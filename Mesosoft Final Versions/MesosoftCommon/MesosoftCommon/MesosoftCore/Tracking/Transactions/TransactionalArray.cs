//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using System.Diagnostics;
using MesosoftCore.Development;

namespace MesosoftCore.Tracking.Transactions
{
    /// <summary>
    /// Transactional array. See <see cref="Transactional{T}"/> for complete information.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Created("Mikkel", DocumentedStage = DocumentedStage.PublicForReview | DocumentedStage.InternalForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
   public class TransactionalArray<T> : TransactionalCollection<T[],T>,ICloneable,IList
   {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
      public TransactionalArray(T[] array) : base(array)
      {}

      /// <summary>
      /// 
      /// </summary>
      public TransactionalArray(IEnumerable<T> collection) : base(ToArray(collection))
      {}
      static T[] ToArray(IEnumerable<T> collection)
      {
         int length = 0;
         foreach(T t in collection)
         {
            length++;
         }
         T[] array = new T[length];
         int index = 0;
         foreach(T t in collection)
         {
            array[index] = t;
            index++;
         }
         return array;
      }

      /// <summary>
      /// 
      /// </summary>
      public TransactionalArray(int size) : base(new T[size])
      {}

      /// <summary>
      /// 
      /// </summary>
      public T this[int index]
      {
         get
         {
            return Value[index];
         }
         set
         {
            Value[index] = value;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public int Length
      {
         get
         {
            return Value.Length;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public void CopyTo(TransactionalArray<T> array,int index)
      {
         T[] values = null;
         Value.CopyTo(values,index);
         array = new TransactionalArray<T>(values);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool IsFixedSize
      {
         get
         {
            return Value.IsFixedSize;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public bool IsReadOnly
      {
         get
         {
            return Value.IsReadOnly;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public object Clone()
      {
         return Value.Clone();
      }
      int IList.Add(object value)
      {
         return (Value as IList).Add(value);
      }
      void IList.Clear()
      {
         (Value as IList).Clear();
      }
      bool IList.Contains(object value)
      {
         return (Value as IList).Contains(value);
      }
      int IList.IndexOf(object value)
      {
         return (Value as IList).IndexOf(value);
      }
      void IList.Insert(int index,object value)
      {
         (Value as IList).Insert(index,value);
      }
      void IList.Remove(object value)
      {
         (Value as IList).Remove(value);
      }
      void IList.RemoveAt(int index)
      {
         (Value as IList).RemoveAt(index);
      }
      object IList.this[int index]
      {
         get
         {
            return (Value as IList)[index];
         }
         set
         {
            (Value as IList)[index] = value;
         }
      }
      int ICollection.Count
      {
         get
         {
            return (Value as IList).Count;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public void CopyTo(Array array,int index)
      {
         Value.CopyTo(array,index);
      }

      /// <summary>
      /// 
      /// </summary>
      public object SyncRoot
      {
         get
         {
            return (Value as IList).SyncRoot;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public bool IsSynchronized
      {
         get
         {
            return (Value as IList).IsSynchronized;
         }
      }
   }
}

