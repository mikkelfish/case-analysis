//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using System.Diagnostics;
using MesosoftCore.Development;

namespace MesosoftCore.Tracking.Transactions
{
    /// <summary>
    /// Transactional stack. See <see cref="Transactional{T}"/> for more information.
    /// </summary>
    /// <typeparam name="T">Type in stack</typeparam>
   [Created("Mikkel", DocumentedStage=DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage=DevelopmentStage.ForReview, Version="0.0" )]
    public class TransactionalStack<T> : TransactionalCollection<Stack<T>,T>,ICollection
   {
       /// <summary>
       /// 
       /// </summary>
      public TransactionalStack() : this(0)
      {}

      /// <summary>
      /// 
      /// </summary>
      public TransactionalStack(IEnumerable<T> collection) : base(new Stack<T>(collection))
      {}

      /// <summary>
      /// 
      /// </summary>
      public TransactionalStack(int capacity) : base(new Stack<T>(capacity))
      {}

      /// <summary>
      /// 
      /// </summary>
      public void Push(T item)
      {
         Value.Push(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public T Pop()
      {
         return Value.Pop();
      }

      /// <summary>
      /// 
      /// </summary>
      public void Clear()
      {
         Value.Clear();
      }

      /// <summary>
      /// 
      /// </summary>
      public bool Contains(T item)
      {
        return Value.Contains(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public int Count
      {
         get
         {
            return Value.Count;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public T Peek()
      {
         return Value.Peek();
      }

      /// <summary>
      /// 
      /// </summary>
      public T[] ToArray()
      {
         return Value.ToArray();
      }

      /// <summary>
      /// 
      /// </summary>
      public void TrimExcess()
      {
         Value.TrimExcess();
      }

      void ICollection.CopyTo(Array array,int arrayIndex)
      {
         (Value as ICollection).CopyTo(array,arrayIndex);
      }
      void CopyTo(T[] array,int arrayIndex)
      {
         Value.CopyTo(array,arrayIndex);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool IsSynchronized
      {
         get
         {
            return false;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public object SyncRoot
      {
         get
         {
            return this;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public Stack<T>.Enumerator GetEnumerator()
      {
         return Value.GetEnumerator();
      }

   }
}

