//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using System.Diagnostics;
using MesosoftCore.Development;


namespace MesosoftCore.Tracking.Transactions
{
    /// <summary>
    /// Transactional array. See <see cref="Transactional{T}"/> for complete information.
    /// </summary>
    [Created("Mikkel", DocumentedStage = DocumentedStage.PublicForReview | DocumentedStage.InternalForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
   public class TransactionalSortedList<K,T> : TransactionalCollection<SortedList<K,T>,KeyValuePair<K,T>>,IDictionary<K,T>,IDictionary
   {
       /// <summary>
       /// 
       /// </summary>
      public TransactionalSortedList(IDictionary<K,T> dictionary) : base(new SortedList<K,T>(dictionary))
      {}
      /// <summary>
      /// 
      /// </summary>
      public TransactionalSortedList(IDictionary<K,T> dictionary, IComparer<K> comparer) : base(new SortedList<K,T>(dictionary,comparer))
      {}
      /// <summary>
      /// 
      /// </summary>
      public TransactionalSortedList(IComparer<K> comparer) : base(new SortedList<K,T>(comparer))
      {}
      /// <summary>
      /// 
      /// </summary>
      public TransactionalSortedList(int capacity) : base(new SortedList<K,T>(capacity))
      {}
      /// <summary>
      /// 
      /// </summary>
      public TransactionalSortedList(int capacity,IComparer<K> comparer) : base(new SortedList<K,T>(capacity,comparer))
      {}
      /// <summary>
      /// 
      /// </summary>
      public int IndexOfKey(K key)
      {
         return Value.IndexOfKey(key);
      }
      /// <summary>
      /// 
      /// </summary>
      public int IndexOfValue(T value)
      {
         return Value.IndexOfValue(value);
      }
      /// <summary>
      /// 
      /// </summary>
      public void RemoveAt(int index)
      {
         Value.RemoveAt(index);
      }
      /// <summary>
      /// 
      /// </summary>
      public void TrimExcess()
      {
         Value.TrimExcess();
      }
      /// <summary>
      /// 
      /// </summary>
      public bool TryGetValue(K key,out T value)
      {
         return Value.TryGetValue(key,out value);
      }
      /// <summary>
      /// 
      /// </summary>
      public int Capacity
      {
         get
         {
            return Value.Capacity;
         }
         set
         {
            Value.Capacity = value;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public int Count
      {
         get
         {
            return Value.Count;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public IComparer<K> Comparer
      {
         get
         {
            return Value.Comparer;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public bool ContainsKey(K key)
      {
         return Value.ContainsKey(key);
      }
      ICollection<K> IDictionary<K,T>.Keys
      {
         get
         {
            return (Value as IDictionary<K,T>).Keys;
         }
      }
      ICollection<T> IDictionary<K,T>.Values
      {
         get
         {
            return (Value as IDictionary<K,T>).Values;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public IList<K> Keys
      {
         get
         {
            return Value.Keys;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public IList<T> Values
      {
         get
         {
            return Value.Values;
         }
      }
      object ICollection.SyncRoot
      {
         get
         {
            return (Value as ICollection).SyncRoot;
         }
      }
      bool ICollection.IsSynchronized
      {
         get
         {
            return (Value as ICollection).IsSynchronized;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public void Clear()
      {
         Value.Clear();
      }
      void ICollection<KeyValuePair<K,T>>.Add(KeyValuePair<K,T> item)
      {
         (Value as ICollection<KeyValuePair<K,T>>).Add(item);
      }
      /// <summary>
      /// 
      /// </summary>
      public T this[K key]
      {
         get
         {
            return Value[key];
         }
         set
         {
            Value[key] = value;
         }
      }
      /// <summary>
      /// 
      /// </summary>
      public void Add(K key,T item)
      {
         Value.Add(key,item);
      }
      /// <summary>
      /// 
      /// </summary>
      public bool ContainsValue(T item)
      {
         return Value.ContainsValue(item);
      }
      /// <summary>
      /// 
      /// </summary>
      public bool Remove(K key)
      {
         return Value.Remove(key);
      }
      void IDictionary.Remove(object key)
      {
         (Value as IDictionary<K,T>).Remove((K)key);
      }
      bool ICollection<KeyValuePair<K,T>>.Contains(KeyValuePair<K,T> item)
      {
         return (Value as ICollection<KeyValuePair<K,T>>).Contains(item);
      }
      void ICollection<KeyValuePair<K,T>>.CopyTo(KeyValuePair<K,T>[] array,int arrayIndex)
      {
         (Value as ICollection<KeyValuePair<K,T>>).CopyTo(array,arrayIndex);
      }
      void ICollection.CopyTo(Array array,int arrayIndex)
      {
         (Value as ICollection).CopyTo(array,arrayIndex);
      }
      bool ICollection<KeyValuePair<K,T>>.Remove(KeyValuePair<K,T> item)
      {
         return (Value as ICollection<KeyValuePair<K,T>>).Remove(item);
      }
      bool ICollection<KeyValuePair<K,T>>.IsReadOnly
      {
         get
         {
            return (Value as ICollection<KeyValuePair<K,T>>).IsReadOnly;
         }
      }
      bool IDictionary.IsReadOnly
      {
         get
         {
            return (Value as IDictionary).IsReadOnly;
         }
      }
      void IDictionary.Add(object key,object value)
      {
         (Value as IDictionary<K,T>).Add((K)key,(T)value);
      }
      bool IDictionary.Contains(object key)
      {
         return (Value as IDictionary<K,T>).ContainsKey((K)key);
      }

      bool IDictionary.IsFixedSize
      {
         get
         {
            return (Value as IDictionary).IsFixedSize;
         }
      }
      object IDictionary.this[object key]
      {
         get
         {
            return (Value as IDictionary)[(K)key];
         }
         set
         {
            (Value as SortedDictionary<K,T>)[(K)key] = (T)value;
         }
      }
      ICollection IDictionary.Keys
      {
         get
         {
            return (Value as IDictionary).Keys;
         }
      }
      ICollection IDictionary.Values
      {
         get
         {
            return (Value as IDictionary).Values;
         }
      }
      IDictionaryEnumerator IDictionary.GetEnumerator()
      {
         return (Value as IDictionary).GetEnumerator();
      }
   }
}

