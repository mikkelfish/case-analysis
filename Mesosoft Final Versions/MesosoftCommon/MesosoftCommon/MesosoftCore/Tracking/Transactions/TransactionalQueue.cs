//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using System.Diagnostics;
using MesosoftCore.Development;



namespace MesosoftCore.Tracking.Transactions
{
    /// <summary>
    /// Transactional array. See <see cref="Transactional{T}"/> for complete information.
    /// </summary>
    [Created("Mikkel", DocumentedStage = DocumentedStage.PublicForReview | DocumentedStage.InternalForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
   public class TransactionalQueue<T> : TransactionalCollection<Queue<T>,T>,ICollection
   {
       /// <summary>
       /// 
       /// </summary>
      public TransactionalQueue() : this(0)
      {}

      /// <summary>
      /// 
      /// </summary>
      public TransactionalQueue(IEnumerable<T> collection) : base(new Queue<T>(collection))
      {}

      /// <summary>
      /// 
      /// </summary>
      public TransactionalQueue(int capacity) : base(new Queue<T>(capacity))
      {}

      /// <summary>
      /// 
      /// </summary>
      public void Enqueue(T item)
      {
         Value.Enqueue(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public T Dequeue()
      {
         return Value.Dequeue();
      }

      /// <summary>
      /// 
      /// </summary>
      public void Clear()
      {
         Value.Clear();
      }

      /// <summary>
      /// 
      /// </summary>
      public bool Contains(T item)
      {
        return Value.Contains(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public int Count
      {
         get
         {
            return Value.Count;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public T Peek()
      {
         return Value.Peek();
      }

      /// <summary>
      /// 
      /// </summary>
      public T[] ToArray()
      {
         return Value.ToArray();
      }
      void ICollection.CopyTo(Array array,int arrayIndex)
      {
         (Value as ICollection).CopyTo(array,arrayIndex);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool IsSynchronized
      {
         get
         {
            return false;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public object SyncRoot
      {
         get
         {
            return this;
         }
      }
   }
}

