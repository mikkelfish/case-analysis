//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using System.Diagnostics;
using MesosoftCore.Development;

namespace MesosoftCore.Tracking.Transactions
{
    /// <summary>
    /// Abstract base class for all collections that are part of Transactional{T}. See <see cref="Transactional{T}"/> for complete information.
    /// </summary>
    /// <typeparam name="C">The type of the class.</typeparam>
    /// <typeparam name="T">The type that the iteration will occur on.</typeparam>
    [Created("Mikkel", DocumentedStage = DocumentedStage.PublicForReview | DocumentedStage.InternalForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
    public abstract class TransactionalCollection<C,T> : Transactional<C>, IEnumerable<T> where C : IEnumerable<T>
   {
       /// <summary>
       /// 
       /// </summary>
      public TransactionalCollection(C collection)
      {
         Value = collection;
      }

      IEnumerator<T> IEnumerable<T>.GetEnumerator()
      {
         return Value.GetEnumerator();
      }
      IEnumerator IEnumerable.GetEnumerator()
      {
         IEnumerable<T> enumerable = this;
         return enumerable.GetEnumerator();
      }
   }
}