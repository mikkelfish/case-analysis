//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using System.Diagnostics;
using System.Collections.ObjectModel;
using MesosoftCore.Development;

namespace MesosoftCore.Tracking.Transactions
{
    /// <summary>
    /// Transactional array. See <see cref="Transactional{T}"/> for complete information.
    /// </summary>
    [Created("Mikkel", DocumentedStage = DocumentedStage.PublicForReview | DocumentedStage.InternalForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
   public class TransactionalList<T> : TransactionalCollection<List<T>,T>,IList<T>
   {
       /// <summary>
       /// 
       /// </summary>
      public TransactionalList() : this(0)
      {}
      public TransactionalList(IEnumerable<T> collection) : base(new List<T>(collection))
      {
      }

      /// <summary>
      /// 
      /// </summary>
      public TransactionalList(int capacity) : base(new List<T>(capacity))
      {}

      /// <summary>
      /// 
      /// </summary>
      public int Capacity
      {
         get
         {
            return Value.Capacity;
         }
         set
         {
            Value.Capacity = value;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public int Count
      {
         get
         {
            return Value.Count;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public int IndexOf(T item)
      {
         return Value.IndexOf(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public T this[int index]
      {
         get
         {
            return Value[index];
         }
         set
         {
            Value[index] = value;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public void Add(T item)
      {
         Value.Add(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public void AddRange(IEnumerable<T> collection)
      {
         Value.AddRange(collection);
      }

      /// <summary>
      /// 
      /// </summary>
      public void Clear()
      {
         Value.Clear();
      }

      /// <summary>
      /// 
      /// </summary>
      public T[] ToArray()
      {
         return Value.ToArray();
      }

      /// <summary>
      /// 
      /// </summary>
      public bool Contains(T item)
      {
         return Value.Contains(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public void Insert(int index,T item)
      {
         Value.Insert(index,item);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool Remove(T item)
      {
         return Value.Remove(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public int RemoveAll(Predicate<T> match)
      {
         return Value.RemoveAll(match);
      }

      /// <summary>
      /// 
      /// </summary>
      public void RemoveAt(int index)
      {
         Value.RemoveAt(index);
      }

      /// <summary>
      /// 
      /// </summary>
      public void Reverse()
      {
         Value.Reverse();
      }

      /// <summary>
      /// 
      /// </summary>
      public void CopyTo(T[] array,int arrayIndex)
      {
         Value.CopyTo(array,arrayIndex);
      }
      bool ICollection<T>.IsReadOnly
      {
         get
         {
            return (Value as ICollection<T>).IsReadOnly;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public ReadOnlyCollection<T> AsReadOnly()
      {
         return Value.AsReadOnly();
      }

      /// <summary>
      /// 
      /// </summary>
      public int BinarySearch(T item)
      {
         return Value.BinarySearch(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public int BinarySearch(T item, IComparer<T> comparer)
      {
         return Value.BinarySearch(item,comparer);
      }

      /// <summary>
      /// 
      /// </summary>
      public int BinarySearch(int index, int count, T item, IComparer<T> comparer)
      {
         return Value.BinarySearch(index,count,item,comparer);
      }

      /// <summary>
      /// 
      /// </summary>
      public List<U> ConvertAll<U>(Converter<T,U> converter)
      {
         return Value.ConvertAll(converter);
      }

      /// <summary>
      /// 
      /// </summary>
      public void CopyTo(int index, T[] array, int arrayIndex, int count)
      {
         Value.CopyTo(index,array,arrayIndex,count);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool Exists(Predicate<T> match)
      {
         return Value.Exists(match);
      }

      /// <summary>
      /// 
      /// </summary>
      public T Find(Predicate<T> match)
      {
         return Value.Find(match);
      }

      /// <summary>
      /// 
      /// </summary>
      public List<T> FindAll(Predicate<T> match)
      {
         return Value.FindAll(match);
      }

      /// <summary>
      /// 
      /// </summary>
      public int FindIndex(Predicate<T> match)
      {
         return Value.FindIndex(match);
      }

      /// <summary>
      /// 
      /// </summary>
      public int FindIndex(int startIndex, Predicate<T> match)
      {
         return Value.FindIndex(startIndex,match);
      }

      /// <summary>
      /// 
      /// </summary>
      public int FindIndex(int startIndex, int count, Predicate<T> match)
      {
         return Value.FindIndex(startIndex,count,match);
      }

      /// <summary>
      /// 
      /// </summary>
      public T FindLast(Predicate<T> match)
      {
         return Value.FindLast(match);
      }

      /// <summary>
      /// 
      /// </summary>
      public int FindLastIndex(Predicate<T> match)
      {
         return Value.FindLastIndex(match);
      }

      /// <summary>
      /// 
      /// </summary>
      public int FindLastIndex(int startIndex, Predicate<T> match)
      {
         return Value.FindLastIndex(startIndex,match);
      }

      /// <summary>
      /// 
      /// </summary>
      public int FindLastIndex(int startIndex, int count, Predicate<T> match)
      {
         return Value.FindLastIndex(startIndex,count,match);
      }

      /// <summary>
      /// 
      /// </summary>
      public void ForEach(Action<T> action)
      {
         Value.ForEach(action);
      }

      /// <summary>
      /// 
      /// </summary>
      public List<T>.Enumerator GetEnumerator()
      {
         return Value.GetEnumerator();
      }

      /// <summary>
      /// 
      /// </summary>
      public List<T> GetRange(int index, int count)
      {
         return Value.GetRange(index,count);
      }

      /// <summary>
      /// 
      /// </summary>
      public int IndexOf(T item, int index)
      {
         return Value.IndexOf(item,index);
      }

      /// <summary>
      /// 
      /// </summary>
      public int IndexOf(T item, int index, int count)
      {
         return Value.IndexOf(item,index,count);
      }

      /// <summary>
      /// 
      /// </summary>
      public void InsertRange(int index,IEnumerable<T> collection)
      {
         Value.InsertRange(index,collection);
      }

      /// <summary>
      /// 
      /// </summary>
      public int LastIndexOf(T item)
      {
         return Value.LastIndexOf(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public int LastIndexOf(T item, int index)
      {
         return Value.LastIndexOf(item,index);
      }

      /// <summary>
      /// 
      /// </summary>
      public int LastIndexOf(T item, int index, int count)
      {
         return Value.LastIndexOf(item,index,count);
      }

      /// <summary>
      /// 
      /// </summary>
      public void RemoveRange(int index, int count)
      {
         Value.RemoveRange(index,count);
      }

      /// <summary>
      /// 
      /// </summary>
      public void Reverse(int index, int count)
      {
         Value.Reverse(index,count);
      }

      /// <summary>
      /// 
      /// </summary>
      public void Sort()
      {
         Value.Sort();
      }

      /// <summary>
      /// 
      /// </summary>
      public void Sort(IComparer<T> comparer)
      {
         Value.Sort(comparer);
      }

      /// <summary>
      /// 
      /// </summary>
      public void Sort(Comparison<T> comparison)
      {
         Value.Sort(comparison);
      }

      /// <summary>
      /// 
      /// </summary>
      public void Sort(int index, int count, IComparer<T> comparer)
      {
         Value.Sort(index,count,comparer);
      }

      /// <summary>
      /// 
      /// </summary>
      public void TrimExcess()
      {
         Value.TrimExcess();
      }

      /// <summary>
      /// 
      /// </summary>
      public bool TrueForAll(Predicate<T> match)
      {
         return Value.TrueForAll(match);
      }
   }
}
