//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using System.Diagnostics;
using MesosoftCore.Development;

namespace MesosoftCore.Tracking.Transactions
{
    /// <summary>
    /// Transactional array. See <see cref="Transactional{T}"/> for complete information.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Created("Mikkel", DocumentedStage = DocumentedStage.PublicForReview | DocumentedStage.InternalForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
   public class TransactionalLinkedList<T> : TransactionalCollection<LinkedList<T>,T>,ICollection<T>,IEnumerable<T>,ICollection
   {
       /// <summary>
       /// 
       /// </summary>
      public TransactionalLinkedList(IEnumerable<T> collection) : base(new LinkedList<T>(collection))
      {}

      /// <summary>
      /// 
      /// </summary>
      public int Count
      {
         get
         {
            return Value.Count;
         }
      }
      void ICollection<T>.Add(T item)
      {
         (Value as ICollection<T>).Add(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public void Clear()
      {
         Value.Clear();
      }

      /// <summary>
      /// 
      /// </summary>
      public bool Contains(T item)
      {
         return Value.Contains(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool Remove(T item)
      {
         return Value.Remove(item);
      }

      /// <summary>
      /// 
      /// </summary>
      public void CopyTo(T[] array,int arrayIndex)
      {
         Value.CopyTo(array,arrayIndex);
      }
      bool ICollection<T>.IsReadOnly
      {
         get
         {
            return (Value as ICollection<T>).IsReadOnly;
         }
      }
      void ICollection.CopyTo(Array array,int arrayIndex)
      {
         (Value as ICollection).CopyTo(array,arrayIndex);
      }

      /// <summary>
      /// 
      /// </summary>
      public bool IsSynchronized
      {
         get
         {
            return false;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public object SyncRoot
      {
         get
         {
            return this;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public LinkedListNode<T> AddAfter(LinkedListNode<T> node,T value)
      {
         return Value.AddAfter(node,value);
      }

      /// <summary>
      /// 
      /// </summary>
      public LinkedListNode<T> AddBefore(LinkedListNode<T> node,T value)
      {
         return Value.AddBefore(node,value);
      }

      /// <summary>
      /// 
      /// </summary>
      public void AddFirst(LinkedListNode<T> node)
      {
         Value.AddFirst(node);
      }

      /// <summary>
      /// 
      /// </summary>
      public LinkedListNode<T> AddFirst(T value)
      {
         return Value.AddFirst(value);
      }

      /// <summary>
      /// 
      /// </summary>
      public LinkedListNode<T> AddLast(T value)
      {
         return Value.AddLast(value);
      }

      /// <summary>
      /// 
      /// </summary>
      public void AddLast(LinkedListNode<T> node)
      {
         Value.AddLast(node);
      }
      /// <summary>
      /// 
      /// </summary>
      public LinkedListNode<T> Find(T value)
      {
         return Value.Find(value);
      }

      /// <summary>
      /// 
      /// </summary>
      public LinkedListNode<T> FindLast(T value)
      {
         return Value.FindLast(value);
      }

      /// <summary>
      /// 
      /// </summary>
      public void Remove(LinkedListNode<T> node)
      {
         Value.Remove(node);
      }

      /// <summary>
      /// 
      /// </summary>
      public void RemoveFirst()
      {
         Value.RemoveFirst();
      }

      /// <summary>
      /// 
      /// </summary>
      public void RemoveLast()
      {
         Value.RemoveLast();
      }

      /// <summary>
      /// 
      /// </summary>
      public LinkedListNode<T> First
      {
         get
         {
            return Value.First;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public LinkedListNode<T> Last
      {
         get
         {
            return Value.Last;
         }
      }
   }
}

