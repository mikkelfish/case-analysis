//Questions? Comments? go to 
//http://www.idesign.net

using System;
using System.Transactions;
using System.Diagnostics;
using System.Collections.Generic;
using MesosoftCore.Extensions;
using MesosoftCore.Tracking.Transactions;
using MesosoftCore.Development;

namespace MesosoftCore.Tracking.Transactions
{
    /// <summary>
    /// Generic wrapper object that enables transactions. This can be used to make any (copyable) object obey the ACID properties of transactions.
    /// </summary>
    /// <typeparam name="T">Any copyable type, there is no constraint. However, if the type does not support Serialization or Blind Assignment (or is cloneable if EnforceStrictCopy is false)
    /// then it will throw an exception when it attempts to register.</typeparam>
    ///<remarks >This wrapper class keeps track of the "indeterminate" state during a transaction by making a copy of the value. This means that the value must support serialization,
    ///blind assignment or have a deep copy when using <see cref="ICloneable.Clone"/>. If the object relies on cloning to make a copy, EnforceStrictCopy must be set to false. This is to ensure
    ///that ACID properties hold, because a shallow copy will not represent the full state of the object. 
    ///<para>
    ///This class makes use of transaction locks as implemented in <see cref="TransactionalLock"/>. This means that each object can only participate in one transaction at a time. This is necessary
    ///to make sure there is never an indeterminate state or race condition. See example for how this affects multi-threaded or nested transactions.
    /// </para>
    /// </remarks>
    ///<exception cref="NotSupportedException">Thrown if object is unable to be copied upon enlistment.</exception>
    ///<example>
    ///The following demonstrates how simple classes can participate in transactions.
    ///<code>
    /// <![CDATA[
    /// Transactional<string> testString = "Testing";
    /// using(TransactionScope scope = new TransactionScope())
    /// {
    ///     testString.Value = "this will be rejected";
    /// }
    /// Debug.Assert(testString.Value == "Testing");
    /// 
    /// using(TransactionScope scope2 = new TransactionScope())
    /// {
    ///     testString.Value = "this will be accepted.";
    ///     scope2.Complete();
    /// }
    /// Debug.Assert(testString.Value == "this will be accepted.");
    /// 
    /// //This is very good if there is a lot of user interaction that should be rolled back.
    /// TransactionalList<int> userEntries = new TransactionalList<int>();
    /// userEntries.Add(0);
    /// try
    /// {
    ///     using(TransactionScope scope3 = new TransactionScope())
    ///     {
    ///        //get user input
    ///        //user adds 1, 2 ,3 
    /// 
    ///        Utilities.SaveToDisk("testfile.xaml", userEntries); //Let's say this fails
    ///     }
    /// }
    /// catch(Exception ex)
    /// {
    ///    //Exception is write protection error for instance
    /// }
    /// Debug.Assert(userEntries.Count == 1);
    /// 
    /// ]]> 
    ///</code>
    /// 
    ///The following demonstrates how a transactional object can only participate in one transaction at a time.
    ///<code>
    /// text.Value = "test";
    /// number.Value = 3;
    /// try
    /// {
    ///     using (TransactionScope scope6 = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(2)))
    ///     {
    ///         number.Value = 4;
    ///         text.Value = "First transaction acquires lock here";
    ///
    ///         using (TransactionScope scope7 = new TransactionScope(TransactionScopeOption.RequiresNew))
    ///         {
    ///             text.Value = "Second transaction blocks, first transaction aborts, second commits";
    ///             scope7.Complete();
    ///         }
    ///         scope6.Complete();
    ///     }
    /// }
    /// catch (TransactionAbortedException e)
    /// {
    ///     MessageBox.Show(e.Message, "scope6");
    /// }
    /// Debug.Assert(text.Value == "Second transaction blocks, first transaction aborts, second commits");
    /// Debug.Assert(number.Value == 3);
    /// </code>
    /// What is happening is that text is already participating in a transaction in scope6. When scope7 has the <see cref="TransactionScopeOption.RequiresNew"/> option, that creates a new
    /// transaction that is not related to the old one. Then when text attempts to join that new transaction, it cannot due to the lock. It blocks 2 seconds until scope6 times out, and then proceeds
    /// and is successful.
    /// 
    /// <para>Compare that to the following.</para>
    /// <code>
    /// text.Value = "test";
    /// number.Value = 3;
    /// try
    /// {
    ///     using (TransactionScope scope6 = new TransactionScope())
    ///     {
    ///         number.Value = 4;
    ///         text.Value = "First transaction acquires lock here";
    ///
    ///         //Unlike the previous example, no new TransactionScope is created. So this is OK.
    ///         using (TransactionScope scope7 = new TransactionScope())
    ///         {
    ///             text.Value = "Second transaction blocks, first transaction aborts, second commits";
    ///             scope7.Complete();
    ///         }
    ///         scope6.Complete();
    ///     }
    /// }
    /// catch (TransactionAbortedException e)
    /// {
    ///     MessageBox.Show(e.Message, "scope6");
    /// }
    /// 
    /// //Both values are changed
    /// Debug.Assert(text.Value == "Second transaction blocks, first transaction aborts, second commits");
    /// Debug.Assert(number.Value == 4);
    /// 
    /// //If either fails, both fail
    /// text.Value = "test";
    /// number.Value = 3;
    /// try
    /// {
    ///     using (TransactionScope scope6 = new TransactionScope())
    ///     {
    ///         number.Value = 4;
    ///         text.Value = "First transaction acquires lock here";
    ///
    ///         using (TransactionScope scope7 = new TransactionScope())
    ///         {
    ///             text.Value = "Second transaction blocks, first transaction aborts, second commits";
    ///             scope7.Complete();
    ///         }
    ///         //scope6.Complete(); SET TO FAIL
    ///     }
    /// }
    /// catch (TransactionAbortedException e)
    /// {
    ///     MessageBox.Show(e.Message, "scope6");
    /// }
    /// Debug.Assert(text.Value == "test");
    /// Debug.Assert(number.Value == 3);
    /// </code>
    /// </example>
    [SelfEnlistment]
    [Created("Mikkel", DocumentedStage = DocumentedStage.PublicForReview | DocumentedStage.InternalForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
    public class Transactional<T> : IEnlistmentNotification
    {
        //The real value of the transaction. This value is guaranteed to be the "consistent" value.
        T m_Value;
        
        //The temporary value of the transaction. This value is what is changed when a transaction is in progress.
        T m_TemporaryValue;

        //The current transaction. It is automatically selected by using ambient Transaction.Curent when the value is involved in a get or set.
        Transaction m_CurrentTransaction;

        //A transaction lock used to guarantee that transactions don't interfere with each other.
        TransactionalLock m_Lock;

        /// <summary>
        /// Gets or sets whether the transaction should support types that may not have strict copying. Initially set to true, it must be set to false if the type 
        /// is going to rely on <see cref="ICloneable.Clone"/> by using a deep copy.
        /// </summary>
        public static bool EnforceStrictCopy { get; set; }

        /// <summary>
        /// Public constructor that takes the value to encapsulate.
        /// </summary>
        /// <param name="value">Value to encapsulate</param>
        public Transactional(T value)
        {
            m_Lock = new TransactionalLock();
            m_Value = value;
        }

        /// <summary>
        /// Public copy constructor
        /// </summary>
        /// <param name="transactional">Transactional to copy. This just passes the parameter's value to the new transactional, it does not copy the value.</param>
        public Transactional(Transactional<T> transactional)
            : this(transactional.Value)
        { }

        /// <summary>
        /// Public default constructor.
        /// </summary>
        public Transactional()
            : this(default(T))
        { }

        //Static constructor sets EnforceStrictCopy to true.
        static Transactional()
        {
            EnforceStrictCopy = true;
        }

        /// <summary>
        /// Called when the object is committed. It sets the temporary value to the consistent value.
        /// </summary>
        /// <param name="enlistment"></param>
        void IEnlistmentNotification.Commit(Enlistment enlistment)
        {
            //Dispose the former consistent value if necessary.
            IDisposable disposable = m_Value as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }

            //Set the consistent value and notify completion.
            m_Value = m_TemporaryValue;
            m_CurrentTransaction = null;
            m_TemporaryValue = default(T);
            m_Lock.Unlock();
            enlistment.Done();
        }

        /// <summary>
        /// Because this is volatile, InDoubt doesn't do anything.
        /// </summary>
        /// <param name="enlistment"></param>
        void IEnlistmentNotification.InDoubt(Enlistment enlistment)
        {
            
            m_Lock.Unlock();
            enlistment.Done();
        }

        /// <summary>
        /// There is no preparation to speak of.
        /// </summary>
        /// <param name="preparingEnlistment"></param>
        void IEnlistmentNotification.Prepare(PreparingEnlistment preparingEnlistment)
        {
            preparingEnlistment.Prepared();
        }

        /// <summary>
        /// Rolls back a transaction. This simply disposes of the temporary copy.
        /// </summary>
        /// <param name="enlistment"></param>
        void IEnlistmentNotification.Rollback(Enlistment enlistment)
        {
            m_CurrentTransaction = null;

            IDisposable disposable = m_TemporaryValue as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }

            m_TemporaryValue = default(T);
            m_Lock.Unlock();
            enlistment.Done();
        }

        /// <summary>
        /// Enlists in the current ambient transaction and creates a temporary copy of the value for the indeterminate state if it doesn't support <see cref="IEnlistmentNotification"/>.
        /// If it does support notification it enlists the object with the transaction if necessary (i.e. it doesn't have <see cref="SelfEnlistmentAttribute"/> attrached).
        /// </summary>
        ///<exception cref="NotSupportedException">Thrown if object is unable to be copied. See the class help for more information.</exception>
        protected virtual void Enlist(T t)
        {
            Debug.Assert(m_CurrentTransaction == null);
            m_CurrentTransaction = Transaction.Current;
            Debug.Assert(m_CurrentTransaction.TransactionInformation.Status == TransactionStatus.Active);
            m_CurrentTransaction.EnlistVolatile(this, EnlistmentOptions.None);

            //If object does not manage its own changes, then make a copy of it
            if (!(t is IEnlistmentNotification))
            {
                try
                {
                    m_TemporaryValue = t.CreateCopy<T>(Transactional<T>.EnforceStrictCopy);
                }
                catch (NotSupportedException exception)
                {
                    throw new NotSupportedException("In order to be valid for transactions, the type must support full mode copy. This means it needs to either be Serializable, valid for Blind Assignment, or ICloneable with a deep copy. If relying on ICloneable, Transactional<T>.EnforceStrictCopy needs to be set to false. Refer to documentation for more.",
                        exception);
                }
            }
            else
            {
                //Otherwise, there is no need to make a copy, so just have the temporary object point to the same place.
                m_TemporaryValue = t;

                //See whether the enlistment object manages its own enlisting by using the ambient transaction. If not we'll need to enlist it here
                SelfEnlistmentAttribute attr = t.GetAttribute<SelfEnlistmentAttribute>(false);
                if (attr == null || attr.EnlistsSelf == false)
                {
                    m_CurrentTransaction.EnlistVolatile(t as IEnlistmentNotification, EnlistmentOptions.None);
                }
            }

        }

        //Set the value, making sure it's already enlisted.
        private void SetValue(T t)
        {
            m_Lock.Lock();
            if (m_CurrentTransaction == null)
            {
                if (Transaction.Current == null)
                {
                    m_Value = t;
                    return;
                }
                else
                {
                    Enlist(t);
                    return;
                }
            }
            else
            {
                //Must have acquired the lock
                Debug.Assert(m_CurrentTransaction == Transaction.Current, "Invalid state in the volatile resource state machine");
                m_TemporaryValue = t;
            }
        }
        
        //  It's the value, making sure it's enlisted.
        T GetValue()
        {
            m_Lock.Lock();
            if (m_CurrentTransaction == null)
            {
                if (Transaction.Current == null)
                {
                    return m_Value;
                }
                else
                {
                    Enlist(m_Value);
                }
            }
            //Must have acquired the lock
            Debug.Assert(m_CurrentTransaction == Transaction.Current, "Invalid state in the volatile resource state machine");

            return m_TemporaryValue;
        }

        /// <summary>
        /// Gets or sets the value of the wrapper.
        /// </summary>
        public T Value
        {
            get
            {
                return GetValue();
            }
            set
            {
                SetValue(value);
            }
        }

        /// <summary>
        /// Implicit conversion to the value.
        /// </summary>
        /// <returns>Transaction.Value</returns>
        public static implicit operator T(Transactional<T> transactional)
        {
            return transactional.Value;
        }

        /// <summary>
        /// Equals override. Transactionals are equal if their values are equal.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator ==(Transactional<T> t1, Transactional<T> t2)
        {
            // Is t1 and t2 null (check the value as well).
            bool t1Null = (Object.ReferenceEquals(t1, null) || t1.Value == null);
            bool t2Null = (Object.ReferenceEquals(t2, null) || t2.Value == null);

            // If they are both null, return true.
            if (t1Null && t2Null)
            {
                return true;
            }

            // If one is null, return false.
            if (t1Null || t2Null)
            {
                return false;
            }
            return EqualityComparer<T>.Default.Equals(t1.Value, t2.Value);
        }
        
        /// <summary>
        /// Equals override. Transactional is equal to a value if its value is.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator ==(Transactional<T> t1, T t2)
        {
            // Is t1 and t2 null (check the value as well).
            bool t1Null = (Object.ReferenceEquals(t1, null) || t1.Value == null);
            bool t2Null = t2 == null;

            // If they are both null, return true.
            if (t1Null && t2Null)
            {
                return true;
            }

            // If one is null, return false.
            if (t1Null || t2Null)
            {
                return false;
            }
            return EqualityComparer<T>.Default.Equals(t1.Value, t2);
        }

        /// <summary>
        /// Equals override. Transactional is equal to a value if its value is.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator ==(T t1, Transactional<T> t2)
        {
            // Is t1 and t2 null (check the value as well)
            bool t1Null = t1 == null;
            bool t2Null = (Object.ReferenceEquals(t2, null) || t2.Value == null);

            // If they are both null, return true.
            if (t1Null && t2Null)
            {
                return true;
            }

            // If one is null, return false.
            if (t1Null || t2Null)
            {
                return false;
            }
            return EqualityComparer<T>.Default.Equals(t1, t2.Value);
        }

        /// <summary>
        ///  Not equals override.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator !=(T t1, Transactional<T> t2)
        {
            return !(t1 == t2);
        }

        /// <summary>
        ///  Not equals override.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator !=(Transactional<T> t1, T t2)
        {
            return !(t1 == t2);
        }

        /// <summary>
        ///  Not equals override.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator !=(Transactional<T> t1, Transactional<T> t2)
        {
            return !(t1 == t2);
        }

        /// <summary>
        /// Hash code override.  Returns Value.GetHashCode();
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        /// <summary>
        /// Equals override.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return Value.Equals(obj);
        }
    }
}