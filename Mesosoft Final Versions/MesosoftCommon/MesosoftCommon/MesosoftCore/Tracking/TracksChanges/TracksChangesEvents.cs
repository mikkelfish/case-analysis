﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace MesosoftCore.Tracking
{
    /// <summary>
    /// Type of action requested by the undo/redo tracking.
    /// </summary>
    public enum UndoRedoTrackingType
    {
        /// <summary>
        /// A changed value was undone.
        /// </summary>
        Undo,
        /// <summary>
        /// A changed value was redone.
        /// </summary>
        Redo,
        /// <summary>
        ///   A value is added to the list, this could either be through a redo or undo.
        /// </summary>
        Added,
        /// <summary>
        /// A value is deleted from the list, this could either be to redo or undo.
        /// </summary>
        Deleted
    }

    /// <summary>
    /// Delegate called when a <see cref="MesosoftCore.Tracking.ITracksChanges"/> object requests a undo or redo; this allows the handlers to provide logic about whether the action should be completed.
    /// </summary>
    /// <param name="sender">The tracking object.</param>
    /// <param name="e">The event data.</param>
    public delegate void PreviewUndoRedoTrackingEventHandler(object sender, PreviewUndoRedoTrackingEventArgs e);
    
    /// <summary>
    /// Delegate called when an object has successfully completed an undo or redo.  This allows handlers to be updated.
    /// </summary>
    /// <param name="sender">The tracking object.</param>
    /// <param name="e">The event data.</param>
    public delegate void UndoRedoTrackingEventHandler(object sender, UndoRedoTrackingEventArgs e);
    #region Event Args

    /// <summary>
    /// EventArgs used to describe an undo/redo request.
    /// </summary>
    public class PreviewUndoRedoTrackingEventArgs : EventArgs
    {
        private bool cancel;
        /// <summary>
        /// Gets or sets whether the request should be canceled.
        /// </summary>
        public bool Cancel
        {
            get { return cancel; }
            set { cancel = value; }
        }

        private object target;
        /// <summary>
        /// Gets the object to be modified if the change is a value change, or the object added or deleted if the change is a list change.
        /// </summary>
        public object Target
        {
            get { return target; }
        }

        private object val;
        /// <summary>
        ///     Gets the changed value.
        /// </summary>
        public object Value
        {
            get { return val; }
        }


        private PropertyInfo info;
        /// <summary>
        /// Gets the property info that describes the property value to be changed.
        /// </summary>
        public PropertyInfo Info
        {
            get { return info; }
        }

        private UndoRedoTrackingType trackType;
        /// <summary>
        ///     Gets the tracking type, this is used to describe whether the request is an undo or redo.
        /// </summary>
        public UndoRedoTrackingType TrackingType
        {
            get { return trackType; }
        }

        /// <summary>
        ///   Public constructor used to describe a redo/undo request.
        /// </summary>
        /// <param name="target">The target object.</param>
        /// <param name="value">The changed value.</param>
        /// <param name="property">The name of the property to be changed.</param>
        /// <param name="track">Tracking information.</param>
        public PreviewUndoRedoTrackingEventArgs(object target, object value, string property, UndoRedoTrackingType track)
        {
            this.target = target;
            this.val = value;
            this.info = this.target.GetType().GetProperty(property);
            this.trackType = track;
        }

        /// <summary>
        ///   Public constructor used to describe a redo/undo request.
        /// </summary>
        /// <param name="target">The target object.</param>
        /// <param name="value">The changed value.</param>
        /// <param name="property"><see cref="PropertyInfo"/> that describes the property to be changed.</param>
        /// <param name="track">Tracking information.</param>
        public PreviewUndoRedoTrackingEventArgs(object target, object value, PropertyInfo property, UndoRedoTrackingType track)
        {
            this.target = target;
            this.val = value;
            this.info = property;
            this.trackType = track;
        }

    }

    /// <summary>
    /// EventArgs used to describe a successful undo/redo operation.
    /// </summary>
    public class UndoRedoTrackingEventArgs : EventArgs
    {
        private object target;
        /// <summary>
        /// Gets the object to be modified if the change is a value change, or the object added or deleted if the change is a list change.
        /// </summary>
        public object Target
        {
            get { return target; }
        }

        private object val;
        /// <summary>
        ///     Gets the changed value.
        /// </summary>
        public object Value
        {
            get { return val; }
        }


        private PropertyInfo info;
        /// <summary>
        /// Gets the property info that describes the property value to be changed.
        /// </summary>
        public PropertyInfo Info
        {
            get { return info; }
        }

        private UndoRedoTrackingType trackType;
        /// <summary>
        ///     Gets the tracking type, this is used to describe whether the request is an undo or redo.
        /// </summary>
        public UndoRedoTrackingType TrackingType
        {
            get { return trackType; }
        }

        /// <summary>
        ///   Public constructor used to describe a redo/undo operation.
        /// </summary>
        /// <param name="target">The target object.</param>
        /// <param name="value">The changed value.</param>
        /// <param name="property">The name of the property to be changed.</param>
        /// <param name="track">Tracking information.</param>
        public UndoRedoTrackingEventArgs(object target, object value, string property, UndoRedoTrackingType track)
        {
            this.target = target;
            this.val = value;
            this.info = this.target.GetType().GetProperty(property);
            this.trackType = track;
        }

        /// <summary>
        ///   Public constructor used to describe a redo/undo operation.
        /// </summary>
        /// <param name="target">The target object.</param>
        /// <param name="value">The changed value.</param>
        /// <param name="property"><see cref="PropertyInfo"/> that describes the property to be changed.</param>
        /// <param name="track">Tracking information.</param>
        public UndoRedoTrackingEventArgs(object target, object value, PropertyInfo property, UndoRedoTrackingType track)
        {
            this.target = target;
            this.val = value;
            this.info = property;
            this.trackType = track;
        }

    }

    #endregion

}
