﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCore.Tracking
{
    /// <summary>
    /// Supports undo and redo capabilities. Allows for handlers to provide logic when the operations are called.
    /// </summary>
    /// <remarks>
    /// The object should implement standard Redo/Undo behavior. To wit, there is a redo stack and an undo stack. Any change can be undone, but manual changes destroy the redo stack.
    /// </remarks>
    /// <example>
    /// The following demonstrates Undo/Redo behavior both on adding/removing items and changing items in a list.
    /// <code>
    /// IntTracksList list = new IntTracksList();
    /// list.Add(0);
    /// list.Add(1);
    /// list.Add(2);
    /// list.Undo();
    /// Console.Writeline(list[list.Count-1]);
    /// list.Redo();
    /// Console.Writeline(list[list.Count-1]);
    /// list.Undo();
    /// list.Add(3);
    /// list.Redo(); //Returns false
    /// Console.Writeline(list[list.Count-1]);
    /// list[0] = 4;
    /// Console.Writeline(list[0]);
    /// list.Undo();
    /// Console.Writeline(list[list.Count-1]);
    /// </code>
    /// Output:
    /// <code>
    /// 1
    /// 2
    /// 3
    /// 4
    /// 0
    /// </code>
    /// </example>
    public interface ITracksChanges
    {
        /// <summary>
        /// Attempt to enter tracking mode.
        /// </summary>
        /// <returns>Was tracking started successfully?</returns>
        bool StartTracking();

        /// <summary>
        /// Attempt to undo the last action.
        /// </summary>
        /// <returns>Was undo successful?</returns>
        bool Undo();

        /// <summary>
        /// Attempt to redo the last undo.
        /// </summary>
        /// <returns>Was redo successful?</returns>
        bool Redo();

        /// <summary>
        /// Stop tracking. This clears any state information as well.
        /// </summary>
        void StopTracking();

        //Gets whether the object is currently tracking changes.
        bool IsTracking { get; }

        /// <summary>
        /// Event fires when there is an undo or redo request, but before the operation is completed. 
        /// This allows handlers to inspect the proposed change and cancel it if needed.
        /// </summary>
        event PreviewUndoRedoTrackingEventHandler PreviewTrackingEvent;
        
        /// <summary>
        /// Fires after an undo or redo operation is completed.
        /// </summary>
        event UndoRedoTrackingEventHandler TrackingEvent;
    }
}
