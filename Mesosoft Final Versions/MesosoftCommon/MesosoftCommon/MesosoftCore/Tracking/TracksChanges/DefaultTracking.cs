﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections;
using System.Reflection;

namespace MesosoftCore.Tracking.TracksChanges
{
    public class DefaultTracking 
    {
        private class trackingObject
        {
            public enum trackingType { Added, Removed, Changed };

            public trackingType Type { get; set; }
            public object Source { get; set; }
            public object Value { get; set; }
            public string PropertyName { get; set; }

            private PropertyInfo propInfo;
            public PropertyInfo PropInfo
            {
                get
                {
                    if (this.propInfo == null && this.PropertyName != null && this.Source != null)
                    {
                        this.propInfo = this.Source.GetType().GetProperty(this.PropertyName);
                    }
                    return propInfo;
                }
            }

            public override bool Equals(object obj)
            {
                if (!(obj is trackingObject)) return false;
                trackingObject other = obj as trackingObject;
                if (other.Source == this.Source && other.Value == this.Value && other.PropertyName == this.PropertyName) return true;
                return false;
            }

            public override int GetHashCode()
            {
                return (this.PropertyName + this.Value + this.Source).GetHashCode();
            }
        }

        private void removeInCollection(trackingObject obj)
        {
            if (obj.Type == trackingObject.trackingType.Changed) throw new Exception("Internal error");
            ICollection collection = obj.Source as ICollection;
            

        }

        private void addInCollection(trackingObject obj)
        {
            if (obj.Type == trackingObject.trackingType.Changed) throw new Exception("Internal error");
            ICollection collection = obj.Source as ICollection;
        }

        public bool IsActive { get; set; }

        public event PreviewUndoRedoTrackingEventHandler PreviewTrackingEvent;        
        public event UndoRedoTrackingEventHandler TrackingEvent;

        private Stack<trackingObject> undoStack = new Stack<trackingObject>();
        private Stack<trackingObject> redoStack = new Stack<trackingObject>();

        private bool preview(trackingObject item, UndoRedoTrackingType trackType)
        {
            PreviewUndoRedoTrackingEventHandler handler = this.PreviewTrackingEvent;
            PreviewUndoRedoTrackingEventArgs e = new PreviewUndoRedoTrackingEventArgs(item.Source, item.Value, item.PropertyName, trackType);
            if (handler != null)
            {
                handler(this, e);
            }
            return e.Cancel;
        }

        private void track(trackingObject item, UndoRedoTrackingType trackType)
        {
            UndoRedoTrackingEventHandler handler = this.TrackingEvent;
            if (handler != null)
            {
                handler(this, new UndoRedoTrackingEventArgs(item.Source, item.Value, item.PropertyName, trackType));
            }
        }

        public bool RegisterObject(object obj)
        {
            if (obj is INotifyPropertyChanging)
            {
                (obj as INotifyPropertyChanging).PropertyChanging += new PropertyChangingEventHandler(DefaultTracking_PropertyChanging);
            }

            return true;
        }

        void DefaultTracking_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (!this.IsActive) return;

            if (sender == null) throw new NullReferenceException("The sender is null.");
            if (sender.GetType().GetProperty(e.PropertyName) == null) throw new InvalidOperationException("The object of type " + sender.GetType().FullName + " does not have property " + e.PropertyName);

            trackingObject undoObj = undoStack.Count > 0 ? undoStack.Peek() : null;
            trackingObject redoObj = redoStack.Count > 0 ? redoStack.Peek() : null;

            object val = sender.GetType().GetProperty(e.PropertyName).GetValue(sender, null);
            if (undoObj.Equals(val) || redoObj.Equals(val)) return; //Test this!

            trackingObject track = new trackingObject() {Type = trackingObject.trackingType.Changed, Source = sender, PropertyName = e.PropertyName, Value = sender.GetType().GetProperty(e.PropertyName).GetValue(sender, null) };
            this.redoStack.Clear();
            undoStack.Push(track);
        }

        public void UnregisterObject(object obj)
        {
            if (obj is INotifyPropertyChanging)
            {
                (obj as INotifyPropertyChanging).PropertyChanging -= new PropertyChangingEventHandler(DefaultTracking_PropertyChanging);
            }
        }

        public bool StartTracking()
        {
            this.IsActive = true;
            return true;
        }

        public void EndTracking()
        {
            this.IsActive = false;
            this.undoStack.Clear();
            this.redoStack.Clear();
        }

        public bool Redo()
        {
            if (!this.IsActive) return new InvalidOperationException("Not currently in tracking mode.");
            if (this.redoStack.Count == 0) return false;

            trackingObject obj = this.redoStack.Peek();

            UndoRedoTrackingType type = UndoRedoTrackingType.Redo;
            if (obj.Type == trackingObject.trackingType.Added)
            {
                type = UndoRedoTrackingType.Added;
            }
            else if (obj.Type == trackingObject.trackingType.Removed)
            {
                type = UndoRedoTrackingType.Deleted;
            }

            if (this.preview(obj, type)) return false;


            obj = this.redoStack.Pop();
            this.undoStack.Push(obj);

            if (obj.Type == trackingObject.trackingType.Changed) 
                obj.PropInfo.SetValue(obj.Source, obj.Value, null);

            this.track(obj, type);

            return true;
        }

        public bool Undo()
        {
            if (!this.IsActive) return new InvalidOperationException("Not currently in tracking mode.");
            if (this.undoStack.Count == 0) return false;

            trackingObject obj = this.undoStack.Peek();

            UndoRedoTrackingType type = UndoRedoTrackingType.Undo;
            if (obj.Type == trackingObject.trackingType.Added)
            {
                type = UndoRedoTrackingType.Added;
            }
            else if (obj.Type == trackingObject.trackingType.Removed)
            {
                type = UndoRedoTrackingType.Deleted;
            }


            if (this.preview(obj, type)) return false;


            obj = this.undoStack.Pop();
            this.redoStack.Push(obj);

            if(obj.Type == trackingObject.trackingType.Changed) 
                obj.PropInfo.SetValue(obj.Source, obj.Value, null);
            

            this.track(obj, type);

            return true;
        }

        public void AddedItems(ICollection collection, object[] items)
        {
            trackingObject track = new trackingObject() 
                { Type = trackingObject.trackingType.Added, Source = collection, Value=items };

            this.undoStack.Push(track);
            
        }

        public void RemovedItems(ICollection collection, object[] items)
        {
            trackingObject track = new trackingObject() { Type = trackingObject.trackingType.Removed, Source = collection, Value = items };

            this.undoStack.Push(track);
        }
    }
}
