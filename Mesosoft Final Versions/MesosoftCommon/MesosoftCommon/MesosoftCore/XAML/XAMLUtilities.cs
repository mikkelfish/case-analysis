﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows.Markup;
using System.IO;
using System.Xml;

namespace MesosoftCore.XAML
{
    public static class XAMLUtilities
    {
        //This creates a parser context so the XAML IO functions can take advantage of the 
        //Xaml-mapped namespaces. 
        private static ParserContext createParserContext()
        {
            ParserContext parserContext = new ParserContext();

            //Get all loaded assemblies
            Assembly[] allAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<NamespaceMapEntry> namespaces = new List<NamespaceMapEntry>();
            List<string> assemblies = new List<string>();

            //Look for XmlnsDefinitions that map CLR to Xaml namespaces and put in the parser context
            foreach (Assembly assm in allAssemblies)
            {
                object[] attrs = assm.GetCustomAttributes(typeof(XmlnsDefinitionAttribute), true);
                foreach (XmlnsDefinitionAttribute attr in attrs)
                {
                    string assemName = attr.AssemblyName;
                    if (assemName == null)
                        assemName = assm.FullName;
                    namespaces.Add(new NamespaceMapEntry(attr.XmlNamespace, assemName, attr.ClrNamespace));
                    assemblies.Add(assemName);
                }
            }


            XamlTypeMapper mapper = new XamlTypeMapper(assemblies.ToArray(), namespaces.ToArray());
            parserContext.XamlTypeMapper = mapper;
            return parserContext;
        }
        
        /// <summary>
        /// Save an object in XAML as a string. The function will encapsulate the object with <see cref="XAMLEncapsulator"/>
        /// if necessary and then use <see cref="System.Windows.Markup.XamlWriter.Save"/> to save the object. 
        /// </summary>
        /// <param name="obj">The object to save.</param>
        /// <returns>String representing the XAML.</returns>
        /// <exception cref="ArgumentNullException">Obj is null.</exception>
        /// <exception cref="SecurityException">Application is not running in full trust.</exception>
        /// <seealso cref="XAMLEncapsulator"/>
        /// <seealso cref="System.Windows.Markup.XamlWriter.Save"/>
        public static string Save(object obj)
        {
            if (XAMLEncapsulator.NeedsToBeEncapsulated(obj))
            {
                XAMLEncapsulator capsule = new XAMLEncapsulator();
                capsule.Encapsulate(obj);
                return System.Windows.Markup.XamlWriter.Save(capsule);
            }
            return XamlWriter.Save(obj);
        }

        /// <summary>
        /// Save an object in XAML with a supplied serialization manager. The function will encapsulate the object with <see cref="XAMLEncapsulator"/>
        /// if necessary and then use <see cref="System.Windows.Markup.XamlWriter.Save"/> to save the object. 
        /// </summary>
        /// <param name="obj">The object to save.</param>
        /// <param name="manager">The manager to save with.</param>
        /// <exception cref="ArgumentNullException">Obj or manager is null.</exception>
        /// <exception cref="SecurityException">Application is not running in full trust.</exception>
        /// <seealso cref="XAMLEncapsulator"/>
        /// <seealso cref="System.Windows.Markup.XamlWriter.Save"/>
        public static void Save(object obj, XamlDesignerSerializationManager manager)
        {
            if (XAMLEncapsulator.NeedsToBeEncapsulated(obj))
            {
                XAMLEncapsulator capsule = new XAMLEncapsulator();
                capsule.Encapsulate(obj);
                System.Windows.Markup.XamlWriter.Save(capsule, manager);
            }
            else XamlWriter.Save(obj, manager);
        }

        /// <summary>
        /// Save an object in XAML to a stream. The function will encapsulate the object with <see cref="XAMLEncapsulator"/>
        /// if necessary and then use <see cref="System.Windows.Markup.XamlWriter.Save"/> to save the object. 
        /// </summary>
        /// <param name="obj">The object to save.</param>
        /// <param name="manager">The stream to write to.</param>
        /// <exception cref="ArgumentNullException">Obj or stream is null.</exception>
        /// <exception cref="SecurityException">Application is not running in full trust.</exception>
        /// <seealso cref="XAMLEncapsulator"/>
        /// <seealso cref="System.Windows.Markup.XamlWriter.Save"/>
        public static void Save(object obj, Stream stream)
        {
            if (XAMLEncapsulator.NeedsToBeEncapsulated(obj))
            {
                XAMLEncapsulator capsule = new XAMLEncapsulator();
                capsule.Encapsulate(obj);
                System.Windows.Markup.XamlWriter.Save(capsule, stream);
            }
            else XamlWriter.Save(obj, stream);
        }

        /// <summary>
        /// Save an object in XAML to a given TextWriter. The function will encapsulate the object with <see cref="XAMLEncapsulator"/>
        /// if necessary and then use <see cref="System.Windows.Markup.XamlWriter.Save"/> to save the object. 
        /// </summary>
        /// <param name="obj">The object to save.</param>
        /// <param name="manager">The TextWriter to write to.</param>
        /// <exception cref="ArgumentNullException">Obj or writer is null.</exception>
        /// <exception cref="SecurityException">Application is not running in full trust.</exception>
        /// <seealso cref="XAMLEncapsulator"/>
        /// <seealso cref="System.Windows.Markup.XamlWriter.Save"/>
        public static void Save(object obj, TextWriter writer)
        {
            if (XAMLEncapsulator.NeedsToBeEncapsulated(obj))
            {
                XAMLEncapsulator capsule = new XAMLEncapsulator();
                capsule.Encapsulate(obj);
                System.Windows.Markup.XamlWriter.Save(capsule, writer);
            }
            else XamlWriter.Save(obj, writer);
        }

        /// <summary>
        /// Save an object in XAML to a given XmlWriter. The function will encapsulate the object with <see cref="XAMLEncapsulator"/>
        /// if necessary and then use <see cref="System.Windows.Markup.XamlWriter.Save"/> to save the object. 
        /// </summary>
        /// <param name="obj">The object to save.</param>
        /// <param name="manager">The writer to save with.</param>
        /// <exception cref="ArgumentNullException">Obj or writer is null.</exception>
        /// <exception cref="SecurityException">Application is not running in full trust.</exception>
        /// <seealso cref="XAMLEncapsulator"/>
        /// <seealso cref="System.Windows.Markup.XamlWriter.Save"/>
        public static void Save(object obj, XmlWriter writer)
        {
            if (XAMLEncapsulator.NeedsToBeEncapsulated(obj))
            {
                XAMLEncapsulator capsule = new XAMLEncapsulator();
                capsule.Encapsulate(obj);
                System.Windows.Markup.XamlWriter.Save(capsule, writer);
            }
            else XamlWriter.Save(obj, writer);
        }

        /// <summary>
        /// Load an XAML file with the given filename and return the created object using <see cref="System.Windows.Markup.XamlReader.Load"/>.
        /// </summary>
        /// <param name="path">The path to the XamlFile</param>
        /// <returns>The objected represented by the Xaml.</returns>
        /// <exception cref="FileNotFoundException">Thrown if the path is invalid.</exception>
        public static object Load(string path)
        {
            if (!File.Exists(path)) throw new FileNotFoundException("File " + path + " not found.");
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                ParserContext context = createParserContext();
                return Load(stream, context);
            }
        }

        /// <summary>
        /// Create an object from an XAML containing stream
        /// </summary>
        /// <param name="stream">Stream containing the XAML</param>
        /// <returns>The object represented by the XAML.</returns>
        public static object Load(Stream stream)
        {
            ParserContext context = createParserContext();
            object obj = System.Windows.Markup.XamlReader.Load(stream, context);
            if (!(obj is XAMLEncapsulator)) return obj;
            return (obj as XAMLEncapsulator).CreateObject();
        }

        /// <summary>
        /// Create an object from an XAML containing stream
        /// </summary>
        /// <param name="stream">Stream containing the XAML</param>
        /// <param name="context">The parser context.</param>
        /// <returns>The object represented by the XAML.</returns>
        public static object Load(Stream stream, ParserContext context)
        {
            object obj = System.Windows.Markup.XamlReader.Load(stream, context);
            if (!(obj is XAMLEncapsulator)) return obj;
            return (obj as XAMLEncapsulator).CreateObject();
        }

        /// <summary>
        /// Create an object from an XmlReader
        /// </summary>
        /// <param name="reader">Reader to use.</param>
        /// <returns>The object represented by the XAML.</returns>
        public static object Load(XmlReader reader)
        {
            object obj = System.Windows.Markup.XamlReader.Load(reader);
            if (!(obj is XAMLEncapsulator)) return obj;
            return (obj as XAMLEncapsulator).CreateObject();
        }
    }
}
