﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using MesosoftCore.Development;
using System.Collections;
using MesosoftCore.Inspection;
using MesosoftCore.Utilities.Reflection;

namespace MesosoftCore.XAML
{
    /// <summary>
    /// This class is for internal use only. The reason why it is public is because it is a requirement by the 
    /// XAML standard. Please ignore it.
    /// </summary>
    //This class is used to hold the value for each property in an object.
    [Created("Mikkel", DocumentedStage=DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage=DevelopmentStage.ForReview, Version="0.0" )]
    public class XEC
    {
        /// <summary>
        /// Gets or sets whether the value is a reference to the original object.
        /// </summary>
        public bool SR { get; set; }

        /// <summary>
        /// Gets or sets the name of the property.
        /// </summary>
        public string PN { get; set; }

        /// <summary>
        /// Gets or sets the value of the property.
        /// </summary>
        public object V { get; set; }
    }

    /// <summary>
    /// This class is for internal use only. The reason why it is public is because it is a requirement by the 
    /// XAML standard. Please ignore it.
    /// </summary>
    [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
    public class XET
    {
        /// <summary>
        /// Gets whether the type helper has already been setup. The type helper can only be set up for one type.
        /// </summary>
        public bool IS { get; private set; }

        /// <summary>
        /// The "XAML-friendly" type. This means it can't have any generic parameters, nor be nested.
        /// </summary>
        public object T { get; set; }

        /// <summary>
        /// If the real type is nested in another type, then the full qualification of that nested type is here.
        /// </summary>
        public string NN { get; set; }

        private List<object> typeParameters = new List<object>();
        /// <summary>
        /// A list of all the generic parameters. These parameters themselves may be nested or have their own generic parameters,
        /// so they need to be encapsulator helpers too.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<object> TP
        {
            get { return typeParameters; }
        }

        #region Private Functions
        //Helper function that returns whether the type needs to be encapsulated
        private bool needToHelp(Type t)
        {
            if (t.IsNested || t.IsGenericType) return true;
            return false;
        }

        //Helper function that makes sure the type is XAML safe
        private object getType(Type t, Type[] genericParameters)
        {
            if (needToHelp(t))
            {
                XET helper = new XET();
                helper.setup(t, genericParameters);
                return helper;
            }
            return t;
        }

        private void setup(Type type, Type[] genericParameters)
        {
            if (this.IS)
                throw new InvalidOperationException("This XAMLEncapsulatorTypeHelper has already been set up for a type and is sealed.");

            //Set (perhaps temporarily) the base type.
            this.T = type;

            if (!needToHelp(type)) return;

            //Get the parameters and store whether they belong to this type or should be passed along to the declaring type
            List<Type[]> figureOutParas = new List<Type[]>();
            if (type.IsGenericType)
            {
                //Basically what this does is pull off all the generic parameters that are not at the end "leaf" of the nested class structure.
                //For instance, if there is Class1<T>.Class2<U>.Class3<G, V>.Class4<Z> then we need to pull out T,U,G,V and pass them on
                //The actual type will be Class1 `1+Class2 `1+Class3 `2+Class4 `1[[Descriptions of all the types]]
                //The code below parses this long string and counts how many types it needs to pull out and which to pass through

                #region Parse out generic parameters

                //If we've been passed parameters, then use them, otherwise query the type for generic parameters
                //This is because only the full first type will have the parameters carried with it. The other ones will just have the generic T,V,etc. 
                //and we need to replace them with what was actually passed.
                Type[] generics = type.GetGenericArguments();
                if (genericParameters != null && genericParameters.Length != 0)
                    generics = genericParameters;

                int totalCount = 0;
                string shorten = type.FullName;
                if(shorten.Contains('['))
                    shorten = shorten.Substring(0, shorten.IndexOf('['));
                
                string[] splits = shorten.Split('`');

                List<Type> others = new List<Type>();
                //This will be all the parent nested types
                for (int i = 1; i < splits.Length-1; i++)
                {
                    string onlyNumber = splits[i];
                    if (onlyNumber.Contains('['))
                    {
                        onlyNumber = onlyNumber.Substring(0, onlyNumber.IndexOf('['));
                    }

                    if (onlyNumber.Contains('+'))
                    {
                        onlyNumber = onlyNumber.Substring(0, onlyNumber.IndexOf('+'));
                    }

                    int count = int.Parse(onlyNumber);
                    for (int j = 0; j < count; j++)
                    {
                        others.Add(generics[totalCount + j]);
                    }
                    totalCount += count;
                }
                figureOutParas.Add(others.ToArray());

                string last = splits[splits.Length - 1];
                if (last.Contains('['))
                {
                    last = last.Substring(0, last.IndexOf('['));
                }
                
                if (last.Contains('+'))
                {
                    last = last.Substring(0, last.IndexOf('+'));
                }
                int lastCount = int.Parse(last);
                Type[] lastTypeArray = new Type[lastCount];
                for (int j = 0; j < lastCount; j++)
                {
                    lastTypeArray[j] = generics[totalCount + j];
                }
                figureOutParas.Add(lastTypeArray);

                #endregion
            }

            bool nest = type.IsNested;
            //If the type is nested then we need to store the declaring type as the base type
            if (nest)
            {
                if (figureOutParas.Count != 0)
                    this.T = this.getType(type.DeclaringType, figureOutParas[0]);
                else this.T = this.getType(type.DeclaringType, null);
                this.NN = type.Name;
            }

            if (type.IsGenericType)
            {
                //Get all the arguments and store them
                Type[] args = figureOutParas[1];
                //if (genericParameters != null) args = genericParameters;
                foreach (Type para in args)
                {
                    this.TP.Add(this.getType(para, null));
                }

                //Get the unbounded type as the base type.
                if (!nest) this.T = type.GetGenericTypeDefinition();
            }
            this.IS = true;
        }

        //Helper function that gets the real type
        private Type makeType(object t)
        {
            if (t is XET) return (t as XET).CreateType();
            return t as Type;
        }

        #endregion

        /// <summary>
        /// Store the type's information so it can be written in XAML.
        /// </summary>
        /// <param name="type">The type to encapsulate</param>
        /// <exception cref="InvalidOperationException">Thrown if the encapsulator has already been set up with a type.</exception>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]        
        public void Setup(Type type)
        {
            this.setup(type, null);
        }

        /// <summary>
        /// Create the real type from all the information in the encapsulation.
        /// </summary>
        /// <returns>The fully formed type.</returns>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
        public Type CreateType()
        {
            Type toRet = this.makeType(this.T);

            Type[] createdArguments = new Type[0];
            if(toRet.IsNested)
                createdArguments = toRet.GetGenericArguments();

            if (this.NN != null)
            {
                //It can be public or private.
                toRet = toRet.GetNestedType(this.NN, BindingFlags.Public | BindingFlags.NonPublic);
            }

            if (this.TP.Count == 0) return toRet;

            Type[] paras = new Type[createdArguments.Length + this.TP.Count];
            for (int i = 0; i < createdArguments.Length; i++)
            {
                paras[i] = createdArguments[i];
            }

            for (int i = 0; i < this.TP.Count; i++)
            {
                //Unwrap the type parameters
                paras[i + createdArguments.Length] = this.makeType(this.TP[i]);
            }

            return toRet.MakeGenericType(paras);
        }
    }

    /// <summary>
    /// This class is used to create an XAML safe representation of any object. It works for collections, generics, nested classes, and non-default constructors.
    /// </summary>
    /// <remarks>
    /// Presently, it does not store events. Use <see cref="XAMLEncapsulator.NeedsToBeEncapsulated"/> to test whether the use of this class is required. If it is,
    /// then follow the example code below.
    /// </remarks>
    /// 
    [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
    public class XAMLEncapsulator
    {
        /// <summary>
        /// Figures out whether the passed object needs to be encapsulated.
        /// </summary>
        /// <param name="obj">Object to test.</param>
        /// <returns>Returns whether it should be encapsulated. If not, it means the object is safe for direct XAML writing.</returns>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
        public static bool NeedsToBeEncapsulated(object obj)
        {
            if (obj == null) return false;

            //If obj is not an encapsulator helper or it is but doesn't have a self reference
            //AND it is a generic type, then that means it must be encapsulated.
            bool ok = (!(obj is XEC) || !(obj as XEC).SR) && obj.GetType().IsGenericType;
            
            //If obj is not public or it is a collection then it must be encapsulated
            if (ok || !obj.GetType().IsPublic || obj is ICollection) return true;

            return false;
        }
   
        /// <summary>
        /// Each encapsulator can only store one object. If <see cref="XAMLEncapsulator.Encapsulate"/> has already been called
        /// then the object is sealed.
        /// </summary>
        public bool IsSealed { get; private set; }

        /// <summary>
        /// An XAML-safe representation of the type of the encapsulated object. Ignore this.
        /// </summary>
        public XET Type { get; set; }

        private List<XEC> entries = new List<XEC>();
        /// <summary>
        /// XAML safe description of the properties in the object. Ignore this.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<XEC> Entries
        {
            get
            {
                return this.entries;
            }
        }

        private List<object> objectsInCollection = new List<object>();
        /// <summary>
        /// If the encapsulated object is a collection, these are the objects in the collection. Ignore this.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<object> ObjectsInCollection
        {
            get
            {
                return this.objectsInCollection;
            }
        }

        private List<object> constructorArgs = new List<object>();
        /// <summary>
        /// If encapsulated object is <see cref="IConstructorArgsProvider"/> then this stores the construction arguments. Ignore this.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<object> ConstructorArgs
        {
            get
            {
                return this.constructorArgs;
            }
        }

        #region Private functions
        //Returns an object in a good form.
        private object getObject(object obj)
        {
            if (obj == null) return null;

            bool needs = NeedsToBeEncapsulated(obj);
            if (!needs) return obj;
            XAMLEncapsulator capsule = new XAMLEncapsulator();
            capsule.Encapsulate(obj);
            return capsule;
        }

        //Gets the real form of the object
        private object revealObject(object obj)
        {
            if (obj is XAMLEncapsulator)
            {
                return (obj as XAMLEncapsulator).CreateObject();
            }
            return obj;
        }

        [Todo("Mikkel", DevelopmentPriority.Critical, Comments = "Test all combinations")]
        private List<object> writeCollection(ICollection collection)
        {
            List<object> toRet = new List<object>();

            if (collection is IList)
            {
                foreach (object o in collection)
                {
                    toRet.Add(this.getObject(o));
                }
            }
            else if (collection is IDictionary)
            {
                foreach (object key in (collection as IDictionary).Keys)
                {
                    object val = (collection as IDictionary)[key];

                    DictionaryEntry entry = new DictionaryEntry(this.getObject(key), this.getObject(val));
                    toRet.Add(entry);
                }
            }
            else if (collection is Stack || collection.GetType().GetGenericTypeDefinition() == typeof(Stack<>))
            {
                foreach (object o in collection)
                {
                    toRet.Add(this.getObject(o));
                }
            }
            else if (collection is Queue || collection.GetType().GetGenericTypeDefinition() == typeof(Queue<>))
            {
                foreach (object o in collection)
                {
                    toRet.Add(this.getObject(o));
                }
            }
            else if (collection.GetType().GetGenericTypeDefinition() == typeof(LinkedList<>))
            {
                CommonReflection.RunGenericMethod("writeLinkedList", collection.GetType().GetGenericArguments(), this, new object[] { toRet, collection });
            }
            else
            {
                throw new NotSupportedException("The passed collection is not supported.");
            }

            return toRet;
        }

        private void writeLinkedList<T>(List<object> list, LinkedList<T> linked)
        {
            LinkedListNode<T> node = linked.First;
            while (node != null)
            {
                list.Add(this.getObject(node.Value));
                node = node.Next;
            }
        }

        private void readLinkedList<T>(LinkedList<T> list, ICollection collection)
        {
            LinkedListNode<T> lastNode = null;
            foreach (object o in collection)
            {
                if (lastNode == null)
                    lastNode = list.AddFirst((T)this.revealObject(o));
                else lastNode = list.AddAfter(lastNode, (T)this.revealObject(o));
            }
        }

        private void readQueue<T>(Queue<T> queue, ICollection collection)
        {
            foreach (object o in collection)
            {
                queue.Enqueue((T)this.revealObject(o));
            }
        }


        private void readStack<T>(Stack<T> stack, ICollection collection)
        {
            foreach (object o in collection)
            {
                stack.Push((T)this.revealObject(o));
            }
        }

        [Todo("Mikkel", DevelopmentPriority.Critical, Comments = "Test all combinations")]
        private void readCollection(ICollection obj, ICollection collection)
        {
            if (obj is IList)
            {
                foreach (object o in collection)
                {
                    (obj as IList).Add(this.revealObject(o));
                }
            }
            else if (obj is IDictionary)
            {
                foreach (object pair in collection)
                {
                    PropertyInfo keyInfo = pair.GetType().GetProperty("Key");
                    PropertyInfo valueInfo = pair.GetType().GetProperty("Value");

                    object key = this.revealObject(keyInfo.GetValue(pair, null));
                    object value = this.revealObject(valueInfo.GetValue(pair, null));

                    (obj as IDictionary).Add(key, value);
                }
            }
            else if (collection is Stack)
            {
                //Test order
                foreach (object o in collection)
                {
                    (obj as Stack).Push(this.revealObject(o));
                }
            }
            else if (obj is Queue)
            {
                //Test order
                foreach (object o in collection)
                {
                    (obj as Queue).Enqueue(this.revealObject(o));
                }
            }
            else
            {
                if (obj.GetType().GetGenericTypeDefinition() == typeof(LinkedList<>))
                {
                    CommonReflection.RunGenericMethod("readLinkedList", obj.GetType().GetGenericArguments(), this, new object[] { obj, collection });
                }
                else if (obj.GetType().GetGenericTypeDefinition() == typeof(Stack<>))
                {
                    CommonReflection.RunGenericMethod("readStack", obj.GetType().GetGenericArguments(), this, new object[] { obj, collection });
                }
                else if (obj.GetType().GetGenericTypeDefinition() == typeof(Queue<>))
                {
                    CommonReflection.RunGenericMethod("readQueue", obj.GetType().GetGenericArguments(), this, new object[] { obj, collection });
                }
                else
                {
                    throw new NotSupportedException("The passed collection is not supported.");
                }
            }
        }


        #endregion

  
        /// <summary>
        /// This function encapsulates the passed object and stores it in a way that it can be written to XAML.
        /// It is the call that builds up the encapsulator functionality.
        /// </summary>
        /// <param name="obj">The object to encapsulate</param>
        /// <exception cref="ArgumentNullException">Thrown if obj is null.</exception>
        [Todo("Mikkel", DevelopmentPriority.Critical, Comments="Check to see how self references work and how collections work both if members and if the object itself.")]
        public void Encapsulate(object obj)
        {
            if (obj == null) throw new ArgumentNullException("Passed object must not be null in order for it to be encapsulated.");

            this.Type = new XET();
            this.Type.Setup(obj.GetType());

            //If this is a collection we need to do something special.
            if (obj is ICollection)
            {
                this.objectsInCollection.AddRange(this.writeCollection(obj as ICollection));
            }

            //Get all public properties.
            PropertyInfo[] infos = obj.GetType().GetProperties();
            foreach (PropertyInfo info in infos)
            {
                //  See if the property has a visibility attribute
                DesignerSerializationVisibilityAttribute[] attr =
                    (DesignerSerializationVisibilityAttribute[])info.GetCustomAttributes(typeof(DesignerSerializationVisibilityAttribute), true);
                //if property cannot be written it is set to hidden, continue
                if (!info.CanWrite && (attr.Length == 0 || attr[0].Visibility == DesignerSerializationVisibility.Hidden)) continue;

                //Get index parameters
                ParameterInfo[] para = info.GetIndexParameters();
                
                // property has an index then continue
                if (para.Length != 0)
                {
                    continue;
                }
                else
                {
                    //get the current value of the property
                    object val = info.GetValue(obj, null);

                    //Need to encapsulate it so we can figure out what property this value belongs to
                    XEC helper = new XEC() { PN = info.Name };
                    
                    //If there is a self reference then set it.
                    if (val != null && val.Equals(obj))
                    {
                        helper.SR = true;
                    }
                    else
                    {
                        //Otherwise get the value
                        helper.V = this.getObject(val);
                    }

                    //Check to see how self reference works                    
                    this.entries.Add(helper);
                }
            }

            //If object has constructor args then save them
            if (obj is IConstructorArgsProvider)
            {
                if ((obj as IConstructorArgsProvider).ConstructionArgs != null)
                {
                    foreach (object arg in (obj as IConstructorArgsProvider).ConstructionArgs)
                    {
                        this.ConstructorArgs.Add(this.getObject(arg));
                    }
                }
            }
        }

        /// <summary>
        /// Use the information stored in the encapsulator to recreate the encapsulated object.
        /// </summary>
        /// <returns>The object that was originally encapsulated.</returns>
        [Created("Mikkel", DocumentedStage = DocumentedStage.InternalForReview | DocumentedStage.PublicForReview, DevelopmentStage = DevelopmentStage.ForReview, Version = "0.0")]
        public object CreateObject()
        {
            Type realType = this.Type.CreateType();
            object obj = null;

            //Based on whether there are stored construction args or not, create an instance of the class.
            if (this.constructorArgs == null || this.constructorArgs.Count == 0)
                obj = Activator.CreateInstance(realType);
            else
            {
                //Uncover the real arguments
                object[] realArgs = new object[this.constructorArgs.Count];
                for (int i = 0; i < realArgs.Length; i++)
                {
                    realArgs[i] = this.revealObject(this.constructorArgs[i]);
                }

                obj = Activator.CreateInstance(realType, realArgs);
            }

            if (obj is ICollection)
            {
                this.readCollection(obj as ICollection, this.ObjectsInCollection);
            }

            //For each property, get the value and set it.
            foreach (XEC helper in this.entries)
            {
                PropertyInfo info = realType.GetProperty(helper.PN);
                if(info == null) throw new InvalidOperationException("The property " + helper.PN + " was not found in type " + realType.FullName + ". Cannot recreate object.");


                if (info.CanWrite)
                {
                    if (!(helper.V is XAMLEncapsulator))
                    {
                        if (helper.V is XEC && (helper.V as XEC).SR)
                        {
                            info.SetValue(obj, obj, null);
                        }
                        else info.SetValue(obj, this.revealObject(helper.V), null);
                    }
                    else
                    {
                        object genobj = (helper.V as XAMLEncapsulator).CreateObject();
                        info.SetValue(obj, genobj, null);
                    }
                }
                else if (helper.V is ICollection || (helper.V is XAMLEncapsulator && (helper.V as XAMLEncapsulator).objectsInCollection.Count != 0))
                {
                    //If value is collection then get the object's collection
                    ICollection builtIn = info.GetValue(obj, null) as ICollection;

                    if(builtIn == null) 
                        throw new InvalidOperationException("Helper value for property " + helper.PN + " is a collection, but property type is not a collection.");
                    this.readCollection(builtIn, this.revealObject(helper.V) as ICollection);
                }


            }

            return obj;
        }

    }
}
