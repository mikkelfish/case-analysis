function filtered = ButterworthFilter(Data, SamplingRate, FilterCutOff, filterType)
[FilterB FilterA]=butter(3,FilterCutOff/SamplingRate,filterType);
filtered=filtfilt(FilterB,FilterA,Data);