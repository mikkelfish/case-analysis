function [spectrum x] = powerSpectrum(data, Fs)

N = length(data);
T = N * Fs;
p = abs(fft(data))/(N/2); %% absolute value of the fft
p = p(1:N/2).^2; %% take the power of positve freq. half
freq = (0:N/2-1)/T; %% find the corresponding frequency in Hz

spectrum = p;
x = freq;