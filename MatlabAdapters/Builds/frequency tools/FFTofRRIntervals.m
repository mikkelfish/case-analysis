function FFTofRRIntervals(tag,fid)
InterpolationRate=4;
%*** INITIAL STUFF ********************************************************
clc
close all
myposition=[38          65        1215         667];

FileName=['D:\MatlabStrohl\Human\' tag '\' tag '_IEH_Best200_L.mat'];
load(FileName)

fprintf('PreAdjustment: Itrig(1)=%6.1f,Itrig(Last)=%6.1f,Sec=%6.1f\n',Itrig(1),Itrig(length(Itrig)),Itrig(length(Itrig))-Itrig(1));
TargetEnd=Itrig(1)+480;
Itrig(Itrig>TargetEnd)=[];
fprintf('PostAdjustment:Itrig(1)=%6.1f,Itrig(Last)=%6.1f,Sec=%6.1f\n',Itrig(1),Itrig(length(Itrig)),Itrig(length(Itrig))-Itrig(1));

fprintf('PreAdjustment: HR(1)=%6.1f,HR(Last)=%6.1f,Sec=%6.1f\n',HR(1),HR(length(HR)),HR(length(HR))-HR(1));
TargetEnd=HR(1)+480;
HR(HR>TargetEnd)=[];
HR=HR-HR(1);
fprintf('PostAdjustment:HR(1)=%6.1f,HR(Last)=%6.1f,Sec=%6.1f\n',HR(1),HR(length(HR)),HR(length(HR))-HR(1));

RR=diff(HR)*1000;
numHR=length(HR);
TotalSeconds=HR(numHR);
numItrig=length(Itrig);
Irate=numItrig/TotalSeconds;

figure(1);set(1,'units','pixels','position',myposition,'menubar','none','papersize',[11 8.5],'paperpositionmode','auto');
subplot(211)
X=HR(1:length(HR)-1);
Y=RR;
size(X)
size(Y)
plot(X,Y,'.k');
length(X)
set(gca,'box','off','tickdir','out');
xlabel('Time (sec)')
ylabel('R-R Interval (ms)')
xlim([HR(2) HR(numHR)]);
ylim([prctile(Y,1) prctile(Y,99)]);
% h_line=refline(0,mean(Y));
% set(h_line,'color','g','linewidth',3)
legend(...
['Instantaneous RR Interval (ms) | Mean = ' num2str(mean(Y),'%8.2f') ' | SD = ' num2str(std(Y),'%5.2f') ' | CV = ' num2str(std(Y)/mean(Y),'%6.4f') '| cyc/min = ' num2str(mean(60000./mean(Y)),'%8.2f')],...
'location','NorthOutside');
title('Raw Discrete Event Series')

subplot(212)
xi=0:1/InterpolationRate:HR(numHR);
yi = spline(X,Y,xi);
yi=(yi-mean(yi))/mean(yi);
plot(xi,yi);
set(gca,'box','off','tickdir','out');
xlabel('Time (sec)')
ylabel('R-R Interval (ms)')
xlim([xi(1) xi(length(xi))-3]);
%ylim([min(yi) max(yi)])
ylim([prctile(yi,1) prctile(yi,99)]);

FileName=['D:\MatlabStrohl\Human\' tag '\' tag '_FFT_Preprocessing_1.pdf'];
print(gcf,[FileName],'-dpdf','-r600');

title('Interpolated, Centerd and Standardized Discrete Event Series (4 Hz)')
suptitle('Raw Discrete Event Series vs Interpolated and Centered Event Series (4 Hz)')

figure(2);set(2,'units','pixels','position',myposition,'menubar','none','papersize',[11 8.5],'paperpositionmode','auto');
w=hann(length(yi));
w=w';
size(w)
size(yi)
yi=w.*yi;
plot(xi,yi,'-k','linewidth',1)
title('Windowed (Hann)');

FileName=['D:\MatlabStrohl\Human\' tag '\' tag '_FFT_Preprocessing_2.pdf'];
print(gcf,[FileName],'-dpdf','-r600');


figure(3);set(3,'units','pixels','position',myposition,'menubar','none','papersize',[11 8.5],'paperpositionmode','auto');

Fs=InterpolationRate;
T=1/Fs;                       % Sample time
L = length(yi);        % Length of signal
t = (0:L-1)*T;                % Time vector

NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(yi,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
halfY=2*abs(Y(1:NFFT/2+1));
halfYpower=halfY.*halfY;
halfYpower=halfYpower;
CorrectionFactor=var(RR)/sum(halfYpower(f<=.40));
halfYpower=CorrectionFactor*halfYpower;
% a = 1;
% b = [1/5 1/5 1/5 1/5 1/5];
% SmoothY=filter(b,a,halfYpower);
% indexout=(f>30);
% f(indexout)=[];
% halfYpower(indexout)=[];
% f(1:3)=[];
% halfYpower(1:3)=[];

out=[f' halfYpower'];

plot(out(:,1),out(:,2),'-k')
set(gca,'box','off','TickDir','out')
xlim([0 0.5]);
maxhalfYpower=max(halfYpower(f>0.04 & f < 0.5));
ylim([0 maxhalfYpower*1.2]);
titstr=[tag ' - FFT of RR-Interval Discrete Event Series'];
h=title(titstr)           ;set(h,'FontName','SAS Monospace','FontSize',10);
h=xlabel('Frequency (Hz)');set(h,'FontName','SAS Monospace','FontSize',10);
h=ylabel('Power Spectral Density')    ;set(h,'FontName','SAS Monospace','FontSize',10);
LowLimIndex=find(out(:,1) < Irate,1,'last')-25;
Hi_LimIndex=find(out(:,1) > Irate,1,'first')+25;
LowLim=out(LowLimIndex,1);
Hi_Lim=out(Hi_LimIndex,1);
fprintf('I-L=%8.4f, H-I=%8.4f, H-L=%8.4f, Increment=%8.4f\n',Irate-LowLim,Hi_Lim-Irate,Hi_Lim-LowLim,out(LowLimIndex,1)-out(LowLimIndex-1,1));
fprintf('I-L=%8.4f, H-I=%8.4f, H-L=%8.4f\n',(Irate-LowLim+1)*60,(Hi_Lim-Irate+1)*60,(Hi_Lim-LowLim)*60);
IndexAtMax=find(halfYpower==max(halfYpower));
fprintf('Max at %f, Max = %f\n',f(IndexAtMax),halfYpower(IndexAtMax));

yy=[0 maxhalfYpower*.6];
xx=[.04 .04]          ;hold on;plot(xx,yy,'-k','linewidth',3);
xx=[.15 .15]          ;hold on;plot(xx,yy,'-k','linewidth',3);
xx=[.40 .40]          ;hold on;plot(xx,yy,'-k','linewidth',3);
text(0.02,maxhalfYpower*.6,'VLF','horizontalalignment','center','FontName','SAS Monospace','FontSize',6);
text((0.04+0.15)/2,maxhalfYpower*.6,'LF Range','horizontalalignment','center','FontName','SAS Monospace','FontSize',6);
text((0.15+0.40)/2,maxhalfYpower*.6,'HF Range','horizontalalignment','center','FontName','SAS Monospace','FontSize',6);
start=[0.020+0.010 maxhalfYpower*.6];stop =[0.04 maxhalfYpower*.6];hold on;arrow(start,stop);
start=[0.020-0.010 maxhalfYpower*.6];stop =[0.00 maxhalfYpower*.6];hold on;arrow(start,stop);
start=[0.095+0.020 maxhalfYpower*.6];stop =[0.15 maxhalfYpower*.6];hold on;arrow(start,stop);
start=[0.095-0.020 maxhalfYpower*.6];stop =[0.04 maxhalfYpower*.6];hold on;arrow(start,stop);
start=[0.275+0.020 maxhalfYpower*.6];stop =[0.40 maxhalfYpower*.6];hold on;arrow(start,stop);
start=[0.275-0.020 maxhalfYpower*.6];stop =[0.15 maxhalfYpower*.6];hold on;arrow(start,stop);

% xx=[Irate Irate]      ;hold on;plot(xx,yy,'-r','linewidth',4);
% xx=[LowLim LowLim]    ;hold on;plot(xx,yy,'-r','linewidth',2);
% xx=[Hi_Lim Hi_Lim]    ;hold on;plot(xx,yy,'-r','linewidth',2);
TotSum=sum(halfYpower(f > .04 & f <=.50));
LFSum =sum(halfYpower(f > .04 & f <=.15));
HFSum =sum(halfYpower(f > .15 & f <=.40));
LFnu=LFSum/TotSum;
HFnu=HFSum/TotSum;
LF_HF_Ratio=LFSum/HFSum;
IBandSum=sum(halfYpower(f >= LowLim & f<=Hi_Lim));
IBandnu=IBandSum/TotSum;
[RSA]=GetRSA(tag);
[pvalues]=Getpvalues(tag);
fprintf('%s: %f, %f, %f, %f\n',tag,pvalues);
mystring={['Total Power.= '];
          ['LF Power....= '];
          ['HF Power....= '];
          ['I Band Power= '];
          ['LF nu.......= '];
          ['HF nu.......= '];
          ['I Band nu...= '];
          ['LF/HF.......= '];
          ['Seconds.....= '];
          ['InspRate....= '];
          ['RSA.........= '];};
text(0.38,maxhalfYpower*.9,mystring,'horizontalalignment','left','FontName','SAS Monospace','FontSize',6)
mystring={[num2str(TotSum      ,'%7.1f')];
          [num2str(LFSum       ,'%7.1f')];
          [num2str(HFSum       ,'%7.1f')];
          [num2str(IBandSum    ,'%7.1f')];
          [num2str(LFnu        ,'%7.2f')];
          [num2str(HFnu        ,'%7.2f')];
          [num2str(IBandnu     ,'%7.2f')];
          [num2str(LF_HF_Ratio ,'%7.2f')];
          [num2str(TotalSeconds,'%7.1f')];
          [num2str(Irate       ,'%7.3f')];
          [num2str(RSA         ,'%7.3f')];};
%           ['Var(RR)     = ' num2str(var(RR)     ,'%7.1f')];
%           ['Power at I rate = ' num2str(IrateSum,'%6.3f')];
%           ['Irate/Total Ratio = ' num2str(IrateTotRatio,'%6.3f')];};
text(0.5-.002,maxhalfYpower*.9,mystring,'horizontalalignment','right','FontName','SAS Monospace','FontSize',6)

FileName=['D:\MatlabStrohl\Human\' tag '\' tag '_RSA_FFT.pdf'];
print(gcf,[FileName],'-dpdf','-r600');
fid=fopen('D:\MatlabStrohl\Human\RSA_FROM_FFT.csv','a');
mystring=[tag ',' num2str(TotalSeconds,'%7.1f') ',' num2str(TotSum    ,'%7.1f') ',' num2str(var(RR)     ,'%7.1f') ...
              ',' num2str(LFSum       ,'%5.1f') ',' num2str(LFnu      ,'%5.2f') ',' num2str(LF_HF_Ratio ,'%7.2f') ...
              ',' num2str(Irate       ,'%6.3f') ',' num2str(HFSum     ,'%5.1f') ',' num2str(HFnu        ,'%5.2f') ...
              ',' num2str(IBandSum    ,'%7.1f') ',' num2str(IBandnu   ,'%7.2f') ',' num2str(RSA         ,'%7.4f') ...
              ',' num2str(pvalues(1)  ,'%7.4f') ',' num2str(pvalues(2),'%7.4f') ',' num2str(pvalues(3)  ,'%7.4f') ',' num2str(pvalues(4)   ,'%7.4f' )];
%           ['Power at I rate = ' num2str(IrateSum,'%6.3f')];
%           ['Irate/Total Ratio = ' num2str(IrateTotRatio,'%6.3f')];};
fprintf(fid,'%s\n',mystring);
fclose(fid);

return
function [RSA]=GetRSA(tag)
if strcmpi(tag,'Sub1010V1');RSA=0.92799;end;
if strcmpi(tag,'Sub1010V2');RSA=0.91920;end;
if strcmpi(tag,'Sub1012V1');RSA=0.94618;end;
if strcmpi(tag,'Sub1012V2');RSA=0.95539;end;
if strcmpi(tag,'Sub1013V1');RSA=0.95203;end;
if strcmpi(tag,'Sub1013V2');RSA=0.95673;end;
if strcmpi(tag,'Sub1014V1');RSA=0.89553;end;
if strcmpi(tag,'Sub1014V2');RSA=0.90508;end;
if strcmpi(tag,'Sub1015V1');RSA=0.92815;end;
if strcmpi(tag,'Sub1015V2');RSA=0.90215;end;
if strcmpi(tag,'Sub1019V1');RSA=0.97264;end;
if strcmpi(tag,'Sub1019V2');RSA=0.98766;end;
if strcmpi(tag,'Sub1020V1');RSA=0.90582;end;
if strcmpi(tag,'Sub1020V2');RSA=0.90526;end;
if strcmpi(tag,'Sub1021V1');RSA=0.94079;end;
if strcmpi(tag,'Sub1021V2');RSA=0.95661;end;
if strcmpi(tag,'Sub1029V1');RSA=0.96650;end;
if strcmpi(tag,'Sub1029V2');RSA=0.98167;end;
if strcmpi(tag,'Sub1031V1');RSA=0.95859;end;
if strcmpi(tag,'Sub1031V2');RSA=0.92662;end;
if strcmpi(tag,'Sub1033V1');RSA=0.94622;end;
if strcmpi(tag,'Sub1033V2');RSA=0.94384;end;
if strcmpi(tag,'Sub1034V1');RSA=0.94068;end;
if strcmpi(tag,'Sub1034V2');RSA=0.94346;end;
if strcmpi(tag,'Sub1035V1');RSA=0.89260;end;
if strcmpi(tag,'Sub1035V2');RSA=0.94320;end;
if strcmpi(tag,'Sub1036V1');RSA=0.89597;end;
if strcmpi(tag,'Sub1036V2');RSA=0.91382;end;
if strcmpi(tag,'Sub1037V1');RSA=0.94951;end;
if strcmpi(tag,'Sub1037V2');RSA=0.95046;end;
if strcmpi(tag,'Sub1040V1');RSA=0.90043;end;
if strcmpi(tag,'Sub1040V2');RSA=0.94113;end;
if strcmpi(tag,'Sub1043V1');RSA=0.92928;end;
if strcmpi(tag,'Sub1043V2');RSA=0.93091;end;
if strcmpi(tag,'Sub1044V1');RSA=0.82240;end;
if strcmpi(tag,'Sub1044V2');RSA=0.80782;end;
if strcmpi(tag,'Sub1045V1');RSA=0.77693;end;
if strcmpi(tag,'Sub1045V2');RSA=0.81283;end;
return
function [pvalues]=Getpvalues(tag)
% 1     2      3            4           5           6           7            8          9          10 
%SUB, VISIT,pR_10_RtoI, pR_10_ItoR, pR_14_RtoI, pR_14_ItoR, pR_10_RtoE, pR_10_EtoR, pR_14_RtoE, pR_14_EtoR
pvalueArray=[
1010	1	3.6710306	3.9089087	6.6111434	5.9849318	0.91523901	0.29425954	0.64957638	0.79607183;
1010	2	2.8856678	1.3757477	1.4129777	1.8662416	1.9248237	0.975845	3.0526261	1.8176455;
1012	1	9.0167049	1.4452246	9.3279737	4.1469884	0.77899563	0.53247511	2.015359	1.8647907;
1012	2	10.177317	2.3250706	8.0319081	3.4039601	0.51616418	0.025646845	0.34637782	0.46666594;
1013	1	0.84634611	0.087213231	0.61820735	0.79022163	0.43800604	0.29285478	0.44844162	0.91454303;
1013	2	0.35203319	0.086915666	0.60135792	0.069564262	0.5055062	1.2434082	0.21525584	1.4368421;
1014	1	0.41494961	0.48237797	2.0198378	0.79727555	0.24711075	0.95337123	1.0078737	2.662012;
1014	2	2.1467659	0.14835185	2.0506449	0.10755251	0.37205124	1.8723679	1.3461736	1.2803792;
1015	1	0.73958104	1.7364941	2.0877979	2.1680077	0.44959668	0.73394563	0.86078108	0.36984872;
1015	2	3.8985843	1.2125108	6.3107169	2.2145092	1.61968	0.25152494	1.2696434	0.61192778;
1019	1	3.0401722	2.0970166	2.1336314	3.1401776	0.13350756	0.56809986	0.037952934	0.69511204;
1019	2	1.743076	1.3692683	7.3411497	3.1623636	0.96289476	4.5244583	7.252566	6.002695;
1020	1	1.2636678	1.4495708	1.7222024	1.8021936	1.5010978	0.40056148	2.9725124	0.58093604;
1020	2	6.3282791	0.11335227	7.7964961	4.3597582	0.52397358	0.53342121	0.75556105	2.1376452;
1021	1	2.2345768	2.4625997	1.9176912	2.4510932	0.18054242	0.14997993	0.021798668	0.26316085;
1021	2	0.73926794	2.52151	1.0857097	2.9858179	0.18441713	0.39972843	0.82007245	0.467172;
1029	1	0.73431829	0.98549155	0.75767219	0.65026721	0.2269378	0.080314069	0.8359282	0.3547628;
1029	2	0.50520401	0.1421596	2.1078713	0.076545817	0.14496678	1.0375879	0.83283632	0.70570828;
1031	1	5.4629282	3.0756798	3.4426121	5.9727626	6.7625889	4.1075647	4.2693648	3.3390788;
1031	2	0.12507764	0.45469729	0.18760857	1.107779	0.79034837	1.0219932	1.449027	0.81003854;
1033	1	0.01379823	0.80574688	0.26796564	1.3753002	0.011606062	0.00338965	1.0452262	0.15201213;
1033	2	0.33271348	1.1466976	0.12472125	0.98976975	0.61625439	0.96526859	0.72675977	0.59751061;
1034	1	0.15304836	0.24619646	0.23049838	1.3635751	0.21113722	0.14998452	0.68284325	1.2098122;
1034	2	0.25882555	0.39032219	0.096358076	0.28543677	0.3459527	0.20078306	0.45673623	0.25916178;
1035	1	0.80557487	2.7334147	1.3206498	1.5999589	2.1091758	0.67857048	5.6596727	3.3615338;
1035	2	1.3389488	0.58611049	1.162424	0.5062776	0.028586213	0.011713918	0.54195217	0.21167988;
1036	1	1.6854937	0.04471187	0.098925159	1.4019292	0.59952681	0.13081275	4.0872528	1.5723183;
1036	2	1.2454451	0.75500048	0.75227316	2.6159558	1.9139407	0.99779125	3.0271378	1.775514;
1037	1	2.1801079	2.5631146	0.67382561	1.1642958	0.59965116	0.52274251	1.743587	0.53249868;
1037	2	0.38560121	0.741744	0.3160149	0.78265019	0.26835555	0.52879468	0.45209665	0.19948916;
1040	1	0.48219722	1.1253633	0.60637414	0.67577058	1.7670789	2.6574009	3.1409903	2.1870073;
1040	2	0.5333154	0.66496068	5.2441524	1.2691855	0.18029561	0.21843451	2.3524304	0.92472054;
1043	1	1.5599591	0.064320918	2.5190876	0.48135952	0.049022682	0.3216919	0.69141002	0.57677668;
1043	2	0.3830572	0.14091261	1.2531302	0.23501323	0.000267754	0.49717145	1.139295	0.9119307;
1044	1	0.96970014	1.0060107	16	0.62913575	0.74999545	1.638226	4.0905843	4.9880053;
1044	2	0.42740772	0.8048031	14.523226	2.1582545	0.74997053	1.1694456	4.7578296	3.3176882;
1045	1	0.87378734	0.63597999	8.2349957	15.95459	0.0614579	0.32271367	3.4603091	4.2540584;
1045	2	0.11527592	0.17655415	1.3206016	3.2447841	0.028165455	0.60355044	5.1537988	4.1505429;];
SUB=str2num(tag(4:7));
Visit=str2num(tag(9));
for i = 1:38
    if pvalueArray(i,1)==SUB && pvalueArray(i,2) == Visit;
        pvalues(1)=pvalueArray(i, 5);
        pvalues(2)=pvalueArray(i, 6);
        pvalues(3)=pvalueArray(i, 9);
        pvalues(4)=pvalueArray(i,10);
        return
    end;
end;
fprintf('SHOULD NEVER GET HERE!!!');
gun;
return

