function [freqs power] = FFTofIntervals(data,interpolationRate, maxFreq)
dataLength = length(data);
intervals=diff(data)*1000;
X=data(1:length(data)-1);
Y=intervals;
xi=0:1/interpolationRate:data(dataLength);
yi = spline(X,Y,xi);
yi=(yi-mean(yi))/mean(yi);

w=hann(length(yi));
w=w';
yi=w.*yi;

Fs=interpolationRate;
T=1/Fs;                       % Sample time
L = length(yi);        % Length of signal
t = (0:L-1)*T;                % Time vector
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(yi,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
halfY=2*abs(Y(1:NFFT/2+1));
halfYpower=halfY.*halfY;
halfYpower=halfYpower;
CorrectionFactor=var(intervals)/sum(halfYpower(f<=maxFreq));
halfYpower=CorrectionFactor*halfYpower;


freqs = f';
power = halfYpower';

