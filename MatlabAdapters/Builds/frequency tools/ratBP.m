

function [ratio, higFreq, lowFreq, px, Lomb] = ratBP(B,band1low,band1high,band2low,band2high)

ofac = 1;
hifac = 1;
%INCR = 2;
%DECR = 1;

%if stop == 0
%    stop = inf;
%end;
%B=ones(2,length(A));
%tot_N = length(A);
%i=1;
%delta=0;
%curr_state = 0;
%prev_state = 0;
%bit = 1;
%sc=0;

%for t=1:length(A)-1
%    if(A(1,t)>start && A(1,t)<stop)
%        delta = A(2,t+1)-A(2,t);
%        if delta > 0 
%            curr_state = INCR; 
%        end
%        if delta < 0 
%             curr_state = DECR; 
%        end
%        if bit == 1  
%            prev_state = curr_state;
%            bit = 0;
%        end
%        if curr_state - prev_state == -1 
%            B(:,i) = A (:,t);
%            i = i+1;
%        end
%        if delta == 0
%            B(1,i)=(A(1,t)+A(1,t+1))/2.0;
%           B(2,i)=A(2,t)+((A(2,t)+A(2,t+1)-A(2,t-1)+A(2,t+2))/4);
%            B(2,i) = A(2,t);
%            i=i+1;
%            bit = 1;
%            sc = sc +1 ;
%        end
%        prev_state = curr_state;
%    end    
%end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% No prob in this

i=length(B);
PrdGrm=zeros(2,i);
maxFreq=0;
for t=1:i
    PrdGrm(2,t)=B(2,t); 
    PrdGrm(1,t)=B(1,t);
%    PrdGrm(2,t)=sin(4*pi*t/5);
%    PrdGrm(1,t)=t/5; 
    if t > 1
      if(1/(PrdGrm(1,t)-PrdGrm(1,t-1))) > maxFreq
          maxFreq = 1/(PrdGrm(1,t)-PrdGrm(1,t-1));
      end;
   end
end;

 clear A;
 clear B;

tmax = max(PrdGrm(1,:));
tmin = min(PrdGrm(1,:));
tdif = tmax - tmin;
tave = (tmax+tmin)/2;
pnow = 1/(tdif*ofac);
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% No prob in this
% 
% % Calculation of Average of Periodogram (APG)
APG = 0;
for t=1:length(PrdGrm)
    APG=APG+PrdGrm(2,t);
end;
APG=APG/length(PrdGrm);
% 
% Calculate the Variance of Periodogram (VPG)
VPG=0;
for t=1:length(PrdGrm)
    VPG=VPG+(PrdGrm(2,t)-APG)^2;
end;
VPG=VPG/(length(PrdGrm)-1);
if VPG == 0;
    VPG = 1;
    APG = 0;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% No prob in this

for t=1:length(PrdGrm)
    PrdGrm(2,t)=PrdGrm(2,t)-APG;
end;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

w = length(PrdGrm);
nout  = hifac*ofac*w/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Tau=zeros(1,w);
arg1=zeros(1,w);
arg2=zeros(1,w);
wpr = zeros(1,w);
wpi = zeros(1,w);
wr = zeros(1,w);
wi = zeros(1,w);
Lomb = zeros(1,nout);
for j=1:w;
    arg = 2*pi*((PrdGrm(1,j)-tave)*pnow);
    wpr(j)= -2*power(sin(arg/2),2);
    wpi(j)=sin(arg);
    wr(j)=cos(arg);
    wi(j)=wpi(j);
end;

px = zeros(1,nout);
for i=1:nout;
    px(i) = pnow;
    nr = 0;
    dr = 0;
    for j=1:w;
        cosine = wr(j);
        sine = wi(j);
        nr = nr + (sine*cosine);
        dr = dr + ((cosine*cosine)-(sine*sine));
    end;
    wtau = atan2(2*nr,dr)/2;
    swtau = sin(wtau);
    cwtau = cos(wtau);
    sums=0;
    sumc=0;
    sumsy=0;
    sumcy=0;
    for j=1:w;
        s = wi(j);
        c = wr(j);
        ss = s*cwtau - c*swtau; % sin(a-b) = sin(a)cos(b) - cos(a)sin(b)
        cc = c*cwtau + s*swtau; % cos(a-b) = cos(a)cos(b) + sin(a)sin(b)
        sums = sums + (ss*ss);  % square and update
        sumc = sumc + (cc*cc);
        yy = PrdGrm(2,j);       % already normalized
        sumsy = sumsy + (yy*ss); % PrdGrm * Sin
        sumcy = sumcy + (yy*cc); % PrdGrm * Cos
        wtemp  = wr(j);         % update arguments
        wr(j) = (wr(j)*wpr(j)-wi(j)*wpi(j))+wr(j);
        wi(j) = (wi(j)*wpr(j)+wtemp*wpi(j))+wi(j);
    end;
    Lomb(i) = 0.5*(sumcy*sumcy/sumc+sumsy*sumsy/sums)/VPG; % Final formula :)
    pnow = pnow + (1/(ofac*tdif));                         % Update the frequency   
end;

lowFreq=0;
higFreq=0;
ratio = 0;
for i=1:nout
   if px(i) >= band1low && px(i) < band1high 
       lowFreq = lowFreq+Lomb(i);
   end
   if px(i) >= band2low && px(i) < band2high
           higFreq = higFreq+Lomb(i);
   end
end
ratio=higFreq/lowFreq;
% stem(px,Lomb);

% disp ('Low Fequency'), disp(lowFreq);
% disp ('High Fequency'), disp(higFreq);
% disp ('Ratio'), disp(ratio);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % 
%N=nout;
%fft_x=fft(PrdGrm(2,:),N);    
%Mx=abs(fft_x)/N;        
%fx=(1:N);   

% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % 
