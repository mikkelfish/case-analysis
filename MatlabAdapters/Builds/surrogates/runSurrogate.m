function [final_surr] = runSurrogate(y, amp, numiters)

down = find_down(y, amp);
up = find_up(y, amp);

final_surr=surr_1(y,up,down,1, numiters);