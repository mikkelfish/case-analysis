function [newup] = find_up(y,amp)
len = length(y);
y_n = y(1:len-1);
y_f = y(2:len);
up = zeros(1,len);
up(y_n < amp & y_f >= amp) = 1;
upind = find(up)+1;

diffup = diff(upind);
lend = length(diffup);
diffup_n = diffup;


sd = sqrt(var(diffup));
diffup_n(diffup < mean(diffup)-sd)= 0;
diffup_f = diffup;
diffup_f(diffup >= mean(diffup)-sd)= 0;

indices = find(diffup_n);
for i = 1:length(indices)
    if i == 1
        diffup_n(indices(i)) = diffup_n(indices(i))+sum(diffup_f(1:indices(i)));
    else
        diffup_n(indices(i)) = diffup_n(indices(i))+sum(diffup_f(indices(i-1):indices(i)));
    end
end
diffup_n = diffup_n(find(diffup_n));
newind = [upind(1) upind(1)+cumsum(diffup_n)];
newup = zeros(1,len);
newup(newind) = 1;