function [SD]=myss(data,M)

if(size(data,1)>1) data=data'; end
if(size(data,1)>1) disp('error1'); return; end
if(nargin<2) M=150; end

err = zeros(1,M);
ED=mean(data);data=data-ED;
Len=length(data);
[R,IX]=sort(rand(1,Len));
R=data(IX);
C=sort(data);
Y=fft(data);
S=abs(Y);
for i=1:M
    %     close all;
    %     hist(R,100);
    %     pause();
    Y=fft(R);
    V=(Y./abs(Y)).*S;
    SDtemp=ifft(V);
    [B,IX]=sort(SDtemp);
    R1=R;
    R(IX)=C;
    err(i) = sum(R1-R);
    if i>2
        if err(i)== 0 && err(i-1) ==0 && err(i-2) == 0
            break;
        end
    end
    
end
SD=R+ED;
% efinal=sum(R1-R);
% [ad b]= hist(data,100);
% [as b]= hist(SD,100);
% ehist = sum(ad-as);
% datacorr = fftshift(xcorr(data));
% datasurr = fftshift(xcorr(surr));
% datacorr = datacorr(1:10000);
% datasurr = datasurr(1:10000);
% ecorr = abs(datacorr-datasurr);
%
