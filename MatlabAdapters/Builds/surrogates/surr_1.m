function [final_surr] = surr_1(y,up,dn,option, numiters)

up = find(up);
dn = find(dn);
count = min(length(dn),length(up));
up = up(1:count);
dn = dn(1:count);
%y=y(up(1):length(y));
len = length(y);

final_surr = zeros(1,len);

while up(1) > dn(1)
    up = up(1:count-1);
    dn = dn(2:count);
    count = count -1;
end

for i = 1:count
    switch i
        case count    
            deact = [deact,y(dn(i):len)];
            act = [act,y(up(i):dn(i))];
        case 1
            deact = y(dn(i):up(i+1));
            act = y(up(i):dn(i));
        otherwise
            deact = [deact,y(dn(i):up(i+1))];
            act = [act,y(up(i):dn(i))];
    end
    
    
end;
    
surr_act = myss(act,numiters);
for i = 1:count
	if i < count
		final_surr(up(i):dn(i))=surr_act(1:dn(i)-up(i)+1);
		final_surr(dn(i):up(i+1)) = y(dn(i):up(i+1));
		surr_act = surr_act(dn(i)-up(i)+2:length(surr_act));
	end
	if i == count
		final_surr(up(i):dn(i))=surr_act(1:dn(i)-up(i)+1);
		final_surr(dn(i):len) = y(dn(i):len);
	end
end

