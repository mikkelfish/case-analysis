%   ItSurrDat.m
function [SD,efinal]=ItSurrDat(data,M)
%   ItSurrDat.m
%   usage: [SD,efinal]=ItSurrDat(data,M)
%   Generates surrogate data set of the same length and with the same 
%   linear statistical properties as data.  The distribution, 
%   autocorrelation, and power spectrum are preserved, while the phase is 
%   iteratively modified.
%
%   Inputs:
%   data        time-series data
%   M           maximum iterations (default=50)
%
%   Outputs:
%   SD          surrogate data
%   efinal      error at final iteration
%
%   Anatoly Zlotnik, May 2006
%   Thanks to Farhad Kaffashi for suggestions
%
%   [1] Zlotnik, A., Algorithm Development for Modeling and 
%       Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
%       Western Reserve University, June 2006 
%   [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
%       Physica, 142:3; 2000.
%

if(size(data,1)>1) data=data'; end
if(size(data,1)>1) disp('error1'); return; end
if(nargin<2) M=50; end


ED=mean(data);data=data-ED;
Len=length(data);
[R,IX]=sort(rand(1,Len));
R=data(IX);
C=sort(data);
P=floor(log2(Len))+1; fpts=2^P;
if(fpts*0.8<Len) P=P+1; fpts=2^P; end
pad1=floor((fpts-Len)/2); pad2=floor((fpts-Len)/2)+mod((fpts-Len),2);
Y=fft([zeros(1,pad1) data zeros(1,pad2)]);
S=abs(Y(2:fpts/2+1));
%Phi=atan(imag(Y(1:Len/2,:))./real(Y(1:Len/2,:)));
for i=1:M
    Y=fft([zeros(1,pad1) R zeros(1,pad2)]);
    V=(Y(2:fpts/2+1)./abs(Y(2:fpts/2+1))).*S;
    V(fpts/2+1:fpts-1)=conj(V(fpts/2-1:-1:1));V=[sum(data) V];
    SDtemp=ifft(V); SDtemp=SDtemp(pad1+1:pad1+Len);
    [B,IX]=sort(SDtemp);R1=R;
    R(IX)=C;
    if(sum(R1-R)==0) break; end
end
SD=R+ED;efinal=sum(R1-R);