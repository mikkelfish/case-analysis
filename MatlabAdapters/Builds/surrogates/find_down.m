function [newdn] = find_down(y,amp)
len = length(y);
y_n = y(1:len-20);
y_f = y(21:len);
dn = zeros(1,len);
dn(y_n >= amp & y_f < amp) = 1;
dnind = find(dn)+20;

diffdn = diff(dnind);
lend = length(diffdn);
diffdn_n = diffdn;

sd = sqrt(var(diffdn));
diffdn_n(diffdn < mean(diffdn)+sd)= 0;
diffdn_f = diffdn;
diffdn_f(diffdn >= mean(diffdn)+sd)= 0;

indices = find(diffdn_n);
for i = 1:length(indices)
    if i == 1
        diffdn_n(indices(i)) = diffdn_n(indices(i))+sum(diffdn_f(1:indices(i)));
    else
        diffdn_n(indices(i)) = diffdn_n(indices(i))+sum(diffdn_f(indices(i-1):indices(i)));
    end
end
diffdn_n = diffdn_n(find(diffdn_n));
newind = [dnind(1) dnind(1)+cumsum(diffdn_n)];
newdn = zeros(1,len);
newdn(newind) = 1;

