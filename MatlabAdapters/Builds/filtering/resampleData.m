function [downsampled outputRes] = resampleData(data, amount, inputRes)
downsampled = decimate(data, amount);
outputRes = inputRes * amount;