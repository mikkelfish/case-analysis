function [filty] = my_filt(y,res,f1,f2)
sF = 1.0/res;
if f1 > f2
    temp = f1;
    f1 = f2;
    f2 = temp;
end    
y=y-mean(y);
Len = length(y);
T = Len/sF;
Y = fftshift(fft(y));
myfilt = ones(1,Len);
freq = [-Len/2:Len/2-1]./T;
myfilt(abs(freq)>f1 & abs(freq)<=f2)=0;
if size(Y) == size(myfilt)
    Y=Y.*myfilt;
else
    Y=Y'.*myfilt;
end;
filty = ifft(fftshift(Y));
filty = filty(Len:-1:1);
end

