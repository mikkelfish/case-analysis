function [index tdown]= multiband_crossfreq_coupling(x,w,fcPNA,fcSNA,sp,bw,triggers)
Bn=length(fcSNA); %number of frequency bin
%mphase=0; % don't know (or doesn't make sense) to calculate phase different of two signal with different freq.

fx = fcPNA/(sp/2);
x1 = BPfilter(x(:,1),(fcPNA-bw/2)/(sp/2),(fcPNA+bw/2)/(sp/2)); %bandpass filter
xdat=hilbert(x1); 
px = atan2(imag(xdat),real(xdat)); neg = find(px<0); px(neg)=px(neg)+2*pi;
f1 = (fcSNA-bw/2)/(sp/2);
f2 = f1+bw/(sp/2);
if w~=0 %define window as 1,2,3...5 seconds
    N = sp*w; 
    Seg = floor(length(x)/N); 
    tdown = w*(1:Seg);
    index =zeros(Bn,Seg);     
    for i = 1:Bn %for all band
        y1 = BPfilter(x(:,2),f1(i),f2(i));
        ydat=hilbert(y1);%compute hilbert transform
        py = atan2(imag(ydat),real(ydat)); neg = find(py<0); py(neg)=py(neg)+2*pi;
        %atan2() is similar to angle(), but we need to swap x an y 
        fy = fcSNA(i)/(sp/2);
        [m n] = rat(fy/fx);
        gamma=m*px-n*py; gamma=mod(gamma,2*pi);
        for s = 0:Seg-1 %segmentation 
            index(i,s+1) = sqrt((mean(cos(gamma(s*N+1:(s+1)*N))))^2+(mean(sin(gamma(s*N+1:(s+1)*N))))^2);   
        end
    end

else % if w==0, we divide by breath length
    %tdown = breath_marker(x(:,1),sp); %expiration detection, you may have a better algorithm do to this
    tdown = triggers;
    index =zeros(Bn,length(tdown));    
    for i = 1:Bn %for all band
        y1 = BPfilter(x(:,2),f1(i),f2(i));
        ydat=hilbert(y1);%compute hilbert transform
        py = atan2(imag(ydat),real(ydat)); neg = find(py<0); py(neg)=py(neg)+2*pi;
        fy = fcSNA(i)/(sp/2);
        [m n] = rat(fy/fx);
        gamma=m*px-n*py; gamma=mod(gamma,2*pi);
        for s = 1:length(tdown)-1          
            index(i,s+1) = sqrt((mean(cos(gamma(tdown(s):tdown(s+1)-1))))^2+(mean(sin(gamma(tdown(s):tdown(s+1)-1))))^2);   
        end
    end
 
end
