function Y=BPfilter(X,f1,f2)
%Two matlab function here, FIRCLS and FILTFILT
%FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
f = [0 f1 f2 1];
a = [0 1 0]; n = 256;
up = [0.005 1.02 0.005]; lo = [-0.005 0.98 -0.005];
b = fircls(n,f,a,up,lo); %fir filter
Y = filtfilt(b,1,X); %zero phase filtering
