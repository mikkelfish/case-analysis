function [index mphase tdown]= multiband_coupling(x,w,f1,f2,sp,triggers)
Bn=length(f1); %number of frequency bin

if w~=0 %define window as 1,2,3...5 seconds
    N = sp*w; 
    Seg = floor(length(x)/N); 
    tdown = w*(1:Seg);
    index =zeros(Bn,Seg);     
    mphase=index; %variable declaration
    for i = 1:Bn %for all band
        x1 = BPfilter(x(:,1),f1(i),f2(i)); %bandpass filter
        y1 = BPfilter(x(:,2),f1(i),f2(i));
        xdat=hilbert(x1); ydat=hilbert(y1);%compute hilbert transform
        %atan2() is similar to angle(), but we need to swap x an y 
        gamma = atan2(imag(ydat).*real(xdat) - real(ydat).*imag(xdat),...
            real(ydat).*real(xdat) + imag(ydat).*imag(xdat));
        neg = find(gamma<0); gamma(neg)=gamma(neg)+2*pi;
        gamma = 2*pi-gamma; %to define like angle
        for s = 0:Seg-1 %segmentation 
            index(i,s+1) = sqrt((mean(cos(gamma(s*N+1:(s+1)*N))))^2+(mean(sin(gamma(s*N+1:(s+1)*N))))^2);   
            mphase(i,s+1)=atan2(mean(sin(gamma(s*N+1:(s+1)*N))),mean(cos(gamma(s*N+1:(s+1)*N))));
            if mphase(i,s+1)<0, mphase(i,s+1)=mphase(i,s+1)+2*pi;end
        end
    end

else % if w==0, we divide by breath length
    %tdown = breath_marker(x(:,1),sp); %expiration detection, you may have a better algorithm do to this
    tdown = triggers;
    index =zeros(Bn,length(tdown));    mphase=index; %variable declaration
    for i = 1:Bn %for all band
        x1 = BPfilter(x(:,1),f1(i),f2(i));
        y1 = BPfilter(x(:,2),f1(i),f2(i));
        xdat=hilbert(x1); ydat=hilbert(y1);
        gamma = atan2(imag(ydat).*real(xdat) - real(ydat).*imag(xdat),...
            real(ydat).*real(xdat) + imag(ydat).*imag(xdat));
        neg = find(gamma<0); gamma(neg)=gamma(neg)+2*pi;
        gamma = 2*pi-gamma; %to defind like angle
        for s = 1:length(tdown)-1          
            index(i,s+1) = sqrt((mean(cos(gamma(tdown(s):tdown(s+1)-1))))^2+(mean(sin(gamma(tdown(s):tdown(s+1)-1))))^2);   
            mphase(i,s+1)=atan2(mean(sin(gamma(tdown(s):tdown(s+1)-1))),mean(cos(gamma(tdown(s):tdown(s+1)-1))));
            if mphase(i,s+1)<0, mphase(i,s+1)=mphase(i,s+1)+2*pi;end
        end
    end
 
end
