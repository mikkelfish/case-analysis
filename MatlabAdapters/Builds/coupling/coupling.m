function [variance, means] = runHilbertCoupling(data1, data2, samplingFrequency, targetFrequency, windowLength)
	sp = samplingFrequency; 
	spdat = targetFrequency;
		
    y1=resample(data1,spdat,sp);
    y2=resample(data2,spdat,sp);
    sp=spdat;
    pna=(y1-mean(y1))/std(y1);
    sna=(y2-mean(y2))/std(y2);
    xdat=hilbert(pna);
    ydat = hilbert(sna);
    angleh = angle((real(xdat).*real(ydat) + imag(xdat).*imag(ydat))+(imag(xdat).*real(ydat) - real(xdat).*imag(ydat))*j);
    neg = find(angleh<0); angleh(neg)=angleh(neg)+2*pi;
	
	w=windowLength;
	N = sp*w;
	Seg = floor(length(y1)/N);
	B1 = zeros(Seg,1); B2=B1; 
	for s = 0:Seg-1  
		B1(s+1)=sqrt((mean(cos(angleh(s*N+1:(s+1)*N))))^2+(mean(sin(angleh(s*N+1:(s+1)*N))))^2);
		B2(s+1)=angle(mean(cos(angleh(s*N+1:(s+1)*N))) + mean(sin(angleh(s*N+1:(s+1)*N)))*j);
		if B2(s+1)<0, B2(s+1)=B2(s+1)+2*pi;end
	end
	
	variance = B1;
	means = B2;