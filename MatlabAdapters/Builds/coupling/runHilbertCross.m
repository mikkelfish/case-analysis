function [index t] = runHilbertCross(data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers)

sp = dataFreq;
spdat = targetFreq;
x = [data1; data2]';
bw = freqBand;
fcmin = minFreq;
fcmax = maxFreq;
df = freqStep;
w = windowSize;

%resampling
y1=resample(x(:,1),spdat,sp); %pna
y2=resample(x(:,2),spdat,sp); %sna
sp=spdat;
x=[y1 y2];

%bw = 0.5;%bandwidth
%df = 0.1;%delta f
%fcmin = 0.3;% center freq of the first freq band
%fcmax = 2.3;% center freq of the last freq band
%w=0; %window, if window=0->segmentation based on breath length

%pass band frequency 
%f1=(fcmin-bw/2:df:fcmax-bw/2)/(sp/2); 
%f2=f1+bw/(sp/2);

%[index means t]= multiband_coupling(x,w,f1,f2,sp,triggers);
%figure(1)
%surface(t/sp,fcmin:df:fcmax,b1); shading flat; colormap 'hot'; axis 'tight';
%ylabel('frequency (Hz)');
%xlabel('time (seconds)');

%figure(2)
%surface(t/sp,fcmin:df:fcmax,b2); shading flat; colormap 'hot';axis 'tight';
%ylabel('frequency (Hz)');
%xlabel('time (seconds)');

%Here is cross freq coupling
%compare fcPNA component of PNA to fcSNA component of SNA;

if w ~= 0
   triggers = 0; 
end

fcPNA = data1Freq; 
fcSNA = (fcmin:df:fcmax);
[b3 t]= multiband_crossfreq_coupling(x,w,fcPNA,fcSNA,sp,bw,triggers);

%figure(3)
%surface(t/sp,fcSNA,b3); shading flat; colormap 'hot'; axis 'tight';
%ylabel('frequency (Hz)');
%xlabel('time (seconds)');

index = b3;