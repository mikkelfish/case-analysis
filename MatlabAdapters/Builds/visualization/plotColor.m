function plotColor(x, y, r, fignum, map)
figure(fignum);
clf;
pcolor(x,y,r);
colormap(map);
shading flat;