function plotHistograms(x, data, titlestr, xlabelstr, ylabelstr, colors, linecolors, info, savepath)
s = size(x);
for i=1:s(1)
    bar(x(i, 1:length(x)), data(i, 1:length(data)), 'FaceColor', colors(i), 'EdgeColor', linecolors(i));   
    hold all;
end

title(titlestr);
if isempty(xlabelstr) ~= 1
    xlabel(xlabelstr);
end

if isempty(ylabelstr) ~= 1
   ylabel(ylabelstr); 
end

if isempty(info) ~= 1
   v = axis;
   text(v(2), v(4), info, 'HorizontalAlignment', 'right', 'VerticalAlignment', 'top'); 
end

if isempty(savepath) ~= 1
   [savepath titlestr]
   print(gcf,[savepath titlestr  '.eps'],'-deps','-r150'); 
end

hold off;