/*
* MATLAB Compiler: 4.11 (R2009b)
* Date: Thu Dec 17 11:52:31 2009
* Arguments: "-B" "macro_default" "-W" "dotnet:AllMatlab,coupling,0.0,private" "-d"
* "C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\AllMatlab\src" "-T" "link:lib" "-v"
* "class{coupling:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\BPfilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_coupling.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_crossfreq_coupling.m,C:\Users\Mik
* kel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbert.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbertCross.m}"
* "class{surrogates:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m}"
* "class{filtering:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\my_filt.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\resampleData.m}"
* "class{frequencytools:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\BandButterworthFilter.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ButterworthFilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\powerSpectrum.m}" "class{informationtheory:C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
* theory\MutInf.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\information theory\titration.m}"
* "class{projections:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m}" 
*/
using System;
using System.Reflection;
using System.IO;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using MathWorks.MATLAB.NET.ComponentData;

namespace AllMatlabNative
{
  /// <summary>
  /// The projections class provides a CLS compliant, Object (native) interface to the
  /// M-functions contained in the files:
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m
  /// </summary>
  /// <remarks>
  /// @Version 0.0
  /// </remarks>
  public class projections : IDisposable
  {
    #region Constructors

    /// <summary internal= "true">
    /// The static constructor instantiates and initializes the MATLAB Component Runtime
    /// instance.
    /// </summary>
    static projections()
    {
      if (MWMCR.MCRAppInitialized)
      {
        Assembly assembly= Assembly.GetExecutingAssembly();

        string ctfFilePath= assembly.Location;

        int lastDelimiter= ctfFilePath.LastIndexOf(@"\");

        ctfFilePath= ctfFilePath.Remove(lastDelimiter, (ctfFilePath.Length - lastDelimiter));

        string ctfFileName = MCRComponentState.MCC_AllMatlab_name_data + ".ctf";

        Stream embeddedCtfStream = null;

        String[] resourceStrings = assembly.GetManifestResourceNames();

        foreach (String name in resourceStrings)
        {
          if (name.Contains(ctfFileName))
          {
            embeddedCtfStream = assembly.GetManifestResourceStream(name);
            break;
          }
        }
        mcr= new MWMCR(MCRComponentState.MCC_AllMatlab_name_data,
                       MCRComponentState.MCC_AllMatlab_root_data,
                       MCRComponentState.MCC_AllMatlab_public_data,
                       MCRComponentState.MCC_AllMatlab_session_data,
                       MCRComponentState.MCC_AllMatlab_matlabpath_data,
                       MCRComponentState.MCC_AllMatlab_classpath_data,
                       MCRComponentState.MCC_AllMatlab_libpath_data,
                       MCRComponentState.MCC_AllMatlab_mcr_application_options,
                       MCRComponentState.MCC_AllMatlab_mcr_runtime_options,
                       MCRComponentState.MCC_AllMatlab_mcr_pref_dir,
                       MCRComponentState.MCC_AllMatlab_set_warning_state,
                       ctfFilePath, embeddedCtfStream, true);
      }
      else
      {
        throw new ApplicationException("MWArray assembly could not be initialized");
      }
    }


    /// <summary>
    /// Constructs a new instance of the projections class.
    /// </summary>
    public projections()
    {
    }


    #endregion Constructors

    #region Finalize

    /// <summary internal= "true">
    /// Class destructor called by the CLR garbage collector.
    /// </summary>
    ~projections()
    {
      Dispose(false);
    }


    /// <summary>
    /// Frees the native resources associated with this object
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      GC.SuppressFinalize(this);
    }


    /// <summary internal= "true">
    /// Internal dispose function
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        disposed= true;

        if (disposing)
        {
          // Free managed resources;
        }

        // Free native resources
      }
    }


    #endregion Finalize

    #region Methods

    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ipca()
    {
      return mcr.EvaluateFunction("ipca", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="pc_data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ipca(Object pc_data)
    {
      return mcr.EvaluateFunction("ipca", pc_data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="pc_data">Input argument #1</param>
    /// <param name="paxis">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ipca(Object pc_data, Object paxis)
    {
      return mcr.EvaluateFunction("ipca", pc_data, paxis);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ipca(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "ipca", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="pc_data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ipca(int numArgsOut, Object pc_data)
    {
      return mcr.EvaluateFunction(numArgsOut, "ipca", pc_data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="pc_data">Input argument #1</param>
    /// <param name="paxis">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ipca(int numArgsOut, Object pc_data, Object paxis)
    {
      return mcr.EvaluateFunction(numArgsOut, "ipca", pc_data, paxis);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pc_evectors()
    {
      return mcr.EvaluateFunction("pc_evectors", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <param name="A">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pc_evectors(Object A)
    {
      return mcr.EvaluateFunction("pc_evectors", A);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <param name="A">Input argument #1</param>
    /// <param name="numvecs">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pc_evectors(Object A, Object numvecs)
    {
      return mcr.EvaluateFunction("pc_evectors", A, numvecs);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pc_evectors(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "pc_evectors", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="A">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pc_evectors(int numArgsOut, Object A)
    {
      return mcr.EvaluateFunction(numArgsOut, "pc_evectors", A);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="A">Input argument #1</param>
    /// <param name="numvecs">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pc_evectors(int numArgsOut, Object A, Object numvecs)
    {
      return mcr.EvaluateFunction(numArgsOut, "pc_evectors", A, numvecs);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pca2()
    {
      return mcr.EvaluateFunction("pca2", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <param name="a">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pca2(Object a)
    {
      return mcr.EvaluateFunction("pca2", a);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <param name="a">Input argument #1</param>
    /// <param name="center">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pca2(Object a, Object center)
    {
      return mcr.EvaluateFunction("pca2", a, center);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pca2(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "pca2", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="a">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pca2(int numArgsOut, Object a)
    {
      return mcr.EvaluateFunction(numArgsOut, "pca2", a);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="a">Input argument #1</param>
    /// <param name="center">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pca2(int numArgsOut, Object a, Object center)
    {
      return mcr.EvaluateFunction(numArgsOut, "pca2", a, center);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runPCA()
    {
      return mcr.EvaluateFunction("runPCA", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runPCA(Object data)
    {
      return mcr.EvaluateFunction("runPCA", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="projectOnto">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runPCA(Object data, Object projectOnto)
    {
      return mcr.EvaluateFunction("runPCA", data, projectOnto);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="projectOnto">Input argument #2</param>
    /// <param name="maxVectors">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runPCA(Object data, Object projectOnto, Object maxVectors)
    {
      return mcr.EvaluateFunction("runPCA", data, projectOnto, maxVectors);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runPCA(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "runPCA", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runPCA(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "runPCA", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="projectOnto">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runPCA(int numArgsOut, Object data, Object projectOnto)
    {
      return mcr.EvaluateFunction(numArgsOut, "runPCA", data, projectOnto);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="projectOnto">Input argument #2</param>
    /// <param name="maxVectors">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runPCA(int numArgsOut, Object data, Object projectOnto, Object 
                     maxVectors)
    {
      return mcr.EvaluateFunction(numArgsOut, "runPCA", data, projectOnto, maxVectors);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object sortem()
    {
      return mcr.EvaluateFunction("sortem", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <param name="vectors_in1">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object sortem(Object vectors_in1)
    {
      return mcr.EvaluateFunction("sortem", vectors_in1);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <param name="vectors_in1">Input argument #1</param>
    /// <param name="values_in1">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object sortem(Object vectors_in1, Object values_in1)
    {
      return mcr.EvaluateFunction("sortem", vectors_in1, values_in1);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] sortem(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "sortem", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="vectors_in1">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] sortem(int numArgsOut, Object vectors_in1)
    {
      return mcr.EvaluateFunction(numArgsOut, "sortem", vectors_in1);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="vectors_in1">Input argument #1</param>
    /// <param name="values_in1">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] sortem(int numArgsOut, Object vectors_in1, Object values_in1)
    {
      return mcr.EvaluateFunction(numArgsOut, "sortem", vectors_in1, values_in1);
    }


    /// <summary>
    /// This method will cause a MATLAB figure window to behave as a modal dialog box.
    /// The method will not return until all the figure windows associated with this
    /// component have been closed.
    /// </summary>
    /// <remarks>
    /// An application should only call this method when required to keep the
    /// MATLAB figure window from disappearing.  Other techniques, such as calling
    /// Console.ReadLine() from the application should be considered where
    /// possible.</remarks>
    ///
    public void WaitForFiguresToDie()
    {
      mcr.WaitForFiguresToDie();
    }



    #endregion Methods

    #region Class Members

    private static MWMCR mcr= null;

    private bool disposed= false;

    #endregion Class Members
  }
}
