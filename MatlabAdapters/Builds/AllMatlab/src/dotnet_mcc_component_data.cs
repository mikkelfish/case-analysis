//
// MATLAB Compiler: 4.11 (R2009b)
// Date: Thu Sep 30 18:06:50 2010
// Arguments: "-B" "macro_default" "-W" "dotnet:AllMatlab,All,0.0,private" "-d"
// "C:\Users\Mikkel Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\AllMatlab\src" "-T" "link:lib" "-v"
// "class{All:C:\Users\Mikkel Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\frequency
// tools\BandButterworthFilter.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\coupling\BPfilter.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
// tools\ButterworthFilter.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_coupling.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_crossfreq_coupling.m,C:\Users\Mi
// kkel Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
// theory\MutInf.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\filtering\my_filt.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\visualization\plotColor.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\visualization\plotHistograms.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
// tools\powerSpectrum.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ratBP.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\filtering\resampleData.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbert.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbertCross.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
// theory\RunMultiOrder.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
// theory\RunSingle.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
// theory\ShannonEntropy.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio
// 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m,C:\Users\Mikkel
// Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
// theory\titration.m}" 
//


using System;

namespace MathWorks.MATLAB.NET.ComponentData
{
  /// <summary>
  /// The MCR Component State
  /// </summary>
  public class MCRComponentState
  {
      /// <summary>
      /// The .NET Builder component name
      /// </summary>
      public static string MCC_AllMatlab_name_data= "AllMatlab";

      /// <summary>
      /// The component root data
      /// </summary>
      public static string MCC_AllMatlab_root_data= "";

      /// <summary>
      /// The public encryption key for the .NET Builder component
      /// </summary>
      public static byte[] MCC_AllMatlab_public_data=
                              {(byte)'3', (byte)'0', (byte)'8', (byte)'1',
                               (byte)'9', (byte)'D', (byte)'3', (byte)'0',
                               (byte)'0', (byte)'D', (byte)'0', (byte)'6',
                               (byte)'0', (byte)'9', (byte)'2', (byte)'A',
                               (byte)'8', (byte)'6', (byte)'4', (byte)'8',
                               (byte)'8', (byte)'6', (byte)'F', (byte)'7',
                               (byte)'0', (byte)'D', (byte)'0', (byte)'1',
                               (byte)'0', (byte)'1', (byte)'0', (byte)'1',
                               (byte)'0', (byte)'5', (byte)'0', (byte)'0',
                               (byte)'0', (byte)'3', (byte)'8', (byte)'1',
                               (byte)'8', (byte)'B', (byte)'0', (byte)'0',
                               (byte)'3', (byte)'0', (byte)'8', (byte)'1',
                               (byte)'8', (byte)'7', (byte)'0', (byte)'2',
                               (byte)'8', (byte)'1', (byte)'8', (byte)'1',
                               (byte)'0', (byte)'0', (byte)'C', (byte)'4',
                               (byte)'9', (byte)'C', (byte)'A', (byte)'C',
                               (byte)'3', (byte)'4', (byte)'E', (byte)'D',
                               (byte)'1', (byte)'3', (byte)'A', (byte)'5',
                               (byte)'2', (byte)'0', (byte)'6', (byte)'5',
                               (byte)'8', (byte)'F', (byte)'6', (byte)'F',
                               (byte)'8', (byte)'E', (byte)'0', (byte)'1',
                               (byte)'3', (byte)'8', (byte)'C', (byte)'4',
                               (byte)'3', (byte)'1', (byte)'5', (byte)'B',
                               (byte)'4', (byte)'3', (byte)'1', (byte)'5',
                               (byte)'2', (byte)'7', (byte)'7', (byte)'E',
                               (byte)'D', (byte)'3', (byte)'F', (byte)'7',
                               (byte)'D', (byte)'A', (byte)'E', (byte)'5',
                               (byte)'3', (byte)'0', (byte)'9', (byte)'9',
                               (byte)'D', (byte)'B', (byte)'0', (byte)'8',
                               (byte)'E', (byte)'E', (byte)'5', (byte)'8',
                               (byte)'9', (byte)'F', (byte)'8', (byte)'0',
                               (byte)'4', (byte)'D', (byte)'4', (byte)'B',
                               (byte)'9', (byte)'8', (byte)'1', (byte)'3',
                               (byte)'2', (byte)'6', (byte)'A', (byte)'5',
                               (byte)'2', (byte)'C', (byte)'C', (byte)'E',
                               (byte)'4', (byte)'3', (byte)'8', (byte)'2',
                               (byte)'E', (byte)'9', (byte)'F', (byte)'2',
                               (byte)'B', (byte)'4', (byte)'D', (byte)'0',
                               (byte)'8', (byte)'5', (byte)'E', (byte)'B',
                               (byte)'9', (byte)'5', (byte)'0', (byte)'C',
                               (byte)'7', (byte)'A', (byte)'B', (byte)'1',
                               (byte)'2', (byte)'E', (byte)'D', (byte)'E',
                               (byte)'2', (byte)'D', (byte)'4', (byte)'1',
                               (byte)'2', (byte)'9', (byte)'7', (byte)'8',
                               (byte)'2', (byte)'0', (byte)'E', (byte)'6',
                               (byte)'3', (byte)'7', (byte)'7', (byte)'A',
                               (byte)'5', (byte)'F', (byte)'E', (byte)'B',
                               (byte)'5', (byte)'6', (byte)'8', (byte)'9',
                               (byte)'D', (byte)'4', (byte)'E', (byte)'6',
                               (byte)'0', (byte)'3', (byte)'2', (byte)'F',
                               (byte)'6', (byte)'0', (byte)'C', (byte)'4',
                               (byte)'3', (byte)'0', (byte)'7', (byte)'4',
                               (byte)'A', (byte)'0', (byte)'4', (byte)'C',
                               (byte)'2', (byte)'6', (byte)'A', (byte)'B',
                               (byte)'7', (byte)'2', (byte)'F', (byte)'5',
                               (byte)'4', (byte)'B', (byte)'5', (byte)'1',
                               (byte)'B', (byte)'B', (byte)'4', (byte)'6',
                               (byte)'0', (byte)'5', (byte)'7', (byte)'8',
                               (byte)'7', (byte)'8', (byte)'5', (byte)'B',
                               (byte)'1', (byte)'9', (byte)'9', (byte)'0',
                               (byte)'1', (byte)'4', (byte)'3', (byte)'1',
                               (byte)'4', (byte)'A', (byte)'6', (byte)'5',
                               (byte)'F', (byte)'0', (byte)'9', (byte)'0',
                               (byte)'B', (byte)'6', (byte)'1', (byte)'F',
                               (byte)'C', (byte)'2', (byte)'0', (byte)'1',
                               (byte)'6', (byte)'9', (byte)'4', (byte)'5',
                               (byte)'3', (byte)'B', (byte)'5', (byte)'8',
                               (byte)'F', (byte)'C', (byte)'8', (byte)'B',
                               (byte)'A', (byte)'4', (byte)'3', (byte)'E',
                               (byte)'6', (byte)'7', (byte)'7', (byte)'6',
                               (byte)'E', (byte)'B', (byte)'7', (byte)'E',
                               (byte)'C', (byte)'D', (byte)'3', (byte)'1',
                               (byte)'7', (byte)'8', (byte)'B', (byte)'5',
                               (byte)'6', (byte)'A', (byte)'B', (byte)'0',
                               (byte)'F', (byte)'A', (byte)'0', (byte)'6',
                               (byte)'D', (byte)'D', (byte)'6', (byte)'4',
                               (byte)'9', (byte)'6', (byte)'7', (byte)'C',
                               (byte)'B', (byte)'1', (byte)'4', (byte)'9',
                               (byte)'E', (byte)'5', (byte)'0', (byte)'2',
                               (byte)'0', (byte)'1', (byte)'1', (byte)'1'};

      /// <summary>
      /// The session encryption key for the .NET Builder component
      /// </summary>
      public static byte[] MCC_AllMatlab_session_data=
                              {(byte)'9', (byte)'8', (byte)'5', (byte)'B',
                               (byte)'5', (byte)'4', (byte)'0', (byte)'5',
                               (byte)'6', (byte)'D', (byte)'F', (byte)'6',
                               (byte)'7', (byte)'8', (byte)'8', (byte)'6',
                               (byte)'7', (byte)'A', (byte)'8', (byte)'5',
                               (byte)'1', (byte)'7', (byte)'2', (byte)'6',
                               (byte)'0', (byte)'B', (byte)'A', (byte)'A',
                               (byte)'7', (byte)'B', (byte)'5', (byte)'D',
                               (byte)'8', (byte)'5', (byte)'9', (byte)'1',
                               (byte)'A', (byte)'4', (byte)'2', (byte)'E',
                               (byte)'D', (byte)'7', (byte)'E', (byte)'B',
                               (byte)'4', (byte)'8', (byte)'1', (byte)'7',
                               (byte)'B', (byte)'9', (byte)'1', (byte)'9',
                               (byte)'4', (byte)'F', (byte)'B', (byte)'A',
                               (byte)'7', (byte)'D', (byte)'2', (byte)'D',
                               (byte)'6', (byte)'5', (byte)'0', (byte)'4',
                               (byte)'A', (byte)'A', (byte)'4', (byte)'B',
                               (byte)'0', (byte)'4', (byte)'E', (byte)'8',
                               (byte)'A', (byte)'E', (byte)'6', (byte)'1',
                               (byte)'2', (byte)'C', (byte)'9', (byte)'8',
                               (byte)'2', (byte)'2', (byte)'F', (byte)'D',
                               (byte)'3', (byte)'D', (byte)'C', (byte)'1',
                               (byte)'C', (byte)'8', (byte)'D', (byte)'5',
                               (byte)'1', (byte)'C', (byte)'1', (byte)'B',
                               (byte)'6', (byte)'3', (byte)'D', (byte)'4',
                               (byte)'0', (byte)'6', (byte)'6', (byte)'3',
                               (byte)'9', (byte)'9', (byte)'9', (byte)'3',
                               (byte)'3', (byte)'4', (byte)'6', (byte)'6',
                               (byte)'B', (byte)'F', (byte)'F', (byte)'A',
                               (byte)'2', (byte)'6', (byte)'5', (byte)'2',
                               (byte)'E', (byte)'9', (byte)'4', (byte)'4',
                               (byte)'D', (byte)'6', (byte)'8', (byte)'3',
                               (byte)'5', (byte)'4', (byte)'4', (byte)'B',
                               (byte)'2', (byte)'0', (byte)'2', (byte)'9',
                               (byte)'F', (byte)'3', (byte)'E', (byte)'0',
                               (byte)'0', (byte)'2', (byte)'3', (byte)'6',
                               (byte)'4', (byte)'9', (byte)'2', (byte)'F',
                               (byte)'1', (byte)'2', (byte)'1', (byte)'F',
                               (byte)'0', (byte)'1', (byte)'F', (byte)'0',
                               (byte)'1', (byte)'1', (byte)'F', (byte)'9',
                               (byte)'C', (byte)'D', (byte)'E', (byte)'6',
                               (byte)'0', (byte)'A', (byte)'7', (byte)'9',
                               (byte)'B', (byte)'7', (byte)'6', (byte)'4',
                               (byte)'7', (byte)'1', (byte)'E', (byte)'1',
                               (byte)'C', (byte)'E', (byte)'B', (byte)'5',
                               (byte)'9', (byte)'C', (byte)'D', (byte)'4',
                               (byte)'F', (byte)'3', (byte)'8', (byte)'E',
                               (byte)'C', (byte)'B', (byte)'D', (byte)'B',
                               (byte)'5', (byte)'0', (byte)'6', (byte)'C',
                               (byte)'3', (byte)'0', (byte)'8', (byte)'5',
                               (byte)'3', (byte)'F', (byte)'C', (byte)'2',
                               (byte)'7', (byte)'C', (byte)'4', (byte)'4',
                               (byte)'4', (byte)'6', (byte)'9', (byte)'B',
                               (byte)'5', (byte)'5', (byte)'1', (byte)'7',
                               (byte)'3', (byte)'E', (byte)'B', (byte)'0',
                               (byte)'8', (byte)'1', (byte)'4', (byte)'6',
                               (byte)'F', (byte)'3', (byte)'D', (byte)'F',
                               (byte)'9', (byte)'9', (byte)'B', (byte)'8',
                               (byte)'2', (byte)'B', (byte)'1', (byte)'6',
                               (byte)'C', (byte)'A', (byte)'C', (byte)'E',
                               (byte)'E', (byte)'3', (byte)'5', (byte)'5',
                               (byte)'0', (byte)'5', (byte)'1', (byte)'9',
                               (byte)'E', (byte)'9', (byte)'4', (byte)'C',
                               (byte)'3', (byte)'5', (byte)'7', (byte)'2'};

      /// <summary>
      /// The MATLAB path for the .NET Builder component
      /// </summary>
      public static string[] MCC_AllMatlab_matlabpath_data= {"AllMatlab/",
                                                             "$TOOLBOXDEPLOYDIR/",
                                                             "Users/Mikkel Fishman/Documents/Visual Studio 2008/mesosoft/MatlabAdapters/Builds/frequency tools/",
                                                             "Users/Mikkel Fishman/Documents/Visual Studio 2008/mesosoft/MatlabAdapters/Builds/coupling/",
                                                             "Users/Mikkel Fishman/Documents/Visual Studio 2008/mesosoft/MatlabAdapters/Builds/surrogates/",
                                                             "Users/Mikkel Fishman/Documents/Visual Studio 2008/mesosoft/MatlabAdapters/Builds/projections/",
                                                             "Users/Mikkel Fishman/Documents/Visual Studio 2008/mesosoft/MatlabAdapters/Builds/information theory/",
                                                             "Users/Mikkel Fishman/Documents/Visual Studio 2008/mesosoft/MatlabAdapters/Builds/filtering/",
                                                             "$TOOLBOXMATLABDIR/general/",
                                                             "$TOOLBOXMATLABDIR/ops/",
                                                             "$TOOLBOXMATLABDIR/lang/",
                                                             "$TOOLBOXMATLABDIR/elmat/",
                                                             "$TOOLBOXMATLABDIR/randfun/",
                                                             "$TOOLBOXMATLABDIR/elfun/",
                                                             "$TOOLBOXMATLABDIR/specfun/",
                                                             "$TOOLBOXMATLABDIR/matfun/",
                                                             "$TOOLBOXMATLABDIR/datafun/",
                                                             "$TOOLBOXMATLABDIR/polyfun/",
                                                             "$TOOLBOXMATLABDIR/funfun/",
                                                             "$TOOLBOXMATLABDIR/sparfun/",
                                                             "$TOOLBOXMATLABDIR/scribe/",
                                                             "$TOOLBOXMATLABDIR/graph2d/",
                                                             "$TOOLBOXMATLABDIR/graph3d/",
                                                             "$TOOLBOXMATLABDIR/specgraph/",
                                                             "$TOOLBOXMATLABDIR/graphics/",
                                                             "$TOOLBOXMATLABDIR/uitools/",
                                                             "$TOOLBOXMATLABDIR/strfun/",
                                                             "$TOOLBOXMATLABDIR/imagesci/",
                                                             "$TOOLBOXMATLABDIR/iofun/",
                                                             "$TOOLBOXMATLABDIR/audiovideo/",
                                                             "$TOOLBOXMATLABDIR/timefun/",
                                                             "$TOOLBOXMATLABDIR/datatypes/",
                                                             "$TOOLBOXMATLABDIR/verctrl/",
                                                             "$TOOLBOXMATLABDIR/codetools/",
                                                             "$TOOLBOXMATLABDIR/helptools/",
                                                             "$TOOLBOXMATLABDIR/winfun/",
                                                             "$TOOLBOXMATLABDIR/winfun/NET/",
                                                             "$TOOLBOXMATLABDIR/demos/",
                                                             "$TOOLBOXMATLABDIR/timeseries/",
                                                             "$TOOLBOXMATLABDIR/hds/",
                                                             "$TOOLBOXMATLABDIR/guide/",
                                                             "$TOOLBOXMATLABDIR/plottools/",
                                                             "toolbox/local/",
                                                             "toolbox/shared/controllib/",
                                                             "toolbox/shared/dastudio/",
                                                             "$TOOLBOXMATLABDIR/datamanager/",
                                                             "toolbox/bioinfo/biodemos/",
                                                             "toolbox/compiler/",
                                                             "toolbox/control/control/",
                                                             "toolbox/control/ctrlguis/",
                                                             "toolbox/control/ctrlobsolete/",
                                                             "toolbox/control/ctrlutil/",
                                                             "toolbox/shared/slcontrollib/",
                                                             "toolbox/shared/spcuilib/",
                                                             "toolbox/shared/siglib/",
                                                             "toolbox/ident/ident/",
                                                             "toolbox/ident/nlident/",
                                                             "toolbox/ident/idobsolete/",
                                                             "toolbox/ident/idutils/",
                                                             "toolbox/images/colorspaces/",
                                                             "toolbox/images/images/",
                                                             "toolbox/images/iptformats/",
                                                             "toolbox/images/iptutils/",
                                                             "toolbox/shared/imageslib/",
                                                             "toolbox/map/map/",
                                                             "toolbox/map/mapdemos/",
                                                             "toolbox/map/mapdisp/",
                                                             "toolbox/map/mapproj/",
                                                             "toolbox/shared/maputils/",
                                                             "toolbox/shared/mapgeodesy/",
                                                             "toolbox/signal/signal/",
                                                             "toolbox/signal/sigtools/",
                                                             "toolbox/sldo/sloptim/sloptguis/",
                                                             "toolbox/stats/"};
      /// <summary>
      /// The MATLAB path count
      /// </summary>
      public static int MCC_AllMatlab_matlabpath_data_count= 74;

      /// <summary>
      /// The class path for the .NET Builder component
      /// </summary>
      public static string[] MCC_AllMatlab_classpath_data= {"java/jar/toolbox/control.jar",
                                                            "java/jar/toolbox/images.jar"};

      /// <summary>
      /// The class path count
      /// </summary>
      public static int MCC_AllMatlab_classpath_data_count= 2;

      /// <summary>
      /// The lib path for the .NET Builder component
      /// </summary>
      public static string[] MCC_AllMatlab_libpath_data= {"bin/win32/"};

      /// <summary>
      /// The lib path count
      /// </summary>
      public static int MCC_AllMatlab_libpath_data_count= 1;

      /// <summary>
      /// The MCR application options
      /// </summary>
      public static string[] MCC_AllMatlab_mcr_application_options= {"\0"};

      /// <summary>
      /// The MCR application options count
      /// </summary>
      public static int MCC_AllMatlab_mcr_application_option_count= 0;

      /// <summary>
      /// The MCR runtime options
      /// </summary>
      public static string[] MCC_AllMatlab_mcr_runtime_options= {"\0"};

      /// <summary>
      /// The MCR runtime options count
      /// </summary>
      public static int MCC_AllMatlab_mcr_runtime_option_count= 0;

      /// <summary>
      /// The component preferences directory
      /// </summary>
      public static string MCC_AllMatlab_mcr_pref_dir= "AllMatlab_22D52FB7609354F6A0895B30DE852F01";

      /// <summary>
      /// Runtime warning states
      /// </summary>
      public static string[] MCC_AllMatlab_set_warning_state= {"off:MATLAB:dispatcher:nameConflict"};
      /// <summary>
      /// Runtime warning state count
      /// </summary>
      public static int MCC_AllMatlab_set_warning_state_count= 0;
  }
}
