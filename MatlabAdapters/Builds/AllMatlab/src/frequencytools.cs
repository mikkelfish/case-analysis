/*
* MATLAB Compiler: 4.11 (R2009b)
* Date: Thu Dec 17 11:52:31 2009
* Arguments: "-B" "macro_default" "-W" "dotnet:AllMatlab,coupling,0.0,private" "-d"
* "C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\AllMatlab\src" "-T" "link:lib" "-v"
* "class{coupling:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\BPfilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_coupling.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_crossfreq_coupling.m,C:\Users\Mik
* kel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbert.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbertCross.m}"
* "class{surrogates:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m}"
* "class{filtering:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\my_filt.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\resampleData.m}"
* "class{frequencytools:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\BandButterworthFilter.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ButterworthFilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\powerSpectrum.m}" "class{informationtheory:C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
* theory\MutInf.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\information theory\titration.m}"
* "class{projections:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m}" 
*/
using System;
using System.Reflection;
using System.IO;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using MathWorks.MATLAB.NET.ComponentData;

namespace AllMatlab
{
  /// <summary>
  /// The frequencytools class provides a CLS compliant, MWArray interface to the
  /// M-functions contained in the files:
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\BandButterworthFilter.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ButterworthFilter.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\powerSpectrum.m
  /// </summary>
  /// <remarks>
  /// @Version 0.0
  /// </remarks>
  public class frequencytools : IDisposable
  {
    #region Constructors

    /// <summary internal= "true">
    /// The static constructor instantiates and initializes the MATLAB Component Runtime
    /// instance.
    /// </summary>
    static frequencytools()
    {
      if (MWMCR.MCRAppInitialized)
      {
        Assembly assembly= Assembly.GetExecutingAssembly();

        string ctfFilePath= assembly.Location;

        int lastDelimiter= ctfFilePath.LastIndexOf(@"\");

        ctfFilePath= ctfFilePath.Remove(lastDelimiter, (ctfFilePath.Length - lastDelimiter));

        string ctfFileName = MCRComponentState.MCC_AllMatlab_name_data + ".ctf";

        Stream embeddedCtfStream = null;

        String[] resourceStrings = assembly.GetManifestResourceNames();

        foreach (String name in resourceStrings)
        {
          if (name.Contains(ctfFileName))
          {
            embeddedCtfStream = assembly.GetManifestResourceStream(name);
            break;
          }
        }
        mcr= new MWMCR(MCRComponentState.MCC_AllMatlab_name_data,
                       MCRComponentState.MCC_AllMatlab_root_data,
                       MCRComponentState.MCC_AllMatlab_public_data,
                       MCRComponentState.MCC_AllMatlab_session_data,
                       MCRComponentState.MCC_AllMatlab_matlabpath_data,
                       MCRComponentState.MCC_AllMatlab_classpath_data,
                       MCRComponentState.MCC_AllMatlab_libpath_data,
                       MCRComponentState.MCC_AllMatlab_mcr_application_options,
                       MCRComponentState.MCC_AllMatlab_mcr_runtime_options,
                       MCRComponentState.MCC_AllMatlab_mcr_pref_dir,
                       MCRComponentState.MCC_AllMatlab_set_warning_state,
                       ctfFilePath, embeddedCtfStream, true);
      }
      else
      {
        throw new ApplicationException("MWArray assembly could not be initialized");
      }
    }


    /// <summary>
    /// Constructs a new instance of the frequencytools class.
    /// </summary>
    public frequencytools()
    {
    }


    #endregion Constructors

    #region Finalize

    /// <summary internal= "true">
    /// Class destructor called by the CLR garbage collector.
    /// </summary>
    ~frequencytools()
    {
      Dispose(false);
    }


    /// <summary>
    /// Frees the native resources associated with this object
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      GC.SuppressFinalize(this);
    }


    /// <summary internal= "true">
    /// Internal dispose function
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        disposed= true;

        if (disposing)
        {
          // Free managed resources;
        }

        // Free native resources
      }
    }


    #endregion Finalize

    #region Methods

    /// <summary>
    /// Provides a single output, 0-input MWArrayinterface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray BandButterworthFilter()
    {
      return mcr.EvaluateFunction("BandButterworthFilter", new MWArray[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input MWArrayinterface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray BandButterworthFilter(MWArray Data)
    {
      return mcr.EvaluateFunction("BandButterworthFilter", Data);
    }


    /// <summary>
    /// Provides a single output, 2-input MWArrayinterface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray BandButterworthFilter(MWArray Data, MWArray SamplingRate)
    {
      return mcr.EvaluateFunction("BandButterworthFilter", Data, SamplingRate);
    }


    /// <summary>
    /// Provides a single output, 3-input MWArrayinterface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray BandButterworthFilter(MWArray Data, MWArray SamplingRate, MWArray 
                                   FilterCutOff)
    {
      return mcr.EvaluateFunction("BandButterworthFilter", Data, SamplingRate, FilterCutOff);
    }


    /// <summary>
    /// Provides a single output, 4-input MWArrayinterface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <param name="order">Input argument #4</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray BandButterworthFilter(MWArray Data, MWArray SamplingRate, MWArray 
                                   FilterCutOff, MWArray order)
    {
      return mcr.EvaluateFunction("BandButterworthFilter", Data, SamplingRate, FilterCutOff, order);
    }


    /// <summary>
    /// Provides the standard 0-input MWArray interface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] BandButterworthFilter(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter", new MWArray[]{});
    }


    /// <summary>
    /// Provides the standard 1-input MWArray interface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] BandButterworthFilter(int numArgsOut, MWArray Data)
    {
      return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter", Data);
    }


    /// <summary>
    /// Provides the standard 2-input MWArray interface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] BandButterworthFilter(int numArgsOut, MWArray Data, MWArray 
                                     SamplingRate)
    {
      return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter", Data, SamplingRate);
    }


    /// <summary>
    /// Provides the standard 3-input MWArray interface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] BandButterworthFilter(int numArgsOut, MWArray Data, MWArray 
                                     SamplingRate, MWArray FilterCutOff)
    {
      return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter", Data, SamplingRate, FilterCutOff);
    }


    /// <summary>
    /// Provides the standard 4-input MWArray interface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <param name="order">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] BandButterworthFilter(int numArgsOut, MWArray Data, MWArray 
                                     SamplingRate, MWArray FilterCutOff, MWArray order)
    {
      return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter", Data, SamplingRate, FilterCutOff, order);
    }


    /// <summary>
    /// Provides an interface for the BandButterworthFilter function in which the input
    /// and output
    /// arguments are specified as an array of MWArrays.
    /// </summary>
    /// <remarks>
    /// This method will allocate and return by reference the output argument
    /// array.<newpara></newpara>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return</param>
    /// <param name= "argsOut">Array of MWArray output arguments</param>
    /// <param name= "argsIn">Array of MWArray input arguments</param>
    ///
    public void BandButterworthFilter(int numArgsOut, ref MWArray[] argsOut, MWArray[] 
                            argsIn)
    {
      mcr.EvaluateFunction("BandButterworthFilter", numArgsOut, ref argsOut, argsIn);
    }


    /// <summary>
    /// Provides a single output, 0-input MWArrayinterface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray ButterworthFilter()
    {
      return mcr.EvaluateFunction("ButterworthFilter", new MWArray[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input MWArrayinterface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray ButterworthFilter(MWArray Data)
    {
      return mcr.EvaluateFunction("ButterworthFilter", Data);
    }


    /// <summary>
    /// Provides a single output, 2-input MWArrayinterface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray ButterworthFilter(MWArray Data, MWArray SamplingRate)
    {
      return mcr.EvaluateFunction("ButterworthFilter", Data, SamplingRate);
    }


    /// <summary>
    /// Provides a single output, 3-input MWArrayinterface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray ButterworthFilter(MWArray Data, MWArray SamplingRate, MWArray 
                               FilterCutOff)
    {
      return mcr.EvaluateFunction("ButterworthFilter", Data, SamplingRate, FilterCutOff);
    }


    /// <summary>
    /// Provides a single output, 4-input MWArrayinterface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <param name="filterType">Input argument #4</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray ButterworthFilter(MWArray Data, MWArray SamplingRate, MWArray 
                               FilterCutOff, MWArray filterType)
    {
      return mcr.EvaluateFunction("ButterworthFilter", Data, SamplingRate, FilterCutOff, filterType);
    }


    /// <summary>
    /// Provides the standard 0-input MWArray interface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] ButterworthFilter(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", new MWArray[]{});
    }


    /// <summary>
    /// Provides the standard 1-input MWArray interface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] ButterworthFilter(int numArgsOut, MWArray Data)
    {
      return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", Data);
    }


    /// <summary>
    /// Provides the standard 2-input MWArray interface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] ButterworthFilter(int numArgsOut, MWArray Data, MWArray SamplingRate)
    {
      return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", Data, SamplingRate);
    }


    /// <summary>
    /// Provides the standard 3-input MWArray interface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] ButterworthFilter(int numArgsOut, MWArray Data, MWArray 
                                 SamplingRate, MWArray FilterCutOff)
    {
      return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", Data, SamplingRate, FilterCutOff);
    }


    /// <summary>
    /// Provides the standard 4-input MWArray interface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <param name="filterType">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] ButterworthFilter(int numArgsOut, MWArray Data, MWArray 
                                 SamplingRate, MWArray FilterCutOff, MWArray filterType)
    {
      return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", Data, SamplingRate, FilterCutOff, filterType);
    }


    /// <summary>
    /// Provides an interface for the ButterworthFilter function in which the input and
    /// output
    /// arguments are specified as an array of MWArrays.
    /// </summary>
    /// <remarks>
    /// This method will allocate and return by reference the output argument
    /// array.<newpara></newpara>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return</param>
    /// <param name= "argsOut">Array of MWArray output arguments</param>
    /// <param name= "argsIn">Array of MWArray input arguments</param>
    ///
    public void ButterworthFilter(int numArgsOut, ref MWArray[] argsOut, MWArray[] argsIn)
    {
      mcr.EvaluateFunction("ButterworthFilter", numArgsOut, ref argsOut, argsIn);
    }


    /// <summary>
    /// Provides a single output, 0-input MWArrayinterface to the powerSpectrum
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray powerSpectrum()
    {
      return mcr.EvaluateFunction("powerSpectrum", new MWArray[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input MWArrayinterface to the powerSpectrum
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y_in1">Input argument #1</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray powerSpectrum(MWArray y_in1)
    {
      return mcr.EvaluateFunction("powerSpectrum", y_in1);
    }


    /// <summary>
    /// Provides a single output, 2-input MWArrayinterface to the powerSpectrum
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y_in1">Input argument #1</param>
    /// <param name="Fs">Input argument #2</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray powerSpectrum(MWArray y_in1, MWArray Fs)
    {
      return mcr.EvaluateFunction("powerSpectrum", y_in1, Fs);
    }


    /// <summary>
    /// Provides the standard 0-input MWArray interface to the powerSpectrum M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] powerSpectrum(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "powerSpectrum", new MWArray[]{});
    }


    /// <summary>
    /// Provides the standard 1-input MWArray interface to the powerSpectrum M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y_in1">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] powerSpectrum(int numArgsOut, MWArray y_in1)
    {
      return mcr.EvaluateFunction(numArgsOut, "powerSpectrum", y_in1);
    }


    /// <summary>
    /// Provides the standard 2-input MWArray interface to the powerSpectrum M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y_in1">Input argument #1</param>
    /// <param name="Fs">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] powerSpectrum(int numArgsOut, MWArray y_in1, MWArray Fs)
    {
      return mcr.EvaluateFunction(numArgsOut, "powerSpectrum", y_in1, Fs);
    }


    /// <summary>
    /// Provides an interface for the powerSpectrum function in which the input and
    /// output
    /// arguments are specified as an array of MWArrays.
    /// </summary>
    /// <remarks>
    /// This method will allocate and return by reference the output argument
    /// array.<newpara></newpara>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return</param>
    /// <param name= "argsOut">Array of MWArray output arguments</param>
    /// <param name= "argsIn">Array of MWArray input arguments</param>
    ///
    public void powerSpectrum(int numArgsOut, ref MWArray[] argsOut, MWArray[] argsIn)
    {
      mcr.EvaluateFunction("powerSpectrum", numArgsOut, ref argsOut, argsIn);
    }


    /// <summary>
    /// This method will cause a MATLAB figure window to behave as a modal dialog box.
    /// The method will not return until all the figure windows associated with this
    /// component have been closed.
    /// </summary>
    /// <remarks>
    /// An application should only call this method when required to keep the
    /// MATLAB figure window from disappearing.  Other techniques, such as calling
    /// Console.ReadLine() from the application should be considered where
    /// possible.</remarks>
    ///
    public void WaitForFiguresToDie()
    {
      mcr.WaitForFiguresToDie();
    }



    #endregion Methods

    #region Class Members

    private static MWMCR mcr= null;

    private bool disposed= false;

    #endregion Class Members
  }
}
