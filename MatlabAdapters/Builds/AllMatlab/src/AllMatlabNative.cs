/*
* MATLAB Compiler: 4.10 (R2009a)
* Date: Tue Nov 10 18:55:18 2009
* Arguments: "-B" "macro_default" "-W" "dotnet:AllMatlab,AllMatlab,0.0,private" "-d"
* "C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\AllMatlab\src" "-T" "link:lib" "-v"
* "class{AllMatlab:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\BandButterworthFilter.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ButterworthFilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\powerSpectrum.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\my_filt.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\coupling.m}" 
*/

using System;
using System.Reflection;
using System.IO;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using MathWorks.MATLAB.NET.ComponentData;
#if SHARED
[assembly: System.Reflection.AssemblyKeyFile(@"")]
#endif

namespace AllMatlabNative
{
  /// <summary>
  /// The AllMatlab class provides a CLS compliant, Object (native) interface to the
  /// M-functions contained in the files:
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\BandButterworthFilter.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ButterworthFilter.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\powerSpectrum.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\filtering\my_filt.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\coupling.m
  /// <newpara></newpara>
  /// deployprint.m
  /// <newpara></newpara>
  /// printdlg.m
  /// </summary>
  /// <remarks>
  /// @Version 0.0
  /// </remarks>
  public class AllMatlab : IDisposable
  {
      #region Constructors

      /// <summary internal= "true">
      /// The static constructor instantiates and initializes the MATLAB Component
      /// Runtime instance.
      /// </summary>
      static AllMatlab()
      {
          if (MWMCR.MCRAppInitialized)
          {
              Assembly assembly= Assembly.GetExecutingAssembly();

              string ctfFilePath= assembly.Location;

              int lastDelimiter= ctfFilePath.LastIndexOf(@"\");

              ctfFilePath= ctfFilePath.Remove(lastDelimiter, (ctfFilePath.Length - lastDelimiter));

              string ctfFileName = MCRComponentState.MCC_AllMatlab_name_data + ".ctf";

              Stream embeddedCtfStream = null;

              String[] resourceStrings = assembly.GetManifestResourceNames();

              foreach (String name in resourceStrings)
                {
                  if (name.Contains(ctfFileName))
                    {
                      embeddedCtfStream = assembly.GetManifestResourceStream(name);
                      break;
                    }
                }
              mcr= new MWMCR(MCRComponentState.MCC_AllMatlab_name_data,
                             MCRComponentState.MCC_AllMatlab_root_data,
                             MCRComponentState.MCC_AllMatlab_public_data,
                             MCRComponentState.MCC_AllMatlab_session_data,
                             MCRComponentState.MCC_AllMatlab_matlabpath_data,
                             MCRComponentState.MCC_AllMatlab_classpath_data,
                             MCRComponentState.MCC_AllMatlab_libpath_data,
                             MCRComponentState.MCC_AllMatlab_mcr_application_options,
                             MCRComponentState.MCC_AllMatlab_mcr_runtime_options,
                             MCRComponentState.MCC_AllMatlab_mcr_pref_dir,
                             MCRComponentState.MCC_AllMatlab_set_warning_state,
                             ctfFilePath, embeddedCtfStream, true);
          }
          else
          {
              throw new ApplicationException("MWArray assembly could not be initialized");
          }
      }


      /// <summary>
      /// Constructs a new instance of the AllMatlab class.
      /// </summary>
      public AllMatlab()
      {
      }


      #endregion Constructors

      #region Finalize

      /// <summary internal= "true">
      /// Class destructor called by the CLR garbage collector.
      /// </summary>
      ~AllMatlab()
      {
          Dispose(false);
      }


      /// <summary>
      /// Frees the native resources associated with this object
      /// </summary>
      public void Dispose()
      {
          Dispose(true);

          GC.SuppressFinalize(this);
      }


      /// <summary internal= "true">
      /// Internal dispose function
      /// </summary>
      protected virtual void Dispose(bool disposing)
      {
          if (!disposed)
          {
              disposed= true;

              if (disposing)
              {
                  // Free managed resources;
              }

              // Free native resources
          }
      }


      #endregion Finalize

      #region Methods

      /// <summary>
      /// Provides a single output, 0-input Object interface to the find_down M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object find_down()
      {
          return mcr.EvaluateFunction("find_down", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the find_down M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object find_down(Object y)
      {
          return mcr.EvaluateFunction("find_down", y);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the find_down M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="amp">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object find_down(Object y, Object amp)
      {
          return mcr.EvaluateFunction("find_down", y, amp);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the find_down M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] find_down(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "find_down", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the find_down M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] find_down(int numArgsOut, Object y)
      {
          return mcr.EvaluateFunction(numArgsOut, "find_down", y);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the find_down M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="amp">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] find_down(int numArgsOut, Object y, Object amp)
      {
          return mcr.EvaluateFunction(numArgsOut, "find_down", y, amp);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the find_up M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object find_up()
      {
          return mcr.EvaluateFunction("find_up", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the find_up M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object find_up(Object y)
      {
          return mcr.EvaluateFunction("find_up", y);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the find_up M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="amp">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object find_up(Object y, Object amp)
      {
          return mcr.EvaluateFunction("find_up", y, amp);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the find_up M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] find_up(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "find_up", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the find_up M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] find_up(int numArgsOut, Object y)
      {
          return mcr.EvaluateFunction(numArgsOut, "find_up", y);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the find_up M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="amp">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] find_up(int numArgsOut, Object y, Object amp)
      {
          return mcr.EvaluateFunction(numArgsOut, "find_up", y, amp);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the ItSurrDat M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// ItSurrDat.m
      /// usage: [SD,efinal]=ItSurrDat(data,M)
      /// Generates surrogate data set of the same length and with the same 
      /// linear statistical properties as data.  The distribution, 
      /// autocorrelation, and power spectrum are preserved, while the phase is 
      /// iteratively modified.
      /// Inputs:
      /// data        time-series data
      /// M           maximum iterations (default=50)
      /// Outputs:
      /// SD          surrogate data
      /// efinal      error at final iteration
      /// Anatoly Zlotnik, May 2006
      /// Thanks to Farhad Kaffashi for suggestions
      /// [1] Zlotnik, A., Algorithm Development for Modeling and 
      /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
      /// Western Reserve University, June 2006 
      /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
      /// Physica, 142:3; 2000.
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ItSurrDat()
      {
          return mcr.EvaluateFunction("ItSurrDat", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the ItSurrDat M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// ItSurrDat.m
      /// usage: [SD,efinal]=ItSurrDat(data,M)
      /// Generates surrogate data set of the same length and with the same 
      /// linear statistical properties as data.  The distribution, 
      /// autocorrelation, and power spectrum are preserved, while the phase is 
      /// iteratively modified.
      /// Inputs:
      /// data        time-series data
      /// M           maximum iterations (default=50)
      /// Outputs:
      /// SD          surrogate data
      /// efinal      error at final iteration
      /// Anatoly Zlotnik, May 2006
      /// Thanks to Farhad Kaffashi for suggestions
      /// [1] Zlotnik, A., Algorithm Development for Modeling and 
      /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
      /// Western Reserve University, June 2006 
      /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
      /// Physica, 142:3; 2000.
      /// </remarks>
      /// <param name="data">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ItSurrDat(Object data)
      {
          return mcr.EvaluateFunction("ItSurrDat", data);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the ItSurrDat M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// ItSurrDat.m
      /// usage: [SD,efinal]=ItSurrDat(data,M)
      /// Generates surrogate data set of the same length and with the same 
      /// linear statistical properties as data.  The distribution, 
      /// autocorrelation, and power spectrum are preserved, while the phase is 
      /// iteratively modified.
      /// Inputs:
      /// data        time-series data
      /// M           maximum iterations (default=50)
      /// Outputs:
      /// SD          surrogate data
      /// efinal      error at final iteration
      /// Anatoly Zlotnik, May 2006
      /// Thanks to Farhad Kaffashi for suggestions
      /// [1] Zlotnik, A., Algorithm Development for Modeling and 
      /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
      /// Western Reserve University, June 2006 
      /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
      /// Physica, 142:3; 2000.
      /// </remarks>
      /// <param name="data">Input argument #1</param>
      /// <param name="M">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ItSurrDat(Object data, Object M)
      {
          return mcr.EvaluateFunction("ItSurrDat", data, M);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the ItSurrDat M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// ItSurrDat.m
      /// usage: [SD,efinal]=ItSurrDat(data,M)
      /// Generates surrogate data set of the same length and with the same 
      /// linear statistical properties as data.  The distribution, 
      /// autocorrelation, and power spectrum are preserved, while the phase is 
      /// iteratively modified.
      /// Inputs:
      /// data        time-series data
      /// M           maximum iterations (default=50)
      /// Outputs:
      /// SD          surrogate data
      /// efinal      error at final iteration
      /// Anatoly Zlotnik, May 2006
      /// Thanks to Farhad Kaffashi for suggestions
      /// [1] Zlotnik, A., Algorithm Development for Modeling and 
      /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
      /// Western Reserve University, June 2006 
      /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
      /// Physica, 142:3; 2000.
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ItSurrDat(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "ItSurrDat", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the ItSurrDat M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// ItSurrDat.m
      /// usage: [SD,efinal]=ItSurrDat(data,M)
      /// Generates surrogate data set of the same length and with the same 
      /// linear statistical properties as data.  The distribution, 
      /// autocorrelation, and power spectrum are preserved, while the phase is 
      /// iteratively modified.
      /// Inputs:
      /// data        time-series data
      /// M           maximum iterations (default=50)
      /// Outputs:
      /// SD          surrogate data
      /// efinal      error at final iteration
      /// Anatoly Zlotnik, May 2006
      /// Thanks to Farhad Kaffashi for suggestions
      /// [1] Zlotnik, A., Algorithm Development for Modeling and 
      /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
      /// Western Reserve University, June 2006 
      /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
      /// Physica, 142:3; 2000.
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ItSurrDat(int numArgsOut, Object data)
      {
          return mcr.EvaluateFunction(numArgsOut, "ItSurrDat", data);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the ItSurrDat M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// ItSurrDat.m
      /// usage: [SD,efinal]=ItSurrDat(data,M)
      /// Generates surrogate data set of the same length and with the same 
      /// linear statistical properties as data.  The distribution, 
      /// autocorrelation, and power spectrum are preserved, while the phase is 
      /// iteratively modified.
      /// Inputs:
      /// data        time-series data
      /// M           maximum iterations (default=50)
      /// Outputs:
      /// SD          surrogate data
      /// efinal      error at final iteration
      /// Anatoly Zlotnik, May 2006
      /// Thanks to Farhad Kaffashi for suggestions
      /// [1] Zlotnik, A., Algorithm Development for Modeling and 
      /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
      /// Western Reserve University, June 2006 
      /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
      /// Physica, 142:3; 2000.
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data">Input argument #1</param>
      /// <param name="M">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ItSurrDat(int numArgsOut, Object data, Object M)
      {
          return mcr.EvaluateFunction(numArgsOut, "ItSurrDat", data, M);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the myss M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// close all;
      /// hist(R,100);
      /// pause();
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object myss()
      {
          return mcr.EvaluateFunction("myss", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the myss M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// close all;
      /// hist(R,100);
      /// pause();
      /// </remarks>
      /// <param name="data">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object myss(Object data)
      {
          return mcr.EvaluateFunction("myss", data);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the myss M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// close all;
      /// hist(R,100);
      /// pause();
      /// </remarks>
      /// <param name="data">Input argument #1</param>
      /// <param name="M">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object myss(Object data, Object M)
      {
          return mcr.EvaluateFunction("myss", data, M);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the myss M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// close all;
      /// hist(R,100);
      /// pause();
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] myss(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "myss", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the myss M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// close all;
      /// hist(R,100);
      /// pause();
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] myss(int numArgsOut, Object data)
      {
          return mcr.EvaluateFunction(numArgsOut, "myss", data);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the myss M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// close all;
      /// hist(R,100);
      /// pause();
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data">Input argument #1</param>
      /// <param name="M">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] myss(int numArgsOut, Object data, Object M)
      {
          return mcr.EvaluateFunction(numArgsOut, "myss", data, M);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the runSurrogate
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object runSurrogate()
      {
          return mcr.EvaluateFunction("runSurrogate", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the runSurrogate
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object runSurrogate(Object y)
      {
          return mcr.EvaluateFunction("runSurrogate", y);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the runSurrogate
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="amp">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object runSurrogate(Object y, Object amp)
      {
          return mcr.EvaluateFunction("runSurrogate", y, amp);
      }


      /// <summary>
      /// Provides a single output, 3-input Object interface to the runSurrogate
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="amp">Input argument #2</param>
      /// <param name="numiters">Input argument #3</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object runSurrogate(Object y, Object amp, Object numiters)
      {
          return mcr.EvaluateFunction("runSurrogate", y, amp, numiters);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the runSurrogate M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] runSurrogate(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut,
                                      "runSurrogate", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the runSurrogate M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] runSurrogate(int numArgsOut, Object y)
      {
          return mcr.EvaluateFunction(numArgsOut, "runSurrogate", y);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the runSurrogate M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="amp">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] runSurrogate(int numArgsOut, Object y, Object amp)
      {
          return mcr.EvaluateFunction(numArgsOut, "runSurrogate", y, amp);
      }


      /// <summary>
      /// Provides the standard 3-input Object interface to the runSurrogate M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="amp">Input argument #2</param>
      /// <param name="numiters">Input argument #3</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] runSurrogate(int numArgsOut, Object y,
                                   Object amp, Object numiters)
      {
          return mcr.EvaluateFunction(numArgsOut, "runSurrogate",
                                      y, amp, numiters);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object surr_1()
      {
          return mcr.EvaluateFunction("surr_1", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object surr_1(Object y)
      {
          return mcr.EvaluateFunction("surr_1", y);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="up">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object surr_1(Object y, Object up)
      {
          return mcr.EvaluateFunction("surr_1", y, up);
      }


      /// <summary>
      /// Provides a single output, 3-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="up">Input argument #2</param>
      /// <param name="dn">Input argument #3</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object surr_1(Object y, Object up, Object dn)
      {
          return mcr.EvaluateFunction("surr_1", y, up, dn);
      }


      /// <summary>
      /// Provides a single output, 4-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="up">Input argument #2</param>
      /// <param name="dn">Input argument #3</param>
      /// <param name="option">Input argument #4</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object surr_1(Object y, Object up, Object dn, Object option)
      {
          return mcr.EvaluateFunction("surr_1", y, up, dn, option);
      }


      /// <summary>
      /// Provides a single output, 5-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="up">Input argument #2</param>
      /// <param name="dn">Input argument #3</param>
      /// <param name="option">Input argument #4</param>
      /// <param name="numiters">Input argument #5</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object surr_1(Object y, Object up, Object dn,
                           Object option, Object numiters)
      {
          return mcr.EvaluateFunction("surr_1", y, up, dn, option, numiters);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] surr_1(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "surr_1", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] surr_1(int numArgsOut, Object y)
      {
          return mcr.EvaluateFunction(numArgsOut, "surr_1", y);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="up">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] surr_1(int numArgsOut, Object y, Object up)
      {
          return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up);
      }


      /// <summary>
      /// Provides the standard 3-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="up">Input argument #2</param>
      /// <param name="dn">Input argument #3</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] surr_1(int numArgsOut, Object y, Object up, Object dn)
      {
          return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up, dn);
      }


      /// <summary>
      /// Provides the standard 4-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="up">Input argument #2</param>
      /// <param name="dn">Input argument #3</param>
      /// <param name="option">Input argument #4</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] surr_1(int numArgsOut, Object y, Object up,
                             Object dn, Object option)
      {
          return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up, dn, option);
      }


      /// <summary>
      /// Provides the standard 5-input Object interface to the surr_1 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// y=y(up(1):length(y));
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="up">Input argument #2</param>
      /// <param name="dn">Input argument #3</param>
      /// <param name="option">Input argument #4</param>
      /// <param name="numiters">Input argument #5</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] surr_1(int numArgsOut, Object y, Object up,
                             Object dn, Object option, Object numiters)
      {
          return mcr.EvaluateFunction(numArgsOut, "surr_1", y,
                                      up, dn, option, numiters);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the ipca M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ipca()
      {
          return mcr.EvaluateFunction("ipca", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the ipca M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="pc_data">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ipca(Object pc_data)
      {
          return mcr.EvaluateFunction("ipca", pc_data);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the ipca M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="pc_data">Input argument #1</param>
      /// <param name="paxis">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ipca(Object pc_data, Object paxis)
      {
          return mcr.EvaluateFunction("ipca", pc_data, paxis);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the ipca M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ipca(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "ipca", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the ipca M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="pc_data">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ipca(int numArgsOut, Object pc_data)
      {
          return mcr.EvaluateFunction(numArgsOut, "ipca", pc_data);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the ipca M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="pc_data">Input argument #1</param>
      /// <param name="paxis">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ipca(int numArgsOut, Object pc_data, Object paxis)
      {
          return mcr.EvaluateFunction(numArgsOut, "ipca", pc_data, paxis);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the pc_evectors
      /// M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
      /// of A, using Turk and Pentland's trick for numrows >> numcols
      /// Returns the eigenvectors as the colums of Vectors and a
      /// vector of ALL the eigenvectors in Values.
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object pc_evectors()
      {
          return mcr.EvaluateFunction("pc_evectors", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the pc_evectors
      /// M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
      /// of A, using Turk and Pentland's trick for numrows >> numcols
      /// Returns the eigenvectors as the colums of Vectors and a
      /// vector of ALL the eigenvectors in Values.
      /// </remarks>
      /// <param name="A">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object pc_evectors(Object A)
      {
          return mcr.EvaluateFunction("pc_evectors", A);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the pc_evectors
      /// M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
      /// of A, using Turk and Pentland's trick for numrows >> numcols
      /// Returns the eigenvectors as the colums of Vectors and a
      /// vector of ALL the eigenvectors in Values.
      /// </remarks>
      /// <param name="A">Input argument #1</param>
      /// <param name="numvecs">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object pc_evectors(Object A, Object numvecs)
      {
          return mcr.EvaluateFunction("pc_evectors", A, numvecs);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the pc_evectors M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
      /// of A, using Turk and Pentland's trick for numrows >> numcols
      /// Returns the eigenvectors as the colums of Vectors and a
      /// vector of ALL the eigenvectors in Values.
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] pc_evectors(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut,
                                      "pc_evectors", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the pc_evectors M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
      /// of A, using Turk and Pentland's trick for numrows >> numcols
      /// Returns the eigenvectors as the colums of Vectors and a
      /// vector of ALL the eigenvectors in Values.
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="A">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] pc_evectors(int numArgsOut, Object A)
      {
          return mcr.EvaluateFunction(numArgsOut, "pc_evectors", A);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the pc_evectors M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
      /// of A, using Turk and Pentland's trick for numrows >> numcols
      /// Returns the eigenvectors as the colums of Vectors and a
      /// vector of ALL the eigenvectors in Values.
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="A">Input argument #1</param>
      /// <param name="numvecs">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] pc_evectors(int numArgsOut, Object A, Object numvecs)
      {
          return mcr.EvaluateFunction(numArgsOut, "pc_evectors", A, numvecs);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the pca2 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// [T,D,msum] = pca2( a, center )
      /// Returns principal components
      /// of xsize x ysize image a as
      /// eigenvalues and eigenvectors.
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object pca2()
      {
          return mcr.EvaluateFunction("pca2", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the pca2 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// [T,D,msum] = pca2( a, center )
      /// Returns principal components
      /// of xsize x ysize image a as
      /// eigenvalues and eigenvectors.
      /// </remarks>
      /// <param name="a">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object pca2(Object a)
      {
          return mcr.EvaluateFunction("pca2", a);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the pca2 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// [T,D,msum] = pca2( a, center )
      /// Returns principal components
      /// of xsize x ysize image a as
      /// eigenvalues and eigenvectors.
      /// </remarks>
      /// <param name="a">Input argument #1</param>
      /// <param name="center">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object pca2(Object a, Object center)
      {
          return mcr.EvaluateFunction("pca2", a, center);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the pca2 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// [T,D,msum] = pca2( a, center )
      /// Returns principal components
      /// of xsize x ysize image a as
      /// eigenvalues and eigenvectors.
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] pca2(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "pca2", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the pca2 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// [T,D,msum] = pca2( a, center )
      /// Returns principal components
      /// of xsize x ysize image a as
      /// eigenvalues and eigenvectors.
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="a">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] pca2(int numArgsOut, Object a)
      {
          return mcr.EvaluateFunction(numArgsOut, "pca2", a);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the pca2 M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// [T,D,msum] = pca2( a, center )
      /// Returns principal components
      /// of xsize x ysize image a as
      /// eigenvalues and eigenvectors.
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="a">Input argument #1</param>
      /// <param name="center">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] pca2(int numArgsOut, Object a, Object center)
      {
          return mcr.EvaluateFunction(numArgsOut, "pca2", a, center);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the runPCA M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object runPCA()
      {
          return mcr.EvaluateFunction("runPCA", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the runPCA M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="data">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object runPCA(Object data)
      {
          return mcr.EvaluateFunction("runPCA", data);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the runPCA M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="data">Input argument #1</param>
      /// <param name="projectOnto">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object runPCA(Object data, Object projectOnto)
      {
          return mcr.EvaluateFunction("runPCA", data, projectOnto);
      }


      /// <summary>
      /// Provides a single output, 3-input Object interface to the runPCA M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="data">Input argument #1</param>
      /// <param name="projectOnto">Input argument #2</param>
      /// <param name="maxVectors">Input argument #3</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object runPCA(Object data, Object projectOnto, Object maxVectors)
      {
          return mcr.EvaluateFunction("runPCA", data, projectOnto, maxVectors);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the runPCA M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] runPCA(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "runPCA", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the runPCA M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] runPCA(int numArgsOut, Object data)
      {
          return mcr.EvaluateFunction(numArgsOut, "runPCA", data);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the runPCA M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data">Input argument #1</param>
      /// <param name="projectOnto">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] runPCA(int numArgsOut, Object data, Object projectOnto)
      {
          return mcr.EvaluateFunction(numArgsOut, "runPCA", data, projectOnto);
      }


      /// <summary>
      /// Provides the standard 3-input Object interface to the runPCA M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data">Input argument #1</param>
      /// <param name="projectOnto">Input argument #2</param>
      /// <param name="maxVectors">Input argument #3</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] runPCA(int numArgsOut, Object data,
                             Object projectOnto, Object maxVectors)
      {
          return mcr.EvaluateFunction(numArgsOut, "runPCA", data,
                                      projectOnto, maxVectors);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the sortem M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// this error message is directly from Matthew Dailey's sortem.m
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object sortem()
      {
          return mcr.EvaluateFunction("sortem", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the sortem M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// this error message is directly from Matthew Dailey's sortem.m
      /// </remarks>
      /// <param name="vectors_in1">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object sortem(Object vectors_in1)
      {
          return mcr.EvaluateFunction("sortem", vectors_in1);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the sortem M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// this error message is directly from Matthew Dailey's sortem.m
      /// </remarks>
      /// <param name="vectors_in1">Input argument #1</param>
      /// <param name="values_in1">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object sortem(Object vectors_in1, Object values_in1)
      {
          return mcr.EvaluateFunction("sortem", vectors_in1, values_in1);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the sortem M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// this error message is directly from Matthew Dailey's sortem.m
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] sortem(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "sortem", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the sortem M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// this error message is directly from Matthew Dailey's sortem.m
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="vectors_in1">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] sortem(int numArgsOut, Object vectors_in1)
      {
          return mcr.EvaluateFunction(numArgsOut, "sortem", vectors_in1);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the sortem M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// this error message is directly from Matthew Dailey's sortem.m
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="vectors_in1">Input argument #1</param>
      /// <param name="values_in1">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] sortem(int numArgsOut, Object vectors_in1,
                             Object values_in1)
      {
          return mcr.EvaluateFunction(numArgsOut, "sortem",
                                      vectors_in1, values_in1);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the BandButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object BandButterworthFilter()
      {
          return mcr.EvaluateFunction("BandButterworthFilter", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the BandButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="Data">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object BandButterworthFilter(Object Data)
      {
          return mcr.EvaluateFunction("BandButterworthFilter", Data);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the BandButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object BandButterworthFilter(Object Data, Object SamplingRate)
      {
          return mcr.EvaluateFunction("BandButterworthFilter",
                                      Data, SamplingRate);
      }


      /// <summary>
      /// Provides a single output, 3-input Object interface to the BandButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <param name="FilterCutOff">Input argument #3</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object BandButterworthFilter(Object Data, Object SamplingRate,
                                          Object FilterCutOff)
      {
          return mcr.EvaluateFunction("BandButterworthFilter", Data,
                                      SamplingRate, FilterCutOff);
      }


      /// <summary>
      /// Provides a single output, 4-input Object interface to the BandButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <param name="FilterCutOff">Input argument #3</param>
      /// <param name="order">Input argument #4</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object BandButterworthFilter(Object Data, Object SamplingRate,
                                          Object FilterCutOff, Object order)
      {
          return mcr.EvaluateFunction("BandButterworthFilter", Data,
                                      SamplingRate, FilterCutOff, order);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the BandButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] BandButterworthFilter(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut,
                                      "BandButterworthFilter", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the BandButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="Data">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] BandButterworthFilter(int numArgsOut, Object Data)
      {
          return mcr.EvaluateFunction(numArgsOut,
                                      "BandButterworthFilter", Data);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the BandButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] BandButterworthFilter(int numArgsOut, Object Data,
                                            Object SamplingRate)
      {
          return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter",
                                      Data, SamplingRate);
      }


      /// <summary>
      /// Provides the standard 3-input Object interface to the BandButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <param name="FilterCutOff">Input argument #3</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] BandButterworthFilter(int numArgsOut, Object Data,
                                            Object SamplingRate,
                                            Object FilterCutOff)
      {
          return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter",
                                      Data, SamplingRate, FilterCutOff);
      }


      /// <summary>
      /// Provides the standard 4-input Object interface to the BandButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <param name="FilterCutOff">Input argument #3</param>
      /// <param name="order">Input argument #4</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] BandButterworthFilter(int numArgsOut, Object Data,
                                            Object SamplingRate,
                                            Object FilterCutOff, Object order)
      {
          return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter",
                                      Data, SamplingRate, FilterCutOff, order);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the ButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ButterworthFilter()
      {
          return mcr.EvaluateFunction("ButterworthFilter", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the ButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="Data">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ButterworthFilter(Object Data)
      {
          return mcr.EvaluateFunction("ButterworthFilter", Data);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the ButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ButterworthFilter(Object Data, Object SamplingRate)
      {
          return mcr.EvaluateFunction("ButterworthFilter", Data, SamplingRate);
      }


      /// <summary>
      /// Provides a single output, 3-input Object interface to the ButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <param name="FilterCutOff">Input argument #3</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ButterworthFilter(Object Data, Object SamplingRate,
                                      Object FilterCutOff)
      {
          return mcr.EvaluateFunction("ButterworthFilter", Data,
                                      SamplingRate, FilterCutOff);
      }


      /// <summary>
      /// Provides a single output, 4-input Object interface to the ButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <param name="FilterCutOff">Input argument #3</param>
      /// <param name="filterType">Input argument #4</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object ButterworthFilter(Object Data, Object SamplingRate,
                                      Object FilterCutOff, Object filterType)
      {
          return mcr.EvaluateFunction("ButterworthFilter", Data, SamplingRate,
                                      FilterCutOff, filterType);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the ButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ButterworthFilter(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut,
                                      "ButterworthFilter", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the ButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="Data">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ButterworthFilter(int numArgsOut, Object Data)
      {
          return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", Data);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the ButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ButterworthFilter(int numArgsOut, Object Data,
                                        Object SamplingRate)
      {
          return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter",
                                      Data, SamplingRate);
      }


      /// <summary>
      /// Provides the standard 3-input Object interface to the ButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <param name="FilterCutOff">Input argument #3</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ButterworthFilter(int numArgsOut, Object Data,
                                        Object SamplingRate,
                                        Object FilterCutOff)
      {
          return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter",
                                      Data, SamplingRate, FilterCutOff);
      }


      /// <summary>
      /// Provides the standard 4-input Object interface to the ButterworthFilter
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="Data">Input argument #1</param>
      /// <param name="SamplingRate">Input argument #2</param>
      /// <param name="FilterCutOff">Input argument #3</param>
      /// <param name="filterType">Input argument #4</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] ButterworthFilter(int numArgsOut, Object Data,
                                        Object SamplingRate,
                                        Object FilterCutOff, Object filterType)
      {
          return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", Data,
                                      SamplingRate, FilterCutOff, filterType);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the powerSpectrum
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object powerSpectrum()
      {
          return mcr.EvaluateFunction("powerSpectrum", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the powerSpectrum
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y_in1">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object powerSpectrum(Object y_in1)
      {
          return mcr.EvaluateFunction("powerSpectrum", y_in1);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the powerSpectrum
      /// M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y_in1">Input argument #1</param>
      /// <param name="Fs">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object powerSpectrum(Object y_in1, Object Fs)
      {
          return mcr.EvaluateFunction("powerSpectrum", y_in1, Fs);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the powerSpectrum M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] powerSpectrum(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut,
                                      "powerSpectrum", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the powerSpectrum M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y_in1">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] powerSpectrum(int numArgsOut, Object y_in1)
      {
          return mcr.EvaluateFunction(numArgsOut, "powerSpectrum", y_in1);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the powerSpectrum M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y_in1">Input argument #1</param>
      /// <param name="Fs">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] powerSpectrum(int numArgsOut, Object y_in1, Object Fs)
      {
          return mcr.EvaluateFunction(numArgsOut, "powerSpectrum", y_in1, Fs);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the my_filt M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object my_filt()
      {
          return mcr.EvaluateFunction("my_filt", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the my_filt M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object my_filt(Object y)
      {
          return mcr.EvaluateFunction("my_filt", y);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the my_filt M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="res">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object my_filt(Object y, Object res)
      {
          return mcr.EvaluateFunction("my_filt", y, res);
      }


      /// <summary>
      /// Provides a single output, 3-input Object interface to the my_filt M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="res">Input argument #2</param>
      /// <param name="f1">Input argument #3</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object my_filt(Object y, Object res, Object f1)
      {
          return mcr.EvaluateFunction("my_filt", y, res, f1);
      }


      /// <summary>
      /// Provides a single output, 4-input Object interface to the my_filt M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="y">Input argument #1</param>
      /// <param name="res">Input argument #2</param>
      /// <param name="f1">Input argument #3</param>
      /// <param name="f2">Input argument #4</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object my_filt(Object y, Object res, Object f1, Object f2)
      {
          return mcr.EvaluateFunction("my_filt", y, res, f1, f2);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the my_filt M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] my_filt(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "my_filt", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the my_filt M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] my_filt(int numArgsOut, Object y)
      {
          return mcr.EvaluateFunction(numArgsOut, "my_filt", y);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the my_filt M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="res">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] my_filt(int numArgsOut, Object y, Object res)
      {
          return mcr.EvaluateFunction(numArgsOut, "my_filt", y, res);
      }


      /// <summary>
      /// Provides the standard 3-input Object interface to the my_filt M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="res">Input argument #2</param>
      /// <param name="f1">Input argument #3</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] my_filt(int numArgsOut, Object y, Object res, Object f1)
      {
          return mcr.EvaluateFunction(numArgsOut, "my_filt", y, res, f1);
      }


      /// <summary>
      /// Provides the standard 4-input Object interface to the my_filt M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="y">Input argument #1</param>
      /// <param name="res">Input argument #2</param>
      /// <param name="f1">Input argument #3</param>
      /// <param name="f2">Input argument #4</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] my_filt(int numArgsOut, Object y,
                              Object res, Object f1, Object f2)
      {
          return mcr.EvaluateFunction(numArgsOut, "my_filt", y, res, f1, f2);
      }


      /// <summary>
      /// Provides a single output, 0-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object coupling()
      {
          return mcr.EvaluateFunction("coupling", new Object[]{});
      }


      /// <summary>
      /// Provides a single output, 1-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="data1">Input argument #1</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object coupling(Object data1)
      {
          return mcr.EvaluateFunction("coupling", data1);
      }


      /// <summary>
      /// Provides a single output, 2-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="data1">Input argument #1</param>
      /// <param name="data2">Input argument #2</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object coupling(Object data1, Object data2)
      {
          return mcr.EvaluateFunction("coupling", data1, data2);
      }


      /// <summary>
      /// Provides a single output, 3-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="data1">Input argument #1</param>
      /// <param name="data2">Input argument #2</param>
      /// <param name="samplingFrequency">Input argument #3</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object coupling(Object data1, Object data2,
                             Object samplingFrequency)
      {
          return mcr.EvaluateFunction("coupling", data1, data2,
                                      samplingFrequency);
      }


      /// <summary>
      /// Provides a single output, 4-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="data1">Input argument #1</param>
      /// <param name="data2">Input argument #2</param>
      /// <param name="samplingFrequency">Input argument #3</param>
      /// <param name="targetFrequency">Input argument #4</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object coupling(Object data1, Object data2,
                             Object samplingFrequency, Object targetFrequency)
      {
          return mcr.EvaluateFunction("coupling", data1, data2,
                                      samplingFrequency, targetFrequency);
      }


      /// <summary>
      /// Provides a single output, 5-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="data1">Input argument #1</param>
      /// <param name="data2">Input argument #2</param>
      /// <param name="samplingFrequency">Input argument #3</param>
      /// <param name="targetFrequency">Input argument #4</param>
      /// <param name="windowLength">Input argument #5</param>
      /// <returns>An Object containing the first output argument.</returns>
      ///
      public Object coupling(Object data1, Object data2,
                             Object samplingFrequency,
                             Object targetFrequency, Object windowLength)
      {
          return mcr.EvaluateFunction("coupling", data1, data2,
                                      samplingFrequency,
                                      targetFrequency, windowLength);
      }


      /// <summary>
      /// Provides the standard 0-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] coupling(int numArgsOut)
      {
          return mcr.EvaluateFunction(numArgsOut, "coupling", new Object[]{});
      }


      /// <summary>
      /// Provides the standard 1-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data1">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] coupling(int numArgsOut, Object data1)
      {
          return mcr.EvaluateFunction(numArgsOut, "coupling", data1);
      }


      /// <summary>
      /// Provides the standard 2-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data1">Input argument #1</param>
      /// <param name="data2">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] coupling(int numArgsOut, Object data1, Object data2)
      {
          return mcr.EvaluateFunction(numArgsOut, "coupling", data1, data2);
      }


      /// <summary>
      /// Provides the standard 3-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data1">Input argument #1</param>
      /// <param name="data2">Input argument #2</param>
      /// <param name="samplingFrequency">Input argument #3</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] coupling(int numArgsOut, Object data1,
                               Object data2, Object samplingFrequency)
      {
          return mcr.EvaluateFunction(numArgsOut, "coupling", data1,
                                      data2, samplingFrequency);
      }


      /// <summary>
      /// Provides the standard 4-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data1">Input argument #1</param>
      /// <param name="data2">Input argument #2</param>
      /// <param name="samplingFrequency">Input argument #3</param>
      /// <param name="targetFrequency">Input argument #4</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] coupling(int numArgsOut, Object data1, Object data2,
                               Object samplingFrequency, Object targetFrequency)
      {
          return mcr.EvaluateFunction(numArgsOut, "coupling", data1, data2,
                                      samplingFrequency, targetFrequency);
      }


      /// <summary>
      /// Provides the standard 5-input Object interface to the coupling M-function.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="data1">Input argument #1</param>
      /// <param name="data2">Input argument #2</param>
      /// <param name="samplingFrequency">Input argument #3</param>
      /// <param name="targetFrequency">Input argument #4</param>
      /// <param name="windowLength">Input argument #5</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public Object[] coupling(int numArgsOut, Object data1, Object data2,
                               Object samplingFrequency,
                               Object targetFrequency, Object windowLength)
      {
          return mcr.EvaluateFunction(numArgsOut, "coupling", data1,
                                      data2, samplingFrequency,
                                      targetFrequency, windowLength);
      }


      /// <summary>
      /// This method will cause a MATLAB figure window to behave as a modal dialog box.
      /// The method will not return until all the figure windows associated with this
      /// component have been closed.
      /// </summary>
      /// <remarks>
      /// An application should only call this method when required to keep the
      /// MATLAB figure window from disappearing.  Other techniques, such as calling
      /// Console.ReadLine() from the application should be considered where
      /// possible.</remarks>
      ///
      public void WaitForFiguresToDie()
      {
          mcr.WaitForFiguresToDie();
      }


      
      #endregion Methods

      #region Class Members

      private static MWMCR mcr= null;

      private bool disposed= false;

      #endregion Class Members
  }
}
