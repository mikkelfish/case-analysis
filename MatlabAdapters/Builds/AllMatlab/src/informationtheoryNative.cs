/*
* MATLAB Compiler: 4.11 (R2009b)
* Date: Thu Dec 17 11:52:31 2009
* Arguments: "-B" "macro_default" "-W" "dotnet:AllMatlab,coupling,0.0,private" "-d"
* "C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\AllMatlab\src" "-T" "link:lib" "-v"
* "class{coupling:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\BPfilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_coupling.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_crossfreq_coupling.m,C:\Users\Mik
* kel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbert.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbertCross.m}"
* "class{surrogates:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m}"
* "class{filtering:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\my_filt.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\resampleData.m}"
* "class{frequencytools:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\BandButterworthFilter.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ButterworthFilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\powerSpectrum.m}" "class{informationtheory:C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
* theory\MutInf.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\information theory\titration.m}"
* "class{projections:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m}" 
*/
using System;
using System.Reflection;
using System.IO;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using MathWorks.MATLAB.NET.ComponentData;

namespace AllMatlabNative
{
  /// <summary>
  /// The informationtheory class provides a CLS compliant, Object (native) interface to
  /// the M-functions contained in the files:
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\information theory\MutInf.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\information theory\titration.m
  /// </summary>
  /// <remarks>
  /// @Version 0.0
  /// </remarks>
  public class informationtheory : IDisposable
  {
    #region Constructors

    /// <summary internal= "true">
    /// The static constructor instantiates and initializes the MATLAB Component Runtime
    /// instance.
    /// </summary>
    static informationtheory()
    {
      if (MWMCR.MCRAppInitialized)
      {
        Assembly assembly= Assembly.GetExecutingAssembly();

        string ctfFilePath= assembly.Location;

        int lastDelimiter= ctfFilePath.LastIndexOf(@"\");

        ctfFilePath= ctfFilePath.Remove(lastDelimiter, (ctfFilePath.Length - lastDelimiter));

        string ctfFileName = MCRComponentState.MCC_AllMatlab_name_data + ".ctf";

        Stream embeddedCtfStream = null;

        String[] resourceStrings = assembly.GetManifestResourceNames();

        foreach (String name in resourceStrings)
        {
          if (name.Contains(ctfFileName))
          {
            embeddedCtfStream = assembly.GetManifestResourceStream(name);
            break;
          }
        }
        mcr= new MWMCR(MCRComponentState.MCC_AllMatlab_name_data,
                       MCRComponentState.MCC_AllMatlab_root_data,
                       MCRComponentState.MCC_AllMatlab_public_data,
                       MCRComponentState.MCC_AllMatlab_session_data,
                       MCRComponentState.MCC_AllMatlab_matlabpath_data,
                       MCRComponentState.MCC_AllMatlab_classpath_data,
                       MCRComponentState.MCC_AllMatlab_libpath_data,
                       MCRComponentState.MCC_AllMatlab_mcr_application_options,
                       MCRComponentState.MCC_AllMatlab_mcr_runtime_options,
                       MCRComponentState.MCC_AllMatlab_mcr_pref_dir,
                       MCRComponentState.MCC_AllMatlab_set_warning_state,
                       ctfFilePath, embeddedCtfStream, true);
      }
      else
      {
        throw new ApplicationException("MWArray assembly could not be initialized");
      }
    }


    /// <summary>
    /// Constructs a new instance of the informationtheory class.
    /// </summary>
    public informationtheory()
    {
    }


    #endregion Constructors

    #region Finalize

    /// <summary internal= "true">
    /// Class destructor called by the CLR garbage collector.
    /// </summary>
    ~informationtheory()
    {
      Dispose(false);
    }


    /// <summary>
    /// Frees the native resources associated with this object
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      GC.SuppressFinalize(this);
    }


    /// <summary internal= "true">
    /// Internal dispose function
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        disposed= true;

        if (disposing)
        {
          // Free managed resources;
        }

        // Free native resources
      }
    }


    #endregion Finalize

    #region Methods

    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object MutInf()
    {
      return mcr.EvaluateFunction("MutInf", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object MutInf(Object data)
    {
      return mcr.EvaluateFunction("MutInf", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="taumax">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object MutInf(Object data, Object taumax)
    {
      return mcr.EvaluateFunction("MutInf", data, taumax);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="taumax">Input argument #2</param>
    /// <param name="nbins">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object MutInf(Object data, Object taumax, Object nbins)
    {
      return mcr.EvaluateFunction("MutInf", data, taumax, nbins);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] MutInf(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "MutInf", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] MutInf(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "MutInf", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="taumax">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] MutInf(int numArgsOut, Object data, Object taumax)
    {
      return mcr.EvaluateFunction(numArgsOut, "MutInf", data, taumax);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="taumax">Input argument #2</param>
    /// <param name="nbins">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] MutInf(int numArgsOut, Object data, Object taumax, Object nbins)
    {
      return mcr.EvaluateFunction(numArgsOut, "MutInf", data, taumax, nbins);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration()
    {
      return mcr.EvaluateFunction("titration", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data)
    {
      return mcr.EvaluateFunction("titration", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit)
    {
      return mcr.EvaluateFunction("titration", data, tit);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit, Object kappa)
    {
      return mcr.EvaluateFunction("titration", data, tit, kappa);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit, Object kappa, Object degree)
    {
      return mcr.EvaluateFunction("titration", data, tit, kappa, degree);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit, Object kappa, Object degree, Object 
                      noise_step)
    {
      return mcr.EvaluateFunction("titration", data, tit, kappa, degree, noise_step);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <param name="pp">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit, Object kappa, Object degree, Object 
                      noise_step, Object pp)
    {
      return mcr.EvaluateFunction("titration", data, tit, kappa, degree, noise_step, pp);
    }


    /// <summary>
    /// Provides a single output, 7-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <param name="pp">Input argument #6</param>
    /// <param name="num_run">Input argument #7</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit, Object kappa, Object degree, Object 
                      noise_step, Object pp, Object num_run)
    {
      return mcr.EvaluateFunction("titration", data, tit, kappa, degree, noise_step, pp, num_run);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit, Object kappa)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit, kappa);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit, Object kappa, 
                        Object degree)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit, kappa, degree);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit, Object kappa, 
                        Object degree, Object noise_step)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit, kappa, degree, noise_step);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <param name="pp">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit, Object kappa, 
                        Object degree, Object noise_step, Object pp)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit, kappa, degree, noise_step, pp);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <param name="pp">Input argument #6</param>
    /// <param name="num_run">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit, Object kappa, 
                        Object degree, Object noise_step, Object pp, Object num_run)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit, kappa, degree, noise_step, pp, num_run);
    }


    /// <summary>
    /// This method will cause a MATLAB figure window to behave as a modal dialog box.
    /// The method will not return until all the figure windows associated with this
    /// component have been closed.
    /// </summary>
    /// <remarks>
    /// An application should only call this method when required to keep the
    /// MATLAB figure window from disappearing.  Other techniques, such as calling
    /// Console.ReadLine() from the application should be considered where
    /// possible.</remarks>
    ///
    public void WaitForFiguresToDie()
    {
      mcr.WaitForFiguresToDie();
    }



    #endregion Methods

    #region Class Members

    private static MWMCR mcr= null;

    private bool disposed= false;

    #endregion Class Members
  }
}
