/*
* MATLAB Compiler: 4.11 (R2009b)
* Date: Thu Dec 17 11:52:31 2009
* Arguments: "-B" "macro_default" "-W" "dotnet:AllMatlab,coupling,0.0,private" "-d"
* "C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\AllMatlab\src" "-T" "link:lib" "-v"
* "class{coupling:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\BPfilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_coupling.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_crossfreq_coupling.m,C:\Users\Mik
* kel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbert.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbertCross.m}"
* "class{surrogates:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m}"
* "class{filtering:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\my_filt.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\resampleData.m}"
* "class{frequencytools:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\BandButterworthFilter.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ButterworthFilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\powerSpectrum.m}" "class{informationtheory:C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
* theory\MutInf.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\information theory\titration.m}"
* "class{projections:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m}" 
*/
using System;
using System.Reflection;
using System.IO;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using MathWorks.MATLAB.NET.ComponentData;

namespace AllMatlabNative
{
  /// <summary>
  /// The surrogates class provides a CLS compliant, Object (native) interface to the
  /// M-functions contained in the files:
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m
  /// </summary>
  /// <remarks>
  /// @Version 0.0
  /// </remarks>
  public class surrogates : IDisposable
  {
    #region Constructors

    /// <summary internal= "true">
    /// The static constructor instantiates and initializes the MATLAB Component Runtime
    /// instance.
    /// </summary>
    static surrogates()
    {
      if (MWMCR.MCRAppInitialized)
      {
        Assembly assembly= Assembly.GetExecutingAssembly();

        string ctfFilePath= assembly.Location;

        int lastDelimiter= ctfFilePath.LastIndexOf(@"\");

        ctfFilePath= ctfFilePath.Remove(lastDelimiter, (ctfFilePath.Length - lastDelimiter));

        string ctfFileName = MCRComponentState.MCC_AllMatlab_name_data + ".ctf";

        Stream embeddedCtfStream = null;

        String[] resourceStrings = assembly.GetManifestResourceNames();

        foreach (String name in resourceStrings)
        {
          if (name.Contains(ctfFileName))
          {
            embeddedCtfStream = assembly.GetManifestResourceStream(name);
            break;
          }
        }
        mcr= new MWMCR(MCRComponentState.MCC_AllMatlab_name_data,
                       MCRComponentState.MCC_AllMatlab_root_data,
                       MCRComponentState.MCC_AllMatlab_public_data,
                       MCRComponentState.MCC_AllMatlab_session_data,
                       MCRComponentState.MCC_AllMatlab_matlabpath_data,
                       MCRComponentState.MCC_AllMatlab_classpath_data,
                       MCRComponentState.MCC_AllMatlab_libpath_data,
                       MCRComponentState.MCC_AllMatlab_mcr_application_options,
                       MCRComponentState.MCC_AllMatlab_mcr_runtime_options,
                       MCRComponentState.MCC_AllMatlab_mcr_pref_dir,
                       MCRComponentState.MCC_AllMatlab_set_warning_state,
                       ctfFilePath, embeddedCtfStream, true);
      }
      else
      {
        throw new ApplicationException("MWArray assembly could not be initialized");
      }
    }


    /// <summary>
    /// Constructs a new instance of the surrogates class.
    /// </summary>
    public surrogates()
    {
    }


    #endregion Constructors

    #region Finalize

    /// <summary internal= "true">
    /// Class destructor called by the CLR garbage collector.
    /// </summary>
    ~surrogates()
    {
      Dispose(false);
    }


    /// <summary>
    /// Frees the native resources associated with this object
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      GC.SuppressFinalize(this);
    }


    /// <summary internal= "true">
    /// Internal dispose function
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        disposed= true;

        if (disposing)
        {
          // Free managed resources;
        }

        // Free native resources
      }
    }


    #endregion Finalize

    #region Methods

    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_down()
    {
      return mcr.EvaluateFunction("find_down", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_down(Object y)
    {
      return mcr.EvaluateFunction("find_down", y);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_down(Object y, Object amp)
    {
      return mcr.EvaluateFunction("find_down", y, amp);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_down(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_down", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_down(int numArgsOut, Object y)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_down", y);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_down(int numArgsOut, Object y, Object amp)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_down", y, amp);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_up()
    {
      return mcr.EvaluateFunction("find_up", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_up(Object y)
    {
      return mcr.EvaluateFunction("find_up", y);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_up(Object y, Object amp)
    {
      return mcr.EvaluateFunction("find_up", y, amp);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_up(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_up", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_up(int numArgsOut, Object y)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_up", y);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_up(int numArgsOut, Object y, Object amp)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_up", y, amp);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ItSurrDat()
    {
      return mcr.EvaluateFunction("ItSurrDat", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ItSurrDat(Object data)
    {
      return mcr.EvaluateFunction("ItSurrDat", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="M">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ItSurrDat(Object data, Object M)
    {
      return mcr.EvaluateFunction("ItSurrDat", data, M);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ItSurrDat(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "ItSurrDat", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ItSurrDat(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "ItSurrDat", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="M">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ItSurrDat(int numArgsOut, Object data, Object M)
    {
      return mcr.EvaluateFunction(numArgsOut, "ItSurrDat", data, M);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object myss()
    {
      return mcr.EvaluateFunction("myss", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object myss(Object data)
    {
      return mcr.EvaluateFunction("myss", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="M">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object myss(Object data, Object M)
    {
      return mcr.EvaluateFunction("myss", data, M);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] myss(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "myss", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] myss(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "myss", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="M">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] myss(int numArgsOut, Object data, Object M)
    {
      return mcr.EvaluateFunction(numArgsOut, "myss", data, M);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runSurrogate()
    {
      return mcr.EvaluateFunction("runSurrogate", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runSurrogate(Object y)
    {
      return mcr.EvaluateFunction("runSurrogate", y);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runSurrogate(Object y, Object amp)
    {
      return mcr.EvaluateFunction("runSurrogate", y, amp);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <param name="numiters">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runSurrogate(Object y, Object amp, Object numiters)
    {
      return mcr.EvaluateFunction("runSurrogate", y, amp, numiters);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runSurrogate(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "runSurrogate", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runSurrogate(int numArgsOut, Object y)
    {
      return mcr.EvaluateFunction(numArgsOut, "runSurrogate", y);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runSurrogate(int numArgsOut, Object y, Object amp)
    {
      return mcr.EvaluateFunction(numArgsOut, "runSurrogate", y, amp);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <param name="numiters">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runSurrogate(int numArgsOut, Object y, Object amp, Object numiters)
    {
      return mcr.EvaluateFunction(numArgsOut, "runSurrogate", y, amp, numiters);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1()
    {
      return mcr.EvaluateFunction("surr_1", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1(Object y)
    {
      return mcr.EvaluateFunction("surr_1", y);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1(Object y, Object up)
    {
      return mcr.EvaluateFunction("surr_1", y, up);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1(Object y, Object up, Object dn)
    {
      return mcr.EvaluateFunction("surr_1", y, up, dn);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <param name="option">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1(Object y, Object up, Object dn, Object option)
    {
      return mcr.EvaluateFunction("surr_1", y, up, dn, option);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <param name="option">Input argument #4</param>
    /// <param name="numiters">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1(Object y, Object up, Object dn, Object option, Object numiters)
    {
      return mcr.EvaluateFunction("surr_1", y, up, dn, option, numiters);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut, Object y)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", y);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut, Object y, Object up)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut, Object y, Object up, Object dn)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up, dn);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <param name="option">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut, Object y, Object up, Object dn, Object option)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up, dn, option);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <param name="option">Input argument #4</param>
    /// <param name="numiters">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut, Object y, Object up, Object dn, Object option, 
                     Object numiters)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up, dn, option, numiters);
    }


    /// <summary>
    /// This method will cause a MATLAB figure window to behave as a modal dialog box.
    /// The method will not return until all the figure windows associated with this
    /// component have been closed.
    /// </summary>
    /// <remarks>
    /// An application should only call this method when required to keep the
    /// MATLAB figure window from disappearing.  Other techniques, such as calling
    /// Console.ReadLine() from the application should be considered where
    /// possible.</remarks>
    ///
    public void WaitForFiguresToDie()
    {
      mcr.WaitForFiguresToDie();
    }



    #endregion Methods

    #region Class Members

    private static MWMCR mcr= null;

    private bool disposed= false;

    #endregion Class Members
  }
}
