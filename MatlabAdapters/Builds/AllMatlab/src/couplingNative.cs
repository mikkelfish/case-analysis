/*
* MATLAB Compiler: 4.11 (R2009b)
* Date: Thu Dec 17 11:52:31 2009
* Arguments: "-B" "macro_default" "-W" "dotnet:AllMatlab,coupling,0.0,private" "-d"
* "C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\AllMatlab\src" "-T" "link:lib" "-v"
* "class{coupling:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\BPfilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_coupling.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_crossfreq_coupling.m,C:\Users\Mik
* kel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbert.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbertCross.m}"
* "class{surrogates:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m}"
* "class{filtering:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\my_filt.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\resampleData.m}"
* "class{frequencytools:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\BandButterworthFilter.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ButterworthFilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\powerSpectrum.m}" "class{informationtheory:C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
* theory\MutInf.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\information theory\titration.m}"
* "class{projections:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m}" 
*/
using System;
using System.Reflection;
using System.IO;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using MathWorks.MATLAB.NET.ComponentData;

#if SHARED
[assembly: System.Reflection.AssemblyKeyFile(@"")]
#endif

namespace AllMatlabNative
{
  /// <summary>
  /// The coupling class provides a CLS compliant, Object (native) interface to the
  /// M-functions contained in the files:
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\BPfilter.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_coupling.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_crossfreq_coupling.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbert.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbertCross.m
  /// <newpara></newpara>
  /// deployprint.m
  /// <newpara></newpara>
  /// printdlg.m
  /// </summary>
  /// <remarks>
  /// @Version 0.0
  /// </remarks>
  public class coupling : IDisposable
  {
    #region Constructors

    /// <summary internal= "true">
    /// The static constructor instantiates and initializes the MATLAB Component Runtime
    /// instance.
    /// </summary>
    static coupling()
    {
      if (MWMCR.MCRAppInitialized)
      {
        Assembly assembly= Assembly.GetExecutingAssembly();

        string ctfFilePath= assembly.Location;

        int lastDelimiter= ctfFilePath.LastIndexOf(@"\");

        ctfFilePath= ctfFilePath.Remove(lastDelimiter, (ctfFilePath.Length - lastDelimiter));

        string ctfFileName = MCRComponentState.MCC_AllMatlab_name_data + ".ctf";

        Stream embeddedCtfStream = null;

        String[] resourceStrings = assembly.GetManifestResourceNames();

        foreach (String name in resourceStrings)
        {
          if (name.Contains(ctfFileName))
          {
            embeddedCtfStream = assembly.GetManifestResourceStream(name);
            break;
          }
        }
        mcr= new MWMCR(MCRComponentState.MCC_AllMatlab_name_data,
                       MCRComponentState.MCC_AllMatlab_root_data,
                       MCRComponentState.MCC_AllMatlab_public_data,
                       MCRComponentState.MCC_AllMatlab_session_data,
                       MCRComponentState.MCC_AllMatlab_matlabpath_data,
                       MCRComponentState.MCC_AllMatlab_classpath_data,
                       MCRComponentState.MCC_AllMatlab_libpath_data,
                       MCRComponentState.MCC_AllMatlab_mcr_application_options,
                       MCRComponentState.MCC_AllMatlab_mcr_runtime_options,
                       MCRComponentState.MCC_AllMatlab_mcr_pref_dir,
                       MCRComponentState.MCC_AllMatlab_set_warning_state,
                       ctfFilePath, embeddedCtfStream, true);
      }
      else
      {
        throw new ApplicationException("MWArray assembly could not be initialized");
      }
    }


    /// <summary>
    /// Constructs a new instance of the coupling class.
    /// </summary>
    public coupling()
    {
    }


    #endregion Constructors

    #region Finalize

    /// <summary internal= "true">
    /// Class destructor called by the CLR garbage collector.
    /// </summary>
    ~coupling()
    {
      Dispose(false);
    }


    /// <summary>
    /// Frees the native resources associated with this object
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      GC.SuppressFinalize(this);
    }


    /// <summary internal= "true">
    /// Internal dispose function
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        disposed= true;

        if (disposing)
        {
          // Free managed resources;
        }

        // Free native resources
      }
    }


    #endregion Finalize

    #region Methods

    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BPfilter()
    {
      return mcr.EvaluateFunction("BPfilter", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="X">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BPfilter(Object X)
    {
      return mcr.EvaluateFunction("BPfilter", X);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="X">Input argument #1</param>
    /// <param name="f1">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BPfilter(Object X, Object f1)
    {
      return mcr.EvaluateFunction("BPfilter", X, f1);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="X">Input argument #1</param>
    /// <param name="f1">Input argument #2</param>
    /// <param name="f2">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BPfilter(Object X, Object f1, Object f2)
    {
      return mcr.EvaluateFunction("BPfilter", X, f1, f2);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BPfilter(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "BPfilter", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="X">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BPfilter(int numArgsOut, Object X)
    {
      return mcr.EvaluateFunction(numArgsOut, "BPfilter", X);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="X">Input argument #1</param>
    /// <param name="f1">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BPfilter(int numArgsOut, Object X, Object f1)
    {
      return mcr.EvaluateFunction(numArgsOut, "BPfilter", X, f1);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="X">Input argument #1</param>
    /// <param name="f1">Input argument #2</param>
    /// <param name="f2">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BPfilter(int numArgsOut, Object X, Object f1, Object f2)
    {
      return mcr.EvaluateFunction(numArgsOut, "BPfilter", X, f1, f2);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling()
    {
      return mcr.EvaluateFunction("multiband_coupling", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x)
    {
      return mcr.EvaluateFunction("multiband_coupling", x);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x, Object w)
    {
      return mcr.EvaluateFunction("multiband_coupling", x, w);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x, Object w, Object f1)
    {
      return mcr.EvaluateFunction("multiband_coupling", x, w, f1);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x, Object w, Object f1, Object f2)
    {
      return mcr.EvaluateFunction("multiband_coupling", x, w, f1, f2);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x, Object w, Object f1, Object f2, Object sp)
    {
      return mcr.EvaluateFunction("multiband_coupling", x, w, f1, f2, sp);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="triggers">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x, Object w, Object f1, Object f2, Object sp, 
                               Object triggers)
    {
      return mcr.EvaluateFunction("multiband_coupling", x, w, f1, f2, sp, triggers);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x, Object w)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x, w);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x, Object w, Object f1)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x, w, f1);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x, Object w, Object f1, 
                                 Object f2)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x, w, f1, f2);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x, Object w, Object f1, 
                                 Object f2, Object sp)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x, w, f1, f2, sp);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="triggers">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x, Object w, Object f1, 
                                 Object f2, Object sp, Object triggers)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x, w, f1, f2, sp, triggers);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling()
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w, Object fcPNA)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w, fcPNA);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w, Object fcPNA, Object 
                                         fcSNA)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w, fcPNA, fcSNA);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w, Object fcPNA, Object 
                                         fcSNA, Object sp)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="bw">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w, Object fcPNA, Object 
                                         fcSNA, Object sp, Object bw)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp, bw);
    }


    /// <summary>
    /// Provides a single output, 7-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="bw">Input argument #6</param>
    /// <param name="triggers">Input argument #7</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w, Object fcPNA, Object 
                                         fcSNA, Object sp, Object bw, Object triggers)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp, bw, triggers);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w, 
                                           Object fcPNA)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w, fcPNA);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w, 
                                           Object fcPNA, Object fcSNA)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w, fcPNA, fcSNA);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w, 
                                           Object fcPNA, Object fcSNA, Object sp)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="bw">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w, 
                                           Object fcPNA, Object fcSNA, Object sp, Object 
                                           bw)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp, bw);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="bw">Input argument #6</param>
    /// <param name="triggers">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w, 
                                           Object fcPNA, Object fcSNA, Object sp, Object 
                                           bw, Object triggers)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp, bw, triggers);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert()
    {
      return mcr.EvaluateFunction("runHilbert", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1)
    {
      return mcr.EvaluateFunction("runHilbert", data1);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq, Object maxFreq)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq);
    }


    /// <summary>
    /// Provides a single output, 7-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq, Object maxFreq, Object freqStep)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep);
    }


    /// <summary>
    /// Provides a single output, 8-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq, Object maxFreq, Object freqStep, 
                       Object freqBand)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand);
    }


    /// <summary>
    /// Provides a single output, 9-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <param name="windowSize">Input argument #9</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq, Object maxFreq, Object freqStep, 
                       Object freqBand, Object windowSize)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand, windowSize);
    }


    /// <summary>
    /// Provides a single output, 10-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <param name="windowSize">Input argument #9</param>
    /// <param name="triggers">Input argument #10</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq, Object maxFreq, Object freqStep, 
                       Object freqBand, Object windowSize, Object triggers)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq, Object maxFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq, Object maxFreq, 
                         Object freqStep)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep);
    }


    /// <summary>
    /// Provides the standard 8-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq, Object maxFreq, 
                         Object freqStep, Object freqBand)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand);
    }


    /// <summary>
    /// Provides the standard 9-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <param name="windowSize">Input argument #9</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq, Object maxFreq, 
                         Object freqStep, Object freqBand, Object windowSize)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand, windowSize);
    }


    /// <summary>
    /// Provides the standard 10-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <param name="windowSize">Input argument #9</param>
    /// <param name="triggers">Input argument #10</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq, Object maxFreq, 
                         Object freqStep, Object freqBand, Object windowSize, Object 
                         triggers)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross()
    {
      return mcr.EvaluateFunction("runHilbertCross", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq);
    }


    /// <summary>
    /// Provides a single output, 7-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq, Object maxFreq)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq);
    }


    /// <summary>
    /// Provides a single output, 8-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq, Object maxFreq, 
                            Object freqStep)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep);
    }


    /// <summary>
    /// Provides a single output, 9-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq, Object maxFreq, 
                            Object freqStep, Object freqBand)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand);
    }


    /// <summary>
    /// Provides a single output, 10-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <param name="windowSize">Input argument #10</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq, Object maxFreq, 
                            Object freqStep, Object freqBand, Object windowSize)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize);
    }


    /// <summary>
    /// Provides a single output, 11-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <param name="windowSize">Input argument #10</param>
    /// <param name="triggers">Input argument #11</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq, Object maxFreq, 
                            Object freqStep, Object freqBand, Object windowSize, Object 
                            triggers)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq, Object maxFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq);
    }


    /// <summary>
    /// Provides the standard 8-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq, Object maxFreq, Object freqStep)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep);
    }


    /// <summary>
    /// Provides the standard 9-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq, Object maxFreq, Object freqStep, Object freqBand)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand);
    }


    /// <summary>
    /// Provides the standard 10-input Object interface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <param name="windowSize">Input argument #10</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq, Object maxFreq, Object freqStep, Object freqBand, 
                              Object windowSize)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize);
    }


    /// <summary>
    /// Provides the standard 11-input Object interface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <param name="windowSize">Input argument #10</param>
    /// <param name="triggers">Input argument #11</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq, Object maxFreq, Object freqStep, Object freqBand, 
                              Object windowSize, Object triggers)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers);
    }


    /// <summary>
    /// This method will cause a MATLAB figure window to behave as a modal dialog box.
    /// The method will not return until all the figure windows associated with this
    /// component have been closed.
    /// </summary>
    /// <remarks>
    /// An application should only call this method when required to keep the
    /// MATLAB figure window from disappearing.  Other techniques, such as calling
    /// Console.ReadLine() from the application should be considered where
    /// possible.</remarks>
    ///
    public void WaitForFiguresToDie()
    {
      mcr.WaitForFiguresToDie();
    }



    #endregion Methods

    #region Class Members

    private static MWMCR mcr= null;

    private bool disposed= false;

    #endregion Class Members
  }
}
