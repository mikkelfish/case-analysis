/*
* MATLAB Compiler: 4.11 (R2009b)
* Date: Thu Sep 30 18:06:50 2010
* Arguments: "-B" "macro_default" "-W" "dotnet:AllMatlab,All,0.0,private" "-d"
* "C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\AllMatlab\src" "-T" "link:lib" "-v"
* "class{All:C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\BandButterworthFilter.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\BPfilter.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\ButterworthFilter.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_coupling.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_crossfreq_coupling.m,C:\Users\Mik
* kel Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
* theory\MutInf.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\my_filt.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\visualization\plotColor.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\visualization\plotHistograms.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\frequency
* tools\powerSpectrum.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ratBP.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\filtering\resampleData.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbert.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbertCross.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
* theory\RunMultiOrder.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
* theory\RunSingle.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
* theory\ShannonEntropy.m,C:\Users\Mikkel Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio
* 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m,C:\Users\Mikkel
* Fishman\Documents\Visual Studio 2008\mesosoft\MatlabAdapters\Builds\information
* theory\titration.m}" 
*/
using System;
using System.Reflection;
using System.IO;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using MathWorks.MATLAB.NET.ComponentData;

#if SHARED
[assembly: System.Reflection.AssemblyKeyFile(@"")]
#endif

namespace AllMatlabNative
{
  /// <summary>
  /// The All class provides a CLS compliant, Object (native) interface to the
  /// M-functions contained in the files:
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\BandButterworthFilter.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\BPfilter.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ButterworthFilter.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_down.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\find_up.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\ipca.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\ItSurrDat.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_coupling.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\multiband_crossfreq_coupling.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\information theory\MutInf.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\filtering\my_filt.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\myss.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\pc_evectors.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\pca2.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\visualization\plotColor.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\visualization\plotHistograms.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\powerSpectrum.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\frequency tools\ratBP.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\filtering\resampleData.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbert.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\coupling\runHilbertCross.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\information theory\RunMultiOrder.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\runPCA.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\information theory\RunSingle.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\runSurrogate.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\information theory\ShannonEntropy.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\projections\sortem.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\surrogates\surr_1.m
  /// <newpara></newpara>
  /// C:\Users\Mikkel Fishman\Documents\Visual Studio
  /// 2008\mesosoft\MatlabAdapters\Builds\information theory\titration.m
  /// <newpara></newpara>
  /// deployprint.m
  /// <newpara></newpara>
  /// printdlg.m
  /// </summary>
  /// <remarks>
  /// @Version 0.0
  /// </remarks>
  public class All : IDisposable
  {
    #region Constructors

    /// <summary internal= "true">
    /// The static constructor instantiates and initializes the MATLAB Component Runtime
    /// instance.
    /// </summary>
    static All()
    {
      if (MWMCR.MCRAppInitialized)
      {
        Assembly assembly= Assembly.GetExecutingAssembly();

        string ctfFilePath= assembly.Location;

        int lastDelimiter= ctfFilePath.LastIndexOf(@"\");

        ctfFilePath= ctfFilePath.Remove(lastDelimiter, (ctfFilePath.Length - lastDelimiter));

        string ctfFileName = MCRComponentState.MCC_AllMatlab_name_data + ".ctf";

        Stream embeddedCtfStream = null;

        String[] resourceStrings = assembly.GetManifestResourceNames();

        foreach (String name in resourceStrings)
        {
          if (name.Contains(ctfFileName))
          {
            embeddedCtfStream = assembly.GetManifestResourceStream(name);
            break;
          }
        }
        mcr= new MWMCR(MCRComponentState.MCC_AllMatlab_name_data,
                       MCRComponentState.MCC_AllMatlab_root_data,
                       MCRComponentState.MCC_AllMatlab_public_data,
                       MCRComponentState.MCC_AllMatlab_session_data,
                       MCRComponentState.MCC_AllMatlab_matlabpath_data,
                       MCRComponentState.MCC_AllMatlab_classpath_data,
                       MCRComponentState.MCC_AllMatlab_libpath_data,
                       MCRComponentState.MCC_AllMatlab_mcr_application_options,
                       MCRComponentState.MCC_AllMatlab_mcr_runtime_options,
                       MCRComponentState.MCC_AllMatlab_mcr_pref_dir,
                       MCRComponentState.MCC_AllMatlab_set_warning_state,
                       ctfFilePath, embeddedCtfStream, true);
      }
      else
      {
        throw new ApplicationException("MWArray assembly could not be initialized");
      }
    }


    /// <summary>
    /// Constructs a new instance of the All class.
    /// </summary>
    public All()
    {
    }


    #endregion Constructors

    #region Finalize

    /// <summary internal= "true">
    /// Class destructor called by the CLR garbage collector.
    /// </summary>
    ~All()
    {
      Dispose(false);
    }


    /// <summary>
    /// Frees the native resources associated with this object
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      GC.SuppressFinalize(this);
    }


    /// <summary internal= "true">
    /// Internal dispose function
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        disposed= true;

        if (disposing)
        {
          // Free managed resources;
        }

        // Free native resources
      }
    }


    #endregion Finalize

    #region Methods

    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BandButterworthFilter()
    {
      return mcr.EvaluateFunction("BandButterworthFilter", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BandButterworthFilter(Object Data)
    {
      return mcr.EvaluateFunction("BandButterworthFilter", Data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BandButterworthFilter(Object Data, Object SamplingRate)
    {
      return mcr.EvaluateFunction("BandButterworthFilter", Data, SamplingRate);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BandButterworthFilter(Object Data, Object SamplingRate, Object 
                                  FilterCutOff)
    {
      return mcr.EvaluateFunction("BandButterworthFilter", Data, SamplingRate, FilterCutOff);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <param name="order">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BandButterworthFilter(Object Data, Object SamplingRate, Object 
                                  FilterCutOff, Object order)
    {
      return mcr.EvaluateFunction("BandButterworthFilter", Data, SamplingRate, FilterCutOff, order);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BandButterworthFilter(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BandButterworthFilter(int numArgsOut, Object Data)
    {
      return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter", Data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BandButterworthFilter(int numArgsOut, Object Data, Object 
                                    SamplingRate)
    {
      return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter", Data, SamplingRate);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BandButterworthFilter(int numArgsOut, Object Data, Object 
                                    SamplingRate, Object FilterCutOff)
    {
      return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter", Data, SamplingRate, FilterCutOff);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the BandButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <param name="order">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BandButterworthFilter(int numArgsOut, Object Data, Object 
                                    SamplingRate, Object FilterCutOff, Object order)
    {
      return mcr.EvaluateFunction(numArgsOut, "BandButterworthFilter", Data, SamplingRate, FilterCutOff, order);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BPfilter()
    {
      return mcr.EvaluateFunction("BPfilter", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="X">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BPfilter(Object X)
    {
      return mcr.EvaluateFunction("BPfilter", X);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="X">Input argument #1</param>
    /// <param name="f1">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BPfilter(Object X, Object f1)
    {
      return mcr.EvaluateFunction("BPfilter", X, f1);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="X">Input argument #1</param>
    /// <param name="f1">Input argument #2</param>
    /// <param name="f2">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object BPfilter(Object X, Object f1, Object f2)
    {
      return mcr.EvaluateFunction("BPfilter", X, f1, f2);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BPfilter(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "BPfilter", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="X">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BPfilter(int numArgsOut, Object X)
    {
      return mcr.EvaluateFunction(numArgsOut, "BPfilter", X);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="X">Input argument #1</param>
    /// <param name="f1">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BPfilter(int numArgsOut, Object X, Object f1)
    {
      return mcr.EvaluateFunction(numArgsOut, "BPfilter", X, f1);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the BPfilter M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Two matlab function here, FIRCLS and FILTFILT
    /// FIRCLS is a FIR filter, FILTFILT is zero-phase filtering.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="X">Input argument #1</param>
    /// <param name="f1">Input argument #2</param>
    /// <param name="f2">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] BPfilter(int numArgsOut, Object X, Object f1, Object f2)
    {
      return mcr.EvaluateFunction(numArgsOut, "BPfilter", X, f1, f2);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ButterworthFilter()
    {
      return mcr.EvaluateFunction("ButterworthFilter", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ButterworthFilter(Object Data)
    {
      return mcr.EvaluateFunction("ButterworthFilter", Data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ButterworthFilter(Object Data, Object SamplingRate)
    {
      return mcr.EvaluateFunction("ButterworthFilter", Data, SamplingRate);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ButterworthFilter(Object Data, Object SamplingRate, Object FilterCutOff)
    {
      return mcr.EvaluateFunction("ButterworthFilter", Data, SamplingRate, FilterCutOff);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <param name="filterType">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ButterworthFilter(Object Data, Object SamplingRate, Object 
                              FilterCutOff, Object filterType)
    {
      return mcr.EvaluateFunction("ButterworthFilter", Data, SamplingRate, FilterCutOff, filterType);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ButterworthFilter(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ButterworthFilter(int numArgsOut, Object Data)
    {
      return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", Data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ButterworthFilter(int numArgsOut, Object Data, Object SamplingRate)
    {
      return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", Data, SamplingRate);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ButterworthFilter(int numArgsOut, Object Data, Object SamplingRate, 
                                Object FilterCutOff)
    {
      return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", Data, SamplingRate, FilterCutOff);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the ButterworthFilter
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="Data">Input argument #1</param>
    /// <param name="SamplingRate">Input argument #2</param>
    /// <param name="FilterCutOff">Input argument #3</param>
    /// <param name="filterType">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ButterworthFilter(int numArgsOut, Object Data, Object SamplingRate, 
                                Object FilterCutOff, Object filterType)
    {
      return mcr.EvaluateFunction(numArgsOut, "ButterworthFilter", Data, SamplingRate, FilterCutOff, filterType);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_down()
    {
      return mcr.EvaluateFunction("find_down", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_down(Object y)
    {
      return mcr.EvaluateFunction("find_down", y);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_down(Object y, Object amp)
    {
      return mcr.EvaluateFunction("find_down", y, amp);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_down(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_down", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_down(int numArgsOut, Object y)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_down", y);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the find_down M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_down(int numArgsOut, Object y, Object amp)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_down", y, amp);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_up()
    {
      return mcr.EvaluateFunction("find_up", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_up(Object y)
    {
      return mcr.EvaluateFunction("find_up", y);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object find_up(Object y, Object amp)
    {
      return mcr.EvaluateFunction("find_up", y, amp);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_up(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_up", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_up(int numArgsOut, Object y)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_up", y);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the find_up M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] find_up(int numArgsOut, Object y, Object amp)
    {
      return mcr.EvaluateFunction(numArgsOut, "find_up", y, amp);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ipca()
    {
      return mcr.EvaluateFunction("ipca", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="pc_data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ipca(Object pc_data)
    {
      return mcr.EvaluateFunction("ipca", pc_data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="pc_data">Input argument #1</param>
    /// <param name="paxis">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ipca(Object pc_data, Object paxis)
    {
      return mcr.EvaluateFunction("ipca", pc_data, paxis);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ipca(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "ipca", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="pc_data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ipca(int numArgsOut, Object pc_data)
    {
      return mcr.EvaluateFunction(numArgsOut, "ipca", pc_data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the ipca M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="pc_data">Input argument #1</param>
    /// <param name="paxis">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ipca(int numArgsOut, Object pc_data, Object paxis)
    {
      return mcr.EvaluateFunction(numArgsOut, "ipca", pc_data, paxis);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ItSurrDat()
    {
      return mcr.EvaluateFunction("ItSurrDat", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ItSurrDat(Object data)
    {
      return mcr.EvaluateFunction("ItSurrDat", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="M">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ItSurrDat(Object data, Object M)
    {
      return mcr.EvaluateFunction("ItSurrDat", data, M);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ItSurrDat(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "ItSurrDat", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ItSurrDat(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "ItSurrDat", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the ItSurrDat M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// ItSurrDat.m
    /// usage: [SD,efinal]=ItSurrDat(data,M)
    /// Generates surrogate data set of the same length and with the same 
    /// linear statistical properties as data.  The distribution, 
    /// autocorrelation, and power spectrum are preserved, while the phase is 
    /// iteratively modified.
    /// Inputs:
    /// data        time-series data
    /// M           maximum iterations (default=50)
    /// Outputs:
    /// SD          surrogate data
    /// efinal      error at final iteration
    /// Anatoly Zlotnik, May 2006
    /// Thanks to Farhad Kaffashi for suggestions
    /// [1] Zlotnik, A., Algorithm Development for Modeling and 
    /// Estimation Problems In Human EEG Analysis; M.S. Thesis. Case 
    /// Western Reserve University, June 2006 
    /// [2] Schreiber, T., and Schmitz, A., Surrogate Time Series.
    /// Physica, 142:3; 2000.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="M">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ItSurrDat(int numArgsOut, Object data, Object M)
    {
      return mcr.EvaluateFunction(numArgsOut, "ItSurrDat", data, M);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling()
    {
      return mcr.EvaluateFunction("multiband_coupling", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x)
    {
      return mcr.EvaluateFunction("multiband_coupling", x);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x, Object w)
    {
      return mcr.EvaluateFunction("multiband_coupling", x, w);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x, Object w, Object f1)
    {
      return mcr.EvaluateFunction("multiband_coupling", x, w, f1);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x, Object w, Object f1, Object f2)
    {
      return mcr.EvaluateFunction("multiband_coupling", x, w, f1, f2);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x, Object w, Object f1, Object f2, Object sp)
    {
      return mcr.EvaluateFunction("multiband_coupling", x, w, f1, f2, sp);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="triggers">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_coupling(Object x, Object w, Object f1, Object f2, Object sp, 
                               Object triggers)
    {
      return mcr.EvaluateFunction("multiband_coupling", x, w, f1, f2, sp, triggers);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x, Object w)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x, w);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x, Object w, Object f1)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x, w, f1);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x, Object w, Object f1, 
                                 Object f2)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x, w, f1, f2);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x, Object w, Object f1, 
                                 Object f2, Object sp)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x, w, f1, f2, sp);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the multiband_coupling
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// atan2() is similar to angle(), but we need to swap x an y 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="triggers">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_coupling(int numArgsOut, Object x, Object w, Object f1, 
                                 Object f2, Object sp, Object triggers)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_coupling", x, w, f1, f2, sp, triggers);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling()
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w, Object fcPNA)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w, fcPNA);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w, Object fcPNA, Object 
                                         fcSNA)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w, fcPNA, fcSNA);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w, Object fcPNA, Object 
                                         fcSNA, Object sp)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="bw">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w, Object fcPNA, Object 
                                         fcSNA, Object sp, Object bw)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp, bw);
    }


    /// <summary>
    /// Provides a single output, 7-input Objectinterface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="bw">Input argument #6</param>
    /// <param name="triggers">Input argument #7</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object multiband_crossfreq_coupling(Object x, Object w, Object fcPNA, Object 
                                         fcSNA, Object sp, Object bw, Object triggers)
    {
      return mcr.EvaluateFunction("multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp, bw, triggers);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w, 
                                           Object fcPNA)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w, fcPNA);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w, 
                                           Object fcPNA, Object fcSNA)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w, fcPNA, fcSNA);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w, 
                                           Object fcPNA, Object fcSNA, Object sp)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="bw">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w, 
                                           Object fcPNA, Object fcSNA, Object sp, Object 
                                           bw)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp, bw);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the
    /// multiband_crossfreq_coupling M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// mphase=0;    don't know (or doesn't make sense) to calculate phase different of
    /// two signal with different freq.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="w">Input argument #2</param>
    /// <param name="fcPNA">Input argument #3</param>
    /// <param name="fcSNA">Input argument #4</param>
    /// <param name="sp">Input argument #5</param>
    /// <param name="bw">Input argument #6</param>
    /// <param name="triggers">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] multiband_crossfreq_coupling(int numArgsOut, Object x, Object w, 
                                           Object fcPNA, Object fcSNA, Object sp, Object 
                                           bw, Object triggers)
    {
      return mcr.EvaluateFunction(numArgsOut, "multiband_crossfreq_coupling", x, w, fcPNA, fcSNA, sp, bw, triggers);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object MutInf()
    {
      return mcr.EvaluateFunction("MutInf", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object MutInf(Object data)
    {
      return mcr.EvaluateFunction("MutInf", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="taumax">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object MutInf(Object data, Object taumax)
    {
      return mcr.EvaluateFunction("MutInf", data, taumax);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="taumax">Input argument #2</param>
    /// <param name="nbins">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object MutInf(Object data, Object taumax, Object nbins)
    {
      return mcr.EvaluateFunction("MutInf", data, taumax, nbins);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] MutInf(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "MutInf", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] MutInf(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "MutInf", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="taumax">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] MutInf(int numArgsOut, Object data, Object taumax)
    {
      return mcr.EvaluateFunction(numArgsOut, "MutInf", data, taumax);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the MutInf M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MutInf.m
    /// usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
    /// Mutual information of time-series data for different delays
    /// Inputs:
    /// data        time-series
    /// taumax      maximum delay value
    /// nbins       bins for histogram (default = 100)
    /// Notes:      If taumax is one integer, mutual information is computed 
    /// for all delay values up to taumax.  If taumax 
    /// Outputs:
    /// I           Mutual Information
    /// taugrid     delay values
    /// E1          Entropy of the time-series
    /// E2          Entropies of the delayed time-series
    /// Anatoly Zlotnik, June 19, 2006
    /// [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
    /// Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
    /// University; 2006
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="taumax">Input argument #2</param>
    /// <param name="nbins">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] MutInf(int numArgsOut, Object data, Object taumax, Object nbins)
    {
      return mcr.EvaluateFunction(numArgsOut, "MutInf", data, taumax, nbins);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the my_filt M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object my_filt()
    {
      return mcr.EvaluateFunction("my_filt", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the my_filt M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object my_filt(Object y)
    {
      return mcr.EvaluateFunction("my_filt", y);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the my_filt M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="res">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object my_filt(Object y, Object res)
    {
      return mcr.EvaluateFunction("my_filt", y, res);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the my_filt M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="res">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object my_filt(Object y, Object res, Object f1)
    {
      return mcr.EvaluateFunction("my_filt", y, res, f1);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the my_filt M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="res">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object my_filt(Object y, Object res, Object f1, Object f2)
    {
      return mcr.EvaluateFunction("my_filt", y, res, f1, f2);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the my_filt M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] my_filt(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "my_filt", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the my_filt M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] my_filt(int numArgsOut, Object y)
    {
      return mcr.EvaluateFunction(numArgsOut, "my_filt", y);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the my_filt M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="res">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] my_filt(int numArgsOut, Object y, Object res)
    {
      return mcr.EvaluateFunction(numArgsOut, "my_filt", y, res);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the my_filt M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="res">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] my_filt(int numArgsOut, Object y, Object res, Object f1)
    {
      return mcr.EvaluateFunction(numArgsOut, "my_filt", y, res, f1);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the my_filt M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="res">Input argument #2</param>
    /// <param name="f1">Input argument #3</param>
    /// <param name="f2">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] my_filt(int numArgsOut, Object y, Object res, Object f1, Object f2)
    {
      return mcr.EvaluateFunction(numArgsOut, "my_filt", y, res, f1, f2);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object myss()
    {
      return mcr.EvaluateFunction("myss", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object myss(Object data)
    {
      return mcr.EvaluateFunction("myss", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="M">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object myss(Object data, Object M)
    {
      return mcr.EvaluateFunction("myss", data, M);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] myss(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "myss", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] myss(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "myss", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the myss M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// close all;
    /// hist(R,100);
    /// pause();
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="M">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] myss(int numArgsOut, Object data, Object M)
    {
      return mcr.EvaluateFunction(numArgsOut, "myss", data, M);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pc_evectors()
    {
      return mcr.EvaluateFunction("pc_evectors", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <param name="A">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pc_evectors(Object A)
    {
      return mcr.EvaluateFunction("pc_evectors", A);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <param name="A">Input argument #1</param>
    /// <param name="numvecs">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pc_evectors(Object A, Object numvecs)
    {
      return mcr.EvaluateFunction("pc_evectors", A, numvecs);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pc_evectors(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "pc_evectors", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="A">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pc_evectors(int numArgsOut, Object A)
    {
      return mcr.EvaluateFunction(numArgsOut, "pc_evectors", A);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the pc_evectors M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// PC_EVECTORS Get the top numvecs eigenvectors of the covariance matrix
    /// of A, using Turk and Pentland's trick for numrows >> numcols
    /// Returns the eigenvectors as the colums of Vectors and a
    /// vector of ALL the eigenvectors in Values.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="A">Input argument #1</param>
    /// <param name="numvecs">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pc_evectors(int numArgsOut, Object A, Object numvecs)
    {
      return mcr.EvaluateFunction(numArgsOut, "pc_evectors", A, numvecs);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pca2()
    {
      return mcr.EvaluateFunction("pca2", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <param name="a">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pca2(Object a)
    {
      return mcr.EvaluateFunction("pca2", a);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <param name="a">Input argument #1</param>
    /// <param name="center">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object pca2(Object a, Object center)
    {
      return mcr.EvaluateFunction("pca2", a, center);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pca2(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "pca2", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="a">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pca2(int numArgsOut, Object a)
    {
      return mcr.EvaluateFunction(numArgsOut, "pca2", a);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the pca2 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// [T,D,msum] = pca2( a, center )
    /// Returns principal components
    /// of xsize x ysize image a as
    /// eigenvalues and eigenvectors.
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="a">Input argument #1</param>
    /// <param name="center">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] pca2(int numArgsOut, Object a, Object center)
    {
      return mcr.EvaluateFunction(numArgsOut, "pca2", a, center);
    }


    /// <summary>
    /// Provides a void output, 0-input Objectinterface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    ///
    public void plotColor()
    {
      mcr.EvaluateFunction(0, "plotColor", new Object[]{});
    }


    /// <summary>
    /// Provides a void output, 1-input Objectinterface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    ///
    public void plotColor(Object x)
    {
      mcr.EvaluateFunction(0, "plotColor", x);
    }


    /// <summary>
    /// Provides a void output, 2-input Objectinterface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="y">Input argument #2</param>
    ///
    public void plotColor(Object x, Object y)
    {
      mcr.EvaluateFunction(0, "plotColor", x, y);
    }


    /// <summary>
    /// Provides a void output, 3-input Objectinterface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="y">Input argument #2</param>
    /// <param name="r">Input argument #3</param>
    ///
    public void plotColor(Object x, Object y, Object r)
    {
      mcr.EvaluateFunction(0, "plotColor", x, y, r);
    }


    /// <summary>
    /// Provides a void output, 4-input Objectinterface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="y">Input argument #2</param>
    /// <param name="r">Input argument #3</param>
    /// <param name="fignum">Input argument #4</param>
    ///
    public void plotColor(Object x, Object y, Object r, Object fignum)
    {
      mcr.EvaluateFunction(0, "plotColor", x, y, r, fignum);
    }


    /// <summary>
    /// Provides a void output, 5-input Objectinterface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="y">Input argument #2</param>
    /// <param name="r">Input argument #3</param>
    /// <param name="fignum">Input argument #4</param>
    /// <param name="map">Input argument #5</param>
    ///
    public void plotColor(Object x, Object y, Object r, Object fignum, Object map)
    {
      mcr.EvaluateFunction(0, "plotColor", x, y, r, fignum, map);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotColor(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotColor", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotColor(int numArgsOut, Object x)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotColor", x);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="y">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotColor(int numArgsOut, Object x, Object y)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotColor", x, y);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="y">Input argument #2</param>
    /// <param name="r">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotColor(int numArgsOut, Object x, Object y, Object r)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotColor", x, y, r);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="y">Input argument #2</param>
    /// <param name="r">Input argument #3</param>
    /// <param name="fignum">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotColor(int numArgsOut, Object x, Object y, Object r, Object fignum)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotColor", x, y, r, fignum);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the plotColor M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="y">Input argument #2</param>
    /// <param name="r">Input argument #3</param>
    /// <param name="fignum">Input argument #4</param>
    /// <param name="map">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotColor(int numArgsOut, Object x, Object y, Object r, Object 
                        fignum, Object map)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotColor", x, y, r, fignum, map);
    }


    /// <summary>
    /// Provides a void output, 0-input Objectinterface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    ///
    public void plotHistograms()
    {
      mcr.EvaluateFunction(0, "plotHistograms", new Object[]{});
    }


    /// <summary>
    /// Provides a void output, 1-input Objectinterface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    ///
    public void plotHistograms(Object x)
    {
      mcr.EvaluateFunction(0, "plotHistograms", x);
    }


    /// <summary>
    /// Provides a void output, 2-input Objectinterface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    ///
    public void plotHistograms(Object x, Object data)
    {
      mcr.EvaluateFunction(0, "plotHistograms", x, data);
    }


    /// <summary>
    /// Provides a void output, 3-input Objectinterface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    ///
    public void plotHistograms(Object x, Object data, Object titlestr)
    {
      mcr.EvaluateFunction(0, "plotHistograms", x, data, titlestr);
    }


    /// <summary>
    /// Provides a void output, 4-input Objectinterface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    ///
    public void plotHistograms(Object x, Object data, Object titlestr, Object xlabelstr)
    {
      mcr.EvaluateFunction(0, "plotHistograms", x, data, titlestr, xlabelstr);
    }


    /// <summary>
    /// Provides a void output, 5-input Objectinterface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <param name="ylabelstr">Input argument #5</param>
    ///
    public void plotHistograms(Object x, Object data, Object titlestr, Object xlabelstr, 
                         Object ylabelstr)
    {
      mcr.EvaluateFunction(0, "plotHistograms", x, data, titlestr, xlabelstr, ylabelstr);
    }


    /// <summary>
    /// Provides a void output, 6-input Objectinterface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <param name="ylabelstr">Input argument #5</param>
    /// <param name="colors">Input argument #6</param>
    ///
    public void plotHistograms(Object x, Object data, Object titlestr, Object xlabelstr, 
                         Object ylabelstr, Object colors)
    {
      mcr.EvaluateFunction(0, "plotHistograms", x, data, titlestr, xlabelstr, ylabelstr, colors);
    }


    /// <summary>
    /// Provides a void output, 7-input Objectinterface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <param name="ylabelstr">Input argument #5</param>
    /// <param name="colors">Input argument #6</param>
    /// <param name="linecolors">Input argument #7</param>
    ///
    public void plotHistograms(Object x, Object data, Object titlestr, Object xlabelstr, 
                         Object ylabelstr, Object colors, Object linecolors)
    {
      mcr.EvaluateFunction(0, "plotHistograms", x, data, titlestr, xlabelstr, ylabelstr, colors, linecolors);
    }


    /// <summary>
    /// Provides a void output, 8-input Objectinterface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <param name="ylabelstr">Input argument #5</param>
    /// <param name="colors">Input argument #6</param>
    /// <param name="linecolors">Input argument #7</param>
    /// <param name="info">Input argument #8</param>
    ///
    public void plotHistograms(Object x, Object data, Object titlestr, Object xlabelstr, 
                         Object ylabelstr, Object colors, Object linecolors, Object info)
    {
      mcr.EvaluateFunction(0, "plotHistograms", x, data, titlestr, xlabelstr, ylabelstr, colors, linecolors, info);
    }


    /// <summary>
    /// Provides a void output, 9-input Objectinterface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <param name="ylabelstr">Input argument #5</param>
    /// <param name="colors">Input argument #6</param>
    /// <param name="linecolors">Input argument #7</param>
    /// <param name="info">Input argument #8</param>
    /// <param name="savepath">Input argument #9</param>
    ///
    public void plotHistograms(Object x, Object data, Object titlestr, Object xlabelstr, 
                         Object ylabelstr, Object colors, Object linecolors, Object info, 
                         Object savepath)
    {
      mcr.EvaluateFunction(0, "plotHistograms", x, data, titlestr, xlabelstr, ylabelstr, colors, linecolors, info, savepath);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotHistograms(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotHistograms", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotHistograms(int numArgsOut, Object x)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotHistograms", x);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotHistograms(int numArgsOut, Object x, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotHistograms", x, data);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotHistograms(int numArgsOut, Object x, Object data, Object titlestr)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotHistograms", x, data, titlestr);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotHistograms(int numArgsOut, Object x, Object data, Object 
                             titlestr, Object xlabelstr)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotHistograms", x, data, titlestr, xlabelstr);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <param name="ylabelstr">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotHistograms(int numArgsOut, Object x, Object data, Object 
                             titlestr, Object xlabelstr, Object ylabelstr)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotHistograms", x, data, titlestr, xlabelstr, ylabelstr);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <param name="ylabelstr">Input argument #5</param>
    /// <param name="colors">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotHistograms(int numArgsOut, Object x, Object data, Object 
                             titlestr, Object xlabelstr, Object ylabelstr, Object colors)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotHistograms", x, data, titlestr, xlabelstr, ylabelstr, colors);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <param name="ylabelstr">Input argument #5</param>
    /// <param name="colors">Input argument #6</param>
    /// <param name="linecolors">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotHistograms(int numArgsOut, Object x, Object data, Object 
                             titlestr, Object xlabelstr, Object ylabelstr, Object colors, 
                             Object linecolors)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotHistograms", x, data, titlestr, xlabelstr, ylabelstr, colors, linecolors);
    }


    /// <summary>
    /// Provides the standard 8-input Object interface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <param name="ylabelstr">Input argument #5</param>
    /// <param name="colors">Input argument #6</param>
    /// <param name="linecolors">Input argument #7</param>
    /// <param name="info">Input argument #8</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotHistograms(int numArgsOut, Object x, Object data, Object 
                             titlestr, Object xlabelstr, Object ylabelstr, Object colors, 
                             Object linecolors, Object info)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotHistograms", x, data, titlestr, xlabelstr, ylabelstr, colors, linecolors, info);
    }


    /// <summary>
    /// Provides the standard 9-input Object interface to the plotHistograms M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="x">Input argument #1</param>
    /// <param name="data">Input argument #2</param>
    /// <param name="titlestr">Input argument #3</param>
    /// <param name="xlabelstr">Input argument #4</param>
    /// <param name="ylabelstr">Input argument #5</param>
    /// <param name="colors">Input argument #6</param>
    /// <param name="linecolors">Input argument #7</param>
    /// <param name="info">Input argument #8</param>
    /// <param name="savepath">Input argument #9</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] plotHistograms(int numArgsOut, Object x, Object data, Object 
                             titlestr, Object xlabelstr, Object ylabelstr, Object colors, 
                             Object linecolors, Object info, Object savepath)
    {
      return mcr.EvaluateFunction(numArgsOut, "plotHistograms", x, data, titlestr, xlabelstr, ylabelstr, colors, linecolors, info, savepath);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the powerSpectrum
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object powerSpectrum()
    {
      return mcr.EvaluateFunction("powerSpectrum", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the powerSpectrum
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object powerSpectrum(Object data)
    {
      return mcr.EvaluateFunction("powerSpectrum", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the powerSpectrum
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="Fs">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object powerSpectrum(Object data, Object Fs)
    {
      return mcr.EvaluateFunction("powerSpectrum", data, Fs);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the powerSpectrum M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] powerSpectrum(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "powerSpectrum", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the powerSpectrum M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] powerSpectrum(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "powerSpectrum", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the powerSpectrum M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="Fs">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] powerSpectrum(int numArgsOut, Object data, Object Fs)
    {
      return mcr.EvaluateFunction(numArgsOut, "powerSpectrum", data, Fs);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ratBP()
    {
      return mcr.EvaluateFunction("ratBP", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="B">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ratBP(Object B)
    {
      return mcr.EvaluateFunction("ratBP", B);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="B">Input argument #1</param>
    /// <param name="band1low">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ratBP(Object B, Object band1low)
    {
      return mcr.EvaluateFunction("ratBP", B, band1low);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="B">Input argument #1</param>
    /// <param name="band1low">Input argument #2</param>
    /// <param name="band1high">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ratBP(Object B, Object band1low, Object band1high)
    {
      return mcr.EvaluateFunction("ratBP", B, band1low, band1high);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="B">Input argument #1</param>
    /// <param name="band1low">Input argument #2</param>
    /// <param name="band1high">Input argument #3</param>
    /// <param name="band2low">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ratBP(Object B, Object band1low, Object band1high, Object band2low)
    {
      return mcr.EvaluateFunction("ratBP", B, band1low, band1high, band2low);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="B">Input argument #1</param>
    /// <param name="band1low">Input argument #2</param>
    /// <param name="band1high">Input argument #3</param>
    /// <param name="band2low">Input argument #4</param>
    /// <param name="band2high">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ratBP(Object B, Object band1low, Object band1high, Object band2low, 
                  Object band2high)
    {
      return mcr.EvaluateFunction("ratBP", B, band1low, band1high, band2low, band2high);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ratBP(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "ratBP", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="B">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ratBP(int numArgsOut, Object B)
    {
      return mcr.EvaluateFunction(numArgsOut, "ratBP", B);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="B">Input argument #1</param>
    /// <param name="band1low">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ratBP(int numArgsOut, Object B, Object band1low)
    {
      return mcr.EvaluateFunction(numArgsOut, "ratBP", B, band1low);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="B">Input argument #1</param>
    /// <param name="band1low">Input argument #2</param>
    /// <param name="band1high">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ratBP(int numArgsOut, Object B, Object band1low, Object band1high)
    {
      return mcr.EvaluateFunction(numArgsOut, "ratBP", B, band1low, band1high);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="B">Input argument #1</param>
    /// <param name="band1low">Input argument #2</param>
    /// <param name="band1high">Input argument #3</param>
    /// <param name="band2low">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ratBP(int numArgsOut, Object B, Object band1low, Object band1high, 
                    Object band2low)
    {
      return mcr.EvaluateFunction(numArgsOut, "ratBP", B, band1low, band1high, band2low);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the ratBP M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// INCR = 2;
    /// DECR = 1;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="B">Input argument #1</param>
    /// <param name="band1low">Input argument #2</param>
    /// <param name="band1high">Input argument #3</param>
    /// <param name="band2low">Input argument #4</param>
    /// <param name="band2high">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ratBP(int numArgsOut, Object B, Object band1low, Object band1high, 
                    Object band2low, Object band2high)
    {
      return mcr.EvaluateFunction(numArgsOut, "ratBP", B, band1low, band1high, band2low, band2high);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the resampleData M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object resampleData()
    {
      return mcr.EvaluateFunction("resampleData", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the resampleData M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object resampleData(Object data)
    {
      return mcr.EvaluateFunction("resampleData", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the resampleData M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="amount">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object resampleData(Object data, Object amount)
    {
      return mcr.EvaluateFunction("resampleData", data, amount);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the resampleData M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="amount">Input argument #2</param>
    /// <param name="inputRes">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object resampleData(Object data, Object amount, Object inputRes)
    {
      return mcr.EvaluateFunction("resampleData", data, amount, inputRes);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the resampleData M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] resampleData(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "resampleData", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the resampleData M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] resampleData(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "resampleData", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the resampleData M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="amount">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] resampleData(int numArgsOut, Object data, Object amount)
    {
      return mcr.EvaluateFunction(numArgsOut, "resampleData", data, amount);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the resampleData M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="amount">Input argument #2</param>
    /// <param name="inputRes">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] resampleData(int numArgsOut, Object data, Object amount, Object 
                           inputRes)
    {
      return mcr.EvaluateFunction(numArgsOut, "resampleData", data, amount, inputRes);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert()
    {
      return mcr.EvaluateFunction("runHilbert", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1)
    {
      return mcr.EvaluateFunction("runHilbert", data1);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq, Object maxFreq)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq);
    }


    /// <summary>
    /// Provides a single output, 7-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq, Object maxFreq, Object freqStep)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep);
    }


    /// <summary>
    /// Provides a single output, 8-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq, Object maxFreq, Object freqStep, 
                       Object freqBand)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand);
    }


    /// <summary>
    /// Provides a single output, 9-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <param name="windowSize">Input argument #9</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq, Object maxFreq, Object freqStep, 
                       Object freqBand, Object windowSize)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand, windowSize);
    }


    /// <summary>
    /// Provides a single output, 10-input Objectinterface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <param name="windowSize">Input argument #9</param>
    /// <param name="triggers">Input argument #10</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbert(Object data1, Object data2, Object dataFreq, Object 
                       targetFreq, Object minFreq, Object maxFreq, Object freqStep, 
                       Object freqBand, Object windowSize, Object triggers)
    {
      return mcr.EvaluateFunction("runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq, Object maxFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq, Object maxFreq, 
                         Object freqStep)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep);
    }


    /// <summary>
    /// Provides the standard 8-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq, Object maxFreq, 
                         Object freqStep, Object freqBand)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand);
    }


    /// <summary>
    /// Provides the standard 9-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <param name="windowSize">Input argument #9</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq, Object maxFreq, 
                         Object freqStep, Object freqBand, Object windowSize)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand, windowSize);
    }


    /// <summary>
    /// Provides the standard 10-input Object interface to the runHilbert M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="minFreq">Input argument #5</param>
    /// <param name="maxFreq">Input argument #6</param>
    /// <param name="freqStep">Input argument #7</param>
    /// <param name="freqBand">Input argument #8</param>
    /// <param name="windowSize">Input argument #9</param>
    /// <param name="triggers">Input argument #10</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbert(int numArgsOut, Object data1, Object data2, Object 
                         dataFreq, Object targetFreq, Object minFreq, Object maxFreq, 
                         Object freqStep, Object freqBand, Object windowSize, Object 
                         triggers)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbert", data1, data2, dataFreq, targetFreq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross()
    {
      return mcr.EvaluateFunction("runHilbertCross", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq);
    }


    /// <summary>
    /// Provides a single output, 7-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq, Object maxFreq)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq);
    }


    /// <summary>
    /// Provides a single output, 8-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq, Object maxFreq, 
                            Object freqStep)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep);
    }


    /// <summary>
    /// Provides a single output, 9-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq, Object maxFreq, 
                            Object freqStep, Object freqBand)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand);
    }


    /// <summary>
    /// Provides a single output, 10-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <param name="windowSize">Input argument #10</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq, Object maxFreq, 
                            Object freqStep, Object freqBand, Object windowSize)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize);
    }


    /// <summary>
    /// Provides a single output, 11-input Objectinterface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <param name="windowSize">Input argument #10</param>
    /// <param name="triggers">Input argument #11</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runHilbertCross(Object data1, Object data2, Object dataFreq, Object 
                            targetFreq, Object data1Freq, Object minFreq, Object maxFreq, 
                            Object freqStep, Object freqBand, Object windowSize, Object 
                            triggers)
    {
      return mcr.EvaluateFunction("runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq, Object maxFreq)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq);
    }


    /// <summary>
    /// Provides the standard 8-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq, Object maxFreq, Object freqStep)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep);
    }


    /// <summary>
    /// Provides the standard 9-input Object interface to the runHilbertCross M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq, Object maxFreq, Object freqStep, Object freqBand)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand);
    }


    /// <summary>
    /// Provides the standard 10-input Object interface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <param name="windowSize">Input argument #10</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq, Object maxFreq, Object freqStep, Object freqBand, 
                              Object windowSize)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize);
    }


    /// <summary>
    /// Provides the standard 11-input Object interface to the runHilbertCross
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// resampling
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data1">Input argument #1</param>
    /// <param name="data2">Input argument #2</param>
    /// <param name="dataFreq">Input argument #3</param>
    /// <param name="targetFreq">Input argument #4</param>
    /// <param name="data1Freq">Input argument #5</param>
    /// <param name="minFreq">Input argument #6</param>
    /// <param name="maxFreq">Input argument #7</param>
    /// <param name="freqStep">Input argument #8</param>
    /// <param name="freqBand">Input argument #9</param>
    /// <param name="windowSize">Input argument #10</param>
    /// <param name="triggers">Input argument #11</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runHilbertCross(int numArgsOut, Object data1, Object data2, Object 
                              dataFreq, Object targetFreq, Object data1Freq, Object 
                              minFreq, Object maxFreq, Object freqStep, Object freqBand, 
                              Object windowSize, Object triggers)
    {
      return mcr.EvaluateFunction(numArgsOut, "runHilbertCross", data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the RunMultiOrder
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object RunMultiOrder()
    {
      return mcr.EvaluateFunction("RunMultiOrder", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the RunMultiOrder
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object RunMultiOrder(Object trigger1Data)
    {
      return mcr.EvaluateFunction("RunMultiOrder", trigger1Data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the RunMultiOrder
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object RunMultiOrder(Object trigger1Data, Object trigger2Data)
    {
      return mcr.EvaluateFunction("RunMultiOrder", trigger1Data, trigger2Data);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the RunMultiOrder
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object RunMultiOrder(Object trigger1Data, Object trigger2Data, Object 
                          numCycles)
    {
      return mcr.EvaluateFunction("RunMultiOrder", trigger1Data, trigger2Data, numCycles);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the RunMultiOrder
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object RunMultiOrder(Object trigger1Data, Object trigger2Data, Object 
                          numCycles, Object numFakes)
    {
      return mcr.EvaluateFunction("RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the RunMultiOrder
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <param name="TimeRes">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object RunMultiOrder(Object trigger1Data, Object trigger2Data, Object 
                          numCycles, Object numFakes, Object TimeRes)
    {
      return mcr.EvaluateFunction("RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes, TimeRes);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the RunMultiOrder
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <param name="TimeRes">Input argument #5</param>
    /// <param name="plot">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object RunMultiOrder(Object trigger1Data, Object trigger2Data, Object 
                          numCycles, Object numFakes, Object TimeRes, Object plot)
    {
      return mcr.EvaluateFunction("RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes, TimeRes, plot);
    }


    /// <summary>
    /// Provides a single output, 7-input Objectinterface to the RunMultiOrder
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <param name="TimeRes">Input argument #5</param>
    /// <param name="plot">Input argument #6</param>
    /// <param name="save">Input argument #7</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object RunMultiOrder(Object trigger1Data, Object trigger2Data, Object 
                          numCycles, Object numFakes, Object TimeRes, Object plot, Object 
                          save)
    {
      return mcr.EvaluateFunction("RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes, TimeRes, plot, save);
    }


    /// <summary>
    /// Provides a single output, 8-input Objectinterface to the RunMultiOrder
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <param name="TimeRes">Input argument #5</param>
    /// <param name="plot">Input argument #6</param>
    /// <param name="save">Input argument #7</param>
    /// <param name="label">Input argument #8</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object RunMultiOrder(Object trigger1Data, Object trigger2Data, Object 
                          numCycles, Object numFakes, Object TimeRes, Object plot, Object 
                          save, Object label)
    {
      return mcr.EvaluateFunction("RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes, TimeRes, plot, save, label);
    }


    /// <summary>
    /// Provides a single output, 9-input Objectinterface to the RunMultiOrder
    /// M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <param name="TimeRes">Input argument #5</param>
    /// <param name="plot">Input argument #6</param>
    /// <param name="save">Input argument #7</param>
    /// <param name="label">Input argument #8</param>
    /// <param name="path">Input argument #9</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object RunMultiOrder(Object trigger1Data, Object trigger2Data, Object 
                          numCycles, Object numFakes, Object TimeRes, Object plot, Object 
                          save, Object label, Object path)
    {
      return mcr.EvaluateFunction("RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes, TimeRes, plot, save, label, path);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the RunMultiOrder M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunMultiOrder(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunMultiOrder", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the RunMultiOrder M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunMultiOrder(int numArgsOut, Object trigger1Data)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunMultiOrder", trigger1Data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the RunMultiOrder M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunMultiOrder(int numArgsOut, Object trigger1Data, Object 
                            trigger2Data)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunMultiOrder", trigger1Data, trigger2Data);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the RunMultiOrder M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunMultiOrder(int numArgsOut, Object trigger1Data, Object 
                            trigger2Data, Object numCycles)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunMultiOrder", trigger1Data, trigger2Data, numCycles);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the RunMultiOrder M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunMultiOrder(int numArgsOut, Object trigger1Data, Object 
                            trigger2Data, Object numCycles, Object numFakes)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the RunMultiOrder M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <param name="TimeRes">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunMultiOrder(int numArgsOut, Object trigger1Data, Object 
                            trigger2Data, Object numCycles, Object numFakes, Object 
                            TimeRes)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes, TimeRes);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the RunMultiOrder M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <param name="TimeRes">Input argument #5</param>
    /// <param name="plot">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunMultiOrder(int numArgsOut, Object trigger1Data, Object 
                            trigger2Data, Object numCycles, Object numFakes, Object 
                            TimeRes, Object plot)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes, TimeRes, plot);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the RunMultiOrder M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <param name="TimeRes">Input argument #5</param>
    /// <param name="plot">Input argument #6</param>
    /// <param name="save">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunMultiOrder(int numArgsOut, Object trigger1Data, Object 
                            trigger2Data, Object numCycles, Object numFakes, Object 
                            TimeRes, Object plot, Object save)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes, TimeRes, plot, save);
    }


    /// <summary>
    /// Provides the standard 8-input Object interface to the RunMultiOrder M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <param name="TimeRes">Input argument #5</param>
    /// <param name="plot">Input argument #6</param>
    /// <param name="save">Input argument #7</param>
    /// <param name="label">Input argument #8</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunMultiOrder(int numArgsOut, Object trigger1Data, Object 
                            trigger2Data, Object numCycles, Object numFakes, Object 
                            TimeRes, Object plot, Object save, Object label)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes, TimeRes, plot, save, label);
    }


    /// <summary>
    /// Provides the standard 9-input Object interface to the RunMultiOrder M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// numFakes=10000;
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="numCycles">Input argument #3</param>
    /// <param name="numFakes">Input argument #4</param>
    /// <param name="TimeRes">Input argument #5</param>
    /// <param name="plot">Input argument #6</param>
    /// <param name="save">Input argument #7</param>
    /// <param name="label">Input argument #8</param>
    /// <param name="path">Input argument #9</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunMultiOrder(int numArgsOut, Object trigger1Data, Object 
                            trigger2Data, Object numCycles, Object numFakes, Object 
                            TimeRes, Object plot, Object save, Object label, Object path)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunMultiOrder", trigger1Data, trigger2Data, numCycles, numFakes, TimeRes, plot, save, label, path);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runPCA()
    {
      return mcr.EvaluateFunction("runPCA", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runPCA(Object data)
    {
      return mcr.EvaluateFunction("runPCA", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="projectOnto">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runPCA(Object data, Object projectOnto)
    {
      return mcr.EvaluateFunction("runPCA", data, projectOnto);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="projectOnto">Input argument #2</param>
    /// <param name="maxVectors">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runPCA(Object data, Object projectOnto, Object maxVectors)
    {
      return mcr.EvaluateFunction("runPCA", data, projectOnto, maxVectors);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runPCA(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "runPCA", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runPCA(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "runPCA", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="projectOnto">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runPCA(int numArgsOut, Object data, Object projectOnto)
    {
      return mcr.EvaluateFunction(numArgsOut, "runPCA", data, projectOnto);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the runPCA M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="projectOnto">Input argument #2</param>
    /// <param name="maxVectors">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runPCA(int numArgsOut, Object data, Object projectOnto, Object 
                     maxVectors)
    {
      return mcr.EvaluateFunction(numArgsOut, "runPCA", data, projectOnto, maxVectors);
    }


    /// <summary>
    /// Provides a void output, 0-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    ///
    public void RunSingle()
    {
      mcr.EvaluateFunction(0, "RunSingle", new Object[]{});
    }


    /// <summary>
    /// Provides a void output, 1-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    ///
    public void RunSingle(Object trigger1Data)
    {
      mcr.EvaluateFunction(0, "RunSingle", trigger1Data);
    }


    /// <summary>
    /// Provides a void output, 2-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    ///
    public void RunSingle(Object trigger1Data, Object trigger2Data)
    {
      mcr.EvaluateFunction(0, "RunSingle", trigger1Data, trigger2Data);
    }


    /// <summary>
    /// Provides a void output, 3-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    ///
    public void RunSingle(Object trigger1Data, Object trigger2Data, Object minbins)
    {
      mcr.EvaluateFunction(0, "RunSingle", trigger1Data, trigger2Data, minbins);
    }


    /// <summary>
    /// Provides a void output, 4-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    ///
    public void RunSingle(Object trigger1Data, Object trigger2Data, Object minbins, 
                    Object maxbins)
    {
      mcr.EvaluateFunction(0, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins);
    }


    /// <summary>
    /// Provides a void output, 5-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    ///
    public void RunSingle(Object trigger1Data, Object trigger2Data, Object minbins, 
                    Object maxbins, Object bestnum)
    {
      mcr.EvaluateFunction(0, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum);
    }


    /// <summary>
    /// Provides a void output, 6-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <param name="mostvar">Input argument #6</param>
    ///
    public void RunSingle(Object trigger1Data, Object trigger2Data, Object minbins, 
                    Object maxbins, Object bestnum, Object mostvar)
    {
      mcr.EvaluateFunction(0, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum, mostvar);
    }


    /// <summary>
    /// Provides a void output, 7-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <param name="mostvar">Input argument #6</param>
    /// <param name="shouldplot">Input argument #7</param>
    ///
    public void RunSingle(Object trigger1Data, Object trigger2Data, Object minbins, 
                    Object maxbins, Object bestnum, Object mostvar, Object shouldplot)
    {
      mcr.EvaluateFunction(0, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum, mostvar, shouldplot);
    }


    /// <summary>
    /// Provides a void output, 8-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <param name="mostvar">Input argument #6</param>
    /// <param name="shouldplot">Input argument #7</param>
    /// <param name="save">Input argument #8</param>
    ///
    public void RunSingle(Object trigger1Data, Object trigger2Data, Object minbins, 
                    Object maxbins, Object bestnum, Object mostvar, Object shouldplot, 
                    Object save)
    {
      mcr.EvaluateFunction(0, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum, mostvar, shouldplot, save);
    }


    /// <summary>
    /// Provides a void output, 9-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <param name="mostvar">Input argument #6</param>
    /// <param name="shouldplot">Input argument #7</param>
    /// <param name="save">Input argument #8</param>
    /// <param name="label">Input argument #9</param>
    ///
    public void RunSingle(Object trigger1Data, Object trigger2Data, Object minbins, 
                    Object maxbins, Object bestnum, Object mostvar, Object shouldplot, 
                    Object save, Object label)
    {
      mcr.EvaluateFunction(0, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum, mostvar, shouldplot, save, label);
    }


    /// <summary>
    /// Provides a void output, 10-input Objectinterface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <param name="mostvar">Input argument #6</param>
    /// <param name="shouldplot">Input argument #7</param>
    /// <param name="save">Input argument #8</param>
    /// <param name="label">Input argument #9</param>
    /// <param name="path">Input argument #10</param>
    ///
    public void RunSingle(Object trigger1Data, Object trigger2Data, Object minbins, 
                    Object maxbins, Object bestnum, Object mostvar, Object shouldplot, 
                    Object save, Object label, Object path)
    {
      mcr.EvaluateFunction(0, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum, mostvar, shouldplot, save, label, path);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut, Object trigger1Data)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", trigger1Data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut, Object trigger1Data, Object trigger2Data)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", trigger1Data, trigger2Data);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut, Object trigger1Data, Object trigger2Data, 
                        Object minbins)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", trigger1Data, trigger2Data, minbins);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut, Object trigger1Data, Object trigger2Data, 
                        Object minbins, Object maxbins)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut, Object trigger1Data, Object trigger2Data, 
                        Object minbins, Object maxbins, Object bestnum)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <param name="mostvar">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut, Object trigger1Data, Object trigger2Data, 
                        Object minbins, Object maxbins, Object bestnum, Object mostvar)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum, mostvar);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <param name="mostvar">Input argument #6</param>
    /// <param name="shouldplot">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut, Object trigger1Data, Object trigger2Data, 
                        Object minbins, Object maxbins, Object bestnum, Object mostvar, 
                        Object shouldplot)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum, mostvar, shouldplot);
    }


    /// <summary>
    /// Provides the standard 8-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <param name="mostvar">Input argument #6</param>
    /// <param name="shouldplot">Input argument #7</param>
    /// <param name="save">Input argument #8</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut, Object trigger1Data, Object trigger2Data, 
                        Object minbins, Object maxbins, Object bestnum, Object mostvar, 
                        Object shouldplot, Object save)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum, mostvar, shouldplot, save);
    }


    /// <summary>
    /// Provides the standard 9-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <param name="mostvar">Input argument #6</param>
    /// <param name="shouldplot">Input argument #7</param>
    /// <param name="save">Input argument #8</param>
    /// <param name="label">Input argument #9</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut, Object trigger1Data, Object trigger2Data, 
                        Object minbins, Object maxbins, Object bestnum, Object mostvar, 
                        Object shouldplot, Object save, Object label)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum, mostvar, shouldplot, save, label);
    }


    /// <summary>
    /// Provides the standard 10-input Object interface to the RunSingle M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// figpos=[1810 250 1200 700];
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="trigger1Data">Input argument #1</param>
    /// <param name="trigger2Data">Input argument #2</param>
    /// <param name="minbins">Input argument #3</param>
    /// <param name="maxbins">Input argument #4</param>
    /// <param name="bestnum">Input argument #5</param>
    /// <param name="mostvar">Input argument #6</param>
    /// <param name="shouldplot">Input argument #7</param>
    /// <param name="save">Input argument #8</param>
    /// <param name="label">Input argument #9</param>
    /// <param name="path">Input argument #10</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] RunSingle(int numArgsOut, Object trigger1Data, Object trigger2Data, 
                        Object minbins, Object maxbins, Object bestnum, Object mostvar, 
                        Object shouldplot, Object save, Object label, Object path)
    {
      return mcr.EvaluateFunction(numArgsOut, "RunSingle", trigger1Data, trigger2Data, minbins, maxbins, bestnum, mostvar, shouldplot, save, label, path);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runSurrogate()
    {
      return mcr.EvaluateFunction("runSurrogate", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runSurrogate(Object y)
    {
      return mcr.EvaluateFunction("runSurrogate", y);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runSurrogate(Object y, Object amp)
    {
      return mcr.EvaluateFunction("runSurrogate", y, amp);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <param name="numiters">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object runSurrogate(Object y, Object amp, Object numiters)
    {
      return mcr.EvaluateFunction("runSurrogate", y, amp, numiters);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runSurrogate(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "runSurrogate", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runSurrogate(int numArgsOut, Object y)
    {
      return mcr.EvaluateFunction(numArgsOut, "runSurrogate", y);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runSurrogate(int numArgsOut, Object y, Object amp)
    {
      return mcr.EvaluateFunction(numArgsOut, "runSurrogate", y, amp);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the runSurrogate M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="amp">Input argument #2</param>
    /// <param name="numiters">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] runSurrogate(int numArgsOut, Object y, Object amp, Object numiters)
    {
      return mcr.EvaluateFunction(numArgsOut, "runSurrogate", y, amp, numiters);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the ShannonEntropy
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ShannonEntropy()
    {
      return mcr.EvaluateFunction("ShannonEntropy", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the ShannonEntropy
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="myhisto">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ShannonEntropy(Object myhisto)
    {
      return mcr.EvaluateFunction("ShannonEntropy", myhisto);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the ShannonEntropy
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="myhisto">Input argument #1</param>
    /// <param name="verboseTF">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ShannonEntropy(Object myhisto, Object verboseTF)
    {
      return mcr.EvaluateFunction("ShannonEntropy", myhisto, verboseTF);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the ShannonEntropy
    /// M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="myhisto">Input argument #1</param>
    /// <param name="verboseTF">Input argument #2</param>
    /// <param name="Nbins">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object ShannonEntropy(Object myhisto, Object verboseTF, Object Nbins)
    {
      return mcr.EvaluateFunction("ShannonEntropy", myhisto, verboseTF, Nbins);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the ShannonEntropy M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ShannonEntropy(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "ShannonEntropy", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the ShannonEntropy M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="myhisto">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ShannonEntropy(int numArgsOut, Object myhisto)
    {
      return mcr.EvaluateFunction(numArgsOut, "ShannonEntropy", myhisto);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the ShannonEntropy M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="myhisto">Input argument #1</param>
    /// <param name="verboseTF">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ShannonEntropy(int numArgsOut, Object myhisto, Object verboseTF)
    {
      return mcr.EvaluateFunction(numArgsOut, "ShannonEntropy", myhisto, verboseTF);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the ShannonEntropy M-function.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="myhisto">Input argument #1</param>
    /// <param name="verboseTF">Input argument #2</param>
    /// <param name="Nbins">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] ShannonEntropy(int numArgsOut, Object myhisto, Object verboseTF, 
                             Object Nbins)
    {
      return mcr.EvaluateFunction(numArgsOut, "ShannonEntropy", myhisto, verboseTF, Nbins);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object sortem()
    {
      return mcr.EvaluateFunction("sortem", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <param name="vectors_in1">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object sortem(Object vectors_in1)
    {
      return mcr.EvaluateFunction("sortem", vectors_in1);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <param name="vectors_in1">Input argument #1</param>
    /// <param name="values_in1">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object sortem(Object vectors_in1, Object values_in1)
    {
      return mcr.EvaluateFunction("sortem", vectors_in1, values_in1);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] sortem(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "sortem", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="vectors_in1">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] sortem(int numArgsOut, Object vectors_in1)
    {
      return mcr.EvaluateFunction(numArgsOut, "sortem", vectors_in1);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the sortem M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// this error message is directly from Matthew Dailey's sortem.m
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="vectors_in1">Input argument #1</param>
    /// <param name="values_in1">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] sortem(int numArgsOut, Object vectors_in1, Object values_in1)
    {
      return mcr.EvaluateFunction(numArgsOut, "sortem", vectors_in1, values_in1);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1()
    {
      return mcr.EvaluateFunction("surr_1", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1(Object y)
    {
      return mcr.EvaluateFunction("surr_1", y);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1(Object y, Object up)
    {
      return mcr.EvaluateFunction("surr_1", y, up);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1(Object y, Object up, Object dn)
    {
      return mcr.EvaluateFunction("surr_1", y, up, dn);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <param name="option">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1(Object y, Object up, Object dn, Object option)
    {
      return mcr.EvaluateFunction("surr_1", y, up, dn, option);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <param name="option">Input argument #4</param>
    /// <param name="numiters">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object surr_1(Object y, Object up, Object dn, Object option, Object numiters)
    {
      return mcr.EvaluateFunction("surr_1", y, up, dn, option, numiters);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut, Object y)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", y);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut, Object y, Object up)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut, Object y, Object up, Object dn)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up, dn);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <param name="option">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut, Object y, Object up, Object dn, Object option)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up, dn, option);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the surr_1 M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// y=y(up(1):length(y));
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="y">Input argument #1</param>
    /// <param name="up">Input argument #2</param>
    /// <param name="dn">Input argument #3</param>
    /// <param name="option">Input argument #4</param>
    /// <param name="numiters">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] surr_1(int numArgsOut, Object y, Object up, Object dn, Object option, 
                     Object numiters)
    {
      return mcr.EvaluateFunction(numArgsOut, "surr_1", y, up, dn, option, numiters);
    }


    /// <summary>
    /// Provides a single output, 0-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration()
    {
      return mcr.EvaluateFunction("titration", new Object[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data)
    {
      return mcr.EvaluateFunction("titration", data);
    }


    /// <summary>
    /// Provides a single output, 2-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit)
    {
      return mcr.EvaluateFunction("titration", data, tit);
    }


    /// <summary>
    /// Provides a single output, 3-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit, Object kappa)
    {
      return mcr.EvaluateFunction("titration", data, tit, kappa);
    }


    /// <summary>
    /// Provides a single output, 4-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit, Object kappa, Object degree)
    {
      return mcr.EvaluateFunction("titration", data, tit, kappa, degree);
    }


    /// <summary>
    /// Provides a single output, 5-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit, Object kappa, Object degree, Object 
                      noise_step)
    {
      return mcr.EvaluateFunction("titration", data, tit, kappa, degree, noise_step);
    }


    /// <summary>
    /// Provides a single output, 6-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <param name="pp">Input argument #6</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit, Object kappa, Object degree, Object 
                      noise_step, Object pp)
    {
      return mcr.EvaluateFunction("titration", data, tit, kappa, degree, noise_step, pp);
    }


    /// <summary>
    /// Provides a single output, 7-input Objectinterface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <param name="pp">Input argument #6</param>
    /// <param name="num_run">Input argument #7</param>
    /// <returns>An Object containing the first output argument.</returns>
    ///
    public Object titration(Object data, Object tit, Object kappa, Object degree, Object 
                      noise_step, Object pp, Object num_run)
    {
      return mcr.EvaluateFunction("titration", data, tit, kappa, degree, noise_step, pp, num_run);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data);
    }


    /// <summary>
    /// Provides the standard 2-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit);
    }


    /// <summary>
    /// Provides the standard 3-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit, Object kappa)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit, kappa);
    }


    /// <summary>
    /// Provides the standard 4-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit, Object kappa, 
                        Object degree)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit, kappa, degree);
    }


    /// <summary>
    /// Provides the standard 5-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit, Object kappa, 
                        Object degree, Object noise_step)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit, kappa, degree, noise_step);
    }


    /// <summary>
    /// Provides the standard 6-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <param name="pp">Input argument #6</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit, Object kappa, 
                        Object degree, Object noise_step, Object pp)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit, kappa, degree, noise_step, pp);
    }


    /// <summary>
    /// Provides the standard 7-input Object interface to the titration M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// Titration of Chaos with added noise
    /// [CofR2, CofR1, Pvalue,
    /// NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
    /// This program will first check if the input data is nonlinear by Akaike cost
    /// function
    /// and by an F-test significance level p&lt;.05.  Titration can only be done if
    /// nonlinearity is
    /// detected. 
    /// Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc.
    /// Natl. Acad. Sci. 2000
    /// Outputs: CofR2 = C(r) for the nonlinear estimate
    /// CofR1 = C(r) for the linear estimate
    /// Pvalue = the pvalue of F-test before titration
    /// NoiseLimit = the minimum amount(  ) of noise added that prevents nonlinear
    /// detection by Akaike and F-test
    /// C(r) is the Akaike cost function and r is the number of polynomial terms
    /// of the nonlinear or linear estimate of the time series using the 
    /// Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature
    /// 1996.
    /// Inputs: data = the discrete time series; 
    /// tit = 1 for titration or tit = 0 for nonlinear detection only
    /// kappa = the memory order of the Volterra series; 
    /// degree = the nonlinear degree of the Volterra series; 
    /// noise_step = the increment step size of added white noise in decimal (.01 for 1  
    /// of signal power); 
    /// pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
    /// num_run = the number of titration runs
    /// [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
    /// defaults to:
    /// tit = 1;    
    /// kappa = 6; 
    /// degree = 3;
    /// noise_step = .01; 1   of signal power
    /// pp = 1; 
    /// num_run=1.
    /// During titration with added noise, NoiseLimit is averaged over 
    /// multiple runs (specified by num_run). 
    /// num_run should be greater or equal to 1. For larger values of num_run 
    /// and smaller values of noise step, NoiseLimit will be more accurate but will
    /// increase 
    /// processing time.
    /// Code developed by Laurence Zapanta and Sung Il Kim 11/2003
    /// "Statistics Toolbox for Matlab is needed for this code" 
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="tit">Input argument #2</param>
    /// <param name="kappa">Input argument #3</param>
    /// <param name="degree">Input argument #4</param>
    /// <param name="noise_step">Input argument #5</param>
    /// <param name="pp">Input argument #6</param>
    /// <param name="num_run">Input argument #7</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] titration(int numArgsOut, Object data, Object tit, Object kappa, 
                        Object degree, Object noise_step, Object pp, Object num_run)
    {
      return mcr.EvaluateFunction(numArgsOut, "titration", data, tit, kappa, degree, noise_step, pp, num_run);
    }


    /// <summary>
    /// This method will cause a MATLAB figure window to behave as a modal dialog box.
    /// The method will not return until all the figure windows associated with this
    /// component have been closed.
    /// </summary>
    /// <remarks>
    /// An application should only call this method when required to keep the
    /// MATLAB figure window from disappearing.  Other techniques, such as calling
    /// Console.ReadLine() from the application should be considered where
    /// possible.</remarks>
    ///
    public void WaitForFiguresToDie()
    {
      mcr.WaitForFiguresToDie();
    }



    #endregion Methods

    #region Class Members

    private static MWMCR mcr= null;

    private bool disposed= false;

    #endregion Class Members
  }
}
