function [ChiRAW,ChiGOF,pRAW,pGOF,dfRAW,dfGOF,RelSE_UNI,RelSE_EXP]=...
         ShannonEntropynew(myhisto,verboseTF,Nbins,pMAX,x,edges,WhichHisto,PathName,SubjectLabel)

N=sum(myhisto);
k=Nbins;
%
%                   CHI SQUARE CALCULATIONS
%
%
% Raw Data
Observed=myhisto(1:Nbins)';
Expected=pMAX*N;

ObsMinusExp=Observed-Expected;
ChiRAW=sum((ObsMinusExp.*ObsMinusExp)./Expected);
dfRAW=k-1;
pRAW=1-chi2cdf(ChiRAW,k-1);

%Matlab GOF test.
[h,pGOF,st]=chi2gof(x,'edges',edges,'expected',Expected);
ChiGOF = st.chi2stat;
dfGOF = st.df;
edgesback=st.edges;
ObservedBack=st.O;
ExpectedBack=st.E;

% %MultiNomial Test http://en.wikipedia.org/wiki/Multinomial_test
% Pie=pMAX;
% x=Observed;
% p=x./N;
% Observedis0=find(Observed==0);
% p(Observedis0)=[];
% Pie(Observedis0)=[];
% x(Observedis0)=[];
% ChiMULTI=-2*sum(x.*log(Pie./p));
% Correction1=1+((k+1)/(6*N))+(k^2/(6*N^2));
% numer=0;
% for i = 1:length(Pie)
%     numer=numer+(1/Pie(i))-1;
% end
% denom=6*N*(k-1);
% Correction2=1+(numer/denom);
% Label=['**********' SubjectLabel '_' BreathType '_' '*****' num2str(WhichHisto) ' Bin, Left ********'];
% ChiMULTI=ChiMULTI/Correction2;
% dfMULTI=k-1-length(Observedis0);
% pvalMULTI=1-chi2cdf(ChiMULTI,dfMULTI);

%[pResample] = ResampleChi(Observed,pMAX,WhichHisto,PathName,SubjectLabel,LeastOrMost);

if verboseTF
    fprintf('%s\n',[SubjectLabel '_'  num2str(WhichHisto) '_Bins=' num2str(Nbins)]);

    c=[pRAW pGOF];
    cp=100*((max(c)-min(c))/min(c));
    fprintf('| ChiRAW = %6.1f | ChiGOF = %6.1f |\n| pRAW=%7.6f   | pGOF = %7.6f  | Percent= %6.2f  |\n| dfRAW =%2.2d       | dfGOF=%2.2d        \n',...
               ChiRAW         , ChiGOF            , pRAW         , pGOF                , cp                 , dfRAW              , dfGOF);
    
    if cp > 10;
        fprintf('\n\n| max(p)=%7.6f | min(p)=%7.6f | PerChg=%6.2f\n',max(c),min(c),cp);
    end;
    fprintf('%s\n',[SubjectLabel '_' num2str(WhichHisto) '_Bins=' num2str(Nbins)]);

    for i= 1:length(edges)-1
        if i <= length(ObservedBack);
            if edges(i) ~= edgesback(i) || Observed(i) ~= ObservedBack(i) || Expected(i) ~= ExpectedBack(i);Astk='*';end;
            if edges(i) == edgesback(i) && Observed(i) == ObservedBack(i) && Expected(i) == ExpectedBack(i);Astk=' ';end;
            fprintf('%sB#%2.2d,Oedg=%4.0f,Nedg=%4.0f,OOb=%3.3d,NOb=%3.3d,OExp=%6.2f,NExp=%6.2f\n',...
                     Astk,i,edges(i),edgesback(i),Observed(i),ObservedBack(i),Expected(i),ExpectedBack(i))
        end
        if i > length(ObservedBack);
            Astk='*';
            fprintf('%sB#%2.2d,Oedg=%4.0f,Nedg=    ,OOb=%3.3d,NOb=   ,OExp=%6.2f,NExp=      \n',...
                     Astk,i,      edges(i),                 Observed(i),        Expected(i));
        end
    end;
end;
%
%                   SHANNON ENTROPY CALCULATIONS
%
%

myhisto = myhisto(1:14);
Nbins = Nbins - 1;
N=sum(myhisto);
k=Nbins;
SE=0;
MaxSE=0;
x=Observed;
p=x./N;
for i = 1:k
    if p(i)    > 0.0;SE   =SE   +(p(i)   *log(p(i)   ));end;   
    if pMAX(i) > 0.0;MaxSE=MaxSE+(pMAX(i)*log(pMAX(i)));end;
end
if    SE < 0;   SE=-1*   SE;end;
if MaxSE < 0;MaxSE=-1*MaxSE;end;
OldMaxSE = -log(1/k);
RelSE_UNI = SE/OldMaxSE;
RelSE_EXP = SE/MaxSE;

if strcmpi(verboseTF,'T')
    fprintf('Nbins=%2.2d | Total N = %4.4d | Sum(p)=%5.4f | Sum(pMAX)=%5.4f\n',Nbins,N,sum(p),sum(pMAX));
    for i=1:Nbins;
         fprintf('bin=%2.2d | Observed=%2.2d | Expected=%5.2f | p = %4.3f | pRandom=%10.5f\n',i,myhisto(i),pMAX(i)*N,p(i),pMAX(i))
    end
    fprintf('| RawSE=%4.3f \n| MaxSE_new=%4.3f | MaxSE_old=%4.3f \n| RelSE_new=%4.3f | RelSE_old=%4.3f\n',...
            SE,MaxSE,OldMaxSE,RelSE,SE/OldMaxSE);
    pause
end;  

return