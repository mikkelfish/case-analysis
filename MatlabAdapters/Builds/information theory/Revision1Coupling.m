function Revision1Coupling(BreathType,PathName,FileName,SaveHistoTF,FastTF,SubjectLabel,LeastOrMost,EPSorJPG)

verboseTF='F';
LotsOfLabels_TF='T';
%figpos=[1742         194         921         642];
%figpos=[50 50 900 600];
%figpos=[1281         101        1280         781];
%figpos=[1593         105        1280         781];
figpos=[115          58        1280         781];
set(0, 'DefaultAxesFontName', 'Arial');
if ~strcmp(BreathType,'I') &&  ~strcmp(BreathType,'E');
    fprintf('Illegal BreathType = %s, Try I or E\n',BreathType)
    %gun
    return
end

%*** INITIAL STUFF ********************************************************
close all

%*** GET MAT FILE *********************************************************
FullPath=[PathName FileName]
[pathstr, NameOnly, ext]=fileparts(FullPath);
NameOnly(findstr(NameOnly,'_'))='-';
load(FullPath);
NumPrevHR=length(TimeToPreviousHR);
NumNextHR=length(TimeToNextHR);

[Pre_DWP,Pre_DW,Next_DWP,Next_DW,MaxAbsValACF,MedianAbsValACF,BGrsqPrev,BGChiPrev,BGpPrev,BGrsqNext,BGChiNext,BGpNext,RHO,PVAL]=...
         AutoCorrelationAnalysis(TimeToPreviousHR,TimeToNextHR,figpos,PathName,NameOnly,BreathType,FastTF,LeastOrMost);

NameIndex=strfind(FullPath,'-');
FullPath=[FullPath(1:NameIndex-1) '_10BinEdges.csv'];
a10=csvread(FullPath);
FullPath=[FullPath(1:NameIndex-1) '_14BinEdges.csv'];
a14=csvread(FullPath);
MaxBinValue10=a10(:,1);
Proportion10 =a10(1:10,2);
MaxBinValue14=a14(:,1);
Proportion14 =a14(1:14,2);

ChiRAW=zeros(4,1);
ChiGOF=ChiRAW;
pRAW  =ChiRAW;
pGOF  =ChiRAW;
dfRAW =ChiRAW;
dfGOF =ChiRAW;
pResample=ChiRAW;
SEUNIarray=zeros(4,1);
SEEXParray=zeros(4,1);
NumHR=zeros(4,1);
%ChiRAW,ChiGOF,pRAW,pGOF,dfRAW ,dfGOF,SEUNIarray;

WhichGraph=0;
for i = 1:2
    
    if i == 1;MaxBinValue=MaxBinValue10;NumBins=10;Proportion=Proportion10;end;
    if i == 2;MaxBinValue=MaxBinValue14;NumBins=14;Proportion=Proportion14;end;

%     HistoPreviousM=histc(TimeToPreviousHR*1000,MaxBinValue);
%     HistoNextM    =histc(TimeToNextHR*1000,MaxBinValue);
    HistoPrevious=myhistc(TimeToPreviousHR*1000,MaxBinValue);
    HistoNext    =myhistc(TimeToNextHR*1000,MaxBinValue);
    %commandwindow();
    
    WhichGraph=WhichGraph+1;
    NumHR(WhichGraph)=sum(HistoPrevious);
    [ChiRAW(WhichGraph),ChiGOF(WhichGraph),pRAW(WhichGraph),pGOF(WhichGraph),dfRAW(WhichGraph),dfGOF(WhichGraph),SEUNIarray(WhichGraph),SEEXParray(WhichGraph),pResample(WhichGraph)]=...
    ShannonEntropy(HistoPrevious,verboseTF,NumBins,Proportion,TimeToPreviousHR*1000,MaxBinValue,WhichGraph,BreathType,PathName,SubjectLabel,LeastOrMost);
    [titstrPre]=BuildTitle(ChiRAW(WhichGraph),dfRAW(WhichGraph),pRAW(WhichGraph),SEUNIarray(WhichGraph),SEEXParray(WhichGraph),NumHR(WhichGraph),MaxBinValue(2));

    WhichGraph=WhichGraph+1;
    NumHR(WhichGraph)=sum(HistoNext);
    [ChiRAW(WhichGraph),ChiGOF(WhichGraph),pRAW(WhichGraph),pGOF(WhichGraph),dfRAW(WhichGraph),dfGOF(WhichGraph),SEUNIarray(WhichGraph),SEEXParray(WhichGraph),pResample(WhichGraph)]=...
    ShannonEntropy(HistoNext,verboseTF,NumBins,Proportion,TimeToNextHR*1000,MaxBinValue,WhichGraph,BreathType,PathName,SubjectLabel,LeastOrMost);
    [titstrNext]=BuildTitle(ChiRAW(WhichGraph),dfRAW(WhichGraph),pRAW(WhichGraph),SEUNIarray(WhichGraph),SEEXParray(WhichGraph),NumHR(WhichGraph),MaxBinValue(2));
    
    figure(1);set(1,'units','pixels','position',figpos,'menubar','none','color',[1 1 1]);
    subplot(121);
    negedges=-1*MaxBinValue;
    centers=zeros(NumBins,1);
    for j=1:NumBins;
        centers(j)=(negedges(j)+negedges(j+1))/2;
    end;
    H_Bar=bar(centers,HistoPrevious);
    set(H_Bar,'FaceColor','none','linewidth',4)
    set(gca,'FontSize',20)
    ylim([0 50]);
    xlim([centers(NumBins)*1.1 0]);
    Expected=Proportion*sum(HistoPrevious);
    hold on;plot(centers,Expected,'-k','linewidth',2);
    hold on;plot(centers,Expected,'ok','linewidth',5);
    set(gca,'Xtick',centers(NumBins:-1:1),'XTickLabel',centers(NumBins:-1:1),'FontSize',9,'tickdir','out');
    if strcmp(BreathType,'I');
       xlabel('ms Before Inspiration','FontSize',20);
    else
       xlabel('ms Before Expiration','FontSize',20);
    end;
    text(mean(centers),45,titstrPre,'HorizontalAlignment','center','FontSize',16);    
    

    set(gca,'tickdir','out')
    box off;
    pos1=get(gca,'position');
    pos1(1)=0.05;
    pos1(3)=0.45;
    set(gca,'position',pos1);
    set(gca,'linewidth',2);

    subplot(122)
    centers=centers*-1;
    H_Bar=bar(centers,HistoNext);
    set(H_Bar,'FaceColor','none','linewidth',4)
    set(gca,'FontSize',20)
    ylim([0 50]);
    xlim([0 centers(NumBins)*1.1]);
    Expected=Proportion*sum(HistoNext);
    hold on;plot(centers,Expected,'-k','linewidth',2);
    hold on;plot(centers,Expected,'ok','linewidth',5);
    set(gca,'Xtick',[0; centers(1:NumBins);],'XTickLabel',[0; centers(1:NumBins);],'FontSize',9,'tickdir','out');
    if strcmp(BreathType,'I');
       xlabel('ms After Inspiration','FontSize',20);
    else
       xlabel('ms After Expiration','FontSize',20);
    end;
    text(mean(centers),45,titstrNext,'HorizontalAlignment','center','FontSize',16);    
    set(gca,'Ytick',[],'YTickLabel',[]);
    
    box off;
    pos2=get(gca,'position');
    pos2(1)=0.50;
    pos2(3)=0.45;
    set(gca,'position',pos2);
    set(gca,'linewidth',2);
   
   if strcmpi(LotsOfLabels_TF,'T');
        if strcmp(BreathType,'I');
%             titstr={[NameOnly ' | Trigger = Inspiration | N Breaths = ' num2str(NumPrevHR)];
%                     [' | N Bins = ' num2str(NumBins) ' | BinWidth= ' num2str(BinWidth,'%5.1f') ' ms | MaxBinEdge= ' num2str(BinWidth*NumBins,'%5.1f') ' ms | ']};
            titstr=[SubjectLabel ' | Inspiration | N Breaths = ' num2str(NumPrevHR) ' | N Bins = ' num2str(NumBins) ' | LorM = ' LeastOrMost];
            H_ST=suptitle(titstr);
            set(H_ST,'FontSize',20)
            %'.eps'],'-deps',);
        else
%             titstr={[NameOnly ' | Trigger = Expiration | N Breaths = ' num2str(NumPrevHR)];
%                     [' | N Bins = ' num2str(NumBins) ' | BinWidth= ' num2str(BinWidth,'%5.1f')  ' ms | MaxBinEdge= ' num2str(BinWidth*NumBins,'%5.1f') ' ms']};
            titstr=[SubjectLabel ' | Expiration | N Breaths = ' num2str(NumPrevHR) ' | N Bins = ' num2str(NumBins) ' | LorM = ' LeastOrMost];
            H_ST=suptitle(titstr);
            set(H_ST,'FontSize',20)
            %'.jpg'],'-djpeg','-r140');
        end;
    else
        suptitle(' ');
    end;
	

    if SaveHistoTF;
         if strcmpi(EPSorJPG,'J');
             FigOutName=[PathName '006_' NameOnly '_Histograms_' BreathType '_' num2str(NumBins,'%2.2d') '_' LeastOrMost '.jpg'];
             export_fig(FigOutName);
         else
             FigOutName=[PathName '006_' NameOnly '_Histograms_' BreathType '_' num2str(NumBins,'%2.2d') '_' LeastOrMost];                 
             %export_fig(FigOutName,'-q101');                 
             printeps(gcf,FigOutName);
         end;
%             if strcmpi(EPSorJPG,'J');
%                 print(gcf,[PathName '006_' NameOnly '_Histograms_' BreathType '_' num2str(NumBins,'%2.2d') '_' LeastOrMost '.jpg'],'-djpeg','-r300');
%             else
%                 print(gcf,[PathName '006_' NameOnly '_Histograms_' BreathType '_' num2str(NumBins,'%2.2d') '_' LeastOrMost '.eps'],'-deps');
%             end
    end;
    %pause
end;

if strcmpi(BreathType,'I');NBreathType=1;end;
if strcmpi(BreathType,'E');NBreathType=2;end;
if strcmpi(LeastOrMost,'L');NLeastOrMost=1;end;
if strcmpi(LeastOrMost,'M');NLeastOrMost=2;end;
fprintf('      SubNum,      Visit,NBreathType,NLeastOrMost,NumHR10Bin,             NumHR14Bin\n');
out =[str2num(SubjectLabel(4:7)),str2num(SubjectLabel(9)),NLeastOrMost,NBreathType,NumHR']
fprintf('    Pre_DW,  Pre_DWP,  _ItoR_DW, Next_DWP,BGrsqPrev,BGChiPrev,  BGpPrev,BGrsqNext,BGChiNext,  BGpNext\n');
out =[Pre_DW,Pre_DWP,Next_DW,Next_DWP,BGrsqPrev,BGChiPrev,BGpPrev,BGrsqNext,BGChiNext,BGpNext]
fprintf('ChiRaw                                  dfRAW                                       pRAW\n');
out = [ChiRAW' dfRAW' pRAW']
fprintf('ChiGOF                                  dfGOF                                       pGOF\n');
out = [ChiGOF' dfGOF' pGOF' ]
fprintf('RelSE\n');
out = [SEUNIarray']
  
% SUB             ="Subj"
% VISIT           ="VISIT"
% LorM            ="Least Or Most Variable 200 Breaths"
% BGrsqRtoE       ="RtoI Breush-Godfrey Rsq "
% BGrsqItoR       ="ItoR Breush-Godfrey Rsq"
% BGpRtoI         ="RtoI Breush-Godfrey p-value"
% BGpItoR         ="ItoR Breush-Godfrey p-value"
% DWP_RtoI        ="RtoI DurbinWatson p-value"
% DWP_ItoR        ="ItoR DurbinWatson p-value"
% p_RtoI          ="RtoI -log10 pvalue - 10 bins"
% p_ItoR          ="ItoR -log10 pvalue - 10 bins"
pRAW=-log10(pRAW);
pGOF=-log10(pGOF);
pResample=-log10(pResample);
if strcmpi(BreathType,'I');
    Header=['SUB,VISIT,N_LorM,'...
            'N_HR10_RtoI,N_HR10_ItoR,N_HR14_RtoI,N_HR14_ItoR,'...
            'BGrsqRtoI,BGrsqItoR,BGpRtoI,BGpItoR,BGChi_RtoI,BGChi_ItoR,'...
            'DW__RtoI,DWP_RtoI,DW_ItoR,DWP_ItoR,'...
            'ChiR_10_RtoI,ChiR_10_ItoR,ChiR_14_RtoI,ChiR_14_ItoR,'...
            'dfR_10_RtoI,dfR_10_ItoR,dfR_14_RtoI,dfR_14_ItoR,'...
            'pR_10_RtoI,pR_10_ItoR,pR_14_RtoI,pR_14_ItoR,'...
            'ChiG_10_RtoI,ChiG_10_ItoR,ChiG_14_RtoI,ChiG_14_ItoR,dfG_10_RtoI,dfG_10_ItoR,dfG_14_RtoI,dfG_14_ItoR,pG_10_RtoI,pG_10_ItoR,pG_14_RtoI,pG_14_ItoR,'...
            'SE_10_RtoI,SE_10_ItoR,SE_14_RtoI,SE_14_ItoR,pRes_10_RtoI,pRes_10_ItoR,pRes_14_RtoI,pRes_14_ItoR,SE_14_RtoI_EXP,SE_14_ItoR_EXP'];
end;
if strcmpi(BreathType,'E');
    Header=['SUB,VISIT,N_LorM,'...
            'N_HR10_RtoE,N_HR10_EtoR,N_HR14_RtoE,N_HR14_EtoR,'...
            'BGrsqRtoE,BGrsqEtoR,BGpRtoE,BGpEtoR,BGChi_RtoE,BGChi_EtoR,'...
            'DW__RtoE,DWP_RtoE,DW_EtoR,DWP_EtoR,'...
            'ChiR_10_RtoE,ChiR_10_EtoR,ChiR_14_RtoE,ChiR_14_EtoR,'...
            'dfR_10_RtoE,dfR_10_EtoR,dfR_14_RtoE,dfR_14_EtoR,'...
            'pR_10_RtoE,pR_10_EtoR,pR_14_RtoE,pR_14_EtoR,'...
            'ChiG_10_RtoE,ChiG_10_EtoR,ChiG_14_RtoE,ChiG_14_EtoR,'...
            'dfG_10_RtoE,dfG_10_EtoR,dfG_14_RtoE,dfG_14_EtoR,'...
            'pG_10_RtoE,pG_10_EtoR,pG_14_RtoE,pG_14_EtoR,'...
            'SE_10_RtoE,SE_10_EtoR,SE_14_RtoE,SE_14_EtoR,pRes_10_RtoE,pRes_10_EtoR,pRes_14_RtoE,pRes_14_EtoR,SE_14_RtoE_EXP,SE_14_EtoR_EXP'];
end;
out =[str2num(SubjectLabel(4:7)) str2num(SubjectLabel(9)) NLeastOrMost NumHR' BGrsqPrev BGrsqNext BGpPrev BGpNext BGChiPrev BGChiNext Pre_DW Pre_DWP Next_DW Next_DWP ChiRAW' dfRAW' pRAW' ChiGOF' dfGOF' pGOF' SEUNIarray' pResample' SEEXParray(3:4)'];
dlmwrite([PathName '007_' NameOnly '_CouplingStats' LeastOrMost '.csv'],out,'precision',8);
if strcmpi(BreathType,'I');fidh=fopen([PathName '007_I_HEADERS_CouplingStats.csv'],'w');end;
if strcmpi(BreathType,'E');fidh=fopen([PathName '007_E_HEADERS_CouplingStats.csv'],'w');end;
Header
fprintf(fidh,'%s,\n',Header);
fclose(fidh);
close all;
return
function [titstr]=BuildTitle(Chi,df,pChi,RelSEUNI,RelSEEXP,NumHR,BinWidth)
line1={['Chi-Square = ' num2str(Chi,'%6.2f')];...
       ['df = ' num2str(df,'%d') ', p = ' num2str(pChi,'%8.3f')];...
       ['Rel. Shan.Ent.(UNI) = ' num2str(RelSEUNI,'%7.3f')];...
       ['Rel. Shan.Ent.(EXP) = ' num2str(RelSEEXP,'%7.3f')];...
       ['Number of R-Waves = ' num2str(NumHR)];...
       ['Bin Width = ' num2str(BinWidth) ' ms'];};
titstr=line1;
return
