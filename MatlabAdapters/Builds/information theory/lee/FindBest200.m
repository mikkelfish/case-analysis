function FindBest200(tag,Best200orALL_B_A,LeastOrMost_L_M)

%*** INITIAL STUFF ********************************************************
clc
close all
%myposition=[1500 0 900 700];
%myposition=[1765         127         900         700];
myposition=[50         10         800         700];
if strcmpi(tag(1:3),'Sub');Prep='Hum';end;
if strcmpi(tag(1:3),'Rat');Prep='Rat';end;
if strcmpi(tag(1:3),'450');Prep='KEN';end;

%*** INITIAL STUFF ********************************************************
clc
close all

%*** GET MAT FILE *********************************************************
%[FileName,PathName] = uigetfile('F:\MatlabStrohl\*.mat','Find .mat File'); 
if ispc;
    %[FileName,PathName] = uigetfile('G:\MatlabStrohl\Human\*.mat','Find .mat File'); 
    if strcmpi(Prep,'Hum')
        FileName = [tag '_IEH.mat'];
        PathName = ['G:\MatlabStrohl\Human\' tag '\']; 
    end;
    if strcmpi(Prep,'Rat')
        FileName=  [ tag '_IEH.mat'];
        PathName = ['G:\MatlabStrohl\ECdata\' tag(1:5) '\']; 
    end        
    if strcmpi(Prep,'KEN')
        FileName=  [ tag '_IEH.mat'];
        PathName = ['G:\MatlabStrohl\KENdata\' tag '_IEH.mat']; 
    end        
end
if isunix;
    [FileName,PathName] = uigetfile('/media/AirLink101/MatlabStrohl/Frank/MechVent/exp2/*.mat','Find .mat File'); 
end
FullPath=[PathName FileName]
[PathStr, NameOnly, ext]=fileparts(FullPath)
NameOnly(findstr(NameOnly,'_'))='-';
if ispc;PathStr=[PathStr '\'];end;
if isunix;PathStr=[PathStr '/'];end;

%*** PROCESS VARIABLES ****************************************************
load([PathStr FileName]);
% s=whos('-file', FullPath)
% who
% pause

numHR=length(HR);
numEtrig=length(Etrig);
numItrig=length(Itrig);
RR=diff(HR)*1000;
I=diff(Itrig)*1000;
E=diff(Etrig)*1000;

%*** CHANGES IN EVENTS OVER TIME - ALL DATA *******************************
figure(1);set(1,'units','pixels','position',myposition,'menubar','none');
PlotIntervalsOverTime(numHR,HR,RR,numItrig,Itrig,I,numEtrig,Etrig,E);
suptitle([NameOnly '-EventsOverALLTime'])
%print(gcf,[PathStr NameOnly '_EventsOverALLTime.eps'],'-deps','-r150');
print(gcf,[PathStr NameOnly '_EventsOverALLTime.jpg'],'-djpeg','-r150');

%*** Find Best 200 ********************************************************
if strcmpi(Best200orALL_B_A,'B') && ~strcmpi(tag,'Sub1015V1')
    commandwindow;
    fprintf('Number Of Inspirations/Expirations = %d\n',min(numItrig,numEtrig))

    count=0;
    for i=1:min(numItrig,numEtrig)-200   
        count=count+1;
        meanI(count)=mean(I(i:i+198));
        stdI(count)=std(I(i:i+198));
        cvI(count)=stdI(count)/meanI(count);
    end    
    figure(2);set(2,'units','pixels','position',myposition,'menubar','none');
    x=1:count;
    subplot(311);plot(x,meanI);xlabel('First Inspiration #');ylabel('Mean Insp Interval (msec)');xlim([1 count]);
    subplot(312);plot(x,stdI) ;xlabel('First Inspiration #');ylabel('SD Insp Interval (msec)');xlim([1 count]);
    subplot(313);plot(x,cvI)  ;xlabel('First Inspiration #');ylabel('CV');;xlim([1 count]);

    minCV=min(cvI);
    maxCV=max(cvI);
    if strcmpi(LeastOrMost_L_M,'L')
        startingI=find(cvI==minCV);
    else
        startingI=find(cvI==maxCV);
    end
        titstr=[tag ' - Find 200 Breath Period with Least & Most Variation in Breathing Rate'];
    suptitle(titstr);
    x=[find(cvI==minCV) find(cvI==minCV)];
    y=[minCV maxCV];
    hold on;
    plot(x,y,':k');
    if x(1) < count/2;
        text(x(1),mean(y),['x=' num2str(x(1)) '|y='  num2str(y(1))],'HorizontalAlignment','left');
    else
        text(x(1),mean(y),['x=' num2str(x(1)) '|y='  num2str(y(1))],'HorizontalAlignment','right');
    end
    x=[find(cvI==maxCV) find(cvI==maxCV)];
    y=[minCV maxCV];
    plot(x,y,':k');
    if x(1) < count/2;
        text(x(1),mean(y),['x=' num2str(x(1)) '|y='  num2str(y(2))],'HorizontalAlignment','left');
    else
        text(x(1),mean(y),['x=' num2str(x(1)) '|y='  num2str(y(2))],'HorizontalAlignment','right');
    end
    print(gcf,[PathStr NameOnly '_FindBest200_.jpg'],'-djpeg','-r150');
    %*** Trim Data ************************************************************

    SecondStart=Itrig(startingI);
    SecondEnd=Itrig(startingI+199);

    ShouldBe200I=length(find(Itrig>=SecondStart & Itrig <= SecondEnd))
    if ShouldBe200I ~= 200;fprintf('Interval does not contain 200 Itrigs\n');return;end;
    ShouldBe200E=length(find(Etrig>=SecondStart & Etrig <= SecondEnd))
    if ShouldBe200E < 200
        for i = 1:5000
            SecondEnd=SecondEnd+.001;
            ShouldBe200I=length(find(Itrig>=SecondStart & Itrig <= SecondEnd));
            ShouldBe200E=length(find(Etrig>=SecondStart & Etrig <= SecondEnd));
            fprintf('SecondStart=%10.3f,SecondEnd=%10.3f, ShouldBe200I=%d,ShouldBe200E=%d\n',...
                     SecondStart,SecondEnd,ShouldBe200I,ShouldBe200E);
            if ShouldBe200I == 200 && ShouldBe200E == 200;break;end;
        end
    end
    HR(HR>SecondEnd)=[];
    Itrig(Itrig>SecondEnd)=[];
    Etrig(Etrig>SecondEnd)=[];

    HR(HR<SecondStart)=[];
    Itrig(Itrig<SecondStart)=[];
    Etrig(Etrig<SecondStart)=[];
    
    FileName = [tag '_IEH_Best200_' LeastOrMost_L_M '.mat'];
    save([PathStr FileName],'Itrig','Etrig','HR')

    RR=diff(HR)*1000;
    I=diff(Itrig)*1000;
    E=diff(Etrig)*1000;
    numHR=length(HR);
    numEtrig=length(Etrig);
    numItrig=length(Itrig);
 
    %*** CHANGES IN EVENTS OVER TIME - TRIMMED DATA ************************
    figure(3);set(3,'units','pixels','position',myposition,'menubar','none');
    PlotIntervalsOverTime(numHR,HR,RR,numItrig,Itrig,I,numEtrig,Etrig,E);
    if strcmpi(LeastOrMost_L_M,'L')
        titstr=[NameOnly ' | Least Variable Inspirations: Starting Sec = ' num2str(SecondStart) ' | Ending Sec = ' num2str(SecondEnd)] 
    else
        titstr=[NameOnly ' | Most Variable Inspirations: Starting Sec = ' num2str(SecondStart) ' | Ending Sec = ' num2str(SecondEnd)] 
    end
    suptitle(titstr);
    % FileName=[PathStr NameOnly '_' num2str(round(SecondStart),'%4.4d') '_' num2str(round(SecondEnd),'%4.4d') '.eps'] 
    % print(gcf,FileName,'-deps','-r150');
    FileName=[PathStr NameOnly '_' num2str(round(SecondStart),'%4.4d') '_' num2str(round(SecondEnd),'%4.4d') '.jpg'] 
    print(gcf,FileName,'-djpeg','-r150');
else
    SecondStart=HR(1);
    SecondEnd=HR(length(HR));
end
%*** INTERVAL HISTOGRAMS **************************************************

figure(4);set(4,'units','pixels','position',myposition,'menubar','none');
minRR=min(RR);
maxRR=max(RR);
RRint=(maxRR-minRR)/100;
edges=minRR:RRint:maxRR;
histoRR=histc(RR,edges);
subplot(311)
bar(edges,histoRR,'edgecolor','none','barwidth',2);
set(gca,'box','off','tickdir','out');
xlabel('RR Interval (msec)')
ylabel('Count')
meanRR=mean(RR);
sdRR=std(RR);
cvRR=sdRR/meanRR;
ylim([0 max(histoRR)*1.2]);
titstr=['R-R Intervals (msec) | Mean = ' num2str(meanRR,'%5.2f') ' | SD = ' num2str(sdRR,'%5.2f') ' | CV = ' num2str(cvRR,'%6.4f') '| cyc/min = ' num2str(mean(60000./meanRR),'%8.2f')];
text(mean(edges),max(histoRR)*1.1,titstr,'HorizontalAlignment','center');

minI=min(I);
maxI=max(I);
minE=min(E);
maxE=max(E);
maxIE=max(maxI,maxE);
minIE=min(minI,minE);
IEint=(maxIE-minIE)/100;
edges=minIE:IEint:maxIE;
histoI=histc(I,edges);
subplot(312)
bar(edges,histoI,'edgecolor','none','barwidth',2);
set(gca,'box','off','tickdir','out');
xlabel('Inspiration Interval (msec)')
ylabel('Count')
meanI=mean(I);
sdI=std(I);
cvI=sdI/meanI;
ylim([0 max(histoI)*1.2]);
titstr=['I-I Intervals (msec) | Mean = ' num2str(meanI,'%5.2f') ' | SD = ' num2str(sdI,'%5.2f') ' | CV = ' num2str(cvI,'%6.4f') '| cyc/min = ' num2str(mean(60000./meanI),'%8.2f')];
text(mean(edges),max(histoI)*1.1,titstr,'HorizontalAlignment','center');

histoE=histc(E,edges);
subplot(313)
bar(edges,histoE,'edgecolor','none','barwidth',2)
set(gca,'box','off','tickdir','out');
xlabel('Expiration Interval (msec)')
ylabel('Count')
meanE=mean(E);
sdE=std(E);
cvE=sdE/meanE;
ylim([0 max(histoE)*1.2]);
titstr=['E-E Intervals (msec) | Mean = ' num2str(meanE,'%5.2f') ' | SD = ' num2str(sdE,'%5.2f') ' | CV = ' num2str(cvE,'%6.4f') '| cyc/min = ' num2str(mean(60000./meanE),'%8.2f')];
text(mean(edges),max(histoE)*1.1,titstr,'HorizontalAlignment','center');

if strcmpi(tag,'Sub1015V1')
    SecondStart=HR(1);
    SecondEnd=HR(length(HR))
end
if strcmpi(LeastOrMost_L_M,'L');
    titstr=[NameOnly ' | Least Variable Inspirations: Starting Sec = ' num2str(SecondStart) ' | Ending Sec = ' num2str(SecondEnd)];
end
if strcmpi(LeastOrMost_L_M,'M');
    titstr=[NameOnly ' | Most Variable Inspirations: Starting Sec = ' num2str(SecondStart) ' | Ending Sec = ' num2str(SecondEnd)];
end
if strcmpi(LeastOrMost_L_M,'L');
    titstr=[NameOnly ' | Starting Sec = ' num2str(SecondStart) ' | Ending Sec = ' num2str(SecondEnd)];
end
suptitle(titstr);

% FileName=[PathStr NameOnly '_IntervalHistogram_' num2str(round(SecondStart),'%4.4d') '_' num2str(round(SecondEnd),'%4.4d') '.eps'] 
% print(gcf,FileName,'-deps','-r150');
FileName=[PathStr NameOnly '_IntervalHistogram_' num2str(round(SecondStart),'%4.4d') '_' num2str(round(SecondEnd),'%4.4d') '.jpg'] 
FileName=[PathStr NameOnly '_IntervalHistogram_' num2str(round(SecondStart),'%4.4d') '_' num2str(round(SecondEnd),'%4.4d') '.jpg'] 
print(gcf,FileName,'-djpeg','-r150');

if strcmpi(Prep,'Hum')
    SubNum=str2num(NameOnly(4:7));
    Visit=str2num(NameOnly(9));
else
    SubNum=str2num(NameOnly(4:5));
    Visit=str2num(NameOnly(7:8));
end
HR_rate =60000./RR;meanHR_rate =mean(HR_rate) ;mdnHR_rate =median(HR_rate) ;stdHR_rate =std(HR_rate );
InspRate=60000./I ;meanInspRate=mean(InspRate);mdnInspRate=median(InspRate);stdInspRate=std(InspRate);
ExpRate =60000./E ;meanExpRate =mean(ExpRate) ;mdnExpRate =median(ExpRate) ;stdExpRate =std(ExpRate );
CV_HR=stdHR_rate/meanHR_rate;
CV_I=stdInspRate/meanInspRate;
CV_E=stdExpRate /meanExpRate;

out=[SubNum,Visit,numHR,numItrig,numEtrig,HR(1),HR(length(HR)),...
meanHR_rate ,mdnHR_rate ,stdHR_rate,CV_HR,...
meanInspRate,mdnInspRate,stdInspRate,CV_I,...
meanExpRate ,mdnExpRate ,stdExpRate,CV_E];
dlmwrite([PathStr NameOnly '_BasicSTATS_' LeastOrMost_L_M '.csv'],out,'precision',8);

%*** FIND BIN WIDTH MAXIMUM ***********************************************
MaximumBinEnd=prctile(RR,0.25);
dlmwrite([PathStr NameOnly '_MaxBinEdge_' LeastOrMost_L_M '.csv'],MaximumBinEnd); 

%*** FIND AND WRITE OUT CRITICAL DATA FOR INSPIRATION COUPLING HISTOGRAMS **********************

TimeToPreviousHR=zeros(numItrig,1);
TimeToNextHR=zeros(numItrig,1);
for i=1:numItrig
    if ~isempty(find(HR==Itrig(i)));
        fprintf('Heart Beat Occurs on an Itrig\n');
        continue
    end
    if ~isempty(find(HR<Itrig(i),1,'last'));TimeToPreviousHR(i)=Itrig(i)-HR(find(HR<Itrig(i),1,'last'));end
    if ~isempty(find(HR>Itrig(i),1,'first'));TimeToNextHR(i)=HR(find(HR>Itrig(i),1,'first'))-Itrig(i);end;
end

savefile=[PathStr NameOnly '_I_Intervals_' LeastOrMost_L_M '_' num2str(round(SecondStart),'%4.4d') '_' num2str(round(SecondEnd),'%4.4d')];
save(savefile,'TimeToPreviousHR','TimeToNextHR','MaximumBinEnd');

%*** FIND AND WRITE OUT CRITICAL DATA FOR EXPIRATION COUPLING HISTOGRAMS **********************

TimeToPreviousHR=zeros(numEtrig,1);
TimeToNextHR=zeros(numEtrig,1);
for i=1:numEtrig
    if ~isempty(find(HR==Etrig(i)));
        fprintf('Heart Beat Occurs on an Etrig\n');
        continue
    end
    if ~isempty(find(HR<Etrig(i),1,'last'));TimeToPreviousHR(i)=Etrig(i)-HR(find(HR<Etrig(i),1,'last'));end;
    if ~isempty(find(HR>Etrig(i),1,'first'));TimeToNextHR(i)=HR(find(HR>Etrig(i),1,'first'))-Etrig(i);end;
end

%*** WRITE OUT CRITICAL DATA FOR COUPLING HISTOGRAMS **********************
savefile=[PathStr NameOnly '_E_Intervals_' LeastOrMost_L_M '_' num2str(round(SecondStart),'%4.4d') '_' num2str(round(SecondEnd),'%4.4d')];
save(savefile,'TimeToPreviousHR','TimeToNextHR','MaximumBinEnd');
return

function PlotIntervalsOverTime(numHR,HR,RR,numItrig,Itrig,I,numEtrig,Etrig,E)
subplot(311)
X=HR(2:numHR);
Y=RR;
plot(X,Y);
set(gca,'box','off','tickdir','out');
xlabel('Time (sec)')
ylabel('R-R Interval (msec)')
xlim([HR(2) HR(numHR)]);
ylim([min(Y) max(Y)])
h_line=refline(0,mean(Y));
set(h_line,'color','g','linewidth',3)
legend(...
['Instantaneous RR Interval (msec) | Mean = ' num2str(mean(Y),'%8.2f') ' | SD = ' num2str(std(Y),'%5.2f') ' | CV = ' num2str(std(Y)/mean(Y),'%6.4f') '| cyc/min = ' num2str(mean(60000./mean(Y)),'%8.2f')],...
'location','NorthOutside');

subplot(312)
X=Itrig(2:numItrig);
Y=I;
plot(X,Y);
set(gca,'box','off','tickdir','out');
xlabel('Time (sec)')
ylabel('I-I (msec)')
xlim([Itrig(2) Itrig(numItrig)]);
ylim([min(Y) max(Y)])
h_line=refline(0,mean(Y));
set(h_line,'color','g','linewidth',3)
legend(['Instantaneous I-I Interval (msec) | Mean = ' num2str(mean(Y),'%8.2f') ' | SD = ' num2str(std(Y),'%5.2f') ' | CV = ' num2str(std(Y)/mean(Y),'%6.4f') '| cyc/min = ' num2str(mean(60000./mean(Y)),'%8.2f')],'location','NorthOutside');

subplot(313)
X=Etrig(2:numEtrig);
Y=E;
plot(X,Y);
set(gca,'box','off','tickdir','out');
xlabel('Time (sec)')
ylabel('E-E (msec)')
xlim([Etrig(2) Etrig(numEtrig)]);
ylim([min(Y) max(Y)])
h_line=refline(0,mean(Y));
set(h_line,'color','g','linewidth',3)
legend(['Instantaneous E-E Inteval (msec) | Mean = ' num2str(mean(Y),'%8.2f') ' | SD = ' num2str(std(Y),'%5.2f') ' | CV = ' num2str(std(Y)/mean(Y),'%6.4f') '| cyc/min = ' num2str(mean(60000./mean(Y)),'%8.2f')],'location','NorthOutside');
return
