function [RelSE,SE_AS_CHI,pSE_AS_CHI]=ShannonEntropy(myhisto,verboseTF,Nbins)
N=sum(myhisto);
k=Nbins;
p=myhisto/N;
SE=0;
for i = 1:k
    if p(i) > 0.0;SE=SE+(p(i)*log(p(i)));end;   
end
if SE < 0;SE=-1*SE;end;
MaxSE = -log(1/k);
RelSE = SE/MaxSE;
SE_AS_CHI=-2*N*(SE-log(k));
Correction=1+((k+1)/(6*N))+(k^2/(6*N^2));
SE_AS_CHI=SE_AS_CHI/Correction;
pSE_AS_CHI=1-chi2cdf(SE_AS_CHI,k-1);
    
if strcmpi(verboseTF,'T')
    disp('myhisto')
    disp(myhisto)
    disp('Nevents')
    disp(N)
    disp('p')
    disp(p)
    disp('sum p')
    disp(sum(p))
    disp('SE')
    disp(SE)
end
return