function RunBaekeyCoupling

clc
clear 

SaveHistoTF=true;
FastTF=false;
commandwindow;
PathName='G:\MatlabStrohl\KENdata\'
FileName='450568-IEH_I_Intervals_X_0001_1798.mat'
load([PathName FileName])
MaximumBinEnd
MaximumBinEnd=700;
save([PathName FileName],'MaximumBinEnd','TimeToNextHR','TimeToPreviousHR');
tag='450568'
BaekeyCoupling('I',PathName,FileName,SaveHistoTF,FastTF,tag,'X')

SaveHistoTF=true;
FastTF=false;
PathName='G:\MatlabStrohl\KENdata\'
FileName='450568-IEH_E_Intervals_X_0001_1798.mat'
load([PathName FileName])
MaximumBinEnd
MaximumBinEnd=700;
save([PathName FileName],'MaximumBinEnd','TimeToNextHR','TimeToPreviousHR');
tag='450568'
BaekeyCoupling('E',PathName,FileName,SaveHistoTF,FastTF,tag,'X')

return
fid=fopen('G:\MatlabStrohl\ECdata\Rat01\Rat01_MAT_FILE_LIST.csv','r');
for i = 1:61
    C = textscan(fid, '%s', 1);
    m=cell2mat(C{1,1});
    tag=m(30:37);
    if strcmpi(tag,'Rat01D12') || strcmpi(tag,'Rat01D32');continue;end
    PathName=m(1:29);
    FileName=m(30:length(m));
    IorE='I';
    LeastOrMost='X';
    fprintf('\ntag=%s\nPathName=%s\nFileName=%s\nIorE=%s\nLeastOrMost=%s\n',tag,PathName,FileName,IorE,LeastOrMost)
%            1         1         3         4         5         6         7
%   12345678901234567890123456789012345678901234567890123456789012345678901234567890
%   G:\MatlabStrohl\ECdata\Rat01\Rat01D36-IEH_I_Intervals_X_3226_3351.mat
    BaekeyCoupling(IorE,PathName,FileName,SaveHistoTF,FastTF,tag,LeastOrMost)
end

%BaekeyCoupling('I',PathName,FileName,SaveHistoTF,FastTF,'Rat01D01','X')
return

%cd G:\MatlabStrohl\Human\
%G:\MatlabStrohl\Human>dir *Intervals*.mat /S /B > MatFileList.csv



fid=fopen('G:\MatlabStrohl\Human\MatFileList.csv','r');
for i = 1:4%4 files
    C = textscan(fid, '%s', 1);
    m=cell2mat(C{1,1});
    tag=m(33:41);
    PathName=m(1:32);
    FileName=m(33:73);
    IorE=m(47);
    LeastOrMost=m(59);
    fprintf('\ntag=%s\nPathName=%s\nFileName=%s\nIorE=%s\n',tag,PathName,FileName,IorE,LeastOrMost)
%            1         1         3         4         5         6         7
%   12345678901234567890123456789012345678901234567890123456789012345678901234567890    
%   G:\MatlabStrohl\Human\Sub1019V1\Sub1019V1-IEH_E_Intervals_0000_1498.mat
    BaekeyCoupling(IorE,PathName,FileName,SaveHistoTF,FastTF,tag,LeastOrMost)
end

return

PathName='G:\MatlabStrohl\Human\Sub1010V1\'
FileName='Sub1010V1-IEH_I_Intervals_0257_1114.mat'
BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF,'Sub1010V1')

% PathName='G:\MatlabStrohl\FrankData\030910\'
% FileName='030910-DiaEMG-09-IEH_I_Intervals_0000_0899.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF,'030910 - Set 01')

% PathName='G:\MatlabStrohl\FrankData\030910\'
% FileName='030910-DiaEMG-01-IEH_I_Intervals_0001_0899.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF,'030910 - Set 01')
% PathName='/media/AirLink101/MatlabStrohl/FrankData/021910/'
% FileName='021910-DiaEMG-02-IEH_I_Intervals_0001_0899.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF,'021910 - Set 02')

% PathName='/media/AirLink101/MatlabStrohl/FrankData/021910/'
% FileName='021910-DiaEMG-03-IEH_I_Intervals_0001_0899.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF,'021910 - Set 03')
% 
% SubjectLabel='WHBP #1'
% PathName='G:\MatlabStrohl\WHBP\100128_000\'
% FileName='100128-000 Lee_I_Intervals_AllEpochs.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF,SubjectLabel)
% 
% SubjectLabel='AnestRat #1'
% SaveHistoTF=true;FastTF=false;
% PathName='G:\MatlabStrohl\Frank\MechVent\exp2\'
% FileName='exp2-Baseline-01-IEH_I_Intervals_0001_6000.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF,SubjectLabel)
% 
% PathName='G:\MatlabStrohl\WHBP\091221_000\'
% FileName='091221-000-Lee-mtlab_I_Intervals_0001_1200.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)
% 
% PathName='G:\MatlabStrohl\WHBP\PNA\'
% FileName='PNAHR091014-0010-1800sec_I_Intervals_0001_0950.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)

% PathName='G:\MatlabStrohl\WHBP\Vagus\'
% FileName='VagusHR091014-0010-1800sec_I_Intervals_0001_0950.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)
% 
% PathName='G:\MatlabStrohl\WHBP\XII\'
% FileName='XII HR for Lee 091014-001 0-1800sec_I_Intervals_0001_0950.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)


% PathName='/media/AirLink101/MatlabStrohl/Frank/MechVent/exp2/'
% FileName='exp2-Baseline-02-IEH_I_Intervals_8700_9600.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)

% PathName='/media/AirLink101/MatlabStrohl/Frank/MechVent/exp2/'
% FileName='exp2-MechVent-00-IEH_I_Intervals_11501_12399.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)

% PathName='/media/AirLink101/MatlabStrohl/Frank/MechVent/exp2/'
% FileName='exp2-MechVent-01-IEH_I_Intervals_15101_15999.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)

% PathName='/media/AirLink101/MatlabStrohl/Frank/MechVent/exp2/'
% FileName='exp2-Recovery-00-IEH_I_Intervals_1300_1900.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)
% FileName='exp2-Recovery-01-IEH_I_Intervals_4611_5509.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)
% FileName='exp2-Recovery-02-IEH_I_Intervals_8211_9109.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)
% FileName='exp2-Recovery-03-IEH_I_Intervals_11811_12709.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)

% PathName='G:\MatlabStrohl\Greg15BaselineWeek0\'
% FileName='Greg15-Baseline(Week0)_I_Intervals_AllEpochs.mat'
% BaekeyCoupling('I',PathName,FileName,1,SaveHistoTF,FastTF)
return
