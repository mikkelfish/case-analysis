function BaekeyCoupling(BreathType,PathName,FileName,SaveHistoTF,FastTF,SubjectLabel,LeastOrMost)

%figpos=[1810 250 1200 700];
figpos=[50 50 900 600];
if strcmpi(SubjectLabel(1:3),'Sub');
    %BinNumberMax=20;
    %BinNumberMin=2
    BinNumberMax=10;
    BinNumberMin=10;
else
    BinNumberMax=10;
    BinNumberMin=10;
end    
if ~strcmp(BreathType,'I') &&  ~strcmp(BreathType,'E');
    fprintf('Illegal BreathType = %s, Try I or E\n',BreathType)
    %gun
    return
end

%*** INITIAL STUFF ********************************************************
close all

%*** GET MAT FILE *********************************************************
%SearchString=['C:\Users\lee\Desktop\MatlabStrohl\*_' BreathType '_Intervals_*.mat'];
% SearchString=['C:\Users\lee\Desktop\SineWaves\*.mat'];
% [FileName,PathName] = uigetfile(SearchString,'Find .mat File'); 
FullPath=[PathName FileName]
[pathstr, NameOnly, ext]=fileparts(FullPath);
NameOnly(findstr(NameOnly,'_'))='-';
load(FullPath);
% s=whos('-file', FullPath)

NumPrevHR=length(TimeToPreviousHR);
NumNextHR=length(TimeToNextHR);
[Pre_DWP,Pre_DW,Next_DWP,Next_DW,MaxAbsValACF,MedianAbsValACF,BGrsqPrev,BGChiPrev,BGpPrev,BGrsqNext,BGChiNext,BGpNext,RHO,PVAL]=...
         AutoCorrelationAnalysis(TimeToPreviousHR,TimeToNextHR,figpos,PathName,NameOnly,BreathType,FastTF,LeastOrMost);
MaxBinValue=zeros(BinNumberMax,1);SEarray=zeros(BinNumberMax,2);Chiarray=zeros(BinNumberMax,2);Parray=zeros(BinNumberMax,2);%1=RtoI or RtoE,2=ItoR orEtoR
verboseTF=false;
for NumBins=BinNumberMin:BinNumberMax
    MaxBinValue(NumBins)=NumBins*floor(MaximumBinEnd/NumBins);
    BinWidth=floor(MaxBinValue(NumBins)/NumBins);
    if ~FastTF;fprintf('NumBins=%d,Overall MaximumBinEnd=%f,This MaximumBinEnd=%f,BinWidth=%f\n',NumBins,MaximumBinEnd,MaxBinValue(NumBins),BinWidth);end;
    posedges=linspace(0.0,MaxBinValue(NumBins),NumBins+1)';
    HistoPrevious=histc(TimeToPreviousHR*1000,posedges);
    maxHistoPrevious=max(HistoPrevious);
    HistoNext=histc(TimeToNextHR*1000,posedges);
    maxHistoNext=max(HistoNext);
    [RelSE,SE_AS_CHI,pSE_AS_CHI]=ShannonEntropy(HistoPrevious,verboseTF,NumBins);
    [titstrPre]=BuildTitle(RelSE,SE_AS_CHI,pSE_AS_CHI);
    SEarray(NumBins,1)=RelSE;Chiarray(NumBins,1)=SE_AS_CHI;Parray(NumBins,1)=pSE_AS_CHI;
    [RelSE,SE_AS_CHI,pSE_AS_CHI]=ShannonEntropy(HistoNext,verboseTF,NumBins);
    [titstrNext]=BuildTitle(RelSE,SE_AS_CHI,pSE_AS_CHI);
    SEarray(NumBins,2)=RelSE;Chiarray(NumBins,2)=SE_AS_CHI;Parray(NumBins,2)=pSE_AS_CHI;

    if ~FastTF;
        figure(1);set(1,'units','pixels','position',figpos,'menubar','none');
        subplot(121);
        negedges=-1*flipud(posedges);
        ReverseHistoPrevious=flipud(HistoPrevious);
        ReverseHistoPrevious(1)=[];
        ReverseHistoPrevious(NumBins+1)=0;
        if NumBins==BinNumberMax;
            out=[negedges ReverseHistoPrevious];
            csvwrite([PathName '008_' NameOnly '_' BreathType '_HistoPrevious_' LeastOrMost '.csv'],out)
        end
        H_Bar=bar(negedges,ReverseHistoPrevious,'histc');
        set(H_Bar,'FaceColor','k')
        set(gca,'FontSize',20)
        h_line1=refline(0,sum(ReverseHistoPrevious)/NumBins);
        set(h_line1,'color','k','LineStyle',':')
        xlim([negedges(1) 0]);
        set(gca,'xtick',[negedges(1) 0]);
        ylim([0 max(maxHistoPrevious,maxHistoNext)*1.2]);
        if strcmp(BreathType,'I');
            xlabel('Msec Prior To Inspiration','FontSize',20);
        else
            xlabel('Msec Prior To Expiration','FontSize',20);
        end;
        set(gca,'tickdir','out')
        box off;
        text(mean(negedges),max(maxHistoPrevious,maxHistoNext)*1.18,titstrPre,'HorizontalAlignment','center','FontSize',16);    
        pos1=get(gca,'position');
        pos1(1)=0.05;
        pos1(3)=0.45;
        set(gca,'position',pos1);
        set(gca,'linewidth',2);

        subplot(122)
        H_Bar=bar(posedges,HistoNext,'histc');
        if NumBins==BinNumberMax;
            out=[posedges HistoNext];
            csvwrite([PathName '008_' NameOnly '_' BreathType '_HistoNext_' LeastOrMost '.csv'],out)
        end
        set(H_Bar,'FaceColor','k')
        set(gca,'FontSize',20)
        h_line2=refline(0,sum(HistoNext)/NumBins);
        set(h_line2,'color','k','LineStyle',':')
        xlim([0 posedges(NumBins+1)]);
        set(gca,'xtick',[0 posedges(NumBins+1)]);
        ylim([0 max(maxHistoPrevious,maxHistoNext)*1.2]);
        if strcmp(BreathType,'I');
            xlabel('Msec After Inspiration','FontSize',20);
        else
            xlabel('Msec After Expiration','FontSize',20);
        end;
        set(gca,'tickdir','out')
        set(gca,'yticklabel',[])
        set(gca,'ytick',[])
        box off;
        text(mean(posedges),max(maxHistoPrevious,maxHistoNext)*1.18,titstrNext,'HorizontalAlignment','center','FontSize',16);    
        pos2=get(gca,'position');
        pos2(1)=0.50;
        pos2(3)=0.45;
        set(gca,'position',pos2);
        set(gca,'linewidth',2);
        if strcmp(BreathType,'I');
%             titstr={[NameOnly ' | Trigger = Inspiration | N Breaths = ' num2str(NumPrevHR)];
%                     [' | N Bins = ' num2str(NumBins) ' | BinWidth= ' num2str(BinWidth,'%5.1f') ' msec | MaxBinEdge= ' num2str(BinWidth*NumBins,'%5.1f') ' msec']};
            titstr=[SubjectLabel ' | Inspiration | N Breaths = ' num2str(NumPrevHR) ' | N Bins = ' num2str(NumBins)];
            H_ST=suptitle(titstr);
            set(H_ST,'FontSize',20)
            %'.eps'],'-deps',);
             if SaveHistoTF;print(gcf,[PathName '006_' NameOnly '_Histograms_I_' num2str(NumBins,'%2.2d') '_' LeastOrMost '.jpg'],'-djpeg','-r150');end;
        else
%             titstr={[NameOnly ' | Trigger = Expiration | N Breaths = ' num2str(NumPrevHR)];
%                     [' | N Bins = ' num2str(NumBins) ' | BinWidth= ' num2str(BinWidth,'%5.1f')  ' msec | MaxBinEdge= ' num2str(BinWidth*NumBins,'%5.1f') ' msec']};
            titstr=[SubjectLabel ' | Expiration | N Breaths = ' num2str(NumPrevHR) ' | N Bins = ' num2str(NumBins)];
            H_ST=suptitle(titstr);
            set(H_ST,'FontSize',20)
            %'.jpg'],'-djpeg','-r150');
            if SaveHistoTF;print(gcf,[PathName '006_' NameOnly '_Histograms_E_' num2str(NumBins,'%2.2d') '_' LeastOrMost '.jpg'],'-djpeg','-r150');end;
        end;
    end;
end
close all;

if ~FastTF && BinNumberMin ~= BinNumberMax;

    figure(1);set(1,'units','pixels','position',figpos,'menubar','none');
    NumBins=(1:BinNumberMax)';

    x=[NumBins(2:BinNumberMax);NumBins(2:BinNumberMax)];
    y=[SEarray(2:BinNumberMax,1);SEarray(2:BinNumberMax,2)];
    for i = 1:BinNumberMax-1;
        group(i,1:9)='pre-Event';
    end
    for i = BinNumberMax:(2*BinNumberMax)-2;
        group(i,1:9)='postEvent';
    end
    suptitle([NameOnly ' | Shannon Entropy as a Function of N Bins']);
    gscatter(x,y,group,'gb','**');
    lsline;
    minSE_Pre =min(SEarray(2:BinNumberMax,1));
    minSE_Next=min(SEarray(2:BinNumberMax,2));
    minSE=min(minSE_Pre,minSE_Next);
    0.999*minSE;
    ylim([minSE 1]);
    % grid on;

    xlabel('N (bins)')
    ylabel('Relative Shannon Entropy')
    if strcmp(BreathType,'I');title('Event=Inspiration');else;title('Event=Expiration');end
    if strcmp(BreathType,'I');
        %print(gcf,[PathName '003_' NameOnly '_SEoverNbins_I_.eps'],'-deps','-r150');
        print(gcf,[PathName '004_' NameOnly '_SEoverNbins_I_' LeastOrMost '.jpg'],'-djpeg','-r150');
    else
        %print(gcf,[PathName '003_' NameOnly '_SEoverNbins_E_.eps'],'-deps','-r150');
        print(gcf,[PathName '004_' NameOnly '_SEoverNbins_E_' LeastOrMost '.jpg'],'-djpeg','-r150');
    end;

    
    figure(1);set(1,'units','pixels','position',figpos,'menubar','none');
    subplot(121)
    plot(NumBins(2:BinNumberMax),Parray(2:BinNumberMax,1),'.k')
    ylim([0 1]);
    % grid on;
    xlabel('N (bins)')
    ylabel('p-value')
    % xa = [2 BinNumberMax];
    % ya = [0.05 0.05];
    % [xaf,yaf] = ds2nfu(xa,ya);
    % annotation('line',xaf,yaf,'color','r','linewidth',2);
    hline=refline(0,0.05);
    set(hline,'color','r','linewidth',2)
    if strcmp(BreathType,'I');title('RI Intervals');else;title('RE Intervals');end

    subplot(122)
    plot(NumBins(2:BinNumberMax),Parray(2:BinNumberMax,2),'.k')
    ylim([0 1]);
    % grid on;
    xlabel('N (bins)')
    ylabel('p-value')
    % xa = [2 BinNumberMax];
    % ya = [0.05 0.05];
    % [xaf,yaf] = ds2nfu(xa,ya);
    % annotation('line',xaf,yaf,'color','r','linewidth',2);
    hline=refline(0,0.05);
    set(hline,'color','r','linewidth',2)
    if strcmp(BreathType,'I');title('IR Intervals');else;title('ER Intervals');end
    suptitle([NameOnly ' | p-value as a Function of N Bins']);
    if strcmp(BreathType,'I');
       %print(gcf,[PathName '004_' NameOnly '_PvalueoverNbins_I_.eps'],'-deps','-r150');
       print(gcf,[PathName '005_' NameOnly '_PvalueoverNbins_I_' LeastOrMost '.jpg'],'-djpeg','-r150');
    else
       %print(gcf,[PathName '004_' NameOnly '_PvalueoverNbins_E_.eps'],'-deps','-r150');
       print(gcf,[PathName '005_' NameOnly '_PvalueoverNbins_E_' LeastOrMost '.jpg'],'-djpeg','-r150');
    end
end;

if strcmpi(SubjectLabel(1:3),'Sub')
    SubjectLabel
    SubNum=str2num(SubjectLabel(4:7))
    Visit =str2num(SubjectLabel(9))
else
    SubjectLabel
    SubNum=str2num(SubjectLabel(4:5))
    Visit =str2num(SubjectLabel(7:8))
end
if strcmp(BreathType,'I')
    NBreathType=1;
else
    if strcmp(BreathType,'E');
        NBreathType=2;
    end
end

Parray=-log10(Parray)

out=[SubNum,Visit,NBreathType,NumPrevHR,NumNextHR,...
    Pre_DW,Pre_DWP,Next_DW,Next_DWP,MaxAbsValACF,MedianAbsValACF,BGrsqPrev,BGChiPrev,BGpPrev,BGrsqNext,BGChiNext,BGpNext,...
    RHO,PVAL,...
    Chiarray(2:BinNumberMax,1)',Chiarray(2:BinNumberMax,2)',...
    Parray(2:BinNumberMax,1)',Parray(2:BinNumberMax,2)',...
    SEarray(2:BinNumberMax,1)',SEarray(2:BinNumberMax,2)'];
dlmwrite([PathName '007_' NameOnly '_CouplingStats' LeastOrMost '.csv'],out,'precision',8)

fidh=fopen([PathName '007_HEADERS_CouplingStats.csv'],'w');
[headers]=CreateHeaders();
size(headers);
for i = 1:133
    fprintf(fidh,'%s,',headers{i});
end
fclose(fidh);
%handel
close all;
return

function [titstr]=BuildTitle(RelSE,SE_AS_CHI,pSE_AS_CHI,NameOnly)
line1={['Chi-Square = ' num2str(SE_AS_CHI,'%6.2f')];...
       ['p = ' num2str(pSE_AS_CHI,'%8.3f')        ];...
       ['Rel. Shan.Ent. = ' num2str(RelSE,'%7.3f')];};
titstr=line1;
return

function [Pre_DWP,Pre_DW,Next_DWP,Next_DW,MaxAbsValACF,MedianAbsValACF,...
         BGrsqPrev,BGChiPrev,BGpPrev,BGrsqNext,BGChiNext,BGpNext,RHO,PVAL]=...
         AutoCorrelationAnalysis(TimeToPreviousHR,TimeToNextHR,figpos,PathName,NameOnly,BreathType,FastTF,LeastOrMost)

NumPrevHR=length(TimeToPreviousHR);
x=(1:NumPrevHR);
whichstats = {'r','tstat'};
Pre_stats = regstats(TimeToPreviousHR,x,'linear',whichstats);
[Pre_DWP,Pre_DW]=dwtest(Pre_stats.r,ones(NumPrevHR,1));
[ACF_Pre_Interval,lags]=xcorr(Pre_stats.r,20,'coeff');
pACF_Pre_Interval=ACF_Pre_Interval(lags >= 0);
AbsValACF=abs(pACF_Pre_Interval(2:10));
MaxAbsValACF=max(AbsValACF);
MedianAbsValACF=median(AbsValACF);
pLags=lags(lags >= 0);
[BGrsqPrev,BGChiPrev,BGpPrev]=BGTest(Pre_stats.r,x,pACF_Pre_Interval,pLags);

NumNextHR=length(TimeToNextHR);
if NumNextHR ~= NumPrevHR;
    fprintf('The Number of Pre and Next Intervals is not equal: %d,%d\n',NumNextHR,NumPrevHR);
    return
end
x=(1:NumNextHR);
Next_stats = regstats(TimeToNextHR,x,'linear',whichstats);
[Next_DWP,Next_DW]=dwtest(Next_stats.r,ones(NumNextHR,1));
[ACF_Next_Interval,lags]=xcorr(Next_stats.r,20,'coeff');
pACF_Next_Interval=ACF_Next_Interval(lags >= 0);
[BGrsqNext,BGChiNext,BGpNext]=BGTest(Next_stats.r,x,pACF_Next_Interval,pLags);

[RHO,PVAL] = corr(TimeToPreviousHR,TimeToNextHR);


if ~FastTF
    figure(1);set(1,'units','pixels','position',figpos,'menubar','none');

    subplot(221);
    plot(x,TimeToPreviousHR,'.k')
    hline=lsline;set(hline,'linewidth',3)
    xlim([0 NumPrevHR]);
    xlabel('Event Number');
    ylabel('Interval (s)');
    t  =Pre_stats.tstat.t(2);
    p  =Pre_stats.tstat.pval(2);
    dfe=Pre_stats.tstat.dfe;
    titstr=['Intervals Prior To Event | t = ' num2str(t,'%5.1f') ' | p = ' num2str(p,'%6.4f')];
    %titstr=['Intervals Prior To Event'];
    title(titstr);

    subplot(222);
    hold on
    scatter(pLags,pACF_Pre_Interval,'+k');
    xlabel('ACF lag');
    ylabel('ACF coefficient');
    titstr='AutoCorrelation Analysis of Residuals';
    title(titstr);
%     mylabel={['Pre:DW=' num2str(Pre_DW,'%5.2f')];['p(2-tailed) = ' num2str(Pre_DWP,'%5.4f')];
%              ['Next:DW=' num2str(Next_DW,'%5.2f')];['p(2-tailed) = ' num2str(Next_DWP,'%5.4f')]};
    mylabel={['BG-Rsq Prev:= ' num2str(BGrsqPrev,'%4.3f')];['BG-Rsq Next:= ' num2str(BGrsqNext,'%4.3f')]};
    % myBestX=find(pACF_Pre_Interval == min(pACF_Pre_Interval));
    % myBestX=min(myBestX,15);
    text(19.5,max(pACF_Pre_Interval),mylabel,'HorizontalAlignment','right','FontName','courier');
    xlim([-1 20]);
    ylim([min(pACF_Pre_Interval) max(pACF_Pre_Interval)*1.1]);
    refline(0,0);
    scatter(pLags,pACF_Next_Interval,'og');
    legend('Pre','Post','location','SE')

    subplot(223)
    plot(x,TimeToNextHR,'.k');
    hline=lsline;set(hline,'linewidth',3)
    xlim([0 NumNextHR]);
    xlabel('Event Number');
    ylabel('Interval (s)');
    t  =Next_stats.tstat.t(2);
    p  =Next_stats.tstat.pval(2);
    dfe=Next_stats.tstat.dfe;
    titstr=['Intervals Following Event | t = ' num2str(t,'%5.1f') ' | p = ' num2str(p,'%6.4f')];
    %titstr=['Intervals Following Event'];
    title(titstr);

    subplot(224);
    plot(TimeToPreviousHR,TimeToNextHR,'.k');
    xlim([0 max(TimeToPreviousHR)])
    ylim([0 max(TimeToNextHR)])
    hline=lsline;
    set(hline,'color','r','linewidth',2);
    whichstats = {'beta'};
    NextVsPrevious_stats = regstats(TimeToNextHR,TimeToPreviousHR,'linear',whichstats);
    hrefline=refline(-1,NextVsPrevious_stats.beta(1));
    set(hrefline,'color','b','linewidth',2)
    xlabel('Time To Previous HB (s)');
    ylabel('Time To Next HB (s)');

    titstr=['Correlation: Pre vs Next | r = ' num2str(RHO,'%4.3f') ' | p =' num2str(PVAL,'%7.6f') ];
    title(titstr);
    axis square
    text(max(TimeToPreviousHR)*.95,max(TimeToPreviousHR)*.90,{'.=data';'red=lsline';'blue=equality'},'horizontalalignment','right','fontname','courier');
    suptitstr=[NameOnly '| ACF Analysis of Intervals'];
    suptitle(suptitstr);
    %'.jpg'],'-djpeg','-r150');
    print(gcf,[PathName '001_' NameOnly '_ACF_AnalysisHistograms_' BreathType '_' LeastOrMost '.jpg'],'-djpeg','-r150');
    close all

%     figure(1);set(1,'units','pixels','position',figpos,'menubar','none');
%     PredictedTimeToNext=-1*TimeToPreviousHR+max(TimeToPreviousHR);
%     ResidualTimeToNext=TimeToNextHR-PredictedTimeToNext;
%     residarray=[ResidualTimeToNext TimeToNextHR TimeToPreviousHR PredictedTimeToNext];
%     sortresid=sortrows(residarray,3);
%     for i = 1:6
%         fprintf('sorted resid, i = %d resid = %f, TimeToNextHR=%f, TimeToPreviousHR=%f, Pred=%f, Intercept=%f\n',i,sortresid(i,:),max(TimeToPreviousHR));
%     end
%     for i = NumNextHR-5:NumNextHR
%         fprintf('sorted resid, i = %d resid = %f, TimeToNextHR=%f, TimeToPreviousHR=%f, Pred=%f, Intercept=%f\n',i,sortresid(i,:),max(TimeToPreviousHR));
%     end 
%     hold on;
%     plot(x,ResidualTimeToNext,'.k')
%     xlabel('Event Number');
%     ylabel('Residual from Line of Equality (sec)')
%     titstr=[NameOnly ' - Residual from Line of Equality (TimeToNextHB)'];
%     Ysmooth=malowess(x,ResidualTimeToNext,'span',0.25);
%     plot(x,Ysmooth,'b','linewidth',3)
%     title(titstr);
%     print(gcf,[PathName '003_' NameOnly '_PrePostResidual_' BreathType '.jpg'],'-djpeg','-r150');

end;
return

function [pACF_Random_Intervals]=GetRandomACF(NumNextHR,TimeToNextHR,x,whichstats);
a=min(TimeToNextHR);
b=max(TimeToNextHR);
RandomIntervals= a + (b-a).*rand(NumNextHR,1);
Random_stats = regstats(RandomIntervals,x,'linear',whichstats);
[ACF_Random_Intervals,lags]=xcorr(Random_stats.r,20,'coeff');
pACF_Random_Intervals=ACF_Random_Intervals(lags >= 0);
return

function [headers]=CreateHeaders();
headers={...
'SubNum','Visit','NBreathType',...
'NumPrevHR','NumNextHR',...
'PRE_DW','PRE_DWP','POST_DW','POST_DWP','MaxAbsValACF','MedianAbsValACF',...
'BGrsqPrev','BGChiPrev','BGpPrev',...
'BGrsqNext','BGChiNext','BGpNext',...
'RHO','PVAL',...
'CHI_PRE_02',...
'CHI_PRE_03',...
'CHI_PRE_04',...
'CHI_PRE_05',...
'CHI_PRE_06',...
'CHI_PRE_07',...
'CHI_PRE_08',...
'CHI_PRE_09',...
'CHI_PRE_10',...
'CHI_PRE_11',...
'CHI_PRE_12',...
'CHI_PRE_13',...
'CHI_PRE_14',...
'CHI_PRE_15',...
'CHI_PRE_16',...
'CHI_PRE_17',...
'CHI_PRE_18',...
'CHI_PRE_19',...
'CHI_PRE_20',...
'CHI_POST_02',...
'CHI_POST_03',...
'CHI_POST_04',...
'CHI_POST_05',...
'CHI_POST_06',...
'CHI_POST_07',...
'CHI_POST_08',...
'CHI_POST_09',...
'CHI_POST_10',...
'CHI_POST_11',...
'CHI_POST_12',...
'CHI_POST_13',...
'CHI_POST_14',...
'CHI_POST_15',...
'CHI_POST_16',...
'CHI_POST_17',...
'CHI_POST_18',...
'CHI_POST_19',...
'CHI_POST_20',...
'P_PRE_02',...
'P_PRE_03',...
'P_PRE_04',...
'P_PRE_05',...
'P_PRE_06',...
'P_PRE_07',...
'P_PRE_08',...
'P_PRE_09',...
'P_PRE_10',...
'P_PRE_11',...
'P_PRE_12',...
'P_PRE_13',...
'P_PRE_14',...
'P_PRE_15',...
'P_PRE_16',...
'P_PRE_17',...
'P_PRE_18',...
'P_PRE_19',...
'P_PRE_20',...
'P_POST_02',...
'P_POST_03',...
'P_POST_04',...
'P_POST_05',...
'P_POST_06',...
'P_POST_07',...
'P_POST_08',...
'P_POST_09',...
'P_POST_10',...
'P_POST_11',...
'P_POST_12',...
'P_POST_13',...
'P_POST_14',...
'P_POST_15',...
'P_POST_16',...
'P_POST_17',...
'P_POST_18',...
'P_POST_19',...
'P_POST_20',...
'SE_PRE_02',...
'SE_PRE_03',...
'SE_PRE_04',...
'SE_PRE_05',...
'SE_PRE_06',...
'SE_PRE_07',...
'SE_PRE_08',...
'SE_PRE_09',...
'SE_PRE_10',...
'SE_PRE_11',...
'SE_PRE_12',...
'SE_PRE_13',...
'SE_PRE_14',...
'SE_PRE_15',...
'SE_PRE_16',...
'SE_PRE_17',...
'SE_PRE_18',...
'SE_PRE_19',...
'SE_PRE_20',...
'SE_POST_02',...
'SE_POST_03',...
'SE_POST_04',...
'SE_POST_05',...
'SE_POST_06',...
'SE_POST_07',...
'SE_POST_08',...
'SE_POST_09',...
'SE_POST_10',...
'SE_POST_11',...
'SE_POST_12',...
'SE_POST_13',...
'SE_POST_14',...
'SE_POST_15',...
'SE_POST_16',...
'SE_POST_17',...
'SE_POST_18',...
'SE_POST_19',...
'SE_POST_20'};
return

function [rsq,Chi,p]=BGTest(r,x,ACF,lags);
NumLags=3;
Chi=0;
p=0;
x=x';
N=length(r);
lag=zeros(N,NumLags);
for i = 2:NumLags+1;
   lag(1:N-i+1,i-1)=r(i:N);
   maxN=length(r(i:N));
end
%lag(maxN:N,:)=[];
%r(maxN:N)=[];
%x(maxN:N)=[];
%lag(maxN:N,:)=[];
%r(maxN:N)=[];
%x(maxN:N)=[];
for i = 1:NumLags
   [RHO] = corr(r,lag(:,i));
   fprintf('lag %d \t my r = %8.6f \t ACF r = %8.6f\n',i,RHO,ACF(i+1))
end
x=[x lag];
whichstats = {'beta','rsquare','fstat','tstat'};
stats = regstats(r,x,'linear',whichstats);
f=stats.fstat.f;
fp=stats.fstat.pval;
dfnum=stats.fstat.dfr;
dfden=stats.fstat.dfe;
rsq=stats.rsquare;
Chi=(N)*rsq;
p=1-chi2cdf(Chi,NumLags);
fprintf('\nF=%10.2f | df=%d,%d | p = %6.4f\n',f,dfnum,dfden,fp)
fprintf('rsq=%5.2f|Chi=%f|p=%f\n',rsq,Chi,p)
fprintf('\nbeta \t t-value \t pvalue');
table=[stats.beta stats.tstat.t stats.tstat.pval]

return