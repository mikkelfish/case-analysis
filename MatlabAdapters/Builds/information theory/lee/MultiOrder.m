function MultiOrder(HumanOrRat,tag,LeastOrMost_L_M)

%MyPosition=[1785 0 1200 700];
%MyPosition=[1630         135        1200         700];
MyPosition=[100 0 1200 700];
%*** INITIAL STUFF ********************************************************
clc
close all

%*** GET MAT FILE *********************************************************
%[FileName,PathName] = uigetfile('G:\MatlabStrohl\ECData\*.mat','Find .mat File'); 
if ispc;
     if strcmpi(HumanOrRat,'R');
         PathName=['G:\MatlabStrohl\ECdata\' tag(1:5) '\'];
         FileName=[tag '_IEH.mat']
     else
         PathName='G:\MatlabStrohl\Human\IEHfiles\'
         FileName=[tag '_IEH_Best200_' LeastOrMost_L_M '.mat']
        [FileName,PathName] = uigetfile(['G:\MatlabStrohl\Human\IEHfiles\' ],'Find .mat File')
     end
 end
if isunix;
    [FileName,PathName] = uigetfile('/media/AirLink101/MatlabStrohl/*.mat','Find .mat File'); 
end
FullPath=[PathName FileName]
[PathStr, NameOnly, ext]=fileparts(FullPath)
NameOnly(findstr(NameOnly,'_'))='-';
if ispc;PathStr=[PathStr '\'];end;
if isunix;PathStr=[PathStr '/'];end;

%*** PROCESS VARIABLES ****************************************************
load([PathStr FileName]);
numHR=length(HR);
numEtrig=length(Etrig);
numItrig=length(Itrig);
RR=diff(HR)*1000;
I=diff(Itrig)*1000;
E=diff(Etrig)*1000;

TimeRepresented=(Itrig(length(Itrig))-Itrig(1))/60.
%**************************************************************************
%*** Find MultiOrder Data - Inspiration - TRIMMED DATA ********************
%**************************************************************************
multiorder=ComputeMultiOrder(Itrig,HR);
numEvents=length(Itrig);
[histo_Inspir,InspirEdges,TimeRes]=CreateMultiOrderHistogram(multiorder,PathStr,NameOnly,'I',numEvents,HumanOrRat,MyPosition);
%**************************************************************************
%*** Find MultiOrder Data - Expiration - TRIMMED DATA *********************
%**************************************************************************
if strcmpi(HumanOrRat,'H');
    multiorder=ComputeMultiOrder(Etrig,HR);
    numEvents=length(Etrig);
    [histo_Expir,ExpirEdges,TimeRes]=CreateMultiOrderHistogram(multiorder,PathStr,NameOnly,'E',numEvents,HumanOrRat,MyPosition);
end;
%**************************************************************************
%*** Create Fake Intervals ************************************************
%**************************************************************************
%numFakes=1000000;
%numFakes=50000;
%numFakes=25000;
numFakes=10000;
%numFakes=1000;
a=Itrig(1);
b=Itrig(length(Itrig));
Fakes= a + (b-a).*rand(numFakes,1);
for i = 1:numFakes
    Fakes(i)=str2num(num2str(Fakes(i),'%8.3f'));
    if mod(i,10000) == 0;fprintf('Percent Complete = %5.2f\n',100*i/numFakes);end
end
%**************************************************************************
%*** Find MultiOrder Data - Fake Events ***********************************
%**************************************************************************
fprintf('computing the multiorder vector for the Fake Intervals\n');
multiorder=ComputeMultiOrder(Fakes,HR);
fprintf('creating the multiorder histogram for the Fake Intervals\n');
[histo_Fake,FakeEdges,TimeRes]=CreateMultiOrderHistogram(multiorder,PathStr,NameOnly,'F',numFakes,HumanOrRat,MyPosition);
numItrig=length(Itrig);
histo_Fake_Inspir=histo_Fake*numItrig/numFakes;
numEtrig=length(Etrig);
histo_Fake_Expir=histo_Fake*numEtrig/numFakes;
PlotTrueAndFake(InspirEdges,histo_Inspir,FakeEdges,histo_Fake_Inspir,'I',PathStr,NameOnly,numItrig,numFakes,TimeRes,TimeRepresented,HumanOrRat,MyPosition,LeastOrMost_L_M);
if strcmpi(HumanOrRat,'H');
    PlotTrueAndFake(ExpirEdges ,histo_Expir ,FakeEdges,histo_Fake_Expir ,'E',PathStr,NameOnly,numEtrig,numFakes,TimeRes,TimeRepresented,HumanOrRat,MyPosition,LeastOrMost_L_M);
end
return

function [multiorder]=ComputeMultiOrder(event,HR)
count=0;
multiorder=zeros(length(event)*10,1);
for i = 1:length(event)
    %fprintf('Inspiration number %d occurs at sample # %8.2f sec\n',i,event(i));
    IndicesOf5Prev=find(HR<event(i),5,'last');
    for j = length(IndicesOf5Prev):-1:1
        %fprintf('---------Previous HB Occurs at %8.2f - diff = %5.3f\n',HR(IndicesOf5Prev(j)),HR(IndicesOf5Prev(j))-event(i));
        count=count+1;
        multiorder(count)=HR(IndicesOf5Prev(j))-event(i);
    end
    IndicesOf5Next=find(HR>event(i),5,'first');
    for j = 1:length(IndicesOf5Next)
        %fprintf('---------Next HB Occurs at %8.2f - diff = %5.3f\n',HR(IndicesOf5Next(j)),HR(IndicesOf5Next(j))-event(i));
        count=count+1;
        multiorder(count)=HR(IndicesOf5Next(j))-event(i);
    end
    if length(event)>1000;
        if mod(i,10000)==0;fprintf('Percent Complete = %5.2f\n',100*i/length(event));end;
    end
end;
multiorder(multiorder == 0)=[];
multiorder=sort(multiorder);
return

function [histo,edges,TimeRes]=CreateMultiOrderHistogram(multiorder,PathStr,NameOnly,MultiOrderType,numEvents,HumanOrRat,MyPosition)
if strcmpi(HumanOrRat,'H')
    TimeRes=0.1;
else
    TimeRes=0.02;
end

minevent=str2double(num2str(min(multiorder),'%6.2f'))-TimeRes;
maxevent=str2double(num2str(max(multiorder),'%6.2f'))+TimeRes;
count=0;
for i = 0:-TimeRes:minevent;count=count+1;edges(count)=i;end;
for i = TimeRes :TimeRes:maxevent;count=count+1;edges(count)=i;end;
edges=sort(edges)';

histo=histc(multiorder,edges);
figure(3);set(3,'units','pixels','position',MyPosition,'menubar','none');
hold on;bar(edges,histo,'histc');
if strcmp(MultiOrderType,'I');xlabel('Time Before/After Inspiration (sec)');end
if strcmp(MultiOrderType,'E');xlabel('Time Before/After Expiration (sec)');end
if strcmp(MultiOrderType,'F');xlabel('Time Before/After Fake Event (sec)');end
ylabel('Number of Events');
titstr=['Interval Resolution = ' num2str(TimeRes) ' sec'];title(titstr);
xx=[0 0];yy=[get(gca,'ylim')];plot(xx,yy,':r','linewidth',1);
close all
return

function PlotTrueAndFake(edges1,histo1,edges2,histo2,InspirOrExpir,PathStr,NameOnly,numEvents,numFakes,TimeRes,TimeRepresented,HumanOrRat,MyPosition,LeastOrMost_L_M)

if strcmp(InspirOrExpir,'I');fignum=4;end;
if strcmp(InspirOrExpir,'E');fignum=5;end; 

figure(fignum);set(fignum,'units','pixels','position',MyPosition,'menubar','none');

subplot(211);
hold on;
bar(edges1,histo1,'histc');
ylim([0 max(histo1)*1.25])
xx=[0 0];yy=[get(gca,'ylim')];plot(xx,yy,'--k','linewidth',3);
if strcmp(InspirOrExpir,'I');
    xlabel('Time Before/After Inspiration (sec)');
    titstr=['MultiOrder Histogram - Inspirations | # Inspirations = ' num2str(numEvents) ' | Time = ' num2str(TimeRepresented,'%4.1f') ' min'];
end;
if strcmp(InspirOrExpir,'E');
    xlabel('Time Before/After Expiration (sec)');
    titstr=['MultiOrder Histogram - Expirations | # Expirations = ' num2str(numEvents)];
end
text(edges1(length(edges1)),max(histo1)*1.2,['Interval Resolution = ' num2str(TimeRes) ' sec'],'horizontalalignment','right')
title(titstr);
ylabel('count');
myylim=get(gca,'ylim');

subplot(212);
hold on;
bar(edges2,histo2,'histc');
xx=[0 0];yy=myylim;plot(xx,yy,'--k','linewidth',3);
xlabel('Time Before/After Fake Event (sec)');ylabel('count');
titstr=['MultiOrder Histogram - ' num2str(numFakes) ' Random (uniform) Fake events'];
title(titstr);
ylim(myylim);

[ChiLeft,ChiRight,MADLeft,MADRight,NbinsLeft,NbinsRight,LeftThreshold,RightThreshold]=...
GetMyChiSquare(edges1,histo1,edges2,histo2,TimeRes,PathStr,NameOnly,InspirOrExpir,LeastOrMost_L_M);

subplot(211);

if strcmpi(HumanOrRat,'R')
    text(-1,max(histo1)*1.1,['   Chi = ' num2str(ChiLeft ,'%8.2f') ', bins=' num2str(NbinsLeft,'%d') ', MAD=' num2str(MADLeft,'%6.2f') ],'horizontalalignment','left')
    text( 1,max(histo1)*1.1,['   Chi = ' num2str(ChiRight,'%8.2f') ', bins=' num2str(NbinsRight,'%d') ', MAD=' num2str(MADRight,'%6.2f')],'horizontalalignment','right')
    subplot(211);xlim([-1.25 1.25]);
    subplot(212);xlim([-1.25 1.25]);
else
    text(-5,max(histo1)*1.1,['   Chi = ' num2str(ChiLeft ,'%8.2f') ', bins=' num2str(NbinsLeft,'%d') ', MAD=' num2str(MADLeft,'%6.2f') ],'horizontalalignment','left')
    text( 5,max(histo1)*1.1,['   Chi = ' num2str(ChiRight,'%8.2f') ', bins=' num2str(NbinsRight,'%d') ', MAD=' num2str(MADRight,'%6.2f')],'horizontalalignment','right')
    subplot(211);hold on;xlim([-6 6]);
    subplot(212);hold on;xlim([-6 6]);
end

subplot(212);
hold on;

if strcmpi(HumanOrRat,'H')
    x=[-6 -4];y=[LeftThreshold   LeftThreshold];plot(x,y,'k','linewidth',2);
    x=[ 4  6];y=[RightThreshold RightThreshold];plot(x,y,'k','linewidth',2);
else
    x=[-1.5 -.75];y=[LeftThreshold   LeftThreshold];plot(x,y,'k','linewidth',2);
    x=[ .75  1.5];y=[RightThreshold RightThreshold];plot(x,y,'k','linewidth',2);
end

if strcmpi(HumanOrRat,'H')
    if strcmp(InspirOrExpir,'I');
        if strcmpi(LeastOrMost_L_M,'L')
            titstr=[NameOnly ' - MultiOrder Histogram Analysis - Inspirations | Least Variable'];
        else
            titstr=[NameOnly ' - MultiOrder Histogram Analysis - Inspirations | Most Variable'];
        end
        suptitle(titstr);
        print(gcf,[PathStr NameOnly '_MultiOrderFinalHistogramInspiration_' LeastOrMost_L_M '.jpg'],'-djpeg','-r150');
    end;
    if strcmp(InspirOrExpir,'E');
        if strcmpi(LeastOrMost_L_M,'L')
            titstr=[NameOnly ' - MultiOrder Histogram Analysis - Expirations | Least Variable'];
        else
            titstr=[NameOnly ' - MultiOrder Histogram Analysis - Expirations | Most Variable'];
        end
        suptitle(titstr);
        print(gcf,[PathStr NameOnly '_MultiOrderFinalHistogramExpiration_' LeastOrMost_L_M '.jpg'],'-djpeg','-r150');
    end;
else;
    if strcmp(InspirOrExpir,'I') && strcmpi(LeastOrMost_L_M,'X');
        titstr=[NameOnly ' - MultiOrder Histogram Analysis - Inspirations'];
        suptitle(titstr);
        print(gcf,[PathStr NameOnly '_MultiOrderFinalHistogramInspiration.jpg'],'-djpeg','-r150');
    end
end
return

function [ChiLeft,ChiRight,MADLeft,MADRight,NbinsLeft,NbinsRight,LeftThreshold,RightThreshold]=...
          GetMyChiSquare(edges1,histo1,edges2,histo2,TimeRes,PathStr,NameOnly,InspirOrExpir,LeastOrMost_L_M)

lowedge=max(edges1(1),edges2(1));lowedge=str2num(num2str(lowedge,'%6.2f'));
hi_edge=min(edges1(length(edges1)),edges2(length(edges2)));hi_edge=str2num(num2str(hi_edge,'%6.2f'));
bins=(lowedge:TimeRes:hi_edge)';
bins=str2num(num2str(bins,'%6.2f'));
numEdges=length(bins);
count=0;
obsCounts=zeros(numEdges,1);
%fprintf('filling obsCount: LowerEdge = %f, Time Resolution = %f, UpperEdge = %f\n',lowedge,TimeRes,hi_edge);
for i = lowedge:TimeRes:hi_edge;
    if TimeRes < .1;
        i_as_edge=str2num(num2str(i,'%8.2f'));
    else
        i_as_edge=str2num(num2str(i,'%8.1f'));
    end;
    %fprintf('I = %f, i as edge = %f\n',i,i_as_edge)
    count=count+1;
    if isempty(edges1==i_as_edge);
        %fprintf('did not find an observed edge that matches i_as_edge\n');
        continue
    else
        %fprintf('found an observed edge that matches i_as_edge\n');
        index=find(edges1>i_as_edge-0.01 & edges1<i_as_edge+0.01);
    end
    if ~isempty(edges1==i_as_edge);obsCounts(count)=histo1(index);end;
end
count=0;
expCounts=zeros(numEdges,1);
fprintf('filling expCount....');
for i = lowedge:TimeRes:hi_edge;
    if TimeRes < .1;
        i_as_edge=str2num(num2str(i,'%8.2f'));
    else
        i_as_edge=str2num(num2str(i,'%8.1f'));
    end;
    %fprintf('I = %f, i as edge = %f\n',i,i_as_edge)
    count=count+1;
    if isempty(edges2==i_as_edge);
        continue %fprintf('did not find an observed edge that matches i_as_edge\n');
    else
        %fprintf('found an observed edge that matches i_as_edge\n');
        index=find(edges2>i_as_edge-0.01 & edges2<i_as_edge+0.01);
    end
    if ~isempty(edges2==i_as_edge);expCounts(count)=histo2(index);end;
end

[length(bins) length(obsCounts) length(expCounts)];
[bins obsCounts expCounts];
sum_obsCounts=sum(obsCounts);
sum_expCounts=sum(expCounts);
fprintf('total observed = %10.2f,total expected = %10.2f\n',sum_obsCounts,sum_expCounts)
out=[bins,obsCounts,expCounts];
if strcmpi(InspirOrExpir,'I');dlmwrite([PathStr NameOnly '_Insp_ChiSquareInfo_' LeastOrMost_L_M '.csv'],out,'precision',8);end
if strcmpi(InspirOrExpir,'E');dlmwrite([PathStr NameOnly '_Expr_ChiSquareInfo_' LeastOrMost_L_M '.csv'],out,'precision',8);end

ZeroBin=find(bins==0);
% LowerLeftEnd=find(bins==-3.500);
% UpperRghtEnd=find(bins==3.400); 
LowerLeftEnd=find(bins==lowedge);
UpperRghtEnd=find(bins==hi_edge); 

if isempty(ZeroBin     );
    fprintf('ZeroBin Not Found |ZeroBin=%6.2f\n',ZeroBin);
end;
if isempty(LowerLeftEnd);
    fprintf('LowerLeftEnd Not Found | LowerLeftEnd=%6.2f| lowedge=%6.2f\n',LowerLeftEnd,lowedge);
    lowedge
    bins
end;
if isempty(UpperRghtEnd);
    fprintf('UpperRghtEnd Not Found | UpperRghtEnd=%6.2f\n',UpperRghtEnd);
end;

fprintf('Lower Left Edge = %5.2f\nLower Rght Edge = %5.2f\nUpper Left Edge = %5.2f\nUpper Rght Edge = %5.2f\n',...
        bins(LowerLeftEnd),bins(ZeroBin-1),bins(ZeroBin),bins(UpperRghtEnd))

LeftMaxExpected=max(expCounts(LowerLeftEnd:ZeroBin-1))
LeftThreshold=0.5*LeftMaxExpected

ChiLeft=0;
MADLeft=0;
NbinsLeft=0;
for i = LowerLeftEnd:1:ZeroBin-1;
    if expCounts(i) > LeftThreshold
        MADLeft=MADLeft+abs(obsCounts(i)-expCounts(i));    
        if expCounts(i)>0;
            ChiLeft=ChiLeft+((obsCounts(i)-expCounts(i)).^2)/expCounts(i);
        end;
        NbinsLeft=NbinsLeft+1;
    %     fprintf('LEFT :i=%d,bin=%4.2f,obsCounts=%d,expCounts=%6.2f,Chi=%8.2f\n',i,bins(i),obsCounts(i),expCounts(i),ChiLeft)
    %     pause
    end
end
MADLeft=MADLeft/NbinsLeft;
fprintf('IorE=%s|Left|Chi=%8.2f,bins=%d,MAD=%8.2f\n',InspirOrExpir,ChiLeft,NbinsLeft,MADLeft)

RightMaxExpected=max(expCounts(ZeroBin:UpperRghtEnd))
RightThreshold=0.5*RightMaxExpected

ChiRight=0;
MADRight=0;
NbinsRight=0;
for i = ZeroBin:1:UpperRghtEnd;
    if expCounts(i) > RightThreshold
        MADRight=MADRight+abs(obsCounts(i)-expCounts(i));    
        if expCounts(i)>0;
            ChiRight=ChiRight+((obsCounts(i)-expCounts(i)).^2)/expCounts(i);
        end;
        NbinsRight=NbinsRight+1;
    %     fprintf('RIGHT:i=%d,bin=%4.2f,obsCounts=%d,expCounts=%6.2f,Chi=%8.2f\n',i,bins(i),obsCounts(i),expCounts(i),ChiRight)
    %     pause
    end
end
MADRight=MADRight/NbinsRight;
fprintf('IorE=%s|Rght|Chi=%8.2f,bins=%d,MAD=%8.2f\n',InspirOrExpir,ChiRight,NbinsRight,MADRight)

if TimeRes == 0.1;%Human data
    SubNum=str2num(NameOnly(4:7));
    Visit=str2num(NameOnly(9));
    if strcmpi(InspirOrExpir,'I');Type=1;end;
    if strcmpi(InspirOrExpir,'E');Type=2;end;
    out=[SubNum,Visit,Type,ChiLeft,NbinsLeft,MADLeft,ChiRight,NbinsRight,MADRight];
    dlmwrite([PathStr NameOnly '_' InspirOrExpir '_MOSTATS_' LeastOrMost_L_M '.csv'],out);
else
    RatNum =str2num(NameOnly(4:5));
    DataSet=str2num(NameOnly(7:8));
    out=[RatNum,DataSet,ChiLeft,NbinsLeft,MADLeft,ChiRight,NbinsRight,MADRight];
    dlmwrite([PathStr NameOnly '_' InspirOrExpir '_MOSTATS_' LeastOrMost_L_M '.csv'],out);
end;
return

