%   Palarm.m
function [Out,Kout]=Palarm(X,pf)
%   Palarm.m
%   usage: [Out,Kout]=Palarm(X,pf)
%   Detects change-points in the mean of a random sequence
%
%   Anatoly Zlotnik and Alexandra Piryatinska, 2004
%   Revised 2005 by Anatoly Zlotnik
%
%   Input:
%
%   X       time-series data
%   pf      false alarm probability
%
%   Notes:  Algorithm details:
%   Parameter   Default Description
%   delta1      1       weighting parameter for phase I
%   delta2      0       weighting parameter for phase II
%   pf          0.1     false detection probability
%   m           3       minimum interval size
%   M           1000    number of terms in KS series
%   epsilon     0.02    minimum relative distance of change point from 
%                       endpoints of local interval
%   
%   Output:
%
%   Out                 data with each interval replaced by local mean
%   Kout                vector containing estimated global change points
%  
%   Anatoly Zlotnik, June 19, 2006
%
%   [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
%       Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
%       University; 2006
%

%Estimation of change points and mean on each stable interval 
warning off MATLAB:fzero:UndeterminedSyntax
if(size(X,1)>1) X=X'; end
if(size(X,1)>1) disp('error in input!'); return; end
%default parameters
if(nargin<7) epsilon=0.02; end
if(nargin<6) M=1000; end; if(nargin<5) m=5; end
if(nargin<4) delta2=0; end; if(nargin<3) delta1=1; end
if(nargin<2) pf=0.1; end;

%scale input
Xin=X; Xbar=mean(X); X=(X-Xbar)/std(X);
N=length(X);

%Calculate Initial change point threshold
thresh=abs(KSinverse(pf,M));

%Calculate initial change points (Phase I)
P0=1; Kin0=[];
[Kone]=PalarmF(X,Kin0,P0,delta1,thresh,m,epsilon);
if(length(Kone)==0) Kout=Kone; Out=mean(Xin)*ones(N,1); return; end
Kone=sort(Kone);

%Calculate new threshold
thresh=abs(KSinverse(pf/10,M));

%Eliminate doubtful change points (Phase II)
[Kout]=diagn(X,Kone,delta2,m,thresh);

%Estimate final locations of change points (Phase III)
delta3=0.5;
[Out,Kout]=finalal(X,Xin,Kout,m,delta3,thresh);

if(size(Out,2)>1) Out=Out'; end

function [Kout]=PalarmF(X,Kin,P,delta,thresh,m,epsilon)
%[Kout]=PalarmF(X,Kin,P,delta,thresh,m,epsilon)
%
%   @2004 by Anatoly Zlotnik and Alexandra Piryatinska
%
%   X       local data
%   Kin     vector containing estimated global change points
%   P       global index of first point of local data
%   delta   detect parameter
%   thresh  minimum statistic for change point characterization
%   m       minimum interval size
%   epsilon minimum relative distance of change point from endpoints
%
%   Kout    updated vector of estimated global change points
%
%Recursive algorithm for detection of change points by successive interval
%bisection

[stat,k]=ystat(X,delta,m);N=length(X);
Sigma=std([(X(1:k)-mean(X(1:k))) (X(k+1:N)-mean(X(k+1:N)))])/sqrt(N);
%Sigma=std(X)/sqrt(length(X)); 
Level=thresh*Sigma;
D=max([round(epsilon*N) 1]);
if(stat(k)<Level) Kout=Kin; return;
elseif(stat(k)>Level)
    X1=X(1:k-D);P1=P;
    X2=X(k+D:N);P2=P+k+D-1;
    if(length(X1)<=m) Ktemp1=Kin; elseif(length(X1)>m)
    Ktemp1=PalarmF(X1,Kin,P1,delta,thresh,m,epsilon); end
    if(length(X2)<=m) Ktemp2=Ktemp1; elseif(length(X2)>m)
    Ktemp2=PalarmF(X2,Ktemp1,P2,delta,thresh,m,epsilon); end
    if(length(X1)>m&length(X2)>m) Kout=[Ktemp2 P+k-1]; else
        Kout=Ktemp2; end
end

function [ystat1,k] = ystat(X,delta,m)
%[ystat1,k] = ystat(X,delta,m)
%
%   @2004 by Anatoly Zlotnik and Alexandra Piryatinska
%
%   X       Input data
%   delta   detect parameter
%   m       minimum interval size
%
%   ystat   test statistic
%   k       estimated change point
%
%calculate statistics for change point and maximize
%to detect change point k 

N=length(X); if(size(X,2)>1) X=X'; end
if(N<m) ystat1=zeros(1,N); k=1; return; end
M1=cumsum(X(1:N-1))./[1:N-1]'; 
M2=flipdim(cumsum(flipdim(X(2:N),1))./[1:N-1]',1);
ystat1=abs((((1-[1:N-1]'/N).*[1:N-1]'/N).^delta).*(M1-M2));
[M,k]=max(ystat1);


function [tresh]=KSinverse(pf,M)
%[tresh]=KSinverse(pf,M)
%
%   @2004 by Anatoly Zlotnik and Alexandra Piryatinska
%
%   pf      rejection probability
%   M       number of terms in series
%
%   ystat   test statistic
%   k       estimated change point
%
%Estimate threshold for change points using inverse of Kolmogorov-Smirnov
%distribution

tresh=fzero(@KSdist,1,[],pf,M);
function [fun]=KSdist(Y,pf,M)
k=[1:M];
s=(-ones(1,M)).^(k+1);
fun=2*sum(s.*exp(-(2*Y^2)*(k.^2)))-pf;


function [meanK, Kout]=finalal(X,Xin,Kin,m,delta,thresh)
%[meanK, Kout]=finalal(X,Xin,Kin,m,delta)
%
%   @2004 by Anatoly Zlotnik and Alexandra Piryatinska
%
%   X       Input data
%   Xin     Original time-series
%   Kin     vector containing estimated global change points
%   m       minimum interval size
%   delta   detect parameter
%
%   meanK   mean of values in intervals
%   Kout       final estimated change points
%
%Calculate final change point estimates and mean of each interval

if(size(X,2)>1), X=X'; end
if(nargin<3), m=5;  delta=0.5; end
if(nargin<4) delta=0.5; end
N=length(X); Z=length(Kin);

Kout=[];
if(length(Kin)>1)
    b=floor((Kin(2)+Kin(1))/2);
    X1=X(1:b);
    [stat1,k1] = ystat(X1,delta,m);
    if(k1>1)
        me=mean(Xin(1:k1-1));
        meanK=me*(ones(1,k1-1));
    else
        meanK=[];
    end
    Kout=[Kout k1];
    for i=2:length(Kin)-1
        a=floor((Kin(i)+Kin(i-1))/2)+1;
        b=floor((Kin(i+1)+Kin(i))/2);
        Xtemp=X(a:b);
        [stat,ktemp] = ystat(Xtemp,delta,m);
        Kout=[Kout a+ktemp-1];
        M1=mean(Xin(Kout(i-1)+1:Kout(i)-1));
        M2=M1*(ones(1,Kout(i)-Kout(i-1)));
        meanK=[meanK M2];
    end
    a=round((Kin(Z)+Kin(Z-1))/2);
    X1=X(a:N);
    [stat,k1] = ystat(X1,delta,m);
    Kout=[Kout a+k1-1];
    M1=mean(Xin(1+Kout(Z-1):Kout(Z)-1));
    M2=M1*(ones(1 ,Kout(Z)-Kout(Z-1)));
    meanK=[meanK M2];
    M3=mean(Xin(Kout(Z)+1:N));
    meanK=[meanK  M3*ones(1 ,N-Kout(Z)+1)];
else [stat1,k1] = ystat(X,delta,m); Kout=[Kout k1];
    meanK=[ones(1,k1)*mean(Xin(1:k1)) ones(1,N-k1)*mean(Xin(k1:N))]; 
end

function [Kout]=diagn(X,Kin,delta,m,thresh)
%[Knew,Kalt]=diagn(X,K,delta,m,thresh)
%
%   @2004 by Anatoly Zlotnik and Alexandra Piryatinska
%
%   X       Input data
%   K       vector containing estimated global change points
%   delta   detect parameter
%   m       minimum interval size
%   thresh  minimum statistic for change point characterization
%
%   Kout    output containing estimated global change points
%
%check estimated global change points for errors and perform update

%Initialize
N=length(X); Z=length(Kin); Kout=[];


if(length(Kin)>1)   %If inital change points found

    %Check first change point
    b=floor((Kin(2)+Kin(1))/2);
    [stat1,k1] = ystat(X(1:b),delta,m); Sigma=std(X(1:b))/sqrt(b); 
    Level=thresh*Sigma;
    if(stat1(k1)>Level) Kout=[Kout k1]; end

    %Check middle change points
    for i=2:length(Kin)-1
        a=floor((Kin(i)+Kin(i-1))/2)+1;
        b=floor((Kin(i+1)+Kin(i))/2);
        [stat,ktemp] = ystat(X(a:b),delta,m); Sigma=std(X(a:b))/sqrt(b-a); 
        Level=thresh*Sigma;
        if(stat(ktemp)>Level) Kout=[Kout a+ktemp-1]; end
    end

    %Check final change point
    a=round((Kin(Z)+Kin(Z-1))/2);
    [stat2,k2] = ystat(X(a:N),delta,m); Sigma=std(X(a:N))/sqrt(N-a); 
    Level=thresh*Sigma;
    if(stat2(k2)>Level) Kout=[Kout a+k2-1]; end

    
else                %If no change points yet found
    [stat1,k1] = ystat(X,delta,m);
    %disp('test');
    if(stat1(k1)>thresh*std(X)/sqrt(N)) Kout=[Kout k1]; end
end
    