function toRet = RunNewCoupling(trigger1Data, trigger2Data, bestnum, mostvar, shouldplot, save, label, path)

mkdir(path(1:length(path)-1));
fileattrib(path(1:length(path)-1),'+w');
[trimmed1 trimmed2 TimeToPrevious TimeToNext MaximumBinEnd] = FindBest(trigger1Data, trigger2Data, bestnum, mostvar, shouldplot, save, label, path);
[fullhist inverseCDF edges10 area10 edges14 area14] = FindCouplingLimits(trimmed2, shouldplot, path, label);
toRet = NewCoupling(TimeToPrevious, TimeToNext, path, label, ~shouldplot, edges14, area14);



return



function toRet = NewCoupling(TimeToPreviousHR, TimeToNextHR, path, label, FastTF, MaxBinValue, Proportion)

figpos=[115          58        1280         781];
NumPrevHR=length(TimeToPreviousHR);
NumNextHR=length(TimeToNextHR);

[Pre_DWP,Pre_DW,Next_DWP,Next_DW,MaxAbsValACF,MedianAbsValACF,BGrsqPrev,BGChiPrev,BGpPrev,BGrsqNext,BGChiNext,BGpNext,RHO,PVAL]=...
         AutoCorrelationAnalysis(TimeToPreviousHR,TimeToNextHR,path, label,FastTF);

ChiRAW=zeros(2,1);
ChiGOF=ChiRAW;
pRAW  =ChiRAW;
pGOF  =ChiRAW;
dfRAW =ChiRAW;
dfGOF =ChiRAW;
SEUNIarray=zeros(2,1);
SEEXParray=zeros(2,1);
NumHR=zeros(2,1);

WhichGraph=0;
NumBins = length(MaxBinValue);
HistoPrevious=myhistc(TimeToPreviousHR*1000,MaxBinValue);
HistoNext    =myhistc(TimeToNextHR*1000,MaxBinValue);
NumBins = length(HistoPrevious);

WhichGraph=WhichGraph+1;
NumHR(WhichGraph)=sum(HistoPrevious);
[ChiRAW(WhichGraph),ChiGOF(WhichGraph),pRAW(WhichGraph),pGOF(WhichGraph),dfRAW(WhichGraph),dfGOF(WhichGraph),SEUNIarray(WhichGraph),SEEXParray(WhichGraph)]=...
ShannonEntropynew(HistoPrevious,FastTF,NumBins,Proportion,TimeToPreviousHR*1000,MaxBinValue,WhichGraph,path,label);
[titstrPre]=BuildTitle(ChiRAW(WhichGraph),dfRAW(WhichGraph),pRAW(WhichGraph),SEUNIarray(WhichGraph),SEEXParray(WhichGraph),NumHR(WhichGraph),MaxBinValue(2));



WhichGraph=WhichGraph+1;
NumHR(WhichGraph)=sum(HistoNext);
[ChiRAW(WhichGraph),ChiGOF(WhichGraph),pRAW(WhichGraph),pGOF(WhichGraph),dfRAW(WhichGraph),dfGOF(WhichGraph),SEUNIarray(WhichGraph),SEEXParray(WhichGraph)]=...
ShannonEntropynew(HistoNext,FastTF,NumBins,Proportion,TimeToNextHR*1000,MaxBinValue,WhichGraph,path,label);
[titstrNext]=BuildTitle(ChiRAW(WhichGraph),dfRAW(WhichGraph),pRAW(WhichGraph),SEUNIarray(WhichGraph),SEEXParray(WhichGraph),NumHR(WhichGraph),MaxBinValue(2));

 figure(1);set(1,'units','pixels','position',figpos,'menubar','none','color',[1 1 1]);
subplot(121);
negedges=-1*MaxBinValue;
centers=zeros(NumBins,1);
for j=1:NumBins;
    centers(j)=(negedges(j)+negedges(j+1))/2;
end;
H_Bar=bar(centers,HistoPrevious);
set(H_Bar,'FaceColor','none','linewidth',4)
set(gca,'FontSize',20)
ylim([0 50]);
xlim([centers(NumBins)*1.1 0]);
Expected=Proportion*sum(HistoPrevious);
hold on;plot(centers,Expected,'-k','linewidth',2);
hold on;plot(centers,Expected,'ok','linewidth',5);
set(gca,'Xtick',centers(NumBins:-1:1),'XTickLabel',centers(NumBins:-1:1),'FontSize',9,'tickdir','out');
xlabel('ms Before Trigger','FontSize',20);
text(mean(centers),45,titstrPre,'HorizontalAlignment','center','FontSize',16);    


set(gca,'tickdir','out')
box off;
pos1=get(gca,'position');
pos1(1)=0.05;
pos1(3)=0.45;
set(gca,'position',pos1);
set(gca,'linewidth',2);

subplot(122)
centers=centers*-1;
H_Bar=bar(centers,HistoNext);
set(H_Bar,'FaceColor','none','linewidth',4)
set(gca,'FontSize',20)
ylim([0 50]);
xlim([0 centers(NumBins)*1.1]);
Expected=Proportion*sum(HistoNext);
hold on;plot(centers,Expected,'-k','linewidth',2);
hold on;plot(centers,Expected,'ok','linewidth',5);
set(gca,'Xtick',[0; centers(1:NumBins);],'XTickLabel',[0; centers(1:NumBins);],'FontSize',9,'tickdir','out');
xlabel('ms After Trigger','FontSize',20);

text(mean(centers),45,titstrNext,'HorizontalAlignment','center','FontSize',16);    
set(gca,'Ytick',[],'YTickLabel',[]);

box off;
pos2=get(gca,'position');
pos2(1)=0.50;
pos2(3)=0.45;
set(gca,'position',pos2);
set(gca,'linewidth',2);


titstr=[label ' | N Breaths = ' num2str(NumPrevHR) ' | N Bins = ' num2str(NumBins) ];
H_ST=suptitle(titstr);
set(H_ST,'FontSize',20)
    %'.eps'],'-deps',);


if ~FastTF

         FigOutName=[path label '_Histograms_' num2str(NumBins,'%2.2d') '.eps' ];   
		 print(gcf,FigOutName,'-deps','-r150');		 
         %printeps(gca,FigOutName);
end;

pRAW=-log10(pRAW);
pGOF=-log10(pGOF);

toRet = [NumHR' BGrsqPrev BGrsqNext BGpPrev BGpNext BGChiPrev BGChiNext ChiRAW' dfRAW' pRAW' ChiGOF' dfGOF' pGOF' SEUNIarray' ];

return



function [titstr]=BuildTitle(Chi,df,pChi,RelSEUNI,RelSEEXP,NumHR,BinWidth)
line1={['Chi-Square = ' num2str(Chi,'%6.2f')];...
       ['df = ' num2str(df,'%d') ', p = ' num2str(pChi,'%8.3f')];...
       ['Rel. Shan.Ent.(UNI) = ' num2str(RelSEUNI,'%7.3f')];...
       ['Rel. Shan.Ent.(EXP) = ' num2str(RelSEEXP,'%7.3f')];...
       ['Number of R-Waves = ' num2str(NumHR)];...
       ['Bin Width = ' num2str(BinWidth) ' ms'];};
titstr=line1;
return

function [Pre_DWP,Pre_DW,Next_DWP,Next_DW,MaxAbsValACF,MedianAbsValACF,...
         BGrsqPrev,BGChiPrev,BGpPrev,BGrsqNext,BGChiNext,BGpNext,RHO,PVAL]=...
         AutoCorrelationAnalysis(TimeToPreviousHR,TimeToNextHR,path, label, FastTF)

NumPrevHR=length(TimeToPreviousHR);
x=(1:NumPrevHR);
whichstats = {'r','tstat'};
Pre_stats = regstats(TimeToPreviousHR,x,'linear',whichstats);
[Pre_DWP,Pre_DW]=dwtest(Pre_stats.r,ones(NumPrevHR,1));
[ACF_Pre_Interval,lags]=xcorr(Pre_stats.r,20,'coeff');
pACF_Pre_Interval=ACF_Pre_Interval(lags >= 0);
AbsValACF=abs(pACF_Pre_Interval(2:10));
MaxAbsValACF=max(AbsValACF);
MedianAbsValACF=median(AbsValACF);
pLags=lags(lags >= 0);
[BGrsqPrev,BGChiPrev,BGpPrev]=BGTest(Pre_stats.r,x,pACF_Pre_Interval,pLags);

NumNextHR=length(TimeToNextHR);
if NumNextHR ~= NumPrevHR;
    fprintf('The Number of Pre and Next Intervals is not equal: %d,%d\n',NumNextHR,NumPrevHR);
    return
end
x=(1:NumNextHR);
Next_stats = regstats(TimeToNextHR,x,'linear',whichstats);
[Next_DWP,Next_DW]=dwtest(Next_stats.r,ones(NumNextHR,1));
[ACF_Next_Interval,lags]=xcorr(Next_stats.r,20,'coeff');
pACF_Next_Interval=ACF_Next_Interval(lags >= 0);
[BGrsqNext,BGChiNext,BGpNext]=BGTest(Next_stats.r,x,pACF_Next_Interval,pLags);

[RHO,PVAL] = corr(TimeToPreviousHR,TimeToNextHR);


if ~FastTF
    figure(1);set(1,'units','pixels','menubar','none');

    subplot(221);
    plot(x,TimeToPreviousHR,'.k')
    hline=lsline;set(hline,'linewidth',3)
    xlim([0 NumPrevHR]);
    xlabel('Event Number');
    ylabel('Interval (s)');
    t  =Pre_stats.tstat.t(2);
    p  =Pre_stats.tstat.pval(2);
    dfe=Pre_stats.tstat.dfe;
    titstr=['Intervals Prior To Event | t = ' num2str(t,'%5.1f') ' | p = ' num2str(p,'%6.4f')];
    %titstr=['Intervals Prior To Event'];
    title(titstr);

    subplot(222);
    hold on
    scatter(pLags,pACF_Pre_Interval,'+k');
    xlabel('ACF lag');
    ylabel('ACF coefficient');
    titstr='AutoCorrelation Analysis of Residuals';
    title(titstr);
%     mylabel={['Pre:DW=' num2str(Pre_DW,'%5.2f')];['p(2-tailed) = ' num2str(Pre_DWP,'%5.4f')];
%              ['Next:DW=' num2str(Next_DW,'%5.2f')];['p(2-tailed) = ' num2str(Next_DWP,'%5.4f')]};
    mylabel={['BG-Rsq Prev:= ' num2str(BGrsqPrev,'%4.3f')];['BG-Rsq Next:= ' num2str(BGrsqNext,'%4.3f')]};
    % myBestX=find(pACF_Pre_Interval == min(pACF_Pre_Interval));
    % myBestX=min(myBestX,15);
    text(19.5,max(pACF_Pre_Interval),mylabel,'HorizontalAlignment','right','FontName','courier');
    xlim([-1 20]);
    ylim([min(pACF_Pre_Interval) max(pACF_Pre_Interval)*1.1]);
    refline(0,0);
    scatter(pLags,pACF_Next_Interval,'og');
    legend('Pre','Post','location','SE')

    subplot(223)
    plot(x,TimeToNextHR,'.k');
    hline=lsline;set(hline,'linewidth',3)
    xlim([0 NumNextHR]);
    xlabel('Event Number');
    ylabel('Interval (s)');
    t  =Next_stats.tstat.t(2);
    p  =Next_stats.tstat.pval(2);
    dfe=Next_stats.tstat.dfe;
    titstr=['Intervals Following Event | t = ' num2str(t,'%5.1f') ' | p = ' num2str(p,'%6.4f')];
    %titstr=['Intervals Following Event'];
    title(titstr);

    subplot(224);
    plot(TimeToPreviousHR,TimeToNextHR,'.k');
    xlim([0 max(TimeToPreviousHR)])
    ylim([0 max(TimeToNextHR)])
    hline=lsline;
    set(hline,'color','r','linewidth',2);
    whichstats = {'beta'};
    NextVsPrevious_stats = regstats(TimeToNextHR,TimeToPreviousHR,'linear',whichstats);
    hrefline=refline(-1,NextVsPrevious_stats.beta(1));
    set(hrefline,'color','b','linewidth',2)
    xlabel('Time To Previous HB (s)');
    ylabel('Time To Next HB (s)');

    titstr=['Correlation: Pre vs Next | r = ' num2str(RHO,'%4.3f') ' | p =' num2str(PVAL,'%7.6f') ];
    title(titstr);
    axis square
    text(max(TimeToPreviousHR)*.95,max(TimeToPreviousHR)*.90,{'.=data';'red=lsline';'blue=equality'},'horizontalalignment','right','fontname','courier');
    suptitstr=[label '| ACF Analysis of Intervals'];
    suptitle(suptitstr);
    %'.jpg'],'-djpeg','-r150');
    print(gcf,[path '001_' label '_ACF_AnalysisHistograms.eps'],'-deps','-r150');
    close all

%     figure(1);set(1,'units','pixels','position',figpos,'menubar','none');
%     PredictedTimeToNext=-1*TimeToPreviousHR+max(TimeToPreviousHR);
%     ResidualTimeToNext=TimeToNextHR-PredictedTimeToNext;
%     residarray=[ResidualTimeToNext TimeToNextHR TimeToPreviousHR PredictedTimeToNext];
%     sortresid=sortrows(residarray,3);
%     for i = 1:6
%         fprintf('sorted resid, i = %d resid = %f, TimeToNextHR=%f, TimeToPreviousHR=%f, Pred=%f, Intercept=%f\n',i,sortresid(i,:),max(TimeToPreviousHR));
%     end
%     for i = NumNextHR-5:NumNextHR
%         fprintf('sorted resid, i = %d resid = %f, TimeToNextHR=%f, TimeToPreviousHR=%f, Pred=%f, Intercept=%f\n',i,sortresid(i,:),max(TimeToPreviousHR));
%     end 
%     hold on;
%     plot(x,ResidualTimeToNext,'.k')
%     xlabel('Event Number');
%     ylabel('Residual from Line of Equality (sec)')
%     titstr=[NameOnly ' - Residual from Line of Equality (TimeToNextHB)'];
%     Ysmooth=malowess(x,ResidualTimeToNext,'span',0.25);
%     plot(x,Ysmooth,'b','linewidth',3)
%     title(titstr);
%     print(gcf,[PathName '003_' NameOnly '_PrePostResidual_' BreathType '.jpg'],'-djpeg','-r150');

end;
return

function [pACF_Random_Intervals]=GetRandomACF(NumNextHR,TimeToNextHR,x,whichstats)
a=min(TimeToNextHR);
b=max(TimeToNextHR);
RandomIntervals= a + (b-a).*rand(NumNextHR,1);
Random_stats = regstats(RandomIntervals,x,'linear',whichstats);
[ACF_Random_Intervals,lags]=xcorr(Random_stats.r,20,'coeff');
pACF_Random_Intervals=ACF_Random_Intervals(lags >= 0);
return


function [rsq,Chi,p]=BGTest(r,x,ACF,lags);
NumLags=3;
Chi=0;
p=0;
x=x';
N=length(r);
lag=zeros(N,NumLags);
for i = 2:NumLags+1;
   lag(1:N-i+1,i-1)=r(i:N);
   maxN=length(r(i:N));
end
%lag(maxN:N,:)=[];
%r(maxN:N)=[];
%x(maxN:N)=[];
%lag(maxN:N,:)=[];
%r(maxN:N)=[];
%x(maxN:N)=[];
for i = 1:NumLags
   [RHO] = corr(r,lag(:,i));
   %fprintf('lag %d \t my r = %8.6f \t ACF r = %8.6f\n',i,RHO,ACF(i+1))
end
x=[x lag];
whichstats = {'beta','rsquare','fstat','tstat'};
stats = regstats(r,x,'linear',whichstats);
f=stats.fstat.f;
fp=stats.fstat.pval;
dfnum=stats.fstat.dfr;
dfden=stats.fstat.dfe;
rsq=stats.rsquare;
Chi=(N)*rsq;
p=1-chi2cdf(Chi,NumLags);
%fprintf('\nF=%10.2f | df=%d,%d | p = %6.4f\n',f,dfnum,dfden,fp)
%fprintf('rsq=%5.2f|Chi=%f|p=%f\n',rsq,Chi,p)
%fprintf('\nbeta \t t-value \t pvalue');
%table=[stats.beta stats.tstat.t stats.tstat.pval]

return



function [hist] = myhistc(x,binedges)
NBins=length(binedges)-1;
hist = zeros(NBins,1);
for i = 1:length(x)
    for j = 1:NBins
        if x(i) >= binedges(j) && x(i) < binedges(j+1);hist(j)=hist(j)+1;end;
    end
end;
return