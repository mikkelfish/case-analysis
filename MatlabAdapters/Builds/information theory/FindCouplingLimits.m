function [fullhist inverseCDF edges10 area10 edges14 area14] = FindCouplingLimits(HR, shouldplot, path, label)
numHR=length(HR);
RR=diff(HR)*1000;
MaximumBinEnd_Low =floor(prctile(RR, 0));
MaximumBinEnd_High=floor(prctile(RR,100));
edges01 = floor(min(RR)):1:ceil(max(RR));

MaximumBinEnd_10= MaximumBinEnd_Low %floor(MaximumBinEnd_Low/10)*10
increment10=round(MaximumBinEnd_10/10);
edges10=zeros(11,1);
for i=2:11;edges10(i) = edges10(i-1)+increment10;end
diff(edges10);

MaximumBinEnd_15= MaximumBinEnd_High %floor(MaximumBinEnd_High/15)*15
increment15=round(MaximumBinEnd_15/15);
edges15=zeros(16,1);
for i=2:16;edges15(i) = edges15(i-1)+increment15;end;
diff(edges15);

edges14=edges15;
%edges14(15)=[];
length(edges14)
fullhist=histc(RR,edges01);

if shouldplot == 1

figure(1);set(1,'units','pixels');set(1,'position',[542         153        1056         648],'papersize',[11 8.5],'paperpositionmode','auto');
subplot(3,1,1);
bar(edges01(1:length(edges01))',fullhist,'k');
EndOfHist1=min(max(RR)+1,1400);
% xlim([min(RR)-1 EndOfHist1]);
xlim([0 EndOfHist1]);
text(MaximumBinEnd_10  ,0,                       '\downarrow','VerticalAlignment','bottom','HorizontalAlignment','center','FontSize',20);
text(MaximumBinEnd_15  ,0                       ,'\downarrow','VerticalAlignment','bottom','HorizontalAlignment','center','FontSize',20);
%text(MaximumBinEnd_Low ,max(n),'\downarrow','VerticalAlignment','bottom','HorizontalAlignment','center','FontSize',16);
%text(MaximumBinEnd_High,max(n),'\downarrow','VerticalAlignment','bottom','HorizontalAlignment','center','FontSize',16);
title('R-to-R Interval Histogram');
ylabel('Frequency');
text(MaximumBinEnd_10,.30*max(fullhist),'0.25 percentile','FontSize',12,'HorizontalAlignment','center','FontSize',10);
text(MaximumBinEnd_15,.30*max(fullhist),'99.75 percentile','FontSize',12,'HorizontalAlignment','center','FontSize',10);
box off;
text(100,-4,'ms','FontSize',14,'HorizontalAlignment','center');

end % end plot



%LengthCDF=MaximumBinEnd_15-MaximumBinEnd_10+1;
LengthCDF = length(fullhist);
myCDF=zeros(LengthCDF,1);
myCDF(1)=fullhist(1);
for i = 2:LengthCDF;
    %fprintf('%4.4d, %f,%d\n',out(i,:))
    
	
	%% I MADE A CHANGE
	if i <= length(fullhist)
		myCDF(i)=myCDF(i-1)+fullhist(i);
	%else myCDF(i) = myCDF(i-1);
	end
end

a = 1;
b = [1/10 1/10 1/10 1/10 1/10 1/10 1/10 1/10 1/10 1/10];
smooth_myCDF = filter(b,a,myCDF);


if shouldplot == 1
subplot(3,1,2);
[AX,H1,H2] = plotyy((1:length(myCDF))+MaximumBinEnd_Low,smooth_myCDF,(1:length(myCDF))+MaximumBinEnd_Low,smooth_myCDF/max(smooth_myCDF));
ylim([0 max(smooth_myCDF)]);
xlim(AX(1),[0 EndOfHist1]);
xlim(AX(2),[0 EndOfHist1]);
set(H1,'color','white','linewidth',1)
set(H2,'color','k','linewidth',2)
set(AX(1),'ytick',[0:200:max(smooth_myCDF)],'ycolor','k');ylabel(AX(1),'Cumulative Frequency')
set(AX(2),'ytick',[0 0.25 0.5 0.75 1.0])
get(AX(2),'yticklabel')
set(AX(2),'ycolor','k')
h=ylabel(AX(2),{'Cumulative Proportion';' ';});
%set(h,'Position',[1118.41 0.489286 1.00011])
set(h,'Position',[1175 0.489286 1.00011]);
set(h,'rotation',270);
box off;
title('R-R Interval Cumulative Density Function');
text(100,-200,'ms','FontSize',14,'HorizontalAlignment','center');
end % end plot

filler=ones(MaximumBinEnd_10-1,1).*max(myCDF);
size(ones(length(myCDF),1));
size(max(myCDF));
size(myCDF);
inverseCDF=(ones(length(myCDF),1).*max(myCDF))-myCDF;
inverseCDF=[filler;inverseCDF];
%csvwrite([PathStr '_inverseCDF.csv'],inverseCDF);
inverseCDF=inverseCDF/max(myCDF);
LowSumCDF =sum(inverseCDF(1:MaximumBinEnd_10));
HighSumCDF=sum(inverseCDF(1:MaximumBinEnd_15));
smooth_inverseCDF = filter(b,a,inverseCDF);
smooth_inverseCDF(1:10)=1.0;


if shouldplot == 1
subplot(3,1,3);

%hold on;
x = 1:length(smooth_inverseCDF);
y = smooth_inverseCDF';
plot(x,y,'-k','linewidth',2);
xlim([0 EndOfHist1]);
ycenter=inverseCDF(edges10(2))/2;
for i = 2:11;
    xx=[edges10(i) edges10(i)];yy=[0 inverseCDF(edges10(i))];%hold on;plot(xx,yy);
    xcenter=(edges10(i-1)+edges10(i))/2;
   %text(xcenter,ycenter,num2str(i-1),'VerticalAlignment','bottom','HorizontalAlignment','center','FontSize',9);    
end;
title('Relative Probability of I-to-R Intervals with No Coupling - 10 bin');
ylabel('Proportion');
text(100,-.2,'ms','FontSize',14,'HorizontalAlignment','center');

end

WhichBin=1;
for i = 1:10;
    SumInBin=0;
    BinStart=edges10(i)+1;
    BinEnd  =edges10(i+1);
    for j=BinStart:BinEnd;
        SumInBin=SumInBin+inverseCDF(j);
    end;
    AreaInBin10(WhichBin)=SumInBin/LowSumCDF;
    %fprintf('i=%4.4d,WhichBin=%2.2d,BinStart=%4.4d,BinEnd=%3.3d,Area=%6.4f,LowSumCDF=%8.2f\n',i,WhichBin,BinStart,BinEnd,AreaInBin10(WhichBin),LowSumCDF);
    WhichBin=WhichBin+1;
    if WhichBin == 11;break;end;
end;


%plot((1:length(inverseCDF))',inverseCDF);
ycenter=inverseCDF(edges14(2))/2;
for i = 2:16;
    xx=[edges14(i) edges14(i)];
        yy=[0 inverseCDF(edges14(i))];
	hold on;plot(xx,yy,'-k','linewidth',1);
    xcenter=(edges14(i-1)+edges14(i))/2;
    text(xcenter,ycenter,num2str(i-1),'VerticalAlignment','bottom'   ,'HorizontalAlignment','center','FontSize',9);
    if i>2;
        AverageBinHeight=(inverseCDF(edges14(i-1)) + inverseCDF(edges14(i)))/2;
        if AverageBinHeight<ycenter;text(xcenter,ycenter*0.8,'\downarrow','VerticalAlignment','top','HorizontalAlignment','center','FontSize',24);end;
    end;
end;
title('Relative Probability of I-to-R Intervals with No Coupling (Chance)');
ylabel('Relative Probability');

for WhichBin = 1:15;
    SumInBin=0;
    BinStart=edges14(WhichBin)+1;
    %if WhichBin==1;BinStart=0;end;
    BinEnd  =edges14(WhichBin+1);
    for j=BinStart:BinEnd;
        SumInBin=SumInBin+inverseCDF(j);
    end;
    AreaInBin14(WhichBin)=SumInBin/HighSumCDF;
    CountInBin14(WhichBin)=AreaInBin14(WhichBin)*200;
    %fprintf('i=%4.4d,WhichBin=%2.2d,BinStart=%4.4d,BinEnd=%3.3d,Area=%6.4f,LowSumCDF=%8.2f\n',i,WhichBin,BinStart,BinEnd,AreaInBin14(WhichBin),LowSumCDF);
end;

for i = 1:length(AreaInBin14);
    xcenter=edges14(i)+((edges14(i+1)-edges14(i))/2);
    text(xcenter,ycenter,num2str(AreaInBin14(i),'%3.3f'),'VerticalAlignment','top','HorizontalAlignment','center','FontSize',8);
    text(xcenter,ycenter*.85,num2str(CountInBin14(i),'%5.1f'),'VerticalAlignment','top','HorizontalAlignment','center','FontSize',8);
end;

edges10 = edges10;
edes14 = edges14;
area10 = AreaInBin10;
area14 = AreaInBin14;

    print(gcf,[path label '_probability.eps'],'-deps','-r150');

return

