%   MutInf.m
function [I,taugrid,E1,E2]=MutInf(data,taumax,nbins)
%   MutInf.m
%   usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
%   Mutual information of time-series data for different delays
%
%   Inputs:
%
%   data        time-series
%   taumax      maximum delay value
%   nbins       bins for histogram (default = 100)
%
%   Notes:      If taumax is one integer, mutual information is computed 
%               for all delay values up to taumax.  If taumax 
%
%   Outputs:
%
%   I           Mutual Information
%   taugrid     delay values
%   E1          Entropy of the time-series
%   E2          Entropies of the delayed time-series
%
%   Anatoly Zlotnik, June 19, 2006
%
%   [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
%       Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
%       University; 2006
%

if(size(data,2)>1) data=data'; end
if(nargin<3) nbins=100; end

N=length(data);
if(length(taumax)>1) 
    taugrid=taumax; else taugrid=[1:taumax]; end
T=length(taugrid);

for i=1:T
    tau=taugrid(i);
    [I(i),E1,E2(i)]=feval('mutualinf',data,tau,nbins);
end

function [MI,E1,E2]=mutualinf(data,tau,nbins)
N=length(data);
binsiz = (max(data)-min(data))/nbins;
binloc = [(min(data)+binsiz/2):binsiz:(max(data)-binsiz/2)];
P1 = hist(data(1:N-tau),binloc)/(N-tau); if(size(P1,2)>1) P1=P1'; end
P2 = hist(data(1+tau:N),binloc)/(N-tau); if(size(P2,2)>1) P2=P2'; end
PP = hist3([data(1:N-tau) data(1+tau:N)],'Edges',{binloc,binloc})/(N-tau);
PP(PP==0) = 1; P1(P1==0) = 1; P2(P2==0) = 1;
E1 = -sum(P1.*log(P1));
E2 = -sum(P2.*log(P2));
EE = -sum(sum(PP.*log(PP)));
MI = E1 + E2 - EE;