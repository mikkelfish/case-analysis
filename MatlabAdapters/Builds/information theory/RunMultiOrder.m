function [ChiLeft,ChiRight,MADLeft,MADRight,NbinsLeft,NbinsRight,LeftThreshold,RightThreshold] = RunMultiOrder(trigger1Data, trigger2Data, bestnum, mostvar, numCycles, numFakes, TimeRes, plot, save, label, path)

mkdir(path);
[trimmed1 trimmed2 TimeToPrevious TimeToNext MaximumBinEnd] = ...
    FindBest(trigger1Data, trigger2Data, bestnum, mostvar, plot, save, label, path);

trigger1Data = trimmed1;
trigger2Data = trimmed2;

TimeRepresented=(trigger1Data(length(trigger1Data))-trigger1Data(1))/60.;
multiorder=ComputeMultiOrder(trigger1Data,trigger2Data, numCycles);
numEvents=length(trigger1Data);
[realhisto,realedges]=CreateMultiOrderHistogram(multiorder,TimeRes);

%numFakes=10000;

a=trigger1Data(1);
b=trigger1Data(length(trigger1Data));
Fakes= a + (b-a).*rand(numFakes,1);
for i = 1:numFakes
    Fakes(i)=str2num(num2str(Fakes(i),'%8.3f'));
end

multiorder=ComputeMultiOrder(Fakes,trigger2Data,numCycles);
[histo_Fake,FakeEdges]=CreateMultiOrderHistogram(multiorder,TimeRes);
histo_Fake_Corrected=histo_Fake*numEvents/numFakes;

if plot == 1
PlotTrueAndFake(realedges,realhisto,FakeEdges,histo_Fake_Corrected,numEvents,numFakes,TimeRes,TimeRepresented, label);

if save == 1
print(gcf,[path label '_MultiOrderHistogram.eps'],'-deps','-r150');
end
end



[ChiLeft,ChiRight,MADLeft,MADRight,NbinsLeft,NbinsRight,LeftThreshold,RightThreshold]=...
GetMyChiSquare(realedges,realhisto,FakeEdges,histo_Fake_Corrected,TimeRes);


return



function [multiorder]=ComputeMultiOrder(event,HR,numCycles)
count=0;
multiorder=zeros(length(event)*numCycles*2,1);
for i = 1:length(event)
    IndicesOfPrev=find(HR<event(i),numCycles,'last');
    for j = length(IndicesOfPrev):-1:1
        count=count+1;
        multiorder(count)=HR(IndicesOfPrev(j))-event(i);
    end
    IndicesOfNext=find(HR>=event(i),numCycles,'first');
    for j = 1:length(IndicesOfNext)
        count=count+1;
        multiorder(count)=HR(IndicesOfNext(j))-event(i);
    end
end;
multiorder(multiorder == 0)=[];
multiorder=sort(multiorder);
return

function [histo,edges]=CreateMultiOrderHistogram(multiorder,TimeRes)

minevent=str2double(num2str(min(multiorder),'%6.2f'))-TimeRes;
maxevent=str2double(num2str(max(multiorder),'%6.2f'))+TimeRes;
count=0;
for i = 0:-TimeRes:minevent;count=count+1;edges(count)=i;end;
for i = TimeRes :TimeRes:maxevent;count=count+1;edges(count)=i;end;
edges=sort(edges)';

histo=histc(multiorder,edges);

%figure(3);set(3,'units','pixels','position',MyPosition,'menubar','none');
%hold on;bar(edges,histo,'histc');
%if strcmp(MultiOrderType,'I');xlabel('Time Before/After Inspiration (sec)');end
%if strcmp(MultiOrderType,'E');xlabel('Time Before/After Expiration (sec)');end
%if strcmp(MultiOrderType,'F');xlabel('Time Before/After Fake Event (sec)');end
%ylabel('Number of Events');
%titstr=['Interval Resolution = ' num2str(TimeRes) ' sec'];title(titstr);
%xx=[0 0];yy=[get(gca,'ylim')];plot(xx,yy,':r','linewidth',1);
%close all
return

function PlotTrueAndFake(edges1,histo1,edges2,histo2,numEvents,numFakes,TimeRes,TimeRepresented, label)


subplot(211);
hold on;
bar(edges1,histo1,'histc');
ylim([0 max(histo1)*1.25])
xx=[0 0];yy=[get(gca,'ylim')];plot(xx,yy,'--k','linewidth',3);
xlabel(['Time Before/After ' label ' (sec)']);
titstr=['MultiOrder Histogram ' label ' | # ' label ' = ' num2str(numEvents) ' | Time = ' num2str(TimeRepresented,'%4.1f') ' min'];

text(edges1(length(edges1)),max(histo1)*1.2,['Interval Resolution = ' num2str(TimeRes) ' sec'],'horizontalalignment','right')
title(titstr);
ylabel('count');
myylim=get(gca,'ylim');

subplot(212);
hold on;
bar(edges2,histo2,'histc');
xx=[0 0];yy=myylim;plot(xx,yy,'--k','linewidth',3);
xlabel('Time Before/After Fake Event (sec)');ylabel('count');
titstr=['MultiOrder Histogram - ' num2str(numFakes) ' Random (uniform) Fake events'];
title(titstr);
ylim(myylim);

[ChiLeft,ChiRight,MADLeft,MADRight,NbinsLeft,NbinsRight,LeftThreshold,RightThreshold]=...
GetMyChiSquare(edges1,histo1,edges2,histo2,TimeRes);

subplot(211);

text(-1,max(histo1)*1.1,['   Chi = ' num2str(ChiLeft ,'%8.2f') ', MAD=' num2str(MADLeft,'%6.2f') ],'horizontalalignment','left')
text( 1,max(histo1)*1.1,['   Chi = ' num2str(ChiRight,'%8.2f') ', MAD=' num2str(MADRight,'%6.2f')],'horizontalalignment','right')
subplot(211);xlim([-1.25 1.25]);
subplot(212);xlim([-1.25 1.25]);

subplot(212);
hold on;

x=[-1.5 -.75];y=[LeftThreshold   LeftThreshold];plot(x,y,'k','linewidth',2);
x=[ .75  1.5];y=[RightThreshold RightThreshold];plot(x,y,'k','linewidth',2);

titstr=[label ' MultiOrder Histogram Analysis '];
suptitle(titstr);

return

function [ChiLeft,ChiRight,MADLeft,MADRight,NbinsLeft,NbinsRight,LeftThreshold,RightThreshold]=...
          GetMyChiSquare(edges1,histo1,edges2,histo2,TimeRes)

lowedge=max(edges1(1),edges2(1));lowedge=str2num(num2str(lowedge,'%6.2f'));
hi_edge=min(edges1(length(edges1)),edges2(length(edges2)));hi_edge=str2num(num2str(hi_edge,'%6.2f'));
bins=(lowedge:TimeRes:hi_edge)';
bins=str2num(num2str(bins,'%6.2f'));
numEdges=length(bins);
count=1;
obsCounts=zeros(numEdges,1);
%fprintf('filling obsCount: LowerEdge = %f, Time Resolution = %f, UpperEdge = %f\n',lowedge,TimeRes,hi_edge);
for i = lowedge:TimeRes:hi_edge;
    if TimeRes < .1;
        i_as_edge=str2num(num2str(i,'%8.2f'));
    else
        i_as_edge=str2num(num2str(i,'%8.1f'));
    end;
    %fprintf('I = %f, i as edge = %f\n',i,i_as_edge)
    if isempty(edges1==i_as_edge);
        %fprintf('did not find an observed edge that matches i_as_edge\n');
        continue
    else
        %fprintf('found an observed edge that matches i_as_edge\n');
        index=find(edges1>i_as_edge-0.01 & edges1<i_as_edge+0.01);
    end
    if ~isempty(edges1==i_as_edge);
        for j = 1:1:size(index)
            obsCounts(count)=histo1(index(j));
            count = count + 1;
        end;
    end;
end
count=1;
expCounts=zeros(numEdges,1);
%fprintf('filling expCount....');
for i = lowedge:TimeRes:hi_edge;
    if TimeRes < .1;
        i_as_edge=str2num(num2str(i,'%8.2f'));
    else
        i_as_edge=str2num(num2str(i,'%8.1f'));
    end;
    %fprintf('I = %f, i as edge = %f\n',i,i_as_edge)
    count=count+1;
    if isempty(edges2==i_as_edge);
        continue %fprintf('did not find an observed edge that matches i_as_edge\n');
    else
        %fprintf('found an observed edge that matches i_as_edge\n');
        index=find(edges2>i_as_edge-0.01 & edges2<i_as_edge+0.01);
    end
    if ~isempty(edges2==i_as_edge)
        for j = 1:1:size(index)
            expCounts(count)=histo2(index(j));
            count=count+1;
        end
    end;
end

%[length(bins) length(obsCounts) length(expCounts)];
%[bins obsCounts expCounts];
%sum_obsCounts=sum(obsCounts);
%sum_expCounts=sum(expCounts);
%fprintf('total observed = %10.2f,total expected = %10.2f\n',sum_obsCounts,sum_expCounts)
%out=[bins,obsCounts,expCounts];


ZeroBin=find(bins==0);
% LowerLeftEnd=find(bins==-3.500);
% UpperRghtEnd=find(bins==3.400); 
LowerLeftEnd=find(bins==lowedge);
UpperRghtEnd=find(bins==hi_edge); 

%if isempty(ZeroBin     );
%    fprintf('ZeroBin Not Found |ZeroBin=%6.2f\n',ZeroBin);
%end;
%if isempty(LowerLeftEnd);
 %   fprintf('LowerLeftEnd Not Found | LowerLeftEnd=%6.2f| lowedge=%6.2f\n',LowerLeftEnd,lowedge);
  %  lowedge
   % bins
%end;
%if isempty(UpperRghtEnd);
   % fprintf('UpperRghtEnd Not Found | UpperRghtEnd=%6.2f\n',UpperRghtEnd);
%end;

%fprintf('Lower Left Edge = %5.2f\nLower Rght Edge = %5.2f\nUpper Left Edge = %5.2f\nUpper Rght Edge = %5.2f\n',...
%       bins(LowerLeftEnd),bins(ZeroBin-1),bins(ZeroBin),bins(UpperRghtEnd))

LeftMaxExpected=max(expCounts(LowerLeftEnd:ZeroBin-1));
LeftThreshold=0.5*LeftMaxExpected;

ChiLeft=0;
MADLeft=0;
NbinsLeft=0;
for i = LowerLeftEnd:1:ZeroBin;
    if expCounts(i) > LeftThreshold
        MADLeft=MADLeft+abs(obsCounts(i)-expCounts(i));    
        if expCounts(i)>0;
            ChiLeft=ChiLeft+((obsCounts(i)-expCounts(i)).^2)/expCounts(i);
        end;
        NbinsLeft=NbinsLeft+1;
    %     fprintf('LEFT :i=%d,bin=%4.2f,obsCounts=%d,expCounts=%6.2f,Chi=%8.2f\n',i,bins(i),obsCounts(i),expCounts(i),ChiLeft)
    %     pause
    end
end
MADLeft=MADLeft/NbinsLeft;
%fprintf('IorE=%s|Left|Chi=%8.2f,bins=%d,MAD=%8.2f\n',InspirOrExpir,ChiLeft,NbinsLeft,MADLeft)

RightMaxExpected=max(expCounts(ZeroBin:UpperRghtEnd));
RightThreshold=0.5*RightMaxExpected;
ChiRight=0;
MADRight=0;
NbinsRight=0;
for i = ZeroBin:1:UpperRghtEnd;
    if expCounts(i) > RightThreshold
        MADRight=MADRight+abs(obsCounts(i)-expCounts(i));    
        if expCounts(i)>0;
            ChiRight=ChiRight+((obsCounts(i)-expCounts(i)).^2)/expCounts(i);
        end;
        NbinsRight=NbinsRight+1;
    %     fprintf('RIGHT:i=%d,bin=%4.2f,obsCounts=%d,expCounts=%6.2f,Chi=%8.2f\n',i,bins(i),obsCounts(i),expCounts(i),ChiRight)
    %     pause
    end
end
MADRight=MADRight/NbinsRight;
%fprintf('IorE=%s|Rght|Chi=%8.2f,bins=%d,MAD=%8.2f\n',InspirOrExpir,ChiRigh
%t,NbinsRight,MADRight)
return
