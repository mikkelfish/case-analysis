function [SlopeEst,nTotal,FnTotal,ChangeInterval,FitData]= DFA(Y,TotalNumPoints, path, label)
% This function calulate the Detrended Fluctuation Analysis
% Ref: Peng C.K. "Quatifiction of scaling exponents and crossover phonomena
%      in nonstationary heartbeat time series"
%
% Inputs
% Y              : Input time series data
% TotalNumPoints : The default value is 40
%
% Outputs
% nToatl  : Calculated n values
% FnTotal : Calculated Fn Values

if size(Y,1)>size(Y,2)
    Y=Y';
end

if nargin<2
    TotalNumPoints=40;
end

Len = length(Y);

Y=cumsum(Y-mean(Y));

nTotal=fix(logspace(log10(10),log10(Len/10),TotalNumPoints));

nTotal(diff(nTotal)==0)=[];
FnTotal=[];

SumN=cumsum([1:max(nTotal)]);
SumN2=cumsum( ( [1: max(nTotal) ]).^2);
ForwardSumY=cumsum(Y);
BackwardSumY=cumsum(fliplr(Y));
ForwardSumYN=cumsum(Y.*[1:Len]);
BackwardSumYN=cumsum(fliplr(Y).*[1:Len]);

nTotal(find(nTotal<2))=[];
    
for  n = nTotal;

    InvMatA=([SumN2(n) SumN(n);SumN(n) n]^(-1));

    if rem(Len,n)==0
        % there is no need for backward estimation
        Yn=zeros(1,Len);
        Para=InvMatA*[ForwardSumYN(n);ForwardSumY(n)];
        Yn(1:n)=Para(1)*[1:n]+Para(2);

        for i=2:fix(Len/n)
            Temp = ForwardSumY(n*i)-ForwardSumY((i-1)*n) ;
            Para=InvMatA*[ForwardSumYN(i*n)-ForwardSumYN((i-1)*n)-(i-1)*n*Temp; Temp];
            Yn((1:n)+(i-1)*n)=Para(1)*[1:n]+Para(2);
        end
        FnTotal=[FnTotal sqrt(mean((Y-Yn).^2))];
    else
        % forward and backward estimation
        
        % Forward estimation
        Yn=zeros(1,fix(Len/n)*n);
        Para=InvMatA*[ForwardSumYN(n);ForwardSumY(n)];
        Yn(1:n)=Para(1)*[1:n]+Para(2);

        for i=2:fix(Len/n)
            Temp = ForwardSumY(n*i)-ForwardSumY((i-1)*n) ;
            Para=InvMatA*[ForwardSumYN(i*n)-ForwardSumYN((i-1)*n)-(i-1)*n*Temp; Temp];
            Yn((1:n)+(i-1)*n)=Para(1)*[1:n]+Para(2);
        end
        
        TempFn=sqrt(mean((Y(1:fix(Len/n)*n)-Yn).^2))/2;
        
        
        % Backward estimation
        Yn=zeros(1,fix(Len/n)*n);
        Para=InvMatA*[BackwardSumYN(n);BackwardSumY(n)];
        Yn(1:n)=Para(1)*[1:n]+Para(2);

        for i=2:fix(Len/n)
            Temp = BackwardSumY(n*i)-BackwardSumY((i-1)*n) ;
            Para=InvMatA*[BackwardSumYN(i*n)-BackwardSumYN((i-1)*n)-(i-1)*n*Temp; Temp];
            Yn((1:n)+(i-1)*n)=Para(1)*[1:n]+Para(2);
        end
        
        FnTotal=[FnTotal sqrt(mean(( Y([rem(Len,n)+1:Len])-fliplr(Yn) ).^2))/2+TempFn];
        
    end

end

%%

FnTotal=log10(FnTotal);

nTotal=log10(nTotal);

Len=length(FnTotal);

Slopes =zeros(1,Len);

% Create a vector of pointwise slopes
for i=2:Len-1
    Slopes(i)=(FnTotal(i+1)-FnTotal(i-1))/(nTotal(i+1)-nTotal(i-1));
end
Slopes(1) = (FnTotal(2)-FnTotal(1))/(nTotal(2)-nTotal(1));
Slopes(Len) = (FnTotal(Len)-FnTotal(Len-1))/(nTotal(Len)-nTotal(Len-1));


subplot(211)
plot(nTotal,Slopes,'*')
hold on



Slopes(find(isnan(Slopes)))=[];

[Out,Kout]=Palarm(Slopes);

PlatStarts=[1 Kout];

PlatEnds=[Kout-1 Len];



FitData=[];
for i = 1:length(PlatStarts)

    X = nTotal(PlatStarts(i):PlatEnds(i));
    Y = FnTotal(PlatStarts(i):PlatEnds(i));
    % Regression
    a(1,1)=length(X);
    a(1,2)=sum(X);
    a(2,1)=a(1,2);
    a(2,2)=sum(X.*X);
    b(1)=sum(Y);
    b(2)=sum(X.*Y);
    c = a\b';
    Intercept(i)=c(1);
    SlopeEst(i)=c(2);
    FitData=[FitData SlopeEst(i)*X+Intercept(i)*ones(size(X))];

end

ChangeInterval=[PlatStarts;PlatEnds];

for i=1:length(PlatStarts)
    plot(nTotal(PlatStarts(i):PlatEnds(i)),Out(PlatStarts(i):PlatEnds(i)),'color','r','LineWidth',1.5)
    grid on
    hold on
end

xlabel('Log(n)','FontName','Arial','FontSize',12)
ylabel('Estimated Slopes','FontName','Arial','FontSize',12)
legend('Pointwise Slopes','Detected Slopes')
title('DFA Slopes','FontName','Arial','FontSize',14)




subplot(212)
plot(nTotal,FnTotal,'*')
hold on;
grid on;
plot(nTotal,FitData,'r')

if nargin == 4
FigOutName=[path 'DFA_' label '.eps' ];   
print(gcf,FigOutName,'-deps','-r150');
end










