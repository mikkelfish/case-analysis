﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StatisticalAnalysis
{
    public class Stationarity
    {
        public static bool TestStationarity(double[] data, int numberDivisions, double tolerance, StatisticsDelegate[] tests, out double[][] stats,  out bool[] bySegment)
        {
            List<double[]> divs = new List<double[]>();
            int perDivision = data.Length / numberDivisions;
            for (int i = 0; i < numberDivisions; i++)
            {
                int startIndex = i * perDivision;
                if (data.Length - startIndex < perDivision - perDivision*0.05)
                    break;

                int amount = perDivision;
                if (data.Length - startIndex < perDivision)
                    amount = data.Length - startIndex;

                double[] div = new double[amount];

                Array.Copy(data, (i * perDivision), div, 0, amount);
                divs.Add(div);
            }

            double[][] all = new double[divs.Count + 1][];

            for (int i = 0; i < tests.Length; i++)
            {
                if (all[0] == null)
                    all[0] = new double[tests.Length];

                all[0][i] = tests[i](data, null);

                for (int j = 0; j < numberDivisions; j++)
                {
                    if (all[j + 1] == null)
                        all[j + 1] = new double[tests.Length];

                    all[j + 1][i] = tests[i](divs[j], null);
                }
            }

            stats = all;

            bool ok = true;
            bool[] good = new bool[divs.Count];
            for (int i = 0; i < good.Length; i++)
            {
                good[i] = true;
                for (int j = 0; j < all[0].Length; j++)
                {
                    if (all[0][j] < all[i + 1][j] - all[0][j] * tolerance ||
                        all[0][j] > all[i + 1][j] + all[0][j] * tolerance)
                    {
                        good[i] = false;
                        ok = false;
                    }
                }
            }
            bySegment = good;
            return ok;
        }
    }
}
