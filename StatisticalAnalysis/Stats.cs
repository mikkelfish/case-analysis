﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StatisticalAnalysis
{
    public static class Stats
    {
        public static double Average(double[] data, object args)
        {
            double sum = 0;
            for (int i = 0; i < data.Length; i++)
            {
                sum += data[i];
            }
            return sum / data.Length;
        }

        public static double StandardDev(double[] data, object args)
        {
            if (data.Length == 1) return 0;
            double avg = Average(data, null);
            double sd = 0;
            for (int i = 0; i < data.Length; i++)
            {
                sd += (data[i] - avg) * (data[i] - avg);
            }

            sd = sd / (data.Length - 1);
            return Math.Sqrt(sd);
        }

        public static double CoefficentOfVariation(double[] data, object args)
        {
            double avg = Average(data, null);
            double s = StandardDev(data, null);

            if (avg == 0) return 0;
            return s / avg;
        }

        //public static double Entropy(double[] data, double logBase)
        //{
        //    double sum = data.Sum();
        //    double[] normalizedData = data.Select(val => val / sum).ToArray();
        //    double result = -normalizedData.Aggregate((entropy, val) => entropy + (val == 0 ? 0 : val * Math.Log(val, logBase)));
        //    return result;
        //}
    }
}
