﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Effects;

//------------------------------------------------------------------------------//
//                                                                              //
// Author:  Derek Bartram                                                       //
// Date:    23/01/2008                                                          //
// Version: 1.000                                                               //
// Website: http://www.derek-bartram.co.uk                                      //
// Email:   webmaster@derek-bartram.co.uk                                       //
//                                                                              //
// This code is provided on a free to use and/or modify basis for personal work //
// provided that this banner remains in each of the source code files that is   //
// found in the original source. For any publicically available work (source    //
// and/or binaries 'Derek Bartram' and 'http://www.derek-bartram.co.uk' must be //
// credited in both the user documentation, source code (where applicable), and //
// in the user interface (typically Help > About would be appropiate). Please   //
// also contact myself via the provided email address to let me know where and  //
// what my code is being used for; this helps me provide better solutions for   //
// all.                                                                         //
//                                                                              //
// THIS SOURCE AND/OR COMPILED LIBRARY MUST NOT BE USED FOR COMMERCIAL WORK,    //
// including not-for-profit work, without prior consent.                        //
//                                                                              //
// This agreement overrides any other agreements made by any other parties. By  //
// using, viewing, linking, or compiling the included source or binaries you    //
// agree to the terms and conditions as set out here and in any included (if    //
// applicable) license.txt. For commercial licensing please see the web address //
// above or contact myself via email. Thank you.                                //
//                                                                              //
// Please contact me at the above email for further help, information,          //
// comments, suggestions, licensing, or feature requests. Thank you.            //
//                                                                              //
//                                                                              //
//------------------------------------------------------------------------------//

namespace DNBSoft.WPF.ProceedureDialog
{
    /// <summary>
    /// Interaction logic for VistaCanvasControl.xaml
    /// </summary>
    public partial class VistaCanvasControl : UserControl
    {
        public VistaCanvasControl()
        {
            InitializeComponent();

            #region setup brushes
            Color color1 = (Color)ColorConverter.ConvertFromString("Blue");
            Color color2 = (Color)ColorConverter.ConvertFromString("Green");
            GradientBrush small_gb = new LinearGradientBrush(color1, color2, 135);
            Color color3 = (Color)ColorConverter.ConvertFromString("LightBlue");
            Color color4 = (Color)ColorConverter.ConvertFromString("LightGreen");
            GradientBrush large_gb = new LinearGradientBrush(color3, color4, 135);
            OuterGlowBitmapEffect ogbe = new OuterGlowBitmapEffect();
            ogbe.GlowColor = (Color)(ColorConverter.ConvertFromString("LightBlue"));
            #endregion

            int fixX = -185;
            int fixY = -165;

            #region small

            #region bottom
            for (int i = 0; i < 5; i++)
            {
                PathFigure pf = new PathFigure();
                pf.StartPoint = new Point(150 + (i * 15) + fixX, 460 - (i * 5) + fixY);
                pf.Segments.Add(new ArcSegment(new Point(300 - (i * 5) + fixX, 300 + (i * 2) + fixY), new Size(500, 1000), 0, false, SweepDirection.Counterclockwise, true));
                PathGeometry pg = new PathGeometry();
                pg.Figures.Add(pf);
                Path p = new Path();
                p.Data = pg;
                p.StrokeThickness = 1;
                p.Stroke = small_gb; // new SolidColorBrush((Color)ColorConverter.ConvertFromString("LightBlue"));
                p.StrokeStartLineCap = PenLineCap.Round;
                p.StrokeEndLineCap = PenLineCap.Round;
                p.BitmapEffect = ogbe;

                canvas1.Children.Add(p);
            }
            #endregion

            #region top
            for (int i = 0; i < 5; i++)
            {
                PathFigure pf = new PathFigure();
                pf.StartPoint = new Point(100 + fixX, 400 + (i * 10) + fixY);
                pf.Segments.Add(new ArcSegment(new Point(300 - (i * 5) + fixX, 300 + (i * 2) + fixY), new Size(500, 1000), 0, false, SweepDirection.Counterclockwise, true));
                PathGeometry pg = new PathGeometry();
                pg.Figures.Add(pf);
                Path p = new Path();
                p.Data = pg;
                p.StrokeThickness = 1;
                p.Stroke = small_gb;// new SolidColorBrush((Color)ColorConverter.ConvertFromString("LightBlue"));
                p.StrokeStartLineCap = PenLineCap.Round;
                p.StrokeEndLineCap = PenLineCap.Round;
                p.BitmapEffect = ogbe;

                canvas1.Children.Add(p);
            }
            #endregion
            #endregion

            #region large
            ogbe = new OuterGlowBitmapEffect();
            ogbe.GlowColor = (Color)(ColorConverter.ConvertFromString("LightBlue"));
            ogbe.GlowSize = ogbe.GlowSize * 2;

            #region top
            for (int i = 0; i < 5; i++)
            {
                PathFigure pf = new PathFigure();
                pf.StartPoint = new Point(100 + fixX, 325 + (i * 15) + fixY);
                pf.Segments.Add(new ArcSegment(new Point(430 - (i * 15) + fixX, 150 + (i * 6) + fixY), new Size(1000, 2000), 0, false, SweepDirection.Counterclockwise, true));
                PathGeometry pg = new PathGeometry();
                pg.Figures.Add(pf);
                Path p = new Path();
                p.Data = pg;
                p.StrokeThickness = 1;
                p.Stroke = small_gb; // new SolidColorBrush((Color)ColorConverter.ConvertFromString("LightBlue"));
                p.StrokeStartLineCap = PenLineCap.Round;
                p.StrokeEndLineCap = PenLineCap.Round;
                p.BitmapEffect = ogbe;

                canvas1.Children.Add(p);
            }
            #endregion

            #region bottom
            for (int i = 0; i < 5; i++)
            {
                PathFigure pf = new PathFigure();
                pf.StartPoint = new Point(100 + (i * 35) + fixX, 525 - (i * 15) + fixY);
                pf.Segments.Add(new ArcSegment(new Point(430 - (i * 15) + fixX, 150 + (i * 6) + fixY), new Size(800, 1600), 0, false, SweepDirection.Counterclockwise, true));
                PathGeometry pg = new PathGeometry();
                pg.Figures.Add(pf);
                Path p = new Path();
                p.Data = pg;
                p.StrokeThickness = 1;
                p.Stroke = small_gb; // new SolidColorBrush((Color)ColorConverter.ConvertFromString("LightBlue"));
                p.StrokeStartLineCap = PenLineCap.Round;
                p.StrokeEndLineCap = PenLineCap.Round;
                p.BitmapEffect = ogbe;

                canvas1.Children.Add(p);
            }
            #endregion
            #endregion
        }
    }
}
