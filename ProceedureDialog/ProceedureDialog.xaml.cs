﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Threading;

//------------------------------------------------------------------------------//
//                                                                              //
// Author:  Derek Bartram                                                       //
// Date:    23/01/2008                                                          //
// Version: 1.000                                                               //
// Website: http://www.derek-bartram.co.uk                                      //
// Email:   webmaster@derek-bartram.co.uk                                       //
//                                                                              //
// This code is provided on a free to use and/or modify basis for personal work //
// provided that this banner remains in each of the source code files that is   //
// found in the original source. For any publicically available work (source    //
// and/or binaries 'Derek Bartram' and 'http://www.derek-bartram.co.uk' must be //
// credited in both the user documentation, source code (where applicable), and //
// in the user interface (typically Help > About would be appropiate). Please   //
// also contact myself via the provided email address to let me know where and  //
// what my code is being used for; this helps me provide better solutions for   //
// all.                                                                         //
//                                                                              //
// THIS SOURCE AND/OR COMPILED LIBRARY MUST NOT BE USED FOR COMMERCIAL WORK,    //
// including not-for-profit work, without prior consent.                        //
//                                                                              //
// This agreement overrides any other agreements made by any other parties. By  //
// using, viewing, linking, or compiling the included source or binaries you    //
// agree to the terms and conditions as set out here and in any included (if    //
// applicable) license.txt. For commercial licensing please see the web address //
// above or contact myself via email. Thank you.                                //
//                                                                              //
// Please contact me at the above email for further help, information,          //
// comments, suggestions, licensing, or feature requests. Thank you.            //
//                                                                              //
//                                                                              //
//------------------------------------------------------------------------------//

namespace DNBSoft.WPF.ProceedureDialog
{
    /// <summary>
    /// Interaction logic for ProceedureDialog.xaml
    /// </summary>
    public partial class ProceedureDialog : Window
    {
        #region class variables
        private IProceedureComponent introductionComponent = null;
        private List<IProceedureComponent> configurationComponents = null;
        private IProceedureComponent confirmationComponent = null;
        private IProceedureProcessComponent progressComponent = null;
        private IProceedureComponent resultsComponent = null;

        private IProceedureComponent currentComponent = null;

        private bool showProcessWarning = true;
        private bool proceedureComplete = false;
        private bool processing = false;

        private bool closeConfirmed = false;

        private TryCancelDialog cancelDialog;
        private delegate void RefreshDelegate();
        #endregion

        #region initialization
        public ProceedureDialog(String title, IProceedureComponent introductionComponent, List<IProceedureComponent> configurationComponents,
            IProceedureComponent confirmationComponent, IProceedureProcessComponent progressComponent, IProceedureComponent resultsComponent)
        {
            InitializeComponent();
            this.Title = title;

            cancelDialog = new TryCancelDialog(title);

            #region attach icon
            try
            {
                Uri myUri = new Uri("Cogs.bmp", UriKind.RelativeOrAbsolute);
                BmpBitmapDecoder decoder = new BmpBitmapDecoder(myUri, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                BitmapSource bitmapSource = decoder.Frames[0];

                imageIcon.Source = bitmapSource;
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Cogs.bmp not found, please place in application directory and restart application to apply icon.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            #endregion

            #region store proceedure components
            if (introductionComponent == null || confirmationComponent == null ||
                progressComponent == null || resultsComponent == null ||
                configurationComponents == null || configurationComponents.Count == 0)
            {
                throw new Exception("NULL IProceedureComponent or empty configuration components list passed to initialization of ProceedureDialog");
            }

            this.introductionComponent = introductionComponent;
            this.confirmationComponent = confirmationComponent;
            this.progressComponent = progressComponent;
            this.resultsComponent = resultsComponent;

            this.configurationComponents = new List<IProceedureComponent>();
            for (int i = 0; i < configurationComponents.Count; i++)
            {
                this.configurationComponents.Add(configurationComponents[i]);
            }
            #endregion

            #region populate key
            introductionLabel.Content = introductionComponent.KeyText;
            confirmationLabel.Content = confirmationComponent.KeyText;
            progressLabel.Content = progressComponent.KeyText;
            resultsLabel.Content = resultsComponent.KeyText;

            StackPanel sP = new StackPanel();
            sP.Orientation = Orientation.Vertical;
            configurationExpander.Content = sP;

            for (int i = 0; i < configurationComponents.Count; i++)
            {
                Label configurationLabel = new Label();
                configurationLabel.Content = configurationComponents[i].KeyText;
                configurationLabel.Margin = new Thickness(30, 0, 0, 0);
                configurationLabel.Height = 25;
                configurationLabel.Tag = configurationComponents[i];
                configurationLabel.MouseUp += new MouseButtonEventHandler(configurationLabel_MouseUp);
                sP.Children.Add(configurationLabel);
            }
            #endregion

            #region load introduction
            contentGrid.Children.Add((UserControl)introductionComponent);
            currentComponent = introductionComponent;
            #endregion

            #region hook finish process
            progressComponent.ProceedureCompleteDelegates.Add(new ProceedureCompleteDelegate(finishProcess));
            #endregion

            updateKeyLinks();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                // Obtain the window handle for WPF application
                IntPtr mainWindowPtr = new WindowInteropHelper(this).Handle;
                HwndSource mainWindowSrc = HwndSource.FromHwnd(mainWindowPtr);
                mainWindowSrc.CompositionTarget.BackgroundColor = Color.FromArgb(0, 0, 0, 0);

                // Get System Dpi
                System.Drawing.Graphics desktop = System.Drawing.Graphics.FromHwnd(mainWindowPtr);
                float DesktopDpiX = desktop.DpiX;
                float DesktopDpiY = desktop.DpiY;

                // Set Margins
                NonClientRegionAPI.MARGINS margins = new NonClientRegionAPI.MARGINS();

                // Extend glass frame into client area
                // Note that the default desktop Dpi is 96dpi. The  margins are
                // adjusted for the system Dpi.
                margins.cxLeftWidth = Convert.ToInt32(5 * (DesktopDpiX / 96));
                margins.cxRightWidth = Convert.ToInt32(5 * (DesktopDpiX / 96));
                margins.cyTopHeight = Convert.ToInt32(((int)75 + 5) * (DesktopDpiX / 96));
                margins.cyBottomHeight = Convert.ToInt32((5 - 10) * (DesktopDpiX / 96));

                int hr = NonClientRegionAPI.DwmExtendFrameIntoClientArea(mainWindowSrc.Handle, ref margins);
                //
                if (hr < 0)
                {
                    //DwmExtendFrameIntoClientArea Failed
                }


                headerGrid.Background = new SolidColorBrush(Color.FromArgb(128, 255, 255, 255));
                buttonBorder.Background = new SolidColorBrush(Color.FromArgb(128, 138, 215, 255));
                buttonBorder.BorderBrush = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
                contentBorder.BorderBrush = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
                contentBorder.Background = new SolidColorBrush(Color.FromArgb(200, 255, 255, 255));
                //keyGrid
                keyBorder.Background = new LinearGradientBrush(Color.FromArgb(128, 255, 255, 255), Color.FromArgb(128, 138, 215, 255), 90.0);
                keyBorder.BorderBrush = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
            }
            // If not Vista, paint background white.
            catch (DllNotFoundException)
            {
                Application.Current.MainWindow.Background = Brushes.WhiteSmoke;
                headerGrid.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            }
        }
        #endregion

        #region key handlers
        private void introductionLabel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (currentComponent != introductionComponent && currentComponent != progressComponent &&
                currentComponent != resultsComponent)
            {
                #region update key expanders
                if (currentComponent == confirmationComponent)
                {
                    confirmationExpander.IsExpanded = false;
                }
                else //must be config
                {
                    configurationExpander.IsExpanded = false;
                }

                introductionExpander.IsExpanded = true;
                #endregion

                currentComponent = introductionComponent;
                contentGrid.Children.RemoveRange(0, contentGrid.Children.Count);
                contentGrid.Children.Add((UserControl)currentComponent);

                updateButtons();
            }   
        }

        private void configurationLabel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (currentComponent != (IProceedureComponent)((Label)sender).Tag && currentComponent != progressComponent &&
                currentComponent != resultsComponent)
            {
                #region update key expanders
                if (currentComponent == introductionComponent)
                {
                    introductionExpander.IsExpanded = false;
                    configurationExpander.IsExpanded = true;
                }
                else if (currentComponent == confirmationComponent)
                {
                    confirmationExpander.IsExpanded = false;
                    configurationExpander.IsExpanded = true;
                }
                #endregion

                currentComponent = (IProceedureComponent)((Label)sender).Tag;
                contentGrid.Children.RemoveRange(0, contentGrid.Children.Count);
                contentGrid.Children.Add((UserControl)currentComponent);

                updateButtons();
            }
        }

        private void confirmationLabel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (currentComponent != confirmationExpander && currentComponent != progressComponent &&
                currentComponent != resultsComponent)
            {
                #region update key expanders
                if (currentComponent == introductionComponent)
                {
                    introductionExpander.IsExpanded = false;
                }
                else //must be config
                {
                    configurationExpander.IsExpanded = false;
                }

                confirmationExpander.IsExpanded = true;
                #endregion

                currentComponent = confirmationComponent;
                contentGrid.Children.RemoveRange(0, contentGrid.Children.Count);
                contentGrid.Children.Add((UserControl)currentComponent);

                updateButtons();
            } 
        }

        private void progressLabel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (currentComponent == resultsComponent)
            {
                #region update key expanders
                resultsExpander.IsExpanded = false;
                progressExpander.IsExpanded = true;
                #endregion

                currentComponent = progressComponent;
                contentGrid.Children.RemoveRange(0, contentGrid.Children.Count);
                contentGrid.Children.Add((UserControl)currentComponent);

                updateButtons();
            } 
        }

        private void resultsLabel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (proceedureComplete && currentComponent == progressComponent)
            {
                #region update key expanders
                progressExpander.IsExpanded = false;
                resultsExpander.IsExpanded = true;
                #endregion

                currentComponent = resultsComponent;
                contentGrid.Children.RemoveRange(0, contentGrid.Children.Count);
                contentGrid.Children.Add((UserControl)currentComponent);

                updateButtons();
            }
        }
        #endregion

        #region back / next / finish handlers
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            #region update key expanders
            if (currentComponent == configurationComponents[0])
            {
                configurationExpander.IsExpanded = false;
                introductionExpander.IsExpanded = true;
            }
            else if (currentComponent == confirmationComponent)
            {
                confirmationExpander.IsExpanded = false;
                configurationExpander.IsExpanded = true;
            }
            else if (currentComponent == progressComponent)
            {
                progressExpander.IsExpanded = false;
                confirmationExpander.IsExpanded = true;
            }
            else if (currentComponent == resultsComponent)
            {
                resultsExpander.IsExpanded = false;
                progressExpander.IsExpanded = true;
            }
            #endregion

            currentComponent = getBack(currentComponent);
            contentGrid.Children.RemoveAt(0);
            contentGrid.Children.Add((UserControl)currentComponent);

            updateButtons();
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            #region update key expanders
            if (currentComponent == introductionComponent)
            {
                introductionExpander.IsExpanded = false;
                configurationExpander.IsExpanded = true;
            }
            else if (currentComponent == configurationComponents[configurationComponents.Count - 1])
            {
                configurationExpander.IsExpanded = false;
                confirmationExpander.IsExpanded = true;
            }
            else if (currentComponent == confirmationComponent)
            {
                confirmationExpander.IsExpanded = false;
                progressExpander.IsExpanded = true;
            }
            else if (currentComponent == progressComponent)
            {
                progressExpander.IsExpanded = false;
                resultsExpander.IsExpanded = true;
            }
            #endregion

            currentComponent = getNext(currentComponent);
            contentGrid.Children.RemoveRange(0, contentGrid.Children.Count);
            contentGrid.Children.Add((UserControl)currentComponent);

            updateButtons();
        }

        private void finishButton_Click(object sender, RoutedEventArgs e)
        {
            if (currentComponent == confirmationComponent)
            {
                #region confirm action
                if (showProcessWarning)
                {
                    if (MessageBox.Show(this, "Are you sure you wish to continue?\r\n\r\nPerforming this operation may not be reversable or cancellable, and may take considerable time.", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Cancel) == MessageBoxResult.Cancel)
                    {
                        return;
                    }
                }
                #endregion

                #region update key expanders
                if (currentComponent == introductionComponent)
                {
                    introductionExpander.IsExpanded = false;
                    configurationExpander.IsExpanded = true;
                }
                else if (currentComponent == configurationComponents[configurationComponents.Count - 1])
                {
                    configurationExpander.IsExpanded = false;
                    confirmationExpander.IsExpanded = true;
                }
                else if (currentComponent == confirmationComponent)
                {
                    confirmationExpander.IsExpanded = false;
                    progressExpander.IsExpanded = true;
                }
                else if (currentComponent == progressComponent)
                {
                    progressExpander.IsExpanded = false;
                    resultsExpander.IsExpanded = true;
                }
                #endregion

                currentComponent = getNext(currentComponent);
                contentGrid.Children.RemoveAt(0);
                contentGrid.Children.Add((UserControl)currentComponent);

                updateButtons();

                processing = true;

                progressComponent.startProceedure();
            }
            else if (currentComponent == progressComponent)
            {
                currentComponent = getNext(currentComponent);
                contentGrid.Children.RemoveAt(0);
                contentGrid.Children.Add((UserControl)currentComponent);
                updateButtons();
            }
            else if (currentComponent == resultsComponent)
            {
                proceedureComplete = true;
                this.Close();
            }
        }

        private void updateButtons()
        {
            if (currentComponent == confirmationComponent)
            {
                backButton.IsEnabled = true;
                nextButton.IsEnabled = false;
                finishButton.IsEnabled = true;
            }
            else if (currentComponent == introductionComponent)
            {
                backButton.IsEnabled = false;
                nextButton.IsEnabled = true;
                finishButton.IsEnabled = false;
            }
            else if (currentComponent == progressComponent)
            {
                backButton.IsEnabled = false;
                nextButton.IsEnabled = false;
                finishButton.IsEnabled = proceedureComplete; ;
            }
            else if (currentComponent == resultsComponent)
            {
                backButton.IsEnabled = true;
                nextButton.IsEnabled = false;
                finishButton.IsEnabled = true;
                finishButton.Content = "Finish";
            }
            else
            {
                backButton.IsEnabled = true;
                nextButton.IsEnabled = true;
                finishButton.IsEnabled = false;
            }

            updateKeyLinks();
        }

        private void updateKeyLinks()
        {
            if (currentComponent == introductionComponent || currentComponent == confirmationComponent)
            {
                introductionLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
                UIElementCollection col = ((StackPanel)(configurationExpander.Content)).Children;
                for (int i = 0; i < col.Count; i++)
                {
                    ((Label)col[i]).Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
                }
                confirmationLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
                progressLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                resultsLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
            }
            else if (currentComponent == progressComponent)
            {
                if (proceedureComplete)
                {
                    introductionLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                    UIElementCollection col = ((StackPanel)(configurationExpander.Content)).Children;
                    for (int i = 0; i < col.Count; i++)
                    {
                        ((Label)col[i]).Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                    }
                    confirmationLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                    progressLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
                    resultsLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
                }
                else
                {
                    introductionLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                    UIElementCollection col = ((StackPanel)(configurationExpander.Content)).Children;
                    for (int i = 0; i < col.Count; i++)
                    {
                        ((Label)col[i]).Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                    }
                    confirmationLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                    progressLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
                    resultsLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                }
            }
            else if (currentComponent == resultsComponent)
            {
                introductionLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                UIElementCollection col = ((StackPanel)(configurationExpander.Content)).Children;
                for (int i = 0; i < col.Count; i++)
                {
                    ((Label)col[i]).Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                }
                confirmationLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                progressLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
                resultsLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
            }
            else
            {
                introductionLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
                UIElementCollection col = ((StackPanel)(configurationExpander.Content)).Children;
                for (int i = 0; i < col.Count; i++)
                {
                    ((Label)col[i]).Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
                }
                confirmationLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Blue"));
                progressLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
                resultsLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
            }
        }

        private IProceedureComponent getNext(IProceedureComponent current)
        {
            if (currentComponent == introductionComponent)
            {
                return configurationComponents[0];
            }
            else if (currentComponent == confirmationComponent)
            {
                return progressComponent;
            }
            else if (currentComponent == progressComponent)
            {
                return resultsComponent;
            }
            else if (currentComponent == resultsComponent)
            {
                return null;
            }
            else
            {
                for (int i = 0; i < configurationComponents.Count - 1; i++)
                {
                    if (currentComponent == configurationComponents[i])
                    {
                        return configurationComponents[i + 1];
                    }
                }

                return confirmationComponent;
            }
        }

        private IProceedureComponent getBack(IProceedureComponent current)
        {
            if (currentComponent == introductionComponent)
            {
                return null;
            }
            else if (currentComponent == confirmationComponent)
            {
                return configurationComponents[configurationComponents.Count - 1];
            }
            else if (currentComponent == progressComponent)
            {
                return null;
            }
            else if (currentComponent == resultsComponent)
            {
                return progressComponent;
            }
            else
            {
                for (int i = 1; i < configurationComponents.Count; i++)
                {
                    if (currentComponent == configurationComponents[i])
                    {
                        return configurationComponents[i - 1];
                    }
                }

                return introductionComponent;
            }
        }
        #endregion

        #region cancel / close handlers
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (processing)
            {
                if (MessageBox.Show(this, "Are you sure you want to cancel processing?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                {
                    Thread t = new Thread(new ThreadStart(waitForCancel));
                    t.Name = "Wait for cancel thread";
                    t.Priority = ThreadPriority.BelowNormal;
                    t.Start();

                    cancelDialog.ShowDialog();
                }
            }
            else if (proceedureComplete)
            {
                closeConfirmed = true;
                this.Close();
            }
            else
            {
                if (MessageBox.Show(this, "Are you sure you want to cancel this wizard?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                {
                    closeConfirmed = true;
                    this.Close();
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!closeConfirmed)
            {
                if (processing)
                {
                    if (MessageBox.Show(this, "Are you sure you want to cancel processing?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    {
                        e.Cancel = true;
                    }
                    else
                    {
                        e.Cancel = true;

                        Thread t = new Thread(new ThreadStart(waitForCancel));
                        t.Name = "Wait for cancel thread";
                        t.Priority = ThreadPriority.BelowNormal;
                        t.Start();

                        cancelDialog.ShowDialog();
                    }
                }
                else if (proceedureComplete)
                {
                    e.Cancel = false;
                }
                else
                {
                    if (MessageBox.Show(this, "Are you sure you want to cancel this wizard?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    {
                        e.Cancel = true;
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                }
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void waitForCancel()
        {
            bool result = progressComponent.cancelProceedure();
            Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new RefreshDelegate(cancelDialog.Hide));

            if (result)
            {
                closeConfirmed = true;
                Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new RefreshDelegate(this.Close));
            }
            else
            {
                Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new RefreshDelegate(this.waitForCancelFail));
            }
        }

        private void waitForCancelFail()
        {
            MessageBox.Show(this, "Error cancelling proceedure...\r\n\r\nEither the operation cannot be cancelled or the process cannot be terminated at this time; check the user documentation for more information.", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
        }
        #endregion

        #region processing handlers
        private void finishProcess()
        {
            finishButton.Content = "Finish";
            finishButton.IsEnabled = true;
            proceedureComplete = true;
            processing = false;

            Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new RefreshDelegate(this.updateKeyLinks));
        }
        #endregion

        #region accessors
        public IProceedureComponent IntroductionComponent
        {
            get
            {
                return introductionComponent;
            }
        }

        public List<IProceedureComponent> ConfigurationComponents
        {
            get
            {
                List<IProceedureComponent> cc = new List<IProceedureComponent>();
                for (int i = 0; i < configurationComponents.Count; i++)
                {
                    cc.Add(configurationComponents[i]);
                }
                return cc;
            }
        }

        public IProceedureComponent ConfirmationComponent
        {
            get
            {
                return confirmationComponent;
            }
        }

        public IProceedureProcessComponent ProgressComponent
        {
            get
            {
                return progressComponent;
            }
        }

        public IProceedureComponent ResultsComponent
        {
            get
            {
                return resultsComponent;
            }
        }

        public String HeaderTitle
        {
            get
            {
                return headingLabel.Content.ToString();
            }
            set
            {
                headingLabel.Content = value;
            }
        }

        public String HeaderDescription
        {
            get
            {
                return summaryLabel.Content.ToString();
            }
            set
            {
                summaryLabel.Content = value;
            }
        }
        #endregion

        

    }
}
