﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Taramon.Exceller;
using System.Collections;

namespace ExcelEvaluation
{
    public enum ExcelDataOrientation { Vertical, Horizontal };

    public abstract class ExcelDataSet
    {

        private string[][] dataFormatted;

        private string[] names;

      

        protected ReadOnlyCollection<string> GetFormattedData(string name)
        {
            int which = -1;
            for (int i = 0; i < this.names.Length; i++)
            {
                if (this.names[i] == name)
                {
                    which = i;
                    break;
                }
            }

            return new ReadOnlyCollection<string>(this.dataFormatted[which]);
        }

        private string iterate(ref string column, ref int cellnum, ExcelDataOrientation orientation, int spacing)
        {
            if (orientation == ExcelDataOrientation.Vertical)
            {
                cellnum = cellnum + spacing;
                return column + cellnum.ToString();
            }

            char last = column[column.Length - 1];
            last = (char)((int)last + spacing);
            if (last > 'Z')
            {
                int numOver = last - 'Z';

                int numToAdvance = numOver / 26;
                int numToAdvance2 = numOver % 26;

                //Set the second value of the column
                last = (char)((int)'A' + numToAdvance2 - 1);

                if (column.Length == 1)
                {
                    //Append the new part
                    column = "A" + new string(new char[]{last});
                }

                column = (char)((int)column[0] + numToAdvance) + column.Substring(1);
            }
            else
                column = column.Substring(0, column.Length-1) + new string(new char[] { last });

            return column + cellnum.ToString();
        }

        protected void FillAdapter(string[] names, string file, string sheet, string startCell, ExcelDataOrientation orientation, int numberToRead, int[] spacing)
        {
            this.dataFormatted = new string[names.Length][];
            this.names = names;
            if (spacing == null)
            {
                spacing = new int[names.Length];
            }

            for (int i = 0; i < names.Length; i++)
            {
//                this.dataFormatted[i] = new List<string>();
                if (spacing[i] == 0) spacing[i] = 1;
            }

            using (ExcelManager manager = new ExcelManager())
            {
                manager.Open(file);

                if (sheet != null)
                {
                    manager.ActivateSheet(sheet);
                }

                string column = "";
                int cellnum = 0;
                for (int i = 0; i < startCell.Length; i++)
                {
                    if (char.IsDigit(startCell[i]))
                    {
                        string rest = startCell.Substring(i);
                        if (!int.TryParse(rest, out cellnum))
                            throw new InvalidOperationException("Passed cell " + startCell + " not valid.");
                    }
                    else column += startCell[i];
                }

                column = column.ToUpper();

                bool readUntilEnd = false;
                if (numberToRead < 0)
                {
                    if (orientation == ExcelDataOrientation.Horizontal)
                        numberToRead = manager.CountRows();
                    else numberToRead = manager.CountColumns();

                    numberToRead -= cellnum;

                    readUntilEnd = true;
                }


                ExcelDataOrientation next =
                       (orientation == ExcelDataOrientation.Horizontal ? ExcelDataOrientation.Vertical :
                           ExcelDataOrientation.Horizontal);

                for (int i = 0; i < names.Length; i++)
                {
                    string targetCell = column;
                    int targetNum = cellnum;
                    string endCell = this.iterate(ref targetCell, ref targetNum, next, numberToRead);
                    string[] list = manager.GetRangeFormattedValues(startCell, endCell);
                    this.dataFormatted[i] = list;

                    startCell = this.iterate(ref column, ref cellnum, orientation, spacing[i]);
                }
               

//                int totalToRead = 0;
 //               for (int i = 0; i < spacing.Length; i++)
  //              {
   //                 totalToRead += spacing[i];
    //            }
  //              totalToRead--;
                

                 
                


                
                //for (int i = 0; i < numberToRead; i++)
                //{

                    //string targetCell = startCell;
                    //string targetcolumn = column;
                    //int targetnum = cellnum;
                    //bool success = false;
                    //for (int j = 0; j < names.Length; j++)
                    //{
                    //    DateTime now = DateTime.Now;
                    //    string val = manager.GetFormattedValue(targetCell) as string;
                    //    if (val != null)
                    //        success = true;
                    //    span1 = span1.Add(DateTime.Now.Subtract(now));

                    //    now = DateTime.Now;
                    //    this.dataFormatted[j].Add(val);

                    //    span2 = span2.Add(DateTime.Now.Subtract(now));
                    //    now = DateTime.Now;

                    //    targetCell = this.iterate(ref targetcolumn, ref targetnum, orientation, spacing[j]);

                    //    span3 = span3.Add(DateTime.Now.Subtract(now));
                    
                    //}

                    //If we're taking a blank line to say to stop reading then remove the last line
                //    if (!success && readUntilEnd)
                //    {
                //        for (int j = 0; j < names.Length; j++)
                //        {
                //            this.dataFormatted[j].RemoveAt(this.dataFormatted[j].Count - 1);
                //        }
                //        break;
                //    }

                //    ExcelDataOrientation next =
                //        (orientation == ExcelDataOrientation.Horizontal ? ExcelDataOrientation.Vertical :
                //            ExcelDataOrientation.Horizontal);
                //    startCell = this.iterate(ref column, ref cellnum, next, 1);

                //}
            }
        }
    }
}
