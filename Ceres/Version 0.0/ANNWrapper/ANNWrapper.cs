using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;

namespace ANNWrapper
{
    public class ANNWrapper
    {
        //[DllImport("ANN.dll", EntryPoint = "findNearestNeighbors"), SuppressUnmanagedCodeSecurity]
        //private extern unsafe static int* _FindNearestNeighbors(double* data, int dataCount, int dimension, double eps);
        [DllImport("ANNNew.dll", EntryPoint = "_FreeMemory"), SuppressUnmanagedCodeSecurity]
        private extern static void _FreeMemory();

        [DllImport("ANNNew.dll", EntryPoint = "_FindNearest"), SuppressUnmanagedCodeSecurity]
        private extern unsafe static int* _FindNearest(double* data, int dataCount, int dimension, double eps);

        public static int[] FindNearestNeighbors(double[,] data, double eps)
        {
            int dataCount = data.GetLength(0);
            int dimension = data.GetLength(1);

            int[] toRet = new int[dataCount];
            unsafe
            {
                fixed (double* dataP = data)
                {
               // double** temp = new double[dataCount][];

                    int* retVale = _FindNearest(dataP, dataCount, dimension, eps);
                   for (int i = 0; i < dataCount; i++)
                   {
                       toRet[i] = retVale[i];
                   }

                   _FreeMemory();
                }

            }

            return toRet;
        }

        [DllImport("ANNNew.dll", EntryPoint = "_FindNeighborsWithinR"), SuppressUnmanagedCodeSecurity]
        private extern unsafe static double** _FindNeighborsWithinR(double* data, int dataCount, int dimension, double r, double eps, int* numNeighbors, int*** ids);
        public static double[][] FindNeighborsWithinR(double[,] data, double r, double eps, out int[][] ids)
        {
            int dataCount = data.GetLength(0);
            int dimension = data.GetLength(1);

            double[][] toRet = new double[dataCount][];
            ids = new int[dataCount][];
            int[] numNeighbors = new int[dataCount];
            unsafe
            {
                fixed (double* dataP = data)
                {
                    fixed (int* neighborP = numNeighbors)
                    {
                        // double** temp = new double[dataCount][];
                        int** idP=(int**)0;
                        double** retVale = _FindNeighborsWithinR(dataP, dataCount, dimension, r, eps, neighborP, &idP);
                        for (int i = 0; i < dataCount; i++)
                        {
                            toRet[i] = new double[neighborP[i]];
                            ids[i] = new int[neighborP[i]];
                            for (int j = 0; j < toRet[i].Length; j++)
                            {
                                toRet[i][j] = retVale[i][j];
                                ids[i][j] = idP[i][j];
                            }
                        }

                        _FreeMemory();
                        
                    }
                }
            }

            return toRet;
        }
    }
}
