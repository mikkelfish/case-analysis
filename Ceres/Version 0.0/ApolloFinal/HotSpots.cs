//using System;
//using System.Collections.Generic;
//using System.Text;
//using CeresBase.Data;
//using ApolloFinal.Tools.BatchProcessing;
//using System.Windows.Forms;

//namespace ApolloFinal
//{
//    public class HotSpot
//    {
//        public override bool Equals(object obj)
//        {
//            if(!(obj is HotSpot)) return false;
//            HotSpot o = obj as HotSpot;
//            return o.name == this.name && o.experiment == this.experiment && o.beginTime == this.beginTime && o.endTime == this.endTime;
//        }

//        public override int GetHashCode()
//        {
//            return (this.name + this.experiment + this.beginTime + this.endTime).GetHashCode();
//        }

//        private string name;
//        public string Name
//        {
//            get { return name; }
//        }
//        private float beginTime;
//        public float BeginTime
//        {
//            get { return beginTime; }
//        }

//        private float endTime;
//        public float EndTime
//        {
//            get { return endTime; }
//        }

//        private string description;
//        public string Description
//        {
//            get { return description; }
//        }

//        private string experiment;
//        public string Experiment
//        {
//            get { return experiment; }
//        }

//        public static bool ParseSpot(string toParse, out string name, out float begin, out float end, out bool user)
//        {
//            string[] splitUser = toParse.Split('|');
//            if (splitUser.Length != 2)
//            {
//                user = false;
//                name = "";
//                end = float.MaxValue;
//                begin = float.MaxValue;
//                return false;
//            }

//            user = false;
//            if (splitUser[0] == "UserCreated") user = true;

//            try
//            {
//                string[] splitName = splitUser[1].Split(new string[] { ": " }, StringSplitOptions.RemoveEmptyEntries);
//                name = splitName[0];

//                string[] splitTime = splitName[1].Split('-');

//                begin = float.Parse(splitTime[0]);
//                end = float.Parse(splitTime[1]);
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(splitUser[1]);
//                user = false;
//                name = "";
//                end = float.MaxValue;
//                begin = float.MaxValue;
//            }

//            return true;

//        }

//        public override string ToString()
//        {
//            string user = this.isUser ? "UserCreated" : "";
//            return user + "|" + this.name.Replace('/', ',').Replace('-', ',') + ": " + beginTime + "-" + endTime;
//        }

//        private bool isUser = false;
//        public bool IsUser
//        {
//            get
//            {
//                return this.isUser;
//            }
//            set
//            {
//                this.isUser = value;
//            }
//        }

//        public HotSpot(string experiment, string name, string description, float beginTime, float endTime)
//        {
//            this.experiment = experiment;
//            this.name = name;
//            this.description = description;
//            this.beginTime = beginTime;
//            this.endTime = endTime;

//            this.PopulateWithDefaults();
//        }

//        public void PopulateWithDefaults()
//        {
//            IBatchLabel[] labels = CeresBase.General.Utilities.CreateInterfaces<IBatchLabel>(null);
//            foreach (IBatchLabel label in labels)
//            {
//                HotSpotExtension[] exts = label.GetDefaultExtensions();
//                if (exts != null)
//                {
//                    foreach (HotSpotExtension ext in exts)
//                    {
//                        if (!this.HasExtension(ext.Category, ext.Name))
//                            this.AddExtension(ext.Category, ext.Name, ext.Value);
//                    }
//                }
//            }
//        }

//        public bool HasExtension(string category, string name)
//        {
//            if (!this.extensions.ContainsKey(category)) return false;
//            foreach (HotSpotExtension ext in this.extensions[category])
//            {
//                if (ext.Name == name) return true;
//            }

//            return false;
//        }

//        public void AddExtension(string category, string name, object defaultVal)
//        {
//            if (!this.extensions.ContainsKey(category))
//            {
//                this.extensions.Add(category, new List<HotSpotExtension>());
//            }

//            HotSpotExtension ext = new HotSpotExtension(name, category, defaultVal);
//            if (this.extensions[category].Contains(ext)) throw new Exception("This extension already exists");

//            this.extensions[category].Add(ext);
//        }

//        public HotSpotExtension GetExtension(string category, string name)
//        {
//            if (!this.extensions.ContainsKey(category)) return null;

//            foreach (HotSpotExtension ext in this.extensions[category])
//            {
//                if (ext.Name == name) return ext;
//            }

//            return null;
//        }

//        public HotSpotExtension[] GetExtensions(string category)
//        {
//            if (!this.extensions.ContainsKey(category)) return null;
//            return this.extensions[category].ToArray();
//        }

//        public string[] GetCategories()
//        {
//            string[] toRet = new string[this.extensions.Keys.Count];
//            this.extensions.Keys.CopyTo(toRet, 0);
//            return toRet;
//        }


//        private Dictionary<string, List<HotSpotExtension>> extensions = new Dictionary<string, List<HotSpotExtension>>();
//        //public Dictionary<string, HotSpotExtension> Extensions
//        //{
//        //    get
//        //    {
//        //        return this.extensions;
//        //    }
//        //}

//        public class HotSpotExtension
//        {
//            public override bool Equals(object obj)
//            {
//                if (!(obj is HotSpotExtension))
//                    return false;

//                HotSpotExtension ext = obj as HotSpotExtension;
//                return ext.category == this.category && ext.name == this.name;
//            }

//            public override int GetHashCode()
//            {
//                return (this.category + this.name).GetHashCode();
//            }

//            private string name;
//            public string Name
//            {
//                get { return this.name; }
//            }

//            private string category;
//            public string Category
//            {
//                get { return this.category; }
//            }

//            private object val;
//            public object Value
//            {
//                get
//                {
//                    return this.val;
//                }
//                set
//                {
//                    this.val = value;
//                }
//            }

//            public HotSpotExtension(string name, string category, object val)
//            {
//                this.name = name;
//                this.category = category;
//                this.val = val;
//            }
//        }
//    }
//}
