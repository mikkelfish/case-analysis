//namespace ApolloFinal.UI
//{
//    partial class ImageManager
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.listBoxStored = new System.Windows.Forms.ListBox();
//            this.label1 = new System.Windows.Forms.Label();
//            this.buttonStore = new System.Windows.Forms.Button();
//            this.buttonView = new System.Windows.Forms.Button();
//            this.buttonWrite = new System.Windows.Forms.Button();
//            this.textBox1 = new System.Windows.Forms.TextBox();
//            this.groupBoxLast = new System.Windows.Forms.GroupBox();
//            this.groupBox1 = new System.Windows.Forms.GroupBox();
//            this.buttonDelete = new System.Windows.Forms.Button();
//            this.buttonWriteStored = new System.Windows.Forms.Button();
//            this.buttonViewStored = new System.Windows.Forms.Button();
//            this.groupBoxLast.SuspendLayout();
//            this.groupBox1.SuspendLayout();
//            this.SuspendLayout();
//            // 
//            // listBoxStored
//            // 
//            this.listBoxStored.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
//                        | System.Windows.Forms.AnchorStyles.Left)
//                        | System.Windows.Forms.AnchorStyles.Right)));
//            this.listBoxStored.FormattingEnabled = true;
//            this.listBoxStored.Location = new System.Drawing.Point(7, 19);
//            this.listBoxStored.Name = "listBoxStored";
//            this.listBoxStored.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
//            this.listBoxStored.Size = new System.Drawing.Size(290, 173);
//            this.listBoxStored.TabIndex = 0;
//            this.listBoxStored.SelectedIndexChanged += new System.EventHandler(this.listBoxStored_SelectedIndexChanged);
//            // 
//            // label1
//            // 
//            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//            this.label1.AutoSize = true;
//            this.label1.Location = new System.Drawing.Point(82, 13);
//            this.label1.Name = "label1";
//            this.label1.Size = new System.Drawing.Size(0, 13);
//            this.label1.TabIndex = 1;
//            this.label1.Click += new System.EventHandler(this.label1_Click);
//            // 
//            // buttonStore
//            // 
//            this.buttonStore.Location = new System.Drawing.Point(26, 45);
//            this.buttonStore.Name = "buttonStore";
//            this.buttonStore.Size = new System.Drawing.Size(75, 23);
//            this.buttonStore.TabIndex = 3;
//            this.buttonStore.Text = "Store";
//            this.buttonStore.UseVisualStyleBackColor = true;
//            this.buttonStore.Click += new System.EventHandler(this.buttonStore_Click);
//            // 
//            // buttonView
//            // 
//            this.buttonView.Location = new System.Drawing.Point(107, 45);
//            this.buttonView.Name = "buttonView";
//            this.buttonView.Size = new System.Drawing.Size(75, 23);
//            this.buttonView.TabIndex = 4;
//            this.buttonView.Text = "View";
//            this.buttonView.UseVisualStyleBackColor = true;
//            this.buttonView.Click += new System.EventHandler(this.buttonView_Click);
//            // 
//            // buttonWrite
//            // 
//            this.buttonWrite.Location = new System.Drawing.Point(188, 45);
//            this.buttonWrite.Name = "buttonWrite";
//            this.buttonWrite.Size = new System.Drawing.Size(75, 23);
//            this.buttonWrite.TabIndex = 5;
//            this.buttonWrite.Text = "Write";
//            this.buttonWrite.UseVisualStyleBackColor = true;
//            this.buttonWrite.Click += new System.EventHandler(this.buttonWrite_Click);
//            // 
//            // textBox1
//            // 
//            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//                        | System.Windows.Forms.AnchorStyles.Right)));
//            this.textBox1.Location = new System.Drawing.Point(6, 19);
//            this.textBox1.Name = "textBox1";
//            this.textBox1.Size = new System.Drawing.Size(290, 20);
//            this.textBox1.TabIndex = 7;
//            // 
//            // groupBoxLast
//            // 
//            this.groupBoxLast.Controls.Add(this.textBox1);
//            this.groupBoxLast.Controls.Add(this.buttonStore);
//            this.groupBoxLast.Controls.Add(this.buttonWrite);
//            this.groupBoxLast.Controls.Add(this.buttonView);
//            this.groupBoxLast.Location = new System.Drawing.Point(13, 12);
//            this.groupBoxLast.Name = "groupBoxLast";
//            this.groupBoxLast.Size = new System.Drawing.Size(302, 78);
//            this.groupBoxLast.TabIndex = 8;
//            this.groupBoxLast.TabStop = false;
//            this.groupBoxLast.Text = "Last";
//            // 
//            // groupBox1
//            // 
//            this.groupBox1.Controls.Add(this.buttonDelete);
//            this.groupBox1.Controls.Add(this.buttonWriteStored);
//            this.groupBox1.Controls.Add(this.buttonViewStored);
//            this.groupBox1.Controls.Add(this.listBoxStored);
//            this.groupBox1.Location = new System.Drawing.Point(12, 96);
//            this.groupBox1.Name = "groupBox1";
//            this.groupBox1.Size = new System.Drawing.Size(302, 244);
//            this.groupBox1.TabIndex = 9;
//            this.groupBox1.TabStop = false;
//            this.groupBox1.Text = "Stored";
//            // 
//            // buttonDelete
//            // 
//            this.buttonDelete.Location = new System.Drawing.Point(189, 207);
//            this.buttonDelete.Name = "buttonDelete";
//            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
//            this.buttonDelete.TabIndex = 10;
//            this.buttonDelete.Text = "Delete";
//            this.buttonDelete.UseVisualStyleBackColor = true;
//            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
//            // 
//            // buttonWriteStored
//            // 
//            this.buttonWriteStored.Location = new System.Drawing.Point(108, 207);
//            this.buttonWriteStored.Name = "buttonWriteStored";
//            this.buttonWriteStored.Size = new System.Drawing.Size(75, 23);
//            this.buttonWriteStored.TabIndex = 9;
//            this.buttonWriteStored.Text = "Write";
//            this.buttonWriteStored.UseVisualStyleBackColor = true;
//            this.buttonWriteStored.Click += new System.EventHandler(this.buttonWriteStored_Click);
//            // 
//            // buttonViewStored
//            // 
//            this.buttonViewStored.Location = new System.Drawing.Point(27, 207);
//            this.buttonViewStored.Name = "buttonViewStored";
//            this.buttonViewStored.Size = new System.Drawing.Size(75, 23);
//            this.buttonViewStored.TabIndex = 8;
//            this.buttonViewStored.Text = "View";
//            this.buttonViewStored.UseVisualStyleBackColor = true;
//            this.buttonViewStored.Click += new System.EventHandler(this.buttonViewStored_Click);
//            // 
//            // ImageManager
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.ClientSize = new System.Drawing.Size(335, 351);
//            this.Controls.Add(this.groupBox1);
//            this.Controls.Add(this.groupBoxLast);
//            this.Controls.Add(this.label1);
//            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
//            this.Name = "ImageManager";
//            this.Text = "ImageManager";
//            this.groupBoxLast.ResumeLayout(false);
//            this.groupBoxLast.PerformLayout();
//            this.groupBox1.ResumeLayout(false);
//            this.ResumeLayout(false);
//            this.PerformLayout();

//        }

//        #endregion

//        private System.Windows.Forms.ListBox listBoxStored;
//        private System.Windows.Forms.Label label1;
//        private System.Windows.Forms.Button buttonStore;
//        private System.Windows.Forms.Button buttonView;
//        private System.Windows.Forms.Button buttonWrite;
//        private System.Windows.Forms.TextBox textBox1;
//        private System.Windows.Forms.GroupBox groupBoxLast;
//        private System.Windows.Forms.GroupBox groupBox1;
//        private System.Windows.Forms.Button buttonWriteStored;
//        private System.Windows.Forms.Button buttonViewStored;
//        private System.Windows.Forms.Button buttonDelete;
//    }
//}