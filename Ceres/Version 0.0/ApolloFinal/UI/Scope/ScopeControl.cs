using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CeresBase.Data;
using ThreadSafeConstructs;
using CeresBase.IO.ImageCreation;
using CeresBase.UI;

namespace ApolloFinal.UI.Scope
{
    public partial class ScopeControl : UserControl
    {
        //private int numSteps;
        private ScopeControl scopeParent;
        private ToolTip tip = new ToolTip();
        
        private Dictionary<SpikeVariable, float[]> allData = new Dictionary<SpikeVariable,float[]>();
        private Dictionary<SpikeVariable, float[]> curData = new Dictionary<SpikeVariable,float[]>();

        private bool lockedToMaster;
        public bool LockedToMaster 
        {
            get
            {
                return this.lockedToMaster;
            }
            set
            {
                this.lockedToMaster = value;
             
                    this.contextMenuStrip1.Items[1].Enabled = !this.lockedToMaster;
                
            }
        }
        
        private List<SpikeVariable> vars;
        public SpikeVariable[] Variables
        {
            get
            {
                return this.vars.ToArray();
            }
        }

        public void AddVariable(SpikeVariable var)
        {
            if (this.vars.Contains(var)) return;
            this.vars.Add(var);
            if (var.Min < this.min) this.min = var.Min;
            if (var.Max > this.max) this.max = var.Max;
            if (var.Resolution < this.resolution) this.resolution = var.Resolution;

            this.label1.Text = "";
            foreach (SpikeVariable vara in this.vars)
            {
                this.label1.Text += vara.FullDescription + " ";
            }

        }

        public void RemoveVariable(SpikeVariable var)
        {
            this.vars.Remove(var);
            this.label1.Text = "";
            this.min = double.MaxValue;
            this.max = double.MinValue;
            this.resolution = double.MaxValue;

            foreach (SpikeVariable vara in this.vars)
            {
                this.label1.Text += vara.FullDescription + " ";
                if (vara.Min < this.min) this.min = vara.Min;
                if (vara.Max > this.max) this.max = vara.Max;
                if (vara.Resolution < this.resolution) this.resolution = vara.Resolution;
            }

        }

        public ThreadSafeEvent<PaintEventArgs> BeforeTrace = new ThreadSafeEvent<PaintEventArgs>();
        public ThreadSafeEvent<PaintEventArgs> AfterTrace = new ThreadSafeEvent<PaintEventArgs>();


        private List<ScopeControl> list;
        public List<ScopeControl> Children
        {
            get
            {
                return this.list;
            }
        }

        private ScopeMaster window;
        
        private int state = 0;
        private double tempMin = 0;
        private double tempMax = 0;

        private Bitmap bitmap;

        private List<float> beginSelection = new List<float>();
        private List<float> endSelection = new List<float>();
        private readonly SolidBrush[] selectionColors = new SolidBrush[] 
            { new SolidBrush(Color.Orange), new SolidBrush(Color.LightSteelBlue), new SolidBrush(Color.RosyBrown), new SolidBrush(Color.LightGray), 
            new SolidBrush(Color.LightGreen)};

        private bool subControl;
        public bool SubControl
        {
            get
            {
                return this.subControl;
            }
        }

        private double max;
        public double Max
        {
            get { return max; }
            set { max = value; }
        }

        private double min;
        public double Min
        {
            get { return min; }
            set { min = value; }
        }

        private bool middleMouseFunction = true;
        public bool MiddleMouseFunction
        {
            get { return middleMouseFunction; }
            set { middleMouseFunction = value; }
        }

        private bool leftMouseFunction = true;
        public bool LeftMouseFunction
        {
            get { return leftMouseFunction; }
            set { leftMouseFunction = value; }
        }

        private bool rightMouseFunction = true;
        public bool RightMouseFunction
        {
            get { return rightMouseFunction; }
            set { rightMouseFunction = value; }
        }


        private double resolution = double.MaxValue;
        public double Resolution
        {
            get { return resolution; }
        }

        private bool useLocalNormalization;
        public bool UseLocalNormalization
        {
            get { return useLocalNormalization; }
            set
            {
                this.useLocalNormalization = value;
                lock (this)
                {
                    this.bitmap = null;
                }
                this.Invalidate();
            }
        }
	
	

        

        private double start;
        public double Start
        {
            get
            {
                return this.start;
            }
            set
            {
                if (this.subControl) return;
                this.start = value;
                if (this.start < 0)
                {
                    this.start = 0;
                }
                this.getData();
            }
        }

        private void getData()
        {
            foreach (SpikeVariable var in this.Variables)
            {

                DataFrame frame = var[0];

                if ((var.Header as SpikeVariableHeader).SpikeDesc == SpikeVariableHeader.SpikeType.Raw ||
                    var.SpikeType == SpikeVariableHeader.SpikeType.Integrated)
                {
                    

                    //List<float> temp = new List<float>();

                    //int[] startB = new int[] { (int)(this.start / var.Resolution) };
                    //int[] endB = new int[] { (int)((this.start + this.length) / var.Resolution) - 1 };
                    //if (endB[0] >= frame.Pool.DimensionLengths[0])
                    //    endB[0] = frame.Pool.DimensionLengths[0];

                    //double res = 1.0;

                    ////if (endB[0] - startB[0] > 1500000)
                    ////{
                    ////    res = (endB[0] - startB[0]) / 1500000;
                    ////}
                    //int[] strideB = new int[] { (int)res };


                    //frame.Pool.GetData(startB, endB, strideB,
                    //        delegate(float[] buffer, uint[] startI, uint[] endI, uint[] strides)
                    //        {
                    //            temp.AddRange(buffer);
                    //        }
                    //    );

                    if (!this.curData.ContainsKey(var))
                        this.curData.Add(var, var.GetTimeSeriesData(this.start, this.start + this.length));
                    else this.curData[var] = var.GetTimeSeriesData(this.start, this.start + this.length);
                }
                else
                {
                    if (!allData.ContainsKey(var))
                    {
                        allData.Add(var, var.GetTimeSeriesData(0, double.MaxValue));

                        //List<float> temp = new List<float>();
                        //frame.Pool.GetData(null, null,
                        //    delegate(float[] buffer, uint[] start, uint[] count)
                        //    {
                        //        for (int i = 0; i < buffer.Length; i++)
                        //        {
                        //            //If there is a repeat it means that it is a tag to skip
                        //            if (i > 0 && buffer[i] == buffer[i - 1])
                        //            {
                        //                i++;
                        //            }
                        //            else
                        //            {
                        //                temp.Add(buffer[i]);
                        //            }
                        //        }
                        //        //temp.AddRange(buffer);
                        //    }
                        //);
                        //allData.Add(var, temp.ToArray());

                        //for (int i = 0; i < allData[var].Length; i++)
                        //{
                        //    allData[var][i] *= (float)var.Resolution;
                        //}
                    }

                    double index1 = Math.Floor(CeresBase.MathConstructs.Utilities.BinarySearch<float>((float)(this.start), allData[var]));
                    if (index1 < 0) index1 = 0;
                    if (index1 > allData[var].Length) index1 = allData[var].Length;

                    double index2 = Math.Ceiling(CeresBase.MathConstructs.Utilities.BinarySearch<float>((float)(this.start + this.length),
                        allData[var]));

                    if (index2 > allData[var].Length) index2 = allData[var].Length;
                    if (index2 < 0) index2 = 0;

                    this.curData[var] = new float[(int)index2 - (int)index1];
                    Array.Copy(this.allData[var], (int)index1, this.curData[var], 0, (int)index2 - (int)index1);
                }
            }

            lock (this)
            {
                this.bitmap = null;
            }
            this.Invalidate();
        }

        private double length;
        public double Length
        {
            get
            {
                return this.length;
            }
            set
            {
                if (this.subControl) return;
                this.length = value;
                this.getData();
            }
        }

        private PointF[] createPoints(SpikeVariable var, int x, int y, int width, int height)
        {
            List<PointF> points = new List<PointF>();
            float[] thisData = null;
            lock (this)
            {
                if (!this.curData.ContainsKey(var)) return null;
                thisData = this.curData[var];
            }

            if (thisData == null || (this.min == 0 && this.max == 0)) return null;

            float[] maxes = new float[width + 1];
            float[] mins = new float[width + 1];
            for (int i = 0; i < maxes.Length; i++)
            {
                maxes[i] = float.MinValue;
                mins[i] = float.MaxValue;
            }

            if (var.SpikeType == SpikeVariableHeader.SpikeType.Raw ||
                var.SpikeType == SpikeVariableHeader.SpikeType.Integrated)
            {
                int maxPoints = (int)(this.length / var.Resolution);

                double perPoint = (double)width / (maxPoints > thisData.Length ? (double)maxPoints : (double)thisData.Length);
                
                double lastx = x;
                float min = (float)this.min;
                float max = (float)this.max;
                float missing = CeresBase.Data.DataConstants.GetMissingValue<float>();

                if (this.useLocalNormalization)
                {
                    min = float.MaxValue;
                    max = float.MinValue;
                    for (int i = 0; i < thisData.Length; i++)
                    {
                        if (thisData[i] == missing) continue;
                        if (thisData[i] < min)
                        {
                            min = thisData[i];
                        }
                        else if (thisData[i] > max)
                        {
                            max = thisData[i];
                        }

                    }
                }

                //min = min;
               // max = max;
                for (int i = 0; i < thisData.Length; i++)
                {
                    float val = thisData[i];
                    
                    float perc = .5F;
                    if (val == missing)
                        perc = 0;
                    else if(min != max) perc = (val - min) / (max - min);


                    if (perPoint < .01)
                    {
                        int index = (int)Math.Round(lastx, 0);
                        if (index >= maxes.Length) continue;
                        float yCoord = (float)((1.0 - perc) * height + y);
                        if (yCoord < mins[index]) mins[index] = yCoord;
                        if (yCoord > maxes[index]) maxes[index] = yCoord;
                    }
                    else points.Add(new PointF((float)lastx, (float)((1.0 - perc) * height + y)));
                    
                    lastx += perPoint;
                }

                if (perPoint < .01)
                {
                    for (int i = 0; i < maxes.Length; i++)
                    {
                        if (mins[i] == float.MaxValue || maxes[i] == float.MinValue) continue;
                        points.Add(new PointF(i, mins[i]));
                        points.Add(new PointF(i, maxes[i]));
                    }
                }
            }
            else if (var.SpikeType == SpikeVariableHeader.SpikeType.Pulse)
            {
                double lastTime = this.length + this.start;
                double firstTime = this.start;
                int padding = 10;
                int drawWidth = width - padding * 2 - 2;
                int drawHeight = height - 5 - 2;
                for (int i = 0; i < thisData.Length; i++)
                {
                    double val = thisData[i];
                    double perc = (val - firstTime) / (lastTime - firstTime);
                    if (perc < 0 || perc > 1) continue;
                    points.Add(new Point(x + padding + (int)(perc * drawWidth), y + this.label1.Height + 5));
                    points.Add(new Point(x + padding + (int)(perc * drawWidth), y + drawHeight));
                }
            }

            return points.ToArray();
        }

        private void drawImage(SpikeVariable var, ImageCreator creator, ImageElement parent, Rectangle bounds, PointF[] points)
        {
            if (var.SpikeType == SpikeVariableHeader.SpikeType.Raw ||
                              var.SpikeType == SpikeVariableHeader.SpikeType.Integrated)
            {
                Lines lines = new Lines(parent);
                lines.MaxXRes = 6000;

                foreach (PointF point in points)
                {
                   // Point p = new Point((int)point.X, (int)point.Y);
                    lines.AddPoint(point);
                }
                
                creator.AddImageElement(lines);
            }
            else
            {
                for (int i = 0; i < points.Length; i += 2)
                {
                    Line line = new Line(parent, new PointF(points[i].X, points[i].Y), new PointF(points[i + 1].X,
                        points[i + 1].Y));
                    creator.AddImageElement(line);
                }


                int padding = 10;
                int drawWidth = bounds.Width - padding * 2;
                int drawHeight = bounds.Height - 5;
                Line bottom = new Line(parent, new PointF(bounds.X + padding, bounds.Y + drawHeight), 
                    new PointF(bounds.X + drawWidth + padding, bounds.Y + drawHeight));
                creator.AddImageElement(bottom);
                // e.Graphics.DrawLine(Pens.Black, e.ClipRectangle.X + padding, e.ClipRectangle.Y + drawHeight, e.ClipRectangle.X + drawWidth + padding, e.ClipRectangle.Y + drawHeight);
            }
        }

        public void ManualPaint(PaintEventArgs e)
        {
            Bitmap printmap = new Bitmap((int)(e.ClipRectangle.Width), (int)(e.ClipRectangle.Height ));
            Graphics g = Graphics.FromImage(printmap);
            g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.Default;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            //g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            //g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            //g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
            //g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

            foreach (SpikeVariable var in this.Variables)
            {
                PointF[] points = this.createPoints(var, 0, 0, printmap.Width, printmap.Height);
                this.manualPaint(var, new PaintEventArgs(g, new Rectangle(0, 0, printmap.Width, printmap.Height)), points);
            }
           
            
            e.Graphics.DrawImage(printmap, e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width,
                e.ClipRectangle.Height);
            g.Dispose();
            printmap.Dispose();

            

            e.Graphics.DrawString(this.label1.Text, this.label1.Font, Brushes.Black, e.ClipRectangle.X + 5, e.ClipRectangle.Y + 5);
            SizeF strSize = e.Graphics.MeasureString(this.label1.Text, this.label1.Font);
            if (this.SubControl)
            {
                e.Graphics.FillRectangle(new SolidBrush(this.BackColor), e.ClipRectangle.X + strSize.Width + 5, e.ClipRectangle.Y + 5, strSize.Height, strSize.Height);
                Label start = (Label)this.Controls["StartLabel"];
                e.Graphics.DrawString(start.Text, start.Font, Brushes.Black, e.ClipRectangle.X + 5, e.ClipRectangle.Y + e.ClipRectangle.Height - 5 - strSize.Height);
                Label end = (Label)this.Controls["EndLabel"];
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Far;
                format.LineAlignment = StringAlignment.Near;
                Rectangle f = new Rectangle(e.ClipRectangle.X + e.ClipRectangle.Width - 255, e.ClipRectangle.Y + e.ClipRectangle.Height - 5 - (int)strSize.Height,
                    250, (int)strSize.Height);
                e.Graphics.DrawString(end.Text, end.Font, Brushes.Black, f, format);
            }

            e.Graphics.DrawRectangle(Pens.Black, e.ClipRectangle);
        }

        private void manualPaint(SpikeVariable var, PaintEventArgs e, PointF[] points)
        {
            lock (this)
            {
                if (points == null || points.Length == 0) return;



                if (var.SpikeType == SpikeVariableHeader.SpikeType.Raw ||
                    var.SpikeType == SpikeVariableHeader.SpikeType.Integrated)
                {
                //    e.Graphics.DrawLines(Pens.Black, points);
                    if (points.Length == this.Width * 2 -2)
                    {
                        for (int i = 0; i < points.Length; i += 2)
                        {
                            e.Graphics.DrawLine(Pens.Black, points[i], points[i + 1]);
                        }
                    }
                    else e.Graphics.DrawLines(Pens.Black, points);
                }
                else
                {
                    for (int i = 0; i < points.Length; i += 2)
                    {
                        e.Graphics.DrawLine(Pens.Black, points[i], points[i + 1]);
                    }


                    int padding = 10;
                    int drawWidth = e.ClipRectangle.Width - padding * 2;
                    int drawHeight = e.ClipRectangle.Height - 5;
                    e.Graphics.DrawLine(Pens.Black, e.ClipRectangle.X + padding, e.ClipRectangle.Y + drawHeight, e.ClipRectangle.X + drawWidth + padding, e.ClipRectangle.Y + drawHeight);
                }

            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Bitmap map = null;
            lock (this)
            {
                if (this.bitmap == null)
                {
                    this.bitmap = new Bitmap(e.ClipRectangle.Width, e.ClipRectangle.Height, e.Graphics);
                    Graphics g = Graphics.FromImage(this.bitmap);

                    foreach (SpikeVariable var in this.Variables)
                    {
                        PointF[] points = this.createPoints(var, e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width, e.ClipRectangle.Height);
                        if(points != null) this.manualPaint(var, new PaintEventArgs(g, e.ClipRectangle), points);
                    }
                }
                map = this.bitmap;
            }

            for (int i = 0; i < this.beginSelection.Count; i++)
            {
                double perc1 = (this.beginSelection[i] - this.start) / this.length;
                double perc2 = (this.endSelection[i] - this.start) / this.length;

                int beg = (int)(perc1 * e.ClipRectangle.Width);
                int end = (int)(perc2 * e.ClipRectangle.Width);

                if ((beg < 0 && end < 0) || (beg > e.ClipRectangle.Width && end > e.ClipRectangle.Width)) continue;
                if (beg < 0 && end < e.ClipRectangle.Width) beg = 0;
                if (beg >= 0 && end > e.ClipRectangle.Width) end = 0;


                e.Graphics.FillRectangle(this.selectionColors[i], beg + e.ClipRectangle.X, e.ClipRectangle.Y, end - beg, e.ClipRectangle.Height);
            }

            this.BeforeTrace.CallEvent(this, e);

            e.Graphics.DrawImage(map, 0, 0);

            if (this.state > 0)
            {
                double perc1 = 1.0 - (this.tempMax - this.min) / (this.max - this.min);
                double perc2 = 1.0 - (this.tempMin - this.min) / (this.max - this.min);

                Pen pen1 = this.state == 1 ? new Pen(Color.Red) : new Pen(Color.Green);
                Pen pen2 = this.state == 2 ? new Pen(Color.Red) : new Pen(Color.Green); 

                e.Graphics.DrawLine(pen1, 0, (float)(perc1 * this.Height), this.Width, (float)(perc1 * this.Height));
                e.Graphics.DrawLine(pen2, 0, (float)(perc2 * this.Height), this.Width, (float)(perc2 * this.Height));

            }

            this.AfterTrace.CallEvent(this, e);


            base.OnPaint(e);
        }

        private void removeChild(ScopeControl child)
        {
            int index = -1;
            lock (this)
            {
                 index = this.list.IndexOf(child);
                if (index < 0) return;
                this.beginSelection.RemoveAt(index);
                this.endSelection.RemoveAt(index);
                this.list.RemoveAt(index);
                for (int i = index; i < this.list.Count; i++)
                {
                    this.list[i].BackColor = this.selectionColors[i].Color;
                }
            }
            this.Invalidate();
        }

        public void SetStart(double start)
        {
            this.start = start;
        }

        public ScopeControl() : this(null)
        {

        }

        public ScopeControl(ScopeMaster window)
        {
            InitializeComponent();
            this.vars = new List<SpikeVariable>();
           // this.numSteps = Screen.FromControl(this).Bounds.Width * 200;
            this.label1.Text = "";


            this.window = window;
            this.list = new List<ScopeControl>();
            this.subControl = false;

            this.max = float.MinValue;
            this.min = float.MaxValue;

            foreach (SpikeVariable var in this.Variables)
            {
                if (var.Max > this.max)
                {
                    this.max = var.Max;
                }
                if (var.Min < this.min)
                {
                    this.min = var.Min;
                }
            }
            Button button = new Button();
            button.Location = new Point(this.Width - 12, 0);
            button.Size = new Size(10, 10);
            button.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            button.TabStop = false;

            button.Click += new EventHandler(button_Click);
            button.FlatStyle = FlatStyle.System;
            this.Controls.Add(button);


            this.LockedToMaster = true;


            Button buttonLock = new Button();
            buttonLock.Location = new Point(this.Width - 24, 0);
            buttonLock.Size = new Size(10, 10);
            buttonLock.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            buttonLock.ForeColor = Color.Green;
            buttonLock.BackColor = Color.Green;

            buttonLock.TabStop = false;
            buttonLock.FlatStyle = FlatStyle.Flat;
            buttonLock.Click += new EventHandler(buttonLock_Click);
            this.Controls.Add(buttonLock);



        }

        void buttonLock_Click(object sender, EventArgs e)
        {
            this.LockedToMaster = !this.LockedToMaster;
            if (this.LockedToMaster)
            {
                (sender as Button).ForeColor = Color.Green;
                (sender as Button).BackColor = Color.Green;
            }
            else
            {
                (sender as Button).ForeColor = Color.Red;
                (sender as Button).BackColor = Color.Red;
            }


        }

        //private ScopeControl(List<SpikeVariable> vars, ScopeMaster window, ScopeControl scopeParent, Color backcolor, double start, double length)
        //{
        //    this.max = float.MinValue;
        //    this.min = float.MaxValue;

        //    this.vars = vars;
        //    foreach (SpikeVariable var in this.Variables)
        //    {
        //        if (var.Max > this.max)
        //        {
        //            this.max = var.Max;
        //        }
        //        if (var.Min < this.min)
        //        {
        //            this.min = var.Min;
        //        }
        //    }

        //    InitializeComponent();
        //    this.start = start;
        //    this.length = length;
        //    this.subControl = true;
        //    this.vars = vars;
        //   // this.numSteps = Screen.FromControl(this).Bounds.Width * 200;


        //    this.window = window;
        //    this.label1.Text = "";

        //    foreach (SpikeVariable var in vars)
        //    {
        //        this.label1.Text += var.FullDescription + " ";
        //    }
        //    this.BackColor = backcolor;
        //    Button button = new Button();
        //    button.Location = new Point(this.Width - 12, 0);
        //    button.Size = new Size(10, 10);
        //    button.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        //    button.Click += new EventHandler(button_Click);
        //    button.BackColor = Color.White;
        //    this.Controls.Add(button);
        //    this.scopeParent = scopeParent;
        //    this.scopeParent.list.Add(this);

        //    Label label = new Label();
        //    label.AutoSize = true;
        //    label.Name = "StartLabel";
        //    label.Text = "Start: " + Math.Round(this.start, 0).ToString();
        //    label.Location = new Point(5, this.Height - label.Height + 3);
        //    label.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
        //    label.BackColor = Color.FromArgb(0, this.BackColor);
        //    this.Controls.Add(label);

        //    label = new Label();
        //    label.AutoSize = false;
        //    label.Size = new Size(250, label.Height);
        //    label.TextAlign = ContentAlignment.TopRight;
        //    label.Name = "EndLabel";
        //    label.Text = "End: " + Math.Round((this.length + this.start)).ToString();
        //    label.Location = new Point(this.Width - 255, this.Height - label.Height + 3);
        //    label.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
        //    label.BackColor = Color.FromArgb(0, this.BackColor);

        //    this.Controls.Add(label);
        //    this.getData();

        //}

        void button_Click(object sender, EventArgs e)
        {
            if (this.window != null)
            {
                this.window.RemoveScopeControl(this);
                if(this.scopeParent != null) this.scopeParent.removeChild(this);
                this.curData = null;
            }
        }

        private void ScopeControl_Resize(object sender, EventArgs e)
        {
            lock (this)
            {
                this.bitmap = null;
            }
        }

        private double beginMouseTime = -1;

        private void ScopeControl_MouseDown(object sender, MouseEventArgs e)
        {
            double perc = (double)e.X / (double)this.Width;
            float time = (float)(perc * this.length + this.start);

            bool move = true;
            if (e.Button == MouseButtons.Left && this.LeftMouseFunction)
            {
                if (this.LockedToMaster || Microsoft.Win32.Imports.IsKeyDown(Keys.Control))
                {
                    int loc = e.X + Cursor.Size.Width;
                    tip.Show("Time: " + Math.Round(time, 2).ToString(), this, this.Width / 2, 15);
                    move = false;
                }
            }
            

            if (!this.LockedToMaster &&  e.Button == MouseButtons.Left && this.LeftMouseFunction)
            {
                if (move)
                {
                    int loc = e.X + Cursor.Size.Width;
                    tip.Show("Start: " + Math.Round(this.start, 2).ToString() + " End: " + Math.Round(this.start + this.length, 2).ToString(),
                        this, this.Width / 2, 15);
                    beginMouseTime = time;
                    this.Invalidate();
                }

            }

            //if (tip.Active && e.Button != MouseButtons.Right)
            //{
            //    this.Invalidate();
            //}

            if (e.Button == MouseButtons.Middle && this.MiddleMouseFunction)
            {
                if (this.state == 0)
                {
                    this.tempMax = this.max;
                    this.tempMin = this.min;
                    this.state++;
                    this.Invalidate();
                }
            }

            //if (e.Button != MouseButtons.Right || !Microsoft.Win32.Imports.IsKeyDown(Keys.ControlKey) || this.subControl || !this.RightMouseFunction) return;
            //if (this.beginSelection.Count == this.selectionColors.Length)
            //{
            //    MessageBox.Show("Cannot create any more zooms");
            //    return;
            //}


            //lock (this)
            //{
            //    this.beginSelection.Add(time);
            //    this.endSelection.Add(time);                
            //}
        }


        private void ScopeControl_MouseMove(object sender, MouseEventArgs e)
        {
            double perc = (double)e.X / (double)this.Width;
            float time = (float)(perc * this.length + this.start);

            bool move = true;

            if (e.Button == MouseButtons.Left && this.LeftMouseFunction)
            {
                if (this.LockedToMaster || Microsoft.Win32.Imports.IsKeyDown(Keys.Control))
                {
                    int loc = e.X + Cursor.Size.Width;
                    tip.Show("Time: " + Math.Round(time, 2).ToString(), this, this.Width / 2, 15);
                    move = false;
                }
            }

            if (!this.LockedToMaster && e.Button == MouseButtons.Left && this.LeftMouseFunction && beginMouseTime > 0)
            {
                //float time = (float)(perc * this.length + this.start);

                if (move)
                {
                    tip.Show("Start: " + Math.Round(this.start, 2).ToString() + " End: " + Math.Round(this.start + this.length, 2).ToString(),
                       this, this.Width / 2, 15);


                    double amount = this.length * perc;
                    if (this.beginMouseTime - amount > 0)
                    {
                        this.Start = this.beginMouseTime - amount;
                        this.Invalidate();
                    }
                }
            }

            //if (tip.Active && e.Button != MouseButtons.Right)
            //{
            //    this.Invalidate();
            //}

            if (this.state == 1)
            {
                double maxPerc = (double)e.Y / (double)this.Height;
                this.tempMax = (1.0 - maxPerc) * (this.max - this.min) + this.min;
                if (this.tempMax <= this.tempMin)
                {
                    this.tempMax = this.tempMin + 0.00000001;
                }
                this.Invalidate();

            }
            else if (this.state == 2)
            {
                double minPerc = (double)e.Y / (double)this.Height;
                this.tempMin =  (1.0 - minPerc) * (this.max - this.min) + this.min;
                if (this.tempMin >= this.tempMax)
                {
                    this.tempMin = this.tempMax - 0.00000001;
                }
                this.Invalidate();
            }

            //if (e.Button != MouseButtons.Right || !Microsoft.Win32.Imports.IsKeyDown(Keys.ControlKey) || this.subControl || !this.RightMouseFunction) return;
            //if (this.beginSelection.Count > this.selectionColors.Length) return;


            //lock (this)
            //{
            //    if (this.beginSelection.Count != 0)
            //    {
            //        float beg = this.beginSelection[this.beginSelection.Count - 1];
            //        if (beg < time) this.endSelection[this.endSelection.Count - 1] = time;
            //        else if (beg > time)
            //        {
            //            this.beginSelection[this.beginSelection.Count - 1] = time;
            //            this.endSelection[this.endSelection.Count - 1] = beg;
            //        }
            //        int loc = e.X + Cursor.Size.Width;
            //        tip.Show("Start Time: " + this.beginSelection[this.beginSelection.Count - 1].ToString() + "\nEnd Time: " +
            //      this.endSelection[this.endSelection.Count - 1].ToString(), this, loc, e.Y);
            //    }
            //}
            //this.Invalidate();
        }

        private void ScopeControl_MouseUp(object sender, MouseEventArgs e)
        {
            tip.Hide(this);

            if (state != 0)
            {
                this.state++;
            }
            if (state > 2)
            {
                
                this.min = this.tempMin;
                this.max = this.tempMax;
                state = 0;
                lock (this)
                {
                    this.bitmap = null;
                }
            }

            this.beginMouseTime = -1;

            this.Invalidate();

            //if (e.Button != MouseButtons.Right || !Microsoft.Win32.Imports.IsKeyDown(Keys.ControlKey) || this.subControl || this.window == null || !this.RightMouseFunction) return;
            //if (this.beginSelection.Count > this.selectionColors.Length) return;
            //if (this.endSelection[this.endSelection.Count - 1] - this.beginSelection[this.beginSelection.Count - 1]
            //    < (this.vars[0].Resolution * 20))
            //{
            //    this.beginSelection.RemoveAt(this.beginSelection.Count - 1);
            //    this.endSelection.RemoveAt(this.endSelection.Count - 1);
            //    return;
            //}
            //this.window.AddScopeControl(new ScopeControl(this.vars, this.window, this, this.selectionColors[this.beginSelection.Count - 1].Color, this.beginSelection[this.beginSelection.Count - 1],
            //    this.endSelection[this.endSelection.Count - 1] - this.beginSelection[this.beginSelection.Count - 1]));
        
        }

        private void ScopeControl_DoubleClick(object sender, EventArgs e)
        {
            lock (this)
            {
                this.max = float.MinValue;
                this.min = float.MaxValue;

                foreach (SpikeVariable var in this.Variables)
                {
                    if (var.Max > this.max)
                    {
                        this.max = var.Max;
                    }
                    if (var.Min < this.min)
                    {
                        this.min = var.Min;
                    }
                }
                this.bitmap = null;
            }
            this.Invalidate();
        }

        private void saveImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog diag = new SaveFileDialog();
            diag.AddExtension = true;
            diag.DefaultExt = ".svg";
            diag.Filter = "Scalable Vector Graphics(*.svg)|*.svg|All Files(*.*)|*.*";
            if (diag.ShowDialog() == DialogResult.Cancel) return;

            SVG svg = new SVG();
            ImageContainer container = new ImageContainer(null, new Rectangle(0, 0, 700, 300));
            svg.AddImageElement(container);
            this.SaveImage(svg, container);            
            svg.Create(new object[] { diag.FileName });

        }

        public void SaveImage(ImageCreator creator, ImageContainer parent)
        {
            Font font = new Font(FontFamily.GenericSerif, 16);
            foreach (SpikeVariable var in this.Variables)
            {
                creator.AddImageElement(new TextElement(parent, font, var.Header.VarName, new Point(5, 5)));
                
                PointF[] points = this.createPoints(var, 0, 0, (int)parent.Size.Width, (int)parent.Size.Height);
                this.drawImage(var, creator, parent, new Rectangle(0, 0, (int)parent.Size.Width, (int)parent.Size.Height), points);
            }
        }

        private void setWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.LockedToMaster)
                return;

            RequestInformation req = new RequestInformation();
            req.Grid.SetInformationToCollect(
                new Type[] { typeof(double), typeof(double) },
                new string[] { "Time", "Time" },
                new string[] { "Start Time", "Length" },
                new string[] { "Start Time", "Length" },
                null, new object[] { 0, 60 });
            if (req.ShowDialog() == DialogResult.Cancel) return;


            double startD = (double)req.Grid.Results["Start Time"];
            double lengthD = (double)req.Grid.Results["Length"];

            this.Start = startD;
            this.Length = lengthD;
        }
    }
}
