namespace ApolloFinal.UI.Scope
{
    partial class Graph3D
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Graph3D));
            this.checkBoxShowBox = new System.Windows.Forms.CheckBox();
            this.openGLControl1 = new CeresBase.IO.Controls.OpenGLControl();
            this.SuspendLayout();
            // 
            // checkBoxShowBox
            // 
            this.checkBoxShowBox.AutoSize = true;
            this.checkBoxShowBox.Location = new System.Drawing.Point(500, 12);
            this.checkBoxShowBox.Name = "checkBoxShowBox";
            this.checkBoxShowBox.Size = new System.Drawing.Size(74, 17);
            this.checkBoxShowBox.TabIndex = 1;
            this.checkBoxShowBox.Text = "Show Box";
            this.checkBoxShowBox.UseVisualStyleBackColor = true;
            // 
            // openGLControl1
            // 
            this.openGLControl1.AccumBits = ((byte)(0));
            this.openGLControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.openGLControl1.AutoCheckErrors = false;
            this.openGLControl1.AutoFinish = true;
            this.openGLControl1.AutoMakeCurrent = true;
            this.openGLControl1.AutoSwapBuffers = true;
            this.openGLControl1.BackColor = System.Drawing.Color.Black;
            this.openGLControl1.BackGroundColor = System.Drawing.Color.White;
            this.openGLControl1.ColorBits = ((byte)(32));
            this.openGLControl1.DepthBits = ((byte)(16));
            this.openGLControl1.FrontClip = 0;
            this.openGLControl1.Location = new System.Drawing.Point(12, 12);
            this.openGLControl1.Name = "openGLControl1";
            this.openGLControl1.ShowBox = false;
            this.openGLControl1.Size = new System.Drawing.Size(481, 316);
            this.openGLControl1.StencilBits = ((byte)(0));
            this.openGLControl1.TabIndex = 0;
            this.openGLControl1.TransformMatrix = ((Sharp3D.Math.Core.Matrix4F)(resources.GetObject("openGLControl1.TransformMatrix")));
            this.openGLControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.openGLControl1_MouseDown);
            this.openGLControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.openGLControl1_MouseMove);
            // 
            // Graph3D
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 340);
            this.Controls.Add(this.checkBoxShowBox);
            this.Controls.Add(this.openGLControl1);
            this.Name = "Graph3D";
            this.Text = "Graph3D";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CeresBase.IO.Controls.OpenGLControl openGLControl1;
        private System.Windows.Forms.CheckBox checkBoxShowBox;
    }
}