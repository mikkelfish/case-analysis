using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ApolloFinal.UI.Scope
{
    public partial class UpdatedScopeControl : UserControl
    {
        private BackgroundWorker bgWorker = new BackgroundWorker();

        int largeImageLength = 15*25000;
        int smallImageLength = 25000;

        private List<SpikeVariable> vars = new List<SpikeVariable>();
        public SpikeVariable[] Variables
        {
            get { return vars.ToArray(); }
        }

        private Dictionary<SpikeVariable, bool[]> createdLargeImages = 
            new Dictionary<SpikeVariable, bool[]>();

        private Dictionary<SpikeVariable, bool[]> createdSmallImages =
            new Dictionary<SpikeVariable, bool[]>();


        public UpdatedScopeControl()
        {
            InitializeComponent();
        }
    }
}
