﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using CeresBase.Projects;
using CeresBase.Data;

namespace ApolloFinal.UI.Scope
{
    /// <summary>
    /// Interaction logic for EpochAddPopup.xaml
    /// </summary>
    public partial class EpochAddPopup : UserControl
    {


        public string EpochName
        {
            get { return (string)GetValue(EpochNameProperty); }
            set { SetValue(EpochNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EpochName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EpochNameProperty =
            DependencyProperty.Register("EpochName", typeof(string), typeof(EpochAddPopup));



        public bool ApplyToAllVariables
        {
            get { return (bool)GetValue(ApplyToAllVariablesProperty); }
            set { SetValue(ApplyToAllVariablesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ApplyToAllVariables.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ApplyToAllVariablesProperty =
            DependencyProperty.Register("ApplyToAllVariables", typeof(bool), typeof(EpochAddPopup));




        public float StartTime
        {
            get { return (float)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StartTime.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StartTimeProperty =
            DependencyProperty.Register("StartTime", typeof(float), typeof(EpochAddPopup));



        public float Length
        {
            get { return (float)GetValue(LengthProperty); }
            set { SetValue(LengthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Length.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LengthProperty =
            DependencyProperty.Register("Length", typeof(float), typeof(EpochAddPopup));


        public Variable Variable { get; set; }


        public bool CheckValidity()
        {
            

            bool isOK = true;

            if (EpochCentral.EpochExists(new Epoch(this.Variable, this.EpochName, (float)this.StartTime, (float)(this.StartTime + this.Length))))
            {
                System.Windows.Controls.ToolTip tooltip = new ToolTip();
                tooltip.Content = "This epoch already exisits. Please change the name or time.";
              
                tooltip.IsOpen = true;

                DispatcherTimer timer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 3), IsEnabled = true };
                timer.Tag = tooltip;
                timer.Tick += new EventHandler(
                    delegate(object timerSender, EventArgs timerArgs)
                    {
                        DispatcherTimer time = timerSender as DispatcherTimer;
                        if (time.Tag != null)
                        {
                            (time.Tag as ToolTip).IsOpen = false;
                        }
                    }
                );

                isOK = false;
            }

            if (this.EpochName == null || this.EpochName == "")
            {
                System.Windows.Controls.ToolTip tooltip = new ToolTip();
                tooltip.Content = "Please enter a name for the epoch.";
                tooltip.PlacementTarget = this.name;
                tooltip.Placement = PlacementMode.Top;
                this.name.ToolTip = tooltip;


                tooltip.IsOpen = true;

                DispatcherTimer timer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 3), IsEnabled = true };
                timer.Tag = tooltip;
                timer.Tick += new EventHandler(
                    delegate(object timerSender, EventArgs timerArgs)
                    {
                        DispatcherTimer time = timerSender as DispatcherTimer;
                        if (time.Tag != null)
                        {
                            (time.Tag as ToolTip).IsOpen = false;
                        }
                    }
                );

                isOK = false;
            }

            if (this.StartTime < 0)
            {
                System.Windows.Controls.ToolTip tooltip = new ToolTip();
                tooltip.Content = "Please enter a positive start time.";
                tooltip.Placement = PlacementMode.Top;
                tooltip.PlacementTarget = this.start;
                this.start.ToolTip = tooltip;

                tooltip.IsOpen = true;

                DispatcherTimer timer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 3), IsEnabled = true };
                timer.Tag = tooltip;
                timer.Tick += new EventHandler(
                    delegate(object timerSender, EventArgs timerArgs)
                    {
                        DispatcherTimer time = timerSender as DispatcherTimer;
                        if (time.Tag != null)
                        {
                            (time.Tag as ToolTip).IsOpen = false;
                        }
                    }
                );
                isOK = false;

            }

            if (this.Length < 0)
            {
                System.Windows.Controls.ToolTip tooltip = new ToolTip();
                tooltip.Content = "Please enter a positive length.";
                this.end.ToolTip = tooltip;
                tooltip.Placement = PlacementMode.Top;
                tooltip.PlacementTarget = this.end;
                tooltip.IsOpen = true;

                DispatcherTimer timer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 3), IsEnabled = true };
                timer.Tag = tooltip;
                timer.Tick += new EventHandler(
                    delegate(object timerSender, EventArgs timerArgs)
                    {
                        DispatcherTimer time = timerSender as DispatcherTimer;
                        if (time.Tag != null)
                        {
                            (time.Tag as ToolTip).IsOpen = false;
                        }
                    }
                );

                isOK = false;

            }


            return isOK;
        }


        public EpochAddPopup()
        {
            InitializeComponent();

            this.IsVisibleChanged += new DependencyPropertyChangedEventHandler(EpochAddPopup_IsVisibleChanged);
        }

        void EpochAddPopup_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.name.Focus();
        }

        void EpochAddPopup_GotFocus(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
