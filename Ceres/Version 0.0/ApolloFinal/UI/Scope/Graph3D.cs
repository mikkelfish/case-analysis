using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CeresBase.IO.Controls;
using Sharp3D.Math.Core;
using Sharp3D.Math.Geometry3D;

namespace ApolloFinal.UI.Scope
{
    public partial class Graph3D : Form
    {
        public Graph3D()
        {
            InitializeComponent();
            this.checkBoxShowBox.DataBindings.Add("Checked", this.openGLControl1, "ShowBox");
        }

        public OpenGLControl OpenGL
        {
            get
            {
                return this.openGLControl1;
            }
        }

        private int lastX = 0;
        private int lastY = 0;

        private void openGLControl1_MouseDown(object sender, MouseEventArgs e)
        {
            this.lastX = e.X;
            this.lastY = e.Y;
        }

        private void openGLControl1_MouseMove(object sender, MouseEventArgs e)
        {        
            if(Graph3D.MouseButtons == MouseButtons.Middle ||
                (Graph3D.MouseButtons == (MouseButtons.Right | MouseButtons.Left)))
            {
                    double scale = (double)Math.Exp((this.lastY - e.Y) / (double)this.openGLControl1.Height);
                    this.openGLControl1.Zoom *= scale;
                    this.lastX = e.X;
                    this.lastY = e.Y;
                    this.openGLControl1.Invalidate();
            }
            else if (Graph3D.MouseButtons == MouseButtons.Right)
            {
                double transx = (this.lastX - e.X) * -4.0 / (double)this.openGLControl1.Width;
                double transy = (this.lastY - e.Y) * 4.0 / (double)this.openGLControl1.Height;

                Matrix4F tempMat = (Matrix4F)this.openGLControl1.TransformMatrix.Clone();
                tempMat[12] += (float)transx;
                tempMat[13] += (float)transy;

                this.openGLControl1.TransformMatrix = tempMat;
                this.openGLControl1.Invalidate();
                this.lastX = e.X;
                this.lastY = e.Y;

            }
            else if (Graph3D.MouseButtons == MouseButtons.Left)
            {
                double angley = (e.X - this.lastX) * (this.openGLControl1.Maxes[0] -
                    this.openGLControl1.Mins[0])/ (this.openGLControl1.Width * 2.0);
                double anglex = (e.Y - this.lastY) * (this.openGLControl1.Maxes[0] -
                    this.openGLControl1.Mins[0]) / (this.openGLControl1.Height * 2.0);
                Matrix4F tempMat = Matrix4F.Multiply(this.openGLControl1.TransformMatrix, 
                    Sharp3D.Math.Geometry3D.Transformation.RotateX(anglex));

                tempMat = Matrix4F.Multiply(tempMat, Transformation.RotateY(angley));
                this.openGLControl1.TransformMatrix = tempMat;
                this.openGLControl1.Invalidate();
                this.lastX = e.X;
                this.lastY = e.Y;
            }
        }


    }
}