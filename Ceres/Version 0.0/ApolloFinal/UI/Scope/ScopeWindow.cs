using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ApolloFinal.Tools;
using CeresBase.Data;
using ApolloFinal.Tools.AttractorReconstruction;
using ApolloFinal.Tools.BatchProcessable;
using ApolloFinal.Tools.PoincareStats;
using CeresBase.UI;
using CeresBase.FlowControl;
using System.Windows.Forms.Integration;
using System.Windows;
using CeresBase.Projects;
using ApolloFinal.Tools.IntervalTools;
using ApolloFinal.Tools.IntervalTools.CycleScoring;
using MesosoftCommon.DataStructures;
using System.Linq;
using ApolloFinal.UI.NewUI;
using ApolloFinal.Tools.MixedTools;
using CeresBase.IO.Serialization;
using DBManager;
using CeresBase.IO.Serialization.PostgreSQL;
using CeresBase.FlowControl.Adapters.Permutations.Output;
using System.Windows.Controls;
using ApolloFinal.IO.EDF;

namespace ApolloFinal.UI.Scope
{
    public partial class ScopeWindow : CeresBase.UI.BaseWindowing
    {
        public ScopeWindow()
        {
            InitializeComponent();


        }

        CeresBase.UI.VariableLoading.VariableLoading loader;

        private void loadVariablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loader == null)
            {
                loader = CeresBase.UI.VariableLoading.VariableLoading.Instance;
                ElementHost.EnableModelessKeyboardInterop(loader);
                loader.Closing += new CancelEventHandler(l_Closing);
                loader.Show();
            }
            else
            {
                loader.Activate();
                loader.Show();
                loader.WindowState = System.Windows.WindowState.Maximized;
            }

            
        }

        void l_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;

            (sender as Window).Dispatcher.BeginInvoke(
                    System.Windows.Threading.DispatcherPriority.Normal,
                        new Action(delegate() { (sender as Window).Hide(); }
                ));
        }

        private void scopeAdded(object sender, EventArgs e)
        {
            (sender as ScopeControl).UseLocalNormalization = this.localNormalizationToolStripMenuItem.Checked;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void autosizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.scopeMaster1.AutoSizeControls = !this.autosizeToolStripMenuItem.Checked;
            this.autosizeToolStripMenuItem.Checked = this.scopeMaster1.AutoSizeControls;
        }

        private void scoreCyclesToolStripMenuItem_Click(object sender, EventArgs e)
        {
          //  Tools.CycleDetection.CycleCreator creator = new Tools.CycleDetection.CycleCreator();
          ////  if (creator.ShowDialog() == DialogResult.Cancel) return;
          //  creator.Show();
            //this.scopeMaster1.BuildVariables();
            
        }

        private void cleanDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.CleanDataTool.CleanData(VariableSource.Variables.ToArray());
        }

     
        private void histogramsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void convertToSpikeSorterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MakeSpikeSortable sortable = new MakeSpikeSortable();
            sortable.ShowDialog();
        }

        private void filterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Tools.TemporaryDataTools.RemoveFrequency(this.scopeMaster1.ScopeDisplays[0].Variables, 60);
        }

        private void spikeTriggToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.SpikeTriggeredAverage.SpikeTriggeredTool avg = new ApolloFinal.Tools.SpikeTriggeredAverage.SpikeTriggeredTool();
            avg.Show();
        }

        private void localNormalizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.localNormalizationToolStripMenuItem.Checked = !this.localNormalizationToolStripMenuItem.Checked;
            foreach (ScopeControl disp in this.scopeMaster1.ScopeDisplays)
            {
                disp.UseLocalNormalization = this.localNormalizationToolStripMenuItem.Checked;
            }
        }

        //private void autoRunToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    AutoRun run = new AutoRun();
        //    run.Show();
        //}



        private void saveScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog diag = new SaveFileDialog();
            diag.AddExtension = true;
            diag.DefaultExt = ".svg";
            diag.Filter = "Scalable Vector Graphics(*.svg)|*.svg|All Files(*.*)|*.*";
            if (diag.ShowDialog() == DialogResult.Cancel) return;

            this.scopeMaster1.SaveScreen(diag.FileName);
        }

      

      

        //private void sighProjectToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    SighProject proj = new SighProject();
        //    proj.Show();
        //}

      
        
        private void scopeMaster1_Load(object sender, EventArgs e)
        {

            NewSerializationCentral.CurrentUser = GroupUserCentral.Users.SingleOrDefault(u => u.Name == "postgres");


            //DBProxy.DBProxySingleton.Instance.Connect(user);
            //DBProxy.DBProxySingleton.Instance.UpdateEvent += new EventHandler<DBProxy.DBEventArgs>(Instance_UpdateEvent);


            //if (!NewSerializationCentral.AvailableDatabases.Any(db => db.Name == "localdb"))
            //{
            //    NewSerializationCentral.AddNewDatabase("localdb", "Default Database");
            //}

            //NewSerializationCentral.ChangeDatabase("localdb");
        }

        void Instance_UpdateEvent(object sender, DBProxy.DBEventArgs e)
        {
            if (e.ClientSource == DBProxy.DBProxySingleton.Instance.ID)
                return;

            string text = "";
            switch (e.Action)
            {
                case DBProxy.DBAction.DBAdded:

                    return;

                case DBProxy.DBAction.DBRemoved:

                    return;

                case DBProxy.DBAction.UserAdded:
                    text = "A User Has Been Added Added";
                    break;

                case DBProxy.DBAction.DBChanged:
                    if (e.Args != NewSerializationCentral.CurrentDatabase.DatabaseName)
                        return;

                    text = "Your database has been updated";
                    break;

                default:
                    return;
            }

            if(this.Created)
            this.BeginInvoke(new Action<string>(enableRefresh), text);          
        }

        private void enableRefresh(string val)
        {
            this.refreshDatabaseToolStripMenuItem.Enabled = true;
            this.refreshDatabaseToolStripMenuItem.ToolTipText = val;            
        }

        private void routineBatchThingyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Window win = new System.Windows.Window();
            win.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            RoutineBatchControl rbc = new RoutineBatchControl();
            rbc.Controller = new RawRoutineBatchControl(rbc);
            
            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(win);
            win.Content = rbc;
            win.Height = rbc.Height + 32;
            win.Width = rbc.Width + 8;
            rbc.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            rbc.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            win.Title = "Routines for Raw Data";
            win.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Windows.Window win = new System.Windows.Window();
            win.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            RoutineBatchControl rbc = new RoutineBatchControl();
            rbc.Controller = new IntervalRoutineController(rbc);

            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(win);
            win.Content = rbc;
            win.Height = rbc.Height + 32;
            win.Width = rbc.Width + 8;
            rbc.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            rbc.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            win.Title = "Routines for Interval Data";
            win.Show();
        }

        private void mixedRoutinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Window win = new System.Windows.Window();
            win.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            RoutineBatchControl rbc = new RoutineBatchControl();
            rbc.Controller = new MixedRoutineController(rbc);

            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(win);
            win.Content = rbc;
            win.Height = rbc.Height + 32;
            win.Width = rbc.Width + 8;
            rbc.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            rbc.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            win.Title = "Routines for Mixed Data";
            win.Show();
        }

        private void newIntervalScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CycleScoringWindow wind = new CycleScoringWindow();
            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(wind);

            BeginInvokeObservableCollection<Epoch> epochList = EpochCentral.BindToManager(wind.Dispatcher);
            Epoch es = epochList.Where(ep => (ep.Variable as SpikeVariable).SpikeType != SpikeVariableHeader.SpikeType.Pulse ).OrderBy(ep => ep.Variable.Header.Description).ThenBy(ep=>ep.BeginTime).FirstOrDefault(ep => ep.IsUser);
            if (es != null) wind.CurrentEpoch = es;

            wind.Show();
           // wind.WindowState = System.Windows.WindowState.Maximized;

        }

        private void serializationTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
         //   CeresBase.IO.Serialization.PostgreSQL.PostgreSQLDBEngine.Test();            
        }

        private void epochDeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EpochLoadedDelete to = new EpochLoadedDelete();
            to.Show();
        }

        private void epochMergeToolStripMenuItem_Click(object sender, EventArgs e)
        {
          //  EpochCentral.MergeEpochs();
        }

        private void showFullRecordsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.showFullRecordsToolStripMenuItem.Checked = !this.showFullRecordsToolStripMenuItem.Checked;
            this.scopeMaster1.ShowAllOfRecord = this.showFullRecordsToolStripMenuItem.Checked;
        }

        private void dBTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CeresBase.IO.Serialization.PostgreSQL.PostgreSQLDBEngine.Test();
            //GroupUserCentral.IansTest();
            
        }

        private void mergeEpochsToolStripMenuItem_Click(object sender, EventArgs e)
        {
          //  EpochCentral.MergeEpochs();

        }

        private void submitBugReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msg = "Username: tester Password: tester\nRemember to put your name and email address when reporting an issue.";

            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                msg += "\nState that the issue occurs in version " + System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }

            System.Diagnostics.Process.Start("http://mikkelsdomain.case.edu:8080");
            System.Windows.MessageBox.Show(msg);

        }

        private void newUIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApolloMainWindow win1 = new ApolloMainWindow();
        }

        private void histogramsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Tools.Histograms.CreateHistograms create = new ApolloFinal.Tools.Histograms.CreateHistograms();
            create.Show();
        }

        private void loadOldDataIntoDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CeresBase.IO.Serialization.LegacyMerge.MergeOldIntoDatabase();
        }

        private void switchDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseSwitch switchDb = new DatabaseSwitch();
            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(switchDb);

            switchDb.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
         
        }

        private void aboutToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            string msg = "Created at Case Western Reserve University, School of Medicine" + "\n" + "Funding provided by $GRANTS$";
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                msg += "\nVersion " + System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }

            System.Windows.MessageBox.Show(msg);
        }

        private void epochViewerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EpochLoadedDelete to = new EpochLoadedDelete();
            to.Show();
        }

        private void epochMergerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void addDbToolStripMenuItem_Click(object sender, EventArgs e)
        {
              
                // DBProxy.DBProxySingleton.Instance.AddDatabase("wikiwikiwa");
                DBProxy.DBProxySingleton.Instance.AddUser(new User() { Name = "Wazza" });
                //string[] what = DBProxy.DBProxySingleton.Instance.GetDBs();

                //object s = ConfigurationManager.GetSection("system.serviceModel");

                //ServiceReference1.DBManagerServiceClient client = new CeresBase.ServiceReference1.DBManagerServiceClient();
                //client.AddDatabase("Testing");
                //client.AddUser("fwop", "se");
                //client.AddDatabase("whee");
                //bool what = client.AddGroup("", "");

                //string[] dbsg = client.GetDatabases("g", "fix");

        }

        private void refreshDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewSerializationCentral.ReloadDatabase();

            this.refreshDatabaseToolStripMenuItem.Enabled = false;
        }

        private void switchUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (NewSerializationCentral.CurrentUser.SuperUser)
                NewSerializationCentral.CurrentUser = new User() { SuperUser = false };
            else NewSerializationCentral.CurrentUser = new User() { SuperUser = true };
        }

        private void permissionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserAndGroupAdmin ad = new UserAndGroupAdmin();
            ad.Show();
        }

        private void changeUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeUser user = new ChangeUser();
            user.Show();
        }

        private void mergeEpochsIntoListToolStripMenuItem_Click(object sender, EventArgs e)
        {
           // NewSerializationCentral.MergeOldDBIntoList("IList:CeresBase.Projects.EpochTranslator", "CeresBase.Projects.EpochTranslator");
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //PickOneControl r = new PickOneControl();
            //r.ControlsToView.Add(new System.Windows.Controls.Button() { Content="Blahblah" });
            //r.ControlsToView.Add(new System.Windows.Controls.Button() { Content = "Blahblah2" });
            //r.ControlsToView.Add(new System.Windows.Controls.Button() { Content = "Blahblah3" });
            //r.Show();
        }

 

    }
}
