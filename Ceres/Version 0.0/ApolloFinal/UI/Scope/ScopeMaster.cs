using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CeresBase.Data;
using CeresBase.UI;
using ThreadSafeConstructs;
using CeresBase.IO.ImageCreation;
using ApolloFinal.Tools;
using CeresBase.IO;
using System.Windows.Forms.Integration;
using CeresBase.IO.Controls;
using CeresBase.IO.Serialization;

namespace ApolloFinal.UI.Scope
{
    public partial class ScopeMaster : UserControl
    {
        private double longest = double.MinValue;
        private double lowestres = double.MaxValue;
        private double length = 60000;
        private Timer timer = new Timer();

        private bool autoSizeControls = false;
        public bool AutoSizeControls
        {
            get
            {
                return this.autoSizeControls;
            }
            set
            {
                this.autoSizeControls = value;
                this.resizeControls();
            }
        }

        private bool showAllOfRecord;
        public bool ShowAllOfRecord 
        {
            get
            {
                return this.showAllOfRecord;
            }
            set
            {
                this.showAllOfRecord = value;
                if (this.showAllOfRecord)
                {
                    this.hScrollBar1.Enabled = false;

                    this.textBoxBeginningTime.Text = "0";
                    this.textBoxBeginningTime.Enabled = false;

                    this.textBox_length.Enabled = false;

                    foreach (ScopeControl control in this.flowControl_displays.Controls)
                    {
                        if (!control.LockedToMaster) continue;

                        control.Start = 0;
                        control.Length = control.Variables[0].TotalLength;
                    }
                }
                else
                {
                    this.hScrollBar1.Enabled = true;
                    this.textBoxBeginningTime.Enabled = true;

                    this.textBox_length.Enabled = true;

                    this.changeLength();
                }

            }
        }

        public ThreadSafeEvent<EventArgs> ScopeAdded = new ThreadSafeEvent<EventArgs>();
        public ThreadSafeEvent<EventArgs> ScopeDeleted = new ThreadSafeEvent<EventArgs>();

        public ScopeControl[] ScopeDisplays
        {
            get
            {
                List<ScopeControl> list = new List<ScopeControl>();
                foreach (ScopeControl cont in this.flowControl_displays.Controls)
                {
                    list.Add(cont);
                }
                return list.ToArray();
            }
        }

        private int normHeight = 0;

        private void ScopeMaster_Load(object sender, EventArgs e)
        {
        //    Binding binding = new Binding("Tag", VariableSource.Variables, null);
        //    binding.BindingComplete += new BindingCompleteEventHandler(binding_BindingComplete);

        //    this.listView1.DataBindings.Add(binding);

            

        //    binding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
        //    binding.BindingManagerBase.CurrentItemChanged += new EventHandler(BindingManagerBase_CurrentChanged);

            try
            {
                NewSerializationCentral.DatabaseChanged += new EventHandler(SerializationCentral_DatabaseChanged);
            }
            catch(Exception ex)
            {
                MessageBox.Show("There has been an error starting the database. Please restart it (if you know how) and try again or contact someone that can help. \nError: " + ex.InnerException.Message + " trace: " + ex.InnerException.StackTrace);

                if (System.Windows.Application.Current == null)
                {
                    System.Windows.Forms.Application.Exit();
                    System.Windows.Forms.Application.ExitThread();
                }
                else
                {
                    System.Windows.Application.Current.Shutdown();
                }

                return;
            }




                VariableSource.Variables.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Variables_CollectionChanged);
 
            this.updateList();

            this.elementHost1.Child = new EpochNavigator();
            (this.elementHost1.Child as EpochNavigator).Master = this;

        }

        void SerializationCentral_DatabaseChanged(object sender, EventArgs e)
        {
            ScopeControl[] controls = this.ScopeDisplays;
            foreach (ScopeControl control in controls)
            {
                this.RemoveScopeControl(control);
            }
        }

        void Variables_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            try
            {
                

                this.Invoke(new CeresBase.General.Utilities.EmptyHandler(this.updateList));
                //this.updateList();

            }
            catch (Exception f)
            {
                //CeresBase.Global.ErrorBox(f); 
            }
        }

      

        private void updateList()
        {
            this.listView1.BeginUpdate();
            this.listView1.Items.Clear();
            foreach (SpikeVariable var in VariableSource.Variables)
            {
                ListViewItem item = new ListViewItem(var.FullDescription);
                item.Tag = var;
                this.listView1.Items.Add(item);

                // Add experiment to HotSpotSelect
                try
                {
                    

                    //if (var.HotSpots.Count == 0)
                    //{
                    //    this.HSSelect.LoadExperiment(var.Header.ExperimentName);
                    //}
                    //else
                    //{
                    //    this.HSSelect.LoadHotSpots(var.HotSpots.ToArray());
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.StackTrace);
                }

                //HSSelect.(var.Header.ExperimentName.ToString());
                //foreach (HotSpot spot in var.HotSpots)
                //{
                //    if (!HSSelect.Contains(var.Header.ExperimentName, "", spot.Name, spot.BeginTime, spot.EndTime))
                //    {
                //        this.HSSelect.Add(var.Header.ExperimentName, "", spot.Name, spot.BeginTime.ToString(),
                //            spot.EndTime.ToString(), spot.Description);
                //    }
                //}
            }
            this.listView1.EndUpdate();

            
        }



        public void BuildVariables()
        {
            //try
            //{
            //    this.listView1.Items.Clear();
            //    foreach (SpikeVariable var in VariableSource.Variables)
            //    {
            //        ListViewItem item = new ListViewItem(var.FullDescription);
            //        item.Tag = var;
            //        this.listView1.Items.Add(item);

            //        // Add experiment to HotSpotSelect
            //        HSSelect.AddExperiment(var.Header.ExperimentName.ToString());
            //        foreach (HotSpot spot in var.HotSpots)
            //        {
            //            if (!HSSelect.Contains(var.Header.ExperimentName, "", spot.Name, spot.BeginTime, spot.EndTime))
            //            {
            //                this.HSSelect.Add(var.Header.ExperimentName, "", spot.Name, spot.BeginTime.ToString(),
            //                    spot.EndTime.ToString(), spot.Description);
            //            }
            //        }
            //    }
            //    this.HSSelect.Refresh();
            //}
            //catch (Exception f) { CeresBase.Global.ErrorBox(f); }
        }

        public ScopeMaster()
        {
            InitializeComponent();

            this.BuildVariables();

            this.timer.Tick += new EventHandler(timer_Tick);

            this.textBox_length.Text = Math.Round(this.length/1000.0,4).ToString();

            this.Resize += new EventHandler(flowControl_displays_Resize);

            //this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printPage);
            //this.printDialog1.Document = this.printDocument1;
        }

        private void resetLengths()
        {
            this.longest = 0;
            this.lowestres = double.MaxValue;
            foreach (ScopeControl control in this.flowControl_displays.Controls)
            {
                if (control.SubControl) continue;

                foreach (SpikeVariable var in control.Variables)
                {
                    if (var.TotalLength > longest)
                    {
                        this.longest = var.TotalLength*1000.0;
                    }

                    if (var.Resolution < this.lowestres)
                    {
                        this.lowestres = var.Resolution;
                    }
                }
            }
            this.setBarRanges();
        }

        private void resizeControls()
        {
            int height=0;
            if (this.flowControl_displays.Controls.Count == 0) return;
            if (this.autoSizeControls)
            {
                height = this.flowControl_displays.Height / this.flowControl_displays.Controls.Count;
                height -= 6;
                this.flowControl_displays.AutoScroll = false;
            }
            else
            {
                this.flowControl_displays.AutoScroll = true;
                height = this.normHeight;
            }

            foreach (ScopeControl control in this.flowControl_displays.Controls)
            {
                control.Height = height;
                control.Width = this.flowControl_displays.Width - control.Padding.Right - this.flowControl_displays.Padding.Right - 10;
                if (this.flowControl_displays.VerticalScroll.Visible) control.Width -= 15;

                control.Invalidate();
            }
        }

        private void butt_Click(object sender, EventArgs e)
        {
            ToggleButton butt = sender as ToggleButton;
            if (butt.Toggled)
            {
                this.AddScopeControl(butt.Tag as ScopeControl);
            }
            else
            {
                this.RemoveScopeControl(butt.Tag as ScopeControl);
            }
        }

        public void RemoveScopeControl(ScopeControl control)
        {
            this.flowControl_displays.SuspendLayout();

            this.flowControl_displays.Controls.Remove(control);
            this.resetLengths();
            this.resizeControls();
            this.ScopeDeleted.CallEvent(control, EventArgs.Empty);

            this.flowControl_displays.ResumeLayout();
        }

        public void AddScopeControl(ScopeControl control)
        {
            if (normHeight == 0) normHeight = control.Height;

            this.flowControl_displays.SuspendLayout();

            
            this.flowControl_displays.Controls.Add(control);
            this.resetLengths();

            if (this.length > 0)
            {
                control.Length = this.length/1000.0;
                control.Start = this.hScrollBar1.Value/1000.0;
            }


            this.resizeControls();

            this.ScopeAdded.CallEvent(control, EventArgs.Empty);
            control.AllowDrop = true;
            control.DragOver += new DragEventHandler(control_DragOver);
            control.DragDrop += new DragEventHandler(control_DragDrop);

            this.flowControl_displays.ResumeLayout();

        }

        void control_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(SpikeVariable)))
            {
                SpikeVariable var = (SpikeVariable)e.Data.GetData(typeof(SpikeVariable));
                ScopeControl cont = sender as ScopeControl;
                cont.AddVariable(var);
                cont.Invalidate();
            }
        }

        void control_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(SpikeVariable)))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

       

        private void flowControl_displays_Resize(object sender, EventArgs e)
        {
            this.resizeControls();
        }

        private void changeLength()
        {
            double len = double.Parse(this.textBox_length.Text);
            this.length = len*1000.0;

            if (this.IsHandleCreated)
            {
                this.Invoke(new CeresBase.General.Utilities.EmptyHandler(this.setNewControlLengths));
                this.setBarRanges();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.changeLength();

        }

        private void setBarRanges()
        {         
            this.hScrollBar1.Minimum = 0;
            int largeChange = (int)(this.length / 5.0);
            
//            this.hScrollBar1.LargeChange = (int)(this.hScrollBar1.Maximum * .01);
            this.hScrollBar1.Maximum = (int)((this.longest - this.length) + largeChange-1);
            if (this.hScrollBar1.Maximum < this.hScrollBar1.Minimum)
            {
                this.hScrollBar1.Maximum = this.hScrollBar1.Minimum;
            }

            this.hScrollBar1.LargeChange = largeChange;
            this.hScrollBar1.SmallChange = this.hScrollBar1.LargeChange / 5;

            this.textBoxBeginningTime.Text = Math.Round(this.hScrollBar1.Value/1000.0,4).ToString();
            this.labeltime_End.Text = Math.Round((this.hScrollBar1.Value + this.length)/1000.0, 4).ToString(); 
        }

        private void setNewControlLengths()
        {
            foreach (ScopeControl control in this.flowControl_displays.Controls)
            {
                if (!control.LockedToMaster) continue;

                control.Length = this.length/1000.0;

                if (control.Start != this.hScrollBar1.Value / 1000.0)
                    control.Start = this.hScrollBar1.Value / 1000.0;
            }
        }

        private void setNewControlValue()
        {
            foreach (ScopeControl control in this.flowControl_displays.Controls)
            {
                if (!control.LockedToMaster) continue;

                control.Start = this.hScrollBar1.Value/1000.0;

                if (control.Length != this.length / 1000.0)
                    control.Length = this.length / 1000.0;
            }
        }

        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            base.OnInvalidated(e);
            foreach (ScopeControl control in this.flowControl_displays.Controls)
            {
                control.Invalidate();
            }

        }

        #region Scroll callbacks

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            double divide = this.length / 4;
            if (Microsoft.Win32.Imports.IsKeyDown(Keys.ControlKey))
            {
                divide = this.length / 2;
            }
            else if (Microsoft.Win32.Imports.IsKeyDown(Keys.ShiftKey))
            {
                divide = this.length;
            }

            if (divide < 1) divide = 1;

            if (e.Type == ScrollEventType.LargeIncrement)
            {
                if (e.OldValue + divide < this.hScrollBar1.Maximum)
                {
                    this.hScrollBar1.Value = (int)(e.OldValue + divide);
                    e.NewValue = this.hScrollBar1.Value;
                }
                else this.hScrollBar1.Value = this.hScrollBar1.Maximum;
            }
            else if (e.Type == ScrollEventType.LargeDecrement)
            {
                if (e.OldValue - divide > this.hScrollBar1.Minimum)
                {
                    this.hScrollBar1.Value = (int)(e.OldValue - divide);
                    e.NewValue = this.hScrollBar1.Value;
                }
                else this.hScrollBar1.Value = 0;
            }
            

            this.textBoxBeginningTime.Text = Math.Round(this.hScrollBar1.Value/1000.0,4).ToString();
            this.labeltime_End.Text = Math.Round((this.hScrollBar1.Value + this.length)/1000.0, 4).ToString(); 
            
            
            if (e.Type != ScrollEventType.EndScroll || e.Type == ScrollEventType.ThumbPosition) return;
            if (timer.Enabled) timer.Stop();

            this.Invoke(new CeresBase.General.Utilities.EmptyHandler(this.setNewControlValue));
        }

        void timer_Tick(object sender, EventArgs e)
        {
            foreach (ScopeControl control in this.flowControl_displays.Controls)
            {
                if (!control.LockedToMaster) continue;
               if(control.Start != this.hScrollBar1.Value/1000.0) control.Start = this.hScrollBar1.Value/1000.0;
            }
            this.timer.Stop();
        }


        private void hScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            if (timer.Enabled) timer.Stop();
            timer.Interval = 100;
            timer.Start();
        }

        #endregion

        //private void printToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    if (this.printDialog1.ShowDialog() == DialogResult.Cancel) return;

        //    this.curPrintPage = 0;
        //    this.numControlsPrinted = 0;
        //    this.printDocument1.Print();
            
        //}



        private void textBox_length_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.changeLength();
            }           
        }

        public void ChangeStartAndLength(double start, double length)
        {
            double time = start * 1000.0;
            if (time >= 0 && time < this.hScrollBar1.Maximum - this.length)
            {
                this.hScrollBar1.Value = (int)(time);
                this.textBoxBeginningTime.Text = Math.Round(this.hScrollBar1.Value / 1000.0, 4).ToString();                
                this.labeltime_End.Text = Math.Round((this.hScrollBar1.Value + this.length) / 1000.0, 4).ToString();
                this.setNewControlValue();
            }

            if (length > 0)
            {
                this.length = length * 1000.0;
                this.Invoke(new CeresBase.General.Utilities.EmptyHandler(this.setNewControlLengths));
                this.setBarRanges();
            }
        }

        private void textBoxBeginningTime_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
            {
                return;
            }

            try
            {
                double time = double.Parse(this.textBoxBeginningTime.Text)*1000.0;
                if (time >= 0 && time < this.hScrollBar1.Maximum - this.length)
                {
                    this.hScrollBar1.Value = (int)(time);
                    this.labeltime_End.Text = Math.Round((this.hScrollBar1.Value + this.length)/1000.0, 4).ToString();
                    this.setNewControlValue();
                }
            }
            catch
            {
                MessageBox.Show("The entered text was not a valid number. Please try again.");
            }
        }

        private void addControlToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.AddScopeControl(new ScopeControl(this));
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if (sender != this.listView1) return;
            ScopeControl control = new ScopeControl(this);
            if (this.listView1.SelectedItems.Count == 0) return;
            control.AddVariable(this.listView1.SelectedItems[0].Tag as SpikeVariable);
            this.AddScopeControl(control);
        }

        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.listView1.SelectedItems.Count == 0) return;
            //if(e.Button == MouseButtons.Right)
            //    this.listView1.DoDragDrop(this.listView1.SelectedItems[0].Tag, DragDropEffects.Copy);
        }


        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void integrateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //RequestInformation req = new RequestInformation();
            //req.Grid.SetInformationToCollect(new Type[] { typeof(int), typeof(double) }, new string[] {"Integrate", "Integrate" },
            //    new string[] { "Output Freq", "Time Constant" }, new string[] { "Output frequency.", "Enter the time constant" },
            //    null, null);
            //if (req.ShowDialog() == DialogResult.Cancel) return;
            //int freq = (int)req.Grid.Results["Output Freq"];
            //double timeConstant = (double)req.Grid.Results["Time Constant"];
            //List<SpikeVariable> vars = new List<SpikeVariable>();
            //foreach(ListViewItem varItem in this.listView1.SelectedItems)
            //{
            //    vars.Add(varItem.Tag as SpikeVariable);
            //}
            //SpikeVariable[] integrated = Tools.TemporaryDataTools.IntegrateVariables(vars.ToArray(), freq, timeConstant);
            //VariableSource.AddVariables(integrated);
            //this.BuildVariables();

        }

        private void filterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            //List<SpikeVariable> vars = new List<SpikeVariable>();
            //foreach (ListViewItem varItem in this.listView1.SelectedItems)
            //{
            //    vars.Add(varItem.Tag as SpikeVariable);
            //}

            //ButterFilter filter = new ButterFilter();
            //string[] names;
            //object[] args = filter.GetArguments(vars.ToArray(), out names);

            //if (args == null) return;

            //SpikeVariable[] filtered = (SpikeVariable[])filter.Process(vars.ToArray(), args);
            //VariableSource.AddVariables(filtered);
            //this.BuildVariables();
        }

        private void simpleMovingAverageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //RequestInformation req = new RequestInformation();
            //req.Grid.SetInformationToCollect(new Type[] { typeof(double) },
            //    new string[] { "SMA"},
            //    new string[] { "Time"},
            //    new string[] { "The time window to average across." },
            //    null, null);
            //if (req.ShowDialog() == DialogResult.Cancel) return;
            //double time = (double)req.Grid.Results["Time"];
            
            //foreach (ListViewItem varItem in this.listView1.SelectedItems)
            //{
            //    SpikeVariable var = Tools.TemporaryDataTools.SimpleMovingAverage(varItem.Tag as SpikeVariable, time);
            //    if (var != null) VariableSource.AddVariable(var);
            //    else MessageBox.Show("Couldn't create moving average of " + (varItem.Tag as SpikeVariable).FullDescription + " only integrated variables may be used.");
            //}
            //this.BuildVariables();                
        }

        public void SaveScreen(string path)
        {            
            SVG svg = new SVG();
            int y = 0;
            int height = 300;
            int width =  700;
            int spacing = 10;

            Font font = new Font(FontFamily.GenericSerif, 16);
            ImageContainer labelContainer = new ImageContainer(null, new Rectangle(0, y, width, 25));

            TextElement startEnd = new TextElement(labelContainer, font, "Start Time: " + this.textBoxBeginningTime.Text + " End Time: " + this.labeltime_End.Text,
                new PointF(5, 0));
            svg.AddImageElement(startEnd);
            y += 25;

            foreach (ScopeControl cont in this.flowControl_displays.Controls)
            {
                ImageContainer container = new ImageContainer(null, new Rectangle(0, y, width, height));
                y+= height + spacing;
                cont.SaveImage(svg, container);
            }
            svg.Create(new object[] { path });
        }

        private void thresholdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ThresholdCutoff cutoff = new ThresholdCutoff();
            if (cutoff.ShowDialog() == DialogResult.Cancel) return;
            this.BuildVariables();
        }

        private void truncateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Truncator trunc = new Truncator();
            trunc.Master = this;
            trunc.Show();
        }

        private void getMaxAndMinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string toDisp = string.Empty;
            foreach (ScopeControl control in this.ScopeDisplays)
            {
                SpikeVariable var = control.Variables[0];

                if (var.SpikeType == SpikeVariableHeader.SpikeType.Pulse) continue;



                float min = float.MaxValue;
                float max = float.MinValue;
                if (control.UseLocalNormalization)
                {
                    float[] data = var.GetTimeSeriesData(control.Start, control.Start + control.Length);

                    //int index = (int)(control.Start / var.Resolution);
                    //int count = (int)(control.Length / var.Resolution);
                    //var[0].Pool.GetData(new int[] { index }, new int[] { count },
                    //    delegate(float[] data, uint[] indices, uint[] counts)
                    //    {
                            for (int i = 0; i < data.Length; i++)
                            {
                                if (data[i] < min) min = data[i];
                                if (data[i] > max) max = data[i];
                            }
                    //    }
                    //);
                }
                else
                {
                    min = (float)control.Min;
                    max = (float)control.Max;
                }

                toDisp += var.FullDescription + " " + max.ToString() + "; " + min.ToString() + "\n";
            }
            MessageBox.Show(toDisp);
        }

        private void makePlotToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    PoincarePlot plot = new PoincarePlot();
        //    SpikeVariable var = this.listView1.SelectedItems[0].Tag as SpikeVariable;
        //    List<float> vals = new List<float>();
        //    var[0].Pool.GetData(null, null,
        //        delegate(float[] data, uint[] count, uint[] indices)
        //        {
        //            vals.AddRange(data);
        //        }
        //    );

        //    float min = float.MaxValue;
        //    for (int i = 0; i < vals.Count; i++)
        //    {
        //        if (vals[i] < min)
        //            min = vals[i];
        //    }

        //    for (int i = 0; i < vals.Count; i++)
        //    {
        //        vals[i] += Math.Abs(min) + Math.Abs(min) * .1F;
        //    }

        //    plot.Times = vals.ToArray();
        //    plot.RawTimes = new float[vals.Count];
        //    for (int i = 0; i < plot.RawTimes.Length; i++)
        //    {
        //        plot.RawTimes[i] = i;
        //    }

        //    //plot.RawTimes = vals.ToArray();
        //    plot.Show();
        }

        private void tEMPToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    SpikeVariable var = this.listView1.SelectedItems[0].Tag as SpikeVariable;
        //    List<float> vals = new List<float>();
        //    var[0].Pool.GetData(null, null,
        //        delegate(float[] data, uint[] count, uint[] indices)
        //        {
        //            vals.AddRange(data);
        //        }
        //    );

        //    float[] diffs = new float[vals.Count - 1];
        //    for (int i = 0; i < vals.Count - 1; i++)
        //    {
        //        diffs[i] = vals[i] - vals[i + 1];
        //    }

        //    int[] by = new int[] { 10, 100, 1000 };
            
            
        //    double[] runningmean = new double[by.Length];
        //    double[] runningsd = new double[by.Length];
        //    List<double>[] sds = new List<double>[by.Length];

        //    double[] runningmeanRaw = new double[by.Length];
        //    double[] runningsdRaw = new double[by.Length];
        //    List<double>[] sdsRaw = new List<double>[by.Length];

        //    for (int i = 0; i < by.Length; i++)
        //    {
        //        sds[i] = new List<double>();
        //        sdsRaw[i] = new List<double>();

        //        for (int j = 0; j < diffs.Length; j++)
        //        {
        //            if ((j) % by[i] == 0 && j != 0)
        //            {
        //                runningmean[i] /= by[i];
        //                runningmeanRaw[i] /= by[i];

        //                for (int k = j - by[i]; k < j; k++)
        //                {
        //                    runningsd[i] += (diffs[k] - runningmean[i]) * (diffs[k] - runningmean[i]);
        //                    runningsdRaw[i] += (vals[k] - runningmeanRaw[i]) * (vals[k] - runningmeanRaw[i]);

        //                }

        //                runningsd[i] /= (by[i] - 1);
        //                runningsd[i] = Math.Sqrt(runningsd[i]);
        //                runningsd[i] = Math.Sqrt(0.5 * runningsd[i] * runningsd[i]);
        //                sds[i].Add(runningsd[i]);


        //                runningsdRaw[i] /= (by[i] - 1);
        //                runningsdRaw[i] = Math.Sqrt(runningsdRaw[i]);
        //                sdsRaw[i].Add(runningsdRaw[i]);

        //                runningsd[i] = 0;
        //                runningmean[i] = 0;
        //            }

        //            runningmean[i] += diffs[j];
        //            runningmeanRaw[i] += vals[j];
        //        }
        //    }

        //    List<object[]> toWrite = new List<object[]>();

        //    for (int i = 0; i < sds[0].Count; i++)
        //    {
        //        object[] add = new object[by.Length*2];
        //        for (int j = 0; j < by.Length; j++)
        //        {
        //            if (i < sds[j].Count)
        //            {
        //                add[j*2] = sds[j][i];
        //                add[j * 2 + 1] = sdsRaw[j][i];

        //            }
        //        }
        //        toWrite.Add(add);
        //    }

        //    SaveFileDialog diag = new SaveFileDialog();
        //    if (diag.ShowDialog() == DialogResult.Cancel) return;
        //    CSVWriter.WriteCSV(diag.FileName, toWrite.ToArray());


        }

        

        private void buttonManage_Click(object sender, EventArgs e)
        {

            //ApolloFinal.Tools.HotSpots.HotSpotSelect.LaunchManagement(this.HSSelect);
        }

        private void exportVariablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select one or more variables to export.");
                return;
            }

            Exporter export = new Exporter();
            if (export.ShowDialog() == DialogResult.Cancel) return;

            List<Variable> vars = new List<Variable>();
            foreach (object o in this.listView1.SelectedItems)
            {
                vars.Add((o as ListViewItem).Tag as Variable);
            }

            if (export.Writer != null)
            {
                export.Writer.WriteVariables(vars.ToArray(), export.FilePath);
            }
        }

    }
}