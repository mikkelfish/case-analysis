﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CeresBase.Projects;
using MesosoftCommon.DataStructures;
using System.ComponentModel;
using System.Windows.Controls.Primitives;
using CeresBase.FlowControl;
using Deepforest.WPF.Controls;
using CeresBase.UI;
using CeresBase.Data;

namespace ApolloFinal.UI.Scope
{
    /// <summary>
    /// Interaction logic for EpochNavigator.xaml
    /// </summary>
    public partial class EpochNavigator : UserControl
    {
        public bool ConstrainToWidth { get; set; }

        private bool sortSet = false;
        private float lastLength = 30.0F;

        private List<ScopeControl> controls = new List<ScopeControl>();

        public bool FilterEpochsNotOnScreen
        {
            get { return (bool)GetValue(FilterEpochsNotOnScreenProperty); }
            set { SetValue(FilterEpochsNotOnScreenProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FilterEpochsNotOnScreen.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FilterEpochsNotOnScreenProperty =
            DependencyProperty.Register("FilterEpochsNotOnScreen", typeof(bool), typeof(EpochNavigator), new PropertyMetadata(filterChanged));

        private static void filterChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            EpochNavigator navigator = obj as EpochNavigator;
            if (navigator.epochList.ItemsSource != null)
            {
                CollectionView source = navigator.epochList.ItemsSource as CollectionView;
                source.Refresh();

              

                navigator.epochList.ContextMenu.IsOpen = false;

            }
        }

        public BeginInvokeObservableCollection<Epoch> Epochs 
        {
            get
            {
                return EpochCentral.BindToManager(this.Dispatcher);
            }
        }

        public bool IsAdding { get; set; }

        private ScopeMaster master;
        public ScopeMaster Master 
        {
            get
            {

                return this.master;
            }
            set
            {
                if (this.master != null)
                {
                    this.master.ScopeAdded -= new ThreadSafeConstructs.ThreadSafeEventHandler<EventArgs>(this.scopeAdded);
                    this.master.ScopeDeleted -= new ThreadSafeConstructs.ThreadSafeEventHandler<EventArgs>(this.scopeRemoved);                    
                }

                this.master = value;
                this.master.ScopeAdded += new ThreadSafeConstructs.ThreadSafeEventHandler<EventArgs>(this.scopeAdded);
                this.master.ScopeDeleted += new ThreadSafeConstructs.ThreadSafeEventHandler<EventArgs>(this.scopeRemoved);
            }
        }

        private void scopeAdded(object sender, EventArgs e)
        {
            (sender as ScopeControl).MouseClick += new System.Windows.Forms.MouseEventHandler(EpochNavigator_MouseDown);
            this.controls.Add(sender as ScopeControl);
            CollectionView source = this.epochList.ItemsSource as CollectionView;
            source.Refresh();

            (sender as ScopeControl).Cursor = this.thisCursor;

            //this.epochList.BeginInit();
            //this.epochList.EndInit();
        }

        private void scopeRemoved(object sender, EventArgs e)
        {
            (sender as ScopeControl).MouseClick -= new System.Windows.Forms.MouseEventHandler(EpochNavigator_MouseDown);
            this.controls.Remove(sender as ScopeControl);
            this.epochList.BeginInit();
            this.epochList.EndInit();

            CollectionView source = this.epochList.ItemsSource as CollectionView;
            source.Refresh();
        }

        private ContentBox cb;

        void EpochNavigator_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!this.IsAdding) return;

            ScopeControl control = sender as ScopeControl;

            float perc = (float)e.X / (float)control.Width;
            float time =  (float)Math.Round(control.Start + perc * control.Length, 1);

            //object toCapture = MesosoftCommon.Utilities.Reflection.DynamicObjects.CreateObject(
            //    new Type[] { typeof(string), typeof(double), typeof(double) },
            //    new string[] { "Epoch Creation", "Epoch Creation", "Epoch Creation" },
            //    new string[] { "Name", "Begin Time", "Length" },
            //    new string[] { "Name", "Begin Time", "Length" },
            //    null,
            //    new object[] { "", time, this.lastLength });






            EpochAddPopup popup = new EpochAddPopup();
            popup.StartTime = time;
            popup.Length = this.lastLength;
            popup.Variable = control.Variables[0];

            if (this.cb != null)
            {
                this.cb.Close();
            }

            this.cb = new ContentBox();
            this.cb.ContentBoxClosed += new EventHandler<ContentBoxEventArgs>(cb_ContentBoxClosed);
            cb.Buttons = ContentBoxButton.OKCancel;
            cb.Top = e.Location.Y;
            cb.Left = e.Location.X;
            cb.Title = "Add Epoch";
            cb.ContentElement = popup;

            cb.ShowDialog();



        }

        void cb_ContentBoxClosed(object sender, ContentBoxEventArgs e)
        {
            if (e.Result == ContentBoxResult.Cancel)
                return;
            EpochAddPopup popup = (sender as ContentBox).ContentElement as EpochAddPopup;
            if (!popup.CheckValidity()) e.Cancel = true;
            else
            {
                if (!popup.ApplyToAllVariables)
                {
                    Epoch epoch = new Epoch(popup.Variable, popup.EpochName, popup.StartTime, popup.StartTime + popup.Length) { IsUser = true };
                    EpochCentral.AddEpoch(epoch);
                    EpochCentral.ProcessPendingEpochs();
                }
                else
                {
                    Variable[] vars = VariableSource.GetAllVariablesInExperiment(popup.Variable.Header.ExperimentName);
                    foreach (Variable var in vars)
                    {
                        Epoch epoch = new Epoch(var, popup.EpochName, popup.StartTime, popup.StartTime + popup.Length) { IsUser = true };
                        EpochCentral.AddEpoch(epoch);
                    }

                    EpochCentral.ProcessPendingEpochs();
                }

                this.lastLength = popup.Length;

            }

            

        }

        public EpochNavigator()
        {
            InitializeComponent();

            this.Epochs.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Epochs_CollectionChanged);
            

            CheckBox box = (this.epochList.ContextMenu.Items[0] as MenuItem).Header as CheckBox;
            //BindingExpression es = box.GetBindingExpression(CheckBox.IsCheckedProperty);
            
            Binding binding = new Binding() { Source = this, Path = new PropertyPath("FilterEpochsNotOnScreen" )};
            box.SetBinding(CheckBox.IsCheckedProperty, binding);

            //CollectionViewSource source = this.epochList.TryFindResource("cvs") as CollectionViewSource;          
            //source.Source = EpochCentral.BindToManager(this.epochList);

            //Binding binding = new Binding();
            //binding.Source = source;
            //this.epochList.SetBinding(ListBox.ItemsSourceProperty, binding);

            
        }

        void Epochs_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            CollectionViewSource source = this.epochList.TryFindResource("cvs") as CollectionViewSource;
            if (!this.sortSet)
            {
                if (source.View != null)
                {
                    ListCollectionView lcv = source.View as ListCollectionView;
                    lcv.CustomSort = new EpochComparer();
                    this.sortSet = true;
                }
            }
        }

        private void goButton_Click(object sender, RoutedEventArgs e)
        {            
            if (this.Master != null)
            {
                Epoch epoch = this.epochList.SelectedItem as Epoch;
                if (epoch != null)
                {
                    double length = -1;
                    if (this.ConstrainToWidth)
                    {
                        length = epoch.EndTime - epoch.BeginTime;
                    }
                    this.Master.ChangeStartAndLength(epoch.BeginTime, length);
                }
            }
        }

        private void manageButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Window win = new System.Windows.Window();
            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(win);

            EpochManager manager = new EpochManager();

            manager.HorizontalAlignment = HorizontalAlignment.Stretch;
            manager.VerticalAlignment = VerticalAlignment.Stretch;
            win.Content = manager;
            win.Height = manager.Height + 32;
            win.Width = manager.Width + 8;
            win.Show();
        }

        private System.Windows.Forms.Cursor thisCursor = System.Windows.Forms.Cursors.Default;

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Primitives.ToggleButton tbutton = sender as System.Windows.Controls.Primitives.ToggleButton;
            if (tbutton.IsChecked.Value)
            {
                foreach (ScopeControl control in this.controls)
                {
                    control.Cursor = System.Windows.Forms.Cursors.Cross;
                }
                thisCursor = System.Windows.Forms.Cursors.Cross;
            }
            else
            {
                foreach (ScopeControl control in this.controls)
                {
                    control.Cursor = System.Windows.Forms.Cursors.Default;
                }
                thisCursor = System.Windows.Forms.Cursors.Default;
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Function currently disabled");

            //if (this.epochList.SelectedItems.Count == 0)
            //{
            //    MessageBox.Show("Please select epochs to delete");
            //    return;
            //}

            //if (MessageBox.Show("Confirm Delete?", "Delete", MessageBoxButton.YesNo) == MessageBoxResult.No) return;



            //this.epochList.BeginInit();

            //Epoch[] epochs = this.epochList.SelectedItems.Cast<Epoch>().ToArray();
            //EpochCentral.RemoveEpochs(epochs);

            //foreach ()
            //{
            //    EpochCentral.RemoveEpoch(epoch);
            //}
            //this.epochList.EndInit();
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            e.Accepted = true;
            Epoch epoch = e.Item as Epoch;

            if (this.FilterEpochsNotOnScreen)
            {
                if (!epoch.AppliesToExperiment)
                {
                    if (this.controls.Any(control => control.Variables.Any(var => var == epoch.Variable)))
                    {
                        e.Accepted = true;
                    }
                    else e.Accepted = false;
                }
                else 
                {
                    if (this.controls.Any(control => control.Variables.Any(var => var.Header.ExperimentName == epoch.Variable.Header.ExperimentName)))
                    {
                        e.Accepted = true;
                    }
                    else e.Accepted = false;

                }
            }
        }

        
    }
}
