namespace ApolloFinal.UI.Scope
{
    partial class ScopeMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        //private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox_length = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.filterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.integrateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.truncateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getMaxAndMinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makePlotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tEMPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flowControl_displays = new System.Windows.Forms.FlowLayoutPanel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.labeltime_End = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxBeginningTime = new System.Windows.Forms.TextBox();
            this.toolboxHotSpots = new System.Windows.Forms.GroupBox();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.exportVariablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolboxHotSpots.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_length
            // 
            this.textBox_length.Location = new System.Drawing.Point(6, 30);
            this.textBox_length.Name = "textBox_length";
            this.textBox_length.Size = new System.Drawing.Size(62, 20);
            this.textBox_length.TabIndex = 1;
            this.textBox_length.Text = "0";
            this.textBox_length.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_length_KeyDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBox_length);
            this.groupBox1.Location = new System.Drawing.Point(12, 400);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(79, 111);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Screen Width";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 56);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Set";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.listView1);
            this.groupBox2.Location = new System.Drawing.Point(97, 400);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(172, 111);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Variables";
            // 
            // listView1
            // 
            this.listView1.ContextMenuStrip = this.contextMenuStrip2;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Location = new System.Drawing.Point(3, 16);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(166, 92);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            this.listView1.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            this.listView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDown);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filterToolStripMenuItem,
            this.integrateToolStripMenuItem,
            this.thresholdToolStripMenuItem,
            this.truncateToolStripMenuItem,
            this.getMaxAndMinToolStripMenuItem,
            this.makePlotToolStripMenuItem,
            this.tEMPToolStripMenuItem,
            this.toolStripSeparator1,
            this.exportVariablesToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(167, 208);
            // 
            // filterToolStripMenuItem
            // 
            this.filterToolStripMenuItem.Name = "filterToolStripMenuItem";
            this.filterToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.filterToolStripMenuItem.Text = "Filter";
            this.filterToolStripMenuItem.Click += new System.EventHandler(this.filterToolStripMenuItem_Click);
            // 
            // integrateToolStripMenuItem
            // 
            this.integrateToolStripMenuItem.Name = "integrateToolStripMenuItem";
            this.integrateToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.integrateToolStripMenuItem.Text = "Integrate";
            this.integrateToolStripMenuItem.Click += new System.EventHandler(this.integrateToolStripMenuItem_Click);
            // 
            // thresholdToolStripMenuItem
            // 
            this.thresholdToolStripMenuItem.Name = "thresholdToolStripMenuItem";
            this.thresholdToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.thresholdToolStripMenuItem.Text = "Threshold";
            this.thresholdToolStripMenuItem.Click += new System.EventHandler(this.thresholdToolStripMenuItem_Click);
            // 
            // truncateToolStripMenuItem
            // 
            this.truncateToolStripMenuItem.Name = "truncateToolStripMenuItem";
            this.truncateToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.truncateToolStripMenuItem.Text = "Truncate";
            this.truncateToolStripMenuItem.Click += new System.EventHandler(this.truncateToolStripMenuItem_Click);
            // 
            // getMaxAndMinToolStripMenuItem
            // 
            this.getMaxAndMinToolStripMenuItem.Name = "getMaxAndMinToolStripMenuItem";
            this.getMaxAndMinToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.getMaxAndMinToolStripMenuItem.Text = "Get Max and Min";
            this.getMaxAndMinToolStripMenuItem.Click += new System.EventHandler(this.getMaxAndMinToolStripMenuItem_Click);
            // 
            // makePlotToolStripMenuItem
            // 
            this.makePlotToolStripMenuItem.Name = "makePlotToolStripMenuItem";
            this.makePlotToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.makePlotToolStripMenuItem.Text = "Make Plot";
            this.makePlotToolStripMenuItem.Click += new System.EventHandler(this.makePlotToolStripMenuItem_Click);
            // 
            // tEMPToolStripMenuItem
            // 
            this.tEMPToolStripMenuItem.Name = "tEMPToolStripMenuItem";
            this.tEMPToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.tEMPToolStripMenuItem.Text = "TEMP";
            this.tEMPToolStripMenuItem.Click += new System.EventHandler(this.tEMPToolStripMenuItem_Click);
            // 
            // flowControl_displays
            // 
            this.flowControl_displays.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flowControl_displays.AutoScroll = true;
            this.flowControl_displays.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.flowControl_displays.Location = new System.Drawing.Point(3, 29);
            this.flowControl_displays.Name = "flowControl_displays";
            this.flowControl_displays.Size = new System.Drawing.Size(684, 336);
            this.flowControl_displays.TabIndex = 6;
            this.flowControl_displays.Resize += new System.EventHandler(this.listView1_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addControlToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(143, 26);
            // 
            // addControlToolStripMenuItem
            // 
            this.addControlToolStripMenuItem.Name = "addControlToolStripMenuItem";
            this.addControlToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.addControlToolStripMenuItem.Text = "Add Control";
            this.addControlToolStripMenuItem.Click += new System.EventHandler(this.addControlToolStripMenuItem_Click_1);
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.hScrollBar1.Location = new System.Drawing.Point(12, 380);
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(673, 17);
            this.hScrollBar1.TabIndex = 0;
            this.hScrollBar1.ValueChanged += new System.EventHandler(this.hScrollBar1_ValueChanged);
            this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
            // 
            // labeltime_End
            // 
            this.labeltime_End.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labeltime_End.Location = new System.Drawing.Point(579, 6);
            this.labeltime_End.Name = "labeltime_End";
            this.labeltime_End.Size = new System.Drawing.Size(108, 13);
            this.labeltime_End.TabIndex = 1;
            this.labeltime_End.Text = "0";
            this.labeltime_End.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.textBoxBeginningTime);
            this.panel1.Controls.Add(this.labeltime_End);
            this.panel1.Controls.Add(this.flowControl_displays);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(694, 372);
            this.panel1.TabIndex = 7;
            // 
            // textBoxBeginningTime
            // 
            this.textBoxBeginningTime.Location = new System.Drawing.Point(3, 3);
            this.textBoxBeginningTime.Name = "textBoxBeginningTime";
            this.textBoxBeginningTime.Size = new System.Drawing.Size(66, 20);
            this.textBoxBeginningTime.TabIndex = 7;
            this.textBoxBeginningTime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxBeginningTime_KeyDown);
            // 
            // toolboxHotSpots
            // 
            this.toolboxHotSpots.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.toolboxHotSpots.Controls.Add(this.elementHost1);
            this.toolboxHotSpots.Location = new System.Drawing.Point(275, 400);
            this.toolboxHotSpots.Name = "toolboxHotSpots";
            this.toolboxHotSpots.Size = new System.Drawing.Size(417, 111);
            this.toolboxHotSpots.TabIndex = 10;
            this.toolboxHotSpots.TabStop = false;
            this.toolboxHotSpots.Text = "Epochs";
            // 
            // elementHost1
            // 
            this.elementHost1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.elementHost1.Location = new System.Drawing.Point(7, 16);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(403, 89);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // exportVariablesToolStripMenuItem
            // 
            this.exportVariablesToolStripMenuItem.Name = "exportVariablesToolStripMenuItem";
            this.exportVariablesToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.exportVariablesToolStripMenuItem.Text = "Export  Variables";
            this.exportVariablesToolStripMenuItem.Click += new System.EventHandler(this.exportVariablesToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(163, 6);
            // 
            // ScopeMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolboxHotSpots);
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "ScopeMaster";
            this.Size = new System.Drawing.Size(700, 514);
            this.Load += new System.EventHandler(this.ScopeMaster_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolboxHotSpots.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        //private System.Windows.Forms.MenuStrip menuStrip1;
        //private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox_length;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FlowLayoutPanel flowControl_displays;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.Label labeltime_End;
        private System.Windows.Forms.Panel panel1;
        //private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        //private System.Windows.Forms.PrintDialog printDialog1;
        //private System.Drawing.Printing.PrintDocument printDocument1;
        //private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem autoSizeToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxBeginningTime;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.ToolStripMenuItem addControlToolStripMenuItem;
        private System.Windows.Forms.GroupBox toolboxHotSpots;
        // private ApolloFinal.Tools.HotSpots.HotSpotSelector HSSelect;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem filterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem integrateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem truncateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getMaxAndMinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makePlotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tEMPToolStripMenuItem;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exportVariablesToolStripMenuItem;
    }
}