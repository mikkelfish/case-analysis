namespace ApolloFinal.UI.Scope
{
    partial class ScopeWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadVariablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.switchDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.loadOldDataIntoDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autosizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localNormalizationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showFullRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newIntervalScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.routineBatchThingyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mixedRoutinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.permissionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.devToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dBTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newUIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.histogramsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.epochViewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addDbToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.switchUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mergeEpochsIntoListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.submitBugReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scopeMaster1 = new ApolloFinal.UI.Scope.ScopeMaster();
            this.testingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.administrationToolStripMenuItem,
            this.devToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.refreshDatabaseToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(917, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadVariablesToolStripMenuItem,
            this.switchDatabaseToolStripMenuItem,
            this.saveScreenToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.toolStripSeparator1,
            this.loadOldDataIntoDatabaseToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadVariablesToolStripMenuItem
            // 
            this.loadVariablesToolStripMenuItem.Name = "loadVariablesToolStripMenuItem";
            this.loadVariablesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.loadVariablesToolStripMenuItem.Text = "Load Variables";
            this.loadVariablesToolStripMenuItem.Click += new System.EventHandler(this.loadVariablesToolStripMenuItem_Click);
            // 
            // switchDatabaseToolStripMenuItem
            // 
            this.switchDatabaseToolStripMenuItem.Name = "switchDatabaseToolStripMenuItem";
            this.switchDatabaseToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.switchDatabaseToolStripMenuItem.Text = "Switch Database";
            this.switchDatabaseToolStripMenuItem.Click += new System.EventHandler(this.switchDatabaseToolStripMenuItem_Click);
            // 
            // saveScreenToolStripMenuItem
            // 
            this.saveScreenToolStripMenuItem.Name = "saveScreenToolStripMenuItem";
            this.saveScreenToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.saveScreenToolStripMenuItem.Text = "Save Screen";
            this.saveScreenToolStripMenuItem.Click += new System.EventHandler(this.saveScreenToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(221, 6);
            // 
            // loadOldDataIntoDatabaseToolStripMenuItem
            // 
            this.loadOldDataIntoDatabaseToolStripMenuItem.Name = "loadOldDataIntoDatabaseToolStripMenuItem";
            this.loadOldDataIntoDatabaseToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.loadOldDataIntoDatabaseToolStripMenuItem.Text = "Load Old Data Into Database";
            this.loadOldDataIntoDatabaseToolStripMenuItem.Click += new System.EventHandler(this.loadOldDataIntoDatabaseToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autosizeToolStripMenuItem,
            this.localNormalizationToolStripMenuItem,
            this.showFullRecordsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // autosizeToolStripMenuItem
            // 
            this.autosizeToolStripMenuItem.Name = "autosizeToolStripMenuItem";
            this.autosizeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.autosizeToolStripMenuItem.Text = "Autosize";
            this.autosizeToolStripMenuItem.Click += new System.EventHandler(this.autosizeToolStripMenuItem_Click);
            // 
            // localNormalizationToolStripMenuItem
            // 
            this.localNormalizationToolStripMenuItem.Name = "localNormalizationToolStripMenuItem";
            this.localNormalizationToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.localNormalizationToolStripMenuItem.Text = "Local Normalization";
            this.localNormalizationToolStripMenuItem.Click += new System.EventHandler(this.localNormalizationToolStripMenuItem_Click);
            // 
            // showFullRecordsToolStripMenuItem
            // 
            this.showFullRecordsToolStripMenuItem.Name = "showFullRecordsToolStripMenuItem";
            this.showFullRecordsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.showFullRecordsToolStripMenuItem.Text = "Show Full Records";
            this.showFullRecordsToolStripMenuItem.Click += new System.EventHandler(this.showFullRecordsToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newIntervalScreenToolStripMenuItem,
            this.toolStripMenuItem1,
            this.toolStripSeparator3,
            this.routineBatchThingyToolStripMenuItem,
            this.mixedRoutinesToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // newIntervalScreenToolStripMenuItem
            // 
            this.newIntervalScreenToolStripMenuItem.Name = "newIntervalScreenToolStripMenuItem";
            this.newIntervalScreenToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.newIntervalScreenToolStripMenuItem.Text = "Score Intervals";
            this.newIntervalScreenToolStripMenuItem.Click += new System.EventHandler(this.newIntervalScreenToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(162, 22);
            this.toolStripMenuItem1.Text = "Interval Routines";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(159, 6);
            // 
            // routineBatchThingyToolStripMenuItem
            // 
            this.routineBatchThingyToolStripMenuItem.Name = "routineBatchThingyToolStripMenuItem";
            this.routineBatchThingyToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.routineBatchThingyToolStripMenuItem.Text = "Raw Routines";
            this.routineBatchThingyToolStripMenuItem.Click += new System.EventHandler(this.routineBatchThingyToolStripMenuItem_Click);
            // 
            // mixedRoutinesToolStripMenuItem
            // 
            this.mixedRoutinesToolStripMenuItem.Name = "mixedRoutinesToolStripMenuItem";
            this.mixedRoutinesToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.mixedRoutinesToolStripMenuItem.Text = "Mixed Routines";
            this.mixedRoutinesToolStripMenuItem.Click += new System.EventHandler(this.mixedRoutinesToolStripMenuItem_Click);
            // 
            // administrationToolStripMenuItem
            // 
            this.administrationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.permissionsToolStripMenuItem,
            this.changeUserToolStripMenuItem});
            this.administrationToolStripMenuItem.Name = "administrationToolStripMenuItem";
            this.administrationToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.administrationToolStripMenuItem.Text = "Administration";
            // 
            // permissionsToolStripMenuItem
            // 
            this.permissionsToolStripMenuItem.Name = "permissionsToolStripMenuItem";
            this.permissionsToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.permissionsToolStripMenuItem.Text = "Permissions";
            this.permissionsToolStripMenuItem.Click += new System.EventHandler(this.permissionsToolStripMenuItem_Click);
            // 
            // changeUserToolStripMenuItem
            // 
            this.changeUserToolStripMenuItem.Name = "changeUserToolStripMenuItem";
            this.changeUserToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.changeUserToolStripMenuItem.Text = "Change User";
            this.changeUserToolStripMenuItem.Click += new System.EventHandler(this.changeUserToolStripMenuItem_Click);
            // 
            // devToolStripMenuItem
            // 
            this.devToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dBTestToolStripMenuItem,
            this.newUIToolStripMenuItem,
            this.histogramsToolStripMenuItem,
            this.epochViewerToolStripMenuItem,
            this.addDbToolStripMenuItem,
            this.switchUserToolStripMenuItem,
            this.mergeEpochsIntoListToolStripMenuItem,
            this.testingToolStripMenuItem});
            this.devToolStripMenuItem.Name = "devToolStripMenuItem";
            this.devToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.devToolStripMenuItem.Text = "Dev";
            // 
            // dBTestToolStripMenuItem
            // 
            this.dBTestToolStripMenuItem.Name = "dBTestToolStripMenuItem";
            this.dBTestToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.dBTestToolStripMenuItem.Text = "DB Test";
            this.dBTestToolStripMenuItem.Click += new System.EventHandler(this.dBTestToolStripMenuItem_Click);
            // 
            // newUIToolStripMenuItem
            // 
            this.newUIToolStripMenuItem.Name = "newUIToolStripMenuItem";
            this.newUIToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.newUIToolStripMenuItem.Text = "New UI";
            this.newUIToolStripMenuItem.Click += new System.EventHandler(this.newUIToolStripMenuItem_Click);
            // 
            // histogramsToolStripMenuItem
            // 
            this.histogramsToolStripMenuItem.Name = "histogramsToolStripMenuItem";
            this.histogramsToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.histogramsToolStripMenuItem.Text = "Histograms";
            this.histogramsToolStripMenuItem.Click += new System.EventHandler(this.histogramsToolStripMenuItem_Click_1);
            // 
            // epochViewerToolStripMenuItem
            // 
            this.epochViewerToolStripMenuItem.Name = "epochViewerToolStripMenuItem";
            this.epochViewerToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.epochViewerToolStripMenuItem.Text = "Epoch Viewer";
            this.epochViewerToolStripMenuItem.Click += new System.EventHandler(this.epochViewerToolStripMenuItem_Click);
            // 
            // addDbToolStripMenuItem
            // 
            this.addDbToolStripMenuItem.Name = "addDbToolStripMenuItem";
            this.addDbToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.addDbToolStripMenuItem.Text = "Add db";
            this.addDbToolStripMenuItem.Click += new System.EventHandler(this.addDbToolStripMenuItem_Click);
            // 
            // switchUserToolStripMenuItem
            // 
            this.switchUserToolStripMenuItem.Name = "switchUserToolStripMenuItem";
            this.switchUserToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.switchUserToolStripMenuItem.Text = "Switch User";
            this.switchUserToolStripMenuItem.Click += new System.EventHandler(this.switchUserToolStripMenuItem_Click);
            // 
            // mergeEpochsIntoListToolStripMenuItem
            // 
            this.mergeEpochsIntoListToolStripMenuItem.Name = "mergeEpochsIntoListToolStripMenuItem";
            this.mergeEpochsIntoListToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.mergeEpochsIntoListToolStripMenuItem.Text = "Merge epochs into list";
            this.mergeEpochsIntoListToolStripMenuItem.Click += new System.EventHandler(this.mergeEpochsIntoListToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.submitBugReportToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // submitBugReportToolStripMenuItem
            // 
            this.submitBugReportToolStripMenuItem.Name = "submitBugReportToolStripMenuItem";
            this.submitBugReportToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.submitBugReportToolStripMenuItem.Text = "Submit Bug Report";
            this.submitBugReportToolStripMenuItem.Click += new System.EventHandler(this.submitBugReportToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click_1);
            // 
            // refreshDatabaseToolStripMenuItem
            // 
            this.refreshDatabaseToolStripMenuItem.Enabled = false;
            this.refreshDatabaseToolStripMenuItem.Name = "refreshDatabaseToolStripMenuItem";
            this.refreshDatabaseToolStripMenuItem.Size = new System.Drawing.Size(109, 20);
            this.refreshDatabaseToolStripMenuItem.Text = "Refresh Database";
            this.refreshDatabaseToolStripMenuItem.Click += new System.EventHandler(this.refreshDatabaseToolStripMenuItem_Click);
            // 
            // scopeMaster1
            // 
            this.scopeMaster1.AutoSizeControls = false;
            this.scopeMaster1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scopeMaster1.Location = new System.Drawing.Point(0, 24);
            this.scopeMaster1.Name = "scopeMaster1";
            this.scopeMaster1.ShowAllOfRecord = false;
            this.scopeMaster1.Size = new System.Drawing.Size(917, 580);
            this.scopeMaster1.TabIndex = 1;
            this.scopeMaster1.Load += new System.EventHandler(this.scopeMaster1_Load);
            // 
            // testingToolStripMenuItem
            // 
            this.testingToolStripMenuItem.Name = "testingToolStripMenuItem";
            this.testingToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.testingToolStripMenuItem.Text = "Testing";
            this.testingToolStripMenuItem.Click += new System.EventHandler(this.testingToolStripMenuItem_Click);
            // 
            // ScopeWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 604);
            this.Controls.Add(this.scopeMaster1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ScopeWindow";
            this.Text = "ScopeWindow";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadVariablesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autosizeToolStripMenuItem;
        private ScopeMaster scopeMaster1;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localNormalizationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem routineBatchThingyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem newIntervalScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showFullRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem devToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dBTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem submitBugReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newUIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mixedRoutinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem histogramsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem loadOldDataIntoDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem switchDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem epochViewerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addDbToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem switchUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem permissionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mergeEpochsIntoListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testingToolStripMenuItem;
    }
}
