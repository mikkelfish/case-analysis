﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DNBSoft.WPF.ProceedureDialog;

namespace ApolloFinal.UI.NewUI.Wizard
{
    /// <summary>
    /// Interaction logic for ApolloWizardSelectExperiment.xaml
    /// </summary>
    public partial class ApolloWizardCreateExperiment : UserControl, IProceedureComponent
    {
        public ApolloWizardCreateExperiment()
        {
            InitializeComponent();
        }

        #region IProceedureComponent Members

        public string KeyText
        {
            get { return "Create an Experiment"; }
        }

        #endregion
    }
}
