﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Blacklight.Controls;
using CeresBase.Projects;
using DNBSoft.WPF.ProceedureDialog;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Microsoft.VisualBasic;

namespace ApolloFinal.UI.NewUI
{
    /// <summary>
    /// Interaction logic for ApolloFinalMainWindow.xaml
    /// </summary>
    public partial class ApolloMainWindow : Window
    {
        private CeresBase.UI.VariableLoading.VariableLoading loader;

        public ApolloMainWindow()
        {
            this.displayCollection = new ApolloDisplayCollection("Group 0");
            InitializeComponent();


            //this.displayCollection = new ObservableCollection<ApolloDisplayGroup>();
           
        }

        public ApolloDisplayCollection DisplayCollection { get { return this.displayCollection; } }
        private ApolloDisplayCollection displayCollection = new ApolloDisplayCollection();
           

        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuLoadVariables_Click(object sender, RoutedEventArgs e)
        {
            //List<IProceedureComponent> configs = new List<IProceedureComponent>();
            //configs.Add(new DefaultConfiguration1());
            //configs.Add(new ApolloWizardCreateExperiment());

            //ProceedureDialog g = new ProceedureDialog("Apollo Load Variable Wizard", new ApolloWizardIntro(), configs, new DefaultConfirmation(),
            //                                                    new DefaultProgress(), new DefaultResults());

            //Application app = new Application();
            //app.Run(g);
            if (loader == null)
            {
                loader = CeresBase.UI.VariableLoading.VariableLoading.Instance;
                //ElementHost.EnableModelessKeyboardInterop(loader);
                loader.Closing += new CancelEventHandler(l_Closing);
                loader.Show();
            }
            else
            {
                loader.Activate();
                loader.Show();
                loader.WindowState = System.Windows.WindowState.Maximized;
            }

            
        }

        void l_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;

            (sender as Window).Dispatcher.BeginInvoke(
                    System.Windows.Threading.DispatcherPriority.Normal,
                        new Action(delegate() { (sender as Window).Hide(); }
                ));
        }


        #region Functions for changing DisplayCollection Items
        private void RenameGroup_Click(object sender, RoutedEventArgs e)
        {
            string NewName = Interaction.InputBox("Please enter the new name of the selected group below.", "Rename Group", this.displayCollection.SelectedItem.GroupName.ToString(),-1,-1);
            if (NewName != "")
                this.displayCollection.SelectedItem.GroupName = NewName;
        }

        private void NewGroup_Click(object sender, RoutedEventArgs e)
        {
            this.displayCollection.Add(new ApolloDisplayGroup("Group " + this.displayCollection.Count));
        }

        private void CopyGroup_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DeleteGroup_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete \"" + this.displayCollection.SelectedItem.GroupName.ToString() + "\"?", "Delete Group", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
            {
                if (this.displayCollection.Count == 1)
                    this.displayCollection.Add(new ApolloDisplayGroup("Group 0"));
                this.displayCollection.Remove(this.displayCollection.SelectedItem);
            }  
        }
        #endregion

        private void ApolloGroupTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.displayCollection.SelectedItem = ((sender as TabControl).SelectedItem) as ApolloDisplayGroup;
        }
    }
}
