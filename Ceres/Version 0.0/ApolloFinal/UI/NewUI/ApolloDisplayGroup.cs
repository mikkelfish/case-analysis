﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApolloFinal.UI.NewUI.Controls;
using System.ComponentModel;

namespace ApolloFinal.UI.NewUI
{
    /// <summary>
    /// Represents a group of all the Apollo UI controls required to view data on-screen.
    /// </summary>
    public class ApolloDisplayGroup : INotifyPropertyChanged
    {
        /// <summary>
        /// Create a display group containing all Apollo controls.
        /// </summary>
        /// <param name="groupName">The title used in the UI to display the group.</param>
        public ApolloDisplayGroup(string groupName)
        {
            this.GroupName = groupName;

            this.ScopeViewer = new ApolloScopeView();
            this.ScopeController = new ApolloScopeControl();
            this.VariableSelector = new ApolloVariableSelector();
            this.EpochSelector = new ApolloEpochSelector();
            this.AnalysisController = new ApolloAnalysisControl();
            this.StatusController = new ApolloStatusControl();

            this.ScopeController.ScopeParametersChanged += new ScopeParametersChangedHandler(ScopeController_ScopeParametersChanged);
            this.EpochSelector.EpochChanged +=new ApolloEpochSelector.EpochEventHandler(EpochSelector_EpochChanged);

        }

        void ScopeController_ScopeParametersChanged(ScopeParametersEventArgs e)
        {
            this.ScopeViewer.GoTo(e.NewStartTime, e.NewEndTime);
        }

        private string groupName;
        /// <summary>
        /// Gets or sets the title used to refer to the group.
        /// </summary>
        public string GroupName {
            get { return groupName; }
            set { 
                groupName = value;
                OnPropertyChanged("GroupName");  
            }
        }
        

        public ApolloScopeView ScopeViewer { get; set; }
        public ApolloScopeControl ScopeController { get; set; }
        public ApolloVariableSelector VariableSelector { get; set; }
        public ApolloEpochSelector EpochSelector { get; set; }
        public ApolloAnalysisControl AnalysisController { get; set; }
        public ApolloStatusControl StatusController { get; set; }

        #region Control Event Handlers
        void EpochSelector_EpochChanged(object sender, CeresBase.Projects.Epoch epoch)
        {
            this.ScopeViewer.GoTo(epoch.BeginTime, epoch.EndTime);
        }
        #endregion
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
