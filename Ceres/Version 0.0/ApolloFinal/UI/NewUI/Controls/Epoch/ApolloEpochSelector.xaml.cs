﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MesosoftCommon.DataStructures;
using CeresBase.Projects;
using MesosoftCommon.ExtensionMethods;
using CeresBase.Data;

namespace ApolloFinal.UI.NewUI.Controls
{
    /// <summary>
    /// Interaction logic for ApolloEpochSelector.xaml
    /// </summary>
    public partial class ApolloEpochSelector : UserControl
    {
        public ApolloEpochSelector()
        {
            InitializeComponent();

            Binding binding = new Binding() { Source = this, Path = new PropertyPath("FilterEpochsNotOnScreen")};
            ContextMenu myMenu = (this.Content as Grid).TryFindResource("context") as ContextMenu;
            ((myMenu.Items[0] as MenuItem).Header as CheckBox).SetBinding(CheckBox.IsCheckedProperty, binding);

        }

        #region Epoch Filtering Functions
        public bool FilterEpochsNotOnScreen
        {
            get { return (bool)GetValue(FilterEpochsNotOnScreenProperty); }
            set { SetValue(FilterEpochsNotOnScreenProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FilterEpochsNotOnScreen.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FilterEpochsNotOnScreenProperty =
            DependencyProperty.Register("FilterEpochsNotOnScreen", typeof(bool), typeof(ApolloEpochSelector), new PropertyMetadata(filterChanged));

        private static void filterChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ApolloEpochSelector navigator = obj as ApolloEpochSelector;
            if (navigator.epochList.ItemsSource != null)
            {
                CollectionView source = navigator.epochList.ItemsSource as CollectionView;
                source.Refresh();



                navigator.epochList.ContextMenu.IsOpen = false;

            }
        }

        //private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        //{
        //    e.Accepted = true;
        //    Epoch epoch = e.Item as Epoch;

        //    if (this.FilterEpochsNotOnScreen)
        //    {
        //        if (!epoch.AppliesToExperiment)
        //        {
        //            if (this.controls.Any(control => control.Variables.Any(var => var == epoch.Variable)))
        //            {
        //                e.Accepted = true;
        //            }
        //            else e.Accepted = false;
        //        }
        //        else
        //        {
        //            if (this.controls.Any(control => control.Variables.Any(var => var.Header.ExperimentName == epoch.Variable.Header.ExperimentName)))
        //            {
        //                e.Accepted = true;
        //            }
        //            else e.Accepted = false;

        //        }
        //    }
        //}
        #endregion


        public BeginInvokeObservableCollection<Epoch> Epochs
        {
            get
            {
                return EpochCentral.BindToManager(this.Dispatcher);

            }
        }

        private void epochList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EpochChanged(this, this.epochList.SelectedItem as Epoch);
        }

        #region Events
        public delegate void EpochEventHandler(object sender, Epoch epoch);
        public event EpochEventHandler EpochChanged;
        #endregion


    }
}
