﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloFinal.UI.NewUI.Controls
{
    /// <summary>
    /// Interaction logic for ApolloScopeView.xaml
    /// </summary>
    public partial class ApolloScopeView : UserControl
    {
        public ApolloScopeView()
        {
            this.offset = 0;
            this.bufferLength = 120;
            this.RangeStart = 0;
            this.RangeEnd = 60;

            InitializeComponent();
            
        }

        private double offset;
        private double bufferLength;

        private void functionButton_Click(object sender, RoutedEventArgs e)
        {
            this.ScopeAccessQuickMenu.IsOpen = true;
        }

        private void rangeSlider_RangeChanged(object sender, EventArgs e)
        {
            foreach (ApolloScopeChannel channel in scope.channelStackPanel.Children.OfType<ApolloScopeChannel>())
            {
                channel.Draw(this.rangeSlider.RangeStart + this.offset, this.rangeSlider.RangeEnd + this.offset);
            }
        }

        #region Scope and RangeSlider Dependency Property declarations
        public double RangeStart
        {
            get { return (double)GetValue(RangeStartProperty); }
            set { SetValue(RangeStartProperty, value); }
        }

        public static readonly DependencyProperty RangeStartProperty = DependencyProperty.Register("RangeStart", typeof(double), typeof(ApolloScopeView), new PropertyMetadata(0.0, RangeStartChanged));

        public double RangeEnd
        {
            get { return (double)GetValue(RangeEndProperty); }
            set { SetValue(RangeEndProperty, value); }
        }

        public static readonly DependencyProperty RangeEndProperty = DependencyProperty.Register("RangeEnd", typeof(double), typeof(ApolloScopeView), new PropertyMetadata(0.0, RangeEndChanged));

        #endregion

        #region Code to deal with Scope Dependency Properties

        private static void RangeStartChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            d.SetValue(RangeStartProperty, e.NewValue);       
        }

        private static void RangeEndChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            d.SetValue(RangeEndProperty, e.NewValue);
        }
        #endregion

        /// <summary>
        /// Sets the start and end time for the scope to display data.
        /// </summary>
        /// <param name="start">Start time (in seconds).</param>
        /// <param name="end">End time (in seconds).</param>
        public void GoTo(double start, double end)
        {
            if (end > start)
            {
                this.offset = start;
                this.bufferLength = end - start;
                this.rangeSlider.Maximum = this.bufferLength;
                this.rangeSlider.RangeStart = 0;
                this.rangeSlider.RangeEnd = this.bufferLength;                
                this.rangeSlider.RangeChanged += new EventHandler(rangeSlider_RangeChanged);
            }
            else if (end == start)
            {
                this.offset = start;
                this.bufferLength = start + 30;
                this.rangeSlider.Maximum = this.bufferLength;
                this.rangeSlider.RangeStart = 0;
                this.rangeSlider.RangeEnd = this.bufferLength;
                this.rangeSlider.RangeChanged += new EventHandler(rangeSlider_RangeChanged);
            }
        }

        private void ScopeAccessQuickClear_Click(object sender, RoutedEventArgs e)
        {
            this.scope.channelStackPanel.Children.Clear();
        }
    }
}
