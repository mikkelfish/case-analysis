﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using CeresBase.Data;
using ThreadSafeConstructs;
using CeresBase.IO.ImageCreation;
using CeresBase.UI;

namespace ApolloFinal.UI.NewUI.Controls
{
    /// <summary>
    /// Interaction logic for ApolloScopeChannel.xaml
    /// </summary>
    public partial class ApolloScopeChannel : UserControl
    {
        public ApolloScopeChannel(VisualVariable visualVariable, double start, double end)
        {
            InitializeComponent();
            this.VisualVariable = visualVariable;
            this.channelData.Stroke = this.VisualVariable.Stroke;
            this.channelData.StrokeThickness = 2;
            this.Draw(start, end);
        }

        public VisualVariable VisualVariable;

        public bool LockedToScope { get; set; }

        /// <summary>
        /// Draws the PolyLine according to the variable data in a range.
        /// </summary>
        /// <param name="start">The start time of the desired data.</param>
        /// <param name="end">The end time of the desired data.</param>
        public void Draw(double start, double end)
        {

            float[] dataPoints = VisualVariable.Variable[0].GetContiguousData(start, end);
            //this.channelData.Points = dataPoints.Select((index, val) => new Point(index, val)).ToArray();

            this.channelData.Points.Clear();

            for(int i = 0; i < dataPoints.Length; i++)
            {
               this.channelData.Points.Add(new Point(i, dataPoints[i]));
            }
           
        }

        private void channelData_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Draw(0, 30);
        }

    }
}
