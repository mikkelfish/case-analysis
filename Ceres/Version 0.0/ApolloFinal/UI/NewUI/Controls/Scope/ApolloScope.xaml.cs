﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloFinal.UI.NewUI.Controls
{
    /// <summary>
    /// Interaction logic for ApolloScope.xaml
    /// </summary>
    public partial class ApolloScope : UserControl
    {
        public ApolloScope()
        {
            InitializeComponent();
        }
        
        /// <summary>
        /// Dragging and dropping behavior.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void channelStackPanel_DragOver(object sender, DragEventArgs e)
        {
            this.addChannel((e.Data.GetData(typeof(VisualVariable[])) as VisualVariable[]), 0);
        }

        private void addChannel(VisualVariable[] visualVariable, uint startingPosition)
        {
            foreach (VisualVariable item in visualVariable)
            {
                channelStackPanel.Children.Add(new ApolloScopeChannel(item, 60, 120));   
            }
        }
    }
}
