﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApolloFinal.UI.NewUI
{
    public delegate void ScopeParametersChangedHandler(ScopeParametersEventArgs e);

    public class ScopeParametersEventArgs : EventArgs
    {
        public double NewStartTime { get; set; }
        public double NewEndTime { get; set; }
        public double NewScreenBuffer { get; set; }
        public double NewScreenWidth { get; set; }

        public bool ScreenBufferChanged { get; set; }
    }
}
