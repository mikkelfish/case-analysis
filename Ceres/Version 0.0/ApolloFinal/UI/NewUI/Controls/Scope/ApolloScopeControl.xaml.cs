﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApolloFinal.UI.NewUI.Controls
{
    /// <summary>
    /// Interaction logic for ApolloScopeControl.xaml
    /// </summary>
    public partial class ApolloScopeControl : UserControl
    {
        public ApolloScopeControl()
        {
            InitializeComponent();
        }

        public event ScopeParametersChangedHandler ScopeParametersChanged;
        protected virtual void OnSizeChanged(ScopeParametersEventArgs e)
        {
            if(ScopeParametersChanged != null)
            {
                ScopeParametersChanged(e);
            }
        }

        private void GotoButton_Click(object sender, RoutedEventArgs e)
        {
            double startMin = 0;
            double startSec = 0;
            double endMin = 0;
            double endSec = 0;

            if (double.TryParse(this.StartTimeMinute.Text, out startMin) && double.TryParse(this.StartTimeSecond.Text, out startSec) &&
                double.TryParse(this.EndTimeMinute.Text, out endMin) && double.TryParse(this.EndTimeSecond.Text, out endSec))
            {
                double startTime = startMin * 60 + startSec;
                double endTime = endMin * 60 + endSec;
                OnSizeChanged(new ScopeParametersEventArgs() { NewStartTime = startTime, NewEndTime = endTime });

            }
        }
    }
}
