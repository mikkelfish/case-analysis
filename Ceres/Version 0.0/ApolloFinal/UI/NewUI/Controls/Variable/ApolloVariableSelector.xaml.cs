﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using CeresBase.Data;
using CeresBase.UI;
using System.Collections.ObjectModel;
using MesosoftCommon.DataStructures;
using System.Collections.Specialized;

namespace ApolloFinal.UI.NewUI.Controls
{
    /// <summary>
    /// Interaction logic for ApolloVariableSelector.xaml
    /// </summary>
    public partial class ApolloVariableSelector : UserControl
    {

        public ApolloVariableSelector()
        {

            this.visualVariableList = new ObservableCollection<VisualVariable>();

            InitializeComponent();
            VariableSource.Variables.CollectionChanged += new NotifyCollectionChangedEventHandler(Variables_CollectionChanged);
        }

        void Variables_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // MessageBox.Show("test");
            //this.Dispatcher.BeginInvoke(new EventHandler(delegate(object test, EventArgs whee) { MessageBox.Show("Test"); }), sender, e);
            if (e.Action == NotifyCollectionChangedAction.Add && e.NewItems != null)
            {
                foreach (Variable var in e.NewItems)
                {
                    this.visualVariableList.Add(new VisualVariable(var));
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove && e.OldItems != null)
            {
                foreach (Variable var in e.OldItems)
                {
                    this.visualVariableList.Remove(this.visualVariableList.First(v => v.Variable == var));
                }
            }
            else
                return; //throw new NotImplementedException();
        }

     
  


        public ObservableCollection<VisualVariable> VisualVariableList
        {
            get
            {


                return this.visualVariableList;                
            }
        }
        private ObservableCollection<VisualVariable> visualVariableList;

        #region Events
        private void variableListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show(sender.ToString());
        }
        #endregion

        #region Dragging and Dropping

        private void variableListView_MouseDown(object sender, MouseButtonEventArgs e)
        {
                    
            VisualVariable[] toPass = this.variableListView.SelectedItems.Cast<VisualVariable>().ToArray();

            if (toPass != null && toPass.Length > 0)
            {
                if (DragDrop.DoDragDrop(variableListView, toPass, DragDropEffects.Copy) == DragDropEffects.Copy)
                {
                    e.Handled = true;
                }

            }
     
        }

        #endregion




    }
}
