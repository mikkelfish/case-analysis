﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CeresBase.Projects;
using CeresBase.Data;
using System.Globalization;
using System.Windows;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace ApolloFinal.UI.NewUI.Controls
{
    [ValueConversion(typeof(Variable), typeof(String))]
    public class ApolloVariableSelectorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            Variable item = value as Variable;
           if (item == null) return "None";
                return item.FullDescription + " Converted";
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
