﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Data;
using System.Windows.Media;
using System.Threading;

namespace ApolloFinal.UI.NewUI.Controls
{
    public class VisualVariable
    {
        public VisualVariable(Variable newVar)
        {
            this.Variable = newVar;

            Random r = new Random();
            Thread.Sleep(50);
            this.Stroke = new SolidColorBrush(Color.FromRgb((byte)r.Next(0, 255), (byte)r.Next(0, 255), (byte)r.Next(0, 255)));

        }
        /// <summary>
        /// The actual Variable which contains data.
        /// </summary>
        public Variable Variable { get; set; }

        /// <summary>
        /// Gets or sets the color used to draw the Variable on screen.
        /// </summary>
        public System.Windows.Media.Brush Stroke { get; set; }

        /// <summary>
        /// Gets or sets whether the Variable on screen stays locked in position when scrolling. 
        /// </summary>
        public bool LockedToScope { get; set; }

        public override string ToString()
        {
            return this.Variable.FullVarName;
        }

    }
}
