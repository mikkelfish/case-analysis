﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Collections;

namespace ApolloFinal.UI.NewUI
{
    /// <summary>
    /// Represents a collection of all the display groups used to view data in the ApolloFinal UI.
    /// </summary>
    public class ApolloDisplayCollection : ObservableCollection<ApolloDisplayGroup>
    {
        #region Constructors
        /// <summary>
        /// Creates a new ApolloDisplayCollection. This will insert a new group into the collection automatically.
        /// </summary>
        public ApolloDisplayCollection()
        {
            this.Add(new ApolloDisplayGroup("Undefined Group"));
        }
        
        /// <summary>
        /// Creates a new ApolloDisplayCollection with one ApolloDisplayGroup item with a custom title.
        /// </summary>
        /// <param name="groupName">The title used in the UI to display the group.</param>
        public ApolloDisplayCollection(string groupName)
        {
            this.Add(new ApolloDisplayGroup(groupName));
        }

        /// <summary>
        /// Creates a new ApolloDisplayCollection from an array of ApolloDisplayGroup.
        /// </summary>
        /// <param name="displayGroups"></param>
        public ApolloDisplayCollection(ApolloDisplayGroup[] displayGroups)
        {
            foreach (ApolloDisplayGroup item in displayGroups)
            {
                this.Add(item);                
            }
        }
        #endregion

        public ApolloDisplayGroup SelectedItem { get; set; }

    }
}
