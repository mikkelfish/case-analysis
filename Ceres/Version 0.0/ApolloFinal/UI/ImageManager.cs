//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;
//using System.Windows.Forms;
//using CeresBase.Projects.Flowable;

//namespace ApolloFinal.UI
//{
//    public partial class ImageManager : Form
//    {
//        private class ImageEntry
//        {
//            private IBatchViewable[] view;
//            public IBatchViewable[] Viewable 
//            {
//                get { return this.view; }
//                set { this.view = value; }
//            }

//            private string disp = "Please enter a name";
//            public string DisplayName 
//            {
//                get { return this.disp; }
//                set { this.disp = value; }
//            }

//            public ImageEntry(IBatchViewable[] view)
//            {
//                this.view = view;
//            }

//            public override string ToString()
//            {
//                return this.DisplayName;
//            }
//        }

//        private static ImageEntry lastEntry;
//        private static CeresBase.UI.BindableList<ImageEntry> entries = new CeresBase.UI.BindableList<ImageEntry>();

//        public string nameCopy
//        {
//            get
//            {
//                if (lastEntry == null)
//                    return "No image has been viewed.";
//                return lastEntry.DisplayName;
//            }
//            set
//            {
//                if (lastEntry == null)
//                    return;
//                lastEntry.DisplayName = value;
//            }
//        }


//        public static void SetLast(IBatchViewable[] view)
//        {
//            lastEntry = new ImageEntry(view);
//            EventHandler handle = fired;
//            if(handle != null) handle(null, null);  
//        }

//        private static event EventHandler fired; 

//        public ImageManager()
//        {
//            InitializeComponent();

//            fired += new EventHandler(ImageManager_fired);

//            this.textBox1.DataBindings.Add("Text", this, "nameCopy");
//            this.listBoxStored.DataSource = entries;
//        }

//        void ImageManager_fired(object sender, EventArgs e)
//        {
//            if(this.textBox1.DataBindings.Count == 1) 
//                this.textBox1.DataBindings[0].ReadValue();
//        }

//        private void label1_Click(object sender, EventArgs e)
//        {

//        }

//        private void listBoxStored_SelectedIndexChanged(object sender, EventArgs e)
//        {

//        }

//        private void buttonStore_Click(object sender, EventArgs e)
//        {
//            if (lastEntry == null)
//            {
//                MessageBox.Show("Can't determine what to save.");
//                return;
//            }

//            entries.Add(lastEntry);
//        }

//        private void buttonView_Click(object sender, EventArgs e)
//        {
//            if (lastEntry == null)
//            {
//                MessageBox.Show("Can't determine what to view.");
//                return;
//            }

//            lastEntry.Viewable[0].View(lastEntry.Viewable, false);
//        }

//        private void buttonWrite_Click(object sender, EventArgs e)
//        {
//            if (lastEntry == null)
//            {
//                MessageBox.Show("Can't determine what to write.");
//                return;
//            }

//            List<IBatchWritable> toWrite = new List<IBatchWritable>();
//            for (int i = 0; i < lastEntry.Viewable.Length; i++)
//            {
//                if (lastEntry.Viewable[i] is IBatchWritable) toWrite.Add(lastEntry.Viewable[i] as IBatchWritable);
//            }

//            if (toWrite.Count == 0)
//            {
//                MessageBox.Show("Can't write this item.");
//                return;
//            }

//            toWrite[0].WriteToDisk(toWrite.ToArray());
//        }

//        private void buttonViewStored_Click(object sender, EventArgs e)
//        {
//            if (this.listBoxStored.SelectedItems.Count == 0)
//            {
//                MessageBox.Show("Please select an item.");
//                return;
//            }

//            List<IBatchViewable> toView = new List<IBatchViewable>();
//            foreach (ImageEntry view in this.listBoxStored.SelectedItems)
//            {
//                toView.AddRange(view.Viewable);
//            }

//            toView[0].View(toView.ToArray(), false);
//        }

//        private void buttonWriteStored_Click(object sender, EventArgs e)
//        {
//            if (this.listBoxStored.SelectedItems.Count == 0)
//            {
//                MessageBox.Show("Please select an item.");
//                return;
//            }

//            List<IBatchWritable> toWrite = new List<IBatchWritable>();
//            foreach (ImageEntry entry in this.listBoxStored.SelectedItems)
//            {
//                for (int i = 0; i < entry.Viewable.Length; i++)
//                {
//                    if (entry.Viewable[i] is IBatchWritable) 
//                        toWrite.Add(entry.Viewable[i] as IBatchWritable);
//                }
//            }
            
//            if (toWrite.Count == 0)
//            {
//                MessageBox.Show("Can't write any items.");
//                return;
//            }

//            toWrite[0].WriteToDisk(toWrite.ToArray());
           
//        }

//        private void buttonDelete_Click(object sender, EventArgs e)
//        {
//            if (this.listBoxStored.SelectedItem == null)
//            {
//                MessageBox.Show("Please select an item.");
//                return;
//            }

//            List<object> sel = new List<object>();

//            foreach (object o in this.listBoxStored.SelectedItems)
//            {
//                sel.Add(o);
//            }
//            foreach (object entry in sel)
//            {
//                entries.Remove(entry as ImageEntry);
//            }

//        }
//    }
//}