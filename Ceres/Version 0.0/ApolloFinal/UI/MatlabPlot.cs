//using System;
//using System.Collections.Generic;
//using System.Text;
//using ApolloFinal.Tools.BatchProcessing;
//using MathWorks.MATLAB.NET.Arrays;
//using ApolloFinal.UI.Scope;
//using System.Collections;
//using CeresBase.IO;
//using System.Windows.Forms;
//using CeresBase.UI;
//using CeresBase.Projects.Flowable;

//namespace ApolloFinal.UI
//{
//    public class MatlabPlot : IBatchViewable, IBatchWritable
//    {

//        public override string ToString()
//        {
//            if (this.Label != null) return this.Label;
//            return base.ToString();
//        }

//        private IList xData;
//        public IList XData
//        {
//            get { return xData; }
//            set { xData = value; }
//        }

//        private IList yData;
//        public IList YData
//        {
//            get { return yData; }
//            set { yData = value; }
//        }

//        private IList zData;
//        public IList ZData
//        {
//            get { return zData; }
//            set { zData = value; }
//        }

//        private string label="";
//        public string Label
//        {
//            get { return label; }
//            set { label = value; }
//        }

//        public IBatchViewable[] View(IBatchViewable[] viewable)
//        {
//            return this.View(viewable, true);
//        }
	
//        public IBatchViewable[] View(IBatchViewable[] viewable, bool useManager)
//        {
            

//            List<IBatchViewable> toRet = new List<IBatchViewable>();
//            List<MatlabPlot> plots = new List<MatlabPlot>();
//            MatlabPlot first = null;
//            int numGraphs = 0;
//            int numYs = 0;
//            foreach (IBatchViewable view in viewable)
//            {
//                if (!(view is MatlabPlot))
//                {
//                    toRet.Add(view);
//                    continue;
//                }
//                else
//                {
//                    if(first == null)
//                    {
//                        first = view as MatlabPlot;
//                        int firstRank = (first.yData as Array).Rank;
//                        if (firstRank == 1)
//                        {
//                            numYs = first.yData.Count;
//                        }
//                        else
//                        {
//                            numYs = (first.yData as Array).GetLength(1);
//                        }
//                    }

//                    MatlabPlot thisOne = view as MatlabPlot;
//                    int yRank = (thisOne.yData as Array).Rank;
//                    int thisYs = 0;
//                    if (yRank == 1)
//                    {
//                        thisYs = thisOne.yData.Count;
//                    }
//                    else
//                    {
//                        thisYs = (thisOne.yData as Array).GetLength(1);
//                    }

//                    if(first.zData == null && thisOne.zData == null && thisOne.xData.Count == first.xData.Count &&
//                        thisYs == numYs)
//                    {
//                        plots.Add(thisOne);
//                        if (yRank == 1) numGraphs++;
//                        else numGraphs += (thisOne.yData as Array).GetLength(0);

//                    }
//                    else if (first.zData != null && thisOne.zData != null && first.zData.Count == thisOne.zData.Count &&
//                        thisOne.xData.Count == first.xData.Count && thisOne.yData.Count == first.yData.Count)
//                    {
//                        plots.Add(thisOne);
//                    }
//                    else toRet.Add(view);
//                }
//            }

//            if(useManager) ImageManager.SetLast(plots.ToArray());

//            if (first == null) return toRet.ToArray();

//            if (first.zData == null)
//            {
//                //EverythingMatlab.EverythingMatlabclass suite = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;
               

//                double[,] yData = new double[numGraphs, first.xData.Count];
//                int curYData = 0;
//                for (int i = 0; i < plots.Count; i++)
//                {
//                    Array asArray = plots[i].yData as Array;
//                    if (asArray.Rank == 1)
//                    {
//                        for (int j = 0; j < plots[i].yData.Count; j++)
//                        {
//                            yData[curYData, j] = Convert.ToDouble(plots[i].yData[j]);
//                        }
//                        curYData++;
//                    }
//                    else
//                    {
//                        for (int j = 0; j < asArray.GetLength(0); j++)
//                        {
//                            for (int k = 0; k < asArray.GetLength(1); k++)
//                            {
//                                try
//                                {

//                                    if (plots[i].yData.GetType() == typeof(float[,]))
//                                    {
//                                        yData[curYData, k] = Convert.ToDouble((plots[i].yData as float[,])[j, k]);
//                                    }
//                                    else
//                                    {
//                                        yData[curYData, k] = Convert.ToDouble((plots[i].yData as double[,])[j, k]);
//                                    }
//                                }
//                                catch
//                                {
//                                    int y = 0;
//                                }
//                            }
//                            curYData++;
//                        }
//                    }
//                }
//                MWNumericArray xarray = new MWNumericArray((Array)first.xData);
//                MWNumericArray yarray = new MWNumericArray(yData);
//                //suite.plotter(xarray, yarray);
//                //SimpleGraph sg = new SimpleGraph(first.xData, yData);
//                //sg.Show();
//            }
//            else
//            {
//                Graph3D graph = new Graph3D();

//                for (int j = 0; j < plots.Count; j++)
//                {
//                    double[,] points = new double[plots[j].xData.Count, 3];
//                    for (int i = 0; i < plots[j].xData.Count; i++)
//                    {
//                        points[i, 0] = (float)plots[j].xData[i];
//                        points[i, 1] = (float)plots[j].yData[i];
//                        points[i, 2] = (float)plots[j].zData[i];
//                    }

//                    graph.OpenGL.AddThreeDObject(points);
//                }

//                graph.Show();

//                //if (MatlabPlot.threed == null)
//                //{
//                //    MatlabPlot.threed = new MatlabThreeD.MatlabThreeD();
//                //}
//                //MatlabPlot.threed.ThreeDPlot((MWNumericArray)plot.XData, (MWNumericArray)plot.YData, (MWNumericArray)plot.ZData);
//            }


//            return toRet.ToArray();
//        }

//        #region IBatchWritable Members

//        public IBatchWritable[] WriteToDisk(IBatchWritable[] toWrite)
//        {
//            SaveFileDialog diag = new SaveFileDialog();
//            if (this.label != null)
//            {
//                string toBuild = "";
//                for (int i = 0; i < this.label.Length; i++)
//                {
//                    if (!char.IsLetterOrDigit(this.label[i]) && this.label[i] != ' ')
//                    {
//                        toBuild += "-";
//                    }
//                    else toBuild += this.label[i];
//                }
//                diag.FileName += toBuild;
//            }
//            diag.Filter = "CSV(*.csv)|*.csv|All Files(*.*)|*.*";
//            diag.AddExtension = true;
//            diag.DefaultExt = ".csv";
//            if (diag.ShowDialog() == DialogResult.Cancel) return toWrite;

//            List<IBatchWritable> toRet = new List<IBatchWritable>();
//            List<object[]> output = new List<object[]>();                      
//            foreach (IBatchWritable writeable in toWrite)
//            {
//                if (!(writeable is MatlabPlot))
//                {
//                    toRet.Add(writeable);
//                    continue;
//                }

//                MatlabPlot plot = writeable as MatlabPlot;

//                object[] line1 = new object[1];
//                line1[0] = plot.label;
//                output.Add(line1);

//                int numColumns = 1;
//                if (plot.yData != null)
//                {
//                    int rank = (plot.yData as Array).Rank;
//                    if (rank == 1) numColumns++;
//                    else
//                    {
//                        numColumns += (plot.yData as Array).GetLength(0);
//                    }
//                }

//                if (plot.zData != null)
//                {
//                    int rank = (plot.zData as Array).Rank;
//                    if (rank == 1) numColumns++;
//                    else
//                    {
//                        numColumns += (plot.zData as Array).GetLength(0);
//                    }
//                }

//                for (int i = 0; i < plot.XData.Count; i++)
//                {
//                    object[] line = new object[numColumns];
//                    line[0] = plot.xData[i];
//                    int curCol = 1;


//                    if (plot.yData != null)
//                    {
//                        int rank = (plot.yData as Array).Rank;
//                        if (rank == 1)
//                        {
//                            line[1] = plot.yData[i];
//                            curCol++;
//                        }
//                        else
//                        {
//                            int numYs = (plot.yData as Array).GetLength(0);
//                            for (int j = 0; j < numYs; j++)
//                            {
//                                line[curCol] = (plot.yData as double[,])[j, i];
//                                curCol++;
//                            }
//                        }
//                    }
//                    if (plot.zData != null)
//                    {
//                        int rank = (plot.zData as Array).Rank;

//                        if (rank == 1)
//                        {
//                            line[2] = plot.zData[i];
//                            curCol++;                        
//                        }
//                        else
//                        {
//                            int numZs = (plot.zData as Array).GetLength(0);
//                            for (int j = 0; j < numZs; j++)
//                            {
//                                line[curCol] = (plot.zData as double[,])[j, i];
//                                curCol++;
//                            }
//                        }
//                    }
//                    output.Add(line);
//                }

//                object[] blankLine = new object[3];
//                blankLine[0] = "";
//                blankLine[1] = "";
//                blankLine[2] = "";
//                output.Add(blankLine);
//            }

//            CSVWriter.WriteCSV(diag.FileName, output.ToArray());
//            return toRet.ToArray();            
//        }

//        #endregion

       
//    }
//}
