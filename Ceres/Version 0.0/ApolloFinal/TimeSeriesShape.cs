﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Data;

namespace ApolloFinal
{
    //public static class TimeSeriesShapeExtensions
    //{
    //    public static float[] GetTimeSeriesData(this SpikeVariable var, double start, double end)
    //    {
    //        if(var.SpikeType != SpikeVariableHeader.SpikeType.Pulse)
    //            return var[0].GetContiguousData(start, end);

    //        List<float> toRet = new List<float>();
    //        float[] all = var[0].GetAllData().Select(d => d * (float)var.Resolution).ToArray();
    //        foreach (float val in all)
    //        {
    //            if (val >= start && val <= end)
    //            {
    //                toRet.Add(val);
    //            }
    //        }

    //        return toRet.ToArray();
    //    }

    //}

    public class TimeSeriesShape : IDataShape
    {
        public TimeSeriesShape(double frequency)
        {
            if (frequency <= 0) throw new ArgumentException("Frequency must be greater than 0");

            this.Frequency = frequency;
        }

        public double Frequency { get; private set; }

        public double TimeToGrid(double time)
        {
            double amount = time;
            return amount * Frequency;
        }

        public double[] TimeArrayToGrid(double[] times)
        {
            List<double> toRet = new List<double>();
            foreach (double time in times)
            {
                toRet.Add(this.TimeToGrid(time));
            }

            return toRet.ToArray();
        }

       

        public double GridToTime(double gridcoord)
        {
            return gridcoord / this.Frequency;
        }

        public double[] GridArrayToTime(double[] gridcoords)
        {
            List<double> toRet = new List<double>();
            foreach (double grid in gridcoords)
            {
                toRet.Add(this.GridToTime(grid));
            }

            return toRet.ToArray();
        }

   

        #region IDataShape Members

        double[] IDataShape.ShapeToGrid(object args)
        {
            return new double[]{this.TimeToGrid((double)args)};
        }

        object IDataShape.GridToShape(double[] gridCoords)
        {
            return this.GridToTime(gridCoords[0]);
        }

        double[][] IDataShape.ShapeToGridArray(object[] args)
        {
            return new double[][] { this.TimeArrayToGrid(args.Cast<double>().ToArray()) };
        }

        object[] IDataShape.GridToShapeArray(double[][] gridCoords)
        {
            return this.GridArrayToTime(gridCoords[0]).Cast<object>().ToArray();
        }

        public string[] DimensionNames
        {
            get { return new string[] { "Time" }; }
        }

        #endregion

        #region ICeresSerializable Members

        public void AddSpecificInformation(CeresBase.IO.ICeresWriter writer, CeresBase.IO.ICeresFile file)
        {

        }

        public void ReadSpecificInformation(CeresBase.IO.ICeresReader reader, CeresBase.IO.ICeresFile file)
        {
            
        }

        #endregion
    }
}
