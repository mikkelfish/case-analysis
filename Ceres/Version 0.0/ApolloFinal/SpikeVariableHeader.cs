using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using CeresBase.Development;
using System.ComponentModel;

namespace ApolloFinal
{
    public class SpikeVariableHeader : VariableHeader
    {
        public enum SpikeType { Integrated, Raw, Pulse };

        private SpikeType spikeType;

        [Browsable(false)]
        public override Type VarType
        {
            get
            {
                return typeof(SpikeVariable);
            }
        }



        [CeresCreated("Mikkel", "03/20/06")]
        [Category("SpikeVariable Information")]
        [Description("The type of information stored in this variable.")]
        public SpikeType SpikeDesc
        {
            get { return spikeType; }
        }

        [CeresCreated("Mikkel", "03/20/06")]
        [Category("SpikeVariable Information")]
        [Description("The resolution in seconds/data point")]
        public double Resolution
        {
            get
            {
                return 1D/this.frequency;
            }
        }

        private double frequency;

        [Category("SpikeVariable Information")]
        [Description("Frequency of the data in HZ")]
        public double Frequency
        {
            get
            {
                return frequency;
            }
        }

        protected SpikeVariableHeader(string varName, string unitsName, string experimentName) : base(varName, unitsName, experimentName)
        { 
            this.spikeType = SpikeType.Raw;
            this.frequency = 0;
        }

        [CeresBase.UI.MethodEditor.EditorConstructor]
        public SpikeVariableHeader(
            [CeresBase.UI.MethodEditor.EditorParam(CeresBase.UI.MethodEditor.EditorParamAttribute.VariableDescription)]
            string varName,
           [CeresBase.UI.MethodEditor.EditorParam(CeresBase.UI.MethodEditor.EditorParamAttribute.UnitsDescription)]
            string unitsName,
           [CeresBase.UI.MethodEditor.EditorParam(CeresBase.UI.MethodEditor.EditorParamAttribute.ExperimentDescription)]
            string experimentName,
            [CeresBase.UI.MethodEditor.EditorParam("Specify the frequency")]
            double frequency, 
            [CeresBase.UI.MethodEditor.EditorParam("Specify the type of spikes")]
            SpikeType spikeType)
            : base(varName, unitsName, experimentName)
        {
            this.frequency = frequency;
            this.spikeType = spikeType;
        }

        public override void AddSpecificInformation(CeresBase.IO.ICeresWriter writer, CeresBase.IO.ICeresFile file)
        {
            base.AddSpecificInformation(writer, file);
            writer.AddAttribute(file, this.VarName, "SpikeFrequency", this.frequency);
            writer.AddAttribute(file, this.VarName, "SpikeType", (int)this.spikeType);
        }

        public override void ReadSpecificInformation(CeresBase.IO.ICeresReader reader, CeresBase.IO.ICeresFile file)
        {
            base.ReadSpecificInformation(reader, file);
            this.frequency = (double)reader.ReadAttribute(file, this.VarName, "SpikeFrequency");
            this.spikeType = (SpikeType)reader.ReadAttribute(file, this.VarName, "SpikeType");
        }

        public override CeresBase.IO.DataBase.FileDataBaseVariableEntryParameter GetDataBaseParameter(CeresBase.IO.ICeresReader reader, CeresBase.IO.ICeresFile file)
        {
            SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
            para.Frequency.Value = (double)reader.ReadAttribute(file, this.VarName, "SpikeFrequency");
            para.SpikeType.Value = (SpikeType)reader.ReadAttribute(file, this.VarName, "SpikeType");

            para.SpikeType.IsFixed = true;
            para.Frequency.IsFixed = true;


            return para;
        }

        public override IDataShape CreateShape()
        {
            return new TimeSeriesShape(this.frequency);
        }
    }
}
