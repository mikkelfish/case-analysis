using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;
using CeresBase.Data;
using CeresBase.IO;
using CeresBase.IO.NCDF;
using CeresBase.Projects.Flowable;
using CeresBase.Data.Interfaces;
using System.Linq;

namespace ApolloFinal
{


    public class SpikeVariable : Variable, IBatchWritable, ITimeSeries
    {
        //public static float[][] ReadDataFromMultipleVars(SpikeVariable[] vars, float[] startTimes, float[] endTimes)
        //{
        //    float[][] toRet = new float[vars.Length][];

        //    for (int i = 0; i < vars.Length; i++)
        //    {
        //        SpikeVariable var = vars[i];

        //        int firstIndex = (int)(startTimes[i] / var.Resolution);
        //        int lastIndex = (int)(endTimes[i] / var.Resolution);
        //        List<float> toPass = new List<float>();

        //        var[0].Pool.GetData(new int[] { firstIndex }, new int[] { lastIndex - firstIndex },
        //            delegate(float[] data, uint[] indices, uint[] counts)
        //            {
        //                toPass.AddRange(data);
        //            }
        //        );

        //        toRet[i] = toPass.ToArray();
        //    }

        //    return toRet;
        //}

        //public static float[] ReadData(SpikeVariable var, float startTime, float endTime)
        //{
        //        int firstIndex = (int)(startTime / var.Resolution);
        //        int lastIndex = (int)(endTime / var.Resolution);
        //        List<float> toPass = new List<float>();

        //        var[0].Pool.GetData(new int[] { firstIndex }, new int[] { lastIndex - firstIndex },
        //            delegate(float[] data, uint[] indices, uint[] counts)
        //            {
        //                toPass.AddRange(data);
        //            }
        //        );

        //        return toPass.ToArray();
        //}

        //private List<HotSpot> hotSpots = new List<HotSpot>();
        //public List<HotSpot> HotSpots
        //{
        //    get { return hotSpots; }
        //}

        public float[] GetTimeSeriesData(double start, double end)
        {
            if (this.SpikeType != SpikeVariableHeader.SpikeType.Pulse)
                return this[0].GetContiguousData(start, end);

            List<float> toRet = new List<float>();
            float[] all = this[0].GetAllData().Select(d => d * (float)this.Resolution).ToArray();
            foreach (float val in all)
            {
                if (val >= start && val <= end)
                {
                    toRet.Add(val);
                }
            }

            return toRet.ToArray();
        }

        public double TotalLength
        {
            get
            {
                DataFrame frame = this.GetFrame(0);
                if (frame == null) return 0;
                if (frame.Pool == null) return 0;
                if (this.SpikeType == SpikeVariableHeader.SpikeType.Raw || this.SpikeType == SpikeVariableHeader.SpikeType.Integrated)
                    return frame.Pool.DimensionLengths[0] * (this.Header as SpikeVariableHeader).Resolution;
                else
                    return frame.Pool.GetDataValue(new int[] { frame.Pool.DimensionLengths[0] - 1 }) *
                        (this.Header as SpikeVariableHeader).Resolution;
            }
        }

        public SpikeVariable(SpikeVariableHeader header)
            : base(header)
        {

        }


        /// <summary>
        /// DO NOT USE. ONLY FOR QUERYING
        /// </summary>
        private SpikeVariable()
            : base()
        {

        }


	

        public double Resolution
        {
            get
            {
                return (this.Header as SpikeVariableHeader).Resolution;
            }
        }

        public SpikeVariableHeader.SpikeType SpikeType
        {
            get
            {
                return (this.Header as SpikeVariableHeader).SpikeDesc;
            }
        }

        public override void AddSpecificInformation(ICeresWriter writer, ICeresFile file)
        {
            //string hotSpotInfo = "";
            //foreach (HotSpot spot in this.hotSpots)
            //{
            //    hotSpotInfo += spot.Name + "&&&" + spot.Description + "&&&" + spot.BeginTime.ToString() + "&&&" + spot.EndTime.ToString() + "|||";
            //}

            //writer.AddAttribute(file, this.Header.VarName, "HotSpotInfo", hotSpotInfo);
        }

        public override void ReadSpecificInformation(ICeresReader reader, ICeresFile file)
        {
            //string hotSpotString = (string)reader.ReadAttribute(file, this.Header.VarName, "HotSpotInfo");
            //if (hotSpotString != null)
            //{
            //    string[] hotSpots = hotSpotString.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries);
            //    foreach (string hotSpot in hotSpots)
            //    {
            //        string[] hotSplit = hotSpot.Split(new string[] { "&&&" }, StringSplitOptions.RemoveEmptyEntries);
            //        if(hotSplit.Length == 3)
            //            this.hotSpots.Add(new HotSpot(this.Header.ExperimentName, hotSplit[0], hotSplit[0], float.Parse(hotSplit[1]), float.Parse(hotSplit[2])));
            //        else
            //            this.hotSpots.Add(new HotSpot(this.Header.ExperimentName, hotSplit[0], hotSplit[1], float.Parse(hotSplit[2]), float.Parse(hotSplit[3])));

            //    }
            //}
        }

        public override void OnExit()
        {
            base.OnExit();
            //if (this.Header.FileName != null)
            //{
            //    NetCDFReader read = new NetCDFReader();
            //    if (!read.Supported(this.Header.FileName)) return;
            //    NetCDFWriter writer = new NetCDFWriter();
            //    string hotSpotInfo = "";
            //    foreach (HotSpot spot in this.hotSpots)
            //    {
            //        hotSpotInfo += spot.Name + "&&&" + spot.Description + "&&&" + spot.BeginTime.ToString() + "&&&" + spot.EndTime.ToString() + "|||";
            //    }

            //    writer.AddAttribute(this.Header.FileName, this.Header.VarName, "HotSpotInfo", hotSpotInfo);
            //}
        }

        public override Type HeaderType
        {
            get
            {
                return typeof(SpikeVariableHeader);
            }
        }

        #region IBatchWritable Members

        public IBatchWritable[] WriteToDisk(IBatchWritable[] toWrite)
        {
            List<SpikeVariable> vars = new List<SpikeVariable>();
            List<IBatchWritable> skipped = new List<IBatchWritable>();
            foreach (IBatchWritable write in toWrite)
            {
                if (write is SpikeVariable) vars.Add(write as SpikeVariable);
                else skipped.Add(write);
            }
            CeresBase.IO.IOUtilities.AddVariableToDisk("Save variables to Disk?", 
                vars.ToArray());
            return skipped.ToArray();
        }

        #endregion
    }
}
