using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using CeresBase.UI;
using CeresBase.Settings;
//using Razor.Configuration;

namespace ApolloFinal.IO
{
    //[CeresBase.Settings.XMLSetting("DataMaxFile", "", true, false)]
    public class DataMaxFile : ICeresFile
    {
        private static Dictionary<string, int> numChannelsDict =
            CeresBase.IO.Serialization.NewSerializationCentral.RegisterCollection<Dictionary<string, int>>(typeof(DataMaxFile), "numChannelsDict");

        private bool isAbf;
        public bool IsABF
        {
            get
            {
                return this.isAbf;
            }
        }

        private string fileName;
        public string FilePath
        {
            get
            {
                return this.fileName;
            }
        }

        internal void setVariableNameForChan(string varName)
        {
            this.variableNames[0] = varName;
        }

        private string[] variableNames;
        public string[] VariableNames
        {
            get 
            {
                return this.variableNames;
            }
        }

        public DateTime[] Times
        {
            get { return null; }
        }

        public static bool TestABF(string filename)
        {
            return filename.Contains(".abf");
        }

        public DataMaxFile(string filename)
        {
            if (filename.Contains(".abf")) this.isAbf = true;
            else this.isAbf = false;

            this.fileName = filename;

            if (!this.isAbf) this.variableNames = new string[] { "Chan 1" };
            else
            {
                int numChannels = 16;
                //XMLSettingAttribute attr = CeresBase.Settings.SettingsUtilities.GetXMLSettingAttr(typeof(DataMaxFile));
                
                //if (!attr.Category.Options.Contains(fileName))
                if(!numChannelsDict.ContainsKey(filename))
                {
                    RequestInformation info = new RequestInformation();
                    info.Grid.SetInformationToCollect(new Type[] { typeof(int) }, new string[] { "Cygnus" }, new string[] { "Number Channels" },
                        new string[] { "Enter the number of channels in the file." }, null, null);
                    if (info.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
                    {
                        //numChannels = (int)info.Grid.Results["Number Channels"];
                        //XmlConfigurationOption opt =  attr.Category.Options[filename, true, numChannels];
                        numChannelsDict.Add(filename, numChannels);
                    }

                }
                else
                {
                    //XmlConfigurationOption opt = attr.Category.Options[filename];
                    //numChannels = (int)opt.Value;
                    numChannels = numChannelsDict[filename];
                }


                string[] strings = new string[numChannels];
                for (int i = 1; i <= numChannels; i++)
                {
                    strings[i-1] = "Chan " + i.ToString();
                }
                this.variableNames = strings;
            }

        }

        #region ICeresFile Members


        public Type VariableType
        {
            get { return typeof(SpikeVariable); }
        }

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape); }
        }

        #endregion
    }
}
