using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;
using CeresBase.IO;
using CeresBase.Data;
using System.IO;
using ApolloFinal.IO.DataMax;
using CeresBase.IO.DataBase;
using System.Linq;

namespace ApolloFinal.IO
{
    public class DataMaxReader : ICeresReader   
    {
        //private CeresFileDataBase database;
        
        public object ReadAttribute(ICeresFile file, string name, string attrName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresBase.Data.Variable ReadVariable(string filename, VariableHeader header, DateTime[] times)
        //{
        //    return this.ReadVariables(filename, new VariableHeader[] { header }, new DateTime[][] { times })[0];
        //}

        private void readVariableHelper(byte[] buffer, int size, object state)
        {
            object[] info = state as object[];
            CeresBase.Data.DataPools.DataPoolNetCDF[] pools =
                info[0] as CeresBase.Data.DataPools.DataPoolNetCDF[];
            bool[] map = info[1] as bool[];
            DataMaxFile file = info[2] as DataMaxFile;
            float[] mins = info[3] as float[];
            float[] maxes = info[4] as float[];

            int totalVarsToWrite = 0;
            foreach (bool b in map) totalVarsToWrite++;
            byte[][] channelBuffer = new byte[totalVarsToWrite][];
            for (int i = 0; i < totalVarsToWrite; i++)
            {
                channelBuffer[i] = new byte[(size*2) / totalVarsToWrite];
            }
            
            int chanSpot = 0;
            int spot = 0;

            if (size != buffer.Length)
            {
                int s = 0;
            }

            int numChans = 16;
            int len = size / numChans / 2;
            for (int i = 0; i < len; i++, chanSpot += 4)
            {
                int location = 0;
                for (int j = 0; j < numChans; j++, spot += 2)
                {
                    if (!map[j]) continue;

                    //They do something weird with the data max data
                    float val = ((sbyte)buffer[spot] + ((sbyte)buffer[spot + 1]) * 256);
                    if (val == short.MinValue) val = short.MaxValue;

                    unsafe
                    {
                        byte* valP = (byte*)&val;
                        channelBuffer[location][chanSpot] = *valP;
                        channelBuffer[location][chanSpot+1] = *(valP+1);
                        channelBuffer[location][chanSpot+2] = *(valP+2);
                        channelBuffer[location][chanSpot+3] = *(valP+3);
                    }
                    if (val > maxes[location]) maxes[location] = val;
                    if (val < mins[location]) mins[location] = val;
                    location++;
                }
            }

            int loc = 0;
            for (int i = 0; i < numChans; i++)
            {
                if (!map[i]) continue;
                using (MemoryStream memStr = new MemoryStream(channelBuffer[loc]))
                {
                    pools[loc].SetData(memStr);
                }
                loc++;
            }

            info[5] =  ((int)info[5]) + len;
        }

        private int mapAbf(int fileChannel)
        {
            if (fileChannel == 1) return 1;
            if (fileChannel == 5) return 2;
            if (fileChannel == 9) return 3;
            if (fileChannel == 13) return 4;
            if (fileChannel == 3) return 5;
            if (fileChannel == 7) return 6;
            if (fileChannel == 11) return 7;
            if (fileChannel == 15) return 8;
            if (fileChannel == 2) return 9;
            if (fileChannel == 6) return 10;
            if (fileChannel == 10) return 11;
            if (fileChannel == 14) return 12;
            if (fileChannel == 4) return 13;
            if (fileChannel == 8) return 14;
            if (fileChannel == 12) return 15;
            return 16;
        }

        private int mapBack(int diskChannel)
        {
            if (diskChannel == 1) return 1;
            if (diskChannel == 2) return 5;
            if (diskChannel == 3) return 9;
            if (diskChannel == 4) return 13;
            if (diskChannel == 5) return 3;
            if (diskChannel == 6) return 7;
            if (diskChannel == 7) return 11;
            if (diskChannel == 8) return 15;
            if (diskChannel == 9) return 2;
            if (diskChannel == 10) return 6;
            if (diskChannel == 11) return 10;
            if (diskChannel == 12) return 14;
            if (diskChannel == 13) return 4;
            if (diskChannel == 14) return 8;
            if (diskChannel == 15) return 12;
            return 16;
        }

        //[CeresCreated("Mikkel", "03/20/06")]
        //[CeresToDo("Mikkel", "03/20/06", Comments = "Get Stream from factory", Priority=DevelopmentPriority.Critical)]
        //public Variable[] ReadVariables(string filename, VariableHeader[] headers, DateTime[][] times)
        //{
        //    string experimentName = headers[0].ExperimentName;
        //    DataMaxFile file = new DataMaxFile(filename);

        //    IDataPool[] pools = null;

        //    if (!file.IsABF)
        //    {
        //        pools = new DataMaxDataPool[1];
        //        pools[0] = new DataMaxDataPool(file, (headers[0] as SpikeVariableHeader).Frequency);
        //    }


        //    string[] variableNames = new string[headers.Length];
        //    for (int i = 0; i < variableNames.Length; i++)
        //    {
        //        variableNames[i] = headers[i].VarName;
        //    }

        //    if (file.IsABF)
        //    {
        //        int numChans = 16;


        //        int bufferSize = 2400000 - (2400000 % numChans);

        //        FileStream stream = new FileStream(file.FilePath, FileMode.Open, FileAccess.Read);
        //        BinaryReader reader = new BinaryReader(stream);
        //        stream.Seek(2048, SeekOrigin.Begin);


        //        int length = (int)((stream.Length-2048) / (numChans * 2));

        //        //Todo get from factory
        //        pools =
        //            new CeresBase.Data.DataPools.DataPoolNetCDF[headers.Length];

        //        bool[] map = new bool[numChans];


        //        float[] mins = new float[pools.Length];
        //        float[] maxs = new float[pools.Length];

        //        for (int i = 0; i < pools.Length; i++)
        //        {
        //            pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { length });
        //            string[] split = variableNames[i].Split(' ');
        //            int block = int.Parse(split[1]) + 1;
        //            block = this.mapAbf(block);
        //            map[block - 1] = true;

        //            mins[i] = float.MaxValue;
        //            maxs[i] = float.MinValue;
        //        }

        //        int count = 0;

        //        object[] topass = new object[] { pools, map, file, mins, maxs, count };
        //        CeresBase.General.Utilities.ReadFromStream(reader, -1, bufferSize, this.readVariableHelper,
        //            topass);

        //        for (int i = 0; i < pools.Length; i++)
        //        {
        //            pools[i].SetMinMaxManually(mins[i], maxs[i]);
        //            pools[i].DoneSettingData();
        //        }

        //        reader.Close();

        //        int[] savedSpots = new int[headers.Length];
        //        int curSpot = 0;
        //        for (int i = 0; i < map.Length; i++)
        //        {
        //            if (map[i])
        //            {
        //                savedSpots[curSpot] = this.mapBack(i + 1);
        //                curSpot++;
        //            }
        //        }

        //        IDataPool[] realPools = new IDataPool[pools.Length];
        //        for (int i = 0; i < headers.Length; i++)
        //        {
        //            string[] split = variableNames[i].Split(' ');
        //            int block = int.Parse(split[1]) + 1;
        //            for (int j = 0; j < savedSpots.Length; j++)
        //            {
        //                if (block == savedSpots[j])
        //                {
        //                    realPools[i] = pools[j];
        //                    break;
        //                }
        //            }
        //        }

        //        pools = realPools;

        //        //stream.Close();

        //    }

        //    SpikeVariable[] variables = new SpikeVariable[variableNames.Length];
        //    for (int i = 0; i < variables.Length; i++)                
        //    {
        //        variables[i] = new SpikeVariable((SpikeVariableHeader)headers[i]);
        //        variables[i].AddDataFrame(new DataFrame(variables[i], pools[i], 
        //            new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless));
        //    }

        //    return variables;
        //}

        public byte[] ReadBinary(ICeresFile file, string name)
        {
            throw new Exception("The method or operation is not implemented.");            
        }

        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}

        public double[][] GetShapeValues(ICeresFile file)
        {
            return null;
        }

        //public VariableHeader[] GetAllVariableHeaders(string fileName, string experimentName)
        //{
        //    DataMaxFile file = new DataMaxFile(fileName);
        //    int freq = 0;
        //    VariableHeader[] headers = new VariableHeader[file.VariableNames.Length];
        //    Dictionary<string, object> settings = this.database.GetSettings(fileName);
        //    freq = (int)settings["Frequency"];

        //    if (!file.IsABF)
        //    {
        //        //TODO CHANGE
        //        if (settings != null) file.setVariableNameForChan((string)settings["VariableName"]);
        //        for (int i = 0; i < file.VariableNames.Length; i++)
        //        {
        //            headers[i] = new SpikeVariableHeader(file.VariableNames[i],
        //                "Raw", experimentName, (double)freq, SpikeVariableHeader.SpikeType.Raw);
        //        }
                
        //    }
        //    else
        //    {
        //        string varNames = (string)settings["VariableName"];
        //        string[] varSplits = varNames.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
        //        for (int i = 0; i < varSplits.Length; i++)
        //        {
        //            headers[i] = new SpikeVariableHeader("Chan " + i.ToString(),
        //                "Raw", experimentName , (double)freq, SpikeVariableHeader.SpikeType.Raw);
        //            headers[i].Description = varSplits[i];
        //        }
        //    }
        //    return headers;
        //}

        public bool Supported(string file)
        {
            int index = file.LastIndexOf('.');
            if (index < 0) return false;
            if (file.Substring(index + 1).ToLower() == "abf" || file.Substring(index + 1).ToLower() == "chan") return true;
            return false;
        }

        //public void AddFilesToDB(string[] files)
        //{
        //    CeresBase.UI.RequestInformation infoReq = new CeresBase.UI.RequestInformation();
        //    List<Type> types = new List<Type>();
        //    List<string> names = new List<string>();
        //    List<string> descriptions = new List<string>();

        //    types.Add(typeof(string));
        //    names.Add("Experiment Name");
        //    descriptions.Add("Please enter the name of the experiment this data is from.");


        //    types.Add(typeof(int));
        //    names.Add("Frequency");
        //    descriptions.Add("Please enter the frequency of the file");

        //    DataMaxFile[] dataFiles = new DataMaxFile[files.Length];
        //    for (int i = 0; i < files.Length; i++)
        //    {
        //        dataFiles[i] = new DataMaxFile(files[i]);

        //        if (dataFiles[i].IsABF)
        //        {
        //            for (int j = 0; j < dataFiles[i].VariableNames.Length; j++)
        //            {
        //                types.Add(typeof(string));
        //                names.Add("Channel Number" + (j < 9 ? "0":"") + (j + 1).ToString());
        //                descriptions.Add("Please enter the channel description for file " + files[i].Substring(files[i].LastIndexOf('\\')) );
        //            }
        //        }
        //        else
        //        {
        //            types.Add(typeof(string));
        //            names.Add("Channel Number" + (i < 9 ? "0" : "") + (i + 1).ToString());
        //            descriptions.Add("Please enter the channel description for file " + files[i].Substring(files[i].LastIndexOf('\\')));
        //        }
        //    }
            
        //    infoReq.Grid.SetInformationToCollect(types.ToArray(), null, names.ToArray(), descriptions.ToArray(), null, null);
        //    if (infoReq.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
        //    {
        //        return;
        //    }
        //    string exptName = infoReq.Results["Experiment Name"] as string;
        //    int frequency = (int)infoReq.Results["Frequency"];

        //    for (int i = 0; i < dataFiles.Length; i++)
        //    {
        //        if (!dataFiles[i].IsABF)
        //        {
        //            int parsing;
        //            string chanName = (string)infoReq.Results["Channel Number" + (i < 9 ? "0" : "") + (i + 1).ToString()];
        //            if (int.TryParse(chanName, out parsing))
        //            {
        //                chanName = "Chan " + parsing;
        //            }

        //            dataFiles[i].setVariableNameForChan(chanName.ToString());
        //            this.database.SetSettings(files[i], new string[] { "VariableName", "Frequency" }, new object[] { dataFiles[i].VariableNames[0], frequency });
        //        }
        //        else
        //        {
        //            string allChannels = "";
        //            for (int j = 0; j < dataFiles[i].VariableNames.Length; j++)
        //            {
        //                int parsing;
        //                string chanName = (string)infoReq.Results["Channel Number" + (j < 9 ? "0" : "") + (j + 1).ToString()];
        //                if (int.TryParse(chanName, out parsing))
        //                {
        //                    chanName = "Chan " + parsing;
        //                }

        //                allChannels += chanName  + "&&";
        //            }
        //            this.database.SetSettings(files[i], new string[] { "VariableName", "Frequency" }, new object[] { allChannels, frequency });
        //        }


        //        this.Database.CreateFile(exptName, dataFiles[i]);
        //    }
        //}

        #region ICeresReader Members


        public bool StayNative
        {
            get { return true; }
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            List<FileDataBaseEntryParameter> paras = new List<FileDataBaseEntryParameter>();

            FileDataBaseEntryParameter frequency = new FileDataBaseEntryParameter() { Name = "Frequency", UserEditable = true };
            frequency.Value = 100.0;
            paras.Add(frequency);

            FileDataBaseEntryParameter numChannels = new FileDataBaseEntryParameter() { Name = "Number Channels", UserEditable = true };

            if (DataMaxFile.TestABF(entry.Filepath))
            {
                numChannels.Value = 16;
            }
            else numChannels.Value = 1;

            paras.Add(numChannels);
            return paras.ToArray();
        }

        #endregion

        #region ICeresReader Members


        public FileDataBaseVariableEntryParameter[] GetVariableParameters(FileDataBaseEntry entry)
        {

            int numChannels = (int)entry.Parameters.Single(p=>p.Name=="Number Channels").Value;
            double freq = (double)entry.Parameters.Single(p => p.Name == "Frequency").Value;
            SpikeVariableEntryParameter[] paras = new SpikeVariableEntryParameter[numChannels];
            for (int i = 0; i < numChannels; i++)
            {
                SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();

                para.VariableName.Value = "Var " + (i + 1).ToString();

                para.Units.Value = "Raw";
                para.Frequency.Value = freq;
                para.Frequency.IsFixed = true;
                para.SpikeType.Value = SpikeVariableHeader.SpikeType.Raw;
                para.SpikeType.IsFixed = true;
                paras[i] = para;
            }
            return paras;
        }

        #endregion

 

        #region ICeresReader Members


        public Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
