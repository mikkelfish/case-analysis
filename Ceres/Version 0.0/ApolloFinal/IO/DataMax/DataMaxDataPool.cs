using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using System.IO;

namespace ApolloFinal.IO.DataMax
{
    class DataMaxDataPool : IDataPool
    {

        private float min;
        private float max;
        private double frequency;
        private DataMaxFile file;
        private FileStream stream;
        private bool minMaxFound = false;
        private bool beenSet = false;

        public DataMaxDataPool(DataMaxFile file, double frequency)
        {
            this.file = file;
            this.stream = new FileStream(file.FilePath, FileMode.Open, FileAccess.Read);
            this.frequency = frequency;            
        }

        #region IDataPool Members

        public void FrameAdded()
        {
            
        }

        public void FrameRemoved()
        {
        }

        public void SetMinMaxManually(float min, float max)
        {
            this.min = min;
            this.max = max;
            this.minMaxFound = true;
        }

        public CompressionChain Chain
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public void PopCompression()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Type CurrentStorageType
        {
            get { return typeof(float); }
        }

        public float GetDataValue()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public float GetDataValue(int[] indices)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        private void callback(byte[] data, int count, object tag)
        {
            object[] args = tag as object[];
            DataUtilities.PoolReadDelegate readFunction = args[0] as DataUtilities.PoolReadDelegate;
            int index = (int)args[1];


            float[] fdata = new float[count/2];
            for (int i = 0; i < count; i+=2)
            {
               fdata[i/2] = data[i] + (sbyte)data[i + 1] * 256;              
            }

            readFunction(fdata, new uint[] { (uint)index }, new uint[] { (uint)fdata.Length });
            args[1] = index + count / 2;
        }

        public void GetData(int[] indices, int[] amount, DataUtilities.PoolReadDelegate readFunction)
        {
            int index = 0;
            int am = this.DimensionLengths[0];


            if (indices != null)
            {
                index = indices[0];
            }

            if (amount != null)
            {
                am = amount[0];
            }

            this.stream.Seek((long)(index*2), SeekOrigin.Begin);


            CeresBase.General.Utilities.ReadFromStream(this.stream, am * 2, 960000,
                this.callback, new object[] { readFunction, index });
        }

        private void stridedCallback(byte[] data, int count, object tag)
        {
            object[] args = tag as object[];
            DataUtilities.PoolReadStridedDelegate readFunction = args[0] as DataUtilities.PoolReadStridedDelegate;
            int index = (int)args[1];
            int lastIndex = index + count;
            int stride = (int)args[2];

            int current = 0;
            int amount = (int)Math.Ceiling((double)(count/2)/ stride);
            float[] fdata = new float[amount];
            for (int i = 0; i < count; i += (stride * 2))
            {
                fdata[current] = data[i] + (sbyte)data[i + 1] * 256;
                current++;
            }
            
            readFunction(fdata, new uint[] { (uint)index }, new uint[] { (uint)lastIndex }, 
                new uint[] { (uint)stride });

            (tag as object[])[1] = index + (count/2);
        }

        public void GetData(int[] firstIndices, int[] lastIndices, int[] strides, 
            DataUtilities.PoolReadStridedDelegate readFunction)
        {
            this.stream.Seek((long)(firstIndices[0]*2), SeekOrigin.Begin);
            int index = firstIndices[0];
            int am = lastIndices[0] - index;
            CeresBase.General.Utilities.ReadFromStream(this.stream, am * 2, 960000,
                this.stridedCallback, new object[]{readFunction, firstIndices[0],
                strides[0]});
            
        }

        public void SetDataValue(float val)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetDataValue(float val, int[] indices)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetData(System.IO.Stream data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetData(System.IO.Stream data, int[] indices, int[] amounts)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetData(System.IO.Stream data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetDimensionLength(int index)
        {
            return (int)(this.stream.Length/2);
        }

        public int[] DimensionLengths
        {
            get { return new int[] { (int)(this.stream.Length / 2) }; }
        }

        public float Min
        {
            get {return min; }
        }

        public float Max
        {
            get { return max; }
        }

        public int NumDimensions
        {
            get { return 1; }
        }

        public void DoneSettingData()
        {
            this.beenSet = true;
        }

        public bool BeenSet
        {
            get 
            {
                return this.beenSet;
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        public void CalcMinMax()
        {
            this.min = float.MaxValue;
            this.max = float.MinValue;
            CeresBase.General.Utilities.ReadFromStream(this.stream, -1, 24000,
                delegate(byte[] data, int count, object tag)
                {
                    for (int i = 0; i < count; i += 2)
                    {
                        float val = data[i] + ((sbyte)data[i + 1]) * 256;
                        if (val < this.min)
                        {
                            this.min = val;
                        }

                        if (val > this.max)
                        {
                            this.max = val;
                        }
                    }
                },
                null);
        }

        public bool MinMaxFound
        {
            get { return this.minMaxFound; }
        }
    }
}
