using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using CeresBase.Data;
using System.IO;

namespace ApolloFinal.IO.DataMax
{
    class DataMaxWriter : ICeresWriter
    {
        //private CeresFileDataBase database;

        public void AddAttribute(ICeresFile file, string name, string attrName, object value)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override string ToString()
        {
            return "Cygnus";
        }

        private ICeresFile writeFile(Variable var, string filepath)
        {
            FileStream stream = new FileStream(filepath, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(stream);

            //TODO THINK ABOUT FREQUENCY STUFF?
            DataFrame frame = var[0];

            double posScale = (frame.Max) / short.MaxValue;
            double negScale = (frame.Min) / short.MinValue;
            
            
         //   float scale = ((float)(frame.Max - frame.Min) / (short.MaxValue - short.MinValue));
         //   float min = (float)frame.Min;
            frame.Pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    ////They do something weird with the data max data
                    //float val = ((sbyte)buffer[spot] + ((sbyte)buffer[spot + 1]) * 256);
                    //if (val == short.MinValue) val = short.MaxValue;
                    byte[] toWrite = new byte[data.Length * 2];
                    for (int i = 0; i < toWrite.Length; i += 2)
                    {
                        short val = 0;
                        if (data[i / 2] > 0)
                        {
                            val = (short)Math.Round((data[i/2] /posScale), 0);
                        }
                        else
                        {
                            val = (short)Math.Round((data[i / 2] / negScale),0);
                        }


//                        (short)(((data[i / 2] - min) / scale) + short.MinValue);
                        int realVal = val;
                        if (realVal < 0) realVal += (ushort.MaxValue + 1);
                        toWrite[i] = (byte)(realVal % 256);
                        toWrite[i + 1] = (byte)(realVal / 256);
                    }
                    writer.Write(toWrite);
                }
            );

            writer.Close();
            return new DataMaxFile(filepath);
        }

        public ICeresFile[] WriteVariable(Variable variable, string writeToDirectory)
        {
            if (writeToDirectory[writeToDirectory.Length - 1] != '\\')
                writeToDirectory += '\\';
            string filePath = writeToDirectory + "EXPT_" + 
                variable.Header.ExperimentName + "CHAN_" + variable.Header.VarName +  ".chan";
                return new ICeresFile[] { this.writeFile(variable, filePath) };
        }


        public void WriteBinary(ICeresFile file, string name, byte[] data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}

        public void Dispose()
        {
            throw new Exception("The method or operation is not implemented.");
        }


        #region ICeresWriter Members


        public ICeresFile[] WriteVariables(Variable[] variables, string writeToDirectory)
        {
            List<ICeresFile> files = new List<ICeresFile>();
            foreach (Variable var in variables)
            {
                files.AddRange(this.WriteVariable(var, writeToDirectory));
            }
            return files.ToArray();
        }

        #endregion

        #region ICeresWriter Members


        public void AddAttribute(string fileName, string name, string attrName, object value)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
