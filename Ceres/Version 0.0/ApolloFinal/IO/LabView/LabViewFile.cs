using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using System.IO;
using System.Xml;
//using Razor.Configuration;

namespace ApolloFinal.IO.LabView
{
    //[CeresBase.Settings.XMLSetting("Labview", "", true, false)]
    public class LabViewFile : ICeresFile
    {
        private static Dictionary<string, string> fileSetup =
            CeresBase.IO.Serialization.NewSerializationCentral.RegisterCollection<Dictionary<string, string>>(typeof(LabViewFile), "fileSetup");
       
        private int numberChannels;
        public int NumberChannels
        {
            get { return numberChannels; }
        }

        private double frequency;
        public double Frequency
        {
            get { return frequency; }
        }	

        private string filepath;


        private int numPoints;
        public int NumberDataPoints
        {
            get { return numPoints; }
        }

        private int numChars;
        public int NumChars
        {
            get { return numChars; }
        }

        private string[] channelNames;
        public string[] ChannelNames
        {
            get { return channelNames; }
        }

        private List<string> hotSpotNames = new List<string>();
        public List<string> HotSpotNames
        {
            get { return hotSpotNames; }
        }
	    
        private List<float> hotSpotTimes = new List<float>();
	public List<float> HotSpotTimes
	{
	  get { return hotSpotTimes;}
	}

        public void LoadLineCountAndHotspots()
        {
            if (this.filepath == null || this.numPoints != -1) 
                return;

            using (FileStream stream = new FileStream(this.filepath, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    object[] numLines = new object[] { (int)0 };
                    stream.Seek(0, SeekOrigin.Begin);
                    CeresBase.General.Utilities.ReadFromStream(stream, numChars, 250000, new CeresBase.General.Utilities.StreamAction(this.readLines),
                        numLines);

                    stream.Seek(numChars, SeekOrigin.Begin);
                    List<string> lines = new List<string>();
                    while (true)
                    {
                        string line = reader.ReadLine();
                        if (line == null) break;
                        lines.Add(line);
                    }


                    this.numPoints = (int)numLines[0];

                    string hotSpotInfo = "";
                    for (int i = 0; i < lines.Count - 1; i++)
                    {
                        string[] hotSplits = lines[i].Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                        if (hotSplits.Length != 4) continue;
                        hotSpotInfo += hotSplits[2] + "%%" + hotSplits[3] + "&&";
                        this.hotSpotNames.Add(hotSplits[3]);
                        this.hotSpotTimes.Add(float.Parse(hotSplits[2]));
                        //this.hotSpots.Add(hotSplits[3], float.Parse(hotSplits[2]));
                    }

                    //CeresBase.Settings.XMLSettingAttribute attr =
                    //    CeresBase.Settings.SettingsUtilities.GetXMLSettingAttr(typeof(LabViewFile));



                    string chanNames = this.getChanNames();

                    if (!fileSetup.ContainsKey(filepath))
                    {
                        fileSetup.Add(filepath, this.numChars + "||" + this.numPoints + "||" + this.frequency + "||" + this.numberChannels +
                        "||" + chanNames + "||" + hotSpotInfo);
                    }
                    else fileSetup[filepath] = this.numChars + "||" + this.numPoints + "||" + this.frequency + "||" + this.numberChannels +
                        "||" + chanNames + "||" + hotSpotInfo;

                    //XmlConfigurationOption opt =
                    //    attr.Category.Options[filepath, true];
                    //opt.Value = this.numChars + "||" + this.numPoints + "||" + this.frequency + "||" + this.numberChannels +
                    //    "||" + chanNames + "||" + hotSpotInfo;
                    //attr.WriteToDisk();
                }
            }


            
        }

        private void readLines(byte[] data, int count, object tag)
        {
            int lines = (int)(tag as object[])[0];
            for (int i = 0; i < count; i++)
            {
                if (data[i] == (byte)'\n')
                {
                    lines++;
                }
            }
            (tag as object[])[0] = lines;
        }

        private string getChanNames()
        {
            string chanNames = "";
            for (int i = 0; i < this.channelNames.Length; i++)
            {
               chanNames += this.channelNames[i] + "&&";
            }

            return chanNames;
        }

        public void LoadInitialInformation()
        {
            using (FileStream stream = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            {
                stream.Seek(-1000, SeekOrigin.End);
                char[] buffer = new char[1000];

                using (StreamReader reader = new StreamReader(stream))
                {
                    reader.ReadBlock(buffer, 0, buffer.Length);
                    string test = new string(buffer, 0, buffer.Length);

                    int numRead = 0;
                    char[] tempBuff = new char[256];
                    int lastBuff = 0;
                    for (int i = 0; i < test.Length; i++)
                    {
                        if (char.IsWhiteSpace(test[i]))
                        {
                            string toTest = new string(tempBuff, 0, lastBuff);
                            toTest = toTest.Trim();
                            if (toTest.Length != 0 && !toTest.Contains("."))
                                break;
                            numRead += lastBuff + 1;
                            lastBuff = 0;
                        }
                        else
                        {
                            tempBuff[lastBuff] = test[i];
                            lastBuff++;
                        }
                    }


                    string testing = test.Substring(numRead);


                    this.numChars = (int)stream.Length - testing.Length;

                    string trimmed = test.Trim();
                    string lastLine = trimmed.Substring(trimmed.LastIndexOf('\n')).Trim();
                    
                    string[] splits = lastLine.Split('\t');
                    this.frequency = double.Parse(splits[0].Trim());

                    //Get the channel names

                    int numChannels = int.Parse(splits[splits.Length - 2]);

                    this.channelNames = new string[numChannels];
                    for (int i = 0; i < this.channelNames.Length; i++)
                    {
                        this.channelNames[i] = splits[i + 1].Trim();
                    }

                    string chanNames = this.getChanNames();
                    this.numberChannels = int.Parse(splits[splits.Length - 2]);

                    //CeresBase.Settings.XMLSettingAttribute attr =
                    //CeresBase.Settings.SettingsUtilities.GetXMLSettingAttr(typeof(LabViewFile));
                    //XmlConfigurationOption opt =
                    //    attr.Category.Options[filepath, true, this.numChars + "||" + "-1" + "||" + this.frequency + "||" + this.numberChannels +
                    //    "||" + chanNames + "||" + ""];
                    //attr.WriteToDisk();

                    if (!fileSetup.ContainsKey(filepath))
                    {
                        fileSetup.Add(filepath, this.numChars + "||" + "-1" + "||" + this.frequency + "||" + this.numberChannels +
                            "||" + chanNames + "||" + "");
                    }
                }
            }
        }

        public LabViewFile(string filepath)
        {
            this.filepath = filepath;

            //CeresBase.Settings.XMLSettingAttribute attr =
            //    CeresBase.Settings.SettingsUtilities.GetXMLSettingAttr(typeof(LabViewFile));

            //if (attr.Category.Options.Contains(filepath))
            if(fileSetup.ContainsKey(filepath))
            {
                //XmlConfigurationOption opt = attr.Category.Options[filepath];
                //string toParse = (string)opt.Value;
                string toParse = fileSetup[filepath];
                
                string[] splits = toParse.Split(new string[]{"||"}, StringSplitOptions.RemoveEmptyEntries);
                this.numChars = int.Parse(splits[0]);
                this.numPoints = int.Parse(splits[1]);
                this.frequency = double.Parse(splits[2]);
                this.numberChannels = int.Parse(splits[3]);
                string chanNames = splits[4];
                string[] chanSplits = chanNames.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
                this.channelNames = new string[chanSplits.Length];
                for (int i = 0; i < chanSplits.Length; i++)
                {
                    this.channelNames[i] = chanSplits[i];
                }

                if (splits.Length > 5)
                {
                    string hotSpots = splits[5];
                    string[] hotSplit = hotSpots.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string split in hotSplit)
                    {
                        string[] splitSplit = split.Split(new string[] { "%%" }, StringSplitOptions.RemoveEmptyEntries);
                        this.hotSpotNames.Add(splitSplit[1]);
                        this.hotSpotTimes.Add(float.Parse(splitSplit[0]));
                        //this.hotSpots.Add(splitSplit[1], float.Parse(splitSplit[0]));
                    }
                }
            }
            else
            {
                this.LoadInitialInformation();               
            }
        }

        public Type VariableType
        {
            get { return typeof(SpikeVariable); }
        }

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape); }
        }

        public string FilePath
        {
            get { return this.filepath; }
        }
    }
}
