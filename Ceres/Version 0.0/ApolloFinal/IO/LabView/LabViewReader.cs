using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using CeresBase.Data;
using System.IO;
using ApolloFinal.IO.TextWriter;
using CeresBase.IO.DataBase;
using System.Linq;
using CeresBase.Projects;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Forms;
namespace ApolloFinal.IO.LabView
{
    class LabViewReader : ICeresReader
    {
        //private CeresFileDataBase database;

        public object ReadAttribute(ICeresFile file, string name, string attrName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresBase.Data.Variable ReadVariable(string filename, CeresBase.Data.VariableHeader header, DateTime[] times)
        //{
        //    return this.ReadVariables(filename, new VariableHeader[] { header }, null)[0];
        //}

        private void readData(byte[] data, int count, object tag)
        {
            object[] array = tag as object[];
            int curVar = (int)array[0];
            IDataPool[] pools = array[1] as IDataPool[];
            int[] readChannels = array[2] as int[];
            char[][] buffers = array[3] as char[][];
            int[] buffCount = array[4] as int[];

            float[][] dataBuffers = array[5] as float[][];
            int[] dataBuffCount = array[6] as int[];

            for (int i = 0; i < count; i++)
            {
                char character = (char)data[i];
                if(readChannels[curVar] == -1)
                {
                    if(character == '\t') curVar++;
                    if(character == '\n') curVar = 0;
                    continue;
                }


                if (character == '\t' || character == '\n')
                {
                    string toParse = new string(buffers[curVar], 0, buffCount[curVar]);

                    string[] remove = toParse.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    float val = 0;
                    try
                    {
                        val = float.Parse(remove[0]);
                    }
                    catch
                    {

                    }
                    
                    buffCount[curVar] = 0;

                    dataBuffers[curVar][dataBuffCount[curVar]] = val;
                    dataBuffCount[curVar]++;

                    if (dataBuffCount[curVar] == dataBuffers[curVar].Length)
                    {
                        pools[readChannels[curVar]].SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(dataBuffers[curVar]));
                        dataBuffCount[curVar] = 0;
                    }

                   // pools[readChannels[curVar]].SetDataValue(val);

                    if(character == '\n') curVar = 0;
                    else curVar++;

                    continue;
                }

                //if (char.IsWhiteSpace(character)) 
                //    continue;
                buffers[curVar][buffCount[curVar]] = character;
                buffCount[curVar]++;
            }

            array[0] = curVar;

        }

        //public CeresBase.Data.Variable[] ReadVariables(string filename, CeresBase.Data.VariableHeader[] headers, DateTime[][] times)
        //{
        //    LabViewFile lfile = new LabViewFile(filename);

        //    lfile.LoadLineCountAndHotspots();

        //    int[] readChannels = new int[lfile.NumberChannels];
        //    for (int i = 0; i < readChannels.Length; i++)
        //    {
        //        readChannels[i] = -1;
        //    }

        //    IDataPool[] pools = new IDataPool[readChannels.Length];
          
            
        //    for (int i = 0; i < headers.Length; i++)
        //    {
        //        int channel = int.Parse(headers[i].VarName.Substring(headers[i].VarName.LastIndexOf(' ') + 1));
        //        readChannels[channel] = i;
        //        pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { lfile.NumberDataPoints });
        //    }

        //    FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read);
        //    //StreamReader reader = new StreamReader(stream);

        //    char[][] buffers = new char[lfile.NumberChannels][];
        //    float[][] dataBuffers = new float[lfile.NumberChannels][];

        //    int[] buffCount = new int[lfile.NumberChannels];
        //    int[] dataBuffCount = new int[lfile.NumberChannels];
        //    for (int i = 0; i < buffers.Length; i++)
        //    {
        //        buffers[i] = new char[256];
        //        dataBuffers[i] = new float[2000000];
        //    }

         
        //    object[] tag = new object[] { (int)0, pools, readChannels, buffers, buffCount, dataBuffers, dataBuffCount };
        //    CeresBase.General.Utilities.ReadFromStream(stream, lfile.NumChars, 500000,
        //        readData, tag);

        //    for (int i = 0; i < lfile.NumberChannels; i++)
        //    {
        //        if (dataBuffCount[i] != 0)
        //        {
        //            float[] data = new float[dataBuffCount[i]];
        //            Array.Copy(dataBuffers[i], data, dataBuffCount[i]);
        //            pools[readChannels[i]].SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(data));
        //        }
        //    }


        //    //string line = reader.ReadLine();

        //    //for (int i = 0; i < lfile.NumberDataPoints; i++)
        //    //{
        //    //    string[] splits = line.Split('\t');
        //    //    for (int j = 0; j < readChannels.Length; j++)
        //    //    {
        //    //        pools[j].SetDataValue(float.Parse(splits[readChannels[j]]));
        //    //    }
        //    //    line = reader.ReadLine();

        //    //}

        //    stream.Close();

        //    Variable[] vars = new Variable[headers.Length];
        //    for (int i = 0; i < headers.Length; i++)
        //    {
        //        vars[i] = new SpikeVariable(headers[i] as SpikeVariableHeader);

        //        for (int j = 0; j < lfile.HotSpotNames.Count; j++)
        //        {
        //           // EpochCentral.Epochs.Add(new Epoch(vars[i], lfile.HotSpotNames[j], lfile.HotSpotTimes[j], lfile.HotSpotTimes[j]));

        //            EpochCentral.AddEpoch(new Epoch(vars[i], lfile.HotSpotNames[j], (float)Math.Round(lfile.HotSpotTimes[j], 2), (float)Math.Round(lfile.HotSpotTimes[j], 2)));
        //            //(vars[i] as SpikeVariable).HotSpots.Add(new HotSpot(headers[i].ExperimentName, lfile.HotSpotNames[j], "",
        //            //    lfile.HotSpotTimes[j], lfile.HotSpotTimes[j]));
        //        }

        //        DataFrame frame = new DataFrame(vars[i], pools[i], new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless);
        //        vars[i].AddDataFrame(frame);
        //    }
        //    return vars;
        //}

        public byte[] ReadBinary(ICeresFile file, string name)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}

        public double[][] GetShapeValues(ICeresFile file)
        {
            return null;
        }

        //public CeresBase.Data.VariableHeader[] GetAllVariableHeaders(string file, string experimentName)
        //{
        //    LabViewFile lfile = new LabViewFile(file);
        //    VariableHeader[] headers = new VariableHeader[lfile.NumberChannels];

        //    //Dictionary<string, object> settings = this.database.GetSettings(file);

        //    for (int i = 0; i < lfile.NumberChannels; i++)
        //    {
        //       // string channelName = (string)settings["Channel Name " + i.ToString()];
        //        headers[i] = new SpikeVariableHeader("Channel " + i.ToString(), "Raw", experimentName, lfile.Frequency,
        //            SpikeVariableHeader.SpikeType.Raw);
        //        headers[i].Description = lfile.ChannelNames[i];
        //    }

        //    return headers;
        //}

        public bool Supported(string file)
        {
            int index = file.LastIndexOf('.');
            if (index < 0) return false;
            return file.Substring(index) == ".dat";
        }

        //public void AddFilesToDB(string[] files)
        //{
        //    foreach (string file in files)
        //    {
        //        LabViewFile lfile = new LabViewFile(file);
               
        //        CeresBase.UI.RequestInformation infoReq = new CeresBase.UI.RequestInformation();
        //        List<Type> types = new List<Type>();
        //        List<string> names = new List<string>();
        //        List<string> descriptions = new List<string>();

        //        types.Add(typeof(string));
        //        names.Add("Experiment Name");
        //        descriptions.Add("Please enter the name of the experiment the file " + file + " is from.");

        //        //for (int i = 0; i < lfile.NumberChannels; i++)
        //        //{
        //        //    types.Add(typeof(string));
        //        //    names.Add("Channel Name " + i.ToString());
        //        //    descriptions.Add("Please enter the name of the channel");
        //        //}

        //        infoReq.Grid.SetInformationToCollect(types.ToArray(), null, names.ToArray(), descriptions.ToArray(),
        //            null, null);

        //        if (infoReq.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) continue;


        //        //List<object> settings = new List<object>();
        //        //foreach(string name in names)
        //        //{
        //        //    settings.Add(infoReq.Grid.Results[name]);
        //        //}

        //        //this.database.SetSettings(file, names.ToArray(), settings.ToArray());

                                
        //        this.Database.CreateFile(infoReq.Grid.Results["Experiment Name"] as string, lfile);
        //    }
        //}

        #region ICeresReader Members


        public bool StayNative
        {
            get { return false; }
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(FileDataBaseEntry entry)
        {
            LabViewFile lfile = new LabViewFile(entry.Filepath);
            lfile.LoadInitialInformation();

            //this.numChars = int.Parse(splits[0]);
            //this.numPoints = int.Parse(splits[1]);
            //this.frequency = double.Parse(splits[2]);
            //this.numberChannels = int.Parse(splits[3]);
            //this.channelNames = new string[chanSplits.Length];


            FileDataBaseEntryParameter numberChannels = new FileDataBaseEntryParameter() { Name = "NumberChannels", UserEditable = false, Value=lfile.NumberChannels, SuccessfullySet=true };
            FileDataBaseEntryParameter channelNames = new FileDataBaseEntryParameter() { Name = "ChannelNames", UserEditable = false, Value=lfile.ChannelNames,SuccessfullySet=true };
            FileDataBaseEntryParameter numChars = new FileDataBaseEntryParameter() { Name = "NumChars", UserEditable = false, Value=lfile.NumChars,SuccessfullySet=true };
            FileDataBaseEntryParameter frequency = new FileDataBaseEntryParameter() { Name = "Frequency", UserEditable = false, Value=lfile.Frequency,SuccessfullySet=true };
            
            
            FileDataBaseEntryParameter numPoints = new FileDataBaseEntryParameter() { Name = "NumberPoints", UserEditable = false };
            FileDataBaseEntryParameter hotSpots = new FileDataBaseEntryParameter() { Name = "HotSpotNames", UserEditable = false };
            FileDataBaseEntryParameter hotSpotTimes = new FileDataBaseEntryParameter() { Name = "HotSpotTimes", UserEditable = false };

            return new FileDataBaseEntryParameter[] { numberChannels, channelNames, numChars, frequency, numPoints, hotSpots, hotSpotTimes };

        }

        #endregion


        #region ICeresReader Members


        public FileDataBaseVariableEntryParameter[] GetVariableParameters(FileDataBaseEntry entry)
        {
            LabViewFile lfile = new LabViewFile(entry.Filepath);
            List<FileDataBaseVariableEntryParameter> paras = new List<FileDataBaseVariableEntryParameter>();

            string[] channelNames = entry.Parameters.Single(p => p.Name == "ChannelNames").Value as string[];
            
            for (int i = 0; i < channelNames.Length; i++)
            {
                SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
                para.VariableName.Value = channelNames[i];
                para.VariableName.IsFixed = true;

                para.Units.Value = "Raw";

                para.Frequency.Value = (double)entry.Parameters.Single(p => p.Name == "Frequency").Value;
                para.Frequency.IsFixed = true;

                para.SpikeType.Value = SpikeVariableHeader.SpikeType.Raw;
                para.SpikeType.IsFixed = true;
                paras.Add(para);
            }

            return paras.ToArray();
        }

        #endregion

  

        #region ICeresReader Members


        public Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers)
        {
            bool hasLoaded = fileparams.Single(p => p.Name == "NumberPoints").SuccessfullySet;
            if (!hasLoaded)
            {
                LabViewFile lfile = new LabViewFile(filename);
                lfile.LoadLineCountAndHotspots();
                fileparams.Single(p => p.Name == "NumberPoints").Value = lfile.NumberDataPoints;
                fileparams.Single(p => p.Name == "NumberPoints").SuccessfullySet = true;

                fileparams.Single(p => p.Name == "HotSpotNames").Value = lfile.HotSpotNames.ToArray();
                fileparams.Single(p => p.Name == "HotSpotNames").SuccessfullySet = true;

                fileparams.Single(p => p.Name == "HotSpotTimes").Value = lfile.HotSpotTimes.ToArray();
                fileparams.Single(p => p.Name == "HotSpotTimes").SuccessfullySet = true;
            }

            string[] variableNames = (string[])fileparams.Single(p => p.Name == "ChannelNames").Value as string[];

            int[] readChannels = new int[variableNames.Length];
            for (int i = 0; i < readChannels.Length; i++)
            {
                readChannels[i] = -1;
            }

            IDataPool[] pools = new IDataPool[readChannels.Length];

            int numberPoints = (int)fileparams.Single(p => p.Name == "NumberPoints").Value;
            for (int i = 0; i < headers.Length; i++)
            {
                //int channel = int.Parse(headers[i].VarName.Substring(headers[i].VarName.LastIndexOf(' ') + 1));
                //readChannels[channel] = i;

                for (int j = 0; j < variableNames.Length; j++)
                {
                    if (headers[i].VarName == variableNames[j])
                    {
                        readChannels[j] = i;
                        break;
                    }
                }

                pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { numberPoints });
            }

            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                //StreamReader reader = new StreamReader(stream);

                char[][] buffers = new char[variableNames.Length][];
                float[][] dataBuffers = new float[variableNames.Length][];

                int[] buffCount = new int[variableNames.Length];
                int[] dataBuffCount = new int[variableNames.Length];
                for (int i = 0; i < buffers.Length; i++)
                {
                    buffers[i] = new char[256];
                    dataBuffers[i] = new float[2000000];
                }

                int numChars = (int)fileparams.Single(p => p.Name == "NumChars").Value;

                object[] tag = new object[] { (int)0, pools, readChannels, buffers, buffCount, dataBuffers, dataBuffCount };
                CeresBase.General.Utilities.ReadFromStream(stream, numChars, 500000, readData, tag);

                for (int i = 0; i < variableNames.Length; i++)
                {
                    if (dataBuffCount[i] != 0)
                    {
                        float[] data = new float[dataBuffCount[i]];
                        Array.Copy(dataBuffers[i], data, dataBuffCount[i]);
                        pools[readChannels[i]].SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(data));
                    }
                }
            }
            

            Variable[] vars = new Variable[headers.Length];
            for (int i = 0; i < headers.Length; i++)
            {
                vars[i] = new SpikeVariable(headers[i] as SpikeVariableHeader);

                string[] hotSpots = fileparams.Single(p => p.Name == "HotSpotNames").Value as string[];
                float[] hotSpotTimes = fileparams.Single(p => p.Name == "HotSpotTimes").Value as float[];

                if (hotSpots != null && hotSpotTimes != null)
                {
                    for (int j = 0; j < hotSpots.Length; j++)
                    {
                        EpochCentral.AddEpoch(new Epoch(vars[i], hotSpots[j], hotSpotTimes[j], hotSpotTimes[j]) { AppliesToExperiment = true });


                        //(vars[i] as SpikeVariable).HotSpots.Add(new HotSpot(headers[i].ExperimentName, hotSpots[j], "",
                        //    hotSpotTimes[j], hotSpotTimes[j]));
                    }
                }

                DataFrame frame = new DataFrame(vars[i], pools[i], CeresBase.General.Constants.Timeless);
                vars[i].AddDataFrame(frame);
            }
            return vars;
        }

        #endregion
    }
}
