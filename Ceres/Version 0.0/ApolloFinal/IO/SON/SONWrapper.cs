using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;
using CeresBase.Data;


namespace ApolloFinal.IO.SON
{
    internal static class SONWrapper
    {
        private static void checkError(int code)
        {
            if(code < 0) throw new Exception("ERROR");

        }

        public static class Constants
        {
            public const int SON_NO_FILE = -1;
public const int SON_NO_HANDLES = -4;
public const int SON_NO_ACCESS = -5;
public const int SON_BAD_HANDLE = -6;
public const int SON_OUT_OF_MEMORY = -8;
public const int SON_OUT_OF_HANDLES = -16;  /* This refers to SON file handles */

public const int SON_BAD_READ = -17;
public const int SON_BAD_WRITE = -18;

/*
** now some of our own errors, put in holes that we think we will never
** get from DOS... famous last words!
*/
public const int SON_NO_CHANNEL = -9;
public const int SON_CHANNEL_USED = -10;
public const int SON_CHANNEL_UNUSED = -11;
public const int SON_PAST_EOF = -12;
public const int SON_WRONG_FILE = -13;
public const int SON_NO_EXTRA = -14;
public const int SON_CORRUPT_FILE = -19;
public const int SON_PAST_SOF = -20;
public const int SON_READ_ONLY = -21;
public const int SON_BAD_PARAM = -22;

/*
** These constants define the number and length of various strings
*/
public const int SON_NUMFILECOMMENTS = 5;
public const int SON_COMMENTSZ = 79;
public const int SON_CHANCOMSZ = 71;
public const int SON_UNITSZ = 5;
public const int SON_TITLESZ = 9;
public const int SON_MAXTEXTMARK = 2048; 


        }

        public const int READ_ALL = -1;


        public enum DataType
        {
    ChanOff=0,          /* the channel is OFF - */
    Adc,                /* a 16-bit waveform channel */
    EventFall,          /* Event times (falling edges) */
    EventRise,          /* Event times (rising edges) */
    EventBoth,          /* Event times (both edges) */
    Marker,             /* Event time plus 4 8-bit codes */
    AdcMark,            /* Marker plus Adc waveform data */
    RealMark,           /* Marker plus float numbers */
    TextMark,           /* Marker plus text string */
    RealWave            /* waveform of float numbers */
        }

        [Serializable]
        public struct TTextMark
        {
            public float time;
            public string text;
        }


#region  Internal Functions
        [DllImport("son32.dll", EntryPoint = "SONOpenOldFile"), SuppressUnmanagedCodeSecurity]
        private static extern short SONOpenOldFile(string name, int iOpenMode);

        [DllImport("son32.dll", EntryPoint = "SONMaxChans"), SuppressUnmanagedCodeSecurity]
        private static extern int SONMaxChans(short fh);

        [DllImport("son32.dll", EntryPoint = "SONGetFileComment"), SuppressUnmanagedCodeSecurity]
        private static extern void SONGetFileComment(short fh, int which, StringBuilder comment, short length);

        [DllImport("son32.dll", EntryPoint = "SONCloseFile"), SuppressUnmanagedCodeSecurity]
        private static extern short SONCloseFile(short fh);

        [DllImport("son32.dll", EntryPoint = "SONCreateFile"), SuppressUnmanagedCodeSecurity]
        private static extern short SONCreateFile(string name, int nChannels, short extra);

        [DllImport("son32.dll", EntryPoint = "SONBlocks"), SuppressUnmanagedCodeSecurity]
        private static extern int SONBlocks(short fh, ushort chan);

        [DllImport("son32.dll", EntryPoint = "SONSetFileClock"), SuppressUnmanagedCodeSecurity]
        private static extern void SONSetFileClock(short fh, short usPerTime, short timePerADC);

#region Internal Channel Functions
        [DllImport("son32.dll", EntryPoint = "SONGetChanComment"), SuppressUnmanagedCodeSecurity]
        private static extern void SONGetChanComment(short fh, ushort chan, StringBuilder comment, short length);

        [DllImport("son32.dll", EntryPoint = "SONGetusPerTime"), SuppressUnmanagedCodeSecurity]
        private static extern short SONGetusPerTime(short fh);

        [DllImport("son32.dll", EntryPoint = "SONTimeBase"), SuppressUnmanagedCodeSecurity]
        private static extern double SONTimeBase(short fh, double dTB);

        [DllImport("son32.dll", EntryPoint = "SONChanDivide"), SuppressUnmanagedCodeSecurity]
        private static extern int SONChanDivide(short fh, ushort chan);

        [DllImport("son32.dll", EntryPoint = "SONGetChanTitle"), SuppressUnmanagedCodeSecurity]
        private static extern void SONGetChanTitle(short fh, ushort chan, StringBuilder title);

        [DllImport("son32.dll", EntryPoint = "SONChanKind"), SuppressUnmanagedCodeSecurity]
        private static extern int SONChanKind(short fh, ushort chan);

        [DllImport("son32.dll", EntryPoint = "SONGetADCData"
            ), SuppressUnmanagedCodeSecurity]
        private static extern int SONGetADCData(short fh, ushort chan, IntPtr data, int dataLength,
                  int startTime, int endTime, ref int beginingTime, IntPtr nullPtr);

        [DllImport("son32.dll", EntryPoint = "SONGetExtMarkInfo"), SuppressUnmanagedCodeSecurity]
        private static extern void SONGetExtMarkInfo(short fh, ushort chan, StringBuilder pcUnt, ref ushort points,
               ref int preTrig);

        [DllImport("son32.dll", EntryPoint = "SONItemSize"), SuppressUnmanagedCodeSecurity]
         private static extern ushort SONItemSize(short sFh, ushort chan);

        [DllImport("son32.dll", EntryPoint = "SONGetExtMarkData"), SuppressUnmanagedCodeSecurity]
        private unsafe static extern int SONGetExtMarkData(short fh, ushort chan, [MarshalAs(UnmanagedType.LPArray)]byte[] pMark,
            int max, int sTime, int eTime, IntPtr nullPtr); 
               
        [DllImport("son32.dll", EntryPoint = "SONGetMarkData"), SuppressUnmanagedCodeSecurity]
        private static extern int SONGetMarkData(short fh, ushort chan, [MarshalAs(UnmanagedType.LPArray)]byte[] amData, int max,
            int sTime, int eTime, IntPtr nullPtr);
        //private static extern short SONSetADCChan(short fh, WORD chan, short sPhyCh, short dvd, 
        //    long lBufSz, TpCStr szCom, TpCStr szTitle, float fRate, float scl, float offs, TpCStr szUnt);

        

        [DllImport("son32.dll", EntryPoint = "SONGetADCInfo"), SuppressUnmanagedCodeSecurity]
        private static extern void SONGetADCInfo(short fh, ushort chan, ref float scale, ref float offset,
            StringBuilder units, ref ushort points, ref ushort preTrig);

        [DllImport("son32.dll", EntryPoint = "SONChanBytes"), SuppressUnmanagedCodeSecurity]
        private static extern uint SONChanBytes(short fh, ushort chan);

        [DllImport("son32.dll", EntryPoint = "SONChanMaxTime"), SuppressUnmanagedCodeSecurity]
        private static extern int SONChanMaxTime(short fh, ushort chan);

        [DllImport("son32.dll", EntryPoint = "SONGetRealData"), SuppressUnmanagedCodeSecurity]
        private static extern int SONGetRealData(short fh, ushort chan, IntPtr data, int max,
            int sTime, int eTime, ref int beginningTime,
                          IntPtr nullPtr);

        [DllImport("son32.dll", EntryPoint = "SONSetBuffering"), SuppressUnmanagedCodeSecurity]
        private static extern short SONSetBuffering(short fh, int nChan, int nBytes);

        [DllImport("son32.dll", EntryPoint = "SONSetBuffSpace"), SuppressUnmanagedCodeSecurity]
        private static extern short SONSetBuffSpace(short fh);

        [DllImport("son32.dll", EntryPoint = "SONWriteADCBlock"), SuppressUnmanagedCodeSecurity]
        private static extern int SONWriteADCBlock(short fh, ushort chan, [MarshalAs(UnmanagedType.LPArray)]short[] data, int count, int sTime);




        [DllImport("son32.dll", EntryPoint = "SONSetRealChan"), SuppressUnmanagedCodeSecurity]
        private static extern int SONSetRealChan(short fh, ushort chan, short sPhyChan, int dvd,
                         ushort lBufSz, string szCom, string szTitle,
                         float scale, float offset, string szUnt);

        [DllImport("son32.dll", EntryPoint = "SONSetWaveChan"), SuppressUnmanagedCodeSecurity]
        private static extern short SONSetWaveChan(short fh, ushort chan, short sPhyCh, int dvd,
                 ushort bufferSize, [MarshalAs(UnmanagedType.LPStr)] string comments, [MarshalAs(UnmanagedType.LPStr)] string title,
                 float scl, float offs, [MarshalAs(UnmanagedType.LPStr)] string units);

        //short SONSetWaveChan(short fh, WORD chan, short sPhyCh, TSTime dvd,
        //                 WORD wBufSz, TpCStr szCom, TpCStr szTitle,
        //                 float scl, float offs, TpCStr szUnt); 

        [DllImport("son32.dll", EntryPoint = "SONWriteRealBlock"), SuppressUnmanagedCodeSecurity]
        private static extern int SONWriteRealBlock(short fh, ushort chan, [MarshalAs(UnmanagedType.LPArray)]float[] pfBuff, int count, 
            int sTime);

        [DllImport("son32.dll", EntryPoint = "SONWriteEventBlock"), SuppressUnmanagedCodeSecurity]
        private static extern short SONWriteEventBlock(short fh, ushort chan, [MarshalAs(UnmanagedType.LPArray)]int[] plBuf, int count);

        [DllImport("son32.dll", EntryPoint = "SONSetEventChan"), SuppressUnmanagedCodeSecurity]
        private static extern short SONSetEventChan(short fh, ushort chan, short sPhyCh, int lBufSz,
            [MarshalAs(UnmanagedType.LPStr)] string comments, [MarshalAs(UnmanagedType.LPStr)] string title, float fRate, int evtKind);

        [DllImport("son32.dll", EntryPoint = "SONGetEventData"), SuppressUnmanagedCodeSecurity]
        private static extern int SONGetEventData(short fh, ushort chan, IntPtr data, int max,
                          int sTime, int eTime, bool level, IntPtr nullPtr);

        ///long SONGetADCData(short fh, WORD chan, TpFloat pFloat, long max, 
        //       TSTime sTime, TSTime eTime, TpSTime pbTime, 
        //       TpFilterMask pFltMask); 

#endregion

#endregion

        public static void CloseFile(short fh)
        {
            checkError(SONCloseFile(fh));
        }

        public static short CreateFile(string filename, float frequency, int numChannels)
        {
            short fid = SONWrapper.SONCreateFile(filename, 0, 0);
            short usPerTime = (short)(1/(frequency*1e-6));
            SONWrapper.SONSetFileClock(fid, usPerTime, 1);
            return fid;
        }

        public static void ChannelsSet(short fh)
        {
            SONWrapper.SONSetBuffering(fh, -1, 4194304);
            SONWrapper.SONSetBuffSpace(fh);
        }

        public static ushort[] GetChannels(short fh)
        {
            int maxChans = SONMaxChans(fh);
            List<ushort> channels = new List<ushort>();
            for (ushort i = 0; i < maxChans; i++)
			{
			    int kind = SONChanKind(fh, i);
                if(kind == (int)DataType.Adc || kind == (int)DataType.RealWave 
                    || kind == (int)DataType.EventRise || kind == (int)DataType.EventFall
                    || kind == (int)DataType.EventBoth) channels.Add(i);
			}
            return channels.ToArray();
        }

        public static ushort[] GetTextChannels(short fh)
        {
            int maxChans = SONMaxChans(fh);
            List<ushort> channels = new List<ushort>();
            for (ushort i = 0; i < maxChans; i++)
            {
                int kind = SONChanKind(fh, i);
                if (kind == (int)DataType.TextMark) channels.Add(i);
            }
            return channels.ToArray();
        }

        public static TTextMark[] GetTextMarkers(short fh, ushort chan)
        {
            //WORD nCh, nByt; /* length of character array, bytes per TextMark */
            //int nT, i, cnt; /* number of TextMarks read, counters */ 
            //SONGetExtMarkInfo(sFh, chan, &units, &nCh, &i); /* Get # chars */ 
            //The number of bytes to hold each TextMark structure is given by: 
            //nByt = SONItemSize(sFh, chan); /* Bytes per data item */ 
            //To allocate a buffer for 100 TextMark events and read them you would need code like: 
            //amP = malloc(nByt * 100); /* Allocate space, assume we succeed */ 
            //nT = SONGetExtMarkData(sFh, chan, amP, 100, 0, 100000, NULL); 
            //To write out the time, the first marker byte and the text of each TextMark needs code of 
            //the following type: 
            //wP = amP;  /* pointer to first marker */ 
            //for (i=0; i<nT; ++i) 
            //{ 
            //    printf("Time %d code %d text ", wP->m.mark, wP->m.mvals[0]); 
            //    for(cnt=0;(cnt<nCh) && (wP->t[cnt]!=0);cnt++) 
            //        printf("%c", wP->t[cnt]); /* Print each char */ 
            //    printf("\n"); 
            //    wP = (TpTextMark)((char*)wP + nByt); /* move on by nByt bytes */ 
            //} 

            ushort nCh = 0;
            int s = 0;
            StringBuilder builder = new StringBuilder(Constants.SON_UNITSZ + 1);
            SONGetExtMarkInfo(fh, chan, builder, ref nCh, ref s);

            int itemSize = SONItemSize(fh, chan);
            byte[] test = new byte[itemSize * 200];


            List<TTextMark> marks = new List<TTextMark>();
            try
            {
                int max = GetMaxTime(fh, chan);
                int numMarks = SONGetExtMarkData(fh, chan, test, 200, 0, max, IntPtr.Zero);
                ASCIIEncoding encoding = new ASCIIEncoding();

                for (int i = 0; i < numMarks; i++)
                {
                    TTextMark mark = new TTextMark();
                    int markTime = BitConverter.ToInt32(test, i * itemSize);
                    mark.time = (float)(markTime / SONWrapper.GetFrequencyOfChannel(fh, chan));
                    
                    mark.text = encoding.GetString(test, i * itemSize + 8, nCh);
                    mark.text = mark.text.Substring(0, mark.text.IndexOf('\0'));
                    marks.Add(mark);
                }
            }
            catch (Exception ex)
            {
                int eb = 0;
            }

            return marks.ToArray();
        }

        public static DataType ChannelType(short fh, ushort chan)
        {
            return (DataType)(SONChanKind(fh, chan));
        }

        public static short OpenFile(string filepath)
        {
            short id = SONOpenOldFile(filepath, 1);
            if(id < 0) return -1;
            return id;
        }

        public static string FileComments(short fh)
        {
            string result = "";
            for (int i = 0; i < 5; i++)
			{
			    StringBuilder builder = new StringBuilder(Constants.SON_TITLESZ + 1);
                SONGetFileComment(fh, i, builder, Constants.SON_TITLESZ);
                result += builder.ToString() + " ";
			}
            return result;
        }

#region Public Functions
        public static double GetFrequencyOfChannel(short fh, ushort chan)
        {
            short usPerTime = SONGetusPerTime(fh);
            double dTickLen = SONTimeBase(fh, 0.0);
            int divide = SONChanDivide(fh, chan);
            return 1.0/(divide*(usPerTime*dTickLen));
        }

        public static void CreateChannel(short fh, ushort channelNumber, SpikeVariable var)
        {
            float scale = (float)(((var[0] as DataFrame).Max - (var[0] as DataFrame).Min) / ushort.MaxValue);
          //  scale *= 65536.0F / 10.0F;
            //real=(short*scl*(10/65536))+offs

            short usPerTime = SONGetusPerTime(fh);
            double dTickLen = SONTimeBase(fh, 0.0);

            int divide = (int)(1 / (usPerTime * dTickLen * (var.Header as SpikeVariableHeader).Frequency));

            if (var.SpikeType != SpikeVariableHeader.SpikeType.Pulse)
            {
                //int error = SONWrapper.SONSetWaveChan(fh, channelNumber, -1, divide, (ushort)32768, var.Header.Description,
                //    var.Header.VarName, 1.0, 0, "Raw");
                SONWrapper.SONSetRealChan(fh, channelNumber, -1, divide, (ushort)32768, var.Header.Description,
                    var.Header.VarName, 1.0F, 0, "Raw");
            }
            else
            {
               int error = SONSetEventChan(fh, channelNumber, -1, (ushort)32768, var.Header.Description, var.Header.VarName, (float)(var.Header as SpikeVariableHeader).Frequency, 
                   (int)DataType.EventRise);
            }
        }

        public static int GetChanDivide(short fh, ushort chan)
        {
            return SONChanDivide(fh, chan);
        }

        public static string GetChannelComment(short fh, ushort chan)
        {
            StringBuilder builder = new StringBuilder(Constants.SON_CHANCOMSZ + 1);
            SONGetChanComment(fh, chan, builder, Constants.SON_CHANCOMSZ);
            return builder.ToString();
        }

        public static string GetChannelTitle(short fh, ushort chan)
        {
            StringBuilder builder = new StringBuilder(Constants.SON_TITLESZ + 1);
            SONGetChanTitle(fh, chan, builder);
            return builder.ToString();
        }

        public static int GetMaxTime(short fh, ushort chan)
        {
            return SONChanMaxTime(fh, chan);
        }

        public static void WriteData(short fh, ushort chan, ref int beginTime, float[] data)
        {
            float scale = 0, offset = 0;
            StringBuilder builder = new StringBuilder(Constants.SON_UNITSZ);
            int dataType = SONChanKind(fh, chan);

            if (dataType == (int)DataType.EventRise)
            {
                int[] scaledData = new int[data.Length];
                short usPerTime = SONGetusPerTime(fh);
                double dTickLen = SONTimeBase(fh, 0.0);
                int divide = SONChanDivide(fh, chan);

                for (int i = 0; i < data.Length; i++)
                {
                    scaledData[i] = (int)((data[i] /( dTickLen * divide))/usPerTime);
                }

                SONWriteEventBlock(fh, chan, scaledData, scaledData.Length);
            }
            else
            {
                //SONWrapper.SONGetADCInfo(fh, chan, ref scale, ref offset, builder, null, null);
                //short[] scaledData = new short[data.Length];
                //for (int i = 0; i < data.Length; i++)
                //{
                //    scaledData[i] = (short)((ushort)((data[i] - offset) / scale) + short.MinValue);
                //}

                //beginTime = SONWrapper.SONWriteADCBlock(fh, chan, scaledData, scaledData.Length, beginTime);
                beginTime = SONWrapper.SONWriteRealBlock(fh, chan, data, data.Length, beginTime);
            }
        }

        public static float[] GetData(short fh, ushort chan, int beginTime, int goToTime, out int endTime, out int length)
        {

            int maxTime = SONChanMaxTime(fh, chan);
            if (goToTime < 0) goToTime = maxTime;

            if (beginTime >= maxTime || beginTime < 0)
            {
                endTime = -1;
                length = 0;
                return null;
            }

           
            

            int dataType = SONChanKind(fh, chan);

            float[] data = new float[256000];
            if (dataType == (int)DataType.EventRise || dataType == (int)DataType.EventFall ||
                dataType == (int)DataType.EventBoth)
            {
                int[] edata = new int[256000];
                int amount = 0;
                short usPerTime = SONGetusPerTime(fh);

                unsafe
                {                   
                        fixed (int* ePtr = edata)
                        {
                            amount = SONGetEventData(fh, chan, new IntPtr(ePtr), 256000, beginTime, goToTime, false, new IntPtr(0));
                        }
                        length = amount;
                        for (int i = 0; i < length; i++)
                        {
                            data[i] = edata[i];
                        }
                        if (amount == 0 || amount != data.Length)
                        {
                            endTime = -1;
                        }
                        else endTime = edata[amount - 1];
                 
                }

            }
            else
            {
                int beginningTime = 0;
                int amount = 0;

                float scale = 0, offset = 0;
                StringBuilder builder = new StringBuilder(Constants.SON_UNITSZ);
                ushort pts = 0;
                ushort pretrig = 0;
                SONWrapper.SONGetADCInfo(fh, chan, ref scale, ref offset, builder, ref pts, ref pretrig);

                int offSet = 0;
                uint bytes = SONChanBytes(fh, chan);

                unsafe
                {
                    fixed (float* dP = data)
                    {
                        if (scale != 1 || offset != 0)
                        {
                            short[] adcData = new short[256000];
                            fixed (short* sP = adcData)
                            {

                                amount = SONGetADCData(fh, chan, new IntPtr(sP), adcData.Length, beginTime,
                                    goToTime, ref beginningTime, IntPtr.Zero);
                                short* curS = sP;
                                float* curF = dP;
                                for (int i = 0; i < amount; i++, curS++, curF++)
                                {
                                    //asData[i]/6553.6*scale + offset
                                    *curF = (float)(*curS/6553.6F*scale + offset);
                                }
                            }
                        }
                        else
                        {
                            
                            amount = SONGetRealData(fh, chan, new IntPtr(dP), data.Length, beginTime, goToTime,
                                ref beginningTime, IntPtr.Zero);
                        }
                    }
                }

                endTime = beginningTime + amount * SONChanDivide(fh, chan);
                if (amount != data.Length)
                    endTime = -1;
                length = amount;

            }
            return data;
        }

#endregion


        #region Other Code
        //#ifndef __SON__
//#define __SON__

//#include "machine.h"

//#if defined(macintosh) || defined(_MAC) /* define SONCONVERT in here if 
//you want it */
////    #include <Types.h>   NBNB     this gets rid of stupid VC++
////    #include <Files.h>            dependency search errors
////    #include <Errors.h>
//#define  SONAPI(type) type
//#undef LLIO                         /* LLIO is not used for Mac             
//*/
//#endif                              /* End of the Mac stuff, now for 
//DOS    */

//#ifdef _IS_MSDOS_
//    #include <malloc.h>
//    #include <dos.h>
//    #include <errno.h>
//    #define LLIO                    /* We can use LLIO for MSC/DOS          
//*/
//    #define SONAPI(type) type _pascal
//#endif

//#if defined(_IS_WINDOWS_) && !defined(_MAC) && !defined(WIN32)
//    #define LLIO                    /* We can use LLIO for MSC/Windows      
//*/
//    #define SONAPI(type) type WINAPI
//#endif

//#ifdef WIN32
//    #undef  LLIO                    /* We can use LLIO for MSC/Windows      
//*/
//    #define SONAPI(type) type WINAPI
//#endif

//#define SONMAXCHANS 32          /* The old limit on channels, now the 
//minimum number */
//#define SONABSMAXCHANS 400      /* You are not allowed any more 
//channels than this */

///*
//** Now define the error constants used throughout the program
//*/


///*
//** These define the types of data we can store in our file, and a type
//** that will hold one of these values.
//*/




///*
//**  The TMarker structure defines the marker data structure, which 
//holds
//**  a time value with associated data. The TAdcMark structure is a 
//marker
//**  with attached array of ADC data. TRealMark and TTextMark are very
//**  similar - with real or text data attached.
//*/
//typedef long TSTime;
//#define TSTIME_MAX LONG_MAX
//typedef short TAdc;
//typedef char TMarkBytes[4];

//typedef struct
//{
//    TSTime mark;                /* Marker time as for events */
//    TMarkBytes mvals;           /* the marker values */
//} TMarker;

//#define SON_MAXADCMARK 1024     /* maximum points in AdcMark data 
//(arbitrary) */
//#define SON_MAXAMTRACE 4        /* maximum interleaved traces in 
//AdcMark data */
//typedef struct
//{
//    TMarker m;                  /* the marker structure */
//    TAdc a[SON_MAXADCMARK*SON_MAXAMTRACE];     /* the attached ADC data 
//*/
//} TAdcMark;

//#define SON_MAXREALMARK 512     /* maximum points in RealMark 
//(arbitrary) */
//typedef struct
//{
//    TMarker m;                  /* the marker structure */
//    float r[SON_MAXREALMARK];   /* the attached floating point data */
//} TRealMark;

//#define SON_MAXTEXTMARK 2048    /* maximum points in a Textmark 
//(arbitrary) */
//typedef struct
//{
//    TMarker m;                  /* the marker structure */
//    char t[SON_MAXTEXTMARK];    /* the attached text data */
//} TTextMark;

//typedef TAdc FAR * TpAdc;
//typedef TSTime FAR *TpSTime;
//typedef TMarker FAR * TpMarker;
//typedef TAdcMark FAR * TpAdcMark;
//typedef TRealMark FAR * TpRealMark;
//typedef TTextMark FAR * TpTextMark;
//typedef char FAR * TpStr;
//typedef const char FAR * TpCStr;
//typedef WORD FAR * TpWORD;
//typedef BOOLEAN FAR * TpBOOL;
//typedef float FAR * TpFloat;
//typedef void FAR * TpVoid;
//typedef short FAR * TpShort;
//typedef TMarkBytes FAR * TpMarkBytes;

///* 
//**
//** The marker filter extensions to SON
//**
//** The declaration of the types is only to allow you to declare a 
//structure
//** of type TFilterMask. This structure is to be passed into the two
//** functions only. No other use should be made, except to save it, if
//** needed.
//**
//** We have changed the implementation of TFilterMask so we can use the
//** marker filer in different ways. As long as you made no use of 
//cAllSet
//** the changes should be transparent as the structure is the same size.
//** if you MUST have the old format, #define SON_USEOLDFILTERMASK
//**
//** In the new method we no longer use flags to show that an entire 
//layer is
//** set. Instead, we have a filter mode (bit 0 of the lFlags). All other 
//bits
//** should be 0 as we will use them for further modes in future and we 
//intend
//** this to be backwards compatible.
//**
//** We also have define a new function to get/set the filter mode. We 
//avoid bits
//** 0, 8, 16 and 24 as these were used in the old version to flag 
//complete masks.
//**
//*/

//#define SON_FMASKSZ 32                      /* number of TFilterElt in 
//mask */
//typedef unsigned char TFilterElt;           /* element of a map */
//typedef TFilterElt TLayerMask[SON_FMASKSZ]; /* 256 bits in the bitmap 
//*/



//#define SON_FMASK_ORMODE 0x02000000         /* use OR if data rather 
//than AND */
//#define SON_FMASK_ANDMODE 0x00000000
//#define SON_FMASK_VALID  0x02000000         /* bits that are valid in 
//the mask */

//typedef TFilterMask FAR *TpFilterMask;

//#define SON_FALLLAYERS  -1
//#define SON_FALLITEMS   -1
//#define SON_FCLEAR      0
//#define SON_FSET        1
//#define SON_FINVERT     2
//#define SON_FREAD       -1


//#ifdef __cplusplus
//extern "C" {
//#endif


///*
//** Now definitions of the functions defined in the code
//*/
//SONAPI(void) SONInitFiles(void);
//SONAPI(void) SONCleanUp(void);

//#if defined(macintosh) || defined(_MAC)
//SONAPI(short) SONOpenOldFile(ConstStr255Param name, short vRefNum, long 
//dirID,
//                    SignedByte perm);
//SONAPI(short) SONOpenNewFile(ConstStr255Param name, short fMode, WORD 
//extra,
//                short vRefNum, long dirID, SignedByte perm, 
//                OSType creator, OSType fileType);
//#else

//#endif

//SONAPI(BOOLEAN) SONCanWrite(short fh);
//SONAPI(short) SONEmptyFile(short fh);
//SONAPI(short) SONGetFreeChan(short fh);


//SONAPI(short) SONSetADCMarkChan(short fh, WORD chan, short sPhyCh, 
//short dvd,
//                 long lBufSz, TpCStr szCom, TpCStr szTitle, float 
//fRate, float scl,
//                 float offs, TpCStr szUnt, WORD points, short preTrig);

//SONAPI(short) SONSetWaveMarkChan(short fh, WORD chan, short sPhyCh, 
//TSTime dvd,
//                 long lBufSz, TpCStr szCom, TpCStr szTitle, float 
//fRate, float scl,
//                 float offs, TpCStr szUnt, WORD points, short preTrig, 
//int nTrace);
//SONAPI(short) SONSetRealMarkChan(short fh, WORD chan, short sPhyCh,
//                 long lBufSz, TpCStr szCom, TpCStr szTitle, float 
//fRate,
//                 float min, float max, TpCStr szUnt, WORD points);
//SONAPI(short) SONSetTextMarkChan(short fh, WORD chan, short sPhyCh,
//                 long lBufSz, TpCStr szCom, TpCStr szTitle,
//                 float fRate, TpCStr szUnt, WORD points);
//SONAPI(void) SONSetInitLow(short fh, WORD chan, BOOLEAN bLow);



//SONAPI(short) SONUpdateStart(short fh);
//SONAPI(void) SONSetFileComment(short fh, WORD which, TpCStr szFCom);

//SONAPI(void) SONSetChanComment(short fh, WORD chan, TpCStr szCom);

//SONAPI(void) SONSetChanTitle(short fh, WORD chan, TpCStr szTitle);
//SONAPI(void) SONGetIdealLimits(short fh, WORD chan, TpFloat pfRate, 
//TpFloat pfMin, TpFloat pfMax);
//SONAPI(WORD) SONGetTimePerADC(short fh);
//SONAPI(void) SONSetADCUnits(short fh, WORD chan, TpCStr szUnt);
//SONAPI(void) SONSetADCOffset(short fh, WORD chan, float offset);
//SONAPI(void) SONSetADCScale(short fh, WORD chan, float scale);

//SONAPI(void) SONGetExtMarkInfo(short fh, WORD chan, TpStr pcUnt,
//                 TpWORD points, TpShort preTrig);


//SONAPI(short) SONWriteMarkBlock(short fh, WORD chan, TpMarker pM, long 
//count);

//SONAPI(short) SONWriteExtMarkBlock(short fh, WORD chan, TpMarker pM, 
//long count);

//SONAPI(short) SONSave(short fh, int nChan, TSTime sTime, BOOLEAN 
//bKeep);
//SONAPI(short) SONSaveRange(short fh, int nChan, TSTime sTime, TSTime 
//eTime);
//SONAPI(short) SONKillRange(short fh, int nChan, TSTime sTime, TSTime 
//eTime);
//SONAPI(short) SONIsSaving(short fh, int nChan);
//SONAPI(DWORD) SONFileBytes(short fh);
//

//SONAPI(short) SONLatestTime(short fh, WORD chan, TSTime sTime);
//SONAPI(short) SONCommitIdle(short fh);
//SONAPI(short) SONCommitFile(short fh, BOOLEAN bDelete);


//SONAPI(long) SONGetMarkData(short fh, WORD chan,TpMarker pMark, long 
//max,
//                  TSTime sTime,TSTime eTime, TpFilterMask pFiltMask);


//SONAPI(long) SONGetExtMarkData(short fh, WORD chan, TpMarker pMark, 
//long max,
//                  TSTime sTime,TSTime eTime, TpFilterMask pFiltMask);
//SONAPI(long) SONGetExtraDataSize(short fh);
//SONAPI(int) SONGetVersion(short fh);
//SONAPI(short) SONGetExtraData(short fh, TpVoid buff, WORD bytes,
//                  WORD offset, BOOLEAN writeIt);
//SONAPI(short) SONSetMarker(short fh, WORD chan, TSTime time, TpMarker 
//pMark,
//                  WORD size);
//SONAPI(short)  SONChanDelete(short fh, WORD chan);
//SONAPI(WORD)   SONItemSize(short fh, WORD chan);
//
//SONAPI(TSTime) SONMaxTime(short fh);

//SONAPI(TSTime) SONLastTime(short fh, WORD wChan, TSTime sTime, TSTime 
//eTime,
//                    TpVoid pvVal, TpMarkBytes pMB,
//                    TpBOOL pbMk, TpFilterMask pFiltMask);

//SONAPI(TSTime) SONLastPointsTime(short fh, WORD wChan, TSTime sTime, 
//TSTime eTime,
//                    long lPoints, BOOLEAN bAdc, TpFilterMask 
//pFiltMask);

//SONAPI(long) SONFileSize(short fh);
//SONAPI(int) SONMarkerItem(short fh, WORD wChan, TpMarker pBuff, int n,
//                                          TpMarker pM, TpVoid pvData, 
//BOOLEAN bSet);

//SONAPI(int) SONFilter(TpMarker pM, TpFilterMask pFM);
//SONAPI(int) SONFControl(TpFilterMask pFM, int layer, int item, int 
//set);
//SONAPI(BOOLEAN) SONFEqual(TpFilterMask pFiltMask1, TpFilterMask 
//pFiltMask2);
//SONAPI(int) SONFActive(TpFilterMask pFM);   // added 14/May/02

//#ifndef SON_USEOLDFILTERMASK
//SONAPI(long) SONFMode(TpFilterMask pFM, long lNew);
//#endif

///****************************************************************************
//** New things added at Revision 6
//*/
//typedef struct
//{
//    unsigned char ucHun;    /* hundreths of a second, 0-99 */
//    unsigned char ucSec;    /* seconds, 0-59 */
//    unsigned char ucMin;    /* minutes, 0-59 */
//    unsigned char ucHour;   /* hour - 24 hour clock, 0-23 */
//    unsigned char ucDay;    /* day of month, 1-31 */
//    unsigned char ucMon;    /* month of year, 1-12 */
//    WORD wYear;             /* year 1980-65535! */
//}TSONTimeDate;

//#if defined(macintosh) || defined(_MAC)
//SONAPI(short) SONCreateFile(ConstStr255Param name, int nChannels, WORD 
//extra, 
//                short vRefNum, long dirID, SignedByte perm, 
//                OSType creator, OSType fileType);
//#else

//#endif
//SONAPI(int) SONPhyChan(short fh, WORD wChan);
//SONAPI(float) SONIdealRate(short fh, WORD wChan, float fIR);
//SONAPI(void) SONYRange(short fh, WORD chan, TpFloat pfMin, TpFloat 
//pfMax);
//SONAPI(int) SONYRangeSet(short fh, WORD chan, float fMin, float fMax);
//SONAPI(int) SONMaxItems(short fh, WORD chan);
//SONAPI(int) SONPhySz(short fh, WORD chan);
//SONAPI(int) SONBlocks(short fh, WORD chan);
//SONAPI(int) SONDelBlocks(short fh, WORD chan);


//SONAPI(int) SONTimeDate(short fh, TSONTimeDate* pTDGet, const 
//TSONTimeDate* pTDSet);
//typedef struct {char acID[8];} TSONCreator;    /* application 
//identifier */
//SONAPI(int) SONAppID(short fh, TSONCreator* pCGet, const TSONCreator* 
//pCSet);
//SONAPI(int) SONChanInterleave(short fh, WORD chan);

///* Version 7 */
//SONAPI(int) SONExtMarkAlign(short fh, int n);
//#ifdef __cplusplus
//}
//#endif

//#endif /* __SON__ */

#endregion
    }
}
