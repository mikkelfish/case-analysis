using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using CeresBase.UI;
using CeresBase.Data;
using System.Windows.Forms;

namespace ApolloFinal.IO.SON
{
    class SONWriter : ICeresWriter
    {
        //private CeresFileDataBase database;

        public void AddAttribute(ICeresFile file, string name, string attrName, object value)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ICeresFile[] WriteVariable(CeresBase.Data.Variable variable, string writeToDirectory)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void WriteBinary(ICeresFile file, string name, byte[] data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}

        public void Dispose()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override string ToString()
        {
            return "Spike2 Format";
        }

        public ICeresFile[] WriteVariables(CeresBase.Data.Variable[] variables, string writeToDirectory)
        {
            //RequestInformation info = new RequestInformation();
            //info.Grid.SetInformationToCollect(new Type[] { typeof(string) }, new string[] { "Filename" },
            //    new string[] { "Filename" }, new string[] { "Please enter a filename for this file" },
            //    null, null);
            //if (info.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return null;

            //writeToDirectory += (string)info.Results["Filename"];
            SaveFileDialog save = new SaveFileDialog();
            save.AddExtension = true;
            save.Filter = "Spike2(*.smr)|*.smr|All Files(*.*)|*.*";

            if (save.ShowDialog() == DialogResult.Cancel) return null;
            writeToDirectory = save.FileName;

            float maxFreq = float.MinValue;
            bool useMinMax = false;
            for (int i = 0; i < variables.Length; i++)
			{
			   float freq = (float)((variables[i] as SpikeVariable).Header as SpikeVariableHeader).Frequency;
                if(freq > maxFreq) maxFreq = freq;

                if ((variables[i] as SpikeVariable).Max > 16000)
                {
                    useMinMax = true;
                }
			}
            
            SONFile file = new SONFile(writeToDirectory, maxFreq, variables.Length);
            for (ushort i = 0; i < variables.Length; i++)
            {
                SONWrapper.CreateChannel(file.FileID, i, variables[i] as SpikeVariable);
            }

            SONWrapper.ChannelsSet(file.FileID);


            float min = float.MaxValue;
            float max = float.MinValue;
            if (useMinMax)
            {
                RequestInformation info = new RequestInformation();
                info.Grid.SetInformationToCollect(new Type[] { typeof(float), typeof(float) }, new string[] { "Write", "Write" },
                    new string[] { "Min. Voltage", "Max. Voltage" }, new string[] { "Please enter the minimum voltage.", "Please enter the maximum voltage." },
                    null, null);
                if (info.ShowDialog() != DialogResult.Cancel)
                {
                    min = (float)info.Grid.Results["Min. Voltage"];
                    max = (float)info.Grid.Results["Max. Voltage"];
                }
            }

            for (ushort i = 0; i < variables.Length; i++)
            {
                int time = 0;
                double varMin = (variables[i][0] as DataFrame).Min;
                double varMax = (variables[i][0] as DataFrame).Max;
                (variables[i][0] as DataFrame).Pool.GetData(null, null,
                    delegate(float[] data, uint[] indices, uint[] counts)
                    {
                        if ((variables[i] as SpikeVariable).SpikeType == SpikeVariableHeader.SpikeType.Pulse)
                        {
                            float[] dataInSeconds = new float[data.Length];
                            for (int j = 0; j < dataInSeconds.Length; j++)
                            {
                                dataInSeconds[j] = data[j] * (float)((variables[i] as SpikeVariable).Header as SpikeVariableHeader).Resolution;
                            }
                            SONWrapper.WriteData(file.FileID, i, ref time, dataInSeconds);
                        }
                        else
                        {
                            if (varMax > 16000 && min != float.MaxValue && max != float.MinValue)
                            {
                                for (int j = 0; j < data.Length; j++)
                                {
                                    double perc = (data[j] - varMin) / (varMax - varMin);
                                    data[j] = (float)(min + perc * (max - min));
                                }
                            }
                            SONWrapper.WriteData(file.FileID, i, ref time, data);
                        }
                    }
                );
            }

            file.Dispose();
            return new ICeresFile[] { file };
        }

        #region ICeresWriter Members


        public void AddAttribute(string fileName, string name, string attrName, object value)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
