using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using CeresBase.IO.DataBase;
using CeresBase.Projects;

namespace ApolloFinal.IO.SON
{
    class SONReader : CeresBase.IO.ICeresReader
    {
        //private CeresBase.IO.CeresFileDataBase database;

        public object ReadAttribute(CeresBase.IO.ICeresFile file, string name, string attrName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresBase.Data.Variable ReadVariable(string filename, CeresBase.Data.VariableHeader header, DateTime[] times)
        //{
        //    SONFile son = new SONFile(filename);
        //    string[] splits = header.VarName.Trim().Split(' ');
        //    ushort channel = (ushort)(ushort.Parse(splits[splits.Length - 1]) - 1);

           
        //    //int endTime = 0;
        //    //float[] dataChunk = null;
        //    //int length = 0;
        //    //int total = 0;
        //    //while ((dataChunk = SONWrapper.GetData(son.FileID, channel, endTime, SONWrapper.READ_ALL, out endTime, out length)) != null)
        //    //{
        //    //    total += length;
        //    //    if (endTime < 0) break;
        //    //}

        //    //IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { (int)total});
        //    //endTime = 0;
        //    //while ((dataChunk = SONWrapper.GetData(son.FileID, channel, endTime, SONWrapper.READ_ALL, out endTime, out length)) != null)
        //    //{
        //    //    total += length;
        //    //    byte[] byteMem = CeresBase.Data.DataUtilities.ArrayToByteArray(dataChunk, length);
        //    //    System.IO.MemoryStream stream = new System.IO.MemoryStream(byteMem);
        //    //    pool.SetData(stream);
        //    //    if (endTime < 0) 
        //    //        break;
        //    //}

        //    IDataPool pool = new SONDataPool(son, channel);

        //  SpikeVariable variable = new SpikeVariable((SpikeVariableHeader)header);
        //    DataFrame frame = new DataFrame(variable, pool, new CeresBase.Data.DataShapes.NoShape(),
        //        CeresBase.General.Constants.Timeless);


        //    ushort[] textChans = SONWrapper.GetTextChannels(son.FileID);
        //    foreach (ushort chan in textChans)
        //    {
        //        SONWrapper.TTextMark[] marks = SONWrapper.GetTextMarkers(son.FileID, chan);
        //        foreach (SONWrapper.TTextMark mark in marks)
        //        {
        //            //HotSpot spot = new HotSpot(header.ExperimentName, mark.text, mark.text, mark.time, mark.time);
        //            //variable.HotSpots.Add(spot);

        //            //EpochCentral.Epochs.Add(new Epoch(variable, mark.text, mark.time, mark.time));

        //            //variable.EpochsFromFile.Add(new Epoch(variable, mark.text, mark.time, mark.time));

        //            EpochCentral.AddEpoch(new Epoch(variable, mark.text, mark.time, mark.time) { AppliesToExperiment = true });
        //        }
        //    }
            
        //    variable.AddDataFrame(frame);

        //    return variable;
        //}

        //public CeresBase.Data.Variable[] ReadVariables(string filename, CeresBase.Data.VariableHeader[] headers, DateTime[][] times)
        //{
        //    List<Variable> vars = new List<Variable>();
        //    for (int i = 0; i < headers.Length; i++)
        //    {
        //        vars.Add(this.ReadVariable(filename, headers[i], null));
			 
        //    }
        //    return vars.ToArray();
        //}

        public byte[] ReadBinary(CeresBase.IO.ICeresFile file, string name)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresBase.IO.CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}

        public double[][] GetShapeValues(CeresBase.IO.ICeresFile file)
        {
            return null;
        }

        //public CeresBase.Data.VariableHeader[] GetAllVariableHeaders(string file, string experimentName)
        //{
        //    SONFile son = new SONFile(file);
        //    ushort[] channels = son.Channels;
        //    VariableHeader[] headers = new VariableHeader[channels.Length];
        //    for (int i = 0; i < headers.Length; i++)
        //    {
        //        SpikeVariableHeader.SpikeType type = SpikeVariableHeader.SpikeType.Raw;

        //        SONWrapper.DataType dataType = SONWrapper.ChannelType(son.FileID, channels[i]);
        //        //dataType == (int)DataType.EventRise || dataType == (int)DataType.EventFall ||
        //        //dataType == (int)DataType.AdcMark || dataType == (int)DataType.RealMark ||
        //        //dataType == (int)DataType.EventBoth

        //        if (dataType == SONWrapper.DataType.EventRise || 
        //            dataType == SONWrapper.DataType.EventFall ||
        //            dataType == SONWrapper.DataType.EventBoth)
        //        {
        //            type = SpikeVariableHeader.SpikeType.Pulse;
        //        }

        //        headers[i] = new SpikeVariableHeader("Channel " + (channels[i] + 1).ToString(),
        //            "Raw", experimentName, SONWrapper.GetFrequencyOfChannel(son.FileID, channels[i]),
        //            type);
        //        headers[i].Description = SONWrapper.GetChannelTitle(son.FileID, channels[i]);
        //        headers[i].Comments = SONWrapper.GetChannelComment(son.FileID, channels[i]);

               
        //    }
        //    return headers;
        //}

        public bool Supported(string file)
        {
            return SONFile.CanRead(file);
        }

        //public void AddFilesToDB(string[] files)
        //{
        //    CeresBase.UI.RequestInformation infoReq = new CeresBase.UI.RequestInformation();
        //    List<Type> types = new List<Type>();
        //    List<string> names = new List<string>();
        //    List<string> descriptions = new List<string>();

        //    types.Add(typeof(string));
        //    names.Add("experimentName");
        //    descriptions.Add("Please enter the name of the experiment this data is from.");

        //    infoReq.Grid.SetInformationToCollect(types.ToArray(), null, names.ToArray(), descriptions.ToArray(), null, null);
        //    if (infoReq.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
        //    {
        //        return;
        //    }

        //    for (int i = 0; i < files.Length; i++)
        //    {
        //        SONFile son = new SONFile(files[i]);
        //        this.Database.CreateFile(infoReq.Results["experimentName"] as string, son);
        //    }
        //}

        #region ICeresReader Members


        public bool StayNative
        {
            get { return true; }
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            return null;
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseVariableEntryParameter[] GetVariableParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            SONFile son = new SONFile(entry.Filepath);
            ushort[] channels = son.Channels;
            FileDataBaseVariableEntryParameter[] paras = new FileDataBaseVariableEntryParameter[channels.Length];
            for (int i = 0; i < channels.Length; i++)
            {
                SpikeVariableHeader.SpikeType type = SpikeVariableHeader.SpikeType.Raw;

                SONWrapper.DataType dataType = SONWrapper.ChannelType(son.FileID, channels[i]);
                
                if (dataType == SONWrapper.DataType.EventRise ||
                    dataType == SONWrapper.DataType.EventFall ||
                    dataType == SONWrapper.DataType.EventBoth)
                {
                    type = SpikeVariableHeader.SpikeType.Pulse;
                }

                SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
                para.VariableName.Value = "Channel " + (channels[i] + 1).ToString();
                para.VariableName.IsFixed = true;

                para.Units.Value = "Raw";

                para.Frequency.Value = SONWrapper.GetFrequencyOfChannel(son.FileID, channels[i]);
                para.Frequency.IsFixed = true;

                para.SpikeType.Value = type;
                para.SpikeType.IsFixed = true;

                para.DisplayName.Value = SONWrapper.GetChannelTitle(son.FileID, channels[i]);
                para.Comments.Value = SONWrapper.GetChannelComment(son.FileID, channels[i]);

                paras[i] = para;
            }

            return paras;
        }

        #endregion



        #region ICeresReader Members


        public Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers)
        {

            List<Variable> vars = new List<Variable>();
            SONFile son = new SONFile(filename);
            for (int i = 0; i < headers.Length; i++)
            {
                string[] splits = headers[i].VarName.Trim().Split(' ');
                ushort channel = (ushort)(ushort.Parse(splits[splits.Length - 1]) - 1);
                IDataPool pool = new SONDataPool(son, channel);
                SpikeVariable variable = new SpikeVariable((SpikeVariableHeader)headers[i]);
                DataFrame frame = new DataFrame(variable, pool, CeresBase.General.Constants.Timeless);

                ushort[] textChans = SONWrapper.GetTextChannels(son.FileID);
                foreach (ushort chan in textChans)
                {
                    SONWrapper.TTextMark[] marks = SONWrapper.GetTextMarkers(son.FileID, chan);
                    foreach (SONWrapper.TTextMark mark in marks)
                    {
                        EpochCentral.AddEpoch(new Epoch(variable, mark.text, (float)Math.Round(mark.time,2), (float)Math.Round(mark.time, 2)) { AppliesToExperiment = true });
                    }
                }

                variable.AddDataFrame(frame);

                vars.Add(variable);
            }
        //    son.Dispose();
            return vars.ToArray();
        }

        #endregion
    }
}
