using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using CeresBase.Settings;
//using Razor.Configuration;
using System.Linq;

namespace ApolloFinal.IO.SON
{
    //[XMLSetting("DataPool", "SONFiles", true, false)]
    class SONDataPool : IDataPool
    {
        private class channelInfo
        {
            public int Channel { get; set; }
            public int Count { get; set; }
        }

        private static Dictionary<string, List<channelInfo>> channelInfos =
            CeresBase.IO.Serialization.NewSerializationCentral.RegisterCollection<Dictionary<string, List<channelInfo>>>(typeof(SONDataPool), "channelInfos");

        private SONFile file;
        private ushort channel;
        private float min = float.MaxValue;
        private float max = float.MinValue;
        private int chanDivide;
        private int total = 0;
        private bool isSet = false;
        private bool minMaxFound = false;
        private SONWrapper.DataType type;

        public SONDataPool(SONFile file, int channel)
        {
            this.file = file;
            this.channel = (ushort)channel;
            this.chanDivide = SONWrapper.GetChanDivide(this.file.FileID, (ushort)channel);
            this.type = SONWrapper.ChannelType(this.file.FileID, (ushort)channel);

            //XMLSettingAttribute attr = SettingsUtilities.GetXMLSettingAttr(typeof(SONDataPool));
            //XmlConfigurationCategory cat = attr.Category.Categories[this.file.FilePath, true];
            
            //XmlConfigurationOption opt = cat.Options[channel.ToString(), true];

            if (!channelInfos.ContainsKey(this.file.FilePath))
            {
                channelInfos.Add(this.file.FilePath, new List<channelInfo>());
            }


            //if (opt.Value == null || (opt.Value is string))
            channelInfo info = channelInfos[this.file.FilePath].FirstOrDefault(ch => ch.Channel == channel);
            if(info == null)
            {

                float[] dataChunk = null;

                int endTime = 0;
                int length = 0;

                while ((dataChunk = SONWrapper.GetData(this.file.FileID, this.channel, endTime, SONWrapper.READ_ALL, out endTime, out length)) != null)
                {
                    for (int i = 0; i < length; i++)
                    {
                        if (dataChunk[i] < this.min) this.min = dataChunk[i];
                        if (dataChunk[i] > this.max) this.max = dataChunk[i];
                        total++;
                    }

                    if (endTime < 0) break;
                }
                this.minMaxFound = true;
                
                //opt.Value = total;
                channelInfos[this.file.FilePath].Add(new channelInfo() { Channel = channel, Count = total });
            }
            else
            {
                total = info.Count;


                //total = (int)opt.Value;
              //  this.minMaxFound = true;
            }
        }

       
        public Type CurrentStorageType
        {
            get {return typeof(float); }
        }

        public float GetDataValue()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public float GetDataValue(int[] indices)
        {
            float toRet = 0;
            int tot = 0;
            this.GetData(null, null,
                delegate(float[] data, uint[] indexes, uint[] amounts)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        if (tot == indices[0]) toRet = data[i];
                        tot++;
                    }
                }
            );
            return toRet;
            //float toRet = 0;
            //this.GetData(indices, new int[] { 1 },
            //    delegate(float[] data, uint[] indexes, uint[] counts)
            //    {
            //        toRet = data[0];
            //    }
            //);
            //return toRet;
        }

        public void GetData(int[] indices, int[] amount, DataUtilities.PoolReadDelegate readFunction)
        {

            //if (this.type == SONWrapper.DataType.RealWave ||
            //    this.type == SONWrapper.DataType.Adc)
            //{

                #region ADC

                int firstTime = 0;
                int lastTime = 0;

                uint[] ufirstIndices = new uint[1];

                if (indices == null)
                {
                    firstTime = 0;
                    ufirstIndices[0] = 0;
                }
                else
                {
                    firstTime = (int)(indices[0] * this.chanDivide);
                    ufirstIndices[0] = (uint)indices[0];
                }

                if (amount == null)
                {
                    lastTime = SONWrapper.GetMaxTime(this.file.FileID, this.channel) - firstTime;
                }
                else lastTime = firstTime + (int)(amount[0] * this.chanDivide);

                int bufferSize = 240000;
                float[] buffer = new float[bufferSize];
                int buffSpot = 0;
                int current = 0;
                int length;

                uint[] ucount = new uint[1];

                float[] data = null;
                while ((data = SONWrapper.GetData(this.file.FileID, this.channel, firstTime, lastTime, out firstTime, out length)) != null)
                {
                    for (int i = 0; i < length; i++, current++)
                    {
                        if (amount != null && current >= amount[0]) break;
                        buffer[buffSpot] = data[i];
                        buffSpot++;
                        if (buffSpot == bufferSize)
                        {
                            ucount[0] = (uint)(bufferSize);
                            readFunction(buffer, ufirstIndices, ucount);
                            buffSpot = 0;
                            ufirstIndices[0] += ucount[0];
                        }
                    }
                    if (firstTime < 0 || (amount != null && current >= amount[0])) break;
                }


                if (buffSpot != 0)
                {
                    float[] toPass = new float[buffSpot];
                    Array.Copy(buffer, 0, toPass, 0, buffSpot);

                    ucount[0] = (uint)buffSpot;
                    readFunction(toPass, ufirstIndices, ucount);
                }

                #endregion
            //}
            //else
            //{

            //}
        }

        public void GetData(int[] firstIndices, int[] lastIndices, int[] strides, DataUtilities.PoolReadStridedDelegate readFunction)
        {
            //if (this.type == SONWrapper.DataType.RealWave ||
            //             this.type == SONWrapper.DataType.Adc)
            //{
                #region ADC
                int firstTime = (int)(firstIndices[0] * this.chanDivide);
                int lastTime = (int)(lastIndices[0] * this.chanDivide);
                int stride = strides[0];

                int bufferSize = 240000;
                float[] buffer = new float[bufferSize];
                int buffSpot = 0;
                int current = 0;
                int length;

                uint[] ufirstIndices = new uint[1];
                ufirstIndices[0] = (uint)firstIndices[0];
                uint[] ulastIndices = new uint[1];
                uint[] ustrides = new uint[1];
                ustrides[0] = (uint)stride;


                float[] data = null;

                while ((data = SONWrapper.GetData(this.file.FileID, this.channel, firstTime, lastTime, out firstTime, out length)) != null)
                {
                    for (int i = 0; i < length; i++, current++)
                    {
                        if (current % stride == 0)
                        {
                            buffer[buffSpot] = data[i];
                            buffSpot++;
                            if (buffSpot == bufferSize)
                            {
                                ulastIndices[0] = (uint)(bufferSize * stride) + ufirstIndices[0];
                                readFunction(buffer, ufirstIndices, ulastIndices, ustrides);
                                buffSpot = 0;
                                ufirstIndices[0] = ulastIndices[0] + 1;
                            }
                        }
                    }
                    if (firstTime < 0) break;
                }


                if (buffSpot != 0)
                {
                    float[] toPass = new float[buffSpot];
                    Array.Copy(buffer, 0, toPass, 0, buffSpot);

                    ulastIndices[0] = (uint)(buffSpot * stride) + ufirstIndices[0];
                    readFunction(toPass, ufirstIndices, ulastIndices, ustrides);
                }
                #endregion
            //}
            //else
            //{

            //}
        }

        #region Not Implemented
        public void FrameAdded()
        {
        }

        public void FrameRemoved()
        {
        }

        public void SetMinMaxManually(float min, float max)
        {
            this.min = min;
            this.max = max;
            this.minMaxFound = true;
        }

        public CompressionChain Chain
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public void PopCompression()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetDataValue(float val)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetDataValue(float val, int[] indices)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetData(System.IO.Stream data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetData(System.IO.Stream data, int[] indices, int[] amounts)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetData(System.IO.Stream data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

      

        public int GetDimensionLength(int index)
        {
            return this.total;
        }

        public int[] DimensionLengths
        {
            get { return new int[] { this.total }; }
        }

        public float Min
        {
            get { return this.min; }
        }

        public float Max
        {
            get { return this.max; }
        }

        public int NumDimensions
        {
            get { return 1; }
        }

        public void DoneSettingData()
        {
            this.isSet = true;
        }

        public bool BeenSet
        {
            get { return this.isSet; }
        }

        public void Dispose()
        {
        }

        #region IDataPool Members


        public void CalcMinMax()
        {
            float[] dataChunk = null;

            int endTime = 0;
            int length = 0;

            while ((dataChunk = SONWrapper.GetData(this.file.FileID, this.channel, endTime, SONWrapper.READ_ALL, out endTime, out length)) != null)
            {
                for (int i = 0; i < length; i++)
                {
                    if (dataChunk[i] < this.min) this.min = dataChunk[i];
                    if (dataChunk[i] > this.max) this.max = dataChunk[i];
                    total++;
                }

                if (endTime < 0) break;
            }
        }

        public bool MinMaxFound
        {
            get { return this.minMaxFound; }
        }

        #endregion
    }
}
