using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.IO.SON
{
    public class SONFile : CeresBase.IO.ICeresFile, IDisposable
    {
        private short fID;
        internal short FileID
        {
            get
            {
                return this.fID;
            }
        }

        private string filename;
        private bool disposed = false;

        public static bool CanRead(string filename)
        {
            if (!filename.Contains(".smr")) return false;

            short id = SONWrapper.OpenFile(filename);
            if (id < 0) return false;
            SONWrapper.CloseFile(id);
            return true;
        }

        public SONFile(string filename)
        {
            this.fID = SONWrapper.OpenFile(filename);
            this.filename = filename;
        }

        public SONFile(string filename, float frequency, int numchannels)
        {
            this.fID = SONWrapper.CreateFile(filename, frequency, numchannels);
            this.filename = filename;
        }



        //~SONFile()
        //{
        //    if (!this.disposed)
        //    {
        //        SONWrapper.CloseFile(this.fID);
        //        this.disposed = true;
        //    }
        //}

        public ushort[] Channels
        {
            get
            {
                return SONWrapper.GetChannels(this.fID);
            }
        }
        

        public Type VariableType
        {
            get { return typeof(SpikeVariable); }
        }

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape); }
        }

        public string FilePath
        {
            get { return this.filename; }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (!this.disposed)
            {
                SONWrapper.CloseFile(this.fID);
                this.disposed = true;
            }
        }

        #endregion
    }
}
