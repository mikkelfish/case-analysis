using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using System.Drawing.Design;
using CeresBase.UI;
using CeresBase.Settings;
//using Razor.Configuration;
using CeresBase.Data;
using System.IO;
using CeresBase.IO.DataBase;

namespace ApolloFinal.IO.DSI
{
    //[XMLSetting("DSI", "", true, false)]
    class DSIReader : ICeresReader
    {
        //private CeresFileDataBase db;

        public object ReadAttribute(ICeresFile file, string name, string attrName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresBase.Data.Variable ReadVariable(string filename, CeresBase.Data.VariableHeader header, DateTime[] times)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        private void readDelegate(char[] buffer, int size, object tag)
        {
            object[] args = tag as object[];
            IDataPool pool = args[0] as IDataPool;
            char[] past = args[1] as char[];
            int buffSpot = (int)args[2];
            bool skip = (bool)args[3];
            for (int i = 0; i < size; i ++)
            {
                if (buffer[i] == '#')
                {
                    skip = true;
                    buffSpot = -1;
                }
                else if (buffer[i] == '\n')
                {
                    if (skip)
                    {
                        skip = false;
                        buffSpot = 0;
                    }
                    else
                    {
                        string create = new string(past, 0, buffSpot);
                        float val = 0;
                        if (!float.TryParse(create, out val))
                        {
                            val = CeresBase.Data.DataConstants.GetMissingValue<float>();
                        }
                        pool.SetDataValue(val);
                        skip = true;
                        buffSpot = 0;
                    }
                }
                else if (buffer[i] == ',' && buffSpot != -1)
                {
                    skip = false;
                    buffSpot = 0;
                }
                else if (!skip)
                {
                    past[buffSpot] = buffer[i];
                    buffSpot++;
                }              
            }

            args[2] = buffSpot;
            args[3] = skip;
        }

        //public CeresBase.Data.Variable[] ReadVariables(string filename, CeresBase.Data.VariableHeader[] headers, DateTime[][] times)
        //{
        //    List<Variable> vars = new List<Variable>();
        //    foreach (VariableHeader header in headers)
        //    {
        //        DSIFile dsiFile = new DSIFile(filename);
        //        FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read);
        //        StreamReader reader = new StreamReader(stream);

        //        IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { dsiFile.NumLines });

        //        object[] args = new object[4];
        //        args[0] = pool;
        //        args[1] = new char[40];
        //        args[2] = 0;
        //        args[3] = true;
        //        CeresBase.General.Utilities.ReadTextFromFileStream(reader, -1, 1000000, new CeresBase.General.Utilities.TextStreamAction(this.readDelegate),
        //            args);

        //        reader.Close();
        //        stream.Close();

        //        Variable var = new SpikeVariable(header as SpikeVariableHeader);
        //        DataFrame frame = new DataFrame(var, pool, new CeresBase.Data.DataShapes.NoShape(),
        //            CeresBase.General.Constants.Timeless);
        //        var.AddDataFrame(frame);
        //        vars.Add(var);

        //        //XMLSettingAttribute attr = CeresBase.Settings.SettingsUtilities.GetXMLSettingAttr(typeof(DSIReader));
        //        //XmlConfigurationCategory events = attr.Category.Categories["Events"];
        //        //if (events == null) continue;
        //        //XmlConfigurationOption opt = events.Options[header.ExperimentName];
        //        //if (opt == null) continue;
        //        //string eventFile = (string)opt.Value;



        //    }
        //    return vars.ToArray();
        //}

        public byte[] ReadBinary(ICeresFile file, string name)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.db;
        //    }
        //    set
        //    {
        //        this.db = value;
        //    }
        //}

        public double[][] GetShapeValues(ICeresFile file)
        {
            return null;
        }

        //public CeresBase.Data.VariableHeader[] GetAllVariableHeaders(string file, string experimentName)
        //{
        //    XmlConfigurationCategory cat = CeresBase.Settings.SettingsUtilities.GetXMLSettingAttr(typeof(DSIReader)).Category;
        //    DSIFile dsiFile = new DSIFile(file);
        //    XmlConfigurationCategory varCat = cat.Categories["Var Names"];
        //    if (varCat == null)
        //    {
        //        this.db.DeleteFile(file);
        //        return null;
        //    }
        //    XmlConfigurationOption varOpt = varCat.Options[experimentName];
        //    if (varOpt == null)
        //    {
        //        this.db.DeleteFile(file);
        //        return null;
        //    }

        //    return new VariableHeader[]{ new SpikeVariableHeader(varOpt.Value as string, "Raw", experimentName, (double)(1.0 / dsiFile.Resolution),
        //        SpikeVariableHeader.SpikeType.Integrated)};

        //    //return null;
        //    //DSIFile file = new DSIFile(file);

        //}

        public bool Supported(string file)
        {
            return DSIFile.IsSupported(file);
        }

        //public void AddFilesToDB(string[] files)
        //{
        //    XMLSettingAttribute attr = CeresBase.Settings.SettingsUtilities.GetXMLSettingAttr(typeof(DSIReader));
        //    foreach (string file in files)
        //    {
        //        DSIFile dsiFile = new DSIFile(file);

        //        CeresBase.UI.RequestInformation infoReq = new CeresBase.UI.RequestInformation();
        //        List<Type> types = new List<Type>();
        //        List<string> names = new List<string>();
        //        List<string> descriptions = new List<string>();
        //        List<string> cats = new List<string>();
        //        List<Type> editors = new List<Type>();

        //        types.Add(typeof(string));
        //        names.Add("Variable Name");
        //        cats.Add("DSI");
        //        descriptions.Add("Please enter the variable name.");
        //        editors.Add(null);

        //        //   FileNameEditor

        //        types.Add(typeof(string));
        //        names.Add("Experiment Name");
        //        cats.Add("DSI");
        //        descriptions.Add("Please enter the name of the experiment the file " + file + " is from.");
        //        editors.Add(null);

        //        types.Add(typeof(string));
        //        names.Add("Event File");
        //        cats.Add("DSI");
        //        descriptions.Add("Select the optional file that contains experiment events.");
        //        editors.Add(typeof(FileSelectEditor));
        //        infoReq.Grid.SetInformationToCollect(types.ToArray(), null, names.ToArray(), descriptions.ToArray(),
        //            editors.ToArray(), null);
        //        if (infoReq.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return;


        //        XmlConfigurationCategory cat = attr.Category;
        //        XmlConfigurationCategory eventInfos = cat.Categories["Events", true];
        //        XmlConfigurationOption opt = eventInfos.Options[(string)infoReq.Grid.Results["Experiment Name"], true,
        //            (string)infoReq.Grid.Results["Event File"]];
        //        XmlConfigurationCategory varNames = cat.Categories["Var Names", true];
        //        XmlConfigurationOption varOpt = varNames.Options[(string)infoReq.Grid.Results["Experiment Name"], true,
        //            (string)infoReq.Grid.Results["Variable Name"]];
                
        //        this.db.CreateFile((string)infoReq.Grid.Results["Experiment Name"], dsiFile);

        //    }
        //    attr.WriteToDisk();
        //}

        #region ICeresReader Members


        public bool StayNative
        {
            get { return false; }
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            FileDataBaseEntryParameter eventFile = new FileDataBaseEntryParameter() { Name = "Event File", UserEditable = true, EditorType = typeof(FileSelectEditor) };
            eventFile.Value = "";
            return new FileDataBaseEntryParameter[] { eventFile };
        }

        #endregion

        #region ICeresReader Members


        public FileDataBaseVariableEntryParameter[] GetVariableParameters(FileDataBaseEntry entry)
        {


            DSIFile dsiFile = new DSIFile(entry.Filepath);

            SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
            para.Units.Value = "Raw";
            para.Frequency.Value = (double)(1.0 / dsiFile.Resolution);
            para.Frequency.IsFixed = true;
            para.SpikeType.Value = SpikeVariableHeader.SpikeType.Integrated;
            para.SpikeType.IsFixed = true;

            return new FileDataBaseVariableEntryParameter[]{para};
        }

        #endregion



        #region ICeresReader Members


        public Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
