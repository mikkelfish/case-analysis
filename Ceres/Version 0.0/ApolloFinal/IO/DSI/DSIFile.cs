using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using System.IO;
using CeresBase.Settings;
//using Razor.Configuration;

namespace ApolloFinal.IO.DSI
{
    class DSIFile : ICeresFile
    {
        private static Dictionary<string, int> numLinesDict =
            CeresBase.IO.Serialization.NewSerializationCentral.RegisterCollection<Dictionary<string, int>>(typeof(DSIFile), "numLinesDict");

        private string filename;

        public Type VariableType
        {
            get { return typeof(SpikeVariable); }
        }

        private float resolution;
        public float Resolution
        {
            get { return resolution; }
        }

        private int numLines;
        public int NumLines
        {
            get { return numLines; }
        }

        public static bool IsSupported(string file)
        {
            try
            {
                using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string first = reader.ReadLine();
                        if (first == null) return false;
                        if (first.Length == 17 && first.Contains(",")) return true;
                    }
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
	

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape); }
        }

        public string FilePath
        {
            get { return this.filename; }
        }


        private void countLines(char[] buffer, int count, object tag)
        {
            int[] numLine = tag as int[];
            bool skip = false;
            for (int i = 0; i < count; i++)
            {
                if (buffer[i] == '#')
                {
                    skip = true;
                }

                if (buffer[i] == '\n')
                {
                    if (!skip) numLine[0]++;
                    else skip = false;
                }

            }
        }

        public DSIFile(string file)
        {
            this.filename = file;
            FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(stream);

            //XMLSettingAttribute attr = CeresBase.Settings.SettingsUtilities.GetXMLSettingAttr(typeof(DSIReader));
            //XmlConfigurationCategory numLines = attr.Category.Categories["NumLines", true];
           
            //if (numLines.Options.Contains(file))
            if(numLinesDict.ContainsKey(file))
            {
                //XmlConfigurationOption opt = numLines.Options[file];
                //this.numLines = (int)opt.Value;
                this.numLines = numLinesDict[file];
            }
            else
            {

                int[] numLine = new int[] { 0 };
                CeresBase.General.Utilities.ReadTextFromFileStream(reader, -1, 100000, new CeresBase.General.Utilities.TextStreamAction(countLines),
                    numLine);
                this.numLines = numLine[0];
                //XmlConfigurationOption opt = numLines.Options[file, true, this.numLines];
                numLinesDict.Add(file, this.numLines);
            }

            stream.Seek(0, SeekOrigin.Begin);

            string first = reader.ReadLine();
            string[] split = first.Split(',');
            float time1 = float.Parse(split[0]);

            string second = reader.ReadLine();
            string[] split2 = second.Split(',');
            float time2 = float.Parse(split2[0]);
            this.resolution = time2 - time1;

            reader.Close();
            stream.Close();
        }
    }
}
