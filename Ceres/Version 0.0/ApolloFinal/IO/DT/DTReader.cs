using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using CeresBase.Development;
using System.IO;
using CeresBase.Data;
using CeresBase.IO.DataBase;

namespace ApolloFinal.IO.DT
{
    public class DTReader : ICeresReader
    {
        //private CeresFileDataBase database;

        public object ReadAttribute(ICeresFile file, string name, string attrName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresBase.Data.Variable ReadVariable(string filename, VariableHeader header, DateTime[] times)
        //{
        //    return this.ReadVariables(filename, new VariableHeader[] { header }, new DateTime[][] { times })[0];           
        //}

        //public CeresBase.Data.Variable[] ReadVariables(string filename, VariableHeader[] headers, DateTime[][] times)
        //{
        //    DTFile file = new DTFile(filename);
        //    Dictionary<string, List<float>> values = new Dictionary<string, List<float>>();
        //    for (int i = 0; i < headers.Length; i++)
        //    {
        //        values.Add(headers[i].VarName, new List<float>());
        //    }

        //    using (FileStream stream = new FileStream(file.FilePath, FileMode.Open, FileAccess.Read))
        //    {
        //        using (StreamReader reader = new StreamReader(stream))
        //        {
        //            reader.ReadLine();
        //            reader.ReadLine();

        //            string line = null;
        //            while ((line = reader.ReadLine()) != null)
        //            {
        //                string trimmed = line.Trim();
        //                string[] splits = trimmed.Split(' ');

        //                int channel = int.Parse(splits[0]);
        //                if (channel < 4096)
        //                {
        //                    string var = file.CellPrefix + splits[0];
        //                    if (!values.ContainsKey(var)) continue;
        //                    float value = float.Parse(splits[splits.Length - 1]);
        //                    values[var].Add(value);
        //                }
        //                else
        //                {
        //                    string var = file.IntegratedPrefix + (channel / 4096).ToString();
        //                    if (!values.ContainsKey(var)) continue;
        //                    float value = channel % 4096;
        //                    if (value < 2048) value += 4096;
        //                    values[var].Add(value);
        //                }
        //            }
        //        }
        //    }

        //    List<Variable> vars = new List<Variable>();
        //    for (int i = 0; i < values.Keys.Count; i++)
        //    {
        //        string var = headers[i].VarName;
        //        if (values[var].Count == 0) continue;
        //        SpikeVariable variable = new SpikeVariable((SpikeVariableHeader)headers[i]);
        //        IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { values[var].Count });
        //        pool.SetData(DataUtilities.ArrayToMemoryStream(values[var].ToArray()));
        //        variable.AddDataFrame(new DataFrame(variable, pool, new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless));
        //        vars.Add(variable);
        //    }
        //    return vars.ToArray();
        //}


        public byte[] ReadBinary(ICeresFile file, string name)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}


        public double[][] GetShapeValues(ICeresFile file)
        {
            return null;
        }

        public VariableHeader[] GetAllVariableHeaders(string file, string experimentName)
        {
            DTFile dtFile = new DTFile(file);
            VariableHeader[] headers = new VariableHeader[dtFile.VariableNames.Length];
            for (int i = 0; i < dtFile.VariableNames.Length; i++)
            {
                if (!dtFile.VariableNames[i].Contains(dtFile.IntegratedPrefix))
                {
                    headers[i] = new SpikeVariableHeader(dtFile.VariableNames[i],
                        "Events", experimentName, dtFile.Frequency  , SpikeVariableHeader.SpikeType.Pulse);
                }
                else
                {
                    headers[i] = new SpikeVariableHeader(dtFile.VariableNames[i],
                        "Integrated Values", experimentName, (dtFile.Frequency/ dtFile.IntegratedSpacing), SpikeVariableHeader.SpikeType.Integrated);
                }
            }
            return headers;
        }

        public bool Supported(string file)
        {
            return DTFile.IsSupported(file);
        }

        //public void AddFilesToDB(string[] files)
        //{
        //    //TODO HANDLE MULTIPLE FILES
        //    CeresBase.UI.RequestInformation infoReq = new CeresBase.UI.RequestInformation();
        //    List<Type> types = new List<Type>();
        //    List<string> names = new List<string>();
        //    List<string> descriptions = new List<string>();

        //    types.Add(typeof(string));
        //    names.Add("Experiment Name");
        //    descriptions.Add("Please enter the name of the experiment this data is from.");

        //    for (int i = 0; i < files.Length; i++)
        //    {
        //        types.Add(typeof(string));
        //        names.Add("Channel Number " + i.ToString());
        //        descriptions.Add("Please enter the channel for " + files[i] + ". Enter * for all channels");

        //    }

           

        //    infoReq.Grid.SetInformationToCollect(types.ToArray(), null, names.ToArray(), descriptions.ToArray(), null, null);
        //    if (infoReq.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
        //        return;


        //    DTFile[] dataFiles = new DTFile[files.Length];
        //    for (int i = 0; i < files.Length; i++)
        //    {
        //        dataFiles[i] = new DTFile(files[i]);
        //        string channel = infoReq.Grid.Results["Channel Number " + i.ToString()]  as string;
        //        string exptName = infoReq.Grid.Results["Experiment Name"] as string + "&&" + "Chan: " +
        //            (channel == "*" ? "all" : channel);
        //        this.Database.CreateFile(exptName, dataFiles[i]);

        //    }
        //}

        #region ICeresReader Members


        public bool StayNative
        {
            get { return false; }
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            return null;
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseVariableEntryParameter[] GetVariableParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            DTFile dtFile = new DTFile(entry.Filepath);
            FileDataBaseVariableEntryParameter[] paras = new FileDataBaseVariableEntryParameter[dtFile.VariableNames.Length];
            for (int i = 0; i < dtFile.VariableNames.Length; i++)
            {
                SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
                if (!dtFile.VariableNames[i].Contains(dtFile.IntegratedPrefix))
                {
                    para.VariableName.Value = dtFile.VariableNames[i];
                    para.VariableName.IsFixed = true;

                    para.Units.Value = "Events";

                    para.Frequency.Value = dtFile.Frequency;
                    para.Frequency.IsFixed = true;

                    para.SpikeType.Value = SpikeVariableHeader.SpikeType.Pulse;
                    para.SpikeType.IsFixed = true;
                }
                else
                {
                    para.VariableName.Value = dtFile.VariableNames[i];
                    para.VariableName.IsFixed = true;

                    para.Units.Value = "Integrated Values";

                    para.Frequency.Value = dtFile.Frequency / dtFile.IntegratedSpacing;
                    para.Frequency.IsFixed = true;

                    para.SpikeType.Value = SpikeVariableHeader.SpikeType.Integrated;
                    para.SpikeType.IsFixed = true;
                }

                paras[i] = para;
            }

            return paras;
        }

        #endregion



        #region ICeresReader Members


        public Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers)
        {
            DTFile file = new DTFile(filename);
            Dictionary<string, List<float>> values = new Dictionary<string, List<float>>();
            for (int i = 0; i < headers.Length; i++)
            {
                values.Add(headers[i].VarName, new List<float>());
            }

            using (FileStream stream = new FileStream(file.FilePath, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    reader.ReadLine();
                    reader.ReadLine();

                    string line = null;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string trimmed = line.Trim();
                        string[] splits = trimmed.Split(' ');

                        int channel = int.Parse(splits[0]);
                        if (channel < 4096)
                        {
                            string var = file.CellPrefix + splits[0];
                            if (!values.ContainsKey(var)) continue;
                            float value = float.Parse(splits[splits.Length - 1]);
                            values[var].Add(value);
                        }
                        else
                        {
                            string var = file.IntegratedPrefix + (channel / 4096).ToString();
                            if (!values.ContainsKey(var)) continue;
                            float value = channel % 4096;
                            if (value < 2048) value += 4096;
                            values[var].Add(value);
                        }
                    }
                }
            }

            List<Variable> vars = new List<Variable>();
            for (int i = 0; i < values.Keys.Count; i++)
            {
                string var = headers[i].VarName;
                if (values[var].Count == 0) continue;
                SpikeVariable variable = new SpikeVariable((SpikeVariableHeader)headers[i]);
                IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { values[var].Count });
                pool.SetData(DataUtilities.ArrayToMemoryStream(values[var].ToArray()));
                variable.AddDataFrame(new DataFrame(variable, pool,  CeresBase.General.Constants.Timeless));
                vars.Add(variable);
            }
            return vars.ToArray();
        }

        #endregion
    }
}
