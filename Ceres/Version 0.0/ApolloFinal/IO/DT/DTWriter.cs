using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using CeresBase.Data;
using System.IO;
using System.Windows.Forms;
using System.Globalization;

namespace ApolloFinal.IO.DT
{
    public class DTWriter : ICeresWriter
    {
        //private CeresFileDataBase db;

        public void AddAttribute(ICeresFile file, string name, string attrName, object value)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ICeresFile[] WriteVariable(CeresBase.Data.Variable variable, string writeToDirectory)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ICeresFile[] WriteVariables(CeresBase.Data.Variable[] variables, string writeToDirectory)
        {
            SaveFileDialog diag = new SaveFileDialog();
            diag.DefaultExt = ".bdt";
            diag.AddExtension = true;
            if(Directory.Exists(writeToDirectory)) diag.InitialDirectory = writeToDirectory;
            diag.Filter = "BDT(*.bdt)|*.bdt|All Files(*.*)|*.*";
            if (diag.ShowDialog() == DialogResult.Cancel) return null;

            List<string> integrated = new List<string>();
            List<string> cells = new List<string>();

            double maxTime = double.MinValue;


            foreach (SpikeVariable var in variables)
            {
                if (var.SpikeType == SpikeVariableHeader.SpikeType.Pulse)
                {
                    cells.Add(var.FullVarName);
                }
                else integrated.Add(var.FullVarName);

                if (var.TotalLength > maxTime)
                {
                    maxTime = var.TotalLength;
                }
            }

            double[] resolutions = new double[variables.Length];
            double[] minInts = new double[integrated.Count];
            double[] maxInts = new double[integrated.Count];

            List<float>[] dataToWrite = new List<float>[variables.Length];
            for (int i = 0; i < dataToWrite.Length; i++)
            {
                dataToWrite[i] = new List<float>();
            }

            for (int i = 0; i < integrated.Count; i++)
            {
                for (int j = 0; j < variables.Length; j++)
                {
                    if (variables[j].FullVarName != integrated[i]) continue;
                    (variables[j][0] as DataFrame).Pool.GetData(null, null,
                        delegate(float[] data, uint[] indexes, uint[] counts)
                        {
                            dataToWrite[i].AddRange(data);
                        }
                    );
                    resolutions[i] = (variables[j] as SpikeVariable).Resolution;
                    minInts[i] = (variables[j][0] as DataFrame).Min;
                    maxInts[i] = (variables[j][0] as DataFrame).Max;
                }
            }

            for (int i = 0; i < cells.Count; i++)
            {
                for (int j = 0; j < variables.Length; j++)
                {
                    if (variables[j].FullVarName != cells[i]) continue;
                    (variables[j][0] as DataFrame).Pool.GetData(null, null,
                        delegate(float[] data, uint[] indexes, uint[] counts)
                        {
                            dataToWrite[i + integrated.Count].AddRange(data);
                        }
                    );
                    resolutions[i + integrated.Count] = (variables[j] as SpikeVariable).Resolution;

                }
            }


            int[] indices = new int[variables.Length];
            bool[] completed = new bool[variables.Length];
            double[] curTimes = new double[integrated.Count];

            using (FileStream stream = new FileStream(diag.FileName, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    char[] buffer = new char[512000];
                    int curIndex = 0;
                    double dtres = 10.0/DTFile.BDTFreq;
                    double curIntTime = 0;

                    string timeFormat = "{0,7:#######}";
                    string cellFormat = "{0,5:######}";
                    int dtCount = 0;

                    writer.WriteLine(DTFile.BDTHeader);
                    writer.WriteLine(DTFile.BDTHeader);

                    while (true)
                    {
                        float minTime = float.MaxValue;
                        int which = -1;
                        for (int i = 0; i < cells.Count; i++)
                        {
                            if(completed[i + integrated.Count]) continue;
                            if (dataToWrite[i + integrated.Count][indices[i + integrated.Count]] < minTime)
                            {
                                minTime = dataToWrite[i + integrated.Count][indices[i + integrated.Count]];
                                which = i;
                            }
                        }

                        string values = "";
                        if (which == -1)
                        {
                            //Write integrated
                            for (int i = 0; i < integrated.Count; i++)
                            {
                                if (completed[i]) continue;
                                while (curTimes[i] < curIntTime)
                                {
                                    curTimes[i] += resolutions[i];
                                    indices[i]++;
                                    if (indices[i] >= dataToWrite[i].Count)
                                    {
                                        completed[i] = true;
                                        break;
                                    }
                                }

                                if (completed[i]) break;

                                    //string var = file.IntegratedPrefix + (channel / 4096).ToString();
                                    //if (!values.ContainsKey(var)) continue;
                                    //float value = channel % 4096;
                                    //if (value < 2048) value += 4096;

                                    float perc = (float)((dataToWrite[i][indices[i]] - minInts[i]) / (maxInts[i] - minInts[i]));
                                    float value = 4096 * perc + 2048;
                                    if (value >= 4096)
                                        value -= 4096;
                                    value += (i + 1) * 4096;
                                    value = (int)value;

                                    values = String.Format(cellFormat, value) + " " +
                                        (dtCount == 0 ? "    0" : String.Format(timeFormat, dtCount)) + "\n";

                                //float value = channel % 4096;
                                //if (value < 2048) value += 4096;

                                //float value = i * 4096;
                                //if (value < 2048) value += 4096;
                                //values = ( ).ToString(cellFormat) + " " +
                                //    dataToWrite[which + integrated.Count][indices[which + integrated.Count]].ToString(format) + "\n";
                            }
                            dtCount += 10;
                            curIntTime += dtres;
                        }
                        else
                        {
                            
                            float val = dataToWrite[which + integrated.Count][indices[which + integrated.Count]];
                            val *= (float)resolutions[which + integrated.Count];
                            val *= DTFile.BDTFreq;

                            //Write cell
                            values = String.Format(cellFormat, (which + 1)) + " " +
                                String.Format(timeFormat, val) + "\n";
                            indices[which + integrated.Count]++;
                            if (indices[which + integrated.Count] >= dataToWrite[which + integrated.Count].Count)
                            {
                                completed[which + integrated.Count] = true;
                            }
                        }

                        if (values.Length + curIndex > buffer.Length)
                        {
                            writer.Write(buffer, 0, curIndex);
                            curIndex = 0;
                        }
                        else
                        {
                            values.CopyTo(0, buffer, curIndex, values.Length);
                            curIndex += values.Length;
                        }

                        bool cont = false;
                        for (int i = 0; i < completed.Length; i++)
                        {
                            if (!completed[i])
                            {
                                cont = true;
                                break;
                            }
                        }
                        if (!cont) break;
                    }

                    writer.Write(buffer, 0, curIndex);
                }
            }

            return new ICeresFile[] { new DTFile(diag.FileName) };
        }

        public override string ToString()
        {
            return "BDT";
        }

        public void WriteBinary(ICeresFile file, string name, byte[] data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.db;
        //    }
        //    set
        //    {
        //        this.db = value;
        //    }
        //}

        public void Dispose()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #region ICeresWriter Members


        public void AddAttribute(string fileName, string name, string attrName, object value)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
