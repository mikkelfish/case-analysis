using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;
using CeresBase.IO;
using System.IO;

namespace ApolloFinal.IO.DT
{
    public class DTFile : ICeresFile
    {
        public const string BDTHeader = "   11 1111111";
        public const string BDTHeader2 = "   11   11111";
        public const int BDTFreq = 2000;

        public const string EDTHeader = "   33   3333333";
        public const int EDTFreq = 10000;

        private int frequency;
        private string filename;
        private List<int> cells;
        private List<int> integrated;

        public string CellPrefix
        {
            get
            {
                return "Cell ";
            }
        }

        public string IntegratedPrefix
        {
            get
            {
                return "Integrated Channel ";
            }
        }

        public int Frequency
        {
            get
            {
                return frequency;
            }
        }

        public string FilePath
        {
            get
            {
                return this.filename;
            }
        }

        private int integratedSpacing;
        public int IntegratedSpacing
        {
            get { return integratedSpacing; }
        }
	

        public static bool IsSupported(string filename)
        {
            try
            {
                using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        bool toRet = false;
                        string prev = "";
                        for (int i = 0; i < 2; i++)
                        {
                            string line = reader.ReadLine();
                            if (line == null) return false;
                            if (line == BDTHeader || line == BDTHeader2 || line == EDTHeader)
                            {
                                if (prev == "") prev = line;
                                else if (line == prev) toRet = true;
                            }
                        }
                        return toRet;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public DTFile(string filename)
        {
            this.filename = filename;
            FileStream stream = new FileStream(this.filename, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(stream);
            try
            {
                for (int i = 0; i < 2; i++)
                {
                    string line = reader.ReadLine();
                    if (line == BDTHeader || line == BDTHeader2)
                    {
                        this.frequency = BDTFreq;
                    }
                    else if (line == EDTHeader)
                    {
                        this.frequency = EDTFreq;
                    }
                    else throw new Exception("File not supported");
                }

                string read = "";
                this.cells = new List<int>();
                this.integrated = new List<int>();
                while ((read = reader.ReadLine()) != null)
                {
                    string trimmed = read.Trim();
                    string[] parts = trimmed.Split(' ');
                    int channel = int.Parse(parts[0]);
                    if (channel < 1024)
                    {
                        if (!this.cells.Contains(channel)) this.cells.Add(channel);
                    }
                    else
                    {
                        int integratedChannel = channel / 4096;
                        if (!this.integrated.Contains(integratedChannel)) 
                            this.integrated.Add(integratedChannel);
                        if (integratedSpacing == 0)
                        {
                            int time = int.Parse(parts[parts.Length - 1]);
                            if (time != 0)
                            {
                                integratedSpacing = time;
                            }
                        }
                    }
                }
            }
            finally
            {
                reader.Close();
            }

            List<string> varNames = new List<string>();
            for (int i = 0; i < this.cells.Count; i++)
            {
                varNames.Add(this.CellPrefix + this.cells[i].ToString());
            }

            for (int i = 0; i < this.integrated.Count; i++)
            {
                varNames.Add(this.IntegratedPrefix + this.integrated[i].ToString());
            }

            this.varNames = varNames.ToArray();        
        }

        private string[] varNames;
        public string[] VariableNames
        {
            get 
            {
                return this.varNames;
            }
        }

        public DateTime[] Times
        {
            get { return null; }
        }

        public Type VariableType
        {
            get { return typeof(SpikeVariable); }
        }

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape); }
        }
    }
}
