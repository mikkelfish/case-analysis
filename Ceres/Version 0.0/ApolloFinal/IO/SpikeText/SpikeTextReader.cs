using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using System.IO;
using CeresBase.Data;
using CeresBase.IO.DataBase;
using System.Linq;

namespace ApolloFinal.IO.SpikeText
{
    class SpikeTextReader : ICeresReader
    {
        #region ICeresReader Members

        public object ReadAttribute(ICeresFile file, string name, string attrName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresBase.Data.Variable ReadVariable(string filename, CeresBase.Data.VariableHeader header, DateTime[] times)
        //{
        //    return this.ReadVariables(filename, new CeresBase.Data.VariableHeader[] { header }, null)[0];
        //}

        //public CeresBase.Data.Variable[] ReadVariables(string filename, CeresBase.Data.VariableHeader[] headers, DateTime[][] times)
        //{
        //    SpikeTextFile textFile = new SpikeTextFile(filename);

        //    Dictionary<string, object> settings = this.Database.GetSettings(filename);
            
        //    string countStr = (string)settings["Count"];
        //    string[] splitCounts = countStr.Split(',');
        //    int[] counts = new int[splitCounts.Length];
        //    for (int i = 0; i < counts.Length; i++)
        //    {
        //        counts[i] = int.Parse(splitCounts[i]);
        //    }


        //    IDataPool[] pools = new IDataPool[textFile.VariableNames.Length];
        //    float[] running = new float[textFile.VariableNames.Length];
        //    for (int i = 0; i < pools.Length; i++)
        //    {
        //       if(i == 1 || i == 2) pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] {counts[i] + 1});
        //       else pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { counts[i] });
        //       //if(i != 1 && i != 2) pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] {count});
        //       //else pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { count + 1 });
        //    }

        //    bool first = true;
        //    using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
        //    {
        //        using (StreamReader reader = new StreamReader(stream))
        //        {
        //            string line = reader.ReadLine();
        //            line = reader.ReadLine();
        //            while ( line != null)
        //            {
        //                string[] splits = line.Split('\t');

        //                if (splits.Length != pools.Length) break;

        //                for (int i = 0; i < splits.Length; i++)
        //                {
        //                    float val = 0;
        //                    if (float.TryParse(splits[i], out val))
        //                    {
        //                        if (val == 0) continue;

        //                        //if (val != 0)
        //                        //{
        //                        if (i == 1 || i == 2)
        //                        {
        //                            if (first)
        //                            {
        //                                pools[i].SetDataValue(0);
        //                                first = false;
        //                            }
        //                            if (i == 1) pools[2].SetDataValue(val + running[1]);
        //                            else pools[1].SetDataValue(val + running[1]);
        //                            running[1] += val;
        //                            running[2] += val;
        //                        }
        //                        else
        //                        {
        //                            pools[i].SetDataValue(val + running[i]);
        //                            running[i] += val;
        //                        }
        //                        //}
        //                    }
        //                    else pools[i].SetDataValue(CeresBase.Data.DataConstants.GetMissingValue<float>());
        //                }

        //                line = reader.ReadLine();
        //            }
        //        }
        //    }

        //    SpikeVariable[] vars = new SpikeVariable[headers.Length];
        //    for (int i = 0; i < headers.Length; i++)
        //    {
        //        int which = -1;
        //        for (int j = 0; j < textFile.VariableNames.Length; j++)
        //        {
        //            if (textFile.VariableNames[j] == headers[i].VarName)
        //            {
        //                which = j;
        //                break;
        //            }
        //        }

        //        if (which == 2)
        //        {
        //            pools[which].SetDataValue(running[2]);
        //        }

        //        vars[i] = new SpikeVariable(headers[i] as SpikeVariableHeader);
        //        DataFrame frame = new DataFrame(vars[i], pools[which], new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless);
        //        vars[i].AddDataFrame(frame);
        //    }


        //    return vars;
        //}

        //public byte[] ReadBinary(ICeresFile file, string name)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //private CeresFileDataBase database;
        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}

        public double[][] GetShapeValues(ICeresFile file)
        {
            return null;
        }

        public CeresBase.Data.VariableHeader[] GetAllVariableHeaders(string file, string experimentName)
        {
            SpikeTextFile tfile = new SpikeTextFile(file);
            List<SpikeVariableHeader> headers = new List<SpikeVariableHeader>();
            foreach (string var in tfile.VariableNames)
            {
                headers.Add(new SpikeVariableHeader(var, "Intervals", experimentName, 1.0, SpikeVariableHeader.SpikeType.Pulse));
            }
            return headers.ToArray();
        }

        public bool Supported(string file)
        {
            if (file == null) return false;
            int index = file.LastIndexOf('.');
            if (index < 0) return false;
            return file.Substring(index) == ".interval";
        }

        //private void countCallback(char[] characters, int count, object tag)
        //{
        //    int dcount = (int)(tag as object[])[0];
        //    char delim = (char)(tag as object[])[1];

        //    for (int i = 0; i < count; i++)
        //    {
        //        if (characters[i] == delim)
        //        {
        //            dcount++;
        //        }
        //    }
        //    (tag as object[])[0] = dcount;
        //}

        private int[] countPoints(string file)
        {
            //int count = 0;

            //char delim = '\n';
           
            //object[] opts = new object[] { count, delim };
            //using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            //{
            //    using (StreamReader reader = new StreamReader(stream))
            //    {
            //        CeresBase.General.Utilities.ReadTextFromFileStream(reader,
            //            -1, 256000, new CeresBase.General.Utilities.TextStreamAction(this.countCallback),
            //           opts);
            //    }
            //}
            //return (int)opts[0];

            List<int> tot = new List<int>();
            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string line = reader.ReadLine();
                    line = reader.ReadLine();
                    while (line != null)
                    {
                        string[] splits = line.Split('\t');
                        if (splits.Length < 2) continue;
                        if (tot.Count == 0)
                        {
                            for (int i = 0; i < splits.Length; i++)
                            {
                                tot.Add(0);                                
                            }
                        }

                        for (int i = 0; i < splits.Length; i++)
                        {
                            float val = 0;
                            if (!float.TryParse(splits[i], out val))
                                break;
                            if (val != 0)
                                tot[i]++;
                        }
                        line = reader.ReadLine();
                    }
                }
            }
            return tot.ToArray();
        }

        //public void AddFilesToDB(string[] files)
        //{
        //    foreach (string file in files)
        //    {
        //        CeresBase.UI.RequestInformation infoReq = new CeresBase.UI.RequestInformation();
        //        List<Type> types = new List<Type>();
        //        List<string> names = new List<string>();
        //        List<string> descriptions = new List<string>();

        //        types.Add(typeof(string));
        //        names.Add("Experiment Name");
        //        descriptions.Add("Please enter the name of the experiment the file " + file + " is from.");

        //        //types.Add(typeof(string));
        //        //names.Add("Variable Name");
        //        //descriptions.Add("Enter the variable name.");

        //        //types.Add(typeof(double));
        //        //names.Add("Frequency");
        //        //descriptions.Add("Freq");

        //        //types.Add(typeof(string));
        //        //names.Add("Delimiter");
        //        //descriptions.Add("Ret (Return), Tab, Space or character");

        //        infoReq.Grid.SetInformationToCollect(types.ToArray(), null, names.ToArray(), descriptions.ToArray(),
        //            null, null);

        //        if (infoReq.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) continue;



        //        int[] counts = this.countPoints(file);
        //        string all = "";
        //        for (int i = 0; i < counts.Length; i++)
        //        {
        //            all += counts[i] + ",";
        //        }
        //        all = all.Substring(0, all.Length - 1);

        //        SpikeTextFile tfile = new SpikeTextFile(file);
        //        this.Database.CreateFile(infoReq.Grid.Results["Experiment Name"] as string, tfile);

        //        this.database.SetSettings(file, new string[] { "Count" },
        //           new object[] {all});


        //    }
        //}

        #endregion

        #region ICeresReader Members


        public bool StayNative
        {
            get { return false; }
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(FileDataBaseEntry entry)
        {
            FileDataBaseEntryParameter countParameter = new FileDataBaseEntryParameter() { Name = "Count", UserEditable = false };
            //int[] counts = this.countPoints(entry.Filepath);
            //string all = "";
            //for (int i = 0; i < counts.Length; i++)
            //{
            //    all += counts[i] + ",";
            //}
            //all = all.Substring(0, all.Length - 1);

            //countParameter.Value = all;
            //countParameter.SuccessfullySet = true;

            return new FileDataBaseEntryParameter[] { countParameter };
        }

        #endregion

        #region ICeresReader Members


        public FileDataBaseVariableEntryParameter[] GetVariableParameters(FileDataBaseEntry entry)
        {           
            SpikeTextFile tfile = new SpikeTextFile(entry.Filepath);
            tfile.LoadVarNames();

            List<FileDataBaseVariableEntryParameter> paras = new List<FileDataBaseVariableEntryParameter>();
            foreach (string var in tfile.VariableNames)
            {
                SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
                para.Units.Value = "Intervals";
                para.VariableName.Value = var;
                para.VariableName.IsFixed = true;

                para.Frequency.Value = 1.0;
                para.Frequency.IsFixed = true;

                para.SpikeType.Value = SpikeVariableHeader.SpikeType.Pulse;
                para.SpikeType.IsFixed = true;

                paras.Add(para);
            }

            return paras.ToArray();
        }

        #endregion

        #region ICeresReader Members


        public Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers)
        {
            SpikeTextFile textFile = new SpikeTextFile(filename);
            textFile.LoadVarNames();

            FileDataBaseEntryParameter countParameter = fileparams.Single(fp => fp.Name == "Count");
            if (!countParameter.SuccessfullySet)
            {
                int[] countsV = this.countPoints(filename);
                countParameter.Value = countsV;
                countParameter.SuccessfullySet = true;
            }

            int[] counts = countParameter.Value as int[];
            
            //Dictionary<string, object> settings = this.Database.GetSettings(filename);

            //string countStr = (string)settings["Count"];
            //string[] splitCounts = countStr.Split(',');
            //int[] counts = new int[splitCounts.Length];
            //for (int i = 0; i < counts.Length; i++)
            //{
            //    counts[i] = int.Parse(splitCounts[i]);
            //}


            IDataPool[] pools = new IDataPool[textFile.VariableNames.Length];
            float[] running = new float[textFile.VariableNames.Length];
            for (int i = 0; i < pools.Length; i++)
            {
                if (i == 1 || i == 2) pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { counts[i] + 1 });
                else pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { counts[i] });
                //if(i != 1 && i != 2) pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] {count});
                //else pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { count + 1 });
            }

            bool first = true;
            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string line = reader.ReadLine();
                    line = reader.ReadLine();
                    while (line != null)
                    {
                        string[] splits = line.Split('\t');

                        if (splits.Length != pools.Length) break;

                        for (int i = 0; i < splits.Length; i++)
                        {
                            float val = 0;
                            if (float.TryParse(splits[i], out val))
                            {
                                if (val == 0) continue;

                                //if (val != 0)
                                //{
                                if (i == 1 || i == 2)
                                {
                                    if (first)
                                    {
                                        pools[i].SetDataValue(0);
                                        first = false;
                                    }
                                    if (i == 1) pools[2].SetDataValue(val + running[1]);
                                    else pools[1].SetDataValue(val + running[1]);
                                    running[1] += val;
                                    running[2] += val;
                                }
                                else
                                {
                                    pools[i].SetDataValue(val + running[i]);
                                    running[i] += val;
                                }
                                //}
                            }
                            else pools[i].SetDataValue(CeresBase.Data.DataConstants.GetMissingValue<float>());
                        }

                        line = reader.ReadLine();
                    }
                }
            }

            SpikeVariable[] vars = new SpikeVariable[headers.Length];
            for (int i = 0; i < headers.Length; i++)
            {
                int which = -1;
                for (int j = 0; j < textFile.VariableNames.Length; j++)
                {
                    if (textFile.VariableNames[j] == headers[i].VarName)
                    {
                        which = j;
                        break;
                    }
                }

                if (which == 2)
                {
                    pools[which].SetDataValue(running[2]);
                }

                vars[i] = new SpikeVariable(headers[i] as SpikeVariableHeader);
                DataFrame frame = new DataFrame(vars[i], pools[which], CeresBase.General.Constants.Timeless);
                vars[i].AddDataFrame(frame);
            }


            return vars;
        }

        #endregion
    }
}
