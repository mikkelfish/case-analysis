using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using System.IO;

namespace ApolloFinal.IO.SpikeText
{
    class SpikeTextFile : ICeresFile
    {
        #region ICeresFile Members

        public Type VariableType
        {
            get { return typeof(SpikeVariable); }
        }

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape); }
        }

        private string file;
        public string FilePath
        {
            get { return this.file; }
        }

        private string[] varNames;
        public string[] VariableNames
        {
            get { return varNames; }
        }

        public void LoadVarNames()
        {
            using (FileStream stream = new FileStream(this.file, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string firstLine = reader.ReadLine();
                    string[] splits = firstLine.Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < splits.Length; i++)
                    {
                        splits[i] = splits[i].Trim();
                    }
                    this.varNames = splits;
                }
            }
        }

        public SpikeTextFile(string filename)
        {
            this.file = filename;
           

        }
	

        #endregion
    }
}
