using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using System.IO;
using CeresBase.Data;
using CeresBase.IO.DataBase;
using CeresBase.Projects;
using System.Linq;

namespace ApolloFinal.IO.ClampfitText
{
    class ClampFitTextReader : ICeresReader
    {
        #region ICeresReader Members

        public object ReadAttribute(ICeresFile file, string name, string attrName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresBase.Data.Variable ReadVariable(string filename, CeresBase.Data.VariableHeader header, DateTime[] times)
        //{
        //    return this.ReadVariables(filename, new CeresBase.Data.VariableHeader[] { header },
        //        null)[0];
        //}

        private void readVar(char[] chars, int count, object tag)
        {
            object[] objs = tag as object[];
            char[] buffer = objs[0] as char[];
            IDataPool[] pools = (IDataPool[])objs[1];
            float[][] dataBuff = objs[2] as float[][];           
            int buffSpot = (int)objs[3];
            int[] dataSpots = (int[])objs[4];
            int whichVar = (int)objs[5];
            int[] counts = (int[])objs[6];
            bool firstCol = (bool)objs[7];
            List<float> commentTimes = (List<float>)objs[8];
            List<string> comments = (List<string>)objs[9];
            double freq = (double)objs[10];


            int numVars = pools.Length;
            int which = buffer.Length - buffSpot;
            if (count - buffSpot < which)
            {
                which = count - buffSpot;
            }

            Array.Copy(chars, 0, buffer, buffSpot, which);
            int leftOut = count - which;

            char[] collected = new char[512];
            int colSpot = 0;
            int last = 0;
            float lastTime = 0;
           // bool firstCol = true;
            for (int i = 0; i < count; i++)
            {
                if (buffer[i] != '\t' && buffer[i] != '\n' && buffer[i] != '\r')
                {
                    collected[colSpot] = buffer[i];
                    colSpot++;
                }
                else if(colSpot > 0)
                {
                    string str = new string(collected, 0, colSpot).Trim();

                    if (str.Contains("#"))
                    {
                        double time = (counts[0] + dataSpots[0] - 1) / freq;
                        if (time < 0) time = 0;
                        commentTimes.Add((float)time);
                        comments.Add(str.Substring(2));
                        colSpot = 0;
                        firstCol = true;
                    }
                    else
                    {
                        if (!firstCol)
                        {
                            float val = float.Parse(str);

                            dataBuff[whichVar][dataSpots[whichVar]] = val;
                            dataSpots[whichVar]++;
                            colSpot = 0;
                            if (dataSpots[whichVar] == dataBuff[whichVar].Length)
                            {
                                pools[whichVar].SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(dataBuff[whichVar]));
                                dataSpots[whichVar] = 0;
                                counts[whichVar] += dataBuff[whichVar].Length;
                            }
                            whichVar++;
                            if (whichVar == numVars)
                            {
                                whichVar = 0;
                                firstCol = true;
                            }
                        }
                        else
                        {
                            colSpot = 0;
                            firstCol = false;
                            float.TryParse(str, out lastTime);
                        }
                        for (int j = 0; j < collected.Length; j++)
                        {
                            collected[j] = '\0';
                        }
                    }
                    last = i;
                }
            }

            if (last != count)
            {
                Array.Copy(buffer, last + 1, buffer, 0, count - (last + 1));
                buffSpot = count - (last + 1);
            }
            else buffSpot = 0;

            if (which != count)
            {
                Array.Copy(chars, which, buffer, buffSpot, count - which);
                buffSpot += (count - which);
            }

            //object[] objs = tag as object[];
            //char[] buffer = objs[0] as char[];
            //int buffCount = (int)objs[1];
            //char delimiter = (char)objs[2];
            //IDataPool pool = (IDataPool)objs[3];
            //float[] dataBuff = objs[4] as float[];
            //int dataSpot = (int)objs[5];

//            char[] buffer = objs[0] as char[];
 //           IDataPool[] pools = (IDataPool[])objs[1];
 //           float[,] dataBuff = objs[2] as float[,];
            //int buffSpot = (int[])objs[3];
 //           int[] dataSpots = (int[])objs[4];
           // int whichVar = (int)objs[5];
            objs[3] = buffSpot;
            objs[5] = whichVar;
            objs[7] = firstCol;

        }


        //public CeresBase.Data.Variable[] ReadVariables(string filename, CeresBase.Data.VariableHeader[] headers, DateTime[][] times)
        //{
        //    Dictionary<string, object> settings = this.Database.GetSettings(filename);
        //    int count = (int)settings["NumPoints"];

        //    ClampfitTextFile cfile = new ClampfitTextFile(filename);

        //    IDataPool[] pools = new IDataPool[cfile.ChannelNames.Length];
        //    for (int i = 0; i < pools.Length; i++)
        //    {
        //        pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { count });
        //    }
        //        //= new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { count });
        //    char[] buffer = new char[512000*2];
        //    float[][] dataBuffer = new float[cfile.ChannelNames.Length][];
        //    for (int i = 0; i < dataBuffer.Length; i++)
        //    {
        //        dataBuffer[i] = new float[512000];
        //    }

        //    int buffSpot = 0;
        //    int[] dataSpots = new int[cfile.ChannelNames.Length];

        //    int[] counts = new int[cfile.ChannelNames.Length];

        //    List<float> commentTimes = new List<float>();
        //    List<string> comments = new List<string>();

        //    object[] args = new object[]{buffer, pools,
        //        dataBuffer, buffSpot, dataSpots, 0, counts, true, commentTimes, comments, (headers[0] as SpikeVariableHeader).Frequency};

        //    using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
        //    {
        //        using (StreamReader reader = new StreamReader(stream))
        //        {
        //            string line = reader.ReadLine();
        //            while (line != null && line.Contains("="))
        //            {
        //                line = reader.ReadLine();
        //            }

        //            CeresBase.General.Utilities.ReadTextFromFileStream(reader,
        //                -1, 512000 * 2, new CeresBase.General.Utilities.TextStreamAction(this.readVar),
        //               args);
        //        }
        //    }

        //    buffSpot = (int)args[3];
        //    int whichvar = (int)args[5];
        //    bool firstCol = (bool)args[7];
        //    string lastLine = new string(buffer, 0, buffSpot);
        //    lastLine = lastLine.Replace('\n', ' ');
        //    string[] lineSplits = lastLine.Split(new char[] { '\r' }, StringSplitOptions.RemoveEmptyEntries);            
            
        //    for (int lin = 0; lin < lineSplits.Length; lin++)
        //    {
        //        string[] splits = lineSplits[lin].Split('\t');
        //        for (int i = 0; i < splits.Length; i++)
        //        {
        //            if (firstCol)
        //            {
        //                firstCol = false;
        //                continue;
        //            }
        //            dataBuffer[whichvar][dataSpots[whichvar]] = float.Parse(splits[i].Trim());

        //            if (dataBuffer[whichvar][dataSpots[whichvar]] > 100)
        //            {
        //                int s = 0;
        //            }

        //            dataSpots[whichvar]++;
        //            //counts[whichvar]++;

        //            whichvar++;
        //        }
        //        firstCol = true;
        //        whichvar = 0;
                
        //    }

        //    SpikeVariable[] vars = new SpikeVariable[headers.Length];
        //    for (int i = 0; i < cfile.ChannelNames.Length; i++)
        //    {
        //        int linkTo = -1;
        //        for (int j = 0; j < headers.Length; j++)
        //        {
        //            if (headers[j].VarName == cfile.ChannelNames[i])
        //            {
        //                linkTo = j;
        //                break;
        //            }
        //        }
        //        if (linkTo == -1) continue;

        //        if (dataSpots[i] != 0)
        //        {
        //            float[] temp = new float[dataSpots[i]];
        //            Array.Copy(dataBuffer[i], 0, temp, 0, dataSpots[i]);
        //            counts[i] += dataSpots[i];
        //            pools[i].SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(temp));
        //        }

        //        SpikeVariable var = new SpikeVariable(headers[linkTo] as SpikeVariableHeader);

        //        for (int h = 0; h < comments.Count; h++)
        //        {
        //            //var.HotSpots.Add(new HotSpot(headers[linkTo].ExperimentName, comments[h], "", commentTimes[h], commentTimes[h]));

        //            EpochCentral.AddEpoch(new Epoch(var, comments[h], commentTimes[h], commentTimes[h]) { AppliesToExperiment = true });

        //            //EpochCentral.Epochs.Add(new Epoch(var, comments[h], commentTimes[h], commentTimes[h]));
        //            //var.EpochsFromFile.Add(new Epoch(var, comments[h], commentTimes[h], commentTimes[h]));
        //        }

        //        DataFrame frame = new DataFrame(var, pools[i], new CeresBase.Data.DataShapes.NoShape(),
        //            CeresBase.General.Constants.Timeless);
        //        var.AddDataFrame(frame);
        //        vars[linkTo] = var;
        //    }
            
        //    return vars;
        //}

        public byte[] ReadBinary(ICeresFile file, string name)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //private CeresFileDataBase db;
        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return db;
        //    }
        //    set
        //    {
        //        this.db = value;
        //    }
        //}

        public double[][] GetShapeValues(ICeresFile file)
        {
            return null;
        }

        public CeresBase.Data.VariableHeader[] GetAllVariableHeaders(string file, string experimentName)
        {
            ClampfitTextFile cfile = new ClampfitTextFile(file);
            SpikeVariableHeader[] headers = new SpikeVariableHeader[cfile.ChannelNames.Length];
            for (int i = 0; i < cfile.ChannelNames.Length; i++)
            {
                headers[i] = new SpikeVariableHeader(cfile.ChannelNames[i], "mV", experimentName,
                    1.0 / cfile.Resolution, SpikeVariableHeader.SpikeType.Raw);
            }
            return headers;
        }

        public bool Supported(string file)
        {
            return ClampfitTextFile.IsClampFitText(file);
        }

        private void countCallback(char[] characters, int count, object tag)
        {
            int dcount = (int)(tag as object[])[0];
            bool something = false;
            for (int i = 0; i < count; i++)
            {
                if (characters[i] == '\n' ||
                    characters[i] == '\r')
                {
                    if (something)
                    {
                        dcount++;
                        something = false;
                    }
                }
                else something = true;
            }
            (tag as object[])[0] = dcount;
        }

        private int countPoints(string file)
        {
            int count = 0;

            object[] opts = new object[] { count };
            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string line = reader.ReadLine();
                    while (line != null && line.Contains("="))
                    {
                        line = reader.ReadLine();
                    }

                    CeresBase.General.Utilities.ReadTextFromFileStream(reader,
                        -1, 256000, new CeresBase.General.Utilities.TextStreamAction(this.countCallback),
                       opts);
                }
            }
            return (int)opts[0];
        }


        //public void AddFilesToDB(string[] files)
        //{
        //    foreach (string file in files)
        //    {
        //        CeresBase.UI.RequestInformation infoReq = new CeresBase.UI.RequestInformation();
        //        List<Type> types = new List<Type>();
        //        List<string> names = new List<string>();
        //        List<string> descriptions = new List<string>();

        //        types.Add(typeof(string));
        //        names.Add("Experiment Name");
        //        descriptions.Add("Please enter the name of the experiment the file " + file + " is from.");

        //        infoReq.Grid.SetInformationToCollect(types.ToArray(), null, names.ToArray(), descriptions.ToArray(),
        //            null, null);

        //        if (infoReq.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) continue;


        //        int count = this.countPoints(file);

        //        ClampfitTextFile cfile = new ClampfitTextFile(file);
        //        this.Database.CreateFile(infoReq.Grid.Results["Experiment Name"] as string, cfile);

        //        this.db.SetSettings(file, new string[] { "NumPoints"},
        //           new object[] { count});


        //    }
        //}

        #endregion

        #region ICeresReader Members


        public bool StayNative
        {
            get { return false; }
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            FileDataBaseEntryParameter count = new FileDataBaseEntryParameter() { Name = "Count", UserEditable = false };
            return new FileDataBaseEntryParameter[] { count };
        }

        #endregion

        #region ICeresReader Members


        public FileDataBaseVariableEntryParameter[] GetVariableParameters(FileDataBaseEntry entry)
        {
            ClampfitTextFile cfile = new ClampfitTextFile(entry.Filepath);
            FileDataBaseVariableEntryParameter[] paras = new FileDataBaseVariableEntryParameter[cfile.ChannelNames.Length];
            for (int i = 0; i < cfile.ChannelNames.Length; i++)
            {
                SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
                para.VariableName.Value = cfile.ChannelNames[i];
                para.VariableName.IsFixed = true;

                para.Units.Value = "mV";

                para.Frequency.Value = 1.0 / cfile.Resolution;
                para.Frequency.IsFixed = true;

                para.SpikeType.Value = SpikeVariableHeader.SpikeType.Raw;
                para.SpikeType.IsFixed = true;

                paras[i] = para;
       
            }

            return paras;

        }

        #endregion

   

        #region ICeresReader Members


        public Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers)
        {
            //Dictionary<string, object> settings = this.Database.GetSettings(filename);
            int count = 0;

            if (fileparams.Single(prop => prop.Name == "Count").SuccessfullySet)
            {
                count = (int)fileparams.Single(p => p.Name == "Count").Value;
            }
            else
            {
                count = this.countPoints(filename);
                fileparams.Single(p => p.Name == "Count").Value = count;
                fileparams.Single(p => p.Name == "Count").SuccessfullySet = true;
            }

            ClampfitTextFile cfile = new ClampfitTextFile(filename);

            IDataPool[] pools = new IDataPool[cfile.ChannelNames.Length];
            for (int i = 0; i < pools.Length; i++)
            {
                pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { count });
            }
            //= new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { count });
            char[] buffer = new char[512000 * 2];
            float[][] dataBuffer = new float[cfile.ChannelNames.Length][];
            for (int i = 0; i < dataBuffer.Length; i++)
            {
                dataBuffer[i] = new float[512000];
            }

            int buffSpot = 0;
            int[] dataSpots = new int[cfile.ChannelNames.Length];

            int[] counts = new int[cfile.ChannelNames.Length];

            List<float> commentTimes = new List<float>();
            List<string> comments = new List<string>();

            object[] args = new object[]{buffer, pools,
                dataBuffer, buffSpot, dataSpots, 0, counts, true, commentTimes, comments, (headers[0] as SpikeVariableHeader).Frequency};

            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string line = reader.ReadLine();
                    while (line != null && line.Contains("="))
                    {
                        line = reader.ReadLine();
                    }

                    CeresBase.General.Utilities.ReadTextFromFileStream(reader,
                        -1, 512000 * 2, new CeresBase.General.Utilities.TextStreamAction(this.readVar),
                       args);
                }
            }

            buffSpot = (int)args[3];
            int whichvar = (int)args[5];
            bool firstCol = (bool)args[7];
            string lastLine = new string(buffer, 0, buffSpot);
            lastLine = lastLine.Replace('\n', ' ');
            string[] lineSplits = lastLine.Split(new char[] { '\r' }, StringSplitOptions.RemoveEmptyEntries);

            for (int lin = 0; lin < lineSplits.Length; lin++)
            {
                string[] splits = lineSplits[lin].Split('\t');
                for (int i = 0; i < splits.Length; i++)
                {
                    if (firstCol)
                    {
                        firstCol = false;
                        continue;
                    }
                    dataBuffer[whichvar][dataSpots[whichvar]] = float.Parse(splits[i].Trim());


                    dataSpots[whichvar]++;
                    //counts[whichvar]++;

                    whichvar++;
                }
                firstCol = true;
                whichvar = 0;

            }

            SpikeVariable[] vars = new SpikeVariable[headers.Length];
            for (int i = 0; i < cfile.ChannelNames.Length; i++)
            {
                int linkTo = -1;
                for (int j = 0; j < headers.Length; j++)
                {
                    if (headers[j].VarName == cfile.ChannelNames[i])
                    {
                        linkTo = j;
                        break;
                    }
                }
                if (linkTo == -1) continue;

                if (dataSpots[i] != 0)
                {
                    float[] temp = new float[dataSpots[i]];
                    Array.Copy(dataBuffer[i], 0, temp, 0, dataSpots[i]);
                    counts[i] += dataSpots[i];
                    pools[i].SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(temp));
                }

                SpikeVariable var = new SpikeVariable(headers[linkTo] as SpikeVariableHeader);

                for (int h = 0; h < comments.Count; h++)
                {
                    //var.HotSpots.Add(new HotSpot(headers[linkTo].ExperimentName, comments[h], "", commentTimes[h], commentTimes[h]));

                    EpochCentral.AddEpoch(new Epoch(var, comments[h], commentTimes[h], commentTimes[h]) { AppliesToExperiment = true });

                    //EpochCentral.Epochs.Add(new Epoch(var, comments[h], commentTimes[h], commentTimes[h]));
                    //var.EpochsFromFile.Add(new Epoch(var, comments[h], commentTimes[h], commentTimes[h]));
                }

                DataFrame frame = new DataFrame(var, pools[i], CeresBase.General.Constants.Timeless);
                var.AddDataFrame(frame);
                vars[linkTo] = var;
            }

            return vars;
        }

        #endregion
    }
}
