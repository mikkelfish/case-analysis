using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using System.IO;

namespace ApolloFinal.IO.ClampfitText
{
    class ClampfitTextFile : ICeresFile
    {
        #region ICeresFile Members

        public Type VariableType
        {
            get { return typeof(SpikeVariable); }
        }

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape); }
        }

        private string file;

        public string FilePath
        {
            get { return file; }
        }

        private double res;
        public double Resolution
        {
            get
            {
                return res;
            }
        }

        public static bool IsClampFitText(string name)
        {
            try
            {
                using (FileStream stream = new FileStream(name, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string first = reader.ReadLine();
                        if (first == null) return false;
                        return first.Contains("Interval=");
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        private string[] channelNames;
        public string[] ChannelNames
        {
            get
            {
                return this.channelNames;
            }
        }

        public ClampfitTextFile(string name)
        {
            this.file = name;
            using (FileStream stream = new FileStream(name, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string first = reader.ReadLine();
                    string[] resSplit = first.Split('=');
                    string number = "";
                    string unit = "";
                    for (int i = 0; i < resSplit[1].Length; i++)
                    {
                        if(char.IsDigit(resSplit[1][i]) || resSplit[1][i] == '.')
                        {
                            number += resSplit[1][i];
                        }
                        else
                        {
                            unit += resSplit[1][i];
                        }

                    }
                    this.res = double.Parse(number);
                    if (unit == "ms") this.res /= 1000.0;

//DateTime=-1073160196
//TimeFormat=StartOfBlock
//ChannelTitle=	"PNA"	"N. X"  "T"
                    first = reader.ReadLine();
                    first = reader.ReadLine();
                    first = reader.ReadLine();
                    string[] varSplits = first.Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries);

                    this.channelNames = new string[varSplits.Length / 2];
                    if (this.channelNames.Length != 0)
                    {
                        for (int i = 1; i < varSplits.Length; i += 2)
                        {
                            this.channelNames[i / 2] = varSplits[i];
                        }
                    }
                    else
                    {
                        varSplits = first.Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                        this.channelNames = new string[varSplits.Length - 1];
                        for (int i = 0; i < channelNames.Length; i++)
                        {
                            this.channelNames[i] = varSplits[i + 1];
                        }
                    }
                }
            }

        }

        #endregion
    }
}
