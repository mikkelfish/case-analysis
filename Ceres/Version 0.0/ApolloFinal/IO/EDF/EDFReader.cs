﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.DataBase;
using CeresBase.Data;
using CeresBase.Projects;

namespace ApolloFinal.IO.EDF
{
    public class EDFReader : CeresBase.IO.ICeresReader
    {
        #region ICeresReader Members

        public object ReadAttribute(CeresBase.IO.ICeresFile file, string name, string attrName)
        {
            throw new NotImplementedException();
        }

        public double[][] GetShapeValues(CeresBase.IO.ICeresFile file)
        {
            throw null;
        }

        public bool Supported(string file)
        {
            return EDFFile.CanRead(file);
        }

        public bool StayNative
        {
            get { return true; }
        }

        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            return null;
        }

        public static List<EDFFile> files = new List<EDFFile>();

        public CeresBase.IO.DataBase.FileDataBaseVariableEntryParameter[] GetVariableParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            EDFFile file = files.SingleOrDefault(f => f.FilePath == entry.Filepath);
            if (file == null)
            {
                file = new EDFFile(entry.Filepath);
                files.Add(file);
            }

            FileDataBaseVariableEntryParameter[] toRet = new FileDataBaseVariableEntryParameter[file.NumChannels];
            for (int i = 0; i < toRet.Length; i++)
            {
                double freq =  0;
                double min = 0;
                double max = 0;
                string label = "";
                string units = "";
                EDFWrapper.GetChannelInformation(file.FileID, i, ref freq, ref min, ref max, ref label, ref units);

                SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
                para.VariableName.Value = "Channel " + (i + 1).ToString();
                para.VariableName.IsFixed = true;

                para.Units.Value = units;

                para.Frequency.Value = freq;
                para.Frequency.IsFixed = true;

                para.SpikeType.Value = SpikeVariableHeader.SpikeType.Raw;
                para.SpikeType.IsFixed = true;

                para.DisplayName.Value = label;

                toRet[i] = para;
            }


            return toRet;
        }

        public CeresBase.Data.Variable[] ReadVariables(string filename, CeresBase.IO.DataBase.FileDataBaseEntryParameter[] fileparams, CeresBase.Data.VariableHeader[] headers)
        {
            List<Variable> vars = new List<Variable>();
            EDFFile file = files.SingleOrDefault(f => f.FilePath == filename);
            if (file == null)
            {
                file = new EDFFile(filename);
                files.Add(file);
            }
            for (int i = 0; i < headers.Length; i++)
            {
                string[] splits = headers[i].VarName.Trim().Split(' ');
                ushort channel = (ushort)(ushort.Parse(splits[splits.Length - 1]) - 1);
                IDataPool pool = new EDFDataPool(file, channel);
                SpikeVariable variable = new SpikeVariable((SpikeVariableHeader)headers[i]);
                DataFrame frame = new DataFrame(variable, pool, CeresBase.General.Constants.Timeless);

                for (int j = 0; j < file.NumAnnotations; j++)
                {
                    double time;
                    string desc;
                    EDFWrapper.GetAnnotation(file.FileID, j, out time, out desc);
                    EpochCentral.AddEpoch(new Epoch(variable, desc, (float)time, (float)time));
                }

                variable.AddDataFrame(frame);

                vars.Add(variable);
            }
            //    son.Dispose();
            return vars.ToArray();
        }

        #endregion
    }
}
