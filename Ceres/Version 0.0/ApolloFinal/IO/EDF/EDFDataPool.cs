﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Data;

namespace ApolloFinal.IO.EDF
{
    class EDFDataPool : IDataPool
    {
        private bool isSet;

        private int fileNumber;
        private int channelNumber;
        private double frequency;
        private EDFFile file;

        public EDFDataPool(EDFFile file, int channelNumber)
        {
            this.file = file;
            this.fileNumber = file.FileID;
            this.channelNumber = channelNumber;
            this.CalcMinMax();
        }

        #region Not Implemented
        public CompressionChain Chain
        {
            get { throw new NotImplementedException(); }
        }

        public void PopCompression()
        {
            throw new NotImplementedException();
        }

        public void SetDataValue(float val)
        {
            throw new NotImplementedException();
        }

        public void SetDataValue(float val, int[] indices)
        {
            throw new NotImplementedException();
        }

        public void SetData(System.IO.Stream data)
        {
            throw new NotImplementedException();
        }

        public void SetData(System.IO.Stream data, int[] indices, int[] amounts)
        {
            throw new NotImplementedException();
        }

        public void SetData(System.IO.Stream data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            throw new NotImplementedException();
        }
        
        #endregion

        #region IDataPool Members

        public void FrameAdded()
        {
        }

        public void FrameRemoved()
        {
        }

        public void SetMinMaxManually(float min, float max)
        {
            this.Min = min;
            this.Max = max;
        }

        public Type CurrentStorageType
        {
            get { return typeof(float); }
        }

        public float GetDataValue()
        {
            throw new NotImplementedException();
        }

        public float GetDataValue(int[] indices)
        {
            double startTime = indices[0] / this.frequency;
            return EDFWrapper.GetData(this.file.FileID, this.channelNumber, startTime, 1)[0];
        }

        public void GetData(int[] indices, int[] amount, DataUtilities.PoolReadDelegate readFunction)
        {
            double startTime = indices[0] / this.frequency;
            double firstTime = startTime;

            int totalAmount = 0;
            if (amount == null)
            {
                totalAmount = (int)(this.file.Length * this.frequency) - indices[0];
            }
            else totalAmount = amount[0];

            int bufferSize = 240000;
            float[] buffer = new float[bufferSize];
            int buffSpot = 0;
            int current = 0;
            int length = totalAmount < bufferSize ? totalAmount : bufferSize;

            uint[] ufirstIndices = new uint[1];
            uint[] ucount = new uint[1];

            float[] data = null;
            while ((data = EDFWrapper.GetData(this.file.FileID, this.channelNumber, startTime, length)) != null)
            {
                for (int i = 0; i < data.Length; i++, current++)
                {
                    if (current >= totalAmount) break;
                    buffer[buffSpot] = data[i];
                    buffSpot++;
                    if (buffSpot == bufferSize)
                    {
                        ucount[0] = (uint)(bufferSize);
                        readFunction(buffer, ufirstIndices, ucount);
                        buffSpot = 0;
                        ufirstIndices[0] += ucount[0];
                    }
                }

                length = totalAmount - current;
                if (length > bufferSize)
                    length = bufferSize;

                startTime = firstTime + current / this.frequency;

                if (length <= 0)
                    break;
            }


            if (buffSpot != 0)
            {
                float[] toPass = new float[buffSpot];
                Array.Copy(buffer, 0, toPass, 0, buffSpot);

                ucount[0] = (uint)buffSpot;
                readFunction(toPass, ufirstIndices, ucount);
            }
        }

        public void GetData(int[] firstIndices, int[] lastIndices, int[] strides, DataUtilities.PoolReadStridedDelegate readFunction)
        {
            throw new NotImplementedException();
        }


        public int GetDimensionLength(int index)
        {
           return (int)(this.file.Length * this.frequency);
        }

        public int[] DimensionLengths
        {
            get { return new int[]{this.GetDimensionLength(0)}; }
        }

        public float Min
        {
            get;
            private set;
        }

        public float Max
        {
            get;
            private set;
        }

        public int NumDimensions
        {
            get { return 1; }
        }

        public void DoneSettingData()
        {
            this.isSet = true;
        }

        public void CalcMinMax()
        {

            double freq =0 ;
            double min = 0;
            double max = 0;
            string label = "";
            string units = "";
            EDFWrapper.GetChannelInformation(this.fileNumber, this.channelNumber, ref freq, ref min, ref max, ref label, ref units);
            this.frequency = freq;
            this.Min = Convert.ToSingle(min);
            this.Max = Convert.ToSingle(max);
        }

        public bool BeenSet
        {
            get { return this.isSet; }
        }

        public bool MinMaxFound
        {
            get { return true; }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            
        }

        #endregion
    }
}
