﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApolloFinal.IO.EDF
{
    public class EDFFile : CeresBase.IO.ICeresFile, IDisposable
    {
        private bool disposed = false;
        private string filename;

        private int fID;
        internal int FileID
        {
            get
            {
                return this.fID;
            }
        }

        public double Length { get; private set; }

        public long NumAnnotations { get; set; }

        public int NumChannels
        {
            get;
            private set;
        }

        public static bool CanRead(string filename)
        {
            return filename.Contains(".edf") || filename.Contains(".bdf");
        }

        public EDFFile(string filename)
        {
            this.filename = filename;

            int numchannels = 0;
            long numannot = 0;
            double filelength = 0;

            this.fID = EDFWrapper.OpenFile(filename, ref numchannels, ref numannot, ref filelength);
            this.NumChannels = numchannels;
            this.NumAnnotations = numannot;
            this.Length = filelength;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (!this.disposed && this.FileID >= 0)
            {
                EDFWrapper.CloseFile(this.FileID);
                this.disposed = true;
            }
        }

        #endregion

        ~EDFFile()
        {
            this.Dispose();
        }


        #region ICeresFile Members

        public Type VariableType
        {
            get { return typeof(SpikeVariable); }
        }

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape); }
        }

        public string FilePath
        {
            get { return this.filename; }
        }

        #endregion
    }
}
