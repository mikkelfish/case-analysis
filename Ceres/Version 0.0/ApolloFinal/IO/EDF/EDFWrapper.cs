﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;

namespace ApolloFinal.IO.EDF
{
    public static class EDFWrapper
    {
        //#define EDFLIB_TIME_DIMENSION (10000000LL)
        public const int EDFLIB_MAXSIGNALS = 256;
        public const int EDFLIB_MAX_ANNOTATION_LEN = 512;

        public const int EDFSEEK_SET = 0;
        public const int EDFSEEK_CUR = 1;
        public const int EDFSEEK_END = 2;



        ///* the following defines are used in the member "filetype" of the edf_hdr_struct */
        ///* and as return value for the function edfopen_file_readonly() */
        public const int EDFLIB_FILETYPE_EDF               =   0;
        public const int EDFLIB_FILETYPE_EDFPLUS           =   1;
        public const int EDFLIB_FILETYPE_BDF               =  2;
        public const int EDFLIB_FILETYPE_BDFPLUS            =  3;
        public const int EDFLIB_MALLOC_ERROR                = -1;
        public const int EDFLIB_NO_SUCH_FILE_OR_DIRECTORY   = -2;
        public const int EDFLIB_FILE_CONTAINS_FORMAT_ERRORS = -3;
        public const int EDFLIB_MAXFILES_REACHED           =  -4;
        public const int EDFLIB_FILE_READ_ERROR            =  -5;
        public const int EDFLIB_FILE_ALREADY_OPENED        =  -6;
        public const int EDFLIB_FILETYPE_ERROR             =  -7;
        public const int EDFLIB_FILE_WRITE_ERROR           =  -8;
        public const int EDFLIB_NUMBER_OF_SIGNALS_INVALID  =  -9;
        public const int EDFLIB_FILE_IS_DISCONTINUOUS      =  -10;
        public const int EDFLIB_INVALID_READ_ANNOTS_VALUE = -11;

        ///* values for annotations */
        public const int EDFLIB_DO_NOT_READ_ANNOTATIONS = 0;
        public const int EDFLIB_READ_ANNOTATIONS       = 1;
        public const int EDFLIB_READ_ALL_ANNOTATIONS   =  2;

        ///* the following defines are possible errors returned by edfopen_file_writeonly() */
        public const int EDFLIB_NO_SIGNALS = -20;
        public const int EDFLIB_TOO_MANY_SIGNALS         =   -21;
        public const int EDFLIB_NO_SAMPLES_IN_RECORD     =   -22;
        public const int EDFLIB_DIGMIN_IS_DIGMAX         =   -23;
        public const int EDFLIB_DIGMAX_LOWER_THAN_DIGMIN  =  -24;
        public const int EDFLIB_PHYSMIN_IS_PHYSMAX        =  -25;

        

        #region Internal calls
        [DllImport("EDFDll.dll", EntryPoint = "edflib_version"), SuppressUnmanagedCodeSecurity]
        private static extern int edflib_version();

        //int OpenFile(const char* filename, int* numSignals, long long* numAnnotations, double* fileLength);
        [DllImport("EDFDll.dll", EntryPoint = "OpenFile"), SuppressUnmanagedCodeSecurity]
        private static extern int openFile(string path, ref int numSignals, ref long numAnnoations, ref double fileLength);

        [DllImport("EDFDll.dll", EntryPoint = "edfclose_file"), SuppressUnmanagedCodeSecurity]
        private static extern int edfclose_file(int handle);

        //void GetChannelInformation(int fileNumber, int channelNumber, double* frequency, double* min, double* max, char* label, char* units);
        [DllImport("EDFDll.dll", EntryPoint = "GetChannelInformation"), SuppressUnmanagedCodeSecurity]
        private static extern int getChannelInformation(int fileNumber, int channelNumber, ref double frequency, ref double min, ref double max,
            StringBuilder label, StringBuilder units);

        //int GetData(int fileNumber, int channelNumber, double startTime, int numberToRead, double* buffer)
        [DllImport("EDFDll.dll", EntryPoint = "GetData"), SuppressUnmanagedCodeSecurity]
        private static extern int getData(int fileNumber, int channelNumber, double startTime, int numberToRead, double[] buffer);
       
        //int GetAnnotation(int fileNumber, int annotationNumber, double* time, char* annotation)
        [DllImport("EDFDll.dll", EntryPoint = "GetAnnotation"), SuppressUnmanagedCodeSecurity]
        private static extern int getAnnotation(int fileNumber, int annotationNumber, ref double time, StringBuilder annotation);


        #endregion

        public static double GetVersion()
        {
            double woo = edflib_version() / 100.0;
            return woo;
        }

        public static float[] GetData(int fileNumber, int channelNumber, double startTime, int numberToRead)
        {
            double[] toRet = new double[numberToRead];
            getData(fileNumber, channelNumber, startTime, numberToRead, toRet);

            float[] fToRet = new float[toRet.Length];
            for (int i = 0; i < toRet.Length; i++)
            {
                fToRet[i] = Convert.ToSingle(toRet[i]);
            }
            
            return fToRet;

        }

        public static void GetAnnotation(int fileNumber, int annotationNumber, out double time, out string annotation)
        {
            StringBuilder builder = new StringBuilder("", 1000);
            time = 0;
            getAnnotation(fileNumber, annotationNumber, ref time, builder);
            annotation = builder.ToString();
        }

        public static void GetChannelInformation(int fileNumber, int channelNumber, ref double frequency, ref double min, ref double max, ref string label, ref string units)
        {
            StringBuilder labelb = new StringBuilder("", 1024);
            StringBuilder unitsb = new StringBuilder("", 1024);

            int ok = getChannelInformation(fileNumber, channelNumber, ref frequency, ref min, ref max, labelb, unitsb);
            label = labelb.ToString().Trim();
            units = unitsb.ToString().Trim();

            if (ok < 0) throw new Exception("Error reading EDF Channel");

        }

        public static int OpenFile(string path, ref int numSignals, ref long numAnnotations, ref double fileLength)
        {
            return openFile(path, ref numSignals, ref numAnnotations, ref fileLength);
        }

        public static void CloseFile(int id)
        {
            edfclose_file(id);
        }
    }
}
