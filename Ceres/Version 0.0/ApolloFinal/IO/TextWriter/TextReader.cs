using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using CeresBase.Data;
using System.IO;
using CeresBase.IO.DataBase;
using System.Windows.Data;
using System.Linq;

namespace ApolloFinal.IO.TextWriter
{
    class TextReader : ICeresReader
    {
        #region ICeresReader Members

        public object ReadAttribute(ICeresFile file, string name, string attrName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private void readVar(char[] chars, int count, object tag)
        {
            object[] objs = tag as object[];
            char[] buffer = objs[0] as char[];
            int buffCount = (int)objs[1];
            char delimiter = (char)objs[2];
            IDataPool[] pools = (IDataPool[])objs[3];
            float[][] dataBuffs = objs[4] as float[][];
            int[] dataSpots = (int[])objs[5];
            bool isInterval = (bool)objs[6];
            float[] runningTotals = (float[])objs[7];

            int which = buffer.Length - buffCount;
            if (count - buffCount < which)
            {
                which = count - buffCount;
            }

            Array.Copy(chars, 0, buffer, buffCount, which);
            int leftOut = count - which;
            
            char[] collected = new char[2048];
            int colSpot = 0;
            int last = 0;
            for (int i = 0; i < count; i++)
            {
                if (buffer[i] != '\n' && buffer[i] != '\r')
                {
                    collected[colSpot] = buffer[i];
                    colSpot++;
                }
                else if (buffer[i] == '\n' || buffer[i] == '\r')
                {
                    string str = new string(collected, 0, colSpot).Trim();
                    string[] pieces = str.Split(new char[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);
                    float val = 0;

                    for (int j = 0; j < pieces.Length; j++)
                    {
                        try
                        {
                            val = float.Parse(pieces[j]);

                            if (isInterval)
                            {
                                val = val + runningTotals[j];
                                runningTotals[j] = val;
                            }
                        }
                        catch
                        {
                            val = 0;
                        }
                        dataBuffs[j][dataSpots[j]] = val;
                        dataSpots[j]++;
                        colSpot = 0;
                        if (dataSpots[j] == dataBuffs[j].Length)
                        {
                            pools[j].SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(dataBuffs[j]));
                            dataSpots[j] = 0;
                        }
                    }
                    
                  
                    last = i;
                }
            }

            if (last != count)
            {
                Array.Copy(buffer, last + 1, buffer, 0, count - (last + 1));
                buffCount = count - (last + 1);
            }
            else buffCount = 0;

            if (which != count)
            {
                Array.Copy(chars, which, buffer, buffCount, count - which);
                buffCount += (count - which);
            }

            //object[] objs = tag as object[];
            //char[] buffer = objs[0] as char[];
            //int buffCount = (int)objs[1];
            //char delimiter = (char)objs[2];
            //IDataPool pool = (IDataPool)objs[3];
            //float[] dataBuff = objs[4] as float[];
            //int dataSpot = (int)objs[5];
            objs[1] = buffCount;
            objs[5] = dataSpots;
            objs[7] = runningTotals;
        }

        //public CeresBase.Data.Variable ReadVariable(string filename, CeresBase.Data.VariableHeader header, DateTime[] times)
        //{
        //    Dictionary<string, object> settings = this.Database.GetSettings(filename);
        //    int count = (int)settings["Count"];

        //    int buffLength = 1024000;

        //    IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { count });
        //    char[] buffer = new char[buffLength];
        //    float[] dataBuffer = new float[buffLength];
        //    //object[] objs = tag as object[];
        //    //char[] buffer = objs[0] as char[];
        //    //int buffCount = (int)objs[1];
        //    //char delimiter = (char)objs[2];
        //    //IDataPool pool = (IDataPool)objs[3];
        //    //float[] dataBuff = objs[4] as float[];
        //    //int dataSpot = (int)objs[5];            
        //    string delimeter = (string)settings["Delim"];
        //    char delim = delimeter[0];
        //    if (delimeter.ToLower() == "ret") delim = '\n';
        //    if (delimeter.ToLower() == "tab") delim = '\t';
        //    if (delimeter.ToLower() == "space") delim = ' ';

        //    object[] args = new object[]{buffer, 0, delim, pool,
        //        dataBuffer, 0};
            
        //    using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
        //    {
        //        using (StreamReader reader = new StreamReader(stream))
        //        { 
        //            CeresBase.General.Utilities.ReadTextFromFileStream(reader,
        //                -1, buffLength, new CeresBase.General.Utilities.TextStreamAction(this.readVar),
        //               args);
        //        }
        //    }

        //    int dataSpot = (int)args[5];
        //    if (dataSpot != 0)
        //    {
        //        float[] temp = new float[dataSpot];
        //        Array.Copy(dataBuffer, 0, temp, 0, dataSpot);
        //        pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(temp));

        //        int buffCount = (int)args[1];
        //        string tempstr = "";

        //        for (int i = 0; i < buffCount; i++)
        //        {
        //            if (buffer[i] != delim)
        //            {
        //                tempstr += buffer[i];
        //            }
        //            else
        //            {
        //                float val = float.Parse(tempstr);
        //                pool.SetDataValue(val);
        //                tempstr = "";
        //            }
        //        }

        //    }

        //    SpikeVariable var = new SpikeVariable(header as SpikeVariableHeader);
        //    DataFrame frame = new DataFrame(var, pool, new CeresBase.Data.DataShapes.NoShape(),
        //        CeresBase.General.Constants.Timeless);
        //    var.AddDataFrame(frame);
        //    return var;
        //}

        //public CeresBase.Data.Variable[] ReadVariables(string filename, CeresBase.Data.VariableHeader[] headers, DateTime[][] times)
        //{
        //    return new Variable[] { this.ReadVariable(filename, headers[0], null) };
        //}

        public byte[] ReadBinary(ICeresFile file, string name)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //private CeresFileDataBase database;
        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}

        public double[][] GetShapeValues(ICeresFile file)
        {
            return null;
        }

        //public CeresBase.Data.VariableHeader[] GetAllVariableHeaders(string file, string experimentName)
        //{
        //    Dictionary<string, object> settings = this.Database.GetSettings(file);

        //    bool interval = (bool)settings["Interval"];
        //    SpikeVariableHeader.SpikeType type = SpikeVariableHeader.SpikeType.Raw;
        //    if (interval) type = SpikeVariableHeader.SpikeType.Pulse;

        //    return new SpikeVariableHeader[]{new SpikeVariableHeader((string)settings["VarName"],
        //        "None", experimentName, (double)settings["Freq"], type)};
        //}

        public bool Supported(string file)
        {
            int index = file.LastIndexOf('.');
            if (index < 0) return false;
            return file.Substring(index).ToLower() == ".text" || file.Substring(index).ToLower() == ".csv";
        }

        private void countCallback(char[] characters, int count, object tag)
        {
            int dcount = (int)(tag as object[])[0];
            char delim = (char)(tag as object[])[1];
            bool good = (bool)(tag as object[])[2];

            for (int i = 0; i < count; i++)
            {
                if (characters[i] == '\r' || characters[i] == '\n')
                {
                    if (good) dcount++;
                    good = false;
                }
                else if (char.IsDigit(characters[i]))
                    good = true;
            }
            (tag as object[])[0] = dcount;
            (tag as object[])[2] = good;
        }

        private int countPoints(string file, char delimeter)
        {
            int count = 0;

            object[] opts = new object[] { count, delimeter, false };
            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                using(StreamReader reader = new StreamReader(stream))
                {
                    CeresBase.General.Utilities.ReadTextFromFileStream(reader,
                        -1, 256000, new CeresBase.General.Utilities.TextStreamAction(this.countCallback),
                       opts);
                }
            }
            return (int)opts[0];
        }

        //public void AddFilesToDB(string[] files)
        //{
        //    foreach (string file in files)
        //    {
        //        CeresBase.UI.RequestInformation infoReq = new CeresBase.UI.RequestInformation();
        //        List<Type> types = new List<Type>();
        //        List<string> names = new List<string>();
        //        List<string> descriptions = new List<string>();

        //        types.Add(typeof(string));
        //        names.Add("Experiment Name");
        //        descriptions.Add("Please enter the name of the experiment the file " + file + " is from.");

        //        types.Add(typeof(string));
        //        names.Add("Variable Name");
        //        descriptions.Add("Enter the variable name.");

        //        types.Add(typeof(double));
        //        names.Add("Frequency");
        //        descriptions.Add("Freq");

        //        types.Add(typeof(string));
        //        names.Add("Delimiter");
        //        descriptions.Add("Ret (Return), Tab, Space or character");

        //        types.Add(typeof(bool));
        //        names.Add("IsInterval");
        //        descriptions.Add("Is the signal a interval signal.");

        //        infoReq.Grid.SetInformationToCollect(types.ToArray(), null, names.ToArray(), descriptions.ToArray(),
        //            null, null);

        //        if (infoReq.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) continue;

                

        //        int count = this.countPoints(file, (string)infoReq.Grid.Results["Delimiter"]);

        //        TextFile tfile = new TextFile(file);
        //        this.Database.CreateFile(infoReq.Grid.Results["Experiment Name"] as string, tfile);

        //        this.database.SetSettings(file, new string[] { "VarName", "Freq", "Delim", "Count", "Interval" },
        //           new object[] { infoReq.Grid.Results["Variable Name"], infoReq.Grid.Results["Frequency"],
        //           infoReq.Grid.Results["Delimiter"], count, infoReq.Grid.Results["IsInterval"]});

                
        //    }
        //}

        #endregion

        #region ICeresReader Members


        public bool StayNative
        {
            get { return false; }
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {            
            FileDataBaseEntryParameter frequency = new FileDataBaseEntryParameter() { Name = "Frequency", UserEditable = true, Value=0.0 };
            FileDataBaseEntryParameter delimiter = new FileDataBaseEntryParameter() { Name = "Delimiter", UserEditable = true, Value="Tab" };
            FileDataBaseEntryParameter isInterval = new FileDataBaseEntryParameter() { Name = "IsInterval", UserEditable = true, Value=false };
            FileDataBaseEntryParameter count = new FileDataBaseEntryParameter() { Name = "Count", UserEditable = false, SuccessfullySet = false };
            //REMEMBER TO DO COUNT AFTER THE DELIMITER IS SET
            FileDataBaseEntryParameter numVars = new FileDataBaseEntryParameter() { Name = "NumVars", UserEditable = false, SuccessfullySet = false };

            return new FileDataBaseEntryParameter[] { frequency, delimiter, isInterval, count, numVars };
        }

        #endregion

        #region ICeresReader Members


        public FileDataBaseVariableEntryParameter[] GetVariableParameters(FileDataBaseEntry entry)
        {           
            double freq = (double)entry.Parameters.Single(p => p.Name == "Frequency").Value;
            bool isInterval = (bool)entry.Parameters.Single(p => p.Name == "IsInterval").Value;
            string delimiter = entry.Parameters.SingleOrDefault(p => p.Name == "Delimiter").Value as string;
            char delim = delimiter[0];
            if (delimiter.ToLower() == "ret" || delimiter.ToLower() == "return") delim = '\n';
            if (delimiter.ToLower() == "tab") delim = '\t';
            if (delimiter.ToLower() == "space") delim = ' ';

            FileDataBaseEntryParameter numVarsP = entry.Parameters.Single(p => p.Name == "NumVars");
            int numVars = 1;
            if (numVarsP != null)
            {
                if (!numVarsP.SuccessfullySet)
                {
                    numVarsP.Value = getNumVars(entry.Filepath, delim);
                    numVarsP.SuccessfullySet = true;
                }

                numVars = (int)numVarsP.Value;
            }

            List<FileDataBaseVariableEntryParameter> paras = new List<FileDataBaseVariableEntryParameter>();

            for (int i = 0; i < numVars; i++)
            {
                SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
                para.Units.Value = "Raw";
                para.Frequency.Value = freq;
                para.Frequency.IsFixed = true;
                para.SpikeType.Value = isInterval ? SpikeVariableHeader.SpikeType.Pulse : SpikeVariableHeader.SpikeType.Raw;
                para.SpikeType.IsFixed = true;

                para.VariableName.Value = "Variable " + (i + 1);
                para.VariableName.IsFixed = true;

                paras.Add(para);
            }
            return paras.ToArray();
        }

        #endregion

  

        #region ICeresReader Members


        public Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers)
        {
            string delimiter = fileparams.SingleOrDefault(p => p.Name == "Delimiter").Value as string;
            char delim = delimiter[0];
            if (delimiter.ToLower() == "ret" || delimiter.ToLower() == "return") delim = '\n';
            if (delimiter.ToLower() == "tab") delim = '\t';
            if (delimiter.ToLower() == "space") delim = ' ';

            FileDataBaseEntryParameter countP = fileparams.SingleOrDefault(p => p.Name == "Count");
            if (!countP.SuccessfullySet)
            {
                //Set count
                countP.Value = this.countPoints(filename, delim);
                countP.SuccessfullySet = true;
            }

            int numVars = 1;
            FileDataBaseEntryParameter numVarsP = fileparams.SingleOrDefault(p => p.Name == "NumVars");
            if (numVarsP != null)
            {
                numVars = (int)numVarsP.Value;
            }

            bool isinterval = (bool)fileparams.SingleOrDefault(p => p.Name == "IsInterval").Value;

            int count = (int)countP.Value;

            int buffLength = 1024000;

            IDataPool[] pools = new IDataPool[numVars];
            char[] buffer = new char[buffLength];
            float[][] dataBuffer = new float[numVars][];
            for (int i = 0; i < pools.Length; i++)
            {
                pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { count }); ;
                dataBuffer[i] = new float[buffLength];
            }

            //object[] objs = tag as object[];
            //char[] buffer = objs[0] as char[];
            //int buffCount = (int)objs[1];
            //char delimiter = (char)objs[2];
            //IDataPool pool = (IDataPool)objs[3];
            //float[] dataBuff = objs[4] as float[];
            //int dataSpot = (int)objs[5];            


            float[] runningTotals = new float[numVars];
            int[] dataSpots = new int[numVars];
            object[] args = new object[]{buffer, 0, delim, pools,
                dataBuffer, dataSpots, isinterval, runningTotals};

            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    CeresBase.General.Utilities.ReadTextFromFileStream(reader,
                        -1, buffLength, new CeresBase.General.Utilities.TextStreamAction(this.readVar),
                       args);
                }
            }

            List<Variable> vars = new List<Variable>();
            for (int i = 0; i < numVars; i++)
            {
                //float[] runningTotal = (float)args[7];

                // int[] dataSpots = (int[])args[5];
                if (dataSpots[i] != 0)
                {
                    float[] temp = new float[dataSpots[i]];
                    Array.Copy(dataBuffer[i], 0, temp, 0, dataSpots[i]);
                    pools[i].SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(temp));
                }

            }
            
            int buffCount = (int)args[1];
            string tempstr = "";
            for (int b = 0; b < buffCount; b++)
            {
                if (buffer[b] != '\n' && buffer[b] != '\r')
                {
                    tempstr += buffer[b];
                }
                else
                {
                    string[] splits = tempstr.Split(new char[] { delim }, StringSplitOptions.RemoveEmptyEntries);
                    for (int j = 0; j < splits.Length; j++)
                    {
                        float val = float.Parse(splits[j]);

                        if (isinterval)
                        {
                            val = val + runningTotals[j];
                            runningTotals[j] = val;
                        }

                        pools[j].SetDataValue(val);
                        dataSpots[j]++;
                        tempstr = "";
                    }
                }
            }


            for (int i = 0; i < headers.Length; i++)
            {
                SpikeVariable var = new SpikeVariable(headers[i] as SpikeVariableHeader);
                string[] splits = headers[i].VarName.Split(' ');
                int which = int.Parse(splits[splits.Length-1]) - 1;

                DataFrame frame = new DataFrame(var, pools[which], CeresBase.General.Constants.Timeless);
                var.AddDataFrame(frame);
                vars.Add(var);
            }
            return vars.ToArray();
        }

        private object getNumVars(string filename, char delimiter)
        {
            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string firstline = reader.ReadLine().Trim();
                    string[] splits = firstline.Split(new char[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);
                    return splits.Length;
                }
            }
        }

        #endregion
    }
}
