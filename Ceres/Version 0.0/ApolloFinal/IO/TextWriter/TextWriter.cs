using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using System.IO;
using CeresBase.Data;
using CeresBase.UI;

namespace ApolloFinal.IO.TextData
{
    public class TextDataWriter : ICeresWriter
    {
        //private CeresFileDataBase database;

        public override string ToString()
        {
            return "Text";
        }

        public void AddAttribute(ICeresFile file, string name, string attrName, object value)
        {
            throw new Exception("The method or operation is not implemented.");
        }



        public ICeresFile[] WriteVariable(CeresBase.Data.Variable variable, string writeToDirectory)
        {
            if (writeToDirectory[writeToDirectory.Length - 1] != '\\') writeToDirectory += '\\';
            writeToDirectory += "Experiment_" + variable.Header.ExperimentName.Replace('\\', '_') + " Variable_" + variable.Header.Description + ".text";
            FileStream stream = new FileStream(writeToDirectory, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);

            int total = 0;

            string delim = this.del;
            if (delim.ToUpper() == "TAB") del = "\t";
            if (delim.ToUpper() == "RET") del = "\n";
            if (delim == "" || delim == null) del = "\t";

            float mult = 1;
            if ((variable as SpikeVariable).SpikeType == SpikeVariableHeader.SpikeType.Pulse)
                mult = (float)(variable as SpikeVariable).Resolution;

            char[] buffer = new char[480000];
            for (int i = 0; i < variable.NumFrames; i++)
            {
                int index = 0;
                //writer.Write("FrameNumber" + i.ToString() + " ");
                (variable[i] as DataFrame).Pool.GetData(null, null,
                    delegate(float[] data, uint[] indices, uint[] counts)
                    {
                        for (int j = 0; j < data.Length; j++)
                        {
                            string val = (data[j] * mult).ToString();

                            

                            val.CopyTo(0, buffer, index, val.Length);
                           
                            
                            index+=val.Length;
                           // writer.Write();
                            
                            total++;

                            buffer[index] = del[0];
                            index++;

                            if (index > buffer.Length-20)
                            {
                                writer.Write(buffer, 0, index);
                                for (int k = 0; k < buffer.Length; k++)
                                {
                                    buffer[k] = '0';
                                }
                                index = 0;
                            }                            
                        }
                    }
                );
                if (index != 0)
                {
                    writer.Write(buffer, 0, index/2);
                    writer.Write(buffer, index / 2, index / 2);
                    string st = new string(buffer);
                    if (st.Contains("0707397"))
                    {
                        int y = 0;
                    }
                }
            }


            writer.Close();
            return null;

        }

        public void WriteBinary(ICeresFile file, string name, byte[] data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}

        public void Dispose()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #region ICeresWriter Members

        string del = "Tab";

        public ICeresFile[] WriteVariables(Variable[] variables, string writeToDirectory)
        {
            RequestInformation req = new RequestInformation();
            req.Grid.SetInformationToCollect(new Type[] { typeof(string) },
                new string[] { "Delimiter" },
                new string[] { "Delimiter" },
                new string[] { "Enter the delimiter to use between values." },
                null,
                new object[] { "Tab" });
            if (req.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                del = (string)req.Grid.Results["Delimiter"];
            }

            foreach (Variable var in variables)
            {
                this.WriteVariable(var, writeToDirectory);
            }
            return null;
        }

        #endregion

        #region ICeresWriter Members


        public void AddAttribute(string fileName, string name, string attrName, object value)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
