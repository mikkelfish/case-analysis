using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;

namespace ApolloFinal.IO.TextWriter
{
    class TextFile : ICeresFile
    {
        #region ICeresFile Members

        public Type VariableType
        {
            get { return typeof(SpikeVariable); }
        }

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape);}
        }

        string filePath;

        public TextFile(string path)
        {
            this.filePath = path;
        }

        public string FilePath
        {
            get { return this.filePath; }
        }

        #endregion
    }
}
