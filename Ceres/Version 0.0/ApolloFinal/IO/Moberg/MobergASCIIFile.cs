﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO;
using System.IO;

namespace ApolloFinal.IO.Moberg
{
    public class MobergAsciiFile : ICeresFile
    {
        public class MobergMarker
        {
            public string MarkerName { get; set; }
            public double MarkerLocation { get; set; }
        }

        #region ICeresFile Members

        public Type VariableType
        {
            get { return typeof(SpikeVariable); }
        }

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape); }
        }

        public string FilePath
        {
            private set;
            get;
        }

        public MobergAsciiFile(string file)
        {
            this.FilePath = file;
            this.readFile();
        }

        public double SamplingFrequency { get; private set; }

        public string VariableName { get; set; }

        public string[] Files { get; set; }

        public int[] NumberOfPointsPerFile { get; set; }

        public int TotalPoints { get { return this.NumberOfPointsPerFile.Sum(); } }

        public MobergMarker[] Markers { get; set; }

        public double FirstTime { get; set; }

        public int LineLength { get; set; }

        public static bool CanRead(string file)
        {
            if (!file.Contains("part1of"))
                return false;

            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string line = reader.ReadLine();
                    if (line.Contains("ClockTime") && !line.Contains("Quality")) return true;
                }
            }

            return false;
        }

        private void readFile()
        {
            using (FileStream stream = new FileStream(this.FilePath, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string line = reader.ReadLine();
                    string[] split = line.Split('\t');
                    string[] splits = split[3].Split(',');
                    
                    this.VariableName = splits[0];
                    
                    
                    string[] lines = new string[2];
                    lines[0] = reader.ReadLine();

                    int numLines = 0;
                    string last = null;

                    for (int i = 0; i < 10; i++)
                    {
                        string next = reader.ReadLine();
                        if (next == null)
                            break;
                        last = next;
                        numLines++;
                    }

                    lines[1] = last;

                    double[] times = new double[2];
                    for (int i = 0; i < 2; i++)
                    {
                        double totalSeconds = getTime(lines[i]);
                        times[i] = totalSeconds;
                    }

                    this.FirstTime = times[0];

                    this.SamplingFrequency = 1.0 / ((times[1] - times[0]) / numLines);
                }
            } 

            string[] files = Directory.GetFiles(this.FilePath.Substring(0, this.FilePath.LastIndexOf('\\')));
            string qualityFile = null;
            string[] partFiles = null;
            for (int i = 0; i < files.Length; i++)
            {
              //  if (files[i] == this.FilePath) continue;
                if (files[i].Contains("data_quality") && files[i].Contains(this.VariableName))
                    qualityFile = files[i];
                else if(files[i].Contains(this.VariableName) && files[i].Contains("part"))
                {
                    if (partFiles == null)
                    {
                        int index = files[i].LastIndexOf("of");
                        int index2 = files[i].LastIndexOf('.');
                        int length = int.Parse(files[i].Substring(index + 2, index2 - index - 2));
                        partFiles = new string[length];
                        this.NumberOfPointsPerFile = new int[length];
                    }

                    int ofIndex = files[i].LastIndexOf("of");
                    int partIndex = files[i].LastIndexOf("part");
                   int part = int.Parse(files[i].Substring(partIndex + 4, ofIndex - partIndex - 4));

          

                    partFiles[part - 1] = files[i];

                    using (FileStream stream = new FileStream(files[i], FileMode.Open, FileAccess.Read))
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            string firstLine = reader.ReadLine();
                           char[] buffer = new char[100];
                           int total = reader.Read(buffer, 0, 100);
                           string nextLine = new string(buffer);
                           int index = nextLine.IndexOfAny(new char[] { '\r', '\n' });
                           this.LineLength = index+1 ;
                           if (buffer[index + 1] == '\r' || buffer[index + 1] == '\n')
                               this.LineLength++;

                           // this.LineLength = nextLine.Length + 1;
                            this.NumberOfPointsPerFile[part-1] = (int)((double)(stream.Length - (firstLine.Length+1))/(double)this.LineLength);
                        }
                    }
                }
            }

            this.Files = partFiles;


          
            if (qualityFile != null)
            {
                using (FileStream stream = new FileStream(qualityFile, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string line = reader.ReadLine();
                        line = reader.ReadLine();

                        List<MobergMarker> markers = new List<MobergMarker>();
                        while (line != null)
                        {
                            double time = getTime(line);
                            markers.Add(new MobergMarker() { MarkerLocation = time - this.FirstTime, MarkerName = line.Split('\t')[3] });
                            line = reader.ReadLine();
                        }

                        this.Markers = markers.ToArray();
                    }

                }
            }


        }

        private double getTime(string line)
        {
            string[] timeSplits = line.Split('\t');
            string[] hourSplits = timeSplits[1].Split(':');

            int days = int.Parse(timeSplits[0]);
            int hours = int.Parse(hourSplits[0]);
            int minutes = int.Parse(hourSplits[1]);
            int seconds = int.Parse(hourSplits[2]);
            double milliseconds = double.Parse(timeSplits[2]);

            double totalSeconds = days * (3600 * 24) + hours * 3600 + minutes * 60 + seconds + milliseconds / 1000;
            return totalSeconds;
        }

        #endregion
    }
}
