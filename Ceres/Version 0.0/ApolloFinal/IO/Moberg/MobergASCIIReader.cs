﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO;
using CeresBase.IO.DataBase;
using CeresBase.Data;
using CeresBase.Data.DataPools;
using System.IO;
using CeresBase.Projects;

namespace ApolloFinal.IO.Moberg
{
    class MobergASCIIReader : ICeresReader
    {
        #region ICeresReader Members

        public object ReadAttribute(ICeresFile file, string name, string attrName)
        {
            throw new NotImplementedException();
        }

        public double[][] GetShapeValues(ICeresFile file)
        {
            return null;
        }

        public bool Supported(string file)
        {
            return Moberg.MobergAsciiFile.CanRead(file);
        }

        public bool StayNative
        {
            get { return false; }
        }

        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            return null;
        }

        public CeresBase.IO.DataBase.FileDataBaseVariableEntryParameter[] GetVariableParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            MobergAsciiFile file = new MobergAsciiFile(entry.Filepath);
            SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
            para.VariableName.Value = file.VariableName;
            para.VariableName.IsFixed = true;

            para.Units.Value = "NA";

            para.Frequency.Value = file.SamplingFrequency;
            para.Frequency.IsFixed = true;

            para.SpikeType.Value = SpikeVariableHeader.SpikeType.Raw;
            para.SpikeType.IsFixed = true;

            para.DisplayName.Value = file.VariableName;

            return new FileDataBaseVariableEntryParameter[] { para };
        }

        public CeresBase.Data.Variable[] ReadVariables(string filename, CeresBase.IO.DataBase.FileDataBaseEntryParameter[] fileparams, CeresBase.Data.VariableHeader[] headers)
        {
            MobergAsciiFile file = new MobergAsciiFile(filename);

            IDataPool pool = new DataPoolNetCDF(new int[] { file.TotalPoints });

            int chunkSize = 1000000;
            int numToRead = chunkSize * file.LineLength;
            char[] buffer = new char[numToRead];
            float[] numBuffer = new float[chunkSize];
            int numBuffSpot = 0;

            for (int i = 0; i < file.Files.Length; i++)
            {
                using(FileStream stream = new FileStream(file.Files[i], FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        
                        string first = reader.ReadLine();

                   //     reader.BaseStream.Seek(first.Length, SeekOrigin.Begin);
                     //   reader.DiscardBufferedData();

                        int numChunks = (int)Math.Ceiling(file.NumberOfPointsPerFile[i] / (double)chunkSize);
                        
                        for (int j = 0; j < numChunks; j++)
                        {
                            int numToSkip = 0;

                            int count = numToRead;
                            if(stream.Length - stream.Position < count)
                                count = (int)(stream.Length - stream.Position);
                            int numRead = reader.ReadBlock(buffer, 0, numToRead);

                            if (numToRead < 0) break;
                            for (int k = 0; k < numRead/file.LineLength; k++)
                            {
                                int lineIndex = k * file.LineLength + numToSkip;

                                if (buffer[lineIndex] != '0')
                                {
                                    int s = 0;
                                    //There is a bug where 1000.0 ms is listed
                                    lineIndex++;
                                    numToSkip++;
                                }

                                string toParse = new string(buffer, lineIndex + "00	13:00:35	870.0	".Length, "1.1800000e+002".Length);
                                try
                                {
                                    numBuffer[numBuffSpot] = float.Parse(toParse);
                                    numBuffSpot++;
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }

                                    if (numBuffSpot == chunkSize)
                                    {
                                        pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(numBuffer));
                                        numBuffSpot = 0;
                                    }
  
                            }                            
                        }

                       
                    }
                }
            }

            if (numBuffSpot != 0)
            {
                float[] temp = new float[numBuffSpot];
                Array.Copy(numBuffer, 0, temp, 0, numBuffSpot);
                pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(temp));
            }

            SpikeVariable variable = new SpikeVariable((SpikeVariableHeader)headers[0]);
            DataFrame frame = new DataFrame(variable, pool, CeresBase.General.Constants.Timeless);
            foreach (ApolloFinal.IO.Moberg.MobergAsciiFile.MobergMarker mark in file.Markers)
            {
                EpochCentral.AddEpoch(new Epoch(variable, mark.MarkerName, (float)mark.MarkerLocation, (float)mark.MarkerLocation));
            }
            variable.AddDataFrame(frame);
            return new Variable[] { variable };
            
            
            //SpikeVariable variable = new SpikeVariable((SpikeVariableHeader)headers[0]);
            //DataFrame frame = new DataFrame(variable, pool, CeresBase.General.Constants.Timeless);

            //ushort[] textChans = SONWrapper.GetTextChannels(son.FileID);
            //foreach (ushort chan in textChans)
            //{
            //    SONWrapper.TTextMark[] marks = SONWrapper.GetTextMarkers(son.FileID, chan);
            //    foreach (SONWrapper.TTextMark mark in marks)
            //    {
            //        EpochCentral.AddEpoch(new Epoch(variable, mark.text, (float)Math.Round(mark.time, 2), (float)Math.Round(mark.time, 2)) { AppliesToExperiment = true });
            //    }
            //}

            //variable.AddDataFrame(frame);

            //vars.Add(variable);
            //    son.Dispose();

           // return null;

        }

        #endregion
    }
}
