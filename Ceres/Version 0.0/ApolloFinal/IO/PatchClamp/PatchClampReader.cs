using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using CeresBase.Data;
using System.IO;
using CeresBase.IO.DataBase;

namespace ApolloFinal.IO.PatchClamp
{
    class PatchClampReader : ICeresReader
    {
        //private CeresFileDataBase database;

        public object ReadAttribute(ICeresFile file, string name, string attrName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresBase.Data.Variable ReadVariable(string filename, CeresBase.Data.VariableHeader header, DateTime[] times)
        //{
        //    return this.ReadVariables(filename, new VariableHeader[] { header }, new DateTime[][] { times })[0];
            
        //}

        //public CeresBase.Data.Variable[] ReadVariables(string filename, CeresBase.Data.VariableHeader[] headers, DateTime[][] times)
        //{
        //    PatchClampFile file = new PatchClampFile(filename);

        //    Dictionary<string, object> settings = this.Database.GetSettings(filename);

        //    int[] whichVars = new int[file.NumberChannels];
        //    for (int i = 0; i < file.NumberChannels; i++)
        //    {
        //        for (int j = 0; j < headers.Length; j++)
        //        {
        //            string chanName = ((string)settings["Channel Name " + i.ToString()]);
        //            if (headers[j].VarName == chanName)
        //            {
        //                whichVars[i] = j;
        //                break;
        //            }
        //        }
        //    }
          
        //    List<float>[] data = new List<float>[file.NumberChannels];
        //    for (int i = 0; i < file.NumberChannels; i++)
        //    {
        //        data[i] = new List<float>();
        //    }

        //    using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
        //    {
        //        using (StreamReader reader = new StreamReader(stream))
        //        {
        //            for (int i = 0; i < 10; i++)
        //            {
        //                string kill = reader.ReadLine();
        //            }

        //            string line = null;
        //            while ((line = reader.ReadLine()) != null)
        //            {
        //                string[] splits = line.Split('\t');
        //                for (int i = 0; i < file.NumberChannels; i++)
        //                {
        //                    float val = float.Parse(splits[i + 1]);
        //                    data[whichVars[i]].Add(val);
        //                }
        //            }
        //        }
        //    }

        //    SpikeVariable[] vars = new SpikeVariable[headers.Length];
        //    IDataPool[] pools = new IDataPool[headers.Length];
        //    for (int i = 0; i < pools.Length; i++)
        //    {
        //        pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { data[i].Count });
        //        pools[i].SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(data[i].ToArray()));
        //        vars[i] = new SpikeVariable((SpikeVariableHeader)headers[i]);
        //        DataFrame frame = new DataFrame(vars[i], pools[i], new CeresBase.Data.DataShapes.NoShape(),
        //            CeresBase.General.Constants.Timeless);
        //        vars[i].AddDataFrame(frame);
        //    }
        //    return vars;

        //}

        public byte[] ReadBinary(ICeresFile file, string name)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}

        public double[][] GetShapeValues(ICeresFile file)
        {
            return null;
        }

        //public CeresBase.Data.VariableHeader[] GetAllVariableHeaders(string filename, string experimentName)
        //{
        //    Dictionary<string, object> settings = this.Database.GetSettings(filename);
        //    PatchClampFile file = new PatchClampFile(filename);
        //    VariableHeader[] headers = new VariableHeader[file.NumberChannels];

        //    for (int i = 0; i < file.NumberChannels; i++)
        //    {
        //        headers[i] = new SpikeVariableHeader((string)settings["Channel Name " + i.ToString()],
        //            "V", experimentName, file.Frequency, SpikeVariableHeader.SpikeType.Raw);
        //    }

        //    return headers;
        //}

        public bool Supported(string file)
        {
           return PatchClampFile.IsSupported(file);
        }

        //public void AddFilesToDB(string[] files)
        //{
        //    for (int f = 0; f < files.Length; f++)
        //    {
        //        PatchClampFile file = new PatchClampFile(files[f]);

        //        //TODO HANDLE MULTIPLE FILES
        //        CeresBase.UI.RequestInformation infoReq = new CeresBase.UI.RequestInformation();
        //        List<Type> types = new List<Type>();
        //        List<string> names = new List<string>();
        //        List<string> descriptions = new List<string>();

        //        types.Add(typeof(string));
        //        names.Add("Experiment Name");
        //        descriptions.Add("Please enter the name of the experiment this data is from.");

        //        for (int i = 0; i < file.NumberChannels; i++)
        //        {
        //            types.Add(typeof(string));
        //            names.Add("Channel Name " + i.ToString());
        //            descriptions.Add("Please enter the name for the channel.");
        //        }

        //        infoReq.Grid.SetInformationToCollect(types.ToArray(), null, names.ToArray(), descriptions.ToArray(), null, null);
        //        if (infoReq.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
        //            return;

        //        this.Database.CreateFile((string)infoReq.Results["Experiment Name"], file);
        //        List<string> channelNames = new List<string>();
        //        List<string> chan = new List<string>();
        //        for (int i = 0; i < file.NumberChannels; i++)
        //        {
        //            channelNames.Add((string)infoReq.Results["Channel Name " + i.ToString()]);
        //            chan.Add("Channel Name " + i.ToString());
        //        }
        //        this.Database.SetSettings(files[f], chan.ToArray(), channelNames.ToArray());
        //    }

            
        //}


        #region ICeresReader Members


        public bool StayNative
        {
            get { return false; }
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            return null;
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseVariableEntryParameter[] GetVariableParameters(CeresBase.IO.DataBase.FileDataBaseEntry entry)
        {
            PatchClampFile file = new PatchClampFile(entry.Filepath);
            FileDataBaseVariableEntryParameter[] paras = new FileDataBaseVariableEntryParameter[file.NumberChannels];
            for (int i = 0; i < file.NumberChannels; i++)
            {
                SpikeVariableEntryParameter para = new SpikeVariableEntryParameter();
                para.Units.Value = "V";
                para.Frequency.Value = file.Frequency;
                para.Frequency.IsFixed = true;

                para.SpikeType.Value = SpikeVariableHeader.SpikeType.Raw;
                para.SpikeType.IsFixed = true;

                paras[i] = para;
            }

            return paras;
        }

        #endregion



        #region ICeresReader Members


        public Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
