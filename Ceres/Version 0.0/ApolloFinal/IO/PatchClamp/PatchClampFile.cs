using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.IO;
using System.IO;

namespace ApolloFinal.IO.PatchClamp
{
    class PatchClampFile : ICeresFile
    {
        private string filePath;

        private int numChannels;
        public int NumberChannels
        {
            get { return numChannels; }
        }

        private double frequency;
        public double Frequency
        {
            get { return frequency; }
        }
	

        public static bool IsSupported(string filePath)
        {
            try
            {
                using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        bool toRet = false;
                        string line = reader.ReadLine();
                        if (line == null) return false;
                        if (line.Contains("ATF")) return true;
                        return toRet;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public PatchClampFile(string filePath)
        {
            this.filePath = filePath;
            using(FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                using(StreamReader reader = new StreamReader(stream))
                {
                    for (int i = 0; i < 10; i++)
                    {
                        string kill = reader.ReadLine();
                    }
                    string data = reader.ReadLine();
                    string[] splits = data.Split('\t');
                    this.numChannels = splits.Length - 1;

                    double firstVal = double.Parse(splits[0]);

                    string data2 = reader.ReadLine();
                    splits = data2.Split('\t');
                    double secondVal = double.Parse(splits[0]);

                    this.frequency = 1.0/(secondVal - firstVal);
                }
            }

        }

        public Type VariableType
        {
            get {return typeof(SpikeVariable); }
        }

        public Type ShapeType
        {
            get { return typeof(CeresBase.Data.DataShapes.NoShape); }
        }

        public string FilePath
        {
            get { return this.filePath; }
        }
    }
}
