using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools.AttractorReconstruction
{
    public class NPoint : IComparable
    {
        private float[] points;
        public float[] Points
        {
            get { return points; }
        }

        private int id;
        public int ID
        {
            get { return id; }
        }

        public float CalcDist(NPoint p)
        {
            float dist = 0;
            for (int i = 0; i < p.points.Length; i++)
            {
                dist += (p.points[i] - this.points[i]) * (p.points[i] - this.points[i]);
            }
            return (float)Math.Sqrt(dist);
        }

        public NPoint(int numDims)
        {
            this.points = new float[numDims];
        }
        public NPoint(int numDims, int id)
        {
            this.points = new float[numDims];
            this.id = id;
        }

        public const int AllDimensions = -1;

        private int dimensionToSort = NPoint.AllDimensions;

        public int DimensionToSort
        {
            get { return dimensionToSort; }
            set { dimensionToSort = value; }
        }
	

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (!(obj is NPoint)) throw new ArgumentException("NPoint cannot be compared with non NPoints.");
            NPoint pointB = obj as NPoint;
            if (this.dimensionToSort == NPoint.AllDimensions)
            {
                //implement this later
                throw new Exception("NPoint default not implemented.");
            }
            else
            {
                if (this.points[this.dimensionToSort] > pointB.points[this.dimensionToSort]) return 1;
                if (this.points[this.dimensionToSort] < pointB.points[this.dimensionToSort]) return -1;
                return 0;
            }
        }

        #endregion
    }
}
