using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools.AttractorReconstruction
{
    public static class AttractorReconstruction
    {
        public static NPoint[] CreateTimeEmbedding(float[] data, int embeddingDimension, int delay)
        {
            List<NPoint> points = new List<NPoint>();
            for (int i = 0; i < data.Length - delay*embeddingDimension; i++)
            {
                NPoint point = new NPoint(embeddingDimension, i);
                for (int j = 0; j < embeddingDimension; j++)
                {
                    point.Points[j] = data[i + j * delay];
                }
                points.Add(point);
            }
            return points.ToArray();
        }

        public static NPoint[] CreateMultiTimeEmbedding(float[][] data, int embeddingDimension, int[] delays)
        {
            int maxDelay = int.MinValue;
            int numVars = delays.Length;
            for (int i = 0; i < delays.Length; i++)
            {
                if (delays[i] > maxDelay)
                {
                    maxDelay = delays[i];
                }
            }

            int toSub = maxDelay * embeddingDimension;
            int length = data[0].Length - toSub;
            List<NPoint> points = new List<NPoint>();
            for (int i = 0; i < length; i++)
            {
                NPoint point = new NPoint(embeddingDimension * numVars, i);
                for (int j = 0; j < embeddingDimension; j++)
                {
                    for (int k = 0; k < numVars; k++)
                    {
                        point.Points[j * numVars + k] = data[k][i + j * delays[k]];
                    }
                }
                points.Add(point);
            }

            return points.ToArray();

        }
    }
}
