﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApolloFinal.Tools.IntervalTools;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;
using CeresBase.Projects;

namespace ApolloFinal.Tools.MiscDataTools
{
    [AdapterDescription("Mixed")]
    class MixedAdapter : DataAdapter
    {
        public override string Name
        {
            get
            {
                return "Mixed Data Adapter";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Data Sources";
            }
            set
            {
                
            }
        }

        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public IntervalPackage[] Packages { get; set; }
        public  Epoch[] Epochs { get; set; }
        private Dictionary<string, float[]> data = new Dictionary<string,float[]>();

        [AssociateWithProperty("Epoch Description")]
        public string[] EpochDescription
        {
            get
            {
                return this.Epochs.Select(t => t.FullDescription).ToArray();
            }
        }

        [AssociateWithProperty("Raw Names")]
        public string[] RawNames 
        {
            get
            {
                return this.Epochs.Select(t => t.Variable.FullDescription).ToArray();
            }
        }

        [AssociateWithProperty("Raw Data")]
        public float[][] RawData 
        { 
            get
            {
                List<float[]> toRet = new List<float[]>();
                for (int i = 0; i < this.Epochs.Length; i++)
                {
                    if(!data.ContainsKey(this.Epochs[i].ToString()))
                    {
                        data.Add(this.Epochs[i].ToString(), SpikeVariable.ReadData(this.Epochs[i].Variable as SpikeVariable, this.Epochs[i].BeginTime, this.Epochs[i].EndTime));
                    }

                    toRet.Add(data[this.Epochs[i].ToString()]);
                }

                return toRet.ToArray();
            }
        }
        [AssociateWithProperty("Raw Resolutions")]
        public double[] RawResolutions { get { return this.Epochs.Select(t => (t.Variable as SpikeVariable).Resolution).ToArray(); } }
        [AssociateWithProperty("Raw Start Times")]
        public float[] RawStarts { get { return this.Epochs.Select(t => t.BeginTime).ToArray(); } }
        [AssociateWithProperty("Raw End Times")]
        public float[] RawEnds { get { return this.Epochs.Select(t => t.EndTime).ToArray(); } }

        [AssociateWithProperty("Interval Names")]
        public string[][] IntervalNames { get { return this.Packages.Select(t => t.IntervalNames).ToArray(); } }
        [AssociateWithProperty("Interval Lengths")]
        public float[][][] IntervalLengths { get { return this.Packages.Select<IntervalPackage, float[][]>(t => t.GetAllIntervalLengths()).ToArray(); } }
        [AssociateWithProperty("Interval Start Times")]
        public float[][] IntervalStarts { get { return this.Packages.Select(t => t.BeginTimes).ToArray(); } }
        [AssociateWithProperty("Interval End Times")]
        public float[][] IntervalEnds { get { return this.Packages.Select(t => t.EndTimes).ToArray(); } }

        public override object Clone()
        {
            MixedAdapter adapter = new MixedAdapter();
            adapter.Packages = this.Packages;
            adapter.Epochs = this.Epochs;
            this.cloneHelper(adapter);

            return adapter;
        }
    }
}
