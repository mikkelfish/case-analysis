using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools.MiscDataTools
{
    public static class EulerDerivative
    {
        public static double[] Derivative(double[] data, float h)
        {
            //f(x+h)-2f(x)+f(x-h)/h^2
            List<double> toRet = new List<double>();
            for (int i = 1; i < data.Length-1; i++)
            {
                double val = (data[i + 1] - 2 * data[i] + data[i - 1]) / (h * h);
                toRet.Add(val);
            }
            return toRet.ToArray();
        }
    }
}
