//using System;
//using System.Collections.Generic;
//using System.Text;
//using MathWorks.MATLAB.NET.Arrays;
//using CeresBase.UI;
//using System.Windows.Forms;
//using CeresBase.Data;
//using System.IO;
//using CeresBase.FlowControl.Adapters.Matlab;

//namespace ApolloFinal.Tools.DataTools
//{
//    public class ButterFilter
//    {
//        #region IBatchProcessable Members

//        public bool SuppportsMultiple
//        {
//            get { return true; }
//        }

//        public override string ToString()
//        {
//            return "Butter Filter";
//        }

//        public object[] GetArguments(SpikeVariable[] varsToUse, out string[] names)
//        {
//            RequestInformation req = new RequestInformation();
//            req.Grid.SetInformationToCollect(new Type[] { typeof(float), typeof(float), typeof(ButterFilter.FilterType) },
//                new string[] { "Filter", "Filter", "Filter" },
//                new string[] { "Freq1", "Freq2", "Filter Type" },
//                new string[] { "First (and only if low or high) frequency", "Second (for bandpass) frequency", "What type of filter to apply" },
//                null, null);
//            if (req.ShowDialog() == DialogResult.Cancel)
//            {
//                names = null;
//                return null;
//            }
//            float timeConstant1 = (float)req.Grid.Results["Freq1"];
//            float freq2 = (float)req.Grid.Results["Freq2"];
//            ButterFilter.FilterType type =
//                (ButterFilter.FilterType)req.Grid.Results["Filter Type"];

//            names = new string[varsToUse.Length];
//            for (int i = 0; i < varsToUse.Length; i++)
//            {
//                names[i] = varsToUse[i].FullDescription + " Filtered " + type.ToString() + " " + timeConstant1.ToString("F0") + "-" +
//                    freq2.ToString("F0");
//            }

//            return new object[] { timeConstant1, freq2, type };

//        }


//        public object[] Process(SpikeVariable[] var, object[] args)
//        {
//            float thresholdFreq = (float)args[0];
//            float freq2 = (float)args[1];
//            FilterType filterType = (FilterType)args[2];
//            SpikeVariable[] toRet = new SpikeVariable[var.Length];

//            for (int i = 0; i < var.Length; i++)
//            {
//                double freq = (var[i].Header as SpikeVariableHeader).Frequency;
//                IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF((var[i][0] as DataFrame).Pool.DimensionLengths);

//                EverythingMatlab.EverythingMatlabclass matlab = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;


//                (var[i][0] as DataFrame).Pool.GetData(new int[] { 0 }, new int[] { (var[i][0] as DataFrame).Pool.DimensionLengths[0] },
//                    delegate(float[] data, uint[] indices, uint[] counts)
//                    {
//                        MWNumericArray array = new MWNumericArray(data, false, true);
//                        MWNumericArray thresholds = null;
//                        if (filterType == FilterType.BandPass)
//                        {
//                            double[] threshs = new double[2];
//                            threshs[0] = thresholdFreq;
//                            threshs[1] = freq2;
//                            thresholds = new MWNumericArray(threshs);
//                        }
//                        else thresholds = new MWNumericArray(thresholdFreq);
//                        MWNumericArray frequency = new MWNumericArray(freq);


//                        MWCharArray type = null;
//                        switch (filterType)
//                        {
//                            case FilterType.HighPass:
//                                type = new MWCharArray("high");
//                                break;
//                            case FilterType.LowPass:
//                                type = new MWCharArray("low");
//                                break;
//                        }

//                        lock (MatlabLoader.MatlabLockable)
//                        {
//                            MWNumericArray filtered = null;
//                            if (filterType == FilterType.BandPass)
//                                filtered = (MWNumericArray)matlab.BandButterworthFilter(array, frequency, thresholds);
//                            else filtered = (MWNumericArray)matlab.ButterworthFilter(array, frequency, thresholds, type);

//                            float[] filteredF = (float[])filtered.ToVector(MWArrayComponent.Real);
//                            bool keep = true;
//                            for (int z = 0; z < filteredF.Length; z++)
//                            {
//                                if (filteredF[z] == float.NaN || filteredF[z] < -1000 || filteredF[z] > 1000)
//                                {
//                                    int s = 0;
//                                    filteredF = new float[filteredF.Length];
//                                }
//                            }

//                            using (MemoryStream stream = CeresBase.Data.DataUtilities.ArrayToMemoryStream(filteredF))
//                            {
//                                pool.SetData(stream);
//                            }

//                            filtered.Dispose();
//                            array.Dispose();
//                        }
//                    }
//                );

//                SpikeVariableHeader header = new SpikeVariableHeader(var[i].Header.VarName + " Filtered " + filterType.ToString() + " "
//                    + thresholdFreq.ToString("F0") + "-" + freq2.ToString("F0"), var[i].Header.UnitsName,
//                    var[i].Header.ExperimentName, (var[i].Header as SpikeVariableHeader).Frequency,
//                    (var[i].Header as SpikeVariableHeader).SpikeDesc);
//                header.Description = var[i].Header.VarName + " Filtered " + filterType.ToString() + " "
//                    + thresholdFreq.ToString("F0") + "-" + freq2.ToString("F0");
//                toRet[i] = new SpikeVariable(header);
//                DataFrame frame = new DataFrame(toRet[i], pool, CeresBase.General.Constants.Timeless);
//                toRet[i].AddDataFrame(frame);
//            }
//            return toRet;
//        }

//        public Type OutputType
//        {
//            get { return typeof(SpikeVariable); }
//        }

//        #endregion

//        public enum FilterType { HighPass, LowPass, BandPass };

//        #region ICloneable Members

//        public object Clone()
//        {
//            return new ButterFilter();
//        }

//        #endregion
//    }
//}
