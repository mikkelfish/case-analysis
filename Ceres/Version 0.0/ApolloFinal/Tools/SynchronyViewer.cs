using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CeresBase.IO.ImageCreation;

namespace ApolloFinal.Tools
{
    public partial class SynchronyViewer : Form
    {
        private SpikeVariable[] vars;
        public SpikeVariable[] Variables
        {
            get { return vars; }
            set { vars = value; }
        }

        private double[][][] spikes;
        public double[][][] Spikes
        {
            get { return spikes; }
            set { spikes = value; }
        }

        private int windowWidth;

        public int WindowWidth
        {
            get { return windowWidth; }
            set { windowWidth = value; }
        }
	

        private int startMs;

        public int StartMS
        {
            get 
            { 
                return startMs; 
            }
            set 
            { 
                startMs = value;
                this.setHBar();
            }
        }

        private void setHBar()
        {
            this.hScrollBar.Minimum = startMs;
            this.hScrollBar.LargeChange = (endMs-startMs) / 100;
            this.hScrollBar.SmallChange = (endMs-startMs) / 1000;
            this.hScrollBar.Maximum = (endMs - this.length - startMs) + this.hScrollBar.LargeChange;

            
        }

        private int length = 0;
        public int Length
        {
            get
            {
                return this.length/1000;
            }
            set
            {
                this.length = value*1000;
                this.setHBar();
            }
        }

        private int endMs;
        public int EndMS
        {
            get { return endMs; }
            set 
            { 
                endMs = value;
                this.setHBar();
            }
        }
	
	
        public SynchronyViewer()
        {
            InitializeComponent();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            if (this.listBoxVars.SelectedItems.Count < 2) return;
            this.flowLayoutPanelStorage.Controls.Clear();
            this.flowLayoutPanelStorage.Padding = new Padding(0,0,0,0);
            for (int i = 0; i < this.listBoxVars.SelectedIndices.Count; i++)
            {
                int index1 = this.listBoxVars.SelectedIndices[i];
                for (int j = 0; j < this.listBoxVars.SelectedIndices.Count; j++)
                {
                    if (i == j) continue;

                    int index2 = this.listBoxVars.SelectedIndices[j];
                    Label lbl = new Label();
                    lbl.Text = this.vars[index1].Header.Description + " vs. " + this.vars[index2].Header.Description;
                    lbl.AutoSize = true;
                 //   this.flowLayoutPanelStorage.Controls.Add(lbl);

                    Panel panel = new Panel();
                    panel.Tag = new int[] { index1, index2 };
                    panel.BackColor = Color.White;
                    panel.Paint += new PaintEventHandler(panel_Paint);
                    panel.Size = new Size(this.flowLayoutPanelStorage.Width - 30, 5);
                    panel.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
                    panel.GetType().InvokeMember("DoubleBuffered",
                        System.Reflection.BindingFlags.NonPublic |
                        System.Reflection.BindingFlags.Instance |
                        System.Reflection.BindingFlags.SetProperty,
                        null, panel, new object[] { true });
                    
                    this.flowLayoutPanelStorage.Controls.Add(panel);
                }
            }
        }

        void drawPanel(Panel panel, ImageCreator creator, ImageContainer parent)
        {
            if (panel == null || panel.Tag == null) return;
            int index1 = (panel.Tag as int[])[0];
            int index2 = (panel.Tag as int[])[1];
            int start = this.hScrollBar.Value;
            int end = start + this.length;

            int lastGood = 0;
            bool first = true;
            for (int i = 0; i < this.spikes[index1].Length; i++)
            {
                if (this.spikes[index1][i][0] > end) break;
                if (first && this.spikes[index1][i][this.spikes[index1][i].Length - 1] < start) continue;

                if (first && i != 0)
                {
                    i -= 2;
                    first = false;
                    continue;
                }

                double startSpike = this.spikes[index1][i][0];
                double endSpike = this.spikes[index1][i][this.spikes[index1][i].Length - 1];

                bool added = false;

                for (int j = lastGood; j < this.spikes[index2].Length; j++)
                {
                    double ss2 = this.spikes[index2][j][0];
                    double es2 = this.spikes[index2][j][this.spikes[index2][j].Length - 1];

                    //if the end of the compared spike is less than the startSpike
                    if (startSpike - this.windowWidth > es2)
                    {
                        lastGood++;
                        continue;
                    }

                    //if the beginning of the compared spike is greater than the end of the ref spike
                    if (endSpike + this.windowWidth < ss2)
                    {
                        break;
                    }

                    double startPerc = (double)(this.spikes[index1][i][0] - start) / (double)(this.length);
                    int startX = (int)(startPerc * parent.Size.Width);
                    int endX = 0;
                    if (i + 1 < this.spikes[index1].Length)
                    {
                        double endPerc = (double)(this.spikes[index1][i+1][0] - start) / (double)(this.length);
                        endX = (int)(endPerc * parent.Size.Width);

                    }
                    else endX = (int)parent.Size.Width;

                    if (startX < 0)
                    {
                        startX = 0;
                    }
                    if (endX > parent.Size.Width)
                    {
                        endX = (int)parent.Size.Width;
                    }

                    RectangleElement rect = new RectangleElement(parent,
                        new PointF(startX, 0), endX - startX, parent.Size.Height);
                    rect.Fill = true;
                    rect.Color = Color.Red;
                    creator.AddImageElement(rect);
                    added = true;
                }

                //if (!added)
                //{
                    double startPerc2 = (double)(this.spikes[index1][i][0] - start) / (double)(this.length);
                    int startX2 = (int)(startPerc2 * parent.Size.Width);
                    if (startX2 < 0)
                    {
                        startX2 = 0;
                    }

                    Line line = new Line(parent, new PointF(startX2, 0), new PointF(startX2, parent.Size.Height));
                    line.Color = Color.Black;
                    creator.AddImageElement(line);
                //}
            }

        }

        void panel_Paint(object sender, PaintEventArgs e)
        {
            Panel panel = sender as Panel;
            ImageContainer parent = new ImageContainer(null, new Rectangle(0, 0, panel.Width, panel.Height));
            GDI gdi = new GDI();
            gdi.AddImageElement(parent);         
            this.drawPanel(panel, gdi, parent);
            gdi.Create(new object[] { e.Graphics });
        }

        private void SynchronyViewer_Load(object sender, EventArgs e)
        {
            this.listBoxVars.Items.AddRange(this.vars);
            this.length = this.endMs - this.startMs;
            this.numericUpDown1.Maximum = this.Length;
            this.numericUpDown1.DataBindings.Add("Value", this, "Length", false, DataSourceUpdateMode.OnPropertyChanged);

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            //this.setHBar();
            this.labelEnd.Text = ((this.hScrollBar.Value/1000) + this.numericUpDown1.Value).ToString();
            foreach (Control cont in this.flowLayoutPanelStorage.Controls)
            {
                cont.Invalidate();
            }
        }

        private void hScrollBar_ValueChanged(object sender, EventArgs e)
        {
            this.labelStart.Text = (this.hScrollBar.Value/1000).ToString();
            this.labelEnd.Text = (this.hScrollBar.Value/1000 + this.Length).ToString();
            foreach (Control cont in this.flowLayoutPanelStorage.Controls)
            {
                cont.Invalidate();
            }


        }

        private void flowLayoutPanelStorage_Resize(object sender, EventArgs e)
        {
            foreach (Control cont in this.flowLayoutPanelStorage.Controls)
            {
                if (cont is Panel)
                {
                    cont.Size = new Size(this.flowLayoutPanelStorage.Width - 30, 8);                    
                }
            }

        }

        private void saveScreen(string path)
        {
            SVG svg = new SVG();
            int y = 0;
            int height = 500;
            int width = 700;
            int spacing = 5;
            int perheight = 10;

            ImageContainer bigContainer = new ImageContainer(null, new Rectangle(0, 0, width, height));
            svg.AddImageElement(bigContainer);
            foreach (Control cont in this.flowLayoutPanelStorage.Controls)
            {
                ImageContainer panelCont = new ImageContainer(bigContainer, new Rectangle(0, y, width, perheight));
                RectangleElement rect = new RectangleElement(bigContainer, new PointF(0, y), width, perheight);
                rect.Color = Color.Black;
                rect.Fill = false;
                svg.AddImageElement(rect);

                svg.AddImageElement(panelCont);
                this.drawPanel(cont as Panel, svg, panelCont);
                y += perheight;
                y += spacing;
            }

            svg.Create(new object[] { path });
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SaveFileDialog diag = new SaveFileDialog();
            diag.AddExtension = true;
            diag.DefaultExt = ".svg";
            diag.Filter = "Scalable Vector Graphics(*.svg)|*.svg|All Files(*.*)|*.*";
            if (diag.ShowDialog() == DialogResult.Cancel) return;
            this.saveScreen(diag.FileName);
        }
    }
}