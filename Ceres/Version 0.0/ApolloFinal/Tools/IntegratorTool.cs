using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using System.IO;
using CeresBase.UI.MethodEditor;
using CeresBase.Development;

namespace ApolloFinal.Tools
{
    public static partial class BasicTools
    {
        private class integratorValues
        {
            public float decay;
            public int writeEvery;
            public float[] analogValue;
            public int[] readCount;
            public int written = 0;
        }

        [CeresToDo("Mikkel", "03/20/06", Comments = "Figure out standard way to specify what to integrate.")]
        [CeresToDo("Mikkel", "03/20/06", Comments = "Should offer full variable integration, 2D slice integration and point over time")]
        //[EditorFunction("Do a moving time average integration.", "getIntegratorLengths", "getNumIntegratorVars", "integratorCallback", SupportsStrides = false)]
        public static object Integrator( 
            [EditorParam("Input Frequency in HZ")]
            int inputFreq,
            [EditorParam("Output Frequency in HZ")]
            int outputFreq, 
            [EditorParam("Time Constant")]
            double timeConstant
            )
        {
            integratorValues vals = new integratorValues();
            vals.decay = (float)Math.Pow(Math.E, -(1000.0 / inputFreq) / timeConstant);
            vals.writeEvery = inputFreq / outputFreq;
            //vals.analogValue = 0;
            //vals.readCount = 0;
            return vals;
        }

        private static string[] getNumIntegratorVars(VariableHeader[][] headers, string name)
        {
            string[] toRet = new string[headers[0].Length];
            for (int i = 0; i < headers[0].Length; i++)
            {
                toRet[i] = name + " " + headers[0][i].VarName;
            }
            return toRet;
        }

        private static int[][] getIntegratorLengths(object tag, IDataPool[][] source)
        {
            integratorValues vals = tag as integratorValues;
            int[][] toRet = new int[source[0].Length][];
            for (int i = 0; i < source.Length; i++)
            {
                int ilength = (int)Math.Floor(source[0][i].DimensionLengths[0] / (double)vals.writeEvery);
                double dlength = source[0][i].DimensionLengths[0] / (double)vals.writeEvery;
                if (ilength == dlength) toRet[i] = new int[] { ilength - 1 };
                else toRet[i] = new int[] { ilength };
            }

            vals.analogValue = new float[source.Length];
            vals.readCount = new int[source.Length];
            return toRet;
        }

        private static void integratorCallback(IDataPool[] pools, float[][] data,
            uint[] indices, uint[] counts, object tag)
        {
            integratorValues vals = tag as integratorValues;
            for (int i = 0; i < pools.Length; i++)
            {
                if (data[i] == null) continue;
                for (int j = 0; j < data[i].Length; j++, vals.readCount[i]++)
                {
                    float val = data[i][j];

                    vals.analogValue[i] = vals.analogValue[i] * vals.decay + Math.Abs(val);
                    if (vals.readCount[i] != 0 && vals.readCount[i] % vals.writeEvery == 0)
                    {
                        pools[i].SetDataValue(vals.analogValue[i]);
                        vals.written++;
                    }
                }                
            }
        }
    }
}
