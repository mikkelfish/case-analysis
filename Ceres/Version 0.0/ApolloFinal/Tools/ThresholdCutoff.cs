using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CeresBase.Data;
using ApolloFinal.UI.Scope;
using CeresBase.IO.Controls;
using CeresBase.IO;

namespace ApolloFinal.Tools
{
    public partial class ThresholdCutoff : Form
    {
        private Timer timer = new Timer();
        private SpikeVariable newvar;

        public ThresholdCutoff()
        {
            this.timer.Tick += new EventHandler(timer_Tick);

            InitializeComponent();
        }

        private void ThresholdCutoff_Load(object sender, EventArgs e)
        {
            foreach (Variable var in CeresBase.Data.VariableSource.Variables)
            {
                this.comboBox1.Items.Add(var);
            }
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            if (this.comboBox1.SelectedItem == null)
            {
                MessageBox.Show("Select a variable first");
                return;
            }

            SpikeVariable var = (SpikeVariable)this.comboBox1.SelectedItem;
            int index1 = (int)(this.hScrollBar1.Value / var.Resolution);
            int index2 = (int)((this.hScrollBar1.Value + this.scopeControl1.Length) / (var.Resolution));
            float avg = 0;
            (var[0] as DataFrame).Pool.GetData(new int[] { index1 }, new int[] { index2 - index1 },
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        avg += data[i];
                    }
                }
            );
            avg /= (index2 - index1);
            float upper = (float)this.scopeControl1.Max - avg;
            float lower = (float)this.scopeControl1.Min - avg;

            float[] buffer = new float[500000];
            int index = 0;

            IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { (var[0] as DataFrame).Pool.DimensionLengths[0] });
            (var[0] as DataFrame).Pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    float newavg = 0;
                    for (int i = 0; i < data.Length; i++)
                    {
                        newavg += data[i];
                    }
                    newavg /= data.Length;
                    for (int i = 0; i < data.Length; i++)
                    {
                        float val = data[i];
                        float diff = val - newavg;
                        if (diff > 0 && diff > upper)
                        {
                            val = newavg + upper;
                        }
                        else if (diff < 0 && diff < lower)
                        {
                            val = newavg + lower;
                        }

                        buffer[index] = val;
                        index++;
                        if (index == buffer.Length)
                        {
                            pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(buffer));
                            index = 0;
                        }                        
                        // pool.SetDataValue(val);
                    }
                }
            );

            if (index != 0)
            {
                float[] temp = new float[index];
                Array.Copy(buffer, 0, temp, 0, index);
                pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(buffer));
            }

            SpikeVariableHeader header = new SpikeVariableHeader(var.Header.Description + " Threshold", var.Header.UnitsName,
                var.Header.ExperimentName, 1.0 / var.Resolution, var.SpikeType);
            this.newvar = new SpikeVariable(header);
            DataFrame frame = new DataFrame(newvar, pool, CeresBase.General.Constants.Timeless);
            this.newvar.AddDataFrame(frame);

            this.scopeControl1.RemoveVariable((SpikeVariable)this.comboBox1.SelectedItem);
            this.scopeControl1.AddVariable(newvar);
            this.scopeControl1.Min = frame.Min;
            this.scopeControl1.Max = frame.Max;

            MessageBox.Show("Threshold completed");

            this.scopeControl1.Invalidate();
           // new

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.scopeControl1.Variables.Length != 0)
            {
                this.scopeControl1.RemoveVariable(this.scopeControl1.Variables[0]);
            }

            SpikeVariable var = (SpikeVariable)this.comboBox1.SelectedItem;
            this.scopeControl1.AddVariable(var);
            this.scopeControl1.Length = 30;

            this.hScrollBar1.Maximum = (int)(var.TotalLength - 30);
            this.hScrollBar1.Minimum = 0;
            this.hScrollBar1.LargeChange = (int)(this.hScrollBar1.Maximum * .01);
            this.scopeControl1.Invalidate();
        }

        #region Scroll callbacks

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            double divide = this.scopeControl1.Length / 4;
            if (Microsoft.Win32.Imports.IsKeyDown(Keys.ControlKey))
            {
                divide = this.scopeControl1.Length / 2;
            }
            else if (Microsoft.Win32.Imports.IsKeyDown(Keys.ShiftKey))
            {
                divide = this.scopeControl1.Length;
            }

            if (e.Type == ScrollEventType.LargeIncrement)
            {
                if (e.OldValue + divide < this.hScrollBar1.Maximum)
                {
                    this.hScrollBar1.Value = (int)(e.OldValue + divide);
                    e.NewValue = this.hScrollBar1.Value;
                }
                else this.hScrollBar1.Value = this.hScrollBar1.Maximum;
            }
            else if (e.Type == ScrollEventType.LargeDecrement)
            {
                if (e.OldValue - divide > this.hScrollBar1.Minimum)
                {
                    this.hScrollBar1.Value = (int)(e.OldValue - divide);
                    e.NewValue = this.hScrollBar1.Value;
                }
                else this.hScrollBar1.Value = 0;
            }

            if (e.Type != ScrollEventType.EndScroll || e.Type == ScrollEventType.ThumbPosition) return;
            if (timer.Enabled) timer.Stop();

            this.Invoke(new CeresBase.General.Utilities.EmptyHandler(this.setNewControlValue));
        }

        private void setNewControlValue()
        {
            this.scopeControl1.Start = this.hScrollBar1.Value;
        }


        void timer_Tick(object sender, EventArgs e)
        {
            this.scopeControl1.Start = this.hScrollBar1.Value;
            this.timer.Stop();
        }


        private void hScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            if (timer.Enabled) timer.Stop();
            timer.Interval = 100;
            timer.Start();
        }

        #endregion

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.newvar == null)
            {
                MessageBox.Show("Please press Test to create the new variable before exiting.");
                return;
            }

            VariableSource.AddVariable(this.newvar);
            IOUtilities.AddVariableToDisk("Threshold Created, Save to Disk?",  new Variable[] { this.newvar });
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}