﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects;
using ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    public class ScoringBehavior
    {
        public const string DefaultBehaviorName = "New Behavior";

        public ScoringBehavior()
        {
            this.Name = ScoringBehavior.DefaultBehaviorName;
            this.Pattern = new PatternImplementation();
            this.Algorithm = new EmptyAlgorithm();
        }

        public ScoringBehavior(string name, IScoringAlgorithm algorithm, PatternImplementation pattern)
        {
            this.Algorithm = algorithm;
            this.Pattern = pattern;
            this.Name = name;
        }

        public ScoringBehavior CloneEmpty()
        {
            ScoringBehavior newBehavior = new ScoringBehavior();
            newBehavior.Pattern = this.Pattern.CloneEmpty();
            newBehavior.Algorithm = this.Algorithm.CloneEmpty();
            newBehavior.Name = this.Name;

            return newBehavior;
        }

        public IScoringAlgorithm Algorithm
        {
            get;
            private set;
        }

        public PatternImplementation Pattern
        {
            get;
            private set;
        }

        public Epoch Epoch
        {
            get;
            set;
        }

        public void Score()
        {
            this.Score(null);
        }

        public void Score(float[] transformedData, double res, ScoringSettings passedSettings)
        {
            if (this.Epoch == null) return;

            this.Pattern.RefrainFromMarking = true;


            CycleSeries series = null;

            if (passedSettings == null)
                series = this.Algorithm.Score(this.Epoch, transformedData, res);
            else series = this.Algorithm.Score(this.Epoch, transformedData, res, passedSettings);

            if (series == null) return;

            this.Pattern.ConnectWithSeries(series);

            this.Pattern.RefrainFromMarking = false;
        }

        public void Score(ScoringSettings passedSettings)
        {
            if (this.Epoch == null) return;

            this.Pattern.RefrainFromMarking = true;


            CycleSeries series = null;

            if (passedSettings == null)
                series = this.Algorithm.Score(this.Epoch);
            else series = this.Algorithm.Score(this.Epoch, passedSettings);

            if (series == null) return;

            this.Pattern.ConnectWithSeries(series);

            this.Pattern.RefrainFromMarking = false;
        }

        public string Name { get; private set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
