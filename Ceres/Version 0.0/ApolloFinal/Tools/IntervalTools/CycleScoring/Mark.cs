﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.Interfaces;
using System.ComponentModel;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    public class Mark
    {
        private float time;
        public float Time 
        {
            get
            {
                return this.time;
            }
            set
            {
                float oldEnd = this.time + this.length;
                this.time = value;

                if (this.Implementation == null) return;

                if (this.Implementation.AllMarks == null)
                {
                    this.Length = oldEnd - this.time;
                    return;
                }

                int index = -1;
                for (int i = 0; i < this.Implementation.AllMarks.Count; i++)
                {
                    if (this.Implementation.AllMarks[i] == this)
                    {
                        index = i;
                        break;
                    }
                }


                if (index > 0)
                {
                    if (this.time < this.Implementation.AllMarks[index - 1].time)
                    {
                        this.time = this.Implementation.AllMarks[index - 1].time;
                    }
                }

                

                this.Length = oldEnd - this.time;

                index = -1;
                for (int i = 0; i < this.Implementation.AllMarks.Count; i++)
                {
                    if (this.Implementation.AllMarks[i] == this)
                    {
                        index = i;
                        break;
                    }
                }


                if (index > 0)
                {
                    this.Implementation.AllMarks[index - 1].length = this.time - this.Implementation.AllMarks[index - 1].time;
                    if (this.Implementation.AllMarks[index - 1].length <= 0)
                    {
                        this.Implementation.AllMarks.RemoveAt(index - 1);
                        index--;

                        if (index > 0)
                        {
                            if (this.Implementation.AllMarks[index - 1].ID == this.Implementation.AllMarks[index].ID)
                            {
                                this.Implementation.AllMarks[index - 1].length += this.Implementation.AllMarks[index].length;
                                this.Implementation.AllMarks.RemoveAt(index);
                            }
                        }
                    }
                }
            }
        }

        private float length;
        public float Length 
        {
            get
            {
                return this.length;
            }
            set
            {
                this.length = value;

                if (this.length < 0)
                {
                    this.length = 0;
                }

                int index = -1;

                if (this.Implementation == null) return;


                if (this.Implementation.AllMarks == null) return;

                for (int i = 0; i < this.Implementation.AllMarks.Count; i++)
                {
                    if (this.Implementation.AllMarks[i] == this)
                    {
                        index = i;
                        break;
                    }
                }

                if (this.length == 0)
                {
                    if (index > 0 && index < this.Implementation.AllMarks.Count - 1)
                    {
                        this.Implementation.AllMarks[index - 1].length = this.Implementation.AllMarks[index + 1].time -
                            this.Implementation.AllMarks[index - 1].time;
                        this.Implementation.AllMarks.RemoveAt(index);

                        if (this.Implementation.IntervalNames.Length != 1 && this.Implementation.AllMarks[index - 1].ID == this.Implementation.AllMarks[index].ID)
                        {
                            this.Implementation.AllMarks[index - 1].length += this.Implementation.AllMarks[index].length;
                            this.Implementation.AllMarks.RemoveAt(index);
                        }
                    }
                }
                else
                {

                    if (index < this.Implementation.AllMarks.Count - 1 && index >= 0)
                    {
                        float oldEnd = this.Implementation.AllMarks[index + 1].time + this.Implementation.AllMarks[index + 1].length;
                        this.Implementation.AllMarks[index + 1].time = this.time + this.length;
                        this.Implementation.AllMarks[index + 1].length = oldEnd - this.Implementation.AllMarks[index + 1].time;

                        if (this.Implementation.AllMarks[index + 1].length < 0)
                        {
                            this.Implementation.AllMarks[index].length = oldEnd - this.Implementation.AllMarks[index].time;
                            this.Implementation.AllMarks.RemoveAt(index + 1);

                            if (index + 1 < this.Implementation.AllMarks.Count &&
                                this.Implementation.AllMarks[index + 1].ID == this.Implementation.AllMarks[index].ID)
                            {
                                this.Implementation.AllMarks[index].length += this.Implementation.AllMarks[index + 1].length;
                                this.Implementation.AllMarks.RemoveAt(index + 1);
                            }
                        }
                    }
                }
            }
        }
        public int ID { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public PatternImplementation Implementation { get; private set; }


        public Mark(PatternImplementation impl, float start)
        {
            this.Implementation = impl;
            this.time = start;
        }

        public Mark(PatternImplementation impl, float start, int id, float len)
        {
            this.Implementation = impl;
            this.time = start;
            this.ID = id;
            this.length = len;
        }

        public Mark()
        {

        }

 
    }
}
