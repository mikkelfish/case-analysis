﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MesosoftCommon.Adorners;
using MesosoftCommon.Controls;
using System.Collections.ObjectModel;
using System.Windows.Threading;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    /// <summary>
    /// Interaction logic for ScoredOverlayControl.xaml
    /// </summary>
    public partial class ScoredOverlayControl : UserControl
    {
        public ScoredOverlayControl()
        {
            InitializeComponent();

           
        }

        private class scoredOverlayConverter : IValueConverter
        {
            public Mark Mark { get; private set; }
            public float StartTime { get; set; }
            public float ScreenWidth { get; set; }

            public double ControlWidth { get; private set; }
            public Grid Grid { get; private set; }

            public scoredOverlayConverter(Mark mark, float startTime, float screenWidth, double controlWidth, Grid grid)
            {
                this.Mark = mark;
                this.StartTime = startTime;
                this.ScreenWidth = screenWidth;
                this.ControlWidth = controlWidth;
                this.Grid = grid;
            }

            #region IValueConverter Members

            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                bool returnXInstead = false;

                if (parameter is bool)
                {
                    returnXInstead = (bool)parameter;
                }

                float endTime = this.StartTime + this.ScreenWidth;
                float perc = (this.Mark.Time - this.StartTime) / (endTime - this.StartTime);
                double x = this.ControlWidth * perc;

                if (returnXInstead)
                {
                    return new GridLength(x, GridUnitType.Star);
                }

                double widthPerc = (this.Mark.Length) / (endTime - this.StartTime);
                double width = this.ControlWidth* widthPerc;

                if (x < 0)
                {
                    width += x;
                    x = 0;
                }

                if (x + width > this.ControlWidth)
                {
                    width = this.ControlWidth - x;
                }

                return new GridLength(width, GridUnitType.Star);
            }

            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                //ColumnDefinition def = parameter as ColumnDefinition;

                //double perc = def.Offset / this.ControlWidth;

                //double newStart = this.ScreenWidth * perc;
                //this.Mark.Time =  this.StartTime + (float)newStart;

                //GridLength length = (GridLength)value;
                //double timePerc = length.Value / this.ControlWidth;
                ////this.Mark.Length = (float)(timePerc * this.ScreenWidth);


                //return (float)(timePerc*this.ScreenWidth);

                throw new NotImplementedException();
            }

            #endregion
        }



        public bool ShowIntervalNumbering
        {
            get { return (bool)GetValue(ShowIntervalNumberingProperty); }
            set { SetValue(ShowIntervalNumberingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowIntervalNumbering.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowIntervalNumberingProperty =
            DependencyProperty.Register("ShowIntervalNumbering", typeof(bool), typeof(ScoredOverlayControl), new PropertyMetadata(new PropertyChangedCallback(intervalCallback)));

        private static void intervalCallback(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ScoredOverlayControl control = obj as ScoredOverlayControl;
            control.Redraw();
        }


        private PatternImplementation pattern;
        public PatternImplementation Pattern 
        {
            get
            {
                return this.pattern;
            }
            set
            {
                if (this.pattern != null)
                {
                    this.pattern.PropertyChanged -= new System.ComponentModel.PropertyChangedEventHandler(pattern_PropertyChanged);
                    this.pattern.AllMarks.CollectionChanged -= new System.Collections.Specialized.NotifyCollectionChangedEventHandler(AllMarks_CollectionChanged);

                }
                this.pattern = value;

                this.menuItems.Clear();
                this.ClearHistory();

                if (this.pattern != null)
                {
                    this.pattern.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(pattern_PropertyChanged);
                    this.pattern.AllMarks.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(AllMarks_CollectionChanged);

                    if (this.pattern.IntervalNames != null)
                    {

                        for (int i = 0; i < this.pattern.IntervalNames.Length; i++)
                        {
                            string region = this.pattern.IntervalNames[i];
                            MenuItem item = new MenuItem() { Header = "Add new " + region + " region", InputGestureText = (i + 1).ToString() };
                            item.Tag = i;
                            item.Click += delegate(object sender, RoutedEventArgs e)
                            {
                                this.addRegion((int)item.Tag, this.clickedPoint);
                            };



                            this.menuItems.Add(item);
                        }
                    }

                }

               
                this.Redraw();
            }
        }

        private ObservableCollection<MenuItem> menuItems = new ObservableCollection<MenuItem>();
        public ObservableCollection<MenuItem> MenuItems { get { return this.menuItems; } }

        private bool isAdding;
        private bool refrain;
        private bool needsToAdd;

        void AllMarks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            bool skip = false;
            lock (lockable)
            {
                skip = isAdding || this.pattern.RefrainFromMarking || refrain;
            }
            if (!skip)
                this.Redraw();
            else needsToAdd = true;
        }

        void pattern_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "RefrainFromMarking")
            {
                if (!this.pattern.RefrainFromMarking && this.needsToAdd)
                {
                    this.Redraw();
                }
            }
            else if (e.PropertyName == "AllMarks")
            {
                this.Redraw();
            }
            else if (e.PropertyName == "BulkChangesInProgress")
            {
                this.refrain = this.pattern.BulkChangesInProgress ;

                if (!this.pattern.BulkChangesInProgress && this.needsToAdd)
                {
                    this.Redraw();
                }

            }
        }


        public float StartTime
        {
            get { return (float)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StartTime.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StartTimeProperty =
            DependencyProperty.Register("StartTime", typeof(float), typeof(ScoredOverlayControl), new UIPropertyMetadata(new PropertyChangedCallback(rebuildCallback)));

        public enum OverlayMode { Edit, Add, Delete, None };



        public OverlayMode Mode
        {
            get { return (OverlayMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Mode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register("Mode", typeof(OverlayMode), typeof(ScoredOverlayControl), new PropertyMetadata(new PropertyChangedCallback(modeChanged)));


        private static void modeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ScoredOverlayControl control = obj as ScoredOverlayControl;
            if ((OverlayMode)e.NewValue == OverlayMode.Edit)
            {
                control.editLine.Visibility = Visibility.Visible;
                control.positionOverlays(Mouse.GetPosition(control.grid));
            }
            else
            {
                control.editLine.Visibility = Visibility.Hidden;
            }

            if ((OverlayMode)e.NewValue == OverlayMode.Delete)
            {
                control.deletePath.Visibility = Visibility.Visible;
                control.positionOverlays(Mouse.GetPosition(control.grid));

            }
            else control.deletePath.Visibility = Visibility.Hidden;

            if ((OverlayMode)e.NewValue == OverlayMode.Add)
            {
                control.addPath.Visibility = Visibility.Visible;
                control.positionOverlays(Mouse.GetPosition(control.grid));
            }
            else control.addPath.Visibility = Visibility.Hidden;
        }





        //private static void addChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    ScoredOverlayControl overlay = obj as ScoredOverlayControl;
        //    if (overlay.CanAdd)
        //    {
        //        ContextMenu menu = overlay.grid.FindResource("context") as ContextMenu;
        //        overlay.grid.ContextMenu = menu;
        //    }
        //    else
        //    {
        //        overlay.grid.ContextMenu = null;                
        //    }
        //}


        //public float EndTime
        //{
        //    get { return (float)GetValue(EndTimeProperty); }
        //    set { SetValue(EndTimeProperty, value); }
        //}

        //// Using a DependencyProperty as the backing store for EndTime.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty EndTimeProperty =
        //    DependencyProperty.Register("EndTime", typeof(float), typeof(ScoredOverlayControl), new UIPropertyMetadata(new PropertyChangedCallback(rebuildCallback)));




        public float TimeWidth
        {
            get { return (float)GetValue(TimeWidthProperty); }
            set { SetValue(TimeWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Width.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TimeWidthProperty =
            DependencyProperty.Register("TimeWidth", typeof(float), typeof(ScoredOverlayControl), new UIPropertyMetadata(new PropertyChangedCallback(rebuildCallback)));

        private static void rebuildCallback(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ScoredOverlayControl win = obj as ScoredOverlayControl;
            win.Redraw();
        }

        public float EndTime 
        {
            get
            {
                return this.StartTime + this.TimeWidth;
            }
        }

        public void Redraw()
        {
            this.grid.BeginInit();
            this.grid.Children.Clear();

            if (this.Pattern == null || this.TimeWidth == 0 || this.StartTime > this.EndTime || this.Pattern.AllMarks.Count == 0)
            {
                this.grid.EndInit();
                return;
            }


            foreach (ColumnDefinition def in this.grid.ColumnDefinitions)
            {
                BindingOperations.ClearAllBindings(def);
            }

            this.grid.ColumnDefinitions.Clear();

            Mark lastMark = null;
            int count = 0;

            int whichMark = 0;

            foreach (Mark mark in Pattern.AllMarks)
            {
                whichMark++;
                if (mark.Time + mark.Length < this.StartTime || mark.Time > this.EndTime) continue;


                if (count == 0)
                {
                    scoredOverlayConverter converter = new scoredOverlayConverter(mark, this.StartTime, this.TimeWidth, this.grid.ActualWidth, this.grid);
                    GridLength length = (GridLength)converter.Convert(mark.Length, typeof(GridLength), true, null);
                    if (length.Value > 0)
                    {
                        ColumnDefinition def2 = new ColumnDefinition() { Width = length };
                        this.grid.ColumnDefinitions.Add(def2);

                        Rectangle empty = new Rectangle();
                        //  rectangle.IsHitTestVisible = false;
                        empty.Fill = new SolidColorBrush(Colors.Transparent);
                        empty.HorizontalAlignment = HorizontalAlignment.Stretch;
                        empty.Margin = new Thickness(0, 0, 0, 0);
                        empty.MouseMove += new MouseEventHandler(rectangle_MouseMove);
                        this.grid.Children.Add(empty);
                        Grid.SetColumn(empty, count);

                        count++;
                    }
                }

               
                ColumnDefinition def = new ColumnDefinition();
                def.Tag = mark;
                Binding binding = new Binding();
                binding.Source = mark;
                binding.Path = new PropertyPath("Length");
                binding.Converter = new scoredOverlayConverter(mark, StartTime, TimeWidth, grid.ActualWidth, this.grid);
                binding.Mode = BindingMode.OneWay;
                binding.ConverterParameter = def;
                def.SetBinding(ColumnDefinition.WidthProperty, binding);
                this.grid.ColumnDefinitions.Add(def);


                Rectangle rectangle = new Rectangle();
              //  rectangle.IsHitTestVisible = false;
                rectangle.Opacity = .2;
                rectangle.HorizontalAlignment = HorizontalAlignment.Stretch;
                rectangle.Fill = new SolidColorBrush(this.Pattern.ColorsToUse[mark.ID]);
                rectangle.Margin = new Thickness(0, 0, 0, 0);
                rectangle.MouseMove += new MouseEventHandler(rectangle_MouseMove);
                this.grid.Children.Add(rectangle);
                Grid.SetColumn(rectangle, count);

                if (lastMark != null)
                {
                    if (lastMark.ID == mark.ID)
                    {
                                    //<Line x:Name="separatorLine" Stretch="Uniform" Panel.ZIndex="96" />
                        Line line = new Line();
                        line.X1 = 0;
                        line.Y1 = 0;
                        line.X2 = 0;
                        line.Y2 = 1;
                        line.Stretch = Stretch.UniformToFill;
                        line.HorizontalAlignment = HorizontalAlignment.Left;
                        line.Stroke = Brushes.Red;
                        line.StrokeThickness = 1;
                        line.IsHitTestVisible = false;
                        Panel.SetZIndex(line, 96);
                        this.grid.Children.Add(line);
                        Grid.SetColumn(line, count);

                    }
                }

                if (this.ShowIntervalNumbering)
                {
                    TextBlock block = new TextBlock() { Text = whichMark.ToString() };
                    block.HorizontalAlignment = HorizontalAlignment.Center;
                    block.VerticalAlignment = VerticalAlignment.Center;
                    this.grid.Children.Add(block);
                    Grid.SetColumn(block, count);
                }


                //GridSplitter splitter = new GridSplitter();
                //splitter.Width = 3;
                //splitter.HorizontalAlignment = HorizontalAlignment.Left;
                //splitter.VerticalAlignment = VerticalAlignment.Stretch;
                //splitter.Tag = mark;
                //splitter.LostMouseCapture += new MouseEventHandler(splitter_LostMouseCapture);
                //splitter.MouseMove += new MouseEventHandler(splitter_MouseMove);
                //this.grid.Children.Add(splitter);
                //Grid.SetColumn(splitter, count);


                count++;
                lastMark = mark;
            }

            //if (this.grid.ColumnDefinitions.Count > 0)
            //{
            //    GridSplitter splitter = new GridSplitter();
            //    splitter.Width = 3;
            //    splitter.HorizontalAlignment = HorizontalAlignment.Right;
            //    splitter.VerticalAlignment = VerticalAlignment.Stretch;
            //    splitter.Tag = lastMark;
            //    splitter.Name = "lastSplitter";
            //    splitter.LostMouseCapture += new MouseEventHandler(splitter_LostMouseCapture);
            //    splitter.MouseMove += new MouseEventHandler(splitter_MouseMove);
            //    this.grid.Children.Add(splitter);
            //    Grid.SetColumn(splitter, count - 1);
            //}

            if (lastMark != null)
            {

                float lperc = (lastMark.Time - this.StartTime) / (this.EndTime - this.StartTime);
                double lx = this.grid.ActualWidth * lperc;
                double lwidthPerc = (lastMark.Length) / (this.EndTime - this.StartTime);
                double lwidth = this.grid.ActualWidth * lwidthPerc;

                if (lx + lwidth < this.grid.ActualWidth)
                {
                    ColumnDefinition finaldef = new ColumnDefinition() { Width = new GridLength(this.grid.ActualWidth - (lx + lwidth), GridUnitType.Star) };
                    this.grid.ColumnDefinitions.Add(finaldef);

                    Rectangle empty = new Rectangle();
                    //  rectangle.IsHitTestVisible = false;
                    empty.Fill = new SolidColorBrush(Colors.Transparent);
                    empty.HorizontalAlignment = HorizontalAlignment.Stretch;
                    empty.Margin = new Thickness(0, 0, 0, 0);
                    empty.MouseMove += new MouseEventHandler(rectangle_MouseMove);
                    this.grid.Children.Add(empty);
                    Grid.SetColumn(empty, count - 1);

                }

            }

            this.grid.Children.Add(this.editLine);
            Grid.SetColumn(this.editLine, 0);
            Grid.SetColumnSpan(this.editLine, 1);
            Grid.SetZIndex(this.editLine, 99);

            this.grid.Children.Add(this.deletePath);
            Grid.SetColumn(this.deletePath, 0);
            Grid.SetZIndex(this.deletePath, 98);

            if (this.pattern.IntervalNames.Length == 1)
            {
                this.deletePath.HorizontalAlignment = HorizontalAlignment.Left;
            }

            this.grid.Children.Add(this.addPath);
            Grid.SetColumn(this.addPath, 0);
            Grid.SetColumnSpan(this.editLine, 2);

            Grid.SetZIndex(this.addPath, 97);

            this.grid.EndInit();
        }

        void rectangle_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.Mode == OverlayMode.Edit)
            {
                Rectangle rect = sender as Rectangle;
            }
        }

        private UIElementAdorner adorner;
        private object lockable = new object();
        //void splitter_MouseMove(object sender, MouseEventArgs e)
        //{
        //    GridSplitter splitter = sender as GridSplitter;

        //    if (e.MouseDevice.Captured != splitter)
        //    {
        //        return;
        //    }

        //    Mark mark = splitter.Tag as Mark;

        //    int index = this.pattern.AllMarks.ToList().IndexOf(mark);

        //    double x = e.GetPosition(this.grid).X;
        //    double perc = x / this.grid.ActualWidth;

        //    float time = this.StartTime + (float)perc * this.TimeWidth;

        //    bool warn = false;
        //    if (index > 0)
        //    {
        //        if (time < this.pattern.AllMarks[index - 1].Time)
        //        {
        //            warn = true;
        //        }
        //    }

        //    if (index < this.pattern.AllMarks.Count - 1)
        //    {
        //        if (time > this.pattern.AllMarks[index + 1].Time)
        //        {
        //            warn = true;
        //        }
        //    }

        //    if (warn)
        //    {
        //        lock (this.lockable)
        //        {
        //            displayMessage("Releasing mouse now will delete region.", TimeSpan.Zero);
        //        }
        //    }
        //    else
        //    {
        //        lock (this.lockable)
        //        {
        //            if (this.adorner != null)
        //            {
        //                AdornerLayer layer = AdornerLayer.GetAdornerLayer(this.grid);
        //                layer.Remove(this.adorner);
        //                this.adorner = null;
        //            }
        //        }
        //    }
        //}

        private void displayMessage(string messageStr, TimeSpan span)
        {
            if (this.adorner == null)
            {
                AdornerLayer layer = AdornerLayer.GetAdornerLayer(this.grid);
                MessageControl message = new MessageControl() { Message = messageStr };
                this.adorner = new UIElementAdorner(this.grid, message);
                layer.Add(this.adorner);
            }

            if (span != TimeSpan.Zero)
            {
                DispatcherTimer timer = new DispatcherTimer();
                timer.Interval = span;
                timer.Tick += delegate(object obj, EventArgs e)
                {
                    lock (this.lockable)
                    {
                        if (this.adorner != null)
                        {
                            AdornerLayer layer = AdornerLayer.GetAdornerLayer(this.grid);
                            layer.Remove(this.adorner);
                            this.adorner = null;
                        }
                    }

                    timer.Stop();
                };

                timer.Start();
            }

        }

        //void splitter_LostMouseCapture(object sender, MouseEventArgs e)
        //{
        //    lock (this.lockable)
        //    {
        //        if (this.adorner != null)
        //        {
        //            AdornerLayer layer = AdornerLayer.GetAdornerLayer(this.grid);
        //            layer.Remove(this.adorner);
        //            this.adorner = null;
        //        }
        //    }

        //    GridSplitter splitter = sender as GridSplitter;

        //    double x = e.GetPosition(this.grid).X;
        //    double perc = x / this.grid.ActualWidth;

        //    float time = this.StartTime + (float)perc * this.TimeWidth;
        //    Mark mark = splitter.Tag as Mark;
        //    if (splitter.Name != "lastSplitter")
        //        mark.Time = time;
        //    else
        //    {
        //        mark.Length = time - mark.Time;
        //    }
        //}

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void Control_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            Keyboard.Focus(this.grid);
        }

        private void grid_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Z)
            {
               

                bool ok = this.Undo();
                if (!ok)
                {
                    this.displayMessage("No more actions to undo.", TimeSpan.FromMilliseconds(1500));
                }
                else this.Redraw();

                return;
            }
            else if (e.Key == Key.X)
            {
               bool ok = this.Redo();
               if (!ok)
               {
                   this.displayMessage("No more actions to redo.", TimeSpan.FromMilliseconds(1500));
               }
               else this.Redraw();
               return;
            }

            int which = 0;

            if (e.Key.ToString().Length < 2) return;

            if (!int.TryParse(e.Key.ToString()[1].ToString(), out which))
                return;

            which--;

            if (which < 0 || which >= this.pattern.IntervalNames.Length)
            {
                this.displayMessage("Invalid pattern shortcut. Shortcuts are available for 1-" + (this.pattern.IntervalNames.Length).ToString() + " or right-click to bring up context menu",
                    TimeSpan.FromMilliseconds(1500));
                return;
            }

            Point point = Mouse.GetPosition(this.grid);
            if (point.X < 0 || point.X > this.grid.ActualWidth) return;

            this.addRegion(which, point);
        }
 
        private void addRegion(int whichToAdd, Point point)
        {
            if (this.Mode != OverlayMode.Add) return;

            if (whichToAdd < 0 || whichToAdd >= this.pattern.IntervalNames.Length)
            {
                return;
            }

            HitTestResult result = VisualTreeHelper.HitTest(this.grid, point);
            if (result == null) return;

             int column = Grid.GetColumn(result.VisualHit as UIElement);

             lock (lockable)
             {
                 this.isAdding = true;
             }

             if (column >= 0 && column < this.grid.ColumnDefinitions.Count)
             {
                 ColumnDefinition def = this.grid.ColumnDefinitions[column];

                 double x = point.X;
                 double perc = x / this.grid.ActualWidth;

                 float time = this.StartTime + (float)perc * this.TimeWidth;
                 Mark mark = def.Tag as Mark;

                 int index = this.pattern.AllMarks.IndexOf(mark);
                 if (index < 0)
                 {
                     return;
                 }

                 if (whichToAdd == mark.ID && this.pattern.IntervalNames.Length != 1)
                 {
                     displayMessage("Cannot add the same type of region as the one you are adding to. Select a different region type.", TimeSpan.FromMilliseconds(1500));
                     return;
                 }

                 Mark newMark = new Mark(mark.Implementation, time);
                 newMark.ID = whichToAdd;
                 

                 float markLength = mark.Length * .2F;

                 //if (this.pattern.IntervalNames.Length == 1)
                 //{
                 //    //If it's only one then set the boundary to the next
                 //    markLength = mark.Time + mark.Length - newMark.Time;
                 //}

                 if(newMark.Time + markLength > mark.Time + mark.Length || this.pattern.IntervalNames.Length == 1)
                 {
                     //Don't break into two because it's overflowing into next one
                     newMark.Time = time;

                     if (index + 2 < this.pattern.AllMarks.Count)
                     {
                         if (newMark.ID == this.pattern.AllMarks[index + 1].ID && this.pattern.IntervalNames.Length != 1)
                         {
                             //Instead of adding a new thing and merging, just make the next one larger
                             this.pattern.AllMarks[index + 1].Time = time;
                             this.pattern.AllMarks[index].Length = this.pattern.AllMarks[index + 1].Time - this.pattern.AllMarks[index].Time;
                         }
                         else
                         {
                             this.pattern.AllMarks.Insert(index + 1, newMark);

                             newMark.Length = this.pattern.AllMarks[index + 2].Time - newMark.Time;
                             mark.Length = newMark.Time - mark.Time;

                         }

                     }
                     else newMark.Length = this.EndTime - newMark.Time;
                 }
                 else
                 {
                     this.pattern.AllMarks.Insert(index + 1, newMark);

                     //Break into two
                     Mark newNewMark = new Mark(mark.Implementation, newMark.Time + markLength);
                     newNewMark.ID = mark.ID;

                     this.pattern.AllMarks.Insert(index + 2, newNewMark);

                     newMark.Length = markLength;

                     mark.Length = newMark.Time - mark.Time;


                     if (index + 3 < this.pattern.AllMarks.Count)
                     {
                         newNewMark.Length = this.pattern.AllMarks[index + 3].Time - newNewMark.Time;
                     }
                     else newNewMark.Length = this.EndTime - newNewMark.Time;


                 }

                 lock (lockable)
                 {
                     isAdding = false;
                 }

                 this.Redraw();
             }
        }

        private Point clickedPoint;

        private void grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.clickedPoint = e.MouseDevice.GetPosition(this.grid);

            Rectangle whichRect = null;
            foreach (UIElement obj in this.grid.Children)
            {
                if (!(obj is Rectangle))
                {
                    continue;
                }

                HitTestResult p = VisualTreeHelper.HitTest(obj, e.MouseDevice.GetPosition(obj));
                
                if (p != null)
                {
                    whichRect = obj as Rectangle;
                    break;
                }
            }

            //HitTestResult result = VisualTreeHelper.HitTest(this.grid, this.clickedPoint);
            if (whichRect == null) return;

            int column = Grid.GetColumn(whichRect);
            
            
            if (this.Mode == OverlayMode.Edit)
            {
                

                double perc = this.clickedPoint.X / this.grid.ActualWidth;
                float time = this.StartTime + (float)perc * this.TimeWidth;

                if (column >= 0 && column < this.grid.ColumnDefinitions.Count)
                {
                    bool isFirst = false;
                    bool isLast = false;

                     Mark mark = this.grid.ColumnDefinitions[column].Tag as Mark;
                    if (mark == null)
                    {
                        if (column == 0)
                        {
                            mark = this.grid.ColumnDefinitions[column + 1].Tag as Mark;
                            isFirst = true;
                        }
                        else
                        {
                            mark = this.grid.ColumnDefinitions[column - 1].Tag as Mark;
                            isLast = true;
                        }

                        if (mark == null) return;

                    }
                    float origTime = mark.Time;
                    float origLength = mark.Length;

                     
                        

                    if (e.ChangedButton == MouseButton.Left && !isLast)
                    {
                        if (time >= origTime + origLength)
                        {
                            this.displayMessage("This action would delete the interval and is not allowed. Please delete the interval manually or try again.", 
                                TimeSpan.FromMilliseconds(3000));
                            return;
                        }
                        else
                        {

                            mark.Time = time;
                        }
                    }
                    else if (e.ChangedButton == MouseButton.Right && !isFirst)
                    {
                        if (time - mark.Time <= 0)
                        {
                            this.displayMessage("This action would delete the interval and is not allowed. Please delete the interval manually or try again.",
                               TimeSpan.FromMilliseconds(3000));
                            return;
                        }
                        else
                        {
                            mark.Length = time - mark.Time;
                        }

                    }

                    this.AddEditAction(mark, origTime, origLength);


                    this.Redraw();
                }
            }
            else if (this.Mode == OverlayMode.Delete && e.ChangedButton == MouseButton.Left)
            {

                if (column >= 0 && column < this.grid.ColumnDefinitions.Count)
                {
                    Mark mark = this.grid.ColumnDefinitions[column].Tag as Mark;
                    if (mark != null)
                    {
                        //This will also delete
                        mark.Length = 0;
                        this.AddAddDelAction(OverlayMode.Delete, mark, column);
                        this.Redraw();
                    }
                }

            }
            else if (this.Mode == OverlayMode.Add && e.ChangedButton == MouseButton.Left)
            {
                //Maybe add add functionality later
            }
            
        }

        private void positionOverlays(Point p)
        {
            if (this.Mode == OverlayMode.Edit)
            {
                HitTestResult result = VisualTreeHelper.HitTest(this.grid, p);
                if (result != null)
                {
                    int gridCol = Grid.GetColumn(result.VisualHit as UIElement);
                    if (gridCol != Grid.GetColumn(this.editLine))
                    {
                        Grid.SetColumn(this.editLine, gridCol);
                    }


                    try
                    {
                        Rectangle rect = (from UIElement ele in this.grid.Children
                                          where Grid.GetColumn(ele) == gridCol && ele is Rectangle
                                          select ele).SingleOrDefault() as Rectangle;

                        if (rect != null)
                        {


                            this.editLine.Margin = new Thickness(this.grid.TranslatePoint(p, rect).X, 0, 0, 0);

                        }
                    }
                    catch
                    {

                    }
                }
            }
            else if (this.Mode == OverlayMode.Delete)
            {
                HitTestResult result = VisualTreeHelper.HitTest(this.grid, p);
                if (result != null)
                {
                    int gridCol = Grid.GetColumn(result.VisualHit as UIElement);
                    if (gridCol != Grid.GetColumn(this.deletePath))
                    {
                        Grid.SetColumn(this.deletePath, gridCol);
                        this.deletePath.InvalidateVisual();
                    }
                }
            }
            else if (this.Mode == OverlayMode.Add)
            {
                HitTestResult result = VisualTreeHelper.HitTest(this.grid, p);
                if (result != null)
                {
                    int gridCol = Grid.GetColumn(result.VisualHit as UIElement);
                    if (gridCol != Grid.GetColumn(this.addPath))
                    {
                        Grid.SetColumn(this.addPath, gridCol);
                    }

                    try
                    {

                        Rectangle rect = (from UIElement ele in this.grid.Children
                                          where Grid.GetColumn(ele) == gridCol && ele is Rectangle
                                          select ele).SingleOrDefault() as Rectangle;

                        if (rect != null)
                        {


                            this.addPath.Margin = new Thickness(this.grid.TranslatePoint(p, rect).X - this.addPath.ActualWidth / 2.0, 0, 0, 0);

                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void grid_MouseMove(object sender, MouseEventArgs e)
        {
            this.positionOverlays(e.MouseDevice.GetPosition(this.grid));
        }

        
    }
}
