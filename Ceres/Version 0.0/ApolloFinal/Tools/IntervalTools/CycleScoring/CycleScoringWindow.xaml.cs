﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CeresBase.Projects;
using System.Reflection;
using MesosoftCommon.DataStructures;
using MesosoftCommon.Utilities.Validations;
using ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations.ExponentialMA;
using MesosoftCommon.Controls;
using System.Threading;
using System.Windows.Threading;
using MesosoftCommon.Adorners;
using System.Collections.ObjectModel;
using ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations;
using ApolloFinal.Tools.IntervalTools.CycleScoring.Central;
using CeresBase.UI;
using Deepforest.WPF.Controls;
using System.ComponentModel;
using CeresBase.FlowControl;
using CeresBase.FlowControl.Adapters.RealTimeAdapters;


namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    /// <summary>
    /// Interaction logic for CycleScoringWindow.xaml
    /// </summary>
    public partial class CycleScoringWindow : Window, INotifyPropertyChanged
    {
        #region Routine stuff

        string selectedRoutine;
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            if (box.SelectedItem != null)
                this.selectedRoutine = box.SelectedItem.ToString();
            else this.selectedRoutine = null;
            if(routineApplied)
                this.changeRoutineApplication();
        }

        public ObservableCollection<RoutinePackage> RoutineList
        {
            get
            {
                if (!RoutineCentral.Routines.ContainsKey(this.batch.Controller.GetType().ToString()))
                {
                    RoutineCentral.Routines.Add(this.batch.Controller.GetType().ToString(), new ObservableCollection<RoutinePackage>());
                   // RoutineCentral.Routines["cycleScoring"].CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(CycleScoringWindow_CollectionChanged);
                }
                return RoutineCentral.Routines[this.batch.Controller.GetType().ToString()];
            }
        }

        //void CycleScoringWindow_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    OnPropertyChanged("RoutineList");        
        //}

        private void OnPropertyChanged(string p)
        {
            PropertyChangedEventHandler changed = this.PropertyChanged;
            if (changed != null)
            {
                changed(this, new PropertyChangedEventArgs(p));
            }
        }

        private bool routineApplied = false;
        Button routineButton;
        private void routineApply(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            if (routineButton == null)
                routineButton = b;
            changeRoutineApplication();
        }

       

        private void changeRoutineApplication()
        {
            if (routineApplied)
            {
                this.redoScoringAndDrawing(null, 0);
                this.routineApplied = false;
                this.routineButton.Content = "Apply";
            }
            else
            {
                if (this.selectedRoutine == null) return;
                this.batch.OutputList.Clear();
                this.batch.Run(this.selectedRoutine, new object[] { this.curEpoch });
                this.routineApplied = true;
                if(this.routineButton != null)
                    this.routineButton.Content = "Remove";

            }
        }

        void batch_RunCompleted(object sender, EventArgs e)
        {
            RoutineBatchOutputPoint point = this.batch.OutputList.FirstOrDefault(o => o.SourceProcessName.Contains("Real Time Output"));
            if (point == null) return;
            RealTimeDataAdapter.RealTimeDataAdapterOutput output = point.OutputData as RealTimeDataAdapter.RealTimeDataAdapterOutput;
            this.redoScoringAndDrawing(output.Data, output.Resolution);
        }

        private void routineAddCallback(object sender, RoutedEventArgs e)
        {
            this.batch.MakeNewRoutine();
        }

        private void editRoutineCallback(object sender, RoutedEventArgs e)
        {
            if (this.selectedRoutine == null)
            {
                MessageBox.Show("Select a routine to edit");
                return;
            }

            Routine r = RoutineCentral.GetRoutine(this.selectedRoutine, this.batch.Controller.GetType().ToString());
            RoutineManager rm = RoutineCentral.GetRoutineManager(this.selectedRoutine, r,
                this.batch.Controller.GetType().ToString());
            this.batch.RoutineEdit(r, rm);
        }

        private void routineManage(object sender, RoutedEventArgs e)
        {
            RoutineManagementControl rmc = new RoutineManagementControl();
            rmc.ControllerString = this.batch.Controller.GetType().ToString();

            Window win = new Window();
            win.Title = "Routine Management For Scoring Routines";
            win.Content = rmc;
            win.Height = rmc.Height + 32;
            win.Width = rmc.Width + 8;
            win.Show();
        }

        #endregion

        private class sectionEdit
        {
            public string SectionName { get; set; }
            public string SectionType { get; set; }
            public int SectionNumber { get; set; }
        }

        public CycleScoringWindow()
        {
            InitializeComponent();

            this.batch.RunCompleted += new EventHandler(batch_RunCompleted);
            this.batch.Controller = new CycleScoringController(this.batch);
            BindingExpression ex = this.batch.GetBindingExpression(FrameworkElement.DataContextProperty);
            ex.UpdateTarget();

            this.Loaded += new RoutedEventHandler(CycleScoringWindow_Loaded);
            this.Closed += new EventHandler(CycleScoringWindow_Closed);

            this.possibleEpochs.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(possibleEpochs_CollectionChanged);

        }



        void possibleEpochs_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.setPrevNext();
        }
        
        void CycleScoringWindow_Closed(object sender, EventArgs e)
        {
       
        }

        void CycleScoringWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.buildDisplay();
            if (this.overlay != null)
            {
                this.overlay.Redraw();
            }


            this.setList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding() { Source = this.sectionSource });
        }

        void CycleScoringWindow_Initialized(object sender, EventArgs e)
        {
           
        }



        private bool rebuildOn = true;
        private bool somethingInQueue = false;

        public void TurnOffRebuild()
        {
            this.rebuildOn = false;
        }

        public void TurnOnRebuild()
        {
            this.rebuildOn = true;
            if (this.somethingInQueue)
            {
                this.buildDisplay();
                this.somethingInQueue = false;
            }
        }



        public float ScreenWidth
        {
            get { return (float)GetValue(ScreenWidthProperty); }
            set { SetValue(ScreenWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScreenWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScreenWidthProperty =
            DependencyProperty.Register("ScreenWidth", typeof(float), typeof(CycleScoringWindow), new UIPropertyMetadata(new PropertyChangedCallback(widthChanged)));

        private static void widthChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CycleScoringWindow win = obj as CycleScoringWindow;
            win.TimeDisplay = win.StartTime.ToString("F2") + "-" + (win.StartTime + win.ScreenWidth).ToString("F2");
            win.buildDisplay();
        }

        public string TimeDisplay
        {
            get { return (string)GetValue(TimeDisplayProperty); }
            set { SetValue(TimeDisplayProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TimeDisplay.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TimeDisplayProperty =
            DependencyProperty.Register("TimeDisplay", typeof(string), typeof(CycleScoringWindow));




        public float StartTime
        {
            get { return (float)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StartTime.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StartTimeProperty =
            DependencyProperty.Register("StartTime", typeof(float), typeof(CycleScoringWindow), new UIPropertyMetadata(new PropertyChangedCallback(rebuildCallback)));



        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Description.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(CycleScoringWindow));

        private static void rebuildCallback(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CycleScoringWindow win = obj as CycleScoringWindow;
            win.TimeDisplay = win.StartTime.ToString("F2") + "-" + (win.StartTime + win.ScreenWidth).ToString("F2");
            win.buildDisplay();
        }

        private void buildDisplay()
        {

            if (this.curEpoch == null)
            {
                this.rawTrace.Points.Clear();
                return;
            }

            if (!this.rebuildOn || this.rawTrace.ActualWidth == 0 || this.rawTrace.ActualHeight == 0 )
            {
                this.somethingInQueue = true;
                return;
            }

            int startIndex = (int)( (this.StartTime - this.curEpoch.BeginTime) / this.resolution);
            int length = (int)((this.ScreenWidth) / this.resolution);

            if (length > this.allData.Length)
            {
                length = this.allData.Length;
            }

            if (length + startIndex > this.allData.Length)
            {
                length = this.allData.Length - startIndex;
            }

            //float[] tdata = SpikeVariable.ReadData(this.curEpoch.Variable as SpikeVariable, this.StartTime, this.StartTime + this.ScreenWidth);
            float[] data = new float[length];
            Array.Copy(this.allData, startIndex, data, 0, length);
            if (data.Length == 0) return;

            double perPt = this.rawTrace.ActualWidth / data.Length;
            float min = allData.Min();
            float max = allData.Max();


            PointCollection points = new PointCollection();

            //paths.Add(new LineSegment(new Point(0, 0), true));

            points.Add(new Point(0, 0));

            int numPoints = (int)this.rawTrace.ActualWidth * 10;
            if (numPoints < 10000)
                numPoints = 10000;
            if (data.Length < numPoints)
                numPoints = data.Length;
            int stride = 1;
            if (data.Length > numPoints)
            {
                stride = data.Length / numPoints;
            }
           


            for (int i = 0; i < data.Length; i+=stride)
            {
                 points.Add(new Point((double)i * perPt, this.rawTrace.ActualHeight * (max - data[i]) / (max - min)));
            }

            points.Add(new Point((double)(data.Length) * perPt, this.rawTrace.ActualHeight));

            this.rawTrace.Points = null;
            data = null;
            //this.allData = null;
            GC.WaitForFullGCComplete(1000);

            this.rawTrace.Points = points;

        }

        private float[] allData;
        private double resolution;

        private Epoch nextEpoch;
        private Epoch prevEpoch;



        public bool HasPrevEpoch
        {
            get { return (bool)GetValue(HasPrevEpochProperty); }
            set { SetValue(HasPrevEpochProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasPrevEpoch.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasPrevEpochProperty =
            DependencyProperty.Register("HasPrevEpoch", typeof(bool), typeof(CycleScoringWindow));



        public bool HasNextEpoch
        {
            get { return (bool)GetValue(HasNextEpochProperty); }
            set { SetValue(HasNextEpochProperty, value); }
        }



        public bool ShowScoredEpochs
        {
            get { return (bool)GetValue(ShowScoredEpochsProperty); }
            set { SetValue(ShowScoredEpochsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowOnlyUnscoredEpochs.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowScoredEpochsProperty =
            DependencyProperty.Register("ShowScoredEpochs", typeof(bool), typeof(CycleScoringWindow), new PropertyMetadata(false, new PropertyChangedCallback(showScoredChanged)));



        public string ShowingAllText
        {
            get { return (string)GetValue(ShowingAllTextProperty); }
            set { SetValue(ShowingAllTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowingAllText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowingAllTextProperty =
            DependencyProperty.Register("ShowingAllText", typeof(string), typeof(CycleScoringWindow), new PropertyMetadata("Unscored Epochs Only (press S to switch)"));



        private static void showScoredChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((obj as CycleScoringWindow)).setPrevNext();

            if ((bool)e.NewValue)
            {
                (obj as CycleScoringWindow).ShowingAllText = "All Epochs (press S to switch)";
                
            }
            else
            {
                (obj as CycleScoringWindow).ShowingAllText = "Unscored Epochs Only (press S to switch)";
            }

        }



        // Using a DependencyProperty as the backing store for HasNextEpoch.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasNextEpochProperty =
            DependencyProperty.Register("HasNextEpoch", typeof(bool), typeof(CycleScoringWindow));


        private ObservableCollection<Epoch> possibleEpochs = new ObservableCollection<Epoch>();
        public ObservableCollection<Epoch> PossibleEpochs
        {
            get
            {
                return this.possibleEpochs;
            }
        }

        private void setPrevNext()
        {
            //if (this.curEpoch == null) return;

            IOrderedEnumerable<Epoch> sortedFiltered = null;

            if (this.possibleEpochs.Count == 0)
            {
                BeginInvokeObservableCollection<Epoch> epochList = EpochCentral.BindToManager(this.Dispatcher);

                if (!this.ShowScoredEpochs)
                {
                    sortedFiltered = from Epoch e in epochList
                                     where  (e.Variable as SpikeVariable).SpikeType != SpikeVariableHeader.SpikeType.Pulse  && (e.EndTime - e.BeginTime) > 0 && !IntervalScoringCentral.HasBeenScored(e) //e.Variable == this.curEpoch.Variable &&
                                     orderby e.Variable.FullVarName ascending
                                     select e;
                }
                else
                {
                    sortedFiltered = from Epoch e in epochList
                                     where (e.Variable as SpikeVariable).SpikeType != SpikeVariableHeader.SpikeType.Pulse && (e.EndTime - e.BeginTime) > 0 //e.Variable == this.curEpoch.Variable &&
                                     orderby e.Variable.FullVarName ascending
                                     select e;
                }
            }
            else
            {
                sortedFiltered = from Epoch e in this.possibleEpochs
                                 where (e.Variable as SpikeVariable).SpikeType != SpikeVariableHeader.SpikeType.Pulse && (e.EndTime - e.BeginTime) > 0 //e.Variable == this.curEpoch.Variable &&
                                 orderby e.Variable.FullVarName ascending
                                 select e;
            }

            sortedFiltered = sortedFiltered.ThenBy(e => e.BeginTime);

            if (!sortedFiltered.Contains(this.curEpoch))
            {
                this.CurrentEpoch = sortedFiltered.FirstOrDefault();
            }

           

            List<Epoch> epochs = sortedFiltered.ToList();
            int index = epochs.IndexOf(this.curEpoch);
            if (index > 0)
            {
                this.prevEpoch = epochs[index - 1];
                this.HasPrevEpoch = true;
            }
            else
            {
                this.HasPrevEpoch = false;
            }

            if (index <= epochs.Count - 2 && index != -1)
            {
                this.nextEpoch = epochs[index + 1];
                this.HasNextEpoch = true;
            }
            else
            {
                this.HasNextEpoch = false;
            }
        }

        private Epoch curEpoch;
        public Epoch CurrentEpoch 
        {
            get
            {                
                return this.curEpoch;
            }
            set
            {
                this.curEpoch = value;
                if (IntervalScoringCentral.HasBeenScored(this.curEpoch))
                {
                    this.overlay.Pattern = null;
                    this.Behavior = IntervalScoringCentral.LoadBehaviorFromEpoch(this.curEpoch);
                }
                else if(this.Behavior == null)
                {
                    this.Behavior = IntervalScoringCentral.Behaviors.FirstOrDefault(b => b.Name != ScoringBehavior.DefaultBehaviorName);
                    if (this.Behavior == null)
                    {
                        this.Behavior = IntervalScoringCentral.Behaviors.FirstOrDefault(b => b.Name == ScoringBehavior.DefaultBehaviorName);
                        if (this.Behavior == null)
                            throw new InvalidOperationException("The default behavior is not loading for some reason.");
                    }
                }

                redoScoringAndDrawing(null, 0);
                if (this.routineApplied)
                {
                    if (this.routineButton != null)
                        this.routineButton.Content = "Apply";
                    this.routineApplied = false;
                }
            }
        }

        private void redoScoringAndDrawing(float[] data, double res)
        {
            if (this.curEpoch != null && this.Behavior != null)
            {
                this.Behavior.Epoch = this.curEpoch;
                this.TurnOffRebuild();
                this.StartTime = this.curEpoch.BeginTime;

                if (this.ScreenWidth == 0)
                {
                    this.ScreenWidth = this.curEpoch.EndTime - this.StartTime;
                }

                if (this.StartTime + this.ScreenWidth > this.curEpoch.EndTime)
                {
                    this.ScreenWidth = this.curEpoch.EndTime - this.StartTime;
                }

                this.Description = this.curEpoch.ToString();

                if (data == null)
                {
                    this.allData = (this.curEpoch.Variable as SpikeVariable).GetTimeSeriesData(this.StartTime, this.curEpoch.EndTime); //SpikeVariable.ReadData(this.curEpoch.Variable as SpikeVariable, this.StartTime, this.curEpoch.EndTime);
                    this.resolution = (this.curEpoch.Variable as SpikeVariable).Resolution;
                }
                else
                {
                    this.allData = data;
                    this.resolution = res;
                }

                GC.Collect();

                if (!IntervalScoringCentral.HasBeenScored(this.curEpoch))
                {
                    if (data == null)
                    {
                        if (this.allData != null && this.allData.Length < 100000)
                        {
                            runScoring(null);
                        }
                        else this.overlay.Pattern = null;
                    }
                    else if (data.Length < 100000)
                    {
                        runScoring(data);
                    }
                    else this.overlay.Pattern = null;
                }
                else this.overlay.Pattern = this.Behavior.Pattern;

                this.TurnOnRebuild();
                this.buildDisplay();
                this.setPrevNext();
                this.curMode = gridMode.Scroll;
            }
            else
            {
                this.overlay.Pattern = null;
                this.buildDisplay();
            }
        }

        private void runScoring(float[] data)
        {
            if (this.Behavior == null) return;

            this.overlay.Pattern = null;
            
            this.TurnOffRebuild();

            //this.rawTrace

            if(data == null)
                this.Behavior.Score(this.propertyGrid.Instance as CycleScoring.ScoringSettings);
            else 
                this.Behavior.Score(data, this.resolution, this.propertyGrid.Instance as CycleScoring.ScoringSettings);
                        
            this.TurnOnRebuild();
            this.buildDisplay();

            this.overlay.Pattern = this.Behavior.Pattern;

        }


        public ObservableCollection<ScoringBehavior> Behaviors 
        { 
            get 
            {
                return IntervalScoringCentral.Behaviors;
            } 
        }


        public ObservableCollection<PatternImplementation> ScoringPatterns 
        { 
            get 
            {
                return IntervalScoringCentral.Implementations;
            } 
        }


        public ObservableCollection<IScoringAlgorithm> Algorithms 
        { 
            get 
            {
                return IntervalScoringCentral.Algorithms;
            } 
        }


        public ScoringBehavior Behavior
        {
            get { return (ScoringBehavior)GetValue(BehaviorProperty); }
            set { SetValue(BehaviorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Behavior.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BehaviorProperty =
            DependencyProperty.Register("Behavior", typeof(ScoringBehavior), typeof(CycleScoringWindow), new PropertyMetadata(new PropertyChangedCallback(behaviorChanged)));

        private static void behaviorChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as CycleScoringWindow).Behavior.Epoch = (obj as CycleScoringWindow).curEpoch;
        }


        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.buildDisplay();
        }

        private void slider1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement stack = this.screenWidthContent.TryFindResource("stackWidth") as FrameworkElement;
            if (stack != null)
            {
              
                this.screenWidthContent.Content = stack;

           
            }
        }

     

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement stack = this.screenWidthContent.TryFindResource("stackWidth") as FrameworkElement;
            if (stack != null)
            {
                this.screenWidthContent.Content = stack;

                StackPanel panel = stack as StackPanel;
                TextBox box = null;
                foreach (FrameworkElement ele in panel.Children)
                {
                    if (ele is TextBox)
                    {
                        box = ele as TextBox;
                        break;
                    }
                }

                validateTextBox(box);

            }

        }

        private void validateTextBox(TextBox box)
        {
            if (box == null) return;
            BindingExpression be = BindingOperations.GetBindingExpression(box, TextBox.TextProperty);
            if (be == null) return;
            MaxMinValidationRule validRule = be.ParentBinding.ValidationRules.FirstOrDefault(vr => vr is MaxMinValidationRule) as MaxMinValidationRule;
            if (validRule == null)
            {
                validRule = new MaxMinValidationRule();
                be.ParentBinding.ValidationRules.Add(validRule);
            }
            
            if (this.curEpoch != null && validRule != null)
            {
                validRule.Min = 0.01;
                validRule.Max = this.curEpoch.EndTime - this.curEpoch.BeginTime;
            }
            

            be.UpdateSource();
        }

        private void changeToSlider(object sender, RoutedEventArgs e)
        {
            FrameworkElement slider = this.screenWidthContent.TryFindResource("sliderWidth") as FrameworkElement;
            if (slider != null)
            {
                this.screenWidthContent.Content = slider;

            }
        }

        private void slider1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.curEpoch == null) return;

            Slider slide = sender as Slider;
            double perc = (slide.Maximum  - e.NewValue) / (slide.Maximum - slide.Minimum);

            double amount = (this.curEpoch.EndTime - this.curEpoch.BeginTime) * (perc*perc);
            this.ScreenWidth = (float)amount;

            if (this.StartTime + this.ScreenWidth > this.curEpoch.EndTime)
            {
                this.StartTime = this.curEpoch.EndTime - this.ScreenWidth;
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                validateTextBox(sender as TextBox);
                
            }
        }

       //private bool isScrolling = false;
        private Point firstPoint;
        private float firstTime;


        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.curMode == gridMode.Scroll)
            {
                if (this.curState == gridState.LeftFirst)
                {
                    Point nextPoint = e.MouseDevice.GetPosition(e.MouseDevice.Captured);

                    double dist = nextPoint.X - this.firstPoint.X;
                    //if (Math.Abs(dist) > 10.0)
                   // {
                        this.curState = gridState.Scrolling;
                    //}
                }

                if (this.curState == gridState.Scrolling)
                {
                    Point nextPoint = e.MouseDevice.GetPosition(e.MouseDevice.Captured);
                    double perc = (nextPoint.X - this.firstPoint.X) / (sender as FrameworkElement).ActualWidth;

                    float time = (float)perc * (ScreenWidth);

                    float newStart = firstTime + time;
                    if (newStart < this.curEpoch.BeginTime)
                    {
                        newStart = this.curEpoch.BeginTime;
                        // this.showMessage("At beginning of epoch");
                    }

                    if (newStart + this.ScreenWidth > this.curEpoch.EndTime)
                    {
                        newStart = this.curEpoch.EndTime - this.ScreenWidth;
                        //this.showMessage("At end of epoch");
                    }

                    this.TurnOffRebuild();
                    this.StartTime = newStart;
                    //this.EndTime = newStart + this.ScreenWidth;
                    this.TurnOnRebuild();
                    this.hasMoved = true;
                }
            }
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.curMode == gridMode.Scroll)
            {

                if (this.curState == gridState.Scrolling)
                {
                    this.curState = gridState.None;

                    if (e.MouseDevice.Captured != null)
                        e.MouseDevice.Captured.ReleaseMouseCapture();

                    if (this.hasMoved)
                    {
                        e.Handled = true;
                        this.hasMoved = false;
                    }
                }
                else if (this.curState == gridState.LeftFirst && e.LeftButton == MouseButtonState.Released)
                {
                    this.curState = gridState.None;
                    if (e.MouseDevice.Captured != null)
                        e.MouseDevice.Captured.ReleaseMouseCapture();

                    if (this.hasMoved)
                    {
                        e.Handled = true;
                        this.hasMoved = false;
                    }

                }
                else if (this.curState == gridState.RightFirst && e.RightButton == MouseButtonState.Released)
                {
                    this.curState = gridState.None;
                    if (e.MouseDevice.Captured != null)
                        e.MouseDevice.Captured.ReleaseMouseCapture();

                    if (this.hasMoved)
                    {
                        e.Handled = true;
                        this.hasMoved = false;
                    }
                }
            }

        }

        private enum gridMode { Edit, Add, Scroll, Delete };

        private gridMode gMode;
        private gridMode curMode 
        { 
            get 
            { 
                return this.gMode; 
            }
            set
            {
                this.gMode = value;

                if (this.gMode == gridMode.Add)
                {
                    this.overlay.Mode = ScoredOverlayControl.OverlayMode.Add;
                    Keyboard.Focus(this.overlay);
                }
                else if (this.gMode == gridMode.Edit)
                {
                    this.overlay.Mode = ScoredOverlayControl.OverlayMode.Edit;
                    Keyboard.Focus(this.overlay);
                }
                else if (this.gMode == gridMode.Scroll)
                {
                    this.overlay.Mode = ScoredOverlayControl.OverlayMode.None;
                }
                else if (this.gMode == gridMode.Delete)
                {
                    this.overlay.Mode = ScoredOverlayControl.OverlayMode.Delete;
                    Keyboard.Focus(this.overlay);
                }
            }
        }
      


       


        private enum gridState { None, Scrolling, RightFirst, LeftFirst };
        private gridState curState;
        private bool hasMoved = false;

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.curMode == gridMode.Scroll)
            {

                if (e.LeftButton == MouseButtonState.Pressed && e.RightButton == MouseButtonState.Pressed)
                {
                        float amountToMove = this.ScreenWidth/2.0F;

                    if (this.curState == gridState.LeftFirst)
                    {
                        double diff = Math.Round(this.curEpoch.EndTime - (this.StartTime + amountToMove * 3), 3);
                        double singleDiff = Math.Round(this.curEpoch.EndTime - (this.StartTime + this.ScreenWidth), 3);

                        if (singleDiff == 0)
                        {
                            showMessage("At end of epoch", TimeSpan.FromMilliseconds(1000));

                        }
                        else if (diff >= 0)
                        {
                            this.StartTime += amountToMove;
                        }
                        else if (diff < 0)
                        {
                            this.StartTime = this.curEpoch.EndTime - this.ScreenWidth;
                        }
                    }
                    else
                    {
                        if (this.StartTime == this.curEpoch.BeginTime)
                        {
                            showMessage("At beginning of epoch", TimeSpan.FromMilliseconds(1000));

                        }
                        else if (this.StartTime - amountToMove >= this.curEpoch.BeginTime)
                        {
                            this.StartTime -= amountToMove;
                        }
                        else if (this.StartTime - amountToMove < this.curEpoch.BeginTime)
                        {
                            this.StartTime = this.curEpoch.BeginTime;
                        }
                    }

                    this.hasMoved = true;
                }
                else if (e.LeftButton == MouseButtonState.Pressed && this.curState == gridState.None)
                {
                    FrameworkElement ele = sender as FrameworkElement;
                    e.MouseDevice.Capture(ele, CaptureMode.Element);
                    firstPoint = e.MouseDevice.GetPosition(ele);
                    firstTime = this.StartTime;

                    this.curState = gridState.LeftFirst;
                }
                else if (e.RightButton == MouseButtonState.Pressed && this.curState == gridState.None)
                {
                    this.curState = gridState.RightFirst;
                }
            }
        }

        private object lockable = new object();
        private UIElementAdorner adorner;

        private void showMessage(string messageStr, TimeSpan span)
        {
            this.showMessage(messageStr, span, 0.5);   
        }

        private void showMessage(string messageStr, TimeSpan span, double opacity)
        {

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(this.rawTrace);
       
            MessageControl message = new MessageControl() { Message = messageStr };
            message.Width = layer.ActualWidth;
            message.Height = layer.ActualHeight;
            message.IsHitTestVisible = false;
            message.Opacity = opacity;

            lock (this.lockable)
            {
                if (this.adorner != null)
                {
                    //layer.Remove(this.adorner);
                    return;
                }
                this.adorner = new MesosoftCommon.Adorners.UIElementAdorner(this.rawTrace, message);
            }
            layer.Add(adorner);

            if (span != TimeSpan.Zero)
            {
                DispatcherTimer timer = new DispatcherTimer();
                timer.Interval = span;
                timer.Tick += delegate(object tm, EventArgs ev)
                {
                    DispatcherTimer oops = tm as DispatcherTimer;
                    oops.Stop();
                    lock (this.lockable)
                    {
                        if (this.adorner != null)
                        {
                            layer.Remove(this.adorner);
                            this.adorner = null;

                        }
                    }
                };
                timer.Start();
            }
        }

        //private void backClick(object sender, RoutedEventArgs e)
        //{
        //    if (this.prevEpoch != null)
        //    {
        //        this.save();

        //        this.CurrentEpoch = this.prevEpoch;
        //    }
        //}

     

 

        //private void foreClick(object sender, RoutedEventArgs e)
        //{
        //    if (this.nextEpoch != null)
        //    {
        //        this.save();

        //        this.CurrentEpoch = this.nextEpoch;
        //    }
        //}

        private void Control_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (!this.CreationMode)
            {

                if (e.Key == Key.RightShift || e.Key == Key.LeftShift)
                {
                    this.curMode = gridMode.Add;
                }
                else if (e.Key == Key.Space)
                {
                    this.curMode = gridMode.Edit;
                }
                else if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl)
                {
                    this.curMode = gridMode.Delete;
                }
                else if (e.Key == Key.D)
                {
                    this.overlay.ShowIntervalNumbering = !this.overlay.ShowIntervalNumbering;
                }
                else if (e.Key == Key.S)
                {
                    this.ShowScoredEpochs = !this.ShowScoredEpochs;
                }
            }
        }

        private void Control_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (!this.CreationMode)
            {

                if (e.Key == Key.RightShift || e.Key == Key.LeftShift && this.curMode == gridMode.Add)
                {
                    this.curMode = gridMode.Scroll;
                }
                else if (e.Key == Key.Space && this.curMode == gridMode.Edit)
                {
                    this.curMode = gridMode.Scroll;
                }
                else if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl && this.curMode == gridMode.Delete)
                {
                    this.curMode = gridMode.Scroll;
                }
            }
        }

        private void rescorePreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            (sender as Button).Tag = Keyboard.FocusedElement;

            Keyboard.Focus(sender as Button);
            
        }
        
        private void rescoreMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Right)
            {
                if (this.dropBorder.Opacity == 1.0)
                {

                    this.runScoring(this.allData);

                    this.dropBorder.Opacity = 0.3;
                    DispatcherTimer timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromMilliseconds(2500);
                    timer.Tick += delegate(object timersender, EventArgs timerev)
                    {
                        this.dropBorder.Opacity = 1.0;
                        timer.Stop();
                    };
                    timer.Start();
                }
                else
                {
                    this.dropBorder.Opacity = 1.0;
                }

                Button button = sender as Button;
                if (button.Tag is IInputElement)
                {
                    Keyboard.Focus(button.Tag as IInputElement);

                    button.Tag = null;
                }
            }
        }

        private void rescoreClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            button.Tag = null;
            this.runScoring(this.allData);
            this.expander.IsExpanded = false;
            
        }

        private void textFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = sender as TextBox;
            if (sender == null) return;
            box.SelectAll();
        }

        private void Control_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            HitTestResult expandTest = VisualTreeHelper.HitTest(this.expander, e.GetPosition(this.expander));

            if (expandTest != null)
            {
                return;
            }

           

            if (this.expander.IsExpanded)
            {
                HitTestResult res = VisualTreeHelper.HitTest(this.dropBorder, e.GetPosition(this.dropBorder));
                if (res == null)
                {
                    this.expander.IsExpanded = false;
                }

                this.CreationMode = false;
            }
        }

        private ScoringSettings oldSettings { get; set; }

        private void behaviorCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = sender as ComboBox;

            ScoringBehavior behavior = box.SelectedItem as ScoringBehavior;
            if (behavior == null) return;
            this.Behavior = behavior.CloneEmpty();

            IScoringAlgorithm search = (from IScoringAlgorithm alg in this.implementationComboBox.Items
                                        where alg.ToString() == behavior.Algorithm.ToString()
                                        select alg).SingleOrDefault();

            if (search != null)
            {
                this.implementationComboBox.SelectedItem = search;
            }


            PatternImplementation pattern = (from PatternImplementation patter in this.scoringComboBox.Items
                                             where patter.ToString() == behavior.Pattern.ToString()
                                             select patter).SingleOrDefault();
            if (pattern != null)
            {
                this.scoringComboBox.SelectedItem = pattern;
            }

            this.oldSettings = (behavior.Algorithm.CloneEmpty()).Settings;


            this.expandBehavior.IsEnabled = true;

            if (behavior.Algorithm is EmptyAlgorithm)
            {
                this.implementationComboBox.IsEnabled = true;
                this.scoringComboBox.IsEnabled = true;

                this.behaviorBox.IsEnabled = true;
                this.behaviorBox.Text = "";

            
            }
            else
            {
                this.implementationComboBox.IsEnabled = false;
                this.scoringComboBox.IsEnabled = false;

                this.behaviorBox.IsEnabled = false;
                this.behaviorBox.Text = Behavior.Name;

            }
            this.reloadImp();
        }

        private ObservableCollection<object> sectionSource = new ObservableCollection<object>();

        private void scoringComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            PatternImplementation pattern = box.SelectedItem as PatternImplementation;
            if (pattern == null) return;
            if (pattern.ToString() == PatternImplementation.DefaultPatternName)
            {
                this.availList.IsEnabled = true;
                this.setList.IsEnabled = true;

                DataTemplate temp = this.FindResource("scoringTemplate") as DataTemplate;
                this.setList.ItemTemplate = temp;

                

                this.sectionSource.Clear();

                this.patternGrid.Height = 25;

            }
            else
            {
                this.availList.IsEnabled = false;
                this.setList.IsEnabled = false;

                this.setList.ItemTemplate = null;

                this.sectionSource.Clear();
                if (box.SelectedItem != null)
                {
                    PatternImplementation alg = box.SelectedItem as PatternImplementation;
                    foreach (string name in alg.IntervalNames)
                    {
                        this.sectionSource.Add(name);
                    }
                }

                this.patternGrid.Height = 0;

            }
            
        }

        private bool firstTimeClicked = false;

        private void backMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.prevEpoch != null)
            {
                if (e.ChangedButton == MouseButton.Left)
                {
                    IntervalScoringCentral.SaveToEpoch(this.curEpoch, this.Behavior);
                    if(!this.firstTimeClicked)
                        this.showMessage("Timings automatically saved. Remember, left click forward and backward epoch buttons to move and save, right click to move without saving.", TimeSpan.FromMilliseconds(4000), .9);
                }
                else
                {
                    if (!this.firstTimeClicked)
                        this.showMessage("Timings NOT saved. Remember, left click forward and backward epoch buttons to move and save, right click to move without saving.", TimeSpan.FromMilliseconds(4000), .9);
                }

                this.firstTimeClicked = true;

                this.CurrentEpoch = this.prevEpoch;
            }
        }

        private void foreMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.nextEpoch != null)
            {
                if (e.ChangedButton == MouseButton.Left)
                {
                    IntervalScoringCentral.SaveToEpoch(this.curEpoch, this.Behavior);                    
                    if (!this.firstTimeClicked)
                        this.showMessage("Timings automatically saved. Remember, left click forward and backward epoch buttons to move and save, right click to move without saving.", TimeSpan.FromMilliseconds(4000), .9);
                }
                else
                {
                    if (!this.firstTimeClicked)
                        this.showMessage("Timings NOT saved. Remember, left click forward and backward epoch buttons to move and save, right click to move without saving.", TimeSpan.FromMilliseconds(4000), .9);
                }

                this.firstTimeClicked = true;

                this.CurrentEpoch = this.nextEpoch;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            int s = 0;
        }

        private void expandBehaviorClick(object sender, RoutedEventArgs e)
        {
            this.CreationMode = !this.CreationMode;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ContentBox cb = new ContentBox();
            cb.Title = "Cycle Scoring Help";
            cb.ContentElement = new IntervalHelp();
            cb.Buttons = ContentBoxButton.OK;
            cb.Show();
        }

        private bool creationMode;
        protected bool CreationMode
        {
            get
            {
                return this.creationMode;
            }
            set
            {
                this.creationMode = value;



                if (this.creationMode)
                {
                    this.setupGrid.ColumnDefinitions[2].Width = new GridLength(1, GridUnitType.Star);
                    this.expandBehavior.Content = "<";


                    this.setupGroup.Header = "Default Setup";
                    

                    this.propertyGrid.SetBinding(PropertyGrid.InstanceProperty, new Binding() { Source = this.Behavior.Algorithm.CloneEmpty().Settings });

                }
                else
                {

                    this.setupGrid.ColumnDefinitions[2].Width = new GridLength(0, GridUnitType.Pixel);
                    this.expandBehavior.Content = ">";


                    this.setupGroup.Header = "Current Setup";

                    this.propertyGrid.SetBinding(PropertyGrid.InstanceProperty, new Binding() { Source = oldSettings });
                }

                this.setupGrid.InvalidateVisual();

            }
        }

        private void implementationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BindingExpression ex = this.propertyGrid.GetBindingExpression(PropertyGrid.InstanceProperty);


            reloadImp();
        }

        private void reloadImp()
        {
            if (this.implementationComboBox.SelectedItem is EmptyAlgorithm)
            {

                this.setupButton.IsEnabled = false;

                this.propertyGrid.SetBinding(PropertyGrid.InstanceProperty, new Binding() { Source = this.implementationComboBox, Path = new PropertyPath("SelectedItem.Settings") });

                this.revertButton.IsEnabled = false;

                this.oldSettings = null;
            }
            else
            {

                this.setupButton.IsEnabled = true;

                if ((this.behaviorCombo.SelectedItem as ScoringBehavior).Name == ScoringBehavior.DefaultBehaviorName)
                    this.oldSettings = (this.implementationComboBox.SelectedItem as IScoringAlgorithm).CloneEmpty().Settings;
                else this.oldSettings = (this.behaviorCombo.SelectedItem as ScoringBehavior).Algorithm.CloneEmpty().Settings;

                this.propertyGrid.SetBinding(PropertyGrid.InstanceProperty, new Binding() { Source = oldSettings });

                this.revertButton.IsEnabled = true;



            }
        }

        private void saveBehaviorCallback(object sender, RoutedEventArgs e)
        {
            if (this.Behavior != null && this.Behavior.Name != ScoringBehavior.DefaultBehaviorName)
            {
                IntervalScoringCentral.SaveAlgorithmSettings(this.Behavior, this.propertyGrid.Instance as ScoringSettings);
            }
            else
            {
                if (behaviorBox.Text == "")
                {
                    MessageBox.Show("Please give the behavior a name then save.");
                    return;
                }

                if (behaviorCombo.Items.Cast<ScoringBehavior>().Any(sb => behaviorBox.Text == sb.Name))
                {
                    MessageBox.Show("This behavior name is taken. Please change it and save again.");
                    return;
                }




                PatternImplementation toUse = null;
                if ((this.scoringComboBox.SelectedItem as PatternImplementation).Name == PatternImplementation.DefaultPatternName)
                {
                    if (this.patternNameText.Text == "")
                    {
                        MessageBox.Show("Pattern does not have a name. Give it a name and try again.");
                        return;
                    }

                    if (this.scoringComboBox.Items.Cast<PatternImplementation>().Any(v => v.Name == this.patternNameText.Text))
                    {
                        MessageBox.Show("This pattern name already exists. Please change it and try again.");
                        return;
                    }

                    foreach (sectionEdit edit in this.sectionSource)
                    {
                        if (edit.SectionName == "" || edit.SectionName == null)
                        {
                            MessageBox.Show("Not all sections in the pattern have a name. Give them all names and try again.");
                            return;
                        }
                    }

                    toUse = new PatternImplementation(this.patternNameText.Text, this.sectionSource.Cast<sectionEdit>().Select(ss => ss.SectionName).ToArray(),
                        this.sectionSource.Cast<sectionEdit>().Select(ss => ss.SectionNumber).ToArray());
                }
                else
                {
                    toUse = (this.scoringComboBox.SelectedItem as PatternImplementation).CloneEmpty();
                }

                ScoringBehavior behave = new ScoringBehavior(behaviorBox.Text, (this.implementationComboBox.SelectedItem as IScoringAlgorithm).CloneEmpty(), toUse);
                IntervalScoringCentral.CreateBehavior(behave);

                IntervalScoringCentral.SaveAlgorithmSettings(behave, this.propertyGrid.Instance as ScoringSettings);


                BindingExpression ex = this.behaviorCombo.GetBindingExpression(ComboBox.ItemsSourceProperty);
                ex.UpdateTarget();
                this.behaviorCombo.SelectedItem = this.behaviorCombo.Items.Cast<ScoringBehavior>().Single(b => b.Name == behave.Name);

                BindingExpression ex2 = this.scoringComboBox.GetBindingExpression(ComboBox.ItemsSourceProperty);
                ex2.UpdateTarget();
                this.scoringComboBox.SelectedItem = this.scoringComboBox.Items.Cast<PatternImplementation>().Single(b => b.Name == toUse.Name);                                
            }
        }

        private void revertCallback(object sender, RoutedEventArgs e)
        {
            if (!this.CreationMode)
            {
                this.oldSettings = this.Behavior.Algorithm.CloneEmpty().Settings;
                this.propertyGrid.SetBinding(PropertyGrid.InstanceProperty, new Binding() { Source = oldSettings });
            }
            else
            {
                this.propertyGrid.Instance = this.Behavior.Algorithm.CloneEmpty().Settings;

            }
            
        }

        private void availList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.availList.SelectedItem == null) return;

            sectionEdit test = new sectionEdit() { SectionName = "", SectionType = this.availList.SelectedItem as string, SectionNumber=this.availList.SelectedIndex };
            this.sectionSource.Add(test);
            
        }

        private void deleteSection(object sender, RoutedEventArgs e)
        {
            sectionEdit edit = (sender as Button).Tag as sectionEdit;
            if (edit == null) return;

            this.sectionSource.Remove(edit);
        }

        private void saveClick(object sender, RoutedEventArgs e)
        {
            if (this.curEpoch != null && this.Behavior != null)
            {
                IntervalScoringCentral.SaveToEpoch(this.curEpoch, this.Behavior);
            }

            try
            {
                //If the window isn't opened as a dialog this throws an exception...not sure how to check to see what it was opened as
                this.DialogResult = true;
            }
            catch
            {

            }

            this.Close();

        }

        private void cancelClick(object sender, RoutedEventArgs e)
        {
            try
            {
                this.DialogResult = false;
            }
            catch
            {

            }
            this.Close();
        }

        private void expander_Collapsed(object sender, RoutedEventArgs e)
        {
            this.CreationMode = false;
           
           // Object obj = Keyboard.FocusedElement;
        }





        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion




    }
}
