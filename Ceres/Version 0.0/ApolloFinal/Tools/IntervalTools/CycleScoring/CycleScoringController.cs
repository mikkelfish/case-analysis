﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl;
using CeresBase.FlowControl.Adapters;
using CeresBase.Projects.Flowable;
using ApolloFinal.Tools.BatchProcessable;
using CeresBase.Projects;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    class CycleScoringController : IRoutineController
    {

        public CycleScoringController(RoutineBatchControl control)
        {
            this.BatchControl = control;
        }

        #region IRoutineController Members


        public System.Windows.FrameworkElement GetContentOverride()
        {
            return null;
        }

        public void AdaptersInitialized(Routine routine, AdapterInitPair[] pairs)
        {
            var sortedAdapters = from AdapterInitPair p in pairs
                                 orderby p.Adapter.ID
                                 select p;

            foreach (AdapterInitPair adapter in sortedAdapters)
            {
                if (adapter.Adapter is RawDataAdapter)
                {
                    RawDataAdapter desc = adapter.Adapter as RawDataAdapter;                 
                    desc.Epoch = (routine.Args) as Epoch;
                }
            }
        }

        public void AddAvailableAdapters(RoutineManager rm)
        {
            IFlowControlAdapter[] adapters = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IFlowControlAdapter>(null);
            foreach (IFlowControlAdapter adapter in adapters)
            {
                AdapterDescriptionAttribute[] attr = MesosoftCommon.Utilities.Reflection.Common.GetAttributes<AdapterDescriptionAttribute>(adapter);
                if (attr == null) continue;
                if (attr.Any(at => at.Description == "RealTime" || at.Description == "Raw") && !rm.AvailableAdapters.Any(ad => ad.Name == adapter.Name && ad.Category == adapter.Category))
                {
                    rm.AvailableAdapters.Add(adapter);
                }
            }

            IBatchFlowable[] flows = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IBatchFlowable>(null);
            foreach (IBatchFlowable adapter in flows)
            {
                AdapterDescriptionAttribute[] attr = MesosoftCommon.Utilities.Reflection.Common.GetAttributes<AdapterDescriptionAttribute>(adapter);
                if (attr == null) continue;
                if (attr.Any(at => at.Description == "Raw") && !rm.AvailableAdapters.Any(ad => ad.Name == adapter.ToString() && ad.Category == adapter.ToString()))
                {
                    rm.AvailableAdapters.Add(new FlowableBatchProcessorAdapter(adapter));
                }
            }
        }

        public void AddButtonClicked()
        {
            
        }

        public RoutineBatchControl BatchControl
        {
            get;
            private set;
        }

        #endregion
    }
}
