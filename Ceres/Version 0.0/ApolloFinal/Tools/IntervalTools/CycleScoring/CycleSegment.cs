﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    public abstract class CycleSegment
    {
        public CycleSegment(float start, float end)
        {
            this.startTime = start;
            this.endTime = end;
        }

        public int SegmentType { get; set; }

        private float startTime;
        public float StartTime
        {
            get
            {
                return this.startTime;
            }
            set
            {
                this.startTime = value;
                if (this.startTime > this.endTime)
                {
                    this.startTime = this.endTime;
                }

                if (this.Previous != null)
                {
                    this.Previous.endTime = this.startTime;
                }
            }
        }

        private float endTime;
        public float EndTime
        {
            get
            {
                return this.endTime;
            }
            set
            {

                this.endTime = value;

                if (this.endTime < this.startTime)
                {
                    this.endTime = this.startTime;
                }

                if (this.Next != null)
                {
                    this.Next.startTime = this.endTime;
                }
            }
        }


        public float Length
        {
            get
            {
                return this.EndTime - this.StartTime;
            }
        }

        private CycleSegment previous;
        public CycleSegment Previous
        {
            get
            {
                return this.previous;
            }
            set
            {
               
                this.previous = value;

                if (this.previous == this) return;


                if (this.previous != null)
                {
                    this.previous.next = this;
                    this.previous.EndTime = this.startTime;
                }
            }
        }

        private CycleSegment next;
        public CycleSegment Next
        {
            get
            {
                return this.next;
            }
            set
            {
                CycleSegment oldNext = this.next;
                this.next = value;

                if (this.next == this)
                    return;

                if (this.next != null)
                {
                    this.next.previous = this;
                    this.next.StartTime = this.endTime;
                }

            }
        }
    }
}
