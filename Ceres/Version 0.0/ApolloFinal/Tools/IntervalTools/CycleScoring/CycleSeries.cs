﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    public class CycleSeries
    {
        public CycleSeries(string identifier)
        {
            this.Identifier = identifier;
        }

        private ObservableCollection<CycleSegment> segments = new ObservableCollection<CycleSegment>();
        public ObservableCollection<CycleSegment> Segments
        {
            get
            {
                return this.segments;
            }
        }

        public float Start
        {
            get
            {
                CycleSegment seg = (from CycleSegment segment in segments
                                    orderby segment.StartTime ascending
                                    select segment).FirstOrDefault();
                if (seg == null) return float.NaN;
                return seg.StartTime;

            }
        }

        public float End
        {
            get
            {
                CycleSegment seg = (from CycleSegment segment in segments
                                    orderby segment.EndTime descending
                                    select segment).LastOrDefault();
                if (seg == null) return float.NaN;
                return seg.StartTime;
            }
        }

        public string Identifier
        {
            get;
            private set;
        }

        public virtual void AddSegment(CycleSegment seg, bool merge)
        {
            CycleSegment prev = null;

            for (int i = this.segments.Count-1; i >= 0; i--)
            {
                if (this.segments[i].StartTime < seg.StartTime && this.segments[i].EndTime > seg.StartTime)
                {
                    prev = this.segments[i];
                    break;
                }
                else if (this.segments[i].StartTime < seg.StartTime && this.segments[i].EndTime < seg.StartTime)
                {
                    break;
                }
            }

            //CycleSegment prev = (from CycleSegment test in this.segments
            //            where test.StartTime < seg.StartTime && test.EndTime > seg.StartTime
            //            select test).FirstOrDefault();

            if (prev == null && this.segments.Count > 0)
            {
                prev = this.segments[this.segments.Count - 1];
            }

            CycleSegment next = null;
            for (int i = this.segments.Count - 1; i >= 0; i--)
            {
                if (this.segments[i].StartTime < seg.EndTime && this.segments[i].EndTime > seg.EndTime)
                {
                    next = this.segments[i];
                    break;
                }
                else if(this.segments[i].StartTime < seg.EndTime && this.segments[i].EndTime < seg.StartTime)
                {
                    break;
                }
            }
                
                //(from CycleSegment test in this.segments
                //                 where test.StartTime < seg.EndTime && test.EndTime > seg.EndTime
                //                 select test).FirstOrDefault();

            seg.Previous = prev;
            seg.Next = next;
            this.segments.Add(seg);

            if (merge)
            {
                while (seg.Previous != null && seg.Previous.SegmentType == seg.SegmentType)
                {
                    this.DeleteSegment(seg.Previous);
                }


                while (seg.Next != null && seg.Next.SegmentType == seg.SegmentType)
                {
                    this.DeleteSegment(seg.Next);
                }
            }
        }

        public virtual void DeleteSegment(CycleSegment seg)
        {
            if (seg.Previous != null)
                seg.Previous.Next = seg.Next;
            else if (seg.Next != null)
                seg.Next.Previous = seg.Previous;

            this.segments.Remove(seg);
        }

        public CycleSegment[] FindSegments(Type seriesType, float start, float end)
        {
            if (this.GetType() != seriesType) return null;

            return (from CycleSegment seg in this.segments
                    where seg.StartTime >= start && seg.EndTime <= end
                    select seg).ToArray();

        }
    }
}
