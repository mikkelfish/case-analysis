﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Media;
using System.Collections.ObjectModel;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    public class PatternImplementation : INotifyPropertyChanged
    {
        public const string DefaultPatternName = "New Pattern";

        public PatternImplementation()
        {
            this.Name = PatternImplementation.DefaultPatternName;
        }

        public PatternImplementation(string name, string[] intervalNames, int[] regionPattern)
        {
            this.Name = name;
            this.IntervalNames = intervalNames;
            this.RegionPattern = regionPattern;
        }

        public PatternImplementation CloneEmpty()
        {
            PatternImplementation newImp = new PatternImplementation();
            newImp.Name = this.Name;
            newImp.IntervalNames = this.IntervalNames;
            newImp.RegionPattern = this.RegionPattern;
            return newImp;
        }

        public void ConnectWithSeries(CycleSeries series)
        {
            this.Series = series;
            this.Series.Segments.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Segments_CollectionChanged);
            this.mark();
        }

        public override string ToString()
        {
            return this.Name;
        }


        private bool needToMark = false;

        private bool refrainFromMarking = false;
        public bool RefrainFromMarking 
        {
            get
            {
                return this.refrainFromMarking;
            }
            set
            {
                this.refrainFromMarking = value;
                if (this.refrainFromMarking == false && needToMark)
                {
                    this.mark();
                }

                this.OnPropertyChanged("RefrainFromMarking");
            }
        }

        private bool bulkChanges;
        public bool BulkChangesInProgress
        {
            get
            {
                return this.bulkChanges;
            }
            set
            {
                this.bulkChanges = value;
                this.OnPropertyChanged("BulkChangesInProgress");
            }
        }

        void Segments_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.mark();
        }

        public string Name { get; private set; }

        public string[] IntervalNames
        {
            get;
            private set;
        }

        public int[] RegionPattern { get; private set; }


        public CycleSeries Series { get; private set; }

        public Color[] ColorsToUse
        {
            get
            {
                return new Color[] { Colors.Purple, Colors.Yellow, Colors.Brown};
            }
            set
            {
            }
        }

        public float[][] IntervalLengths
        {
            get
            {
                if (this.IntervalNames == null) return null;


                float[][] toRet = new float[this.IntervalNames.Length][];
                for (int i = 0; i < this.IntervalNames.Length; i++)
                {
                    toRet[i] = new float[this.allmarks.Count / this.IntervalNames.Length];
                }

                int curSpot = 0;
                int index = 0;
                for (int i = 0; i < allmarks.Count; i++)
                {
                    toRet[curSpot][index] = allmarks[i].Length;
                    curSpot++;
                    if (curSpot == this.IntervalNames.Length)
                    {
                        curSpot = 0;
                        index++;
                    }
                }

                return toRet;
            }
        }

        public float[][] IntervalTimes
        {
            get
            {
                if (this.IntervalNames == null) return null;
                float[][] toRet = new float[this.IntervalNames.Length][];
                for (int i = 0; i < this.IntervalNames.Length; i++)
                {
                    toRet[i] = new float[this.allmarks.Count / this.IntervalNames.Length];
                }

                int curSpot = 0;
                int index = 0;
                for (int i = 0; i < allmarks.Count; i++)
                {
                    toRet[curSpot][index] = allmarks[i].Time;
                    curSpot++;
                    if (curSpot == this.IntervalNames.Length)
                    {
                        curSpot = 0;
                        index++;
                    }
                }

                return toRet;

            }
        }


        private ObservableCollection<Mark> allmarks = new ObservableCollection<Mark>();
        public ObservableCollection<Mark> AllMarks { get { return this.allmarks; } }

        private void mark()
        {
            if (this.refrainFromMarking)
            {
                this.needToMark = true;
                return;
            }

            this.BulkChangesInProgress = true;

            this.allmarks.Clear();

            List<Mark> marks = new List<Mark>();

            int curSpot = 0;
            int curMark = this.RegionPattern[0];
            for (int i = 0; i < this.Series.Segments.Count; i++)
            {
               
                if (marks.Count > 0)
                {
                    marks[marks.Count - 1].Length = this.Series.Segments[i].StartTime - marks[marks.Count - 1].Time;
                }

                
                if (this.Series.Segments[i].SegmentType != curMark)
                {
                    continue;
                }



                Mark mark = new Mark(this, this.Series.Segments[i].StartTime) { ID = curSpot };
                marks.Add(mark);

                if (i == this.Series.Segments.Count - 1)
                {
                    curSpot = this.RegionPattern.Length - 1;
                    mark.Length = this.Series.Segments[i].Length;
                }


                curSpot++;
                if (curSpot >= this.RegionPattern.Length)
                {
                    curSpot = 0;
                }

                curMark = this.RegionPattern[curSpot];
            }

            if (marks.Count == 0) return;

          //  marks[marks.Count - 1].Length = this.Series.Segments[this.Series.Segments.Count - 1].EndTime - marks[marks.Count - 1].Time;


            int total = (marks.Count / this.RegionPattern.Length) * this.RegionPattern.Length;

            for(int i = 0; i < total; i++)
            {
                this.allmarks.Add(marks[i]);
            }

            this.BulkChangesInProgress = false;
            


            this.OnPropertyChanged("AllMarks");

        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler ev = this.PropertyChanged;
            if (ev != null)
            {
                ev(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion
    }
}
