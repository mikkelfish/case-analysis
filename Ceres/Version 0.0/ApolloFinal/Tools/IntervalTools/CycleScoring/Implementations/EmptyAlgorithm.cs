﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations
{
    public class EmptyAlgorithm : IScoringAlgorithm
    {
        #region IScoringAlgorithm Members

        public ScoringSettings Settings
        {
            get { return new ScoringSettings(); }
        }

        public string[] RegionTypes
        {
            get { return new string[]{}; }
        }

        public CycleSeries Score(CeresBase.Projects.Epoch epoch)
        {
            return null;
        }

        public override string ToString()
        {
            return "Empty Scoring Algorithm";
        }

        #endregion

        #region IScoringAlgorithm Members


        public IScoringAlgorithm CloneEmpty()
        {
            return new EmptyAlgorithm();
        }

        #endregion

        #region IScoringAlgorithm Members


        public CycleSeries Score(CeresBase.Projects.Epoch epoch, ScoringSettings passedSettings)
        {
            return null;
        }

        #endregion

        #region IScoringAlgorithm Members


        public CycleSeries Score(string identifier, float[] data)
        {
            return null;
        }

        public CycleSeries Score(string identifier, float[] data, ScoringSettings passedSettings)
        {
            return null;
        }

        #endregion

        #region IScoringAlgorithm Members


        public CycleSeries Score(CeresBase.Projects.Epoch epoch, float[] data, double res)
        {
            return null;
        }

        public CycleSeries Score(CeresBase.Projects.Epoch epoch, float[] data, double res, ScoringSettings passedSettings)
        {
            return null; 
        }

        #endregion
    }
}
