﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations.EKG
{
    
    public class PeakSegment : ApolloFinal.Tools.IntervalTools.CycleScoring.CycleSegment
    {
        public const int Peak = 0;
       
        public float Height { get; set; }

        public PeakSegment(float start, float end)
            : base(start, end)
        {

        }
    }
}
