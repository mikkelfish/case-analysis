﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations.EKG
{
    public class PeakScoring : ApolloFinal.Tools.IntervalTools.CycleScoring.IScoringAlgorithm
    {
        public override string ToString()
        {
            return "Peak Scoring";
        }

        public PeakScoring()
        {
            this.settings = new PeakSettings();
        }

        #region IScoringAlgorithm Members

        private PeakSettings settings;
        public ScoringSettings Settings
        {
            get { return this.settings; }
        }

        public string[] RegionTypes
        {
            get { return new string[] { "Peak" }; }
        }

        public CycleSeries Score(CeresBase.Projects.Epoch epoch)
        {
            float[] data = (epoch.Variable as SpikeVariable).GetTimeSeriesData(epoch.BeginTime, epoch.EndTime);
            return this.Score(epoch, data, (epoch.Variable as SpikeVariable).Resolution, null);
        }

        public CycleSeries Score(CeresBase.Projects.Epoch epoch, float[] data, double res)
        {
            return this.Score(epoch, data, res, null);
        }

        public CycleSeries Score(CeresBase.Projects.Epoch epoch, ScoringSettings passedSettings)
        {
            float[] data = (epoch.Variable as SpikeVariable).GetTimeSeriesData(epoch.BeginTime, epoch.EndTime);  // SpikeVariable.ReadData(epoch.Variable as SpikeVariable, epoch.BeginTime, epoch.EndTime);
            return this.Score(epoch, data, (epoch.Variable as SpikeVariable).Resolution, passedSettings);     
        }

        public CycleSeries Score(CeresBase.Projects.Epoch epoch, float[] data, double res, ScoringSettings pSettings)
        {
            CycleSeries toRet = new CycleSeries(epoch.Variable.FullDescription);

            PeakSettings passedSettings = pSettings as PeakSettings;

            float max = data.Max();
            float min = data.Min();

            double threshold = this.settings.Threshold * (max - min);
            double minLength = this.settings.MinLength;

            if (passedSettings != null)
            {
                threshold = passedSettings.Threshold * (max - min);
                minLength = passedSettings.MinLength;
            }

            threshold = min + threshold;

            int first = -1;
            
            for (int i = 0; i < data.Length; )
            {
                if (data[i] < threshold)
                {
                    i += 1;
                    continue;
                }

                float maxv = float.MinValue;
                int second = -1;
                for (int j = i; j < data.Length; j++, i++)
                {
                    if (data[j] < threshold) break;
                    if (data[j] > maxv)
                    {
                        maxv = data[j];
                        second = j;
                    }
                }

                if (second == -1)
                {
                    i++;
                    continue;
                }

                if ((second - first) * res > minLength)
                {
                    if (first != -1)
                    {
                        toRet.AddSegment(new PeakSegment(epoch.BeginTime + (float)(first * res), epoch.BeginTime + (float)(second * res)) { Height = maxv }, false);
                    }
                    first = second;
                }
            }

           
            
            return toRet;
        }

        public IScoringAlgorithm CloneEmpty()
        {
            PeakScoring newScore = new PeakScoring();
            newScore.settings = this.settings.Clone() as PeakSettings;
            return newScore;
        }

        #endregion
    }
}
