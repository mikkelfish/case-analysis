﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations.EKG
{
    public class PeakSettings : ApolloFinal.Tools.IntervalTools.CycleScoring.ScoringSettings, ICloneable
    {
        public double Threshold { get; set; }
        public double MinLength { get; set; }

        #region ICloneable Members

        public object Clone()
        {
            PeakSettings settings = new PeakSettings();
            settings.MinLength = this.MinLength;
            settings.Threshold = this.Threshold;
            return settings;
        }

        #endregion
    }
}
