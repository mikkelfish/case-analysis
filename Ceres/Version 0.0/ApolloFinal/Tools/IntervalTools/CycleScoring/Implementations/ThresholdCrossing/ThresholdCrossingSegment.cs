﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations.ThresholdCrossing
{
    public class ThresholdCrossingSegment : ApolloFinal.Tools.IntervalTools.CycleScoring.CycleSegment
    {
        public const int Below = 0;
        public const int Above = 1;

        public ThresholdCrossingSegment(float start, float end)
            : base(start, end)
        {

        }
    }
}
