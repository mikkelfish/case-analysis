﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations.ThresholdCrossing
{
    public class ThresholdCrossingSettings : ScoringSettings, ICloneable
    {

        public double Threshold { get; set; }
        public double MinSegmentTime { get; set; }

        #region ICloneable Members

        public object Clone()
        {
            ThresholdCrossingSettings settings = new ThresholdCrossingSettings();
            settings.Threshold = this.Threshold;
            settings.MinSegmentTime = this.MinSegmentTime;

            return settings;
        }

        #endregion
    }
}
