﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations.ExponentialMA
{
    public class ExponentialMASettings : ApolloFinal.Tools.IntervalTools.CycleScoring.ScoringSettings, ICloneable
    {
        public double Falloff { get; set; }
        public double Sensitivity { get; set; }
        public double MinLength { get; set; }
        public double PositiveThreshold { get; set; }
        public double NegativeThreshold { get; set; }


        #region ICloneable Members

        public object Clone()
        {
            ExponentialMASettings toRet = new ExponentialMASettings();
            toRet.Falloff = this.Falloff;
            toRet.Sensitivity = this.Sensitivity;
            toRet.MinLength = this.MinLength;
            toRet.PositiveThreshold = this.PositiveThreshold;
            toRet.NegativeThreshold = this.NegativeThreshold;
            return toRet;
        }

        #endregion
    }
}
