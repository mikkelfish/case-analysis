﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations.ExponentialMA
{
    public class ExponentialMASegment : ApolloFinal.Tools.IntervalTools.CycleScoring.CycleSegment
    {
        public const int Positive = 0;
        public const int Negative = 1;
        public const int Flat = 2;

        public float Height { get; set; }

        public ExponentialMASegment(float start, float end)
            : base(start, end)
        {

        }
    }
}
