﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CeresBase.Projects;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations.ExponentialMA
{
    public class ExponentialMAScoring : ApolloFinal.Tools.IntervalTools.CycleScoring.IScoringAlgorithm
    {
        public string[] RegionTypes { get { return new string[] { "Positive", "Negative", "Flat" }; } }

        public ExponentialMAScoring()
        {
            this.settings = new ExponentialMASettings();
        }

        #region IScoringAlgorithm Members

        private ExponentialMASettings settings;
        public ApolloFinal.Tools.IntervalTools.CycleScoring.ScoringSettings Settings
        {
            get
            {
                return this.settings;
            }
        }

        public override string ToString()
        {
            return "Exponential Moving Average Algorithm";
        }
      
        #endregion

        #region IScoringAlgorithm Members


        public CycleSeries Score(CeresBase.Projects.Epoch epoch)
        {
            float[] data = (epoch.Variable as SpikeVariable).GetTimeSeriesData(epoch.BeginTime, epoch.EndTime); 
            return this.Score(epoch, data, (epoch.Variable as SpikeVariable).Resolution, null);
        }

        public CycleSeries Score(Epoch epoch, float[] data, double res)
        {
            return this.Score(epoch, data, res, null);
        }


        private void runScoring(float[] data, CycleSeries series, double sensitivity, double falloff, double posThresh, double negThresh, double minLength,
            double resolution, double startTime)
        {
            double runningSlope = 0;
            int curType = ExponentialMASegment.Flat;
            int firstIndex = 0;

            for (int i = 0; i < data.Length - 1; i++)
            {
                #region Score segments
                float slope = data[i + 1] - data[i];
                runningSlope = runningSlope * falloff + (1.0 - falloff) * slope;

                //If the moving average hasn't changed considerably keep going
                if (runningSlope < sensitivity && runningSlope > -sensitivity && curType == ExponentialMASegment.Flat)
                {
                    continue;
                }
                else if (runningSlope > sensitivity && curType == ExponentialMASegment.Positive)
                {
                    continue;
                }
                else if (runningSlope < -sensitivity && curType == ExponentialMASegment.Negative)
                {
                    continue;
                }
                
                //If it gets down here then there has been a change
                int thisType = curType;
                if (runningSlope < -sensitivity)
                {
                    curType = ExponentialMASegment.Negative;
                }
                else if (runningSlope > sensitivity)
                {
                    curType = ExponentialMASegment.Positive;
                }
                else curType = ExponentialMASegment.Flat;

                //Calculate the height change between the beginning of the segment and the end of the segment
                float height = data[i] - data[firstIndex];


                ExponentialMASegment newSegment = new ExponentialMASegment((float)(startTime + firstIndex * resolution), (float)(startTime + i * resolution)) { SegmentType = thisType, Height = height };
                series.AddSegment(newSegment, true);

                //Mark the start of the new segment
                firstIndex = i + 1;
                #endregion
            }

            //Delete any segments that are too short
            #region Delete short segments
            for (int i = 0; i < series.Segments.Count; )
            {
                if (series.Segments[i].Length < minLength)
                {
                    series.DeleteSegment(series.Segments[i]);
                }
                else i++;
            }
            #endregion

            //Delete any segments that aren't high enough
            #region Delete small segments

            double averagePos = 0;
            int posCount = 0;

            double averageNeg = 0;
            int negCount = 0;

            double averageFlat = 0;
            int flatCount = 0;

            foreach (ExponentialMASegment seg in series.Segments)
            {
                if (seg.SegmentType == ExponentialMASegment.Flat)
                {
                    averageFlat += seg.Height;
                    flatCount++;
                }
                if (seg.SegmentType == ExponentialMASegment.Negative)
                {
                    averageNeg += seg.Height;
                    negCount++;
                }

                if (seg.SegmentType == ExponentialMASegment.Positive)
                {
                    averagePos += seg.Height;
                    posCount++;
                }
            }

            averagePos = averagePos / posCount;
            averageNeg = averageNeg / negCount;
            averageFlat = averageFlat / flatCount;


            for (int i = 0; i < series.Segments.Count; )
            {
                if (series.Segments[i].SegmentType == ExponentialMASegment.Flat)
                {
                    i++;
                    continue;
                }
                else if (series.Segments[i].SegmentType == ExponentialMASegment.Positive)
                {
                    if (Math.Abs((series.Segments[i] as ExponentialMASegment).Height) > averagePos * posThresh)
                    {
                        i++;
                        continue;
                    }

                    series.DeleteSegment(series.Segments[i]);
                }
                else
                {
                    if (Math.Abs((series.Segments[i] as ExponentialMASegment).Height) > averageNeg * negThresh)
                    {
                        i++;
                        continue;
                    }

                    series.DeleteSegment(series.Segments[i]);
                }
            }
            #endregion
        }

        #endregion

        #region IScoringAlgorithm Members


        public IScoringAlgorithm CloneEmpty()
        {
            ExponentialMAScoring newScore = new ExponentialMAScoring();
            newScore.settings = this.settings.Clone() as ExponentialMASettings;
            return newScore;
        }

        #endregion

        public CycleSeries Score(Epoch epoch, float[] transformedData, double res, ScoringSettings pSettings)
        {
            CycleSeries toRet = new CycleSeries(epoch.Variable.FullDescription);

            ExponentialMASettings passedSettings = pSettings as ExponentialMASettings;

            float max = transformedData.Max();
            float min = transformedData.Min();

            double slopeSense = this.settings.Sensitivity * (max - min);
            double negThresh = this.settings.NegativeThreshold;
            double posThresh = this.settings.PositiveThreshold;
            double fallOff = this.settings.Falloff;
            double minLength = this.settings.MinLength;

            if (passedSettings != null)
            {
                slopeSense = passedSettings.Sensitivity * (max - min);
                negThresh = passedSettings.NegativeThreshold;
                posThresh = passedSettings.PositiveThreshold;
                fallOff = passedSettings.Falloff;
                minLength = passedSettings.MinLength;
            }

            this.runScoring(transformedData, toRet, slopeSense, fallOff, posThresh, negThresh, minLength, res, epoch.BeginTime);
            return toRet;
        }

        public CycleSeries Score(Epoch epoch, ExponentialMASettings passedSettings)
        {

            float[] data = (epoch.Variable as SpikeVariable).GetTimeSeriesData(epoch.BeginTime, epoch.EndTime);  // SpikeVariable.ReadData(epoch.Variable as SpikeVariable, epoch.BeginTime, epoch.EndTime);
            return this.Score(epoch, data, (epoch.Variable as SpikeVariable).Resolution,  passedSettings);           
        }

        CycleSeries IScoringAlgorithm.Score(CeresBase.Projects.Epoch epoch, ScoringSettings passedSettings)
        {
            return this.Score(epoch, passedSettings as ExponentialMASettings);
        }

    
    }
}
