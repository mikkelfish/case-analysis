﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    public partial class ScoredOverlayControl : UserControl
    {
        private class historyEntry
        {
            public OverlayMode Mode { get; set; }
            public Mark AffectedMark { get; set; }
            public int Index { get; set; }
            public float Time { get; set; }
            public float Length { get; set; }
            public float OldTime { get; set; }
            public float OldLength { get; set; }
        }

        private List<historyEntry> history = new List<historyEntry>();
        private int historyIndex = -1;

        protected void ClearHistory()
        {
            this.historyIndex = -1;
            this.history.Clear();
        }

        protected void AddEditAction(Mark affectedMark, float oldTime, float oldLength)
        {
            historyEntry entry = new historyEntry() { 
                AffectedMark = affectedMark, Mode = OverlayMode.Edit, Time = affectedMark.Time, Length = affectedMark.Length, OldLength=oldLength, OldTime =oldTime };

            addToUndoList(entry);
        }

        private void addToUndoList(historyEntry entry)
        {
            if (this.historyIndex + 1 >= this.history.Count)
                this.history.Add(entry);
            else
            {
                this.history.Insert(this.historyIndex + 1, entry);

                if (this.historyIndex + 2 < this.history.Count)
                {
                    this.history.RemoveRange(this.historyIndex + 2, this.history.Count - (this.historyIndex + 1) - 1);
                }
            }

            this.historyIndex = this.history.Count - 1;
        }

        protected void AddAddDelAction(OverlayMode mode, Mark affectedMark, int index)
        {
            historyEntry entry = new historyEntry()
            {
                AffectedMark = affectedMark,
                Mode = mode,
                Index = index
            };

            addToUndoList(entry);
        }

        protected bool Undo()
        {
            if (this.historyIndex < 0) return false;

            historyEntry entry = this.history[this.historyIndex];

            if (entry.AffectedMark.Implementation.AllMarks.Contains(entry.AffectedMark))
            {


                if (entry.Mode == OverlayMode.Edit)
                {
                    entry.AffectedMark.Time = entry.OldTime;
                    entry.AffectedMark.Length = entry.OldLength;
                }
                else if (entry.Mode == OverlayMode.Add)
                {
                    entry.AffectedMark.Implementation.AllMarks.RemoveAt(entry.Index);
                }
                else if (entry.Mode == OverlayMode.Delete)
                {
                    entry.AffectedMark.Implementation.AllMarks.Insert(entry.Index, entry.AffectedMark);
                }
            }

            this.historyIndex--;
            return true;
        }

        protected bool Redo()
        {

            if (this.historyIndex + 1 >= this.history.Count) return false;

            historyEntry entry = this.history[this.historyIndex + 1];

            if (entry.AffectedMark.Implementation.AllMarks.Contains(entry.AffectedMark))
            {
                if (entry.Mode == OverlayMode.Edit)
                {
                    entry.AffectedMark.Time = entry.Time;
                    entry.AffectedMark.Length = entry.Length;
                }
                else if (entry.Mode == OverlayMode.Add)
                {
                    entry.AffectedMark.Implementation.AllMarks.Insert(entry.Index, entry.AffectedMark);
                }
                else if (entry.Mode == OverlayMode.Delete)
                {
                    entry.AffectedMark.Implementation.AllMarks.RemoveAt(entry.Index);
                }
            }

            this.historyIndex++;
            return true;
        }
    }
}
