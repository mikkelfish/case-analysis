﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;
using System.Runtime.Serialization;
using System.Reflection;
using DatabaseUtilityLibrary;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Central
{
    class ScoringBehaviorTranslator : ITranslator
    {
        [AlwaysSerialize]
        private class behaviorSaver
        {
            public string ImpName { get; set; }


            public ScoringSettings Settings { get; set; }
            public string AlgType { get; set; }

            public string BehaviorName { get; set; }
        }

        #region ITranslator Members

        public object AttachedObject
        {
            get;
            set;
        }

        public bool SupportsType(Type t)
        {
            if (t == typeof(ScoringBehavior)) return true;
            return false;
        }

        public double Version
        {
            get { return 0.0; }
        }

        public bool Supports(object obj)
        {
            return (obj is ScoringBehavior);
        }

        #endregion

        #region ISerializable Members

        protected ScoringBehaviorTranslator(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            if (info.MemberCount == 0) return;

            behaviorSaver saver = info.GetValue("Saver", typeof(behaviorSaver)) as behaviorSaver;
            if (saver == null) return;

            PatternImplementation pattern = IntervalScoringCentral.Implementations.SingleOrDefault(p => p.Name == saver.ImpName);
            IScoringAlgorithm alg = IntervalScoringCentral.Algorithms.SingleOrDefault(a => a.GetType().FullName == saver.AlgType);
            if (alg != null && pattern != null)
            {
                //Create a new version
                alg = alg.CloneEmpty();

                PropertyInfo[] props = saver.Settings.GetType().GetProperties();
                foreach (PropertyInfo pinfo in props)
                {
                    object val = pinfo.GetValue(saver.Settings, null);
                    pinfo.SetValue(alg.Settings, val, null);
                }

                ScoringBehavior behavior = new ScoringBehavior(saver.BehaviorName, alg, pattern);
                this.AttachedObject = behavior;
            }
        }

        public ScoringBehaviorTranslator()
        {

        }

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            if (this.AttachedObject == null) return;

            ScoringBehavior behavior = this.AttachedObject as ScoringBehavior;

            if (behavior.Name == ScoringBehavior.DefaultBehaviorName)
            {
                return;
            }

            behaviorSaver saver = null;

            SerializationInfo passed = context.Context as SerializationInfo;
            if (passed != null)
            {
                saver = passed.GetValue("Saver", typeof(behaviorSaver)) as behaviorSaver;
            }

            if (saver == null)
                saver = new behaviorSaver();


            saver.ImpName = behavior.Pattern.Name;
            saver.AlgType = behavior.Algorithm.GetType().FullName;
            saver.Settings = behavior.Algorithm.Settings;
            saver.BehaviorName = behavior.Name;

            info.AddValue("Saver", saver);
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return new ScoringBehaviorTranslator();
        }

        #endregion
    }
}
