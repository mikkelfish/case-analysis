﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using ApolloFinal.Tools.IntervalTools.CycleScoring.Implementations.ExponentialMA;
using CeresBase.Projects;
using System.IO;
using System.Reflection;
using System.Windows;
using MesosoftCommon.Utilities.Settings;
using CeresBase.IO.Serialization;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Central
{
    public static class IntervalScoringCentral
    {
        private static ObservableCollection<ScoringBehavior> behaviors = new ObservableCollection<ScoringBehavior>();
        public static ObservableCollection<ScoringBehavior> Behaviors
        {
            get
            {
                return behaviors;
            }
        }

        private static ObservableCollection<PatternImplementation> implementations;
        public static ObservableCollection<PatternImplementation> Implementations
        {
            get
            {
                return implementations;
            }
        }

        private static ObservableCollection<IScoringAlgorithm> algorithms = new ObservableCollection<IScoringAlgorithm>();
        public static ObservableCollection<IScoringAlgorithm> Algorithms
        {
            get
            {
                return algorithms;
            }
        }

        #region Save

        //private static List<behaviorSaver> behaves = new List<behaviorSaver>();
        //private static List<impSaver> imps = new List<impSaver>();

        //private class behaviorSaver
        //{
        //    public string ImpName { get; set; }
          

        //    public ScoringSettings Settings { get; set; }
        //    public string AlgType { get; set; }

        //    public string BehaviorName { get; set; }
        //}

        //private class impSaver
        //{
        //    public string Name { get; set; }
        //    public string[] ImpRegions { get; set; }
        //    public int[] ImpTypes { get; set; }
        //}

        //private static string storagePath = Paths.DefaultPath.LocalPath + "Behaviors\\";


        //private static void saveBehaviors()
        //{
        //    #region Save Behaviors

        //    //foreach (ScoringBehavior behavior in behaviors)
        //    //{
        //    //    if(behavior.Name == ScoringBehavior.DefaultBehaviorName)
        //    //    {
        //    //        continue;
        //    //    }

        //    //    behaviorSaver saver = behaves.SingleOrDefault(b => b.BehaviorName == behavior.Name);
        //    //    if (saver == null)
        //    //    {
        //    //        saver = new behaviorSaver();
        //    //        behaves.Add(saver);
        //    //    }

             

        //    //    saver.ImpName = behavior.Pattern.Name;

        //    //    saver.AlgType = behavior.Algorithm.GetType().FullName;
        //    //    saver.Settings = behavior.Algorithm.Settings;

        //    //    saver.BehaviorName = behavior.Name;
        //    //}

        //    #endregion

        //    #region Save Implementations

        //    //foreach (PatternImplementation imp in implementations)
        //    //{
        //    //    if (imp.Name == PatternImplementation.DefaultPatternName)
        //    //        continue;

        //    //    impSaver saver = imps.SingleOrDefault(i => i.Name == imp.Name);
        //    //    if (saver == null)
        //    //    {
        //    //        saver = new impSaver();
        //    //        imps.Add(saver);
        //    //    }



        //    //    saver.Name = imp.Name;
        //    //    saver.ImpRegions = imp.IntervalNames;
        //    //    saver.ImpTypes = imp.RegionPattern;

        //    //}

        //    #endregion



        //    //if (!Directory.Exists(storagePath))
        //    //{
        //    //    Directory.CreateDirectory(storagePath);
        //    //}

        //    //using (FileStream stream = new FileStream(storagePath + "All Behaviors.xaml", FileMode.Create, FileAccess.Write))
        //    //{
        //    //    MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(behaves, stream);
        //    //}

        //    //using (FileStream stream = new FileStream(storagePath + "All Implementations.xaml", FileMode.Create, FileAccess.Write))
        //    //{
        //    //    MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(imps, stream);
        //    //}  

        //}

        //private static void loadBehaviors()
        //{
        //    //behaves = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(storagePath + "All Behaviors.xaml") as List<behaviorSaver>;
        //    //if (behaves == null) behaves = new List<behaviorSaver>();

        //    //imps = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(storagePath + "All Implementations.xaml") as List<impSaver>;
        //    //if (imps == null) imps = new List<impSaver>();


        //    //#region Create Algorithms

        //    //IScoringAlgorithm[] gotAlgs = CeresBase.General.Utilities.CreateInterfaces<IScoringAlgorithm>(null);
        //    //foreach (IScoringAlgorithm alg in gotAlgs)
        //    //{
        //    //    algorithms.Add(alg);
        //    //}

        //    //#endregion

        //    #region Create Patterns

        //    foreach (impSaver saver in imps)
        //    {
        //        PatternImplementation pattern = new PatternImplementation(saver.Name, saver.ImpRegions, saver.ImpTypes);
        //        implementations.Add(pattern);
        //    }

        //    #endregion

        //    #region Create Behaviors

        //    //foreach (behaviorSaver saver in behaves)
        //    //{
        //    //    PatternImplementation pattern = implementations.SingleOrDefault(p => p.Name == saver.ImpName);

        //    //    IScoringAlgorithm alg = algorithms.SingleOrDefault(a => a.GetType().FullName == saver.AlgType);
        //    //    if (alg != null && pattern != null)
        //    //    {
        //    //        //Create a new version
        //    //        alg = alg.CloneEmpty();

        //    //        PropertyInfo[] props = saver.Settings.GetType().GetProperties();
        //    //        foreach (PropertyInfo info in props)
        //    //        {
        //    //            object val = info.GetValue(saver.Settings, null);
        //    //            info.SetValue(alg.Settings, val, null);
        //    //        }

        //    //        ScoringBehavior behavior = new ScoringBehavior(saver.BehaviorName, alg, pattern);
        //    //        behaviors.Add(behavior);
        //    //    }
        //    //}

        //    #endregion

        //    //Add defaults
        //    //behaviors.Add(new ScoringBehavior());
        //    //implementations.Add(new PatternImplementation());
        //}

        //static void Current_Exit(object sender, ExitEventArgs e)
        //{
        //    saveBehaviors();
        //}

        //static void Application_ApplicationExit(object sender, EventArgs e)
        //{
        //    saveBehaviors();
        //}

        #endregion

        static IntervalScoringCentral()
        {

            NewSerializationCentral.DatabaseChanged += new EventHandler(SerializationCentral_DatabaseChanged);

            #region Create Algorithms

            IScoringAlgorithm[] gotAlgs = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IScoringAlgorithm>(null);
            foreach (IScoringAlgorithm alg in gotAlgs)
            {
                algorithms.Add(alg);
            }

            #endregion

            implementations = NewSerializationCentral.RegisterCollection<ObservableCollection<PatternImplementation>>(typeof(IntervalScoringCentral),
                "implementations");

            behaviors = NewSerializationCentral.RegisterCollection<ObservableCollection<ScoringBehavior>>(typeof(IntervalScoringCentral),
                "behaviors");

            if (!behaviors.Any(b => b.Name == ScoringBehavior.DefaultBehaviorName))
                behaviors.Add(new ScoringBehavior());
            if (!implementations.Any(i => i.Name == PatternImplementation.DefaultPatternName))
                implementations.Add(new PatternImplementation());
        }

        static void SerializationCentral_DatabaseChanged(object sender, EventArgs e)
        {
            if (!behaviors.Any(b => b.Name == ScoringBehavior.DefaultBehaviorName))
                behaviors.Add(new ScoringBehavior());
            if (!implementations.Any(i => i.Name == PatternImplementation.DefaultPatternName))
                implementations.Add(new PatternImplementation());
        }


        public static void CreateBehavior(ScoringBehavior behave)
        {
            if (!implementations.Any(b => b.Name == behave.Pattern.Name))
            {
                implementations.Add(behave.Pattern);
            }

            if (!behaviors.Any(b => b.Name == behave.Name))
            {
                behaviors.Add(behave);
            }

        }

        public static bool HasBeenScored(Epoch epoch)
        {
            if (epoch == null) return false;

            EpochAttachment behave = epoch.Attachments.SingleOrDefault(at => at.Name == "Scoring Behavior");
            EpochAttachment marks = epoch.Attachments.SingleOrDefault(at => at.Name == "Interval Marks");

            return behave != null && marks != null;
        }

        public static void SaveToEpoch(Epoch epoch, ScoringBehavior behavior)
        {
            //if (this.curEpoch == null || this.Behavior == null || this.Behavior.Pattern == null ||
            //    this.Behavior.Pattern.AllMarks.Count == 0)
            //    return;

            EpochAttachment behave = epoch.Attachments.SingleOrDefault(at => at.Name == "Scoring Behavior");
            EpochAttachment marks = epoch.Attachments.SingleOrDefault(at => at.Name == "Interval Marks");

            if (behave == null)
            {
                behave = new EpochAttachment() { Name = "Scoring Behavior", AllowMultiple = false };
                epoch.AddAttachment(behave);
            }
            if (marks == null)
            {
                marks = new EpochAttachment() { Name = "Interval Marks", AllowMultiple = false };
                epoch.AddAttachment(marks);
            }

            behave.Item = behavior.Name;
            marks.Item = behavior.Pattern.AllMarks.ToArray();

            if (behavior.Pattern.AllMarks.Any(m => m.Time < epoch.BeginTime || m.Time > epoch.EndTime))
                throw new Exception("Error");
        }

        public static void SaveAlgorithmSettings(ScoringBehavior behavior, ScoringSettings newSettings)
        {
            ScoringBehavior behave = behaviors.SingleOrDefault(b => b.Name == behavior.Name );
            if (behave == null) return;

            PropertyInfo[] props = newSettings.GetType().GetProperties();
            foreach (PropertyInfo info in props)
            {
                object val = info.GetValue(newSettings, null);
                info.SetValue(behave.Algorithm.Settings, val, null);

                info.SetValue(behavior.Algorithm.Settings, val, null);
            }

        }

        public static ScoringBehavior LoadBehaviorFromEpoch(Epoch epoch)
        {
            if (epoch == null) return null;

            EpochAttachment behavior = epoch.Attachments.SingleOrDefault(at => at.Name == "Scoring Behavior");
            EpochAttachment marks = epoch.Attachments.SingleOrDefault(at => at.Name == "Interval Marks");

            if (behavior == null || marks == null) return null;

            ScoringBehavior behave = Behaviors.SingleOrDefault(b => b.Name == behavior.Item as string);
            if (behave == null)
            {
                MessageBox.Show("Scoring behavior " + behavior.Item + " does not exist. Create it and try again");
                return null;
            }

            behave = behave.CloneEmpty();

           // behave.Pattern.RefrainFromMarking = true;

            behave.Pattern.BulkChangesInProgress = true;

            behave.Pattern.AllMarks.Clear();
            behave.Epoch = epoch;

            Mark[] markArray = marks.Item as Mark[];

            if (markArray != null)
            {
                for (int i = 0; i < markArray.Length; i++)
                {
                    Mark newMark = new Mark(behave.Pattern, markArray[i].Time, markArray[i].ID, markArray[i].Length);
                    behave.Pattern.AllMarks.Add(newMark);
                }

                if (behave.Pattern.AllMarks.Any(m => m.Time < epoch.BeginTime || m.Time > epoch.EndTime))
                    throw new Exception("Error");

            }

            //for (int i = 0; i < markArray.Length; i++)
            //{
            //    try
            //    {
            //        behave.Pattern.AllMarks[i].Length = markArray[i].Length;
            //    }
            //    catch (Exception ex)
            //    {
            //        int s = 0;
            //    }

            //}


           // behave.Pattern.RefrainFromMarking = false;
            behave.Pattern.BulkChangesInProgress = false;



            return behave;
        }
    }
}
