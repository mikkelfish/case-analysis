﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;
using System.Runtime.Serialization;
using DatabaseUtilityLibrary;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring.Central
{
    class PatternImplementationTranslator : ITranslator
    {
        [AlwaysSerialize]
        private class impSaver
        {
            public string Name { get; set; }
            public string[] ImpRegions { get; set; }
            public int[] ImpTypes { get; set; }
        }



        #region ITranslator Members

        public object AttachedObject
        {
            get;
            set;
        }

        public bool SupportsType(Type t)
        {
            if (t == typeof(PatternImplementation)) return true;
            return false;
        }

        public double Version
        {
            get { return 0.0; }
        }

        public bool Supports(object obj)
        {
            return (obj is PatternImplementation);
        }

        #endregion

        protected PatternImplementationTranslator(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            if (info.MemberCount == 0) return;

            impSaver saver = info.GetValue("Saver", typeof(impSaver)) as impSaver;
            PatternImplementation pattern = new PatternImplementation(saver.Name, saver.ImpRegions, saver.ImpTypes);
            this.AttachedObject = pattern;
        }

        public PatternImplementationTranslator()
        {

        }

        #region ISerializable Members

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            if (this.AttachedObject == null) return;

            PatternImplementation pattern = this.AttachedObject as PatternImplementation;

            if (pattern.Name == PatternImplementation.DefaultPatternName)
            {
                return;
            }

            impSaver saver = null;

            SerializationInfo passed = context.Context as SerializationInfo;
            if (passed != null)
            {
                saver = passed.GetValue("Saver", typeof(impSaver)) as impSaver;
            }

            if (saver == null)
                saver = new impSaver();

            saver.Name = pattern.Name;
            saver.ImpRegions = pattern.IntervalNames;
            saver.ImpTypes = pattern.RegionPattern;

            info.AddValue("Saver", saver);


        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return new PatternImplementationTranslator();
        }

        #endregion
    }
}
