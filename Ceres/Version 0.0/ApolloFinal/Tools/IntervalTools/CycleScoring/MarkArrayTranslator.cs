﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.Serialization;
using CeresBase.IO.Serialization.Translation;
using DatabaseUtilityLibrary;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    public class MarkArrayTranslator : ITranslator
    {
        #region ITranslator Members

        public object AttachedObject
        {
            get;
            set;
        }

        public bool SupportsType(Type t)
        {
            return t == typeof(Mark[]);
        }

        public double Version
        {
            get { return 0.0; }
        }

        public bool Supports(object obj)
        {
            if (obj is Mark[]) return true;
            return false;
        }

        #endregion

        [AlwaysSerialize]
        private class markArrayInfo
        {
            public float[] Times { get; set; }
            public float[] Lengths { get; set; }
            public int[] IDs { get; set; }
        }

        public MarkArrayTranslator()
        {

        }

        protected MarkArrayTranslator(SerializationInfo info, StreamingContext context)
        {
            if (info.MemberCount == 0) return;

            markArrayInfo entry = info.GetValue("marray", typeof(markArrayInfo)) as markArrayInfo;
            if (entry != null && entry.Lengths != null)
            {
                Mark[] toRet = new Mark[entry.Lengths.Length];
                for (int i = 0; i < toRet.Length; i++)
                {
                    Mark newMark = new Mark();
                    newMark.Length = entry.Lengths[i];
                    newMark.Time = entry.Times[i];
                    newMark.ID = entry.IDs[i];
                    toRet[i] = newMark;
                }

                this.AttachedObject = toRet;
            }

        }

        #region ISerializable Members

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            markArrayInfo entry = null;

            SerializationInfo passed = context.Context as SerializationInfo;
            if (passed != null)
            {
                entry = passed.GetValue("marray", typeof(markArrayInfo)) as markArrayInfo;
            }

            Mark[] array = this.AttachedObject as Mark[];

            if (entry == null)
            {
                entry = new markArrayInfo();
            }


            entry.Times = array.Select(e => e.Time).ToArray();
            entry.IDs = array.Select(e => e.ID).ToArray();
            entry.Lengths = array.Select(e => e.Length).ToArray();

            info.AddValue("marray", entry);
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return new MarkArrayTranslator();
        }

        #endregion
    }
}
