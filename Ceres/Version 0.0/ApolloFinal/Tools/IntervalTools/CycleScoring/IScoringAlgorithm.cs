﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects;

namespace ApolloFinal.Tools.IntervalTools.CycleScoring
{
    public interface IScoringAlgorithm
    {
        ScoringSettings Settings
        {
            get;
        }

        string[] RegionTypes
        {
            get;
        }

        CycleSeries Score(Epoch epoch);
        CycleSeries Score(Epoch epoch, float[] data, double res);


        CycleSeries Score(Epoch epoch, ScoringSettings passedSettings);

        CycleSeries Score(Epoch epoch, float[] data, double res, ScoringSettings passedSettings);


        IScoringAlgorithm CloneEmpty();

    }
}
