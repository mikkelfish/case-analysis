﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects;
using System.Windows;
using MesosoftCommon.Utilities.Settings;
using System.IO;
using CeresBase.IO.Serialization;

namespace ApolloFinal.Tools.IntervalTools
{
    public static class StatisticsOutputCentral
    {
        private static Dictionary<string, List<int>> selectedIntervals =
            NewSerializationCentral.RegisterCollection<Dictionary<string, List<int>>>(typeof(StatisticsOutputCentral), "selectedIntervals"); // new Dictionary<string, int[]>();



        public static  void SetSelected(IntervalPackage package, int[] toSelect)
        {
            if (!selectedIntervals.ContainsKey(package.PackageTitle))
            {
                selectedIntervals.Add(package.PackageTitle, null);

            }

            IntervalEntry[] entries = package.GetAsEntries();
            List<int> toUnselect = new List<int>();
            int curSel = 0;
            for (int i = 0; i < entries.Length; i++)
			{
                if (curSel == toSelect.Length)
                {
                    toUnselect.Add(entries[i].IntervalNumber);
                }
                else
                {

                    if (toSelect[curSel] != entries[i].IntervalNumber)
                    {
                        toUnselect.Add(entries[i].IntervalNumber);
                    }
                    else
                    {
                        curSel++;
                    }
                }
			}

            selectedIntervals[package.PackageTitle] = toUnselect;

        }

        public static int[] GetUnSelected(string package)
        {
            if (!selectedIntervals.ContainsKey(package))
                return null;

            return selectedIntervals[package].ToArray();
        }

        //static void Current_Exit(object sender, ExitEventArgs e)
        //{
        //    save();
        //}

        //static void Application_ApplicationExit(object sender, EventArgs e)
        //{
        //    save();
        //}

        //private static string storagePath = Paths.DefaultPath.LocalPath + "Intervals\\";


        //private static void save()
        //{
        //    if (!Directory.Exists(storagePath))
        //    {
        //        Directory.CreateDirectory(storagePath);
        //    }

        //    using (FileStream stream = new FileStream(storagePath + "SelectedIntervals.xaml", FileMode.Create, FileAccess.Write))
        //    {
        //        MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(selectedIntervals, stream);
        //    }

        //}

        //private static void load()
        //{
        //    selectedIntervals = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(storagePath + "SelectedIntervals.xaml") as Dictionary<string, int[]>;
        //    if (selectedIntervals == null) selectedIntervals = new Dictionary<string, int[]>();
        //}

        //static StatisticsOutputCentral()
        //{
        //    load();

        //    if (Application.Current == null)
        //        System.Windows.Forms.Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
        //    else
        //        Application.Current.Exit += new ExitEventHandler(Current_Exit);
        //}


    }
}
