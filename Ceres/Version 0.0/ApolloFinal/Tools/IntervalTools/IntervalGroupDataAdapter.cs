﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;
using CeresBase.Data;

namespace ApolloFinal.Tools.IntervalTools
{
    [AdapterDescription("Interval")]
    class IntervalGroupDataAdapter : DataAdapter
    {
        private IntervalPackage package;
        public IntervalPackage Package
        {
            get
            {
                return this.package;
            }
            set
            {
                this.package = value;
            }
        }

        [AssociateWithProperty("Intervals")]
        public float[][] Intervals
        {
            get
            {
                if (this.Package == null) return null;
                return this.Package.GetAllIntervalLengths();
            }
        }

        [AssociateWithProperty("Start Times")]
        public float[] StartTimes
        {
            get
            {
                if (this.Package == null) return null;
                return this.Package.BeginTimes;
            }
        }

        [AssociateWithProperty("End Times")]
        public float[] EndTimes
        {
            get
            {
                if (this.Package == null) return null;
                return this.Package.EndTimes;
            }
        }

        [AssociateWithProperty("Variable Names")]
        public string[] VariableNames
        {
            get
            {
                if (this.Package == null) return null;
                return this.package.IntervalNames;
            }
        }

        [AssociateWithProperty("Description")]
        public string Description
        {
            get
            {
                if (this.Package == null) return null;
                return this.Package.PackageTitle;
            }
        }

        private float[] varData;
        [AssociateWithProperty("Raw Data")]
        public float[] VariableData
        {
            get
            {
                if (this.Package == null) return null;

                if (varData == null)
                {
                    float[][] timings = this.package.GetAllIntervalTimings();
                    if(this.package.DataVariable != null)
                    varData =this.Package.DataVariable.GetTimeSeriesData(timings[0][0], this.package.EndTimes[this.package.EndTimes.Length - 1]);
                }

                return varData;
            }
        }

        [AssociateWithProperty("Raw Resolution")]
        public double DataResolution
        {
            get
            {
                if (this.Package == null) return 0;
                if (this.package.DataVariable == null) return 0;
                return this.Package.DataVariable.Resolution;
            }
        }

        //public override string[] SupplyPropertyNames()
        //{
        //    List<string> toRet = new List<string>();
        //    toRet.Add("Intervals");
        //    toRet.Add("Start Times");
        //    toRet.Add("End Times");
        //    toRet.Add("Variable Names");
        //    toRet.Add("Description");

        //    string[] names = base.SupplyPropertyNames();
        //    if (names != null)
        //    {
        //        toRet.AddRange(names);
        //    }

        //    return toRet.ToArray();
        //}

        //public override bool CanEditProperty(string targetPropertyname)
        //{
        //    if (targetPropertyname == "Intervals" || targetPropertyname == "Start Times" ||
        //        targetPropertyname == "End Times" || targetPropertyname == "Variable Names" ||
        //        targetPropertyname == "Description")
        //    {
        //        return false;
        //    }

        //    return base.CanEditProperty(targetPropertyname);
        //}

        //public override bool CanReceivePropertyLinks(string targetPropertyname)
        //{
        //    if (targetPropertyname == "Intervals" || targetPropertyname == "Start Times" ||
        //        targetPropertyname == "End Times" || targetPropertyname == "Variable Names" ||
        //        targetPropertyname == "Description")
        //    {
        //        return false;
        //    }
        //    return base.CanReceivePropertyLinks(targetPropertyname);
        //}

        //public override bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        //{
        //    return true;

        //    //  return base.CanReceivePropertyValue(propertyValue, targetPropertyName);
        //}

        //public override object[] GetValuesForSerialization()
        //{
        //    return base.GetValuesForSerialization();
        //}

        //public override void LoadValuesFromSerialization(object[] values)
        //{
        //    base.LoadValuesFromSerialization(values);
        //}

        //public override bool ShouldSerialize(string targetPropertyname)
        //{
        //    if (targetPropertyname == "Intervals" || targetPropertyname == "Start Times" ||
        //        targetPropertyname == "End Times" || targetPropertyname == "Variable Names" ||
        //        targetPropertyname == "Description")
        //    {
        //        return true;
        //    }
        //    return base.ShouldSerialize(targetPropertyname);
        //}

        //public override object SupplyPropertyValue(string sourcePropertyName)
        //{
        //    if (this.Package == null) return null;

        //    if (sourcePropertyName == "Intervals")
        //    {
        //        return this.Package.GetAllIntervalLengths();
        //    }

        //    if (sourcePropertyName == "Start Times")
        //    {
        //        return this.Package.BeginTimes;
        //    }

        //    if (sourcePropertyName == "End Times")
        //    {
        //        return this.Package.EndTimes;
        //    }

        //    if (sourcePropertyName == "Variable Names")
        //    {
        //        return this.Package.IntervalNames;
        //    }

        //    if (sourcePropertyName == "Description")
        //    {
        //        return this.package.PackageTitle;
        //    }

        //    return base.SupplyPropertyValue(sourcePropertyName);
        //}

        //public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        //{
        //    if (this.Package == null) return;
        //    base.ReceivePropertyValue(propertyValue, targetPropertyName);
        //}



        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public override object Clone()
        {
            IntervalGroupDataAdapter adapter = new IntervalGroupDataAdapter();
            adapter.Package = this.Package;

            this.cloneHelper(adapter);
            return adapter;
        }

        public override string Name
        {
            get
            {
                return "Interval Group Data Adapter";
            }
            set
            {

            }
        }

        public override string Category
        {
            get
            {
                return "Data Sources";
            }
            set
            {

            }
        }
    }
}
