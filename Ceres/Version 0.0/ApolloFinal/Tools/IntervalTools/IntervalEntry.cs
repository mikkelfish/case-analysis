﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApolloFinal.Tools.IntervalTools
{
    public class IntervalEntry
    {
        public int IntervalNumber { get; private set; }
        public string[] IntervalNames { get; private set; }
        public float[] IntervalTimes { get; private set; }
        public float BaseTime { get; private set; }
        public string Title { get; set; }

        public IntervalEntry(float baseTime, int intNumber, string[] intervalNames, float[] intervalTimes)
        {
            this.BaseTime = baseTime;
            this.IntervalNumber = intNumber;
            this.IntervalTimes = intervalTimes;
            this.IntervalNames = intervalNames;
        }

        public override string ToString()
        {
            return this.IntervalNumber.ToString();
        }
    }
}
