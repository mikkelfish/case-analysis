﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects;
using ApolloFinal.Tools.IntervalTools.CycleScoring;
using ApolloFinal.Tools.IntervalTools.CycleScoring.Central;
using System.Windows;
using System.Windows.Controls;

namespace ApolloFinal.Tools.IntervalTools
{
    public class IntervalPackage : IComparable
    {
        public static IntervalPackage SelectIntervals(IntervalPackage package)
        {
            System.Windows.Window win = new System.Windows.Window();
            win.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            StatisticsOutputControl outputControl = new StatisticsOutputControl();
            outputControl.Package = package;

            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(win);
            win.Content = new Grid();
            (win.Content as Grid).Children.Add(outputControl);
            win.Title = "Select Intervals: " + package.PackageTitle;

            //CeresBase.UI.ContentBox box = new CeresBase.UI.ContentBox();
            //box.ContentElement = new Grid();
            //(box.ContentElement as Grid).Children.Add(outputControl);
            //box.Height = 500;
            //box.Width = 800;
            //box.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //box.ShowDialog();

            win.Height = 500 + 32;
            win.Width = 800 + 8;
            outputControl.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            outputControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            if (package.PackageTitle != null) win.Title = "View Intervals";
            win.ShowDialog();

            if (outputControl.Result != null)
            {                
                StatisticsOutputCentral.SetSelected(package, outputControl.Result.GetAsEntries().Select(se => se.IntervalNumber).ToArray());
                return outputControl.Result;
            }

            return null;
        }


        public static IntervalPackage[] CreatePackages(Epoch[] epochs, bool alwaysSelect)
        {
            List<IntervalPackage> packs = new List<IntervalPackage>();
            foreach (Epoch e in epochs)
            {
                if ((e.Variable as SpikeVariable).SpikeType == SpikeVariableHeader.SpikeType.Pulse)
                {
                    IntervalPackage pack = new IntervalPackage(new SpikeVariable[] { e.Variable as SpikeVariable }, new float[] { e.BeginTime }, new float[] { e.EndTime });
                    pack.PackageTitle = e.ToString();
                    packs.Add(pack);
                }
                else
                {
                    if (!IntervalScoringCentral.HasBeenScored(e))
                    {
                        CycleScoringWindow window = new CycleScoringWindow();


                        foreach (Epoch e2 in epochs)
                        {
                            window.PossibleEpochs.Add(e2);
                        }

                        window.CurrentEpoch = e;


                        window.WindowState = System.Windows.WindowState.Maximized;
                        if (!window.ShowDialog().Value)
                            continue;
                    }

                        IntervalPackage pack = new IntervalPackage(e);
                        pack.PackageTitle = e.ToString();
                        pack.DataVariable = e.Variable as SpikeVariable;
                        packs.Add(pack);
                   
                }
            }

            List<IntervalPackage> toRet = new List<IntervalPackage>();

            foreach (IntervalPackage pack in packs)
            {
                int[] unselected = StatisticsOutputCentral.GetUnSelected(pack.PackageTitle);

                if (!alwaysSelect && unselected != null)
                {
                    IntervalEntry[] entries = pack.GetAsEntries().Where(en => !unselected.Contains(en.IntervalNumber)).OrderBy(en => en.IntervalNumber).ToArray();
                    toRet.Add(new IntervalPackage(entries) { DataVariable = pack.DataVariable });
                }
                else
                {
                    IntervalPackage sepack = SelectIntervals(pack);
                    sepack.DataVariable = pack.DataVariable;
                    if (sepack != null)
                    {
                        toRet.Add(sepack);
                    }
                }
            }

            return toRet.ToArray();
        }



        public SpikeVariable DataVariable { get; private set; }
        //public SpikeVariable[] IntervalVariables { get; private set; }

        public float[] BeginTimes { get; private set; }
        public float[] EndTimes { get; private set; }

        private int[] entryNumbers;


        public string PackageTitle { get; set; }


        private float[][] intervalTimings;
        private Dictionary<string, float[]> intervalLengths;

        private string[] intervalNames;
        public string[] IntervalNames
        {
            get
            {
                return this.intervalNames;
            }
            private set
            {
                this.intervalNames = value;
            }
        }

        public override string ToString()
        {
            if (this.IntervalNames == null) return "Nothing";
            string baseString = "Intervals: ";
            if (this.PackageTitle != null)
            {
                baseString = this.PackageTitle + " " + baseString;
            }
            if(this.BeginTimes!= null && this.EndTimes != null)
                baseString += this.BeginTimes[0].ToString() + " - " + this.EndTimes[0].ToString();
            return baseString;
        }

        public IntervalPackage(Epoch e)
        {
            if (!IntervalScoringCentral.HasBeenScored(e)) throw new ArgumentException("This epoch has not been scored");


            ScoringBehavior behave = IntervalScoringCentral.LoadBehaviorFromEpoch(e);

            if (behave == null)
            {

                throw new InvalidOperationException("The behavior does not exist");
            }

            this.DataVariable = e.Variable as SpikeVariable;

            if (behave.Pattern == null || behave.Pattern.IntervalNames == null) return;
            int totalIntervals = behave.Pattern.IntervalNames.Length;
            //if (totalIntervals > 1)
            //{
            //    totalIntervals++;
            //}
            totalIntervals++;


            this.IntervalNames = new string[totalIntervals];
            this.BeginTimes = new float[this.intervalNames.Length];
            this.EndTimes = new float[this.intervalNames.Length];

            for (int i = 0; i < this.BeginTimes.Length; i++)
            {
                this.BeginTimes[i] = e.BeginTime;
                this.EndTimes[i] = e.EndTime;
            }

            List<float> totalLengths = new List<float>();

            this.intervalLengths = new Dictionary<string, float[]>();


            this.intervalTimings = new float[totalIntervals][];
            for (int i = 0; i < behave.Pattern.IntervalNames.Length; i++)
            {
                this.intervalNames[i] = behave.Pattern.IntervalNames[i];
                this.intervalLengths.Add(this.intervalNames[i], behave.Pattern.IntervalLengths[i]);
                this.intervalTimings[i] = behave.Pattern.IntervalTimes[i];
            }

            //if(totalIntervals > 1)
                this.IntervalNames[this.IntervalNames.Length - 1] = "Total";

            for (int i = 0; i < behave.Pattern.IntervalTimes[0].Length; i++)
            {
                float total = 0;
                for (int j = 0; j < behave.Pattern.IntervalNames.Length; j++)
                {
                    total += behave.Pattern.IntervalLengths[j][i];
                }
                totalLengths.Add(total);
            }

            //if (totalIntervals > 1)
            //{
                this.intervalLengths.Add("Total", totalLengths.ToArray());
                this.intervalTimings[this.intervalTimings.Length - 1] = behave.Pattern.IntervalTimes[0];
            //}
        }

        public IntervalPackage(IntervalEntry[] entries)
        {
            if (entries == null || entries.Length == 0) return;

            entries = entries.OrderBy(e => e.IntervalNumber).ToArray();

            this.entryNumbers = entries.Select(e => e.IntervalNumber).ToArray();
            this.IntervalNames = entries[0].IntervalNames;
            this.intervalLengths = new Dictionary<string, float[]>();

            this.PackageTitle = entries[0].Title;

            for (int i = 0; i < this.IntervalNames.Length; i++)
            {
                List<float> data = new List<float>();
                for (int j = 0; j < entries.Length; j++)
                {
                    data.Add(entries[j].IntervalTimes[i]);
                }
                this.intervalLengths.Add(this.IntervalNames[i], data.ToArray());
            }

            int timingCount = this.intervalNames.Length;
            if (timingCount == 2) timingCount--;

            List<float>[] timings = new List<float>[timingCount];
            for (int i = 0; i < timings.Length; i++)
            {
                timings[i] = new List<float>();
            }

            for (int i = 0; i < entries.Length; i++)
            {
                float start = entries[i].BaseTime;
                for (int j = 0; j < timingCount; j++)
                {
                    timings[j].Add(start);
                    start += entries[i].IntervalTimes[j];
                }
            }
            this.intervalTimings = new float[timings.Length][];
            for (int i = 0; i < this.intervalTimings.Length; i++)
            {
                this.intervalTimings[i] = timings[i].ToArray();
            }

            this.BeginTimes = new float[this.intervalNames.Length];
            this.EndTimes = new float[this.intervalNames.Length];

            for (int i = 0; i < this.BeginTimes.Length; i++)
            {
                this.BeginTimes[i] = entries[0].BaseTime;
                int toRead = this.BeginTimes.Length  -1;
                this.EndTimes[i] = entries[entries.Length - 1].BaseTime + entries[entries.Length - 1].IntervalTimes[toRead];
            }
        }

        public IntervalPackage(SpikeVariable[] intervalVariables, float[] beginTime, float[] endTime) : this(intervalVariables, null, beginTime, endTime)
        {

        }

        public IntervalPackage(SpikeVariable[] intervalVariablesPassed, SpikeVariable dataVariable, float[] beginTime, float[] endTime)
        {
            SpikeVariable[] intervalVariables = intervalVariablesPassed.Clone() as SpikeVariable[];

            this.DataVariable = dataVariable;
            this.BeginTimes = beginTime;
            this.EndTimes = endTime;


            List<string> toRet = intervalVariables.Select(interval => interval.Header.Description).ToList();
            
            if(toRet.Count > 1) toRet.Add("Total");
            this.intervalNames = toRet.ToArray();



            float[] firstTimes = new float[intervalVariables.Length];
            for (int i = 0; i < intervalVariables.Length; i++)
            {
                bool first = true;
                intervalVariables[i][0].Pool.GetData(null, null,
                    delegate(float[] data, uint[] indices, uint[] amounts)
                    {
                        if (first)
                        {
                            firstTimes[i] = data[0] * (float)intervalVariables[i].Resolution;
                            first = false;
                        }
                    }
                );
            }

            int[] order = new int[intervalVariables.Length];
            for (int i = 0; i < order.Length; i++)
            {
                order[i] = i;
            }

            for (int i = 0; i < order.Length; i++)
            {
                for (int j = i + 1; j < order.Length; j++)
                {
                    if (firstTimes[i] > firstTimes[j])
                    {
                        int temp = order[i];
                        order[i] = order[j];
                        order[j] = temp;
                    }
                }
            }

            for (int i = 0; i < order.Length; i++)
            {
                int spot = order[i];
                if (spot < i) continue;

                SpikeVariable tempVar = intervalVariables[spot];
                intervalVariables[spot] = intervalVariables[i];
                intervalVariables[i] = tempVar;

            }


            this.intervalTimings = new float[intervalVariables.Length][];
            for (int i = 0; i < this.intervalTimings.Length; i++)
            {
                List<float> dataL = new List<float>();
                bool oneMore = false;
                intervalVariables[i][0].Pool.GetData(null, null,
                    delegate(float[] data, uint[] indices, uint[] counts)
                    {
                        for (int j = 0; j < data.Length; j++)
                        {
                            float time = data[j] * (float)intervalVariables[i].Resolution;
                            if (time < this.BeginTimes[i]) continue;
                            if (time > this.EndTimes[i])
                            {
                                if (oneMore) break;
                                else oneMore = true;
                            }
                            dataL.Add(time);
                        }
                    }
                );

                this.intervalTimings[i] = dataL.ToArray();
            }

            float totFirst = this.intervalTimings[0][0];
            float totLast = this.intervalTimings[this.intervalTimings.Length - 1][this.intervalTimings[this.intervalTimings.Length - 1].Length - 1];
            for (int i = 0; i < intervalVariables.Length; i++)
            {
                List<float> dataL = new List<float>();
                for (int j = 0; j < this.intervalTimings[i].Length; j++)
                {
                    if (this.intervalTimings[i][j] >= totFirst && this.intervalTimings[i][j] <= totLast)
                    {
                        dataL.Add(this.intervalTimings[i][j]);
                    }
                }
                this.intervalTimings[i] = dataL.ToArray();
            }


            List<float>[] intervals = new List<float>[this.intervalTimings.Length];
            for (int i = 0; i < this.intervalTimings[0].Length - 1; i++)
            {
                for (int j = 0; j < this.intervalTimings.Length; j++)
                {
                    if (i == 0)
                        intervals[j] = new List<float>();

                    float diff = 0F;
                    if (j != this.intervalTimings.Length - 1)
                    {
                        diff = intervalTimings[j + 1][i] -
                            intervalTimings[j][i];
                    }
                    else if (i + 1 < this.intervalTimings[0].Length)
                    {
                        diff = intervalTimings[0][i + 1] -
                            intervalTimings[j][i];
                    }

                    intervals[j].Add(diff);
                }
            }

            this.intervalLengths = new Dictionary<string, float[]>();
            for (int i = 0; i < intervals.Length; i++)
            {
                this.intervalLengths.Add(this.IntervalNames[i], intervals[i].ToArray());
            }

            if (this.intervalNames.Length > 1)
            {
                this.intervalLengths.Add("Total", new float[intervals[0].Count]);

                for (int i = 0; i < this.intervalLengths["Total"].Length; i++)
                {
                    float total = 0;
                    for (int j = 0; j < intervals.Length; j++)
                    {
                        total += intervals[j][i];
                    }
                    this.intervalLengths["Total"][i] = total;
                }
            }
        }

        public float[] GetIntervalLength(string variable)
        {
            return this.intervalLengths[variable];
        }



        public float[][] GetAllIntervalLengths()
        {
            List<float[]> data = new List<float[]>();
            foreach (string key in this.intervalLengths.Keys)
            {
                data.Add(this.intervalLengths[key]);
            }
            return data.ToArray();
        }

        public float[][] GetAllIntervalTimings()
        {
            return this.intervalTimings;
        }

        public IntervalEntry[] GetAsEntries()
        {
            List<IntervalEntry> toRet = new List<IntervalEntry>();
            for (int i = 0; i < this.intervalLengths[this.IntervalNames[0]].Length; i++)
            {
                float[] times = new float[this.IntervalNames.Length]; ;
                for (int j = 0; j < this.IntervalNames.Length; j++)
                {
                    times[j] = this.intervalLengths[this.IntervalNames[j]][i];
                }

                int entryNum = i + 1;
                if (this.entryNumbers != null)
                {
                    entryNum = this.entryNumbers[i];
                }

                IntervalEntry entry = new IntervalEntry(this.intervalTimings[0][i], entryNum, this.IntervalNames, times);
                entry.Title = this.PackageTitle;
                toRet.Add(entry);
            }

            return toRet.ToArray();
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj == null) return 0;
            return this.ToString().CompareTo(obj.ToString());
        }

        #endregion
    }
}
