﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Data;
using CeresBase.Data;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using System.Windows.Controls;
using CeresBase.Projects;
using CeresBase.FlowControl.Adapters.Matlab;
using MesosoftCommon.DataStructures;
using ApolloFinal.Tools.IntervalTools.CycleScoring.Central;
using ApolloFinal.Tools.IntervalTools.CycleScoring;

namespace ApolloFinal.Tools.IntervalTools
{
    public class IntervalRoutineController : IRoutineController
    {
        private class EpochPackage : IComparable
        {
            public Epoch Epoch { get; set; }

            public override string ToString()
            {
                if (this.Epoch == null) return "ERROR";
                return this.Epoch.ToString();
            }

            //public override bool Equals(object obj)
            //{
            //    if (obj == null || !(obj is EpochPackage)) return false;

            //    EpochPackage other = obj as EpochPackage;
            //    return other.ToString() == this.ToString();
            //}

            //public override int GetHashCode()
            //{
            //    return this.ToString().GetHashCode();
            //}

            #region IComparable Members

            public int CompareTo(object obj)
            {
                if (obj == null) return 0;
                return this.ToString().CompareTo(obj.ToString());
            }

            #endregion
        }


        public BeginInvokeObservableCollection<Epoch> EpochList
        {
            get
            {
                return EpochCentral.BindToManager(this.routineUI.Dispatcher);
            }
        }

        public ObservableCollection<Variable> VariableList
        {
            get
            {
                return VariableSource.Variables;
            }
        }

        public IntervalRoutineController(RoutineBatchControl control)
        {
            this.BatchControl = control;
        }

        #region IRoutineController Members

        private IntervalRoutineControlOverride routineUI;

        public System.Windows.FrameworkElement GetContentOverride()
        {
            this.routineUI = new IntervalRoutineControlOverride();
            CollectionViewSource source = new CollectionViewSource();
            source.Source = this.VariableList;
            source.Filter += new FilterEventHandler(source_Filter);
            this.routineUI.VariableListDisplay.ItemsSource = source.View;

            this.routineUI.ViewIntervalsButton.Click += new RoutedEventHandler(ViewIntervalsButton_Click);

            CollectionViewSource epochSource = new CollectionViewSource();
            epochSource.Source = EpochCentral.BindToManager(this.routineUI.Dispatcher);
            epochSource.Filter += new FilterEventHandler(epochSource_Filter);
            this.routineUI.EpochSelect.ItemsSource = epochSource.View;
            this.routineUI.EpochSelect.SelectionChanged += new SelectionChangedEventHandler(EpochSelect_SelectionChanged);

            this.routineUI.ViewEpochIntervals.Click += new RoutedEventHandler(ViewIntervalsButton_Click);

            this.routineUI.EpochListDisplay.ItemsSource = epochSource.View;            
            

            return this.routineUI.Content as FrameworkElement;
        }

        void epochSource_Filter(object sender, FilterEventArgs e)
        {
            Epoch epoch = e.Item as Epoch;
            if (epoch.BeginTime == epoch.EndTime || epoch.AppliesToExperiment)
            {
                e.Accepted = false;
            }
            else e.Accepted = true;
        }

        void EpochSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Epoch epoch = (sender as ComboBox).SelectedItem as Epoch;
            if (epoch == null) return;
            this.routineUI.VariableBeginTime.Text = epoch.BeginTime.ToString();
            this.routineUI.VariableEndTime.Text = epoch.EndTime.ToString();
        }

        void ViewIntervalsButton_Click(object sender, RoutedEventArgs e)
        {
            IntervalPackage[] packages = this.createPackages(true);
            if (packages == null) return;


            foreach (IntervalPackage package in packages)
            {
                    this.BatchControl.ToRunList.Add(package);
            }
        }

     
        void source_Filter(object sender, FilterEventArgs e)
        {
            SpikeVariable var = e.Item as SpikeVariable;
            if (var.SpikeType != SpikeVariableHeader.SpikeType.Pulse)
            {
                e.Accepted = false;
            }
            else e.Accepted = true;
        }

        public void AdaptersInitialized(Routine routine, AdapterInitPair[] pairs)
        {
            var sortedAdapters = from AdapterInitPair p in pairs
                                 orderby p.Adapter.ID
                                 select p;
            foreach (AdapterInitPair adapter in sortedAdapters)
            {
                if (adapter.Adapter is IntervalGroupDataAdapter)
                {
                    IntervalGroupDataAdapter inter = adapter.Adapter as IntervalGroupDataAdapter;
                    inter.Package = routine.Args as IntervalPackage;
                }
            }
        }

        public void AddAvailableAdapters(RoutineManager rm)
        {
            MatlabLoader.BuildAdapterInfos("Interval");

            MatlabAdapter[] matlabs = MatlabLoader.CreateAdapters("Interval");
            if (matlabs != null)
            {
                foreach (MatlabAdapter mlab in matlabs)
                {
                    rm.AvailableAdapters.Add(mlab);
                }
            }

            IFlowControlAdapter[] adapters = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IFlowControlAdapter>(null);
            foreach (IFlowControlAdapter adapter in adapters)
            {
                AdapterDescriptionAttribute[] attr = MesosoftCommon.Utilities.Reflection.Common.GetAttributes<AdapterDescriptionAttribute>(adapter);
                if (attr == null) continue;
                if (attr.Any(at => at.Description == "Interval") && !rm.AvailableAdapters.Any(ad => ad.Name == adapter.Name && ad.Category == adapter.Category))
                {
                    rm.AvailableAdapters.Add(adapter);
                }
            }

            IBatchFlowable[] flows = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IBatchFlowable>(null);
            foreach (IBatchFlowable adapter in flows)
            {
                AdapterDescriptionAttribute[] attr = MesosoftCommon.Utilities.Reflection.Common.GetAttributes<AdapterDescriptionAttribute>(adapter);
                if (attr == null) continue;
                if (attr.Any(at => at.Description == "Interval") && !rm.AvailableAdapters.Any(ad => ad.Name == adapter.ToString() && ad.Category == adapter.ToString()))
                {
                    rm.AvailableAdapters.Add(new FlowableBatchProcessorAdapter(adapter));
                }
            }
        }

        private IntervalPackage[] createPackages(bool alwaysSelect)
        {
            List<IntervalPackage> toRet = new List<IntervalPackage>();
            if (this.routineUI.EpochsTab.IsSelected)
            {
                Epoch[] epochs = this.routineUI.EpochListDisplay.SelectedItems.Cast<Epoch>().ToArray();

                IntervalPackage[] packs = IntervalPackage.CreatePackages(epochs, alwaysSelect);
                if (packs != null)
                {
                    toRet.AddRange(packs);
                }
            }
            else if (this.routineUI.VariableTab.IsSelected)
            {
                #region Run by variable
                if (this.routineUI.VariableListDisplay.SelectedItems.Count == 0) return null;

                SpikeVariable firstVar = this.routineUI.VariableListDisplay.SelectedItems[0] as SpikeVariable;
                int firstAmount = firstVar[0].Pool.GetDimensionLength(0);

                foreach (SpikeVariable var in this.routineUI.VariableListDisplay.SelectedItems)
                {
                    int secondAmount = var[0].Pool.GetDimensionLength(0);
                    if (firstAmount != secondAmount)
                    {
                        MessageBox.Show("Does not support intervals of different length yet.");
                        return null;
                    }
                }

                float begin = 0.0F;
                float end = 0.0F;
                bool beginOK = float.TryParse(this.routineUI.VariableBeginTime.Text, out begin);
                bool endOK = float.TryParse(this.routineUI.VariableEndTime.Text, out end);

                string errorString = "";
                if (!beginOK && !endOK)
                    errorString = "There was a problem with the formatting of both your beginning and ending times.";
                else if (!beginOK)
                    errorString = "There was a problem with the formatting of your beginning time.";
                else if (!endOK)
                    errorString = "There was a problem with the formatting of your ending time.";
                else if (end <= begin)
                    errorString = "The end time is less than or equal to the start time";

                if (errorString != "")
                {
                    MessageBox.Show(errorString, "Failed to add the selected variable.", MessageBoxButton.OK, MessageBoxImage.Stop);
                    return null;
                }

                float[] beg = new float[this.routineUI.VariableListDisplay.SelectedItems.Count];
                float[] endL = new float[this.routineUI.VariableListDisplay.SelectedItems.Count];

                for (int i = 0; i < beg.Length; i++)
                {
                    beg[i] = begin;
                    endL[i] = end;
                }

                IntervalPackage package = new IntervalPackage(this.routineUI.VariableListDisplay.SelectedItems.Cast<SpikeVariable>().ToArray(), beg, endL);
                package.PackageTitle = firstVar.Header.ExperimentName;
                toRet.Add(package);
                #endregion
            }
            return toRet.ToArray();
        }

       
        public void AddButtonClicked()
        {
            IntervalPackage[] package = this.createPackages(false);
            if (package != null)
            {
                foreach (IntervalPackage pack in package)
                {
                    this.BatchControl.ToRunList.Add(pack);
                }
            }
        }

        public RoutineBatchControl BatchControl
        {
            get;
            private set;
        }

        #endregion
    }
}
