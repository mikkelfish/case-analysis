﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;
using CeresBase.UI;

namespace ApolloFinal.Tools.IntervalTools
{
    [AdapterDescription("Interval")]
    class PoincareCreator : AssociateAdapterBase
    {
        [AssociateWithProperty("Intervals")]
        public float[] Intervals { get; set; }

        [AssociateWithProperty("Time Delay")]
        public int TimeDelay { get; set; }

        [AssociateWithProperty("Units")]
        public string Units { get; set; }

        [AssociateWithProperty("Variable Name")]
        public string Description { get; set; }
            
        [AssociateWithProperty("Poincaré Plot", DisableLink=true, IsOutput=true, ReadOnly=true)]
        public GraphableOutput Output { get; set; }

        public override string Name
        {
            get
            {
                return "Poincaré Creation";
            }
            set
            {
               
            }
        }

        public override string Category
        {
            get
            {
                return "Interval Tools";
            }
            set
            {
                
            }
        }

        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            this.Output = new GraphableOutput();
            this.Output.GraphType = GraphingType.Scatter;

            double[] xdata = new double[this.Intervals.Length - this.TimeDelay];
            double[] ydata = new double[this.Intervals.Length - this.TimeDelay];


            for (int i = 0; i < xdata.Length - this.TimeDelay; i++)
            {
                xdata[i] = this.Intervals[i];
                ydata[i] = this.Intervals[i + this.TimeDelay];
            }
 

            this.Output.XData = xdata;
            this.Output.YData = new double[][] { ydata };
            this.Output.XUnits = this.Units;
            this.Output.YUnits = this.Units;

            this.Output.XLabel = "n";
            this.Output.YLabel = "n + " + this.TimeDelay.ToString();

            this.Output.SourceDescription = "Poincaré Plot";
            this.Output.Label = (this.Description == null) ? "" : (" for " + this.Description);

            
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            PoincareCreator creator = new PoincareCreator();
            creator.TimeDelay = this.TimeDelay;
            creator.Units = this.Units;
            creator.Intervals = this.Intervals;
            creator.Description = this.Description;
            

            return creator;
        }

        public override CeresBase.FlowControl.ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
           if (targetPropertyname == "Intervals" && targetValue == null)
                return new ValidationStatus(ValidationState.Fail, "Connect with an Interval Adapter");
            
            //if(targetPropertyname == "Time Delay" && targetValue 
            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
