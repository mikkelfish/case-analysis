﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using CeresBase.IO;
using CeresBase.General;
using ApolloFinal;

namespace ApolloFinal.Tools.IntervalTools
{
    public class StatisticsOutput : CombinedBatchOutput, IBatchViewable
    {
        public float[][] Data { get; private set; }

        public string[] Descriptions { get; private set; }

        public float[] Means { get; private set; }
        public float[] StandardDeviations { get; set; }

        public string Title { get; set; }

        public StatisticsOutput(float[][] data, string[] descriptions)
            : this(data, descriptions, null, 0)
        {

        }

        public StatisticsOutput(float[][] data, string[] descriptions, float[] rawVariable, double resolution)
        {

            if (rawVariable == null)
            {
                this.Descriptions = descriptions;
                this.Data = data;
                this.Means = new float[this.Data.Length];
                this.StandardDeviations = new float[this.Data.Length];

            }
            else
            {
                this.Descriptions = new string[descriptions.Length + 2];
                this.Data = new float[descriptions.Length + 2][];

                for (int i = 0; i < descriptions.Length; i++)
                {
                    this.Descriptions[i] = descriptions[i];
                    this.Data[i] = data[i];
                }


                this.Data[descriptions.Length] = new float[data[0].Length];
                this.Data[descriptions.Length + 1] = new float[data[0].Length];

                this.Descriptions[descriptions.Length] = "Peak";
                this.Descriptions[descriptions.Length + 1] = "Area";

                this.Means = new float[this.Data.Length];
                this.StandardDeviations = new float[this.Data.Length];

                float meanValueOfSignal = CeresBase.MathConstructs.Stats.Average(rawVariable);

                float total = 0;
                for (int i = 0; i < data[0].Length; i++)
                {
                    float max = float.MinValue;
                        float area = 0;
                        int start = (int)(total / resolution);
                        int end = (int)((total + data[data.Length - 1][i]) / resolution);
                        for (int j = start; j < end; j++)
                        {
                            if (j >= rawVariable.Length) break;
                            
                                float val = rawVariable[j] - meanValueOfSignal;
                                if (val < 0) continue;
                                if (rawVariable[j] > max)
                                    max = val;
                                area += val;
                        }

                        this.Data[this.Data.Length - 2][i] = max;
                        this.Data[this.Data.Length - 1][i] = area;
                        total += data[data.Length - 1][i];

                }
            }

            for (int i = 0; i < this.Data.Length; i++)
            {
                this.Means[i] = CeresBase.MathConstructs.Stats.Average(this.Data[i]);
                this.StandardDeviations[i] = CeresBase.MathConstructs.Stats.StandardDev(this.Data[i]);
            }
        }

        #region IBatchWritable Members

        protected override object[][] GetWrittenFormat(ref CombinedBatchOutput[] toWrite)
        {
            StatisticsOutput[] writes = toWrite.Where(v => v is StatisticsOutput).Cast<StatisticsOutput>().ToArray();
            CombinedBatchOutput[] toret = toWrite.Where(v => !(v is StatisticsOutput)).ToArray();

            if (writes == null) return null;


            List<List<object[]>> allLines = new List<List<object[]>>();

            foreach (StatisticsOutput write in writes)
            {
                List<object[]> lines = new List<object[]>();

                lines.Add(new object[] { write.Title });

                lines.Add(new object[] { "Summary" });

                lines.Add(new object[] { "", "Mean", "SD", "CV", "Min", "Max" });

                for (int i = 0; i < write.Descriptions.Length; i++)
                {
                    List<object> line = new List<object>();
                    line.Add(write.Descriptions[i]);
                    line.Add(write.Means[i]);
                    line.Add(write.StandardDeviations[i]);
                    line.Add(write.StandardDeviations[i] / write.Means[i]);

                    var sort = (from float f in write.Data[i]
                                orderby f
                                select f);
                    float min = sort.First();
                    float max = sort.Last();

                    line.Add(min);
                    line.Add(max);
                    lines.Add(line.ToArray());
                }

                lines.Add(new object[] { "" });
                lines.Add(new object[] { "" });

                List<object> headers = new List<object>();
                headers.Add("Interval");
                for (int i = 0; i < write.Descriptions.Length; i++)
                {
                    headers.Add(write.Descriptions[i]);
                }
                lines.Add(headers.ToArray());

                for (int i = 0; i < write.Data[0].Length; i++)
                {
                    List<object> line = new List<object>();
                    line.Add(i + 1);
                    for (int j = 0; j < write.Descriptions.Length; j++)
                    {
                        line.Add(write.Data[j][i]);
                    }
                    lines.Add(line.ToArray());
                }

                allLines.Add(lines);
            }

            List<object[]> completedLines = new List<object[]>();

            int[] maxWidths = new int[allLines.Count];
            int maxLines = -1;
            for (int i = 0; i < allLines.Count; i++)
            {
                if (allLines[i].Count > maxLines)
                {
                    maxLines = allLines[i].Count;
                }

                for (int j = 0; j < allLines[i].Count; j++)
                {
                    if (maxWidths[i] < allLines[i][j].Length)
                    {
                        maxWidths[i] = allLines[i][j].Length;
                    }
                }
            }

            //Create the merged version of each line
            for (int i = 0; i < maxLines; i++)
            {
                List<object> thisLine = new List<object>();
                for (int j = 0; j < allLines.Count; j++)
                {
                    int thisOne = 0;
                    if (i >= allLines[j].Count)
                    {
                        for (int k = 0; k < maxWidths[j]; k++)
                        {
                            thisLine.Add(null);
                        }
                    }
                    else
                    {
                        for (int k = 0; k < allLines[j][i].Length; k++)
                        {
                            thisLine.Add(allLines[j][i][k]);
                            thisOne++;
                        }

                        for (int k = thisOne; k < maxWidths[j]; k++)
                        {
                            thisLine.Add(null);
                        }
                    }

                    thisLine.Add(null);
                }

                completedLines.Add(thisLine.ToArray());
            }

            toWrite = toret;

            return completedLines.ToArray();
        }

        #endregion

        #region IBatchViewable Members

        //public IBatchViewable[] View(IBatchViewable[] viewable, bool saveToManager)
        //{
        //    return this.View(viewable);
        //}

        public Control[] View(IBatchViewable[] viewable, out IBatchViewable[] notUsed)
        {
            IBatchViewable view = viewable.Single(v => this.Equals(v as StatisticsOutput));
            IBatchViewable[] toret = viewable.Where(v => !this.Equals(v as StatisticsOutput)).ToArray();

            if (view == null)
            {
                notUsed = toret;
                return null;
            }

            if (this.Data == null || this.Data.Length == 0) 
            {
                notUsed = toret;
                return null;
            }

            int amount = this.Data[0].Length;
            float startTime = 0;
            List<IntervalEntry> entries = new List<IntervalEntry>();
            for (int i = 0; i < amount; i++)
            {
                float[] intervals = new float[this.Data.Length];
                for (int j = 0; j < this.Data.Length; j++)
                {
                    intervals[j] = this.Data[j][i];
                }

                IntervalEntry entry = new IntervalEntry(startTime, i + 1, this.Descriptions, intervals) { Title = this.Title };
                entries.Add(entry);

                int intervalSpot = intervals.Length - 1;
                if (this.Descriptions[this.Descriptions.Length - 1] == "Area")
                    intervalSpot -= 2;

                startTime += intervals[intervalSpot];
            }

            System.Windows.Window win = new System.Windows.Window();
            win.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            StatisticsOutputControl outputControl = new StatisticsOutputControl();
            outputControl.Package = new IntervalPackage(entries.ToArray());

            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(win);
            win.Content = new Grid();
            (win.Content as Grid).Children.Add(outputControl);

            //CeresBase.UI.ContentBox box = new CeresBase.UI.ContentBox();
            //box.ContentElement = new Grid();
            //(box.ContentElement as Grid).Children.Add(outputControl);
            //box.Height = 500;
            //box.Width = 800;
            //box.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //box.ShowDialog();

            win.Height = 500 + 32;
            win.Width = 800 + 8;
            outputControl.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            outputControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            if (this.Title != null) win.Title = this.Title;
        //    win.ShowDialog();


            notUsed = toret;
            return new Control[] { win };
        }

        #endregion
    }
}
