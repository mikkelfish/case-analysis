﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MesosoftCommon.ExtensionMethods;
using Microsoft.Windows.Controls;
using System.Collections.ObjectModel;
using ApolloFinal.Tools.IntervalTools.CycleScoring;


namespace ApolloFinal.Tools.IntervalTools
{
    /// <summary>
    /// Interaction logic for StatisticsOutputControl.xaml
    /// </summary>
    public partial class StatisticsOutputControl : UserControl
    {

        private IntervalEntry[] selectedEntries;
        public IntervalPackage Result 
        {
            get
            {
                if (this.selectedEntries != null)
                {
                    return new IntervalPackage(this.selectedEntries);
                }
                return null;
            }
        }

        private IntervalPackage package;
        public IntervalPackage Package 
        {
            get
            {
                return this.package;
            }
            set
            {
                this.package = value;

                if (this.package != null)
                {
                    this.list.Items.Clear();

                    IntervalEntry[] given = this.package.GetAsEntries().OrderBy(e=>e.IntervalNumber).ToArray();
                    this.list.BeginInit();
                    foreach (IntervalEntry entry in given)
                    {
                        this.list.Items.Add(entry);

                                //this.list.SelectedItems.Add(entry);
                        //ListBoxItem item = this.list.ItemContainerGenerator.ContainerFromItem(entry) as ListBoxItem;
                        //item.IsSelected = true;
                    }
                    this.list.EndInit();
                    this.list.SelectAll();
                    

                    this.createGrid();
                }
            }
        }

        private ObservableCollection<object> entries = new ObservableCollection<object>();
        public ObservableCollection<object> Entries
        {
            get
            {
                return this.entries;
            }
        }

        private ObservableCollection<object> stats = new ObservableCollection<object>();
        public ObservableCollection<object> Stats
        {
            get
            {
                return this.stats;
            }
        }

        private void createGrid()
        {
            this.entries.Clear();
            this.stats.Clear();

            
            if (this.list.SelectedItems.Count == 0) return;

            this.grid.BeginInit();
            this.grid.Columns.Clear();

          

            this.selectedEntries = this.list.SelectedItems.Cast<IntervalEntry>().ToArray();

            List<float>[] data = new List<float>[this.selectedEntries[0].IntervalNames.Length];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = new List<float>();
            }

            for (int i = 0; i < this.selectedEntries.Length; i++)
            {
                IntervalEntry entry = this.selectedEntries[i];
                List<string> names = new List<string>();
                List<Type> types = new List<Type>();
                List<object> values = new List<object>();


                names.Add("Interval");
                types.Add(typeof(int));
                values.Add(this.selectedEntries[i].IntervalNumber);

                for (int j = 0; j < this.selectedEntries[i].IntervalNames.Length; j++)
                {
                    names.Add(this.selectedEntries[i].IntervalNames[j]);
                    types.Add(typeof(float));
                    values.Add(this.selectedEntries[i].IntervalTimes[j]);

                    data[j].Add(this.selectedEntries[i].IntervalTimes[j]);
                }

                names.Add("Time");
                types.Add(typeof(float));
                values.Add(this.selectedEntries[i].BaseTime);

                this.entries.Add(MesosoftCommon.Utilities.Reflection.DynamicObjects.CreateObject(types.ToArray(), null, names.ToArray(), null, null, values.ToArray()));

                if (i == 0)
                {
                    this.grid.Columns.Add(new DataGridTextColumn() { Header = names[0], Width=new DataGridLength(1, DataGridLengthUnitType.Star), Binding = new Binding() { Path = new PropertyPath(names[0]) } });


                    for (int j = 1; j < names.Count; j++)
                    {
                        string toBind = char.ToUpper(names[j][0]).ToString();
                        if (names[j].Length > 1)
                        {
                            toBind += names[j].Substring(1);
                        }
                        this.grid.Columns.Add(new DataGridTextColumn() { Header = names[j], Width = new DataGridLength(1, DataGridLengthUnitType.Star), Binding = new Binding() { Path = new PropertyPath(toBind), StringFormat = "F2" } });
                    }
                }
            }



            this.grid.EndInit();


            this.statgrid.BeginInit();
            this.statgrid.Columns.Clear();

           

            for (int i = 0; i < 5; i++)
            {
                List<string> statNames = new List<string>();
                List<Type> statTypes = new List<Type>();
                List<object> statVals = new List<object>();

                statNames.Add("Stat");
                statTypes.Add(typeof(string));
                if (i == 0)
                {
                    statVals.Add("Mean");
                }
                else if (i == 1)
                {
                    statVals.Add("Std. Dev.");
                }
                else if (i == 2)
                {
                    statVals.Add("CV");
                }
                else if (i == 3)
                {
                    statVals.Add("Max.");
                }
                else if (i == 4)
                {
                    statVals.Add("Min.");
                }

                for (int j = 0; j < this.selectedEntries[0].IntervalNames.Length; j++)
                {

                    float mean = CeresBase.MathConstructs.Stats.Average(data[j].ToArray());
                    float std = CeresBase.MathConstructs.Stats.StandardDev(data[j].ToArray());
                    float cv =  std/mean;

                    statNames.Add(this.selectedEntries[0].IntervalNames[j]);
                    statTypes.Add(typeof(float));

                    if (i == 0)
                    {
                        statVals.Add(mean);
                    }
                    else if (i == 1)
                    {
                        statVals.Add(std);
                    }
                    else if (i == 2)
                    {
                        statVals.Add(cv);
                    }
                    else if (i == 3)
                    {
                        statVals.Add(data[j].Max());
                    }
                    else if (i == 4)
                    {
                        statVals.Add(data[j].Min());
                    }
                }

                if (i == 0)
                {
                    this.statgrid.Columns.Add(new DataGridTextColumn()
                    {
                        Header = statNames[0],
                        Width = new DataGridLength(1, DataGridLengthUnitType.Star),
                        Binding = new Binding() { Path = new PropertyPath(statNames[0]) }
                    });


                    for (int j = 1; j < statNames.Count; j++)
                    {
                        string toBind = char.ToUpper(statNames[j][0]).ToString();
                        if (statNames[j].Length > 1)
                        {
                            toBind += statNames[j].Substring(1);
                        }

                        this.statgrid.Columns.Add(new DataGridTextColumn()
                        {
                            Header = statNames[j],
                            Width = new DataGridLength(1, DataGridLengthUnitType.Star),
                            Binding = new Binding() { Path = new PropertyPath(toBind), StringFormat = "F2" }
                        });
                    }
                }

                this.stats.Add(MesosoftCommon.Utilities.Reflection.DynamicObjects.CreateObject(statTypes.ToArray(), null, statNames.ToArray(), null, null, statVals.ToArray()));
            }

           
            

            this.statgrid.EndInit();

           //this.grid.AutoGenerateColumns = true;
        }

        

        public StatisticsOutputControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Window window = this.FindTopAncestor<Window>();
            if (window != null)
            {
                window.Close();
            }


        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.createGrid();
        }

        private void deselect()
        {
            foreach(object obj in this.grid.SelectedItems)
            {
                System.Reflection.PropertyInfo info = obj.GetType().GetProperty("Interval");
                int row = (int)info.GetValue(obj, null);
                object selected = this.list.Items[row - 1];
                this.list.SelectedItems.Remove(selected);
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.deselect();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            this.deselect();
            this.createGrid();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            CycleScoringWindow win = new CycleScoringWindow();
            
        }
        
    }
}
