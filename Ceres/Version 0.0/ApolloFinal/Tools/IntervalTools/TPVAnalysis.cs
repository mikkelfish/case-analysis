﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;
using ApolloFinal.Tools.PoincareStats;
using System.Drawing;
using CeresBase.UI;

namespace ApolloFinal.Tools.IntervalTools
{
    [AdapterDescription("Interval")]
    class TPVAnalysis : AssociateAdapterBase
    {
        [AssociateWithProperty("Data", ReadOnly=true)]
        public float[] Data { get; set; }

        [AssociateWithProperty("Max Tau")]
        public int MaxTau { get; set; }

        [AssociateWithProperty("Max Parts")]
        public int MaxPartitionExponent { get; set; }

        [AssociateWithProperty("P")]
        public int P { get; set; }

        [AssociateWithProperty("Label")]
        public string Label { get; set; }

        [AssociateWithProperty("TPVtd", IsOutput= true)]
        public object TPVTD { get; set; }

        [AssociateWithProperty("SD2 and TPVa", IsOutput=true)]
        public object TPVA { get; set; }

        public override string Name
        {
            get
            {
                return "TPV Analysis";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Interval Tools";
            }
            set
            {
                
            }
        }

        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;


        public override void RunAdapter()
        {
            if (this.Data == null) return;

            int maxPartitions = (int)Math.Pow(2, this.MaxPartitionExponent);
            int maxLength = this.Data.Length / maxPartitions;
            maxLength = maxLength * maxPartitions;

            float[] temp = new float[maxLength];
            Array.Copy(this.Data, temp, maxLength);
            this.Data = temp;
            

            List<double> tpvTds = new List<double>();
            double[] xdata = new double[this.MaxTau];

            for (int i = 1; i <= this.MaxTau; i++)
            {
                xdata[i - 1] = i;

                double avg = 0;
                int count = 0;
                for (int j = 0; j < this.Data.Length - i; j++)
                {
                    avg += this.Data[j] - this.Data[j + i];
                    count++;
                }

                avg /= count;

                double sd = 0;
                for (int j = 0; j < this.Data.Length - i; j++)
                {
                    sd += (this.Data[j] - this.Data[j + i]) * (this.Data[j] - this.Data[j + i]) - avg * avg;
                  //  count++;
                }

                sd /= count;

                sd = Math.Sqrt(sd);

                tpvTds.Add(sd / Math.Sqrt(2));
            }


            GraphableOutput tpvtd = new GraphableOutput();
            tpvtd.SourceDescription = "TPVTD Max Tau " + this.MaxTau;
            tpvtd.Label = this.Label;
            tpvtd.GraphType = GraphingType.Line;
            tpvtd.XLabel = "Tau";
            tpvtd.YLabel = "TPVTD";
            tpvtd.XData = xdata;
            tpvtd.YData = new double[][] { tpvTds.ToArray() };

         
            int maxLoop = this.MaxPartitionExponent + 1;

            int numSpots = (int)Math.Pow(2, this.MaxPartitionExponent) - 1;
            double[][] sd2s = new double[maxLoop][];
            double[][] tpvas = new double[maxLoop][];

            double[][] tpvtd2 = new double[maxLoop][];

            for (int loop = 0; loop < maxLoop; loop++)
            {
                int power = (int)Math.Pow(2, loop);

                sd2s[loop] = new double[power];
                tpvas[loop] = new double[power];
                tpvtd2[loop] = new double[power];

                int amount = this.Data.Length / power;

                for (int chunk = 0; chunk < power; chunk++)
                {

                    int start = chunk * amount;

                    float[] data = new float[amount];
                    Array.Copy(this.Data, start, data, 0, amount);
                    float[] successiveDifferences = new float[data.Length - 1];
                    for (int i = 0; i < successiveDifferences.Length; i++)
                    {
                        successiveDifferences[i] = data[i + 1] - data[i];
                    }

                    float sd = CeresBase.MathConstructs.Stats.StandardDev(data);
                    float sd1 = CeresBase.MathConstructs.Stats.StandardDev(successiveDifferences) / (float)Math.Sqrt(2.0);

                    double sd2 = Math.Sqrt(2.0 * sd * sd - sd1 * sd1);
                    sd2s[loop][chunk] = sd2;

                    PointF[][] points = new PointF[1][];
                    points[0] = new PointF[data.Length - 1];
                    for (int i = 0; i < points[0].Length; i++)
                    {
                        PointF point = new PointF(data[i], data[i + 1]);
                        points[0][i] = point;
                    }


                    int[] ps = new int[] { this.P };
                    PoincareCentroid cent = new PoincareCentroid();
                    double[][] toRun = cent.RunStat(points, ps);
                    for (int i = 0; i < ps.Length; i++)
                    {
                        double avg = CeresBase.MathConstructs.Stats.Average(toRun[i]);
                        double sda = CeresBase.MathConstructs.Stats.StandardDev(toRun[i]);
                        double cv = sda / avg;
                        tpvas[loop][chunk] = cv;
                    }

                    points = new PointF[1][];
                    points[0] = new PointF[successiveDifferences.Length - 1];
                    for (int i = 0; i < points[0].Length; i++)
                    {
                        PointF point = new PointF(successiveDifferences[i], successiveDifferences[i + 1]);
                        points[0][i] = point;
                    }

                    toRun = cent.RunStat(points, ps);
                    for (int i = 0; i < ps.Length; i++)
                    {
                        double avg = CeresBase.MathConstructs.Stats.Average(toRun[i]);
                        double sda = CeresBase.MathConstructs.Stats.StandardDev(toRun[i]);
                        double cv = sda / avg;
                        tpvtd2[loop][chunk] = sda;
                    }
                }
            }

            double[] x = new double[sd2s.Length];
            double[][] toOutput = new double[6][];
            for (int i = 0; i < 6; i++)
            {
                toOutput[i] = new double[sd2s.Length];
            }

            for (int i = 0; i < sd2s.Length; i++)
            {
                toOutput[0][i] = CeresBase.MathConstructs.Stats.Average(sd2s[i]);
                toOutput[1][i] = CeresBase.MathConstructs.Stats.StandardDev(sd2s[i]);
                toOutput[2][i] = CeresBase.MathConstructs.Stats.Average(tpvas[i]);
                toOutput[3][i] = CeresBase.MathConstructs.Stats.StandardDev(tpvas[i]);
                toOutput[4][i] = CeresBase.MathConstructs.Stats.Average(tpvtd2[i]);
                toOutput[5][i] = CeresBase.MathConstructs.Stats.StandardDev(tpvtd2[i]);

                x[i] = i + 1;
            }

            GraphableOutput tpva = new GraphableOutput();
            tpva.SourceDescription = "SD2/TPVA P = " + this.P;
            tpva.Label = this.Label;
            tpva.SeriesLabels = new string[]{"SD2 Averages", "SD2 SDs", "TPVA Averages", "TPVA SDs", "TPVS Averages", "TPVS SDs"};
            tpva.XData = x;
            tpva.YData = toOutput;
            tpva.XLabel = "NONE";
            tpva.YUnits = "None";

            this.TPVA = tpva;
            this.TPVTD = tpvtd;
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            TPVAnalysis toRet = new TPVAnalysis();
            toRet.MaxPartitionExponent = this.MaxPartitionExponent;
            toRet.MaxTau = this.MaxTau;
            toRet.P = this.P;
            return toRet;
        }

        public override CeresBase.FlowControl.ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null) return new ValidationStatus(ValidationState.Fail, "Link to data");


            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
