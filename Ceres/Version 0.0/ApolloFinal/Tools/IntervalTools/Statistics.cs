﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;

namespace ApolloFinal.Tools.IntervalTools
{
    [AdapterDescription("Interval")]
    public class Statistics : AssociateAdapterBase
    {
        public override string Name
        {
            get
            {
                return "Interval Statistics";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Interval Tools";
            }
            set
            {
                
            }
        }

        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        [AssociateWithProperty("Interval Data", ReadOnly=true)]
        public float[][] IntervalData { get; set; }

        [AssociateWithProperty("Variable Names", ReadOnly=true)]
        public string[] Descriptions { get; set; }

        [AssociateWithProperty("Title")]
        public string Title { get; set; }

        [AssociateWithProperty("Raw Data", ReadOnly = true)]
        public float[] RawData { get; set; }

        [AssociateWithProperty("Raw Data Res.", ReadOnly=true)]
        public float DataRes { get; set; }

        [AssociateWithProperty("Calculated Statistics", DisableLink=true, IsOutput=true, ReadOnly=true)]
        public StatisticsOutput Output { get; set; }

        [AssociateWithProperty("Calculated Peaks", DisableLink = true, IsOutput = true, ReadOnly = true)]
        public float[] PeakData { get; set; }


        [AssociateWithProperty("Calculated Areas", DisableLink = true, IsOutput = true, ReadOnly = true)]
        public float[] AreaData { get; set; }

        public override void RunAdapter()
        {
           this.Output = new StatisticsOutput(this.IntervalData, this.Descriptions, this.RawData, this.DataRes) { Title = this.Title };
           if (this.RawData != null && this.Output.Descriptions.Length > 2)
           {
               if (this.Output.Descriptions[this.Output.Descriptions.Length - 2] == "Peak")
               {
                   this.PeakData = this.Output.Data[this.Output.Data.Length - 2];
                   this.AreaData = this.Output.Data[this.Output.Data.Length - 1];

               }
           }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            return new Statistics();
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Interval Data" && targetValue == null)
                return new ValidationStatus(ValidationState.Fail);

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
