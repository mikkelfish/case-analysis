﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl;
using CeresBase.FlowControl.Adapters;
using System.Collections;

namespace ApolloFinal.Tools.IntervalTools
{
    [AdapterDescription("Interval")]
    class IntervalPermutationAdapter : AssociateAdapterBase, IPermutationAdapter
    {

        public override string Name
        {
            get
            {
                return "Interval Permutation";
            }
            set
            {
               
            }
        }

        public override string Category
        {
            get
            {
                return "Interval Tools";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("Intervals", CanLinkFrom = false)]
        public float[][] Intervals { get; set; }
        [AssociateWithProperty("Interval Names", CanLinkFrom = false)]
        public string[] IntervalNames { get; set; }
        [AssociateWithProperty("Start Times", CanLinkFrom = false)]
        public float[] BeginTimes { get; set; }
        [AssociateWithProperty("End Times", CanLinkFrom = false)]
        public float[] EndTimes { get; set; }


        [AssociateWithProperty("Interval Data", IsOutput = true)]
        public float[] Output { get; set; }
        [AssociateWithProperty("Interval Name", IsOutput=true)]
        public string IntervalName { get; set; }
        [AssociateWithProperty("Begin Time", IsOutput = true)]
        public float BeginTime { get; set; }
        [AssociateWithProperty("End Time", IsOutput = true)]
        public float EndTime { get; set; }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            if (this.Intervals != null && this.Intervals.Length > this.curIteration)
            {
                this.Output = this.Intervals[this.curIteration];
                
                if(this.IntervalNames != null)
                    this.IntervalName = this.IntervalNames[this.curIteration];
                
                if(this.BeginTimes != null)
                    this.BeginTime = this.BeginTimes[this.curIteration];
                
                if(this.EndTimes != null)
                    this.EndTime = this.EndTimes[this.curIteration];
            }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
           
        }

        public override object Clone()
        {
            IntervalPermutationAdapter adapter = new IntervalPermutationAdapter();

            adapter.Intervals = this.Intervals;
            adapter.IntervalNames = this.IntervalNames;
            adapter.BeginTimes = this.BeginTimes;
            adapter.EndTimes = this.EndTimes;
            return adapter;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Intervals" && targetValue == null)
                return new ValidationStatus(ValidationState.Fail, "Connect with Intervals output from data adapter.");

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }

        #region IPermutationAdapter Members
        public int IterationCount
        {
            get
            {
                if (this.Intervals != null)
                    return this.Intervals.Length;
                return 0;
            }
        }

        private int curIteration = 0;
        public void SetIteration(int iteration)
        {
            this.curIteration = iteration;
        }
        #endregion
    }
}
