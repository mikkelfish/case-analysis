﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;

namespace ApolloFinal.Tools.IntervalTools
{
    [AdapterDescription("Interval")]
    class IntervalAdapterData : DataAdapter, IPermutationAdapter
    {
        private IntervalPackage package;
        public IntervalPackage Package 
        {
            get
            {
                return this.package;
            }
            set
            {
                this.package = value;
                if (this.package != null)
                {
                    this.varName = this.Package.IntervalNames[this.whichVar];
                }
            }
        }

        private int whichVar = 0;


        #region IFlowControlPermutationAdapter Members

        public int IterationCount
        {
            get 
            {
                if (this.package != null)
                {
                    return this.package.IntervalNames.Length;
                }

                return 0;
            }
        }

        public void SetIteration(int iteration)
        {
            this.whichVar = iteration;
            if (this.package != null)
            {
                this.varName = this.Package.IntervalNames[this.whichVar];
            }
        }

        #endregion



        private string varName;

        public override string[] SupplyPropertyNames()
        {
            List<string> toRet = new List<string>();
            toRet.Add("Intervals");
            toRet.Add("Start Time");
            toRet.Add("End Time");
            toRet.Add("Variable Name");

            string[] names = base.SupplyPropertyNames();
            if (names != null)
            {
                toRet.AddRange(names);
            }

            return toRet.ToArray();
        }

        public override bool CanEditProperty(string targetPropertyname)
        {
            if (targetPropertyname == "Intervals" || targetPropertyname == "Start Time" ||
                targetPropertyname == "End Time" || targetPropertyname == "Variable Name")
            {
                return false;
            }

            return base.CanEditProperty(targetPropertyname);
        }

        public override bool CanReceivePropertyLinks(string targetPropertyname)
        {
            if (targetPropertyname == "Intervals" || targetPropertyname == "Start Time" ||
                targetPropertyname == "End Time" || targetPropertyname == "Variable Name")
            {
                return false;
            }
            return base.CanReceivePropertyLinks(targetPropertyname);
        }

        public override bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            return true;

          //  return base.CanReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override object[] GetValuesForSerialization()
        {
            return base.GetValuesForSerialization();
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            base.LoadValuesFromSerialization(values);
        }

        public override bool ShouldSerialize(string targetPropertyname)
        {
            if (targetPropertyname == "Intervals" || targetPropertyname == "Start Time" ||
                targetPropertyname == "End Time" || targetPropertyname == "Variable Name")
            {
                return true;
            }
            return base.ShouldSerialize(targetPropertyname);
        }

        public override object SupplyPropertyValue(string sourcePropertyName)
        {
            if (this.Package == null) return null;

            if(sourcePropertyName == "Intervals")
            {
                return this.Package.GetIntervalLength(this.varName);
            }

            if (sourcePropertyName == "Start Time")
            {
                return this.Package.BeginTimes[this.whichVar];
            }

            if (sourcePropertyName == "End Time")
            {
                return this.Package.EndTimes[this.whichVar];
            }

            if (sourcePropertyName == "Variable Name")
            {
                return this.varName;
            }

            return base.SupplyPropertyValue(sourcePropertyName);
        }

        public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (this.Package == null) return;
            base.ReceivePropertyValue(propertyValue, targetPropertyName);
        }



        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public override object Clone()
        {
            IntervalAdapterData adapter = new IntervalAdapterData();
            adapter.Package = this.Package;
            this.cloneHelper(adapter);
            return adapter;
        }

        public override string Name
        {
            get
            {
                return "Interval Data Adapter";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Data Sources";
            }
            set
            {
                
            }
        }

       
    }
}
