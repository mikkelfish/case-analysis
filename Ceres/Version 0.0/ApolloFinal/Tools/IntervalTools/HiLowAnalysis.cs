﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl.Adapters.Matlab;
using CeresBase.FlowControl;
using CeresBase.General;
using CeresBase.UI;

namespace ApolloFinal.Tools.IntervalTools
{
    [AdapterDescription("Mixed")]
    class HiLowAnalysis : AssociateAdapterBase
    {
        //public class hiLowOutput : CombinedBatchOutput, ILabel
        //{
        //    public string Label { get; set; }

        //    public double Ratio { get; set; }

        //    public double HighFrac { get; set; }

        //    public double LowFrac { get; set; }

        //    public hiLowOutput()
        //    {

        //    }

        //    protected override object[][] GetWrittenFormat(ref CombinedBatchOutput[] write)
        //    {
        //        hiLowOutput[] writes = write.Where(v => v is hiLowOutput).Cast<hiLowOutput>().ToArray();
        //        CombinedBatchOutput[] toret = write.Where(v => !(v is hiLowOutput)).ToArray();

        //        if (writes == null) return null;
        //        write = toret;

        //        List<object[]> lines = new List<object[]>();

        //        foreach (hiLowOutput output in writes)
        //        {

        //            lines.Add(new object[] { "High/Low Frequency Analysis for", output.Label });
        //            lines.Add(new object[] { "Ratio", "High", "Low" });
        //            lines.Add(new object[] { output.Ratio, output.HighFrac, output.LowFrac });
        //            lines.Add(new object[] { "" });
        //        }
        //        return lines.ToArray();


        //    }

        //    public override string ToString()
        //    {
        //        string lab = "High Low";
        //        if (this.Label != null)
        //            lab += " " + this.Label;
        //        return lab;
        //    }
        //}

        [AssociateWithProperty("Label")]
        public string Label { get; set; }

        [AssociateWithProperty("Data")]
        public float[] Data { get; set; }

        [AssociateWithProperty("Data Res.")]
        public double Resolution { get; set; }

        [AssociateWithProperty("Intervals")]
        public float[] Scored { get; set; }

        [AssociateWithProperty("Low Freq")]
        public double LowMin { get; set; }

        [AssociateWithProperty("Low Freq2")]
        public double LowMax { get; set; }

        [AssociateWithProperty("High Freq")]
        public double HighMin { get; set; }

        [AssociateWithProperty("High Freq2")]
        public double HighMax { get; set; }

        [AssociateWithProperty("Ratio", IsOutput = true)]
        public double Ratio { get; set; }

        [AssociateWithProperty("High Fraction", IsOutput = true)]
        public double HighFrac { get; set; }

        [AssociateWithProperty("Low Fraction", IsOutput = true)]
        public double LowFrac { get; set; }

        [AssociateWithProperty("Output", IsOutput = true)]
        public GraphableOutput Output { get; set; }

        public HiLowAnalysis()
        {
            this.LowMin = 0.04;
            this.LowMax = 1;
            this.HighMin = 1;
            this.HighMax = 3;
        }

        public override string Name
        {
            get
            {
                return "High/Low Ratio";
            }
            set
            {
            }
        }

        public override string Category
        {
            get
            {
                return "Frequency Tools";
            }
            set
            {
            }
        }

        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            lock (MatlabLoader.MatlabLockable)
            {
                if (this.Data == null) return;

                int length = this.Scored.Length > this.Data.Length ? this.Data.Length : this.Scored.Length;

                double[,] toPass = new double[2, length];
                for (int i = 0; i < length; i++)
                {
                        float timing = this.Scored[i] - this.Scored[0];

                        toPass[0, i] = timing;


                        int index = 0;
                        if (this.Data.Length > this.Scored.Length)
                            index = (int)(timing / this.Resolution);
                        else index = i;
                        if (index < this.Data.Length)
                            toPass[1, i] = this.Data[index];
                        else toPass[1, i] = this.Data[index - 1];
                }

                object[] res =
                    MatlabLoader.MatlabFunctions.ratBP(5, toPass, this.LowMin, this.LowMax, this.HighMin, this.HighMax) as object[];

                this.Ratio = (res[0] as double[,])[0,0];
                this.HighFrac = (res[1] as double[,])[0, 0];
                this.LowFrac = (res[2] as double[,])[0, 0];
                double[,] x = (res[3] as double[,]);
                double[,] y = (res[4] as double[,]);

                

                this.Output = new GraphableOutput()
                {
                    XData = CeresBase.General.Utilities.SquareArrayToDoubleArray(x)[0],
                    YData = CeresBase.General.Utilities.SquareArrayToDoubleArray(y),
                    ExtraInfo = new object[] { "Ratio: " + this.Ratio, "Low Frac: " + this.LowFrac, "High Frac: " + this.HighFrac },
                    Label = this.Label,
                    SourceDescription = "Hi/Low Lomb Analysis",
                    YLabel = "Magnitude",
                    XLabel = "Frequency",
                    XUnits = "hz"
                };

                //this.Output = new hiLowOutput() { Ratio = this.Ratio, LowFrac = this.LowFrac, HighFrac = this.HighFrac, Label = this.Label };
            }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {

        }

        public override object Clone()
        {
            return new HiLowAnalysis() { HighMin = this.HighMin, HighMax = this.HighMax, LowMin = this.LowMin, LowMax = this.LowMax };
        }

        public override CeresBase.FlowControl.ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Link to data source");
            }

            if (targetPropertyname == "Data Res." && targetValue is double && (double)targetValue == 0)
            {
                return new ValidationStatus(ValidationState.Fail, "Link to data source");

            }

            if (targetPropertyname == "Low Freq")
            {
                double val = (double)targetValue;
                if (val <= 0) return new ValidationStatus(ValidationState.Fail, "Must be greater than 0");
            }

            if (targetPropertyname == "Low Freq2")
            {
                double val = (double)targetValue;
                if (val <= this.LowMin) return new ValidationStatus(ValidationState.Fail, "Must be greater than Low Freq");
            }

            if (targetPropertyname == "High Freq")
            {
                double val = (double)targetValue;
                if (val < this.LowMax) return new ValidationStatus(ValidationState.Fail, "Must be greater than Low Freq 2");
            }

            if (targetPropertyname == "High Freq2")
            {
                double val = (double)targetValue;
                if (val <= this.HighMin) return new ValidationStatus(ValidationState.Fail, "Must be greater than High Freq");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
