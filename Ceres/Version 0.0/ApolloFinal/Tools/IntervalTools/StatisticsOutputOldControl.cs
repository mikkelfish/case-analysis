﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ApolloFinal.Tools.IntervalTools
{
    public partial class StatisticsOutputOldControl : UserControl
    {
        private IntervalEntry[] selectedEntries;

        private void createGrid()
        {
            this.dataGridViewMain.AllowUserToResizeColumns = false;
            this.dataGridViewMain.AllowUserToResizeRows = false;
            this.dataGridViewMain.AllowUserToAddRows = false;
            this.dataGridViewMain.AllowUserToDeleteRows = false;
            this.dataGridViewMain.AllowUserToOrderColumns = false;
            this.dataGridViewMain.ScrollBars = ScrollBars.Vertical;
            this.dataGridViewMain.ReadOnly = true;

            this.dataGridViewMain.Columns.Clear();
            this.dataGridViewMain.Rows.Clear();

            if (this.listBoxIntervals.SelectedItems.Count == 0) return;

            dataGridViewMain.Columns.Add("Interval", "Interval");

            IntervalEntry firstEntry = this.listBoxIntervals.SelectedItems[0] as IntervalEntry;

            for (int i = 0; i < firstEntry.IntervalNames.Length; i++)
            {
                dataGridViewMain.Columns.Add(firstEntry.IntervalNames[i], firstEntry.IntervalNames[i]);
            }

            dataGridViewMain.Columns.Add("Time", "Time");

            List<float>[] data = new List<float>[firstEntry.IntervalNames.Length];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = new List<float>();
            }

            this.selectedEntries = this.listBoxIntervals.SelectedItems.OfType<IntervalEntry>().ToArray();

            for (int i = 0; i < this.listBoxIntervals.SelectedItems.Count; i++)
            {
                IntervalEntry entry = this.listBoxIntervals.SelectedItems[i] as IntervalEntry;

                int row = dataGridViewMain.Rows.Add();

                dataGridViewMain.Rows[row].Cells[0].Value = entry.IntervalNumber;
                for (int j = 0; j < entry.IntervalTimes.Length; j++)
                {
                    dataGridViewMain.Rows[row].Cells[j + 1].Value = (float)Math.Round(entry.IntervalTimes[j],2);
                    data[j].Add(entry.IntervalTimes[j]);
                }
                dataGridViewMain.Rows[row].Cells[1 + entry.IntervalNames.Length].Value = (float)Math.Round(entry.BaseTime,2);
            }

            this.dataGridViewSummary.Columns.Clear();
            this.dataGridViewSummary.Columns.Add("Stat", "Stat");
            for (int i = 0; i < firstEntry.IntervalNames.Length; i++)
            {
                this.dataGridViewSummary.Columns.Add(firstEntry.IntervalNames[i], firstEntry.IntervalNames[i]);
            }

            this.dataGridViewSummary.ScrollBars = ScrollBars.None;

            this.dataGridViewSummary.Rows.Add();
            this.dataGridViewSummary.Rows[0].Cells[0].Value = "Mean";
            this.dataGridViewSummary.Rows.Add();
            this.dataGridViewSummary.Rows[1].Cells[0].Value = "Std. Dev.";
            this.dataGridViewSummary.Rows.Add();
            this.dataGridViewSummary.Rows[2].Cells[0].Value = "CV";

            for (int i = 0; i < firstEntry.IntervalNames.Length; i++)
            {
                float mean = CeresBase.MathConstructs.Stats.Average(data[i].ToArray());
                float std = CeresBase.MathConstructs.Stats.StandardDev(data[i].ToArray());

                this.dataGridViewSummary.Rows[0].Cells[1 + i].Value = (float)Math.Round(mean, 2);
                this.dataGridViewSummary.Rows[1].Cells[1 + i].Value = (float)Math.Round(std, 2);
                this.dataGridViewSummary.Rows[2].Cells[1 + i].Value = (float)Math.Round(std/mean, 2);
            }

            this.resizeGrids();
        }

        public IntervalPackage CreatePackage()
        {
            if (this.selectedEntries != null)
            {
                return new IntervalPackage(this.selectedEntries);
            }

            return null;
        }

        public void Populate(IntervalEntry[] entries)
        {
            this.listBoxIntervals.Items.Clear();
            this.listBoxIntervals.BeginUpdate();

            this.listBoxIntervals.Items.AddRange(entries.OrderBy((ent) => ent.IntervalNumber).ToArray());
            this.listBoxIntervals.EndUpdate();

            for (int i = 0; i < this.listBoxIntervals.Items.Count; i++)
            {
                this.listBoxIntervals.SetSelected(i, true);
            }

            this.createGrid();
        }

        public StatisticsOutputOldControl()
        {
            InitializeComponent();

            this.dataGridViewMain.SizeChanged += new EventHandler(dataGridViewMain_SizeChanged);
            this.dataGridViewSummary.SizeChanged += new EventHandler(dataGridViewSummary_SizeChanged);
        }

        private void resizeGrids()
        {
            if (this.dataGridViewMain.Columns.Count == 0) return;

            int columnSize = (this.dataGridViewMain.Width - 40) / this.dataGridViewMain.Columns.Count;

            for (int i = 0; i < this.dataGridViewMain.Columns.Count; i++)
            {
                this.dataGridViewMain.Columns[i].Width = columnSize;
            }

            columnSize = (this.dataGridViewSummary.Width - 40) / this.dataGridViewSummary.Columns.Count;

            for (int i = 0; i < this.dataGridViewSummary.Columns.Count; i++)
            {
                this.dataGridViewSummary.Columns[i].Width = columnSize;
            }
        }

        void dataGridViewSummary_SizeChanged(object sender, EventArgs e)
        {
            this.resizeGrids();
        }

        void dataGridViewMain_SizeChanged(object sender, EventArgs e)
        {
            this.resizeGrids();
            
        }

        private void deselectRowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.dataGridViewMain.SelectedCells.Count; i++)
			{
                int which = (int)this.dataGridViewMain.Rows[this.dataGridViewMain.SelectedCells[i].RowIndex].Cells[0].Value;
                for (int j = 0; j < this.listBoxIntervals.Items.Count; j++)
                {
                    IntervalEntry entry = this.listBoxIntervals.Items[j] as IntervalEntry;
                    if (entry.IntervalNumber == which)
                    {
                        this.listBoxIntervals.SelectedItems.Remove(entry);
                        break;
                    }
                }            			    
    
			} 
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            this.createGrid();
        }
    }
}
