﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;
using CeresBase.FlowControl.Adapters.Matlab;
using CeresBase.UI;

namespace ApolloFinal.Tools.MixedTools
{
    [AdapterDescription("Mixed")]
    public class LeeCoupling : AssociateAdapterBase
    {
        public override string Name
        {
            get
            {
                return "Lee's Coupling";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Coupling";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("Data1")]
        public float[] Data1 { get; set; }

        [AssociateWithProperty("Data2")]
        public float[] Data2 { get; set; }

        [AssociateWithProperty("Number To Use")]
        public int NumberToUse { get; set; }

        [AssociateWithProperty("Should Plot")]
        public bool ShouldPlot { get; set; }

        [AssociateWithProperty("Should Save")]
        public bool ShouldSave { get; set; }

        [AssociateWithProperty("Path")]
        public string Path { get; set; }
        
        [AssociateWithProperty("Label")]
        public string Label { get; set; }

        [AssociateWithProperty("Output Info",IsOutput=true)]
        public GraphableOutput Graph { get; set; }

        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            //RunNewCoupling(trigger1Data, trigger2Data, bestnum, mostvar, shouldplot, save, label, path)
            lock (MatlabLoader.MatlabLockable)
            {
                object[] toRet = MatlabLoader.MatlabFunctions.RunNewCoupling(1, this.Data1, this.Data2, this.NumberToUse, 0, this.ShouldPlot ? 1 : 0, this.ShouldSave ? 1 : 0, this.Label, this.Path);
                double[,] realRet = toRet[0] as double[,];
                double numBreaths = Convert.ToDouble(realRet[0, 0]);
                double autobeforep = Convert.ToDouble(realRet[0, 4]);
                double autoafterp = Convert.ToDouble(realRet[0, 5]);
                double chibefore = Convert.ToDouble(realRet[0, 8]);
                double chiafter = Convert.ToDouble(realRet[0, 9]);

                CeresBase.UI.GraphableOutput output = new CeresBase.UI.GraphableOutput();
                output.XData = new double[] { -1, 1 };
                output.YData = new double[][] { new double[] { autobeforep, autoafterp }, new double[] { chibefore, chiafter } };
                output.Label = this.Label;
                output.SourceDescription = "Lee coupling";
                output.SeriesLabels = new string[] { "Auto test p", "Chi square" };

                
                int s = 0;

            }
            
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
        }

        public override object Clone()
        {
            LeeCoupling newCoupling = new LeeCoupling();
            newCoupling.NumberToUse = this.NumberToUse;
            newCoupling.ShouldPlot = this.ShouldPlot;
            newCoupling.ShouldSave = this.ShouldSave;
            newCoupling.Path = this.Path;
            newCoupling.Label = this.Label;
            return newCoupling;
        }

        public override CeresBase.FlowControl.ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if ((targetPropertyname == "Data1" || targetPropertyname == "Data2") && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Connect to data");
            }

            if (targetPropertyname == "Number To Use" && targetValue is int && (int)targetValue <= 0)
            {
                return new ValidationStatus(ValidationState.Fail, "Must be greater than 0");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
