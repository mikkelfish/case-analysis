﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.Projects.Flowable;
using MesosoftCommon.Utilities.Validations;
using CeresBase.FlowControl.Adapters.Matlab;
using ApolloFinal.Tools.BatchProcessable;
using CeresBase.UI;

namespace ApolloFinal.Tools.MixedTools
{
    [AdapterDescription("Raw")]
    class DominantHilbertCoupling : IBatchFlowable
    {
        public override string ToString()
        {
            return "Dominant Hilbert Coupling";
        }

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data1 = new BatchProcessableParameter()
            {
                Name = "Data Set",
                Validator = new NotNullValidator() { ExtraInfo = "Connect to data adapter" }
            };

            BatchProcessableParameter whichData = new BatchProcessableParameter()
            {
                Name = "Held Data"

            };

            BatchProcessableParameter samplingFrequency = new BatchProcessableParameter()
            {
                Name = "Sampling Res.",
                Validator = new NotNullValidator() { ExtraInfo = "Connect to resolution array" }
            };

            BatchProcessableParameter targetFrequency = new BatchProcessableParameter()
            {
                Name = "Target Freq.",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };



            BatchProcessableParameter minFreq = new BatchProcessableParameter()
            {
                Name = "Min. Freq",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter maxFreq = new BatchProcessableParameter()
            {
                Name = "Max Freq",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = minFreq, Path = "Val" }
            };

            BatchProcessableParameter freqBand = new BatchProcessableParameter()
            {
                Name = "Freq Band",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter freqStep = new BatchProcessableParameter()
            {
                Name = "Freq Step",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };



            BatchProcessableParameter windowLength = new BatchProcessableParameter()
            {
                Name = "Timing",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter label = new BatchProcessableParameter()
            {
                Name = "Label"
            };

            // minFreq, maxFreq, freqStep, freqBand, windowSize

            // BatchProcessableParameter 
            //data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers)
            return new BatchProcessableParameter[] { data1, whichData, samplingFrequency, targetFrequency,  minFreq, maxFreq, freqBand, freqStep, windowLength, label };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            float[][] data = parameters.Single(s => s.Name == "Data Set").Val as float[][];
            object held = parameters.Single(s => s.Name == "Held Data").Val;
            double[] samplingRes = parameters.Single(s => s.Name == "Sampling Res.").Val as double[];
            double targetFreq = (double)parameters.Single(s => s.Name == "Target Freq.").Val;
            double minFreq = (double)parameters.Single(s => s.Name == "Min. Freq").Val;
            double maxFreq = (double)parameters.Single(s => s.Name == "Max Freq").Val;
            double freqBand = (double)parameters.Single(s => s.Name == "Freq Band").Val;
            double freqStep = (double)parameters.Single(s => s.Name == "Freq Step").Val;
          
            string label = parameters.Single(s => s.Name == "Label").Val as string;
            object windowPara = parameters.Single(s => s.Name == "Timing").Val;

            bool isInterval = windowPara is float[];
            float[] intervalData = null;
            double windowLength = 0;

            float[] heldData = null;

            if (held is int)
            {
                heldData = data[(int)held];
            }
            else heldData = held as float[];

            float[] otherData = data[0];
            if (data[0] == heldData)
            {
                otherData = data[1];
            }

            AllMatlabNative.All function = MatlabLoader.MatlabFunctions;

            lock (MatlabLoader.MatlabLockable)
            {
                GraphableOutput mgraph = null;
                GraphableOutput domfreqs = null;

                if (isInterval)
                {
                    List<double[]> couplingArray = new List<double[]>();
                    List<double> x = new List<double>();

                    intervalData = windowPara as float[];
                    double startTime = 0;
                    for (int i = 0; i < intervalData.Length; i++)
                    {
                        double endTime = startTime + intervalData[i];

                        int startPoint = (int)(startTime / samplingRes[0]);
                        int endPoint = (int)(endTime / samplingRes[0]);

                        float[] temp = new float[endPoint - startPoint];
                        Array.Copy(data, startPoint, temp, 0, temp.Length);

                        double domFreq = 1.0/intervalData[i];

                        object[] output = 
                            function.runHilbertCross(2, heldData, otherData, 1.0 / samplingRes[0], targetFreq, domFreq, minFreq, maxFreq, freqStep, freqBand, endTime-startTime);

                        double[,] index = output[0] as double[,];
                        double[,] times = output[1] as double[,];
                        double[][] vars = CeresBase.General.Utilities.SquareArrayToDoubleArray<double>(index);

                        couplingArray.Add(vars[0]);
                        x.Add(startTime);

                        startTime = endTime;
                    }
                }
                else
                {
                    #region by window
                    windowLength = (double)windowPara;
                    int numberPoints = (int)(windowLength / samplingRes[0]);

                    List<double[]> couplingArray = new List<double[]>();
                    List<double> x = new List<double>();

                    List<double[][]> ffs = new List<double[][]>();

                    for (int i = 0; i < heldData.Length; i += numberPoints)
                    {
                        if (i + numberPoints > heldData.Length)
                            break;

                        float[] temp = new float[numberPoints];
                        Array.Copy(heldData, i, temp, 0, numberPoints);

                        float[] temp2 = new float[numberPoints];
                        Array.Copy(otherData, i, temp2, 0, numberPoints);

                        float minFreqAvailable = (float)(1.0 / windowLength);

                        double[][] ff = FastFourier.RunFastFourier(256, minFreqAvailable, (float)(targetFreq / 2.0), true, 1.0 / samplingRes[0], temp);
                        int index = -1;
                        double max = double.MinValue;
                        for (int j = 0; j < ff[0].Length; j++)
                        {
                            if (ff[0][j] > max)
                            {
                                max = ff[0][j];
                                index = j;
                            }
                        }

                        ffs.Add(ff);

                        double domFreq = ff[1][index];

                        if (domFreq - freqBand / 2.0 < 0)
                        {
                            domFreq = (freqBand / 2.0) + minFreqAvailable * 2;
                        }

                        object[] output = function.runHilbertCross(2, temp, temp2, 1.0 / samplingRes[0], targetFreq, domFreq, minFreq, maxFreq, freqStep, freqBand, windowLength);

                        double[,] indexes = output[0] as double[,];
                        double[,] times = output[1] as double[,];
                        double[][] vars = CeresBase.General.Utilities.SquareArrayToDoubleArray<double>(indexes);

                        double[] allFreq = new double[vars.Length];
                        for (int j = 0; j < vars.Length; j++)
                        {
                            allFreq[j] = vars[j][0];
                        }

                        couplingArray.Add(allFreq);
                        x.Add(i * samplingRes[0]);
                    }

                    string[] series = new string[couplingArray[0].Length];
                    for (int i = 0; i < series.Length; i++)
                    {
                        series[i] = (minFreq + (i * freqStep)).ToString();
                    }

                    double[][] mY = new double[couplingArray[0].Length][];
                    for (int i = 0; i < mY.Length; i++)
                    {
                        mY[i] = new double[couplingArray.Count];
                        for (int j = 0; j < couplingArray.Count; j++)
                        {
                            mY[i][j] = couplingArray[j][i];
                        }
                    }

                    mgraph = new GraphableOutput()
                    {
                        XData = x.ToArray(),
                        XLabel = "Time",
                        XUnits = "sec",
                        SourceDescription = "Coupling Means",
                        Label = label,
                        YData = mY,
                        SeriesLabels = series
                    };

                    double[][] ffYs = new double[ffs.Count][];
                    string[] seriesFF = new string[ffs.Count];
                    for (int j = 0; j < ffs.Count; j++)
                    {
                        ffYs[j] = ffs[j][0];
                        seriesFF[j] = (j * windowLength).ToString();

                    }
                    domfreqs = new GraphableOutput()
                    {
                        XData = ffs[0][1],
                        XLabel = "Freq",
                        XUnits = "Hz",
                        SourceDescription = "Frequencies",
                        Label = label,
                        YData = ffYs,
                        SeriesLabels = seriesFF

                    };

                    #endregion

                }

                return new object[] { mgraph, domfreqs };
            }
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Indexes", "Dominant Frequencies" }; }
        }

        #endregion
    }
}
