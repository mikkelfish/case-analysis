﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using MesosoftCommon.Utilities.Validations;
using CeresBase.FlowControl.Adapters.Matlab;
using CeresBase.UI;

namespace ApolloFinal.Tools.MixedTools
{
    [AdapterDescription("Raw")]
    class HilbertCouplingAdapter : IBatchFlowable
    {
        public override string ToString()
        {
            return "Hilbert Coupling";
        }

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data1 = new BatchProcessableParameter()
            {
                Name = "Data Set",
                Validator = new NotNullValidator() { ExtraInfo = "Connect to data adapter" }
            };

            BatchProcessableParameter samplingFrequency = new BatchProcessableParameter()
            {
                Name = "Sampling Res.",
                Validator = new NotNullValidator() { ExtraInfo = "Connect to resolution array" }
            };

            BatchProcessableParameter targetFrequency = new BatchProcessableParameter()
            {
                Name = "Target Freq.",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter minFreq = new BatchProcessableParameter()
            {
                Name = "Min. Freq",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter maxFreq = new BatchProcessableParameter()
            {
                Name = "Max Freq",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo=minFreq, Path="Val" }
            };

            BatchProcessableParameter freqBand = new BatchProcessableParameter()
            {
                Name = "Freq Band",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter freqStep = new BatchProcessableParameter()
            {
                Name = "Freq Step",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter windowLength = new BatchProcessableParameter()
            {
                Name = "Window Length",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0, TreatNonComparableAsSuccess=true }
            };

            BatchProcessableParameter label = new BatchProcessableParameter()
            {
                Name = "Label"
            };

            // minFreq, maxFreq, freqStep, freqBand, windowSize

           // BatchProcessableParameter 
            //data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers)
            return new BatchProcessableParameter[] { data1, samplingFrequency, targetFrequency, minFreq, maxFreq, freqBand, freqStep, windowLength, label };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            float[][] data = parameters.Single(s => s.Name == "Data Set").Val as float[][];
            double[] samplingRes = parameters.Single(s => s.Name == "Sampling Res.").Val as double[];
            double targetFreq = (double)parameters.Single(s => s.Name == "Target Freq.").Val;
            double minFreq = (double)parameters.Single(s => s.Name == "Min. Freq").Val;
            double maxFreq = (double)parameters.Single(s => s.Name == "Max Freq").Val;
            double freqBand = (double)parameters.Single(s => s.Name == "Freq Band").Val;
            double freqStep = (double)parameters.Single(s => s.Name == "Freq Step").Val;
            
            string label = parameters.Single(s => s.Name == "Label").Val as string;

            object windowPara = parameters.Single(s => s.Name == "Window Length").Val;

            int[] windowTimings = null;
            double windowLength = 0;

            if (windowPara is double[] || windowPara is float[])
            {
                if (windowPara is double[])
                {
                    windowTimings = new int[(windowPara as double[]).Length];
                    for (int i = 0; i < windowTimings.Length; i++)
                    {
                        windowTimings[i] = Convert.ToInt32((windowPara as double[])[i] * targetFreq);
                    }
                }
                else
                {
                    windowTimings = new int[(windowPara as float[]).Length];
                    for (int i = 0; i < windowTimings.Length; i++)
                    {
                        windowTimings[i] = Convert.ToInt32((windowPara as float[])[i] * targetFreq);
                    }
                }
            }
            else
            {
                windowLength = (double)windowPara;
            }

            AllMatlabNative.All function = MatlabLoader.MatlabFunctions;
            lock (MatlabLoader.MatlabLockable)
            {
                object[] output = function.runHilbert(3, data[0], data[1], 1.0 / samplingRes[0], targetFreq, minFreq, maxFreq, freqStep, freqBand, windowLength, windowTimings);

                double[,] meansA = output[1] as double[,];
                double[,] index = output[0] as double[,];
                double[,] times = output[2] as double[,];

                double[][] means = CeresBase.General.Utilities.SquareArrayToDoubleArray<double>(meansA);
                double[][] vars = CeresBase.General.Utilities.SquareArrayToDoubleArray<double>(index);
                double[] x = CeresBase.General.Utilities.SquareArrayToDoubleArray<double>(times)[0];
                for (int i = 0; i < x.Length; i++)
                {
                    x[i] = x[i] / targetFreq;
                }

                string[] series = new string[means.Length];
                for (int i = 0; i < series.Length; i++)
                {
                    series[i] = (minFreq + (i * freqStep)).ToString();
                }

                GraphableOutput mgraph = new GraphableOutput()
                    {
                        XData = x,
                        XLabel = "Time",
                        XUnits = "sec",
                        SourceDescription = "Coupling Means",
                        Label = label,
                        YData = means,
                        SeriesLabels = series
                    };

                GraphableOutput igraph = new GraphableOutput()
                {
                    XData = x,
                    XLabel = "Time",
                    XUnits = "sec",
                    SourceDescription = "Coupling Indexes",
                    Label = label,
                    YData = vars,
                    SeriesLabels = series
                };

                return new object[] { mgraph, igraph };
            }
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Indexes", "Means" }; }
        }

        #endregion
    }
}
