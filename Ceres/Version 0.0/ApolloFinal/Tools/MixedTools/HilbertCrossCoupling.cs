﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using MesosoftCommon.Utilities.Validations;
using CeresBase.FlowControl.Adapters.Matlab;
using CeresBase.UI;

namespace ApolloFinal.Tools.MixedTools
{
    [AdapterDescription("Raw")]
    class HilbertCrossCouplingAdapter : IBatchFlowable
    {
        public override string ToString()
        {
            return "Hilbert Cross Coupling";
        }

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data1 = new BatchProcessableParameter()
            {
                Name = "Data Set",
                Validator = new NotNullValidator() { ExtraInfo = "Connect to data adapter" }
            };

            BatchProcessableParameter whichData = new BatchProcessableParameter()
            {
                Name = "Held Data",
                Val = 0

            };

            BatchProcessableParameter samplingFrequency = new BatchProcessableParameter()
            {
                Name = "Sampling Res.",
                Validator = new NotNullValidator() { ExtraInfo = "Connect to resolution array" }
            };

            BatchProcessableParameter targetFrequency = new BatchProcessableParameter()
            {
                Name = "Target Freq.",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter baseFreq = new BatchProcessableParameter()
            {
                Name = "Comparison Freq",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };


            BatchProcessableParameter minFreq = new BatchProcessableParameter()
            {
                Name = "Min. Freq",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter maxFreq = new BatchProcessableParameter()
            {
                Name = "Max Freq",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = minFreq, Path = "Val" }
            };

            BatchProcessableParameter freqBand = new BatchProcessableParameter()
            {
                Name = "Freq Band",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter freqStep = new BatchProcessableParameter()
            {
                Name = "Freq Step",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };



            BatchProcessableParameter windowLength = new BatchProcessableParameter()
            {
                Name = "Window Length",
                Val = 0.0,
                Validator = new ComparisonValidator() { Operator = ComparisonValidator.Comparison.GreaterThan, CompareTo = 0.0 }
            };

            BatchProcessableParameter label = new BatchProcessableParameter()
            {
                Name = "Label"
            };

            // minFreq, maxFreq, freqStep, freqBand, windowSize

            // BatchProcessableParameter 
            //data1, data2, dataFreq, targetFreq, data1Freq, minFreq, maxFreq, freqStep, freqBand, windowSize, triggers)
            return new BatchProcessableParameter[] { data1, whichData, samplingFrequency, targetFrequency, baseFreq, minFreq, maxFreq, freqBand, freqStep, windowLength, label };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            float[][] data = parameters.Single(s => s.Name == "Data Set").Val as float[][];
            object held = parameters.Single(s => s.Name == "Held Data").Val;
            double[] samplingRes = parameters.Single(s => s.Name == "Sampling Res.").Val as double[];
            double targetFreq = (double)parameters.Single(s => s.Name == "Target Freq.").Val;
            double compare = (double)parameters.Single(s => s.Name == "Comparison Freq").Val;
            double minFreq = (double)parameters.Single(s => s.Name == "Min. Freq").Val;
            double maxFreq = (double)parameters.Single(s => s.Name == "Max Freq").Val;
            double freqBand = (double)parameters.Single(s => s.Name == "Freq Band").Val;
            double freqStep = (double)parameters.Single(s => s.Name == "Freq Step").Val;
            double windowLength = (double)parameters.Single(s => s.Name == "Window Length").Val;
            string label = parameters.Single(s => s.Name == "Label").Val as string;

            AllMatlabNative.All function = MatlabLoader.MatlabFunctions;

            lock (MatlabLoader.MatlabLockable)
            {

                float[] heldData = null;

                if (held is int)
                {
                    heldData = data[(int)held];
                }
                else heldData = held as float[];

                float[] otherData = data[0];
                if (data[0] == heldData)
                {
                    otherData = data[1];
                }

                object[] output = function.runHilbertCross(2, heldData, otherData, 1.0 / samplingRes[0], targetFreq, compare, minFreq, maxFreq, freqStep, freqBand, windowLength);

                double[,] index = output[0] as double[,];
                double[,] times = output[1] as double[,];

                double[][] vars = CeresBase.General.Utilities.SquareArrayToDoubleArray<double>(index);
                double[] x = CeresBase.General.Utilities.SquareArrayToDoubleArray<double>(times)[0];

                string[] series = new string[vars.Length];
                for (int i = 0; i < series.Length; i++)
                {
                    series[i] = (minFreq + (i * freqStep)).ToString();
                }

                GraphableOutput mgraph = new GraphableOutput()
                {
                    XData = x,
                    XLabel = "Time",
                    XUnits = "sec",
                    SourceDescription = "Coupling Means " + Math.Round(compare, 2).ToString(),
                    Label = label,
                    YData = vars,
                    SeriesLabels = series
                };

                return new object[] { mgraph };
            }
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Indexes" }; }
        }

        #endregion
    }
}
