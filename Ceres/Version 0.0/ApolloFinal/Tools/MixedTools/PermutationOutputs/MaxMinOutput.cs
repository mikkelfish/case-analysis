﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;

namespace ApolloFinal.Tools.MixedTools.PermutationOutputs
{
    [AdapterDescription("Mixed")]
    public class MaxMinOutput : AssociateAdapterBase //, IPermutationOutputAdapter
    {
        public override string Name
        {
            get
            {
                return "Find Max/Min";
            }
            set
            {
               
            }
        }

        public override string Category
        {
            get
            {
                return "Permutation Outputs";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("Data")]
        public object Data { get; set; }

        [AssociateWithProperty("Mins", IsOutput = true)]
        public object Mins { get; set; }

        [AssociateWithProperty("Maxes", IsOutput=true)]
        public object Maxes { get; set; }

        private object lockable = new object();

        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        private bool isSingle = false;

        public override void RunAdapter()
        {
            if (this.Mins == null)
            {
                float[][] data;
                float[] mins;
                float[] max;
                this.getMinsAndMaxes(this.Data, out data, out mins, out max);

                this.Mins = mins;
                this.Maxes = max;
            }

            if (isSingle)
            {
                this.Mins = (this.Mins as float[])[0];
                this.Maxes = (this.Maxes as float[])[0];
            }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
        }

        public override object Clone()
        {
            return new MaxMinOutput();
        }

        public override CeresBase.FlowControl.ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Connect to data adapter");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }

        #region IPermutationOutputAdapter Members

        public void ReceivePropertyValue(object propertyValue, string targetPropertyName, int forIteration)
        {
            
            if (targetPropertyName != "Data") return;

            float[][] data;
            float[] min;
            float[] max;
            getMinsAndMaxes(propertyValue, out data, out min, out max);

            lock (this.lockable)
            {
                if (this.Mins == null)
                {
                    this.Mins = new float[data.Length];
                    this.Maxes = new float[data.Length];

                    for (int i = 0; i < data.Length; i++)
                    {
                        (this.Mins as float[])[i] = float.MaxValue;
                        (this.Maxes as float[])[i] = float.MinValue;
                    }
                }

                for (int i = 0; i < data.Length; i++)
                {
                    if (min[i] < (this.Mins as float[])[i])
                        (this.Mins as float[])[i] = min[i];
                    if (max[i] > (this.Maxes as float[])[i])
                        (this.Maxes as float[])[i] = max[i];
                }
            }
        }

        private void getMinsAndMaxes(object propertyValue, out float[][] data, out float[] min, out float[] max)
        {
            data = null;
            if (propertyValue is float[][])
            {
                data = propertyValue as float[][];
                isSingle = false;
            }
            else
            {
                data = new float[1][] { propertyValue as float[] };
                isSingle = true;
            }

            min = new float[data.Length];
            max = new float[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                min[i] = float.MaxValue;
                max[i] = float.MinValue;
                for (int j = 0; j < data[i].Length; j++)
                {
                    if (data[i][j] < min[i])
                        min[i] = data[i][j];
                    if (data[i][j] > max[i])
                        max[i] = data[i][j];
                }
            }
        }

        #endregion
    }
}
