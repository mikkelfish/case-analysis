﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;
using ApolloFinal.Tools.IntervalTools;
using CeresBase.Projects;
using ApolloFinal;

namespace ApolloFinal.Tools.MixedTools
{
    [AdapterDescription("Mixed")]
    class MixedDataAdapter : DataAdapter
    {
        public override string Name
        {
            get
            {
                return "Mixed Data Adapter";
            }
            set
            {

            }
        }

        public override string Category
        {
            get
            {
                return "Data Sources";
            }
            set
            {

            }
        }

        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public IntervalPackage[] RawPackages { get; set; }
        public IntervalPackage[] Packages { get; set; }
        public Epoch[] Epochs { get; set; }
        private Dictionary<string, float[]> data = new Dictionary<string, float[]>();

        [AssociateWithProperty("Underlying Vars")]
        public SpikeVariable[] UnderlyingVariables 
        {
            get
            {
                List<SpikeVariable> vars = new List<SpikeVariable>();

                if (this.Epochs != null)
                {
                    vars.AddRange(this.Epochs.Select(e => e.Variable).Cast<SpikeVariable>().ToArray());
                }
              
                return vars.ToArray();
            }
        }

        [AssociateWithProperty("Epoch Descriptions", IsOutput=true)]
        public string[] EpochDescription
        {
            get
            {
                if (this.Epochs == null) return null;

                return this.Epochs.Select(t => t.FullDescription).ToArray();
            }
        }



        [AssociateWithProperty("Raw Data", IsOutput = true)]
        public float[][] RawData
        {
            get
            {
                if (this.Epochs == null) return null;

                List<float[]> toRet = new List<float[]>();
                for (int i = 0; i < this.Epochs.Length; i++)
                {
                    if (!data.ContainsKey(this.Epochs[i].ToString()))
                    {
                        //data.Add(this.Epochs[i].ToString(), SpikeVariable.ReadData(this.Epochs[i].Variable as SpikeVariable, this.Epochs[i].BeginTime, this.Epochs[i].EndTime));
                        data.Add(this.Epochs[i].ToString(), (this.Epochs[i].Variable as SpikeVariable).GetTimeSeriesData(this.Epochs[i].BeginTime, this.Epochs[i].EndTime));
                    
                    }


                    toRet.Add(data[this.Epochs[i].ToString()]);
                }

                return toRet.ToArray();
            }
        }




        [AssociateWithProperty("Raw Names", IsOutput = true)]
        public string[] RawNames
        {
            get
            {
                if (this.Epochs == null) return null;

                return this.Epochs.Select(t => t.Variable.Header.Description).ToArray();
            }
        }

        [AssociateWithProperty("Raw Resolutions", IsOutput = true)]
        public double[] RawResolutions 
        { 
            get 
            {
                if (this.Epochs == null) return null;
                return this.Epochs.Select(t => (t.Variable as SpikeVariable).Resolution).ToArray(); 
            } 
        }


        [AssociateWithProperty("Raw Trigger Timings", IsOutput = true)]
        public float[][][] RawTriggerTimings
        {
            get
            {
                if (this.RawPackages == null) return null;
                List<float[][]> toRet = new List<float[][]>();

                for (int i = 0; i < this.Epochs.Length; i++)
                {
                    float[][] timings = this.RawPackages.Where(p => p.DataVariable.FullDescription == this.Epochs[i].Variable.FullDescription).Select(p => p.GetAllIntervalTimings()).FirstOrDefault();
                    if (timings == null)
                    {
                        toRet.Add(new float[0][]);
                    }
                    else
                    {
                        toRet.Add(timings);
                    }
                }
          
                return toRet.ToArray();
                
                //return this.RawPackages.Select<IntervalPackage, float[][]>(t => t.GetAllIntervalTimings()).ToArray();
            }
        }

        [AssociateWithProperty("Raw Trigger Names", IsOutput = true)]
        public string[][] RawTriggerNames
        {
            get
            {
                if (this.RawPackages == null) return null;

                List<string[]> toRet = new List<string[]>();
                int curSpot = 0;

                for (int i = 0; i < this.Epochs.Length; i++)
                {
                    string[] names = this.RawPackages.Where(p => p.DataVariable.FullDescription == this.Epochs[i].Variable.FullDescription).Select(p => p.IntervalNames).FirstOrDefault();
                    if (names == null)
                    {
                        toRet.Add(new string[0]);
                    }
                    else
                    {
                        toRet.Add(names);
                    }
                }

                return toRet.ToArray();

                //return this.RawPackages.Select<IntervalPackage, string[]>(t => t.IntervalNames).ToArray();
            }
        }

        [AssociateWithProperty("Raw Start Times", IsOutput = true)]
        public float[] RawStarts
        {
            get
            {
                if (this.Epochs == null) return null;
                return this.Epochs.Select(t => t.BeginTime).ToArray();
            }
        }
        [AssociateWithProperty("Raw End Times", IsOutput = true)]
        public float[] RawEnds
        {
            get
            {
                if (this.Epochs == null) return null;

                return this.Epochs.Select(t => t.EndTime).ToArray();
            }
        }

      
        [AssociateWithProperty("Interval Timings", IsOutput = true)]
        public float[][][] IntervalTimings
        {
            get
            {
                if (this.Packages == null) return null;
                return this.Packages.Select<IntervalPackage, float[][]>(t => t.GetAllIntervalTimings()).ToArray();
            }
        }


        [AssociateWithProperty("Interval Names", IsOutput = true)]
        public string[][] IntervalNames
        {
            get
            {
                if (this.Packages == null) return null;
                return this.Packages.Select(t => t.IntervalNames).ToArray();
            }
        }

        [AssociateWithProperty("Interval Start Times", IsOutput = true)]
        public float[][] IntervalStarts
        {
            get
            {
                if (this.Packages == null) return null;
                return this.Packages.Select(t => t.BeginTimes).ToArray();
            }
        }
        [AssociateWithProperty("Interval End Times", IsOutput = true)]
        public float[][] IntervalEnds
        {
            get
            {
                if (this.Packages == null) return null;
                return this.Packages.Select(t => t.EndTimes).ToArray();
            }
        }

        public override object Clone()
        {
            MixedDataAdapter adapter = new MixedDataAdapter();
            adapter.Packages = this.Packages;
            adapter.Epochs = this.Epochs;
            this.cloneHelper(adapter);

            return adapter;
        }
    }
}
