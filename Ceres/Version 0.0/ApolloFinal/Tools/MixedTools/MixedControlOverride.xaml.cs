﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using CeresBase.Projects;
using ApolloFinal.Tools.IntervalTools;
using MesosoftCommon.DataStructures;
using MesosoftCommon.IO;

namespace ApolloFinal.Tools.MixedTools
{
    /// <summary>
    /// Interaction logic for MixedControlOverride.xaml
    /// </summary>
    public partial class MixedControlOverride : UserControl
    {
        private BeginInvokeObservableCollection<Epoch> invoke;

        private ObservableCollection<Epoch> epochList = new ObservableCollection<Epoch>();
        public ObservableCollection<Epoch> EpochList
        {
            get
            {
                return epochList;
            }
        }

        void MixedControlOverride_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            refresh();
        }

        private void refresh()
        {
            this.epochList.Clear();
            foreach (Epoch epoch in this.invoke)
            {
                if (epoch.BeginTime != epoch.EndTime)
                    epochList.Add(epoch);
            }

            BindingExpression ex = this.dataTree.GetBindingExpression(GenericDisplayHierarchyTree.ItemsProperty);
            if (ex != null)
            {
                ex.UpdateTarget();
            }
        }


        public MixedControlOverride()
        {
            this.invoke = EpochCentral.BindToManager(this.Dispatcher);
            this.invoke.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(MixedControlOverride_CollectionChanged);
            InitializeComponent();
            this.refresh();
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            Epoch epoch = e.Item as Epoch;
            if (epoch.BeginTime == epoch.EndTime || epoch.AppliesToExperiment)
            {
                e.Accepted = false;
            }
            else e.Accepted = true;
        }

        private bool filter(GenericDisplayItem item)
        {
            return true;
        }
    }
}
