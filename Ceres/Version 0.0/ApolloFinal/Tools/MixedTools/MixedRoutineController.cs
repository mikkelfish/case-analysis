﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl;
using MesosoftCommon.DataStructures;
using CeresBase.Projects;
using System.Windows.Data;
using CeresBase.Data;
using System.Windows;
using System.Windows.Controls;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters.Matlab;
using CeresBase.FlowControl.Adapters;
using MesosoftCommon.IO;
using ApolloFinal.Tools.IntervalTools;

namespace ApolloFinal.Tools.MixedTools
{
    public class MixedRoutineController : IRoutineController
    {
        private class epochSet
        {
            public IntervalPackage[] RawTriggers { get; set; }
            public IntervalPackage[] Package { get; set; }
            public Epoch[] Epochs { get; set; }
            public string Display { get; set; }

            public override string ToString()
            {
                return this.Display;
            }
        }

        public MixedRoutineController(RoutineBatchControl control)
        {
            this.BatchControl = control;
        }

        
        #region IRoutineController Members

        private MixedControlOverride routineUI;

        public System.Windows.FrameworkElement GetContentOverride()
        {
            this.routineUI = new MixedControlOverride();

            //Binding binder = new Binding();
            //binder.Source = this.EpochList;
            //binder.Converter = new DisplayHierarchyConverter();
            //binder.ConverterParameter = "Variable.Header.Experiment:\\";

            //CollectionViewSource source = new CollectionViewSource();
            //BindingOperations.SetBinding(source, CollectionViewSource.SourceProperty, binder);
            //source.Filter += new FilterEventHandler(source_Filter);

            //this.routineUI.tree.ItemsSource = source.View;
            return this.routineUI.Content as FrameworkElement;
        }

        public void AdaptersInitialized(Routine routine, AdapterInitPair[] pairs)
        {
            var sortedAdapters = from AdapterInitPair p in pairs
                                 orderby p.Adapter.ID
                                 select p;
            foreach (AdapterInitPair adapter in sortedAdapters)
            {
                if (adapter.Adapter is MixedDataAdapter)
                {
                    MixedDataAdapter adapt = adapter.Adapter as MixedDataAdapter;


                    epochSet set = routine.Args as epochSet;

                    adapt.Epochs = set.Epochs;
                    adapt.Packages = set.Package;
                    adapt.RawPackages = set.RawTriggers;
                }
            }
        }

        public void AddAvailableAdapters(RoutineManager rm)
        {
            MatlabLoader.BuildAdapterInfos("Mixed");
            MatlabLoader.BuildAdapterInfos("Raw");
            MatlabLoader.BuildAdapterInfos("Interval");

            for (int i = 0; i < 3; i++)
            {
                string toMake = "";
                if (i == 0) toMake = "Mixed";
                else if (i == 1) toMake = "Raw";
                else toMake = "Interval";
                MatlabAdapter[] matlabs = MatlabLoader.CreateAdapters(toMake);
                if (matlabs != null)
                {
                    foreach (MatlabAdapter mlab in matlabs)
                    {
                        rm.AvailableAdapters.Add(mlab);
                    }
                }
            }


            IFlowControlAdapter[] adapters = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IFlowControlAdapter>(null);
            foreach (IFlowControlAdapter adapter in adapters)
            {
                if (adapter is ApolloFinal.Tools.BatchProcessable.RawDataAdapter ||
                    adapter is ApolloFinal.Tools.IntervalTools.IntervalGroupDataAdapter)
                    continue;

                AdapterDescriptionAttribute[] attr = MesosoftCommon.Utilities.Reflection.Common.GetAttributes<AdapterDescriptionAttribute>(adapter);
                if (attr == null) continue;
                if (attr.Any(at => at.Description == "Mixed" || at.Description == "Raw" || at.Description == "Interval") && !rm.AvailableAdapters.Any(ad => ad.Name == adapter.Name && ad.Category == adapter.Category))
                {
                    rm.AvailableAdapters.Add(adapter);
                }
            }

            IBatchFlowable[] flows = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IBatchFlowable>(null);
            foreach (IBatchFlowable adapter in flows)
            {
                AdapterDescriptionAttribute[] attr = MesosoftCommon.Utilities.Reflection.Common.GetAttributes<AdapterDescriptionAttribute>(adapter);
                if (attr == null) continue;
                if (attr.Any(at => at.Description == "Mixed" || at.Description == "Raw" || at.Description == "Interval") && !rm.AvailableAdapters.Any(ad => ad.Name == adapter.ToString() && ad.Category == adapter.ToString()))
                {
                    rm.AvailableAdapters.Add(new FlowableBatchProcessorAdapter(adapter));
                }
            }
        }

        private void addToList(List<epochSet> epochs, GenericDisplayItem item)
        {
            if (item.Children.Count != 0 && item.Children[0].Children.Count == 0)
            {
                List<Epoch> toRet = new List<Epoch>();
                foreach (GenericDisplayItem child in item.Children)
                {
                    if(child.IsChecked)
                        toRet.Add(child.Value as Epoch);
                }
                
                if(toRet.Count != 0)
                    epochs.Add(new epochSet() { Epochs = toRet.ToArray(), Display = item.Path });
            }
            else
            {
                List<Epoch[]> toRet = new List<Epoch[]>();
                foreach (GenericDisplayItem child in item.Children)
                {
                    this.addToList(epochs, child);
                }
            }
        }

        public void AddButtonClicked()
        {
            List<epochSet> epochs = new List<epochSet>();
            foreach (GenericDisplayItem item in this.routineUI.dataTree.Items)
            {
                this.addToList( epochs, item);
            }

            foreach (epochSet epoch in epochs)
            {
                Epoch[] intervalEpochs = epoch.Epochs.Where(e => (e.Variable as SpikeVariable).SpikeType == SpikeVariableHeader.SpikeType.Pulse).ToArray();
                Epoch[] otherEpochs = epoch.Epochs.Where(e => (e.Variable as SpikeVariable).SpikeType != SpikeVariableHeader.SpikeType.Pulse).ToArray();

                if (MessageBox.Show("Include Raw Scoring?", "Answer", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    epoch.Package = IntervalPackage.CreatePackages(intervalEpochs, false);
                    epoch.RawTriggers = IntervalPackage.CreatePackages(otherEpochs, false);
                }
                else
                {
                    epoch.Package = new IntervalPackage[0];
                    epoch.RawTriggers = new IntervalPackage[0];
                }
                epoch.Epochs = otherEpochs;

                this.BatchControl.ToRunList.Add(epoch);
            }
            //this.BatchControl.ToRunList.Add(this.routineUI.tree);
        }

        public RoutineBatchControl BatchControl
        {
            get;
            private set;
        }

        #endregion
    }
}
