﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;

namespace ApolloFinal.Tools.MixedTools
{
    [AdapterDescription("Mixed")]
    public class FullVariablePermutation : AssociateAdapterBase, IPermutationAdapter
    {
        public override string Name
        {
            get
            {
                return "Full Variable Permutation";
            }
            set
            {
               
            }
        }

        [AssociateWithProperty("Full Variables")]
        public SpikeVariable[] Variables { get; set; }

        [AssociateWithProperty("Time Window")]
        public double TimeWindow { get; set; }


        [AssociateWithProperty("Data", IsOutput=true)]
        public float[][] Data { get; set; }

        [AssociateWithProperty("Iteration Number", IsOutput = true)]
        public int Iteration { get; set; }

        public override string Category
        {
            get
            {
                return "Variable Permutations";
            }
            set
            {
               
            }
        }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            this.Iteration = this.iteration;
            float[][] toRet = new float[this.Variables.Length][];
            for (int i = 0; i < toRet.Length; i++)
            {
                toRet[i] = 
                    this.Variables[i].GetTimeSeriesData(this.TimeWindow * this.iteration, this.TimeWindow * this.iteration + this.TimeWindow);
            }

            this.Data = toRet;
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
           
        }

        public override object Clone()
        {
            FullVariablePermutation perm = new FullVariablePermutation();
            perm.Variables = this.Variables;
            perm.TimeWindow = this.TimeWindow;
            return perm;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Time Window" && targetValue is double && (double)targetValue == 0)
            {
                return new ValidationStatus(ValidationState.Fail, "Window must be greater than 0");
            }

            if (targetPropertyname == "Variables" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Connect to 'Underlying Vars'");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }

        #region IPermutationAdapter Members

        public int IterationCount
        {
            get 
            {
                if (this.TimeWindow == 0) return 0;
                double min = this.Variables.Min(e => e.TotalLength);
                return (int)(min / this.TimeWindow);
            }
        }

        private int iteration;
        public void SetIteration(int iteration)
        {
            this.iteration = iteration;
        }

        #endregion
    }
}
