﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using CeresBase.UI;

namespace ApolloFinal.Tools.MixedTools
{
    [AdapterDescription("Mixed")]
    class RSADetection : IBatchFlowable
    {
        public override string ToString()
        {
            return "RSA Statistic";
        }

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter heart = new BatchProcessableParameter()
            {
                Name="R Timings",
                TargetType = typeof(float[]),
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator() { ExtraInfo = "Connect to inspiration timeseries." }
            };

            BatchProcessableParameter breathing = new BatchProcessableParameter()
            {
                Name = "Inspiration Timings",
                TargetType = typeof(float[]),
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator() { ExtraInfo = "Connect to inspiration timeseries." }
            };

            return new BatchProcessableParameter[] { heart, breathing };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {

                float[] rs = parameters.Single(s => s.Name == "R Timings").Val as float[];
                float[] ins = parameters.Single(s => s.Name == "Inspiration Timings").Val as float[];

                List<float>[] extracted = new List<float>[4];
                for (int i = 0; i < extracted.Length; i++)
                {
                    extracted[i] = new List<float>();
                }

                int index1 = 0;

                for (int i = 0; i < rs.Length - 3; i++)
                {
                    if (rs[i] > ins[index1])
                    {
                        index1++;
                        continue;
                    }

                    if (rs[i] < ins[index1] && rs[i + 1] >= ins[index1])
                    {
                        index1++;

                        if (i < 1) continue;


                        float prev = rs[i] - rs[i - 1];
                        float zero = rs[i + 1] - rs[i];
                        float next = rs[i + 2] - rs[i + 1];
                        float secondnext = rs[i + 3] - rs[i + 2];
                        float total = prev + zero + next + secondnext;

                        extracted[0].Add(prev / total);
                        extracted[1].Add(zero / total);
                        extracted[2].Add(next/total);
                        extracted[3].Add(secondnext/total);

                        if (index1 >= ins.Length)
                            break;
                    }
                }

                float[] means = extracted.Select(s => s.Average()).ToArray();
                float[] sds = extracted.Select(s => CeresBase.MathConstructs.Stats.StandardDev(s.ToArray())).ToArray();
                float index = (means[3] + means[2]) / (means[1] + means[0]);


                GraphableOutput output = new GraphableOutput();
                output.XData = new double[] { -1, 0, 1, 2 };
                output.YData = new double[][] { new double[] { means[0], means[1], means[2], means[3] }, new double[] { sds[0], sds[1], sds[2], sds[3] } };
                output.ExtraInfo = new object[] { "Index: " + index.ToString() };
                output.SeriesLabels = new string[] { "Means", "SDs" };
                output.SourceDescription = "RSA Detection";
                output.XLabel = "Insp. Offset RR";
                return new object[] { output };

        }

        public string[] OutputDescription
        {
            get { return new string[] { "RSA Stats" }; }
        }

        #endregion
    }
}
