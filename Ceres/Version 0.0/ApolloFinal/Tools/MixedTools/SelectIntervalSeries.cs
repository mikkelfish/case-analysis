﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.Projects.Flowable;
using MesosoftCommon.Utilities.Validations;
using CeresBase.FlowControl.Adapters.Permutations.Output;
using System.Windows.Controls;

namespace ApolloFinal.Tools.MixedTools
{
    [AdapterDescription("Mixed")]
    class SelectIntervalSeries : IBatchFlowable, IRoutineCategoryProvider, IUIProvider
    {
        private class intervalViewer : IBatchViewable
        {

            public string Label { get; set; }

            #region IBatchViewable Members

            public System.Windows.Controls.Control[] View(IBatchViewable[] viewable, out IBatchViewable[] notUsed)
            {
                IBatchViewable[] allInts = viewable.Where(s => s is intervalViewer).ToArray();
                List<Control> toRet = new List<Control>();
                foreach (intervalViewer view in allInts)
                {
                    toRet.Add(new Label() { Content = view.Label });
                }

                notUsed = viewable.Where(s => !(s is intervalViewer)).ToArray();

                return toRet.ToArray();
            }

            #endregion
        }

        

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter rawNames = new BatchProcessableParameter()
            {
                Name = "Raw Names",
                Validator = new NotNullValidator() { ExtraInfo = "Connect to data source" }
            };

            BatchProcessableParameter intNames = new BatchProcessableParameter()
            {
                Name = "Interval Names",
                Validator = new NotNullValidator() { ExtraInfo = "Connect to data source" }
            };

            BatchProcessableParameter intTimings = new BatchProcessableParameter()
            {
                Name = "Interval Timings",
                Validator = new NotNullValidator() { ExtraInfo = "Connect to data source" }
            };

            BatchProcessableParameter numSel = new BatchProcessableParameter()
            {
                Name = "Number To Select",
                Val = 0
            };


            return new BatchProcessableParameter[] { rawNames, intNames, intTimings, numSel };
        }


        public object[] Run(BatchProcessableParameter[] parameters)
        {
            PickOneOutput output = new PickOneOutput();
            output.MustBeEqual = false;

            string[] rawNames = parameters.Single(p => p.Name == "Raw Names").Val as string[];
            string[][] intNames = parameters.Single(p => p.Name == "Interval Names").Val as string[][];
            float[][][] intTimes = parameters.Single(p => p.Name == "Interval Timings").Val as float[][][];
            int numToSel = (int)parameters.Single(p => p.Name == "Number To Select").Val;

            output.NumberToSelect = numToSel;
            if (numToSel != 0)
            {
                output.MustBeEqual = true;
            }

            List<IBatchViewable> views = new List<IBatchViewable>();
            List<float[]> data = new List<float[]>();

            for (int i = 0; i < rawNames.Length; i++)
            {
                for (int j = 0; j < intNames[i].Length; j++)
                {
                    views.Add(new intervalViewer() { Label = rawNames[i] + " " + intNames[i][j] });
                    data.Add(intTimes[i][j]);
                }
            }

            //output.Data = data.ToArray();
            //output.Views = views.ToArray();

            output.RunAdapter(views.ToArray(), data.ToArray());

            return new object[] { output.SelectedOutput };
        }

        public override string ToString()
        {
            return "Select Series";
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Selected Series" }; }
        }

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Selectors"; }
        }

        #endregion

        #region IUIProvider Members

        public bool HasUserInteraction
        {
            get { return true; }
        }

        #endregion
    }
}
