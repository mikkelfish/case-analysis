﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl.Adapters.Matlab;
using System.Windows.Forms;

namespace ApolloFinal.Tools.MixedTools.Histograms
{
    [AdapterDescription("Mixed")]
    public class CycleTriggeredHistogram : IBatchFlowable, IRoutineCategoryProvider
    {
        public class CycleTriggeredHist : BaseHistogram
        {
            public CycleTriggeredHist(BatchProcessableParameter[] paras)
                : base(paras)
            {

            }

            public float[] Triggers { get; private set; }
            public float[] Data { get; private set; }
            public bool DataIsRaw { get; private set; }
            public float RawResolution { get; set; }

            public string Label { get; set; }

            public void MakeHistogram(float[] trigger, float[] data, bool dataIsRaw)
            {
                float firstTrigger = trigger[0];
                for (int i = 0; i < trigger.Length; i++)
                {
                    trigger[i] -= firstTrigger;
                }

                float[] triggerLengths = new float[trigger.Length - 1];
                for (int i = 0; i < triggerLengths.Length; i++)
                {
                    triggerLengths[i] = trigger[i + 1] - trigger[i];
                }

                this.DataIsRaw = dataIsRaw;

                this.Data = data;
                this.Triggers = trigger;

                float mean = CeresBase.MathConstructs.Stats.Average(triggerLengths);
                float sd = CeresBase.MathConstructs.Stats.StandardDev(triggerLengths);
                bool[] include = new bool[triggerLengths.Length];

                int realCycles = 0;
                float[] mults = new float[triggerLengths.Length];
                for (int i = 0; i < mults.Length; i++)
                {
                    mults[i] = mean / triggerLengths[i];
                    if (triggerLengths[i] < mean - sd * this.SDMult || triggerLengths[i] > mean + sd * this.SDMult)
                    {
                        include[i] = false;
                        this.ExcludedCycles++;
                    }
                    else
                    {
                        include[i] = true;
                        realCycles++;
                    }
                }

                List<float> times = new List<float>();
                List<float> rawData = new List<float>();
                float amountToAdd = 0;
                float curTime = 0;
                int otherIndex = 0;
                for (int i = 0; i < trigger.Length - 1; i++)
                {
                    if (!include[i])
                        continue;

                    while (true)
                    {
                        if (this.DataIsRaw)
                        {
                            int rawIndex = (int)(curTime / this.RawResolution);

                            //Don't go over the edge of the rawData
                            if (rawIndex >= data.Length)
                            {
                                break;
                            }

                            //Make sure to continue to the next trigger
                            if (curTime < trigger[i])
                            {
                                curTime += this.RawResolution;
                                continue;
                            }

                            //Stop once there is a new trigger
                            if (curTime >= trigger[i + 1])
                            {
                                //  amountToAdd += mean;
                                break;
                            }

                            rawData.Add(data[rawIndex]);
                            curTime += this.RawResolution;
                        }
                        else
                        {
                            //Don't go over the edge of the data triggers
                            if (otherIndex >= data.Length)
                                break;

                            //Make sure to continue until it gets to this trigger
                            if (data[otherIndex] < trigger[i])
                            {
                                otherIndex++;
                                continue;
                            }

                            //Stop once there is a new trigger
                            if (data[otherIndex] >= trigger[i + 1])
                            {
                                // amountToAdd += mean;
                                break;
                            }

                            curTime = data[otherIndex];
                        }


                        times.Add((curTime - trigger[i]) * mults[i] + amountToAdd);
                    }

                    amountToAdd += mean;
                }

                int firstGood = 0;

                float[] binSums = new float[this.BinCounts.Length];
                int loopNum = 1;
                if (this.DataIsRaw)
                    loopNum++;

                //  base.BinSDS = new double[this.BinCounts.Length];
                for (int loop = 0; loop < loopNum; loop++)
                {
                    double reference = this.Offset;
                    for (int i = 0; i < realCycles; i++)
                    {
                        for (int j = firstGood; j < times.Count; j++)
                        {
                            if (reference > times[j])
                            {
                                firstGood++;
                                continue;
                            }
                            if (times[j] > reference + this.TotalTime) break;

                            int bin = (int)Math.Round(((times[j] - reference) / this.BinWidth), 0);
                            if (bin < this.BinCounts.Length)
                            {
                                if (this.DataIsRaw)
                                {
                                    if (loop == 0)
                                    {
                                        binSums[bin] += rawData[j];
                                        this.BinCounts[bin]++;
                                    }
                                    else
                                        this.BinSDS[bin] += (rawData[j] - binSums[bin]) * (rawData[j] - binSums[bin]);
                                }
                                else this.BinCounts[bin]++;
                            }
                        }
                        reference += mean;
                    }

                    if (this.DataIsRaw)
                    {
                        for (int i = 0; i < binSums.Length; i++)
                        {
                            if (this.BinCounts[i] != 0)
                            {
                                if (loop == 0)
                                {
                                    binSums[i] /= (float)this.BinCounts[i];
                                }
                                else
                                {
                                    //Finish the SD calculation and copy the bin average into bincount like the histogram calls for
                                    this.BinSDS[i] /= this.BinCounts[i];
                                    this.BinSDS[i] = (float)Math.Sqrt(this.BinSDS[i]);
                                    this.BinCounts[i] = binSums[i];
                                }
                            }
                        }
                    }

                }
                  double min = this.BinCounts.Min();
                    double sum = this.BinCounts.Sum(c => c-min);
                    for (int  i = 0; i < this.BinCounts.Length; i++)
                    {
                        this.BinCounts[i] = (this.BinCounts[i] - min) / sum;
                    }
            }             
        }

        public class CycleTriggeredCollection : IBatchWritable //, IBatchViewable
        {
            public string Label { get; set; }

            private List<CycleTriggeredHist> hists = new List<CycleTriggeredHist>();
            public List<CycleTriggeredHist> Hists
            {
                get
                {
                    return this.hists;
                }
            }

            public CycleTriggeredCollection()
            {

            }

            //#region IBatchViewable Members

            //public System.Windows.Controls.Control[] View(IBatchViewable[] viewable, out IBatchViewable[] notUsed)
            //{
            //    throw new NotImplementedException();
            //}

            //#endregion

            #region IBatchWritable Members

            public IBatchWritable[] WriteToDisk(IBatchWritable[] toWrite)
            {
                string[] colors = new string[] {  "r", "none", "b" };
                string[] lcolors = new string[] { "r", "k", "b" };

                IBatchWritable[] toPass = toWrite.Where(s => !(s is CycleTriggeredCollection)).ToArray();
                CycleTriggeredCollection[] collections = toWrite.Where(s => s is CycleTriggeredCollection).Cast<CycleTriggeredCollection>().ToArray();

                FolderBrowserDialog diag = new FolderBrowserDialog();
                if (diag.ShowDialog() == DialogResult.Cancel)
                    return toPass;
                string path = diag.SelectedPath + "\\";

                foreach (CycleTriggeredCollection col in collections)
                {
                    double[,] data = new double[col.hists.Count, col.hists.Max(d => d.BinCounts.Length)];
                    double[,] x = new double[col.hists.Count, col.hists.Max(d => d.BinCounts.Length)];
                    string[] usedC = new string[col.hists.Count];
                    string[] usedCE = new string[col.hists.Count];
                    for (int i = 0; i < col.hists.Count; i++)
                    {
                        // double sum = hists[i].BinCounts.Sum();
                        usedC[i] = colors[i];
                        usedCE[i] = lcolors[i];
                        for (int j = 0; j < data.GetLength(1); j++)
                        {
                            data[i, j] = col.hists[i].BinCounts[j];
                            x[i, j] = col.hists[i].BinTimes[j];
                        }
                    }

                    int st = 0;
                    string lab = col.Label.Replace('/', '-');
                    string[] stuff = col.hists.Select(r => r.Label.Replace('\\', '-')).ToArray();

                    lock (CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabLockable)
                    {
                        MatlabLoader.MatlabFunctions.plotHistograms(x, data, lab, "Time", "Normalized Counts", usedC, usedCE, stuff, path);
                    }
                }


                return toPass;
            }

            #endregion
        }

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {

            List<BatchProcessableParameter> toRet = new List<BatchProcessableParameter>();

            BatchProcessableParameter triggerData = new BatchProcessableParameter()
            {
                Name= "Trigger Data",
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                Editable = false,
                CanLinkFrom = false
            };

            BatchProcessableParameter triggerNames = new BatchProcessableParameter()
            {
                Name = "Trigger Name",
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                Editable = true,
                CanLinkFrom = false
            };

            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                Name = "Nerve1 Data",
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                Editable = false,
                CanLinkFrom = false
            };


            BatchProcessableParameter dataRes = new BatchProcessableParameter()
            {
                Name = "Nerve1 Resolution",
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                Editable = false,
                CanLinkFrom = false
            };

            BatchProcessableParameter label = new BatchProcessableParameter()
            {
                Name = "Nerve1 Name",
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                Editable= false,
                CanLinkFrom = false
            };

            BatchProcessableParameter backData = new BatchProcessableParameter()
            {
                Name = "Nerve2 Data",
                Editable = false
            };

            BatchProcessableParameter backRes = new BatchProcessableParameter()
            {
                Name = "Nerve2 Resolution",
                Editable = false,
                CanLinkFrom = false
            };

      
            BatchProcessableParameter backNames = new BatchProcessableParameter()
            {
                Name = "Nerve2 Name",
                Editable = false
            };

            BatchProcessableParameter plotLabel = new BatchProcessableParameter()
            {
                Name = "Label"
            };

            toRet.Add(triggerData);
            toRet.Add(triggerNames);
            toRet.Add(data);
            toRet.Add(dataRes);
            toRet.Add(label);
            toRet.Add(backData);
            toRet.Add(backRes);
            toRet.Add(backNames);

            toRet.AddRange(BaseHistogram.GetCommonParameters());

            toRet.Add(plotLabel);
            return toRet.ToArray();
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            //bool rawData = false;
            //bool rawBack = false;

            //float[][][] triggerData = null;
            //string[][] triggerLabels = null;

            //float[][][] mainData = null;
            //string[][] mainLabels = null;

            //float[][][] backData = null;
            //string[][] backLabels = null;


            //getDataAndNames(parameters, "Trigger Data", "Trigger Names", ref triggerData, ref triggerLabels);
            //rawData = getDataAndNames(parameters, "Main Data", "Main Names", ref mainData, ref mainLabels);
            //rawBack = getDataAndNames(parameters, "Background Data", "Background Names", ref backData, ref backLabels);

            float[] triggerData = parameters.Single(p => p.Name == "Trigger Data").Val as float[];
            string triggerName = parameters.Single(p => p.Name == "Trigger Name").Val as string;

            float[] nerve1Data = parameters.Single(p => p.Name == "Nerve1 Data").Val as float[];
            double nerve1Res = Convert.ToDouble(parameters.Single(p => p.Name == "Nerve1 Resolution").Val);
            string nerve1Name = parameters.Single(p => p.Name == "Nerve1 Name").Val as string;

            float[] nerve2Data = parameters.Single(p => p.Name == "Nerve2 Data").Val as float[];
            double nerve2Res = Convert.ToDouble(parameters.Single(p => p.Name == "Nerve2 Resolution").Val);
            string nerve2Name = parameters.Single(p => p.Name == "Nerve2 Name").Val as string;

            CycleTriggeredCollection collection = new CycleTriggeredCollection();

            string label = parameters.Single(p => p.Name == "Label").Val as string;

            CycleTriggeredHist hist1 = new CycleTriggeredHist(parameters);
            hist1.RawResolution = (float)nerve1Res;
            hist1.MakeHistogram(triggerData, nerve1Data, true);
            hist1.Label = nerve1Name + " off " + triggerName;
            collection.Hists.Add(hist1);


            if (nerve2Data != null)
            {
                CycleTriggeredHist hist2 = new CycleTriggeredHist(parameters);
                hist2.RawResolution = (float)nerve2Res;
                hist2.MakeHistogram(triggerData, nerve2Data, true);
                hist2.Label = nerve2Name + " off " + triggerName;
                collection.Hists.Add(hist2);
            }

            collection.Label = label;

            return new object[] { collection };
        }

        //private static bool getDataAndNames(BatchProcessableParameter[] parameters, string data, string names, ref float[][][] triggerData, ref string[][] triggerLabels)
        //{
        //    bool isRaw = false;
        //    object triggerDataVal = parameters.Single(p => p.Name == data).Val;
        //    object triggerLabelVal = parameters.Single(p => p.Name == names).Val;

        //    if (triggerDataVal is float[][][])
        //    {
        //        triggerData = triggerDataVal as float[][][];
        //    }
        //    else if (triggerDataVal is float[][])
        //    {
        //        triggerData = new float[1][][];
        //        triggerData[0] = triggerDataVal as float[][];
        //        isRaw = true;
        //    }
        //    else if (triggerDataVal is float[])
        //    {
        //        triggerData = new float[1][][] { new float[1][] };
        //        triggerData[0][0] = triggerDataVal as float[];
        //        isRaw = true;
        //    }
        //    else if (triggerDataVal is AutoHistogram.AutoHist[])
        //    {
        //        AutoHistogram.AutoHist[] passedHists = triggerDataVal as AutoHistogram.AutoHist[];
        //        triggerData = new float[passedHists.Length][][];

        //        //TODO test IE
        //        for (int i = 0; i < triggerData.Length; i++)
        //        {
        //            triggerData[i] = new float[1][];
        //            triggerData[i][0] = passedHists[i].Data;
        //        }
        //    }


        //    if (triggerLabelVal is string[][])
        //    {
        //        triggerLabels = triggerLabelVal as string[][];
        //    }
        //    else if (triggerLabelVal is string[])
        //    {
        //        triggerLabels = new string[1][];
        //        triggerLabels[0] = triggerLabelVal as string[];
        //    }
        //    else if (triggerLabelVal is AutoHistogram.AutoHist[])
        //    {
        //        AutoHistogram.AutoHist[] passedHists = triggerLabelVal as AutoHistogram.AutoHist[];
        //        triggerLabels = new string[1][];
        //        triggerLabels[0] = new string[passedHists.Length];
        //        for (int i = 0; i < triggerLabels.Length; i++)
        //        {
        //            triggerLabels[0][i] = passedHists[i].Label;
        //        }
        //    }

        //    return isRaw;
        //}

        public string[] OutputDescription
        {
            get { return new string[] { "Histograms" }; }
        }

        public override string ToString()
        {
            return "Cycle Triggered Hist";
        }

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Histogram Tools"; }
        }

        #endregion
    }
}
