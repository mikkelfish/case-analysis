﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;

namespace ApolloFinal.Tools.MixedTools.Histograms
{
    [AdapterDescription("Mixed")]
    public class CrossCorrelationHistogram : IBatchFlowable, IRoutineCategoryProvider
    {
        public class CrossCorrelationHist : BaseHistogram
        {
            public CrossCorrelationHist(BatchProcessableParameter[] paras)
                : base(paras)
            {

            }

            public float[] Data1 { get; private set; }
            public float[] Data2 { get; private set; }

            public string Label1 { get; private set; }
            public string Label2 { get; private set; }

            public void CalculateHistogram(float[] data1, float[] data2, string label1, string label2)
            {
                this.Label1 = label1;
                this.Label2 = label2;

              //  float mean = CeresBase.MathConstructs.Stats.Average(data1);
             //   float sd = CeresBase.MathConstructs.Stats.StandardDev(data1);

                int firstGood = 0;
                for (int i = 0; i < data1.Length; i++)
                {
                    float reference = data1[i];

                    //if (reference < mean - sd * this.SDMult || reference > mean + sd * this.SDMult)
                    //    continue;

                    for (int j = firstGood; j < data2.Length; j++)
                    {
                        if (data2[j] < reference)
                        {
                            firstGood++;
                            continue;
                        }


                        if (data2[j] > reference + this.TotalTime) break;

                        float val = data2[j] - reference;

                        if (val > this.BinTimes[this.BinTimes.Length - 1])
                        {
                            val -= (float)this.TotalTime;
                            if (val > 0)
                            {
                               
                                continue;
                            }
                        }

                        int bin = 0;

                        if (val > 0)
                            bin = (int)((val - this.BinTimes[0]) / this.BinWidth);
                        else bin = (int)Math.Ceiling((val - this.BinTimes[0]) / this.BinWidth);

                        if (bin < this.BinCounts.Length)
                            this.BinCounts[bin]++;
                        else this.ExcludedCycles++;
                    }
                }

            }

        }

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            List<BatchProcessableParameter> toRet = new List<BatchProcessableParameter>(BaseHistogram.GetCommonParameters());

            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                Name = "Data",
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                Editable = false,
                CanLinkFrom = false
            };

            BatchProcessableParameter label = new BatchProcessableParameter()
            {
                Name = "Descriptions",
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                Editable=false
            };

            toRet.Insert(0, data);
            toRet.Insert(1, label);

            return toRet.ToArray();
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            List<CrossCorrelationHist> toRet = new List<CrossCorrelationHist>();

            float[][][] allVals = null;

            if (parameters.Single(p => p.Name == "Data").Val is float[][][])
            {
                allVals = parameters.Single(p =>p.Name == "Data").Val as float[][][];
            }
            else if (parameters.Single(p => p.Name == "Data").Val is AutoHistogram.AutoHist[])
            {
                AutoHistogram.AutoHist[] passedHists = parameters.Single(p => p.Name == "Data").Val as AutoHistogram.AutoHist[];
                allVals = new float[passedHists.Length][][];

                //TODO test IE
                for (int i = 0; i < allVals.Length; i++)
                {
                    allVals[i] = new float[1][];
                    allVals[i][0] = passedHists[i].Data;
                }
            }

            string[][] labels = null;
            string[] descs = null;

            if (parameters.Single(p => p.Name == "Descriptions").Val is string[][])
            {
                labels = parameters.Single(p => p.Name == "Descriptions").Val as string[][];
            }
            else if (parameters.Single(p => p.Name == "Descriptions").Val is string[])
            {
                descs = parameters.Single(p => p.Name == "Descriptions").Val as string[];
            }
            else if (parameters.Single(p => p.Name == "Descriptions").Val is AutoHistogram.AutoHist[])
            {
                AutoHistogram.AutoHist[] passedHists = parameters.Single(p => p.Name == "Descriptions").Val as AutoHistogram.AutoHist[];
                descs = new string[passedHists.Length];
                for (int i = 0; i < descs.Length; i++)
                {
                    descs[i] = passedHists[i].Label;
                }
            }


            for (int i = 0; i < allVals.Length; i++)
            {
                for (int j = 0; j < allVals[i].Length; j++)
                {
                    for (int others = 0; others < allVals.Length; others++)
                    {
                        if (others == i) continue;
                        for (int otherk = 0; otherk < allVals[others].Length; otherk++)
                        {
                            float[] data1 = allVals[i][j];
                            float[] data2 = allVals[others][otherk];

                            CrossCorrelationHist hist = new CrossCorrelationHist(parameters);

                            string label1 = "";
                            string label2 = "";
                            if (labels != null)
                            {
                                label1 = labels[i][j];
                                label2 = labels[others][otherk];
                            }
                            else if (descs != null)
                            {
                                label1 = descs[i];
                                label2 = descs[others];
                            }

                            hist.CalculateHistogram(data1, data2, label1, label2);
                            toRet.Add(hist);
                        }

                    }
                }
            }

            return new object[] { toRet.ToArray() };

        }

        public string[] OutputDescription
        {
            get { return new string[] { "Histograms" }; }
        }

        public override string ToString()
        {
            return "Cross Correlations";
        }

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Histogram Tools"; }
        }

        #endregion
    }
}
