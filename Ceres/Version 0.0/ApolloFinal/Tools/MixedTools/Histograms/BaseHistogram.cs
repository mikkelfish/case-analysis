﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;

namespace ApolloFinal.Tools.MixedTools.Histograms
{
    public class BaseHistogram
    {
        public static BatchProcessableParameter[] GetCommonParameters()
        {
            BatchProcessableParameter binwidth = new BatchProcessableParameter()
            {
                Name = "Bin Width",
                Val = 20.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };

            BatchProcessableParameter numbins = new BatchProcessableParameter()
            {
                Name = "Number Bins",
                Val = 100,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0,
                    Operator = MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };

            BatchProcessableParameter stdDev = new BatchProcessableParameter()
            {
                Name = "Std. Dev.",
                Val = 1.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThanEqual,
                    TreatNonComparableAsSuccess = true
                },
                Description = "If value falls outside this standard deviation it is filtered."
            };

            BatchProcessableParameter offset = new BatchProcessableParameter()
            {
                Name = "Offset",
                Val = 0.0,
                Description = "Offset in ms"
            };

            return new BatchProcessableParameter[] { binwidth, numbins, offset, stdDev };
        }

        public double SDMult { get; private set; }
        public double Offset { get; private set; }
        public int TotalCycles { get; private set; }
        public int ExcludedCycles { get; protected set; }
        public double BinWidth { get; private set; }

        public double[] BinTimes
        {
            get
            {
                double start = this.Offset;
                double each = this.TotalTime / this.NumberOfBins;
                double[] toRet = new double[this.NumberOfBins];
                for (int i = 0; i < toRet.Length; i++)
                {
                    toRet[i] = start + i * each;
                }

                return toRet;
            }
        }


        public int NumberOfBins
        {
            get { return this.BinCounts.Length; }
        }

        public double LargestBinCount
        {
            get
            {
                return this.BinCounts.Max();
            }
        }

        public double TotalTime
        {
            get
            {
                return this.BinCounts.Length * this.BinWidth;
            }
        }

        public double[] BinCounts
        {
            get;
            protected set;
        }

        public double[] BinSDS
        {
            get;
            private set;
        }

        public BaseHistogram(BatchProcessableParameter[] parameters)
        {
            double sdmult = (double)parameters.Single(p => p.Name == "Std. Dev.").Val;
            double offset = (double)parameters.Single(p => p.Name == "Offset").Val;
            double binWidth = (double)parameters.Single(p => p.Name == "Bin Width").Val;
            int binCounts = (int)parameters.Single(p => p.Name == "Number Bins").Val;

            //convert to seconds
            this.BinWidth = binWidth/1000.0;
            this.BinCounts = new double[binCounts];
            this.BinSDS = new double[binCounts];
            this.SDMult = sdmult;
            //convert to seconds
            this.Offset = offset/1000.0;
        }
    }
}
