﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using CeresBase.Projects.Flowable;
//using CeresBase.UI;
//using CeresBase.FlowControl.Adapters;

//namespace ApolloFinal.Tools.MixedTools.Histograms.Filters
//{
//    [AdapterDescription("Mixed")]
//    class CrossCorrelationFilter : IBatchFlowable, IRoutineCategoryProvider
//    {
//        #region IBatchFlowable Members

//        public BatchProcessableParameter[] GetParameters()
//        {
//            BatchProcessableParameter histograms = new BatchProcessableParameter()
//            {
//                Name = "Histograms",
//                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
//                Editable = false
//            };

//            return new BatchProcessableParameter[] { histograms };
//        }

//        public object[] Run(BatchProcessableParameter[] parameters)
//        {
//            List<object> outputs = new List<object>();
//            List<CrossCorrelationHistogram.CrossCorrelationHist> passed = new List<CrossCorrelationHistogram.CrossCorrelationHist>();

//            CrossCorrelationHistogram.CrossCorrelationHist[] hists = parameters.Single(p => p.Name == "Histograms").Val as CrossCorrelationHistogram.CrossCorrelationHist[];
//            foreach (CrossCorrelationHistogram.CrossCorrelationHist hist in hists)
//            {
//                GraphableOutput output = new GraphableOutput();
//                output.Label = hist.Label1 + " x " + hist.Label2;
//                output.SourceDescription = "CrossCorrelation Histogram";
//                output.XData = hist.BinTimes;
//                output.YData = new double[][] { hist.BinCounts.ToArray() };
//                output.YLabel = "Count";
//                output.XLabel = "Bin Width: " + Math.Round(hist.BinWidth, 2);
//                output.GraphType = GraphingType.Column;

//                //List<object> extra = new List<object>();

//                //extra.Add("Status: " + hist.CurrentStatus);
//                //extra.Add("Yellow: " + yellow + " Misses: " + numMisses[0]);
//                //extra.Add("Red: " + red + " Misses: " + numMisses[1]);
//                //extra.Add("Entropy: " + hist.Entropy);

//                //output.ExtraInfo = extra.ToArray();
//                outputs.Add(output);
//                passed.Add(hist);
//            }

//            return new object[] { passed.ToArray(), outputs.ToArray() };
//        }

//        public string[] OutputDescription
//        {
//            get { return new string[] { "Histograms Passed", "Histogram Graphs" }; }
//        }

//        public override string ToString()
//        {
//            return "CrossCorr Filter";
//        }

//        #endregion

//        #region IRoutineCategoryProvider Members

//        public string Category
//        {
//            get { return "Histogram Tools"; }
//        }

//        #endregion
//    }
//}
