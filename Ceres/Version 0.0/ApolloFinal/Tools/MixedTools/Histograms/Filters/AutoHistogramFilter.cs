﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using CeresBase.Projects.Flowable;
//using CeresBase.FlowControl.Adapters;
//using CeresBase.UI;

//namespace ApolloFinal.Tools.MixedTools.Histograms.Filters
//{
//    //[AdapterDescription("Mixed")]
//    class AutoHistogramFilter : IBatchFlowable, IRoutineCategoryProvider
//    {
//        public enum Filters { InterspikeInterval, Entropy };

//        public Filters SelectedFilter { get; private set; }

//        #region IBatchFlowable Members

//        public BatchProcessableParameter[] GetParameters()
//        {
//            List<BatchProcessableParameter> toRet = new List<BatchProcessableParameter>();
//            BatchProcessableParameter histograms = new BatchProcessableParameter()
//            {
//                Name = "Histograms",
//                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
//                Editable = false
//            };

//            BatchProcessableParameter filter = new BatchProcessableParameter()
//            {
//                Name = "Filter",
//                Val = Filters.InterspikeInterval
//            };

//            BatchProcessableParameter yellow = new BatchProcessableParameter()
//            {
//                Name = "Yellow Cutoff",
//                Val = 4.0,
//                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
//                {
//                    CompareTo = 0.0,
//                    Operator = MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThanEqual,
//                    TreatNonComparableAsSuccess = true
//                }
//            };

//            BatchProcessableParameter red = new BatchProcessableParameter()
//            {
//                Name = "Red Cutoff",
//                Val = 2.0,
//                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
//                {
//                    CompareTo = yellow,
//                    Path = "Val",
//                    Operator = MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.LessThanEqual,
//                    TreatNonComparableAsSuccess = true
//                }
//            };

//            BatchProcessableParameter numError = new BatchProcessableParameter()
//            {
//                Name = "Misses Allowed",
//                Val = 1,
//                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
//                {
//                    CompareTo = 1,
//                    Operator = MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThanEqual,
//                    TreatNonComparableAsSuccess = true
//                }
//            };

//            BatchProcessableParameter outputFilter = new BatchProcessableParameter()
//            {
//                Name = "Output Filter",
//                Val = AutoHistogram.AutoHist.Status.Green
//            };

//            BatchProcessableParameter chainFilter = new BatchProcessableParameter()
//            {
//                Name = "Chain Filter",
//                Val = AutoHistogram.AutoHist.Status.Green
//            };

//            toRet.Add(histograms);
//            toRet.Add(filter);
//            toRet.Add(yellow);
//            toRet.Add(red);
//            toRet.Add(numError);
//            toRet.Add(outputFilter);
//            toRet.Add(chainFilter);
//            return toRet.ToArray();
//        }

//        private int[] runInterFilter(AutoHistogram.AutoHist hist, double yellow, double red, int missesAllowed)
//        {
//            int lessThanRed = 0;
//            int lessThanYellow = 0;

//            for (int i = 0; i < hist.Data.Length; i++)
//            {
//                if (hist.Data[i] < yellow)
//                {
//                    lessThanYellow++;
//                }

//                if (hist.Data[i] < red)
//                {
//                    lessThanRed++;
//                }
//            }


//            if (lessThanRed > missesAllowed)
//            {
//                hist.CurrentStatus = AutoHistogram.AutoHist.Status.Red;
//            }
//            else if (lessThanYellow > missesAllowed)
//            {
//                hist.CurrentStatus = AutoHistogram.AutoHist.Status.Yellow;
//            }
//            else hist.CurrentStatus = AutoHistogram.AutoHist.Status.Green;

//            return new int[] { lessThanYellow, lessThanRed };

//        }

//        public object[] Run(BatchProcessableParameter[] parameters)
//        {
//            AutoHistogram.AutoHist[] hists = parameters.Single(p => p.Name == "Histograms").Val as AutoHistogram.AutoHist[];
//            double yellow = (double)parameters.Single(p => p.Name == "Yellow Cutoff").Val / 1000.0;
//            double red = (double)parameters.Single(p => p.Name == "Red Cutoff").Val / 1000.0;
//            int misses = (int)parameters.Single(p => p.Name == "Misses Allowed").Val;
//            AutoHistogram.AutoHist.Status outputFilter = (AutoHistogram.AutoHist.Status)parameters.Single(p => p.Name == "Output Filter").Val;
//            AutoHistogram.AutoHist.Status chainFilter = (AutoHistogram.AutoHist.Status)parameters.Single(p => p.Name == "Chain Filter").Val;
//            Filters filter = (Filters)parameters.Single(p => p.Name == "Filter").Val;

//            List<AutoHistogram.AutoHist> passed = new List<AutoHistogram.AutoHist>();
//            List<object> outputs = new List<object>();

//            foreach (AutoHistogram.AutoHist hist in hists)
//            {
//                int[] numMisses = null;
//                if(filter == Filters.InterspikeInterval)
//                    numMisses = this.runInterFilter(hist, yellow, red, misses);

//                if (hist.CurrentStatus == AutoHistogram.AutoHist.Status.Green ||
//                    (hist.CurrentStatus == AutoHistogram.AutoHist.Status.Yellow &&
//                        (outputFilter == AutoHistogram.AutoHist.Status.Yellow || outputFilter == AutoHistogram.AutoHist.Status.Red)) ||
//                    (hist.CurrentStatus == AutoHistogram.AutoHist.Status.Red &&
//                           outputFilter == AutoHistogram.AutoHist.Status.Red))
//                {
//                    GraphableOutput output = new GraphableOutput();
//                    output.Label = hist.Label;
//                    output.SourceDescription = "AutoHistogram";
//                    output.XData = hist.BinTimes;
//                    output.YData = new double[][] { hist.BinCounts.ToArray() };
//                    output.YLabel = "Count";
//                    output.XLabel = "Bin Width: " + Math.Round(hist.BinWidth, 2);
//                    output.GraphType = GraphingType.Column;

//                    List<object> extra = new List<object>();

//                    extra.Add("Status: " + hist.CurrentStatus);
//                    extra.Add("Yellow: " + yellow + " Misses: " + numMisses[0]);
//                    extra.Add("Red: " + red + " Misses: " + numMisses[1]);
//                    extra.Add("Entropy: " + hist.Entropy);

//                    output.ExtraInfo = extra.ToArray();
//                    outputs.Add(output);


//                }

//                if (hist.CurrentStatus == AutoHistogram.AutoHist.Status.Green ||
//                 (hist.CurrentStatus == AutoHistogram.AutoHist.Status.Yellow &&
//                     (chainFilter == AutoHistogram.AutoHist.Status.Yellow || chainFilter == AutoHistogram.AutoHist.Status.Red)) ||
//                 (hist.CurrentStatus == AutoHistogram.AutoHist.Status.Red &&
//                        chainFilter == AutoHistogram.AutoHist.Status.Red))
//                {
//                    passed.Add(hist);
//                }
//            }

//            return new object[] { passed.ToArray(), outputs.ToArray() };
//        }

//        public string[] OutputDescription
//        {
//            get { return new string[] { "Histograms Passed", "Histogram Graphs" }; }
//        }

//        #endregion

//        public override string ToString()
//        {
//            return "AutoHist Filter";
//        }

//        #region IRoutineCategoryProvider Members

//        public string Category
//        {
//            get { return "Histogram Tools"; }
//        }

//        #endregion
//    }
//}
