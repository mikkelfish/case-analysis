﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.Projects.Flowable;
using CeresBase.UI;

namespace ApolloFinal.Tools.MixedTools.Histograms
{
    [AdapterDescription("Mixed")]
    public class AutoHistogram : IBatchFlowable, IRoutineCategoryProvider
    {
        public class AutoHist : BaseHistogram
        {
            //public enum Status { Green, Yellow, Red };

            //public Status CurrentStatus { get; set; }

            public float[] Data { get; private set; }

         
            public string Label { get; set; }

            //public double PeakFrequency { get; private set; }

            //public double Entropy { get; private set; }

            public AutoHist(BatchProcessableParameter[] paras)
                : base(paras)
            {
            }

            public void Calculate(float[] vals1)
            {
                float[] realData = new float[vals1.Length - 1];
                for (int i = 0; i < vals1.Length - 1; i++)
                {
                    realData[i] = vals1[i + 1] - vals1[i];
                }

                this.Data = vals1;


                vals1 = realData;



                float mean = CeresBase.MathConstructs.Stats.Average(vals1);
                float sd = CeresBase.MathConstructs.Stats.StandardDev(vals1);

                for (int i = 0; i < vals1.Length; i++)
                {
                    double val = vals1[i];


                    if (val < mean - sd * this.SDMult || val > mean + sd * this.SDMult)
                    {
                        this.ExcludedCycles++;
                        continue;
                    }

                    if (val > this.BinTimes[this.BinTimes.Length - 1])
                    {
                        val -= this.TotalTime;
                        if (val > 0)
                        {
                            this.ExcludedCycles++;
                            continue;
                        }
                    }

                    int bin = 0;

                    if (val > 0)
                        bin = (int)((val - this.BinTimes[0]) / this.BinWidth);
                    else bin = (int)Math.Ceiling((val - this.BinTimes[0]) / this.BinWidth);

                    //val += this.Offset;

                    //if (val < 0)
                    //{
                    //    val += this.TotalTime;
                    //}

                    //int bin = (int)(val / this.BinWidth);
                    if (bin < this.BinCounts.Length)
                        this.BinCounts[bin]++;
                    else this.ExcludedCycles++;
                }

                

                //double maxcount = double.MinValue;
                //int maxspace = -1;
                //for (int i = 0; i < this.BinCounts.Length; i++)
                //{
                //    if (this.BinCounts[i] > maxcount)
                //    {
                //        maxspace = i;
                //        maxcount = this.BinCounts[i];
                //    }
                //}

               // this.Entropy = CeresBase.MathConstructs.Stats.Entropy(this.BinCounts, 2.0);

               // double time = 
            }
        }

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            List<BatchProcessableParameter> toRet=  new List<BatchProcessableParameter>(BaseHistogram.GetCommonParameters());

            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                Name = "Interval Timings",
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                Editable = false,
                CanLinkFrom = false
            };

            BatchProcessableParameter label = new BatchProcessableParameter()
            {
                Name = "Interval Desc.",
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                Editable=false
            };

            toRet.Insert(0, data);
            toRet.Insert(1, label);

            return toRet.ToArray();
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            List<AutoHist> toRet = new List<AutoHist>();

            float[][][] allVals = parameters.Single(p => p.Name == "Interval Timings").Val as float[][][];
            if (allVals == null && parameters.Single(p => p.Name == "Interval Timings").Val is float[][])
                allVals = new float[1][][] { parameters.Single(p => p.Name == "Interval Timings").Val as float[][] };

            string[][] labels = null;
            string[] descs = null;

            if (parameters.Single(p => p.Name == "Interval Desc.").Val is string[][])
            {
                labels = parameters.Single(p => p.Name == "Interval Desc.").Val as string[][];
            }
            else if(parameters.Single(p => p.Name == "Interval Desc.").Val is string[])
            {
                descs = parameters.Single(p => p.Name == "Interval Desc.").Val as string[];
            }


            for (int i = 0; i < allVals.Length; i++ )
            {
                float[][] vals2 = allVals[i];
                for(int j = 0; j < vals2.Length; j++)
                {
                    float[] vals1 = vals2[j];
                    AutoHist hist = new AutoHist(parameters);
                    hist.Calculate(vals1);
                    if(labels != null)
                        hist.Label = labels[i][j];
                    else if(descs != null)
                        hist.Label = descs[i];
                    toRet.Add(hist);
                }
            }

            return new object[] { toRet.ToArray() };
           
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Histograms" }; }
        }

        public override string ToString()
        {
            return "AutoHistogram";
        }

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Histogram Tools"; }
        }

        #endregion
    }
}
