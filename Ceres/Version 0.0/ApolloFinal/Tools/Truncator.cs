using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CeresBase.Data;

namespace ApolloFinal.Tools
{
    public partial class Truncator : Form
    {
        private UI.Scope.ScopeMaster master;
        public UI.Scope.ScopeMaster Master
        {
            set { this.master = value; }
            get { return master; }
        }
	

        public Truncator()
        {
            InitializeComponent();
        }

        private void Truncator_Load(object sender, EventArgs e)
        {
            foreach (Variable var in VariableSource.Variables)
            {
                this.comboBox1.Items.Add(var);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.comboBox1.SelectedItem == null)
            {
                MessageBox.Show("Please select a variable before truncating.");
                return;
            }

            float begin = 0;
            float end = 0;

            if (!float.TryParse(this.textBoxStart.Text, out begin) || !float.TryParse(this.textBoxEnd.Text, out end))
            {
                MessageBox.Show("Either begin time or end time could not be parsed. Try again.");
                return;
            }

            SpikeVariable var = (SpikeVariable)this.comboBox1.SelectedItem;
            if (begin < 0 || end > var.TotalLength)
            {
                MessageBox.Show("Beginning or ending not in range. Must be between 0 and " + var.TotalLength);
                return;
            }

            

            string toAdd="";
            if (this.textBoxDesc.Text != null && this.textBoxDesc.Text != "")
                toAdd = this.textBoxDesc.Text + " ";

            Truncate trunc = new Truncate();
            Variable newvar =
                (Variable)trunc.Process(new SpikeVariable[] { (SpikeVariable)this.comboBox1.SelectedItem }, new object[] { begin, end, toAdd })[0];

            VariableSource.AddVariable(newvar);

            if (master != null)
            {
                master.BuildVariables();
            }

            CeresBase.IO.IOUtilities.AddVariableToDisk("Variable Truncated, Save to Disk?", 
                new Variable[] { newvar });
            this.Close();
            
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}