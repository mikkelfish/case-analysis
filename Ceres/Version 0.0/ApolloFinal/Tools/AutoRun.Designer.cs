namespace ApolloFinal.Tools
{
    partial class AutoRun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBoxTrigger = new System.Windows.Forms.ComboBox();
            this.comboBoxBase = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxCTH = new System.Windows.Forms.CheckBox();
            this.checkBoxSTA = new System.Windows.Forms.CheckBox();
            this.checkBoxCC = new System.Windows.Forms.CheckBox();
            this.groupBoxCTO = new System.Windows.Forms.GroupBox();
            this.textBoxCTOoff = new System.Windows.Forms.TextBox();
            this.textBoxCTOBCts = new System.Windows.Forms.TextBox();
            this.textBoxCTOBinW = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxCTOSDs = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxCTOTimes = new System.Windows.Forms.TextBox();
            this.toolTipTimeRanges = new System.Windows.Forms.ToolTip(this.components);
            this.textBoxCCoffs = new System.Windows.Forms.TextBox();
            this.textBoxCCbincts = new System.Windows.Forms.TextBox();
            this.textBoxCCbinws = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBoxCCOptions = new System.Windows.Forms.GroupBox();
            this.textBoxCCsds = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxCCtimes = new System.Windows.Forms.TextBox();
            this.groupBoxSTA = new System.Windows.Forms.GroupBox();
            this.checkBoxSTArect = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxSTAend = new System.Windows.Forms.TextBox();
            this.textBoxSTAlengths = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxSTAstart = new System.Windows.Forms.TextBox();
            this.listViewLoaded = new System.Windows.Forms.ListView();
            this.listViewAnalyze = new System.Windows.Forms.ListView();
            this.textBoxDirectory = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.buttonDir = new System.Windows.Forms.Button();
            this.buttonRun = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBoxRaw = new System.Windows.Forms.ComboBox();
            this.groupBoxCTO.SuspendLayout();
            this.groupBoxCCOptions.SuspendLayout();
            this.groupBoxSTA.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxTrigger
            // 
            this.comboBoxTrigger.FormattingEnabled = true;
            this.comboBoxTrigger.Location = new System.Drawing.Point(374, 26);
            this.comboBoxTrigger.Name = "comboBoxTrigger";
            this.comboBoxTrigger.Size = new System.Drawing.Size(328, 21);
            this.comboBoxTrigger.TabIndex = 0;
            // 
            // comboBoxBase
            // 
            this.comboBoxBase.FormattingEnabled = true;
            this.comboBoxBase.Location = new System.Drawing.Point(374, 68);
            this.comboBoxBase.Name = "comboBoxBase";
            this.comboBoxBase.Size = new System.Drawing.Size(325, 21);
            this.comboBoxBase.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(371, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Trigger";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(374, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Base Signal";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Loaded Cells";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 255);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Cells to Analyze";
            // 
            // checkBoxCTH
            // 
            this.checkBoxCTH.AutoSize = true;
            this.checkBoxCTH.Location = new System.Drawing.Point(374, 134);
            this.checkBoxCTH.Name = "checkBoxCTH";
            this.checkBoxCTH.Size = new System.Drawing.Size(176, 17);
            this.checkBoxCTH.TabIndex = 8;
            this.checkBoxCTH.Text = "Make Cycle Trigged Histograms";
            this.checkBoxCTH.UseVisualStyleBackColor = true;
            this.checkBoxCTH.CheckedChanged += new System.EventHandler(this.checkBoxCTH_CheckedChanged);
            // 
            // checkBoxSTA
            // 
            this.checkBoxSTA.AutoSize = true;
            this.checkBoxSTA.Enabled = false;
            this.checkBoxSTA.Location = new System.Drawing.Point(374, 394);
            this.checkBoxSTA.Name = "checkBoxSTA";
            this.checkBoxSTA.Size = new System.Drawing.Size(179, 17);
            this.checkBoxSTA.TabIndex = 9;
            this.checkBoxSTA.Text = "Make Spike Triggered Averages";
            this.checkBoxSTA.UseVisualStyleBackColor = true;
            this.checkBoxSTA.CheckedChanged += new System.EventHandler(this.checkBoxSTA_CheckedChanged);
            // 
            // checkBoxCC
            // 
            this.checkBoxCC.AutoSize = true;
            this.checkBoxCC.Location = new System.Drawing.Point(374, 270);
            this.checkBoxCC.Name = "checkBoxCC";
            this.checkBoxCC.Size = new System.Drawing.Size(140, 17);
            this.checkBoxCC.TabIndex = 10;
            this.checkBoxCC.Text = "Make Cross Correlations";
            this.checkBoxCC.UseVisualStyleBackColor = true;
            this.checkBoxCC.CheckedChanged += new System.EventHandler(this.checkBoxCC_CheckedChanged);
            // 
            // groupBoxCTO
            // 
            this.groupBoxCTO.Controls.Add(this.textBoxCTOoff);
            this.groupBoxCTO.Controls.Add(this.textBoxCTOBCts);
            this.groupBoxCTO.Controls.Add(this.textBoxCTOBinW);
            this.groupBoxCTO.Controls.Add(this.label9);
            this.groupBoxCTO.Controls.Add(this.label8);
            this.groupBoxCTO.Controls.Add(this.label7);
            this.groupBoxCTO.Controls.Add(this.textBoxCTOSDs);
            this.groupBoxCTO.Controls.Add(this.label6);
            this.groupBoxCTO.Controls.Add(this.label5);
            this.groupBoxCTO.Controls.Add(this.textBoxCTOTimes);
            this.groupBoxCTO.Enabled = false;
            this.groupBoxCTO.Location = new System.Drawing.Point(374, 157);
            this.groupBoxCTO.Name = "groupBoxCTO";
            this.groupBoxCTO.Size = new System.Drawing.Size(328, 95);
            this.groupBoxCTO.TabIndex = 11;
            this.groupBoxCTO.TabStop = false;
            this.groupBoxCTO.Text = "Cycle Triggered Options";
            // 
            // textBoxCTOoff
            // 
            this.textBoxCTOoff.Location = new System.Drawing.Point(248, 70);
            this.textBoxCTOoff.Name = "textBoxCTOoff";
            this.textBoxCTOoff.Size = new System.Drawing.Size(74, 20);
            this.textBoxCTOoff.TabIndex = 9;
            // 
            // textBoxCTOBCts
            // 
            this.textBoxCTOBCts.Location = new System.Drawing.Point(162, 70);
            this.textBoxCTOBCts.Name = "textBoxCTOBCts";
            this.textBoxCTOBCts.Size = new System.Drawing.Size(79, 20);
            this.textBoxCTOBCts.TabIndex = 8;
            // 
            // textBoxCTOBinW
            // 
            this.textBoxCTOBinW.Location = new System.Drawing.Point(81, 71);
            this.textBoxCTOBinW.Name = "textBoxCTOBinW";
            this.textBoxCTOBinW.Size = new System.Drawing.Size(74, 20);
            this.textBoxCTOBinW.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(245, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Offsets (ms)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(161, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "BinCounts";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(78, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "BinWidths (ms)";
            // 
            // textBoxCTOSDs
            // 
            this.textBoxCTOSDs.Location = new System.Drawing.Point(6, 71);
            this.textBoxCTOSDs.Name = "textBoxCTOSDs";
            this.textBoxCTOSDs.Size = new System.Drawing.Size(67, 20);
            this.textBoxCTOSDs.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "SDs";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Time Ranges";
            // 
            // textBoxCTOTimes
            // 
            this.textBoxCTOTimes.Location = new System.Drawing.Point(6, 31);
            this.textBoxCTOTimes.Name = "textBoxCTOTimes";
            this.textBoxCTOTimes.Size = new System.Drawing.Size(316, 20);
            this.textBoxCTOTimes.TabIndex = 0;
            // 
            // toolTipTimeRanges
            // 
            this.toolTipTimeRanges.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTipTimeRanges.ToolTipTitle = "Time Range Format";
            // 
            // textBoxCCoffs
            // 
            this.textBoxCCoffs.Location = new System.Drawing.Point(248, 70);
            this.textBoxCCoffs.Name = "textBoxCCoffs";
            this.textBoxCCoffs.Size = new System.Drawing.Size(74, 20);
            this.textBoxCCoffs.TabIndex = 9;
            // 
            // textBoxCCbincts
            // 
            this.textBoxCCbincts.Location = new System.Drawing.Point(162, 70);
            this.textBoxCCbincts.Name = "textBoxCCbincts";
            this.textBoxCCbincts.Size = new System.Drawing.Size(79, 20);
            this.textBoxCCbincts.TabIndex = 8;
            // 
            // textBoxCCbinws
            // 
            this.textBoxCCbinws.Location = new System.Drawing.Point(81, 71);
            this.textBoxCCbinws.Name = "textBoxCCbinws";
            this.textBoxCCbinws.Size = new System.Drawing.Size(74, 20);
            this.textBoxCCbinws.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(245, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Offsets (ms)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(161, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "BinCounts";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(78, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "BinWidths (ms)";
            // 
            // groupBoxCCOptions
            // 
            this.groupBoxCCOptions.Controls.Add(this.textBoxCCoffs);
            this.groupBoxCCOptions.Controls.Add(this.textBoxCCbincts);
            this.groupBoxCCOptions.Controls.Add(this.textBoxCCbinws);
            this.groupBoxCCOptions.Controls.Add(this.label10);
            this.groupBoxCCOptions.Controls.Add(this.label11);
            this.groupBoxCCOptions.Controls.Add(this.label12);
            this.groupBoxCCOptions.Controls.Add(this.textBoxCCsds);
            this.groupBoxCCOptions.Controls.Add(this.label13);
            this.groupBoxCCOptions.Controls.Add(this.label14);
            this.groupBoxCCOptions.Controls.Add(this.textBoxCCtimes);
            this.groupBoxCCOptions.Enabled = false;
            this.groupBoxCCOptions.Location = new System.Drawing.Point(374, 293);
            this.groupBoxCCOptions.Name = "groupBoxCCOptions";
            this.groupBoxCCOptions.Size = new System.Drawing.Size(328, 95);
            this.groupBoxCCOptions.TabIndex = 12;
            this.groupBoxCCOptions.TabStop = false;
            this.groupBoxCCOptions.Text = "Cross Correlation Options";
            // 
            // textBoxCCsds
            // 
            this.textBoxCCsds.Location = new System.Drawing.Point(6, 71);
            this.textBoxCCsds.Name = "textBoxCCsds";
            this.textBoxCCsds.Size = new System.Drawing.Size(67, 20);
            this.textBoxCCsds.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "SDs";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Time Ranges";
            // 
            // textBoxCCtimes
            // 
            this.textBoxCCtimes.Location = new System.Drawing.Point(6, 31);
            this.textBoxCCtimes.Name = "textBoxCCtimes";
            this.textBoxCCtimes.Size = new System.Drawing.Size(316, 20);
            this.textBoxCCtimes.TabIndex = 0;
            // 
            // groupBoxSTA
            // 
            this.groupBoxSTA.Controls.Add(this.checkBoxSTArect);
            this.groupBoxSTA.Controls.Add(this.label20);
            this.groupBoxSTA.Controls.Add(this.textBoxSTAend);
            this.groupBoxSTA.Controls.Add(this.textBoxSTAlengths);
            this.groupBoxSTA.Controls.Add(this.label18);
            this.groupBoxSTA.Controls.Add(this.label19);
            this.groupBoxSTA.Controls.Add(this.textBoxSTAstart);
            this.groupBoxSTA.Enabled = false;
            this.groupBoxSTA.Location = new System.Drawing.Point(374, 417);
            this.groupBoxSTA.Name = "groupBoxSTA";
            this.groupBoxSTA.Size = new System.Drawing.Size(328, 65);
            this.groupBoxSTA.TabIndex = 13;
            this.groupBoxSTA.TabStop = false;
            this.groupBoxSTA.Text = "Spike Triggered Averaging Options";
            // 
            // checkBoxSTArect
            // 
            this.checkBoxSTArect.AutoSize = true;
            this.checkBoxSTArect.Location = new System.Drawing.Point(273, 33);
            this.checkBoxSTArect.Name = "checkBoxSTArect";
            this.checkBoxSTArect.Size = new System.Drawing.Size(49, 17);
            this.checkBoxSTArect.TabIndex = 12;
            this.checkBoxSTArect.Text = "Rect";
            this.checkBoxSTArect.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(78, 15);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 13);
            this.label20.TabIndex = 11;
            this.label20.Text = "End (sec)";
            // 
            // textBoxSTAend
            // 
            this.textBoxSTAend.Location = new System.Drawing.Point(81, 31);
            this.textBoxSTAend.Name = "textBoxSTAend";
            this.textBoxSTAend.Size = new System.Drawing.Size(67, 20);
            this.textBoxSTAend.TabIndex = 10;
            // 
            // textBoxSTAlengths
            // 
            this.textBoxSTAlengths.Location = new System.Drawing.Point(152, 31);
            this.textBoxSTAlengths.Name = "textBoxSTAlengths";
            this.textBoxSTAlengths.Size = new System.Drawing.Size(115, 20);
            this.textBoxSTAlengths.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(149, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Lengths (ms)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Start (sec)";
            // 
            // textBoxSTAstart
            // 
            this.textBoxSTAstart.Location = new System.Drawing.Point(6, 31);
            this.textBoxSTAstart.Name = "textBoxSTAstart";
            this.textBoxSTAstart.Size = new System.Drawing.Size(67, 20);
            this.textBoxSTAstart.TabIndex = 0;
            // 
            // listViewLoaded
            // 
            this.listViewLoaded.Location = new System.Drawing.Point(12, 25);
            this.listViewLoaded.Name = "listViewLoaded";
            this.listViewLoaded.Size = new System.Drawing.Size(356, 227);
            this.listViewLoaded.TabIndex = 14;
            this.listViewLoaded.UseCompatibleStateImageBehavior = false;
            this.listViewLoaded.View = System.Windows.Forms.View.List;
            this.listViewLoaded.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listViewLoaded_MouseMove);
            this.listViewLoaded.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listViewLoaded_MouseDown);
            // 
            // listViewAnalyze
            // 
            this.listViewAnalyze.AllowDrop = true;
            this.listViewAnalyze.Location = new System.Drawing.Point(12, 270);
            this.listViewAnalyze.Name = "listViewAnalyze";
            this.listViewAnalyze.Size = new System.Drawing.Size(356, 212);
            this.listViewAnalyze.TabIndex = 15;
            this.listViewAnalyze.UseCompatibleStateImageBehavior = false;
            this.listViewAnalyze.View = System.Windows.Forms.View.List;
            this.listViewAnalyze.DragEnter += new System.Windows.Forms.DragEventHandler(this.listViewAnalyze_DragEnter);
            this.listViewAnalyze.DragDrop += new System.Windows.Forms.DragEventHandler(this.listViewAnalyze_DragDrop);
            this.listViewAnalyze.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listViewAnalyze_MouseDown);
            // 
            // textBoxDirectory
            // 
            this.textBoxDirectory.Location = new System.Drawing.Point(12, 503);
            this.textBoxDirectory.Name = "textBoxDirectory";
            this.textBoxDirectory.Size = new System.Drawing.Size(219, 20);
            this.textBoxDirectory.TabIndex = 16;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 487);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Save Path";
            // 
            // buttonDir
            // 
            this.buttonDir.Location = new System.Drawing.Point(237, 500);
            this.buttonDir.Name = "buttonDir";
            this.buttonDir.Size = new System.Drawing.Size(27, 23);
            this.buttonDir.TabIndex = 18;
            this.buttonDir.Text = "...";
            this.buttonDir.UseVisualStyleBackColor = true;
            this.buttonDir.Click += new System.EventHandler(this.buttonDir_Click);
            // 
            // buttonRun
            // 
            this.buttonRun.Location = new System.Drawing.Point(475, 501);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(75, 23);
            this.buttonRun.TabIndex = 19;
            this.buttonRun.Text = "Run";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(556, 500);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 20;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(374, 92);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "Raw Signal";
            // 
            // comboBoxRaw
            // 
            this.comboBoxRaw.FormattingEnabled = true;
            this.comboBoxRaw.Location = new System.Drawing.Point(374, 108);
            this.comboBoxRaw.Name = "comboBoxRaw";
            this.comboBoxRaw.Size = new System.Drawing.Size(325, 21);
            this.comboBoxRaw.TabIndex = 21;
            // 
            // AutoRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 535);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.comboBoxRaw);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonRun);
            this.Controls.Add(this.buttonDir);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBoxDirectory);
            this.Controls.Add(this.listViewAnalyze);
            this.Controls.Add(this.listViewLoaded);
            this.Controls.Add(this.groupBoxSTA);
            this.Controls.Add(this.groupBoxCCOptions);
            this.Controls.Add(this.groupBoxCTO);
            this.Controls.Add(this.checkBoxCC);
            this.Controls.Add(this.checkBoxSTA);
            this.Controls.Add(this.checkBoxCTH);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxBase);
            this.Controls.Add(this.comboBoxTrigger);
            this.Name = "AutoRun";
            this.Text = "AutoRun";
            this.Load += new System.EventHandler(this.AutoRun_Load);
            this.groupBoxCTO.ResumeLayout(false);
            this.groupBoxCTO.PerformLayout();
            this.groupBoxCCOptions.ResumeLayout(false);
            this.groupBoxCCOptions.PerformLayout();
            this.groupBoxSTA.ResumeLayout(false);
            this.groupBoxSTA.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxTrigger;
        private System.Windows.Forms.ComboBox comboBoxBase;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxCTH;
        private System.Windows.Forms.CheckBox checkBoxSTA;
        private System.Windows.Forms.CheckBox checkBoxCC;
        private System.Windows.Forms.GroupBox groupBoxCTO;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxCTOSDs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxCTOTimes;
        private System.Windows.Forms.ToolTip toolTipTimeRanges;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxCTOoff;
        private System.Windows.Forms.TextBox textBoxCTOBCts;
        private System.Windows.Forms.TextBox textBoxCTOBinW;
        private System.Windows.Forms.TextBox textBoxCCoffs;
        private System.Windows.Forms.TextBox textBoxCCbincts;
        private System.Windows.Forms.TextBox textBoxCCbinws;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBoxCCOptions;
        private System.Windows.Forms.TextBox textBoxCCsds;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxCCtimes;
        private System.Windows.Forms.GroupBox groupBoxSTA;
        private System.Windows.Forms.CheckBox checkBoxSTArect;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxSTAend;
        private System.Windows.Forms.TextBox textBoxSTAlengths;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxSTAstart;
        private System.Windows.Forms.ListView listViewLoaded;
        private System.Windows.Forms.ListView listViewAnalyze;
        private System.Windows.Forms.TextBox textBoxDirectory;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button buttonDir;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBoxRaw;
    }
}