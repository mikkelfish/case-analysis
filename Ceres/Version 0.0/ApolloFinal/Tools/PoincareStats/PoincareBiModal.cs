using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace ApolloFinal.Tools.PoincareStats
{
    class PoincareBiModal : IPoincareStat
    {
        #region IPoincareStat Members

        private double[] runStat(System.Drawing.PointF[] points)
        {
            List<double> toRet = new List<double>();
            for (int i = 0; i < points.Length - 1; i++)
            {
                PointF pt1 = points[i];
                PointF pt2 = points[i + 1];

                float dist = (float)Math.Abs(pt2.Y - pt1.X);
                double length = Math.Sqrt((pt1.Y - pt1.X) * (pt1.Y - pt1.X) + (pt1.X - pt1.Y) * (pt1.X - pt1.Y));
             //   if (length == 0) dist = 0;
               // else dist /= (float)length * pt2.X;
                dist /= pt2.X;
                toRet.Add(dist);
            }
            return toRet.ToArray();
        }

        public string Description
        {
            get { return "Pt2.Y - Pt1.X/dist if perfectly bimodal"; }
        }

        #endregion

        public override string ToString()
        {
            return "Bimodal";
        }

        #region IPoincareStat Members

        public double[][] RunStat(PointF[][] points, int[] taus)
        {
            double[][] toRet = new double[points.Length][];
            for (int i = 0; i < toRet.Length; i++)
            {
                toRet[i] = this.runStat(points[i]);
            }
            return toRet;
        }

        #endregion
    }

    
}
