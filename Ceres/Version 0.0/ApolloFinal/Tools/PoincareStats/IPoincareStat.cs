using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace ApolloFinal.Tools.PoincareStats
{
    public interface IPoincareStat
    {
        double[][] RunStat(PointF[][] points, int[] taus);
        string Description { get;}

    }
}
