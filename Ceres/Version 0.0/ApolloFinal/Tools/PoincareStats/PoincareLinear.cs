using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace ApolloFinal.Tools.PoincareStats
{
    class PoincareLinear : IPoincareStat
    {
        #region IPoincareStat Members

        private double[] runStat(System.Drawing.PointF[] points)
        {
            List<double> toRet = new List<double>();
            foreach (PointF point in points)
            {
                float distance = Math.Abs(point.X-point.Y);

                if (point.X == 0) continue;
                toRet.Add(distance / point.X);
            }


            return toRet.ToArray();
        }

        #endregion

        #region IPoincareStat Members


        public string Description
        {
            get { return "Measures (Y-X)/X"; }
        }

        #endregion


        public override string ToString()
        {
            return "Linear";
        }

        #region IPoincareStat Members

        public double[][] RunStat(PointF[][] points, int[] taus)
        {
            double[][] toRet = new double[taus.Length][];
            for (int i = 0; i < taus.Length; i++)
            {
                toRet[i] = this.runStat(points[i]);
            }
            return toRet;
        }

        #endregion
    }

    class PoincareLinearNN : IPoincareStat
    {
        #region IPoincareStat Members

        private double[] runStat(System.Drawing.PointF[] points)
        {
            List<double> toRet = new List<double>();
            foreach (PointF point in points)
            {
                float distance = Math.Abs(point.X-point.Y);
                toRet.Add(distance);
            }


            return toRet.ToArray();
        }

        #endregion

        #region IPoincareStat Members


        public string Description
        {
            get { return "Measures |(Y-X)|"; }
        }

        #endregion


        public override string ToString()
        {
            return "Linear NN";
        }

        #region IPoincareStat Members

        public double[][] RunStat(PointF[][] points, int[] taus)
        {
            double[][] toRet = new double[taus.Length][];
            for (int i = 0; i < taus.Length; i++)
            {
                toRet[i] = this.runStat(points[i]);
            }
            return toRet;
        }

        #endregion
    }

    class PoincareLinearNABS : IPoincareStat
    {
        #region IPoincareStat Members

        private double[] runStat(System.Drawing.PointF[] points)
        {
            List<double> toRet = new List<double>();
            foreach (PointF point in points)
            {
                float distance = point.X-point.Y;
                toRet.Add(distance / point.X);
            }


            return toRet.ToArray();
        }

        #endregion

        #region IPoincareStat Members


        public string Description
        {
            get { return "Measures (Y-X)/X"; }
        }

        #endregion


        public override string ToString()
        {
            return "Linear NABS";
        }

        #region IPoincareStat Members

        public double[][] RunStat(PointF[][] points, int[] taus)
        {
            double[][] toRet = new double[taus.Length][];
            for (int i = 0; i < taus.Length; i++)
            {
                toRet[i] = this.runStat(points[i]);
            }
            return toRet;
        }

        #endregion
    }

    class PoincareLinearNNNABS : IPoincareStat
    {
        #region IPoincareStat Members

        private double[] runStat(System.Drawing.PointF[] points)
        {
            List<double> toRet = new List<double>();
            foreach (PointF point in points)
            {
                float distance = point.X-point.Y;
                toRet.Add(distance);
            }


            return toRet.ToArray();
        }

        #endregion

        #region IPoincareStat Members


        public string Description
        {
            get { return "Measures (X-Y)"; }
        }

        #endregion


        public override string ToString()
        {
            return "Linear NN NABS";
        }

        #region IPoincareStat Members

        public double[][] RunStat(PointF[][] points, int[] taus)
        {
            double[][] toRet = new double[taus.Length][];
            for (int i = 0; i < taus.Length; i++)
            {
                toRet[i] = this.runStat(points[i]);
            }
            return toRet;
        }

        #endregion
    }
}
