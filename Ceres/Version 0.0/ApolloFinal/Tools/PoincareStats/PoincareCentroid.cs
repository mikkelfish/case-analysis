using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools.PoincareStats
{
    class PoincareCentroid : IPoincareStat
    {
        #region IPoincareStat Members

        private double[] runStat(System.Drawing.PointF[] points, int m)
        {
            List<double> centroids = new List<double>();
            int centCount = m;
            if (points.Length < centCount) return null;

            double x = 0;
            double y = 0;

            double cent = 0;
            for (int i = 0; i < centCount; i++)
            {
               // cent += (points[i].X + points[i].Y)/2.0 ;

                x += points[i].X;
                y += points[i].Y;
            }

            double distance = Math.Sqrt(x * x + y * y)/centCount;

            centroids.Add(distance);
            for (int i = centCount + 1; i < points.Length; i++)
            {
                x -= points[i - centCount - 1].X;
                y -= points[i - centCount - 1].Y;

                x += points[i].X;
                y += points[i].Y;

                //cent -= (points[i - centCount - 1].X + points[i - centCount - 1].Y) / 2.0;
                //cent += (points[i].X + points[i].Y) / 2.0;
                //double val = (cent / centCount) * Math.Sqrt(2);
                distance = Math.Sqrt(x * x + y * y) / centCount;
                centroids.Add(distance);
            }
            return centroids.ToArray();
        }

        public string Description
        {
            get { return "Centroid"; }
        }

        #endregion

        #region IPoincareStat Members

        public double[][] RunStat(System.Drawing.PointF[][] points, int[] ps)
        {
            double[][] toRet = new double[ps.Length][];
            for (int i = 0; i < toRet.Length; i++)
            {
                toRet[i] = this.runStat(points[0], ps[i]);
            }

            return toRet;
        }

        #endregion
    }
}
