﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl;
using CeresBase.Data;
using System.Reflection;
using System.ComponentModel;
using CeresBase.Projects;
using CeresBase.FlowControl.Adapters;

namespace ApolloFinal.Tools
{
   // [AdapterDescription("Raw Data")]
    public abstract class DataAdapter : StorageAdapter
    {

        public DataAdapter()
        {
            List<string> setnames = new List<string>();
            foreach (EpochDescriptorSet set in EpochDescriptorSetCentral.EpochDescriptorSets)
            {
                setnames.Add(set.Description);
            }

            Type enumType = MesosoftCommon.Utilities.Reflection.DynamicObjects.CreateEnum(setnames.ToArray());

            this.DescriptorType = enumType.InvokeMember("", BindingFlags.CreateInstance, null, null, null);            
        }

        public override Process HostProcess
        {
            get
            {
                return base.HostProcess;
            }
            set
            {
                if (base.HostProcess != null)
                {
                    base.HostProcess.PropertyEdited -= new EventHandler<ProcessPropertyChangedArgs>(HostProcess_PropertyEdited);
                }

                base.HostProcess = value;
                if (base.HostProcess != null)
                {
                    base.HostProcess.PropertyEdited += new EventHandler<ProcessPropertyChangedArgs>(HostProcess_PropertyEdited);
                }
            }
        }

        void HostProcess_PropertyEdited(object sender, ProcessPropertyChangedArgs e)
        {
            Process process = sender as Process;
            if (process == null) return;

            ProcessPropertyChangedInfo info = e.PropertiesChanged.SingleOrDefault(p => p.PropertyName == "Descriptor Type");

            if (info != null)
            {
                this.DescriptorType = info.NewValue;
                process.ReloadPropertiesFromAdapter();
            }
        }


        #region IFlowControlAdapter Members

       

        public EpochDescriptorSet DescriptorSet { get; set; }



        private object descType;
        
        public object DescriptorType 
        {
            get
            {
                return this.descType;
            }
            set
            {
                this.descType = value;

                this.DescriptorSet =  EpochDescriptorSetCentral.EpochDescriptorSets.SingleOrDefault(set => set.Description == this.descType.ToString());
            }
        }

        public override string[] SupplyPropertyNames()
        {
            List<string> names = new List<string>(base.SupplyPropertyNames());


            if (this.DescriptorSet != null)
            {
                foreach (EpochDescriptor desc in this.DescriptorSet.Epochs)
                {
                    names.Add(desc.Name);
                }
            }
           
            names.Add("Descriptor Type");


            return names.ToArray();
        }

        public override object SupplyPropertyValue(string sourcePropertyName)
        {
            if (sourcePropertyName == "Descriptor Type")
            {
                if (this.DescriptorSet == null) return null;
                EpochDescriptor desc = this.DescriptorSet.Epochs.SingleOrDefault(e => e.Name == sourcePropertyName);
                if (desc != null) return desc.Value;
            }
            
            return base.SupplyPropertyValue(sourcePropertyName);

        }

        public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName == "Descriptor Type")
            {
                if (this.DescriptorSet == null) return;

                EpochDescriptor desc = this.DescriptorSet.Epochs.SingleOrDefault(e => e.Name == targetPropertyName);
                if (desc == null) return;

                desc.Value = propertyValue;

            }
            else base.ReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {

            if (targetPropertyName == "Descriptor Type")
            {
                if (this.DescriptorSet == null) return false;
                return this.DescriptorSet.Epochs.Any(ep => ep.Name == targetPropertyName);
            }

            return base.CanReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
        {
            if (sourcePropertyName == "Descriptor Type")
            {

                if (this.DescriptorSet == null)
                {
                    return null;
                }

                EpochDescriptor descriptor = this.DescriptorSet.Epochs.Single(ep => ep.Name == sourcePropertyName);

                FlowControlParameterDescriptor desc = new FlowControlParameterDescriptor();
                desc.Version = "0.0";
                desc.Type = descriptor.Value.GetType();
                return new FlowControlParameterDescriptor[] { desc };
            }
            else return base.TypesSuppliable(sourcePropertyName);
        }

        public override FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
        {
            if (targetPropertyName == "Descriptor Type")
            {
                if (this.DescriptorSet == null)
                {
                    return null;
                }

                EpochDescriptor descriptor = this.DescriptorSet.Epochs.Single(ep => ep.Name == targetPropertyName);

                FlowControlParameterDescriptor desc = new FlowControlParameterDescriptor();
                desc.Version = "0.0";
                desc.Type = descriptor.Value.GetType();
                return new FlowControlParameterDescriptor[] { desc };
            }
            else return base.TypesReceivable(targetPropertyName);
        }

        public override object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
        {
            if (sourcePropertyName == "Descriptor Type")
            {
                if (this.DescriptorSet == null)
                {
                    return null;
                }

                EpochDescriptor descriptor = this.DescriptorSet.Epochs.Single(ep => ep.Name == sourcePropertyName);
                return CeresBase.General.Utilities.Convert(descriptor.Value, supplyType.Type);
            }
            else return base.SupplyPropertyValueAs(sourcePropertyName, supplyType);
            
        }

        public override void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
        {
            if (targetPropertyName == "Descriptor Type")
            {
                if (this.DescriptorSet == null)
                {
                    return;
                }

                EpochDescriptor descriptor = this.DescriptorSet.Epochs.Single(ep => ep.Name == targetPropertyName);
                descriptor.Value = CeresBase.General.Utilities.Convert(propertyValue, receiveType.Type);
            }
            else base.ReceivePropertyValueAs(propertyValue, targetPropertyName, receiveType);
            
        }

        public override Type PropertyType(string targetPropertyName)
        {
            if (targetPropertyName == "Descriptor Type")
            {
                if (this.DescriptorSet == null) return null;

                EpochDescriptor descriptor = this.DescriptorSet.Epochs.Single(ep => ep.Name == targetPropertyName);
                return descriptor.Value.GetType();
            }

            return base.PropertyType(targetPropertyName);
            
        }

        public override void RunAdapter()
        {
            throw new NotImplementedException();
        }

        public override object[] GetValuesForSerialization()
        {
            List<object> toRet = new List<object>();

 

            if (this.DescriptorSet != null)
            {
                toRet.Add(this.DescriptorSet.Description);

            }

        
            return toRet.ToArray();
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            if (values != null && values.Length != 0)
            {
                if (this.DescriptorType.GetType().IsEnum)
                {
                    try
                    {
                        this.DescriptorType = Enum.Parse(this.DescriptorType.GetType(), values[0] as string);
                    }
                    catch
                    {

                    }
                }
            }

        }

        public override bool CanEditProperty(string targetPropertyname)
        {
            if(targetPropertyname == "Descriptor Type")
                return true;
            return base.CanEditProperty(targetPropertyname);
        }

        public override bool CanReceivePropertyLinks(string targetPropertyname)
        {
            if(targetPropertyname == "Descriptor Type")
                return false;
            return base.CanReceivePropertyLinks(targetPropertyname);
        }

        public override bool ShouldSerialize(string targetPropertyname)
        {
            if (targetPropertyname == "Descriptor Type")
                return false;

            return base.ShouldSerialize(targetPropertyname);
        }

        #endregion

        #region ICloneable Members

        protected void cloneHelper(DataAdapter adapter)
        {
            if (this.DescriptorSet != null)
            {
                adapter.DescriptorSet = new EpochDescriptorSet();
                foreach (EpochDescriptor descriptor in this.DescriptorSet.Epochs)
                {
                    adapter.DescriptorSet.Epochs.Add(descriptor);
                }

                adapter.DescriptorSet.Description = this.DescriptorSet.Description;
            }

            adapter.Name = this.Name;
        }

       

        #endregion

    }
}
