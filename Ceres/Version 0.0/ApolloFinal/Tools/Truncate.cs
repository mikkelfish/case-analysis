using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using CeresBase.UI;
using System.Windows.Forms;

namespace ApolloFinal.Tools
{
    public class Truncate
    {
        public override string ToString()
        {
            return "Truncate";
        }

        public bool SuppportsMultiple
        {
            get { return true; }
        }

        public Type OutputType
        {
            get
            {
                return typeof(SpikeVariable);
            }
        }

        public object[] GetArguments(SpikeVariable[] varsToUse, out string[] names)
        {
            RequestInformation req = new RequestInformation();
            req.Grid.SetInformationToCollect(new Type[] { typeof(float), typeof(float), typeof(string) },
                new string[] { "Truncate", "Truncate", "Truncate" },
                new string[] { "Begin", "End", "Desc" },
                new string[] { "Beginning Time", "Ending Time", "Optional Description to Add" },
                null,
                new object[] { 0, 0, "" });
            if (req.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
            {
                names = null;
                return null;
            }

            float begin = (float)req.Grid.Results["Begin"];
            float end = (float)req.Grid.Results["End"];
            string desc = (string)req.Grid.Results["Desc"];

            string err = "";
            if (begin < 0)
            {
                err = "Beginning value cannot be less than 0";
            }
            foreach (SpikeVariable var in varsToUse)
            {
                if (begin >= var.TotalLength || end >= var.TotalLength)
                {
                    err = "Either beginning or end value is greater than the length of a variable.";
                }
            }
            if (err != "")
            {
                if (MessageBox.Show(err + " Try Again?", "Error", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    return this.GetArguments(varsToUse, out names);
                else
                {
                    names = null;
                    return null;
                }
            }

            names = new string[varsToUse.Length];
            for (int i = 0; i < varsToUse.Length; i++)
            {
                names[i] = varsToUse[i].FullDescription + " " + desc + "Trunc.";
            }

            return new object[] { begin, end, desc };
        }

        public object[] Process(SpikeVariable[] varsToUse, object[] args)
        {
            float begin = (float)args[0];
            float end = (float)args[1];
            string toAdd = (string)args[2];

            List<SpikeVariable> vars = new List<SpikeVariable>();

            foreach (SpikeVariable var in varsToUse)
            {

                IDataPool pool = null;
                if (var.SpikeType != SpikeVariableHeader.SpikeType.Pulse)
                {
                    int firstIndex = (int)(begin / var.Resolution);
                    int lastIndex = (int)(end / var.Resolution);

                    pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { lastIndex - firstIndex });
                    (var[0] as DataFrame).Pool.GetData(new int[] { firstIndex }, new int[] { lastIndex - firstIndex },
                        delegate(float[] data, uint[] indices, uint[] counts)
                        {
                            pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(data));
                        }
                    );
                }
                else
                {
                    bool started = false;
                    bool ended = false;
                    List<float> dataList = new List<float>();
                    (var[0] as DataFrame).Pool.GetData(null, null,
                        delegate(float[] data, uint[] indices, uint[] counts)
                        {
                            if (!ended)
                            {
                                for (int i = 0; i < data.Length; i++)
                                {
                                    if (!started && data[i] >= begin)
                                    {
                                        started = true;
                                    }

                                    if (started)
                                    {
                                        dataList.Add(data[i]);
                                    }

                                    if (data[i] >= end)
                                    {
                                        ended = true;
                                    }
                                }
                            }
                        }
                    );

                    pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(dataList));
                }

                SpikeVariable newvar = new SpikeVariable(new SpikeVariableHeader(var.Header.Description + " " + toAdd + "Trunc.",
                var.Header.UnitsName, var.Header.ExperimentName, 1 / var.Resolution, var.SpikeType));
                DataFrame frame = new DataFrame(newvar, pool,  CeresBase.General.Constants.Timeless);
                newvar.AddDataFrame(frame);

                vars.Add(newvar);
            }
            return vars.ToArray();
        }

        #region ICloneable Members

        public object Clone()
        {
            return new Truncate();
        }

        #endregion
    }
}
