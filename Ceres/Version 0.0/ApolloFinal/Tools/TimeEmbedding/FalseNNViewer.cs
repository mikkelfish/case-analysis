using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ApolloFinal.Tools.BatchProcessing;

namespace ApolloFinal.Tools.TimeEmbedding
{
    public partial class FalseNNViewer : Form, IBatchViewable
    {
        public FalseNNViewer()
        {
            InitializeComponent();
        }

        #region IBatchViewable Members

        public IBatchViewable[] View(IBatchViewable[] viewable)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}