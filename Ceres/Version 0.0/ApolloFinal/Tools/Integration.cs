//using System;
//using System.Collections.Generic;
//using System.Text;
//using CeresBase.Data;

//namespace ApolloFinal.Tools
//{
//    public static partial class TemporaryDataTools
//    {
//        public static SpikeVariable[] IntegrateVariables(SpikeVariable[] vars, int outputFreq, double timeConstant)
//        {
//            SpikeVariable[] toRet = new SpikeVariable[vars.Length];

//            for (int i = 0; i < vars.Length; i++)
//            {
//                float decay = (float)Math.Pow(Math.E, -(1000.0 / (vars[i].Header as SpikeVariableHeader).Frequency ) / timeConstant);
//                double writeEvery = ((vars[i].Header as SpikeVariableHeader).Frequency / (double)outputFreq);

//                IDataPool pool = null;

//                if (vars[i].SpikeType == SpikeVariableHeader.SpikeType.Raw ||
//                    vars[i].SpikeType == SpikeVariableHeader.SpikeType.Integrated)
//                {
//                    int ilength = (int)Math.Floor(vars[i][0].Pool.DimensionLengths[0] / (double)writeEvery);
//                    double dlength = vars[i][0].Pool.DimensionLengths[0] / (double)writeEvery;
//                    int[] length;
//                    if (ilength == dlength) length = new int[] { ilength -1 };
//                    else length = new int[] { ilength  };

//                    pool = new CeresBase.Data.DataPools.DataPoolNetCDF(length);

//                    int readCount = 0;
//                    float analogValue = 0;
//                    double current = writeEvery;
//                    int wrote = 0;

//                    float avg = 0;
//                    int count = 0;
//                    (vars[i][0] as DataFrame).Pool.GetData(null, null,
//                        delegate(float[] data, uint[] indices, uint[] counts)
//                        {
//                            for (int j = 0; j < data.Length; j++)
//                            {
//                                avg += data[j];
//                                count++;
//                            }
//                        }
//                    );
//                    avg /= count;

//                    (vars[i][0] as DataFrame).Pool.GetData(null, null,
//                        delegate(float[] data, uint[] indices, uint[] counts)
//                        {
//                            for (int j = 0; j < data.Length; j++, readCount++)
//                            {
//                                analogValue = analogValue * decay + (float)Math.Abs(data[j] - avg);
//                                if (readCount >= current)
//                                {
//                                    pool.SetDataValue(analogValue);
//                                    wrote++;
//                                    current += writeEvery;
//                                }
                                                                
                                
//                                //if (readCount != 0 && readCount % writeEvery == 0)
//                                //{
//                                //}
//                            }
//                        }
//                    );
//                }
//                else
//                {

//                    int ilength = (int)(vars[i].TotalLength*outputFreq);
//                    double dlength = vars[i].TotalLength*outputFreq;
//                    int[] length;
//                    if (ilength == dlength) length = new int[] { ilength - 1 };
//                    else length = new int[] { ilength };

//                    pool = new CeresBase.Data.DataPools.DataPoolNetCDF(length);

//                    float analogValue = 0;
//                    //int writeCount = 0;
//                    List<float> times = new List<float>();
//                    (vars[i][0] as DataFrame).Pool.GetData(null, null,
//                        delegate(float[] data, uint[] indices, uint[] counts)
//                        {
//                            times.AddRange(data);
//                        }
//                    );

//                    int timeIndex = 0;
//                    double time = 0;
//                    double lastTime = vars[i].TotalLength;

//                    int readCount = 0;
//                    while (time < lastTime)
//                    {
//                        if (timeIndex != times.Count && time >= times[timeIndex] * vars[i].Resolution)
//                        {
//                            analogValue = analogValue * decay + (short.MaxValue / 2);
//                            timeIndex++;
//                        }
//                        else analogValue = analogValue * decay;

//                        if (readCount != 0 && readCount % writeEvery == 0)
//                        {
//                            pool.SetDataValue(analogValue);
//                        }

//                        time += vars[i].Resolution;
//                        readCount++;
//                    }

//                    //for (int j = 0; j < length[0]; j++)
//                    //{
//                    //  

//                    //    if (j != 0 && j % writeEvery == 0)
//                    //    {
//                    //        pool.SetDataValue(analogValue);
//                    //    }
//                    //}
//                }

//                SpikeVariableHeader header = new SpikeVariableHeader(vars[i].Header.VarName + " Integrated " + outputFreq + "/" + timeConstant , vars[i].Header.UnitsName, vars[i].Header.ExperimentName,
//                    outputFreq, SpikeVariableHeader.SpikeType.Integrated);
//                header.Description = vars[i].Header.Description + " Integrated " + outputFreq + "/" + timeConstant;
//                toRet[i] = new SpikeVariable(header);
//                DataFrame frame = new DataFrame(toRet[i], pool, CeresBase.General.Constants.Timeless);
//                toRet[i].AddDataFrame(frame);
//            }
//            return toRet;
//        }
//    }
//}
