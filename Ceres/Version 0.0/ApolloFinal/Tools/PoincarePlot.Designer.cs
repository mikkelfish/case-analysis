//namespace ApolloFinal.Tools
//{
//    partial class PoincarePlot
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.components = new System.ComponentModel.Container();
//            this.label1 = new System.Windows.Forms.Label();
//            this.label2 = new System.Windows.Forms.Label();
//            this.label3 = new System.Windows.Forms.Label();
//            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
//            this.textBoxXMax = new System.Windows.Forms.TextBox();
//            this.textBoxYMax = new System.Windows.Forms.TextBox();
//            this.label4 = new System.Windows.Forms.Label();
//            this.numericUpDownSpeed = new System.Windows.Forms.NumericUpDown();
//            this.buttonPlay = new System.Windows.Forms.Button();
//            this.buttonCreate = new System.Windows.Forms.Button();
//            this.checkBoxAutoRange = new System.Windows.Forms.CheckBox();
//            this.label5 = new System.Windows.Forms.Label();
//            this.textBoxCluster = new System.Windows.Forms.TextBox();
//            this.textBoxSD = new System.Windows.Forms.TextBox();
//            this.label6 = new System.Windows.Forms.Label();
//            this.buttonCluster = new System.Windows.Forms.Button();
//            this.listBox1 = new System.Windows.Forms.ListBox();
//            this.checkBox1 = new System.Windows.Forms.CheckBox();
//            this.poincareControl1 = new ApolloFinal.Tools.PoincareControl();
//            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
//            this.saveImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
//            this.button1 = new System.Windows.Forms.Button();
//            this.buttonStatistics = new System.Windows.Forms.Button();
//            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpeed)).BeginInit();
//            this.contextMenuStrip1.SuspendLayout();
//            this.SuspendLayout();
//            // 
//            // label1
//            // 
//            this.label1.AutoSize = true;
//            this.label1.Location = new System.Drawing.Point(12, 9);
//            this.label1.Name = "label1";
//            this.label1.Size = new System.Drawing.Size(24, 13);
//            this.label1.TabIndex = 0;
//            this.label1.Text = "Nth";
//            // 
//            // label2
//            // 
//            this.label2.AutoSize = true;
//            this.label2.Location = new System.Drawing.Point(112, 9);
//            this.label2.Name = "label2";
//            this.label2.Size = new System.Drawing.Size(34, 13);
//            this.label2.TabIndex = 1;
//            this.label2.Text = "XMax";
//            // 
//            // label3
//            // 
//            this.label3.AutoSize = true;
//            this.label3.Location = new System.Drawing.Point(202, 10);
//            this.label3.Name = "label3";
//            this.label3.Size = new System.Drawing.Size(34, 13);
//            this.label3.TabIndex = 2;
//            this.label3.Text = "YMax";
//            // 
//            // numericUpDown1
//            // 
//            this.numericUpDown1.Location = new System.Drawing.Point(42, 7);
//            this.numericUpDown1.Minimum = new decimal(new int[] {
//            1,
//            0,
//            0,
//            0});
//            this.numericUpDown1.Name = "numericUpDown1";
//            this.numericUpDown1.Size = new System.Drawing.Size(64, 20);
//            this.numericUpDown1.TabIndex = 3;
//            this.numericUpDown1.Value = new decimal(new int[] {
//            1,
//            0,
//            0,
//            0});
//            // 
//            // textBoxXMax
//            // 
//            this.textBoxXMax.Location = new System.Drawing.Point(152, 7);
//            this.textBoxXMax.Name = "textBoxXMax";
//            this.textBoxXMax.Size = new System.Drawing.Size(44, 20);
//            this.textBoxXMax.TabIndex = 4;
//            this.textBoxXMax.Text = "1";
//            // 
//            // textBoxYMax
//            // 
//            this.textBoxYMax.Location = new System.Drawing.Point(242, 7);
//            this.textBoxYMax.Name = "textBoxYMax";
//            this.textBoxYMax.Size = new System.Drawing.Size(44, 20);
//            this.textBoxYMax.TabIndex = 5;
//            this.textBoxYMax.Text = "1";
//            // 
//            // label4
//            // 
//            this.label4.AutoSize = true;
//            this.label4.Location = new System.Drawing.Point(373, 11);
//            this.label4.Name = "label4";
//            this.label4.Size = new System.Drawing.Size(61, 13);
//            this.label4.TabIndex = 7;
//            this.label4.Text = "AnimSpeed";
//            // 
//            // numericUpDownSpeed
//            // 
//            this.numericUpDownSpeed.DecimalPlaces = 1;
//            this.numericUpDownSpeed.Increment = new decimal(new int[] {
//            1,
//            0,
//            0,
//            65536});
//            this.numericUpDownSpeed.Location = new System.Drawing.Point(440, 8);
//            this.numericUpDownSpeed.Maximum = new decimal(new int[] {
//            5,
//            0,
//            0,
//            0});
//            this.numericUpDownSpeed.Minimum = new decimal(new int[] {
//            1,
//            0,
//            0,
//            196608});
//            this.numericUpDownSpeed.Name = "numericUpDownSpeed";
//            this.numericUpDownSpeed.Size = new System.Drawing.Size(67, 20);
//            this.numericUpDownSpeed.TabIndex = 8;
//            this.numericUpDownSpeed.Value = new decimal(new int[] {
//            1,
//            0,
//            0,
//            65536});
//            // 
//            // buttonPlay
//            // 
//            this.buttonPlay.Location = new System.Drawing.Point(615, 7);
//            this.buttonPlay.Name = "buttonPlay";
//            this.buttonPlay.Size = new System.Drawing.Size(75, 23);
//            this.buttonPlay.TabIndex = 9;
//            this.buttonPlay.Text = "Play";
//            this.buttonPlay.UseVisualStyleBackColor = true;
//            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
//            // 
//            // buttonCreate
//            // 
//            this.buttonCreate.Location = new System.Drawing.Point(696, 7);
//            this.buttonCreate.Name = "buttonCreate";
//            this.buttonCreate.Size = new System.Drawing.Size(75, 23);
//            this.buttonCreate.TabIndex = 10;
//            this.buttonCreate.Text = "Create";
//            this.buttonCreate.UseVisualStyleBackColor = true;
//            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
//            // 
//            // checkBoxAutoRange
//            // 
//            this.checkBoxAutoRange.AutoSize = true;
//            this.checkBoxAutoRange.Location = new System.Drawing.Point(293, 9);
//            this.checkBoxAutoRange.Name = "checkBoxAutoRange";
//            this.checkBoxAutoRange.Size = new System.Drawing.Size(83, 17);
//            this.checkBoxAutoRange.TabIndex = 12;
//            this.checkBoxAutoRange.Text = "Auto Range";
//            this.checkBoxAutoRange.UseVisualStyleBackColor = true;
//            // 
//            // label5
//            // 
//            this.label5.AutoSize = true;
//            this.label5.Location = new System.Drawing.Point(10, 32);
//            this.label5.Name = "label5";
//            this.label5.Size = new System.Drawing.Size(71, 13);
//            this.label5.TabIndex = 14;
//            this.label5.Text = "Cluster Points";
//            // 
//            // textBoxCluster
//            // 
//            this.textBoxCluster.Location = new System.Drawing.Point(13, 49);
//            this.textBoxCluster.Name = "textBoxCluster";
//            this.textBoxCluster.Size = new System.Drawing.Size(68, 20);
//            this.textBoxCluster.TabIndex = 15;
//            this.textBoxCluster.Text = "10";
//            // 
//            // textBoxSD
//            // 
//            this.textBoxSD.Location = new System.Drawing.Point(12, 92);
//            this.textBoxSD.Name = "textBoxSD";
//            this.textBoxSD.Size = new System.Drawing.Size(68, 20);
//            this.textBoxSD.TabIndex = 17;
//            this.textBoxSD.Text = "3";
//            // 
//            // label6
//            // 
//            this.label6.AutoSize = true;
//            this.label6.Location = new System.Drawing.Point(9, 75);
//            this.label6.Name = "label6";
//            this.label6.Size = new System.Drawing.Size(22, 13);
//            this.label6.TabIndex = 16;
//            this.label6.Text = "SD";
//            // 
//            // buttonCluster
//            // 
//            this.buttonCluster.Location = new System.Drawing.Point(12, 118);
//            this.buttonCluster.Name = "buttonCluster";
//            this.buttonCluster.Size = new System.Drawing.Size(68, 23);
//            this.buttonCluster.TabIndex = 18;
//            this.buttonCluster.Text = "Cluster";
//            this.buttonCluster.UseVisualStyleBackColor = true;
//            this.buttonCluster.Click += new System.EventHandler(this.buttonCluster_Click);
//            // 
//            // listBox1
//            // 
//            this.listBox1.FormattingEnabled = true;
//            this.listBox1.Location = new System.Drawing.Point(12, 173);
//            this.listBox1.Name = "listBox1";
//            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
//            this.listBox1.Size = new System.Drawing.Size(68, 238);
//            this.listBox1.TabIndex = 19;
//            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
//            // 
//            // checkBox1
//            // 
//            this.checkBox1.AutoSize = true;
//            this.checkBox1.Location = new System.Drawing.Point(15, 147);
//            this.checkBox1.Name = "checkBox1";
//            this.checkBox1.Size = new System.Drawing.Size(63, 17);
//            this.checkBox1.TabIndex = 20;
//            this.checkBox1.Text = "Clusters";
//            this.checkBox1.UseVisualStyleBackColor = true;
//            // 
//            // poincareControl1
//            // 
//            this.poincareControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
//                        | System.Windows.Forms.AnchorStyles.Left)
//                        | System.Windows.Forms.AnchorStyles.Right)));
//            this.poincareControl1.AutoRange = false;
//            this.poincareControl1.BackColor = System.Drawing.SystemColors.ControlLightLight;
//            this.poincareControl1.Cluster = false;
//            this.poincareControl1.ClustersToDraw = null;
//            this.poincareControl1.ContextMenuStrip = this.contextMenuStrip1;
//            this.poincareControl1.Location = new System.Drawing.Point(87, 33);
//            this.poincareControl1.Loop = false;
//            this.poincareControl1.MaxXRange = 1F;
//            this.poincareControl1.MaxYRange = 1F;
//            this.poincareControl1.Name = "poincareControl1";
//            this.poincareControl1.SDMult = 1.5F;
//            this.poincareControl1.Size = new System.Drawing.Size(684, 597);
//            this.poincareControl1.TabIndex = 11;
//            // 
//            // contextMenuStrip1
//            // 
//            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
//            this.saveImageToolStripMenuItem});
//            this.contextMenuStrip1.Name = "contextMenuStrip1";
//            this.contextMenuStrip1.Size = new System.Drawing.Size(143, 26);
//            // 
//            // saveImageToolStripMenuItem
//            // 
//            this.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem";
//            this.saveImageToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
//            this.saveImageToolStripMenuItem.Text = "Save Image";
//            this.saveImageToolStripMenuItem.Click += new System.EventHandler(this.saveImageToolStripMenuItem_Click);
//            // 
//            // button1
//            // 
//            this.button1.Location = new System.Drawing.Point(6, 414);
//            this.button1.Name = "button1";
//            this.button1.Size = new System.Drawing.Size(75, 23);
//            this.button1.TabIndex = 21;
//            this.button1.Text = "Create Anim";
//            this.button1.UseVisualStyleBackColor = true;
//            this.button1.Click += new System.EventHandler(this.button1_Click);
//            // 
//            // buttonStatistics
//            // 
//            this.buttonStatistics.Location = new System.Drawing.Point(523, 6);
//            this.buttonStatistics.Name = "buttonStatistics";
//            this.buttonStatistics.Size = new System.Drawing.Size(75, 23);
//            this.buttonStatistics.TabIndex = 22;
//            this.buttonStatistics.Text = "Stats";
//            this.buttonStatistics.UseVisualStyleBackColor = true;
//            this.buttonStatistics.Click += new System.EventHandler(this.buttonStatistics_Click);
//            // 
//            // PoincarePlot
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.ClientSize = new System.Drawing.Size(783, 642);
//            this.Controls.Add(this.buttonStatistics);
//            this.Controls.Add(this.button1);
//            this.Controls.Add(this.checkBox1);
//            this.Controls.Add(this.listBox1);
//            this.Controls.Add(this.buttonCluster);
//            this.Controls.Add(this.textBoxSD);
//            this.Controls.Add(this.label6);
//            this.Controls.Add(this.textBoxCluster);
//            this.Controls.Add(this.label5);
//            this.Controls.Add(this.checkBoxAutoRange);
//            this.Controls.Add(this.poincareControl1);
//            this.Controls.Add(this.buttonCreate);
//            this.Controls.Add(this.buttonPlay);
//            this.Controls.Add(this.numericUpDownSpeed);
//            this.Controls.Add(this.label4);
//            this.Controls.Add(this.textBoxYMax);
//            this.Controls.Add(this.textBoxXMax);
//            this.Controls.Add(this.numericUpDown1);
//            this.Controls.Add(this.label3);
//            this.Controls.Add(this.label2);
//            this.Controls.Add(this.label1);
//            this.Name = "PoincarePlot";
//            this.Text = "Poincare Plot";
//            this.Load += new System.EventHandler(this.PoincarePlot_Load);
//            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpeed)).EndInit();
//            this.contextMenuStrip1.ResumeLayout(false);
//            this.ResumeLayout(false);
//            this.PerformLayout();

//        }

//        #endregion

//        private System.Windows.Forms.Label label1;
//        private System.Windows.Forms.Label label2;
//        private System.Windows.Forms.Label label3;
//        private System.Windows.Forms.NumericUpDown numericUpDown1;
//        private System.Windows.Forms.TextBox textBoxXMax;
//        private System.Windows.Forms.TextBox textBoxYMax;
//        private System.Windows.Forms.Label label4;
//        private System.Windows.Forms.NumericUpDown numericUpDownSpeed;
//        private System.Windows.Forms.Button buttonPlay;
//        private System.Windows.Forms.Button buttonCreate;
//        private System.Windows.Forms.CheckBox checkBoxAutoRange;
//        private System.Windows.Forms.Label label5;
//        private System.Windows.Forms.TextBox textBoxCluster;
//        private PoincareControl poincareControl1;
//        private System.Windows.Forms.TextBox textBoxSD;
//        private System.Windows.Forms.Label label6;
//        private System.Windows.Forms.Button buttonCluster;
//        private System.Windows.Forms.ListBox listBox1;
//        private System.Windows.Forms.CheckBox checkBox1;
//        private System.Windows.Forms.Button button1;
//        private System.Windows.Forms.Button buttonStatistics;
//        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
//        private System.Windows.Forms.ToolStripMenuItem saveImageToolStripMenuItem;
//    }
//}