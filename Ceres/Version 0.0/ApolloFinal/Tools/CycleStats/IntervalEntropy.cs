//using System;
//using System.Collections.Generic;
//using System.Text;
//using MathWorks.MATLAB.NET.Arrays;
//using System.Windows.Forms;
//using CeresBase.FlowControl.Adapters.Matlab;

//namespace ApolloFinal.Tools.CycleStats
//{
//    public static class IntervalEntropy
//    {
//        public static double[] CalcIntervalEntropy(float[] data)
//        {
//            EverythingMatlab.EverythingMatlabclass matlab = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;
//            lock (MatlabLoader.MatlabLockable)
//            {
                
                

//                MWNumericArray dataA = new MWNumericArray(data);
//                //[e,A,B]=sampentau(y,M,r,tau,sflag)
//                MWArray[] mwarray = matlab.IntEntropy(2, dataA);
//                MWNumericArray e = mwarray[0] as MWNumericArray;
//                MWNumericArray hist = mwarray[1] as MWNumericArray;
//                double eVal = ((double[,])e.ToArray(MWArrayComponent.Real))[0, 0];
//                double histVal = ((double[,])hist.ToArray(MWArrayComponent.Real))[0, 0];

//                dataA.Dispose();
//                e.Dispose();
//                hist.Dispose();
//                return new double[] { eVal, histVal };
//            }
//            //   MessageBox.Show("E: " + eVal.ToString() + "\nHist: " + histVal.ToString());

//            //int rank =0;
//            //int hrank = 0;
//            //double rankavg=0;
//            //double hrankavg=0;
//            //for (int i = 0; i < 19; i++)
//            //{
//            //    MWArray[] surrogates = suite.ItSurrDat(2, dataA, (MWNumericArray)(19 + i));
//            //    MWNumericArray surr = surrogates[0] as MWNumericArray;
//            //    double[,] dataSur = (double[,])surr.ToArray(MWArrayComponent.Real);
//            //    float[] realDataSur = new float[dataSur.GetLength(1)];
//            //    for (int j = 0; j < realDataSur.Length; j++)
//            //    {
//            //        realDataSur[j] = Convert.ToSingle(dataSur[0, j]);
//            //    }
//            //    surr.Dispose();

//            //    MWNumericArray surdataA = new MWNumericArray(realDataSur);
//            //    MWArray[] surmwarray = entropy.IntEntropy(2, surdataA);
//            //    MWNumericArray sure = surmwarray[0] as MWNumericArray;
//            //    MWNumericArray surhist = surmwarray[1] as MWNumericArray;

//            //    double sureval = ((double[,])sure.ToArray(MWArrayComponent.Real))[0,0];
//            //    double surhistval = ((double[,])surhist.ToArray(MWArrayComponent.Real))[0,0];

//            //    rankavg += sureval;
//            //    hrankavg += surhistval;
//            //    if (sureval > eVal) rank++;
//            //    if (surhistval > histVal) hrank++;

//            //    surdataA.Dispose();
//            //}

//            //rankavg /= 19.0;
//            //hrankavg /= 19.0;


//            //MessageBox.Show("Entropy: " + eVal.ToString("F4") + " Rank: " + (rank / 19.0).ToString("F0") + " Surr Avg: " + rankavg.ToString("F4") + "\n\n" +
//            //    "Hist Entropy: " + histVal.ToString("F4") + " Rank: " + (hrank / 19.0).ToString("F0") + " Surr Avg: " + hrankavg.ToString("F4") + "\n\n");


//        }
//    }
//}
