namespace ApolloFinal.Tools.Histograms
{
    partial class CreateHistograms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupNumHist = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.labelHist = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxWidth = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxNumBins = new System.Windows.Forms.TextBox();
            this.comboBoxVar1 = new System.Windows.Forms.ComboBox();
            this.comboBoxVar2 = new System.Windows.Forms.ComboBox();
            this.labelvar1 = new System.Windows.Forms.Label();
            this.labelvar2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxStart = new System.Windows.Forms.TextBox();
            this.textBoxEnd = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.textBoxSD = new System.Windows.Forms.TextBox();
            this.labelVar3 = new System.Windows.Forms.Label();
            this.comboBoxVar3 = new System.Windows.Forms.ComboBox();
            this.labelSD = new System.Windows.Forms.Label();
            this.textBoxOffset = new System.Windows.Forms.TextBox();
            this.labelOffset = new System.Windows.Forms.Label();
            this.checkBoxNorm = new System.Windows.Forms.CheckBox();
            this.checkBoxSD = new System.Windows.Forms.CheckBox();
            this.groupNumHist.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 92);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1038, 548);
            this.flowLayoutPanel1.TabIndex = 0;
            this.flowLayoutPanel1.Resize += new System.EventHandler(this.flowLayoutPanel1_Resize);
            // 
            // groupNumHist
            // 
            this.groupNumHist.Controls.Add(this.button5);
            this.groupNumHist.Controls.Add(this.button1);
            this.groupNumHist.Controls.Add(this.button2);
            this.groupNumHist.Controls.Add(this.button3);
            this.groupNumHist.Location = new System.Drawing.Point(12, 1);
            this.groupNumHist.Name = "groupNumHist";
            this.groupNumHist.Size = new System.Drawing.Size(144, 53);
            this.groupNumHist.TabIndex = 1;
            this.groupNumHist.TabStop = false;
            this.groupNumHist.Text = "Number Histograms";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(105, 19);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(27, 23);
            this.button5.TabIndex = 6;
            this.button5.Text = "6";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(39, 19);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(27, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(72, 19);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(27, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "4";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // comboBoxType
            // 
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "1st Order Auto",
            "MultiOrder Auto",
            "Cross Correlation",
            "Cycle Triggered Histogram"});
            this.comboBoxType.Location = new System.Drawing.Point(336, 6);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(168, 21);
            this.comboBoxType.TabIndex = 2;
            this.comboBoxType.SelectedIndexChanged += new System.EventHandler(this.comboBoxType_SelectedIndexChanged);
            // 
            // labelHist
            // 
            this.labelHist.AutoSize = true;
            this.labelHist.Location = new System.Drawing.Point(249, 9);
            this.labelHist.Name = "labelHist";
            this.labelHist.Size = new System.Drawing.Size(81, 13);
            this.labelHist.TabIndex = 3;
            this.labelHist.Text = "Histogram Type";
            this.labelHist.Click += new System.EventHandler(this.labelHist_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(249, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Bin Width (msec)";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBoxWidth
            // 
            this.textBoxWidth.Location = new System.Drawing.Point(336, 33);
            this.textBoxWidth.Name = "textBoxWidth";
            this.textBoxWidth.Size = new System.Drawing.Size(49, 20);
            this.textBoxWidth.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(397, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Num Bins";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBoxNumBins
            // 
            this.textBoxNumBins.Location = new System.Drawing.Point(455, 33);
            this.textBoxNumBins.Name = "textBoxNumBins";
            this.textBoxNumBins.Size = new System.Drawing.Size(49, 20);
            this.textBoxNumBins.TabIndex = 7;
            // 
            // comboBoxVar1
            // 
            this.comboBoxVar1.FormattingEnabled = true;
            this.comboBoxVar1.Location = new System.Drawing.Point(567, 7);
            this.comboBoxVar1.Name = "comboBoxVar1";
            this.comboBoxVar1.Size = new System.Drawing.Size(337, 21);
            this.comboBoxVar1.TabIndex = 8;
            this.comboBoxVar1.SelectedIndexChanged += new System.EventHandler(this.comboBoxVar1_SelectedIndexChanged);
            // 
            // comboBoxVar2
            // 
            this.comboBoxVar2.FormattingEnabled = true;
            this.comboBoxVar2.Location = new System.Drawing.Point(567, 34);
            this.comboBoxVar2.Name = "comboBoxVar2";
            this.comboBoxVar2.Size = new System.Drawing.Size(337, 21);
            this.comboBoxVar2.TabIndex = 9;
            this.comboBoxVar2.SelectedIndexChanged += new System.EventHandler(this.comboBoxVar2_SelectedIndexChanged);
            // 
            // labelvar1
            // 
            this.labelvar1.AutoSize = true;
            this.labelvar1.Location = new System.Drawing.Point(507, 9);
            this.labelvar1.Name = "labelvar1";
            this.labelvar1.Size = new System.Drawing.Size(32, 13);
            this.labelvar1.TabIndex = 10;
            this.labelvar1.Text = "Var 1";
            this.labelvar1.Click += new System.EventHandler(this.label3_Click);
            // 
            // labelvar2
            // 
            this.labelvar2.AutoSize = true;
            this.labelvar2.Location = new System.Drawing.Point(507, 37);
            this.labelvar2.Name = "labelvar2";
            this.labelvar2.Size = new System.Drawing.Size(32, 13);
            this.labelvar2.TabIndex = 11;
            this.labelvar2.Text = "Var 2";
            this.labelvar2.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Start (sec)";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(136, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "End (sec)";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // textBoxStart
            // 
            this.textBoxStart.Location = new System.Drawing.Point(76, 61);
            this.textBoxStart.Name = "textBoxStart";
            this.textBoxStart.Size = new System.Drawing.Size(54, 20);
            this.textBoxStart.TabIndex = 14;
            // 
            // textBoxEnd
            // 
            this.textBoxEnd.Location = new System.Drawing.Point(194, 61);
            this.textBoxEnd.Name = "textBoxEnd";
            this.textBoxEnd.Size = new System.Drawing.Size(49, 20);
            this.textBoxEnd.TabIndex = 15;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(162, 6);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(81, 20);
            this.numericUpDown1.TabIndex = 16;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(162, 27);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(81, 23);
            this.buttonCreate.TabIndex = 17;
            this.buttonCreate.Text = "Create";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // textBoxSD
            // 
            this.textBoxSD.Location = new System.Drawing.Point(336, 60);
            this.textBoxSD.Name = "textBoxSD";
            this.textBoxSD.Size = new System.Drawing.Size(49, 20);
            this.textBoxSD.TabIndex = 18;
            // 
            // labelVar3
            // 
            this.labelVar3.AutoSize = true;
            this.labelVar3.Location = new System.Drawing.Point(507, 64);
            this.labelVar3.Name = "labelVar3";
            this.labelVar3.Size = new System.Drawing.Size(32, 13);
            this.labelVar3.TabIndex = 20;
            this.labelVar3.Text = "Var 3";
            // 
            // comboBoxVar3
            // 
            this.comboBoxVar3.FormattingEnabled = true;
            this.comboBoxVar3.Location = new System.Drawing.Point(567, 61);
            this.comboBoxVar3.Name = "comboBoxVar3";
            this.comboBoxVar3.Size = new System.Drawing.Size(337, 21);
            this.comboBoxVar3.TabIndex = 19;
            this.comboBoxVar3.SelectedIndexChanged += new System.EventHandler(this.comboBoxVar3_SelectedIndexChanged);
            // 
            // labelSD
            // 
            this.labelSD.AutoSize = true;
            this.labelSD.Location = new System.Drawing.Point(249, 64);
            this.labelSD.Name = "labelSD";
            this.labelSD.Size = new System.Drawing.Size(73, 13);
            this.labelSD.TabIndex = 21;
            this.labelSD.Text = "Standard Dev";
            // 
            // textBoxOffset
            // 
            this.textBoxOffset.Location = new System.Drawing.Point(455, 61);
            this.textBoxOffset.Name = "textBoxOffset";
            this.textBoxOffset.Size = new System.Drawing.Size(49, 20);
            this.textBoxOffset.TabIndex = 23;
            // 
            // labelOffset
            // 
            this.labelOffset.AutoSize = true;
            this.labelOffset.Location = new System.Drawing.Point(397, 64);
            this.labelOffset.Name = "labelOffset";
            this.labelOffset.Size = new System.Drawing.Size(35, 13);
            this.labelOffset.TabIndex = 22;
            this.labelOffset.Text = "Offset";
            this.labelOffset.Click += new System.EventHandler(this.labelOffset_Click);
            // 
            // checkBoxNorm
            // 
            this.checkBoxNorm.AutoSize = true;
            this.checkBoxNorm.Location = new System.Drawing.Point(952, 23);
            this.checkBoxNorm.Name = "checkBoxNorm";
            this.checkBoxNorm.Size = new System.Drawing.Size(107, 17);
            this.checkBoxNorm.TabIndex = 24;
            this.checkBoxNorm.Text = "Sub Offset (CTH)";
            this.checkBoxNorm.UseVisualStyleBackColor = true;
            // 
            // checkBoxSD
            // 
            this.checkBoxSD.AutoSize = true;
            this.checkBoxSD.Location = new System.Drawing.Point(952, 46);
            this.checkBoxSD.Name = "checkBoxSD";
            this.checkBoxSD.Size = new System.Drawing.Size(71, 17);
            this.checkBoxSD.TabIndex = 25;
            this.checkBoxSD.Text = "Show SD";
            this.checkBoxSD.UseVisualStyleBackColor = true;
            this.checkBoxSD.CheckedChanged += new System.EventHandler(this.checkBoxSD_CheckedChanged);
            // 
            // CreateHistograms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 652);
            this.Controls.Add(this.checkBoxSD);
            this.Controls.Add(this.checkBoxNorm);
            this.Controls.Add(this.textBoxOffset);
            this.Controls.Add(this.labelOffset);
            this.Controls.Add(this.labelSD);
            this.Controls.Add(this.labelVar3);
            this.Controls.Add(this.comboBoxVar3);
            this.Controls.Add(this.textBoxSD);
            this.Controls.Add(this.buttonCreate);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.textBoxEnd);
            this.Controls.Add(this.textBoxStart);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.labelvar2);
            this.Controls.Add(this.labelvar1);
            this.Controls.Add(this.comboBoxVar2);
            this.Controls.Add(this.comboBoxVar1);
            this.Controls.Add(this.textBoxNumBins);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxWidth);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelHist);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.groupNumHist);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "CreateHistograms";
            this.Text = "CreateHistograms";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.groupNumHist.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox groupNumHist;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.Label labelHist;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxWidth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxNumBins;
        private System.Windows.Forms.ComboBox comboBoxVar1;
        private System.Windows.Forms.ComboBox comboBoxVar2;
        private System.Windows.Forms.Label labelvar1;
        private System.Windows.Forms.Label labelvar2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxStart;
        private System.Windows.Forms.TextBox textBoxEnd;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.TextBox textBoxSD;
        private System.Windows.Forms.Label labelVar3;
        private System.Windows.Forms.ComboBox comboBoxVar3;
        private System.Windows.Forms.Label labelSD;
        private System.Windows.Forms.TextBox textBoxOffset;
        private System.Windows.Forms.Label labelOffset;
        private System.Windows.Forms.CheckBox checkBoxNorm;
        private System.Windows.Forms.CheckBox checkBoxSD;

    }
}