using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;

namespace ApolloFinal.Tools.Histograms
{
    public abstract class CorrelationBase
    {
        private double sdMult;
        public double SDMult
        {
            get
            {
                return this.sdMult;
            }
        }

        private double offset;
        public double Offset
        {
            get { return offset; }
        }

        protected int numTotalCycles;
        public int TotalCycles
        {
            get { return numTotalCycles; }
        }

        protected int excludedCycles;
        public int ExcludedCycles
        {
            get { return excludedCycles; }
        }

        protected float startTime;
        public float StartTime
        {
            get { return startTime; }
        }

        protected float endTime;
        public float EndTime
        {
            get { return endTime; }
        }
	

        private Dictionary<string, string> stats = new Dictionary<string,string>();
        public Dictionary<string, string> Statistics
        {
            get { return stats; }
        }

        public HistogramStats.HistogramStatDelegate StatCallback;
	
	

        private float binWidth;
        /// <summary>
        /// In milliseconds
        /// </summary>
        public float BinWidth
        {
            get { return binWidth; }
        }

        public int NumberOfBins
        {
            get { return this.binCounts.Length; }
        }

        public float LargestBinCount
        {
            get
            {
                float largest = float.MinValue;
                for (int i = 0; i < binCounts.Length; i++)
			    {
                    if (this.binCounts[i] > largest)
                    {
                        largest = this.binCounts[i];
                    }
			    }
                return largest;
            }
        }

        public float TotalTime
        {
            get
            {
                return this.binCounts.Length * this.binWidth;
            }
        }

        protected float[] binCounts;
        public float[] BinCounts
        {
            get { return binCounts; }
        }

        protected float[] binSDS;
        public float[] BinSDS
        {
            get { return binSDS; }
        }

        public CorrelationBase(float binWidth, int numberOfBins, double sdMult, double offset)
        {
            this.binWidth = binWidth;
            this.binCounts = new float[numberOfBins];
            this.sdMult = sdMult;
            this.offset = offset;
            this.stats.Add("Binwidth", binWidth.ToString());
        }

        public abstract void CalculateHistograms(float start, float end);
    }
}
