using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using ApolloFinal;

namespace ApolloFinal.Tools.Histograms
{
    public class AutoSingleOrderHistogram : CorrelationBase
    {
        private SpikeVariable var1;


        //TODO Incorporate in redo
        public AutoSingleOrderHistogram(float binwidth, int numberOfBins, SpikeVariable var1, double sdMult, double offset)
            : base(binwidth, numberOfBins, sdMult, offset)
        {
            this.var1 = var1;
        }

        public override void CalculateHistograms(float start, float end)
        {
            this.startTime = start;
            this.endTime = end;
            //List<double> vals1 = new List<double>();

            //(var1[0] as DataFrame).Pool.GetData(null, null,
            //    delegate(float[] data, uint[] indices, uint[] counts)
            //    {
            //        for (int i = 0; i < data.Length; i++)
            //        {
            //            double val = data[i] * this.var1.Resolution * 1000.0;
            //            if (val < start)
            //            {
            //                continue;
            //            }

            //            if (val > end)
            //            {
            //                continue;
            //            }

            //            vals1.Add(val);
            //        }
            //    }
            // );

            float[] vals1 = var1.GetTimeSeriesData(start, end);

            double sd = 0;
            double mean = 0;
            int real = 0;
            for (int i = 0; i < vals1.Length - 1; i++)
            {
                if (vals1[i + 1] != vals1[i])
                {
                    mean += (vals1[i + 1] - vals1[i]);
                    real++;
                }
                else
                {
                    mean += (vals1[i + 2] - vals1[i]);
                    real++;
                    i+=2;
                }
            }
            mean /= real;

            for (int i = 0; i < vals1.Length - 1; i++)
            {
                double val = (vals1[i + 1] - vals1[i]);
                if (vals1[i + 1] == vals1[i])
                {
                    val = (vals1[i + 2] - vals1[i]);
                    i += 2;
                }
                sd += (val - mean) * (val - mean);
            }
            sd /= (real-1);
            sd = Math.Sqrt(sd);



            for (int i = 0; i < vals1.Length - 1; i++)
            {
                double val = (vals1[i + 1] - vals1[i]);
                if (vals1[i + 1] == vals1[i])
                {
                    val = (vals1[i + 2] - vals1[i]);
                    i += 2;
                }

                if (val < mean - sd * this.SDMult || val > mean + sd * this.SDMult)
                    continue;

                val += this.Offset;

                if (val < 0)
                {
                    val += this.TotalTime;
                }

                int bin = (int)(val / this.BinWidth);
                if (bin < this.binCounts.Length)
                    this.binCounts[bin]++;
            }
        }
    }
}
