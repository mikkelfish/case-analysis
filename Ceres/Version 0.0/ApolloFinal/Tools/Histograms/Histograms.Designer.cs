namespace ApolloFinal.Tools.Histograms
{
    partial class Histograms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
                   
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Histograms));
            this.comboVariables1 = new System.Windows.Forms.ComboBox();
            this.comboVariables2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.performAction = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.UIBeginTime = new System.Windows.Forms.TextBox();
            this.UIEndTime = new System.Windows.Forms.TextBox();
            this.textDump = new System.Windows.Forms.TextBox();
            this.UIBins = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.UIBinSize = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TEMPHEIGHT = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboVariables1
            // 
            this.comboVariables1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboVariables1.FormattingEnabled = true;
            this.comboVariables1.Location = new System.Drawing.Point(100, 12);
            this.comboVariables1.Name = "comboVariables1";
            this.comboVariables1.Size = new System.Drawing.Size(434, 21);
            this.comboVariables1.TabIndex = 0;
            // 
            // comboVariables2
            // 
            this.comboVariables2.Enabled = false;
            this.comboVariables2.FormattingEnabled = true;
            this.comboVariables2.Location = new System.Drawing.Point(100, 39);
            this.comboVariables2.Name = "comboVariables2";
            this.comboVariables2.Size = new System.Drawing.Size(434, 21);
            this.comboVariables2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select Channel:";
            // 
            // performAction
            // 
            this.performAction.Location = new System.Drawing.Point(540, 12);
            this.performAction.Name = "performAction";
            this.performAction.Size = new System.Drawing.Size(89, 48);
            this.performAction.TabIndex = 3;
            this.performAction.Text = "Do Stuff";
            this.performAction.UseVisualStyleBackColor = true;
            this.performAction.Click += new System.EventHandler(this.performAction_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Time:                             to";
            // 
            // UIBeginTime
            // 
            this.UIBeginTime.Location = new System.Drawing.Point(99, 77);
            this.UIBeginTime.Name = "UIBeginTime";
            this.UIBeginTime.Size = new System.Drawing.Size(62, 20);
            this.UIBeginTime.TabIndex = 5;
            this.UIBeginTime.Text = "50";
            // 
            // UIEndTime
            // 
            this.UIEndTime.Location = new System.Drawing.Point(195, 77);
            this.UIEndTime.Name = "UIEndTime";
            this.UIEndTime.Size = new System.Drawing.Size(62, 20);
            this.UIEndTime.TabIndex = 6;
            this.UIEndTime.Text = "60";
            // 
            // textDump
            // 
            this.textDump.Location = new System.Drawing.Point(15, 154);
            this.textDump.Multiline = true;
            this.textDump.Name = "textDump";
            this.textDump.Size = new System.Drawing.Size(754, 50);
            this.textDump.TabIndex = 7;
            this.textDump.Text = "need to make sure time units are all proper\r\n\r\nneed to pass to method only the se" +
                "lected variable, not all variables";
            this.textDump.WordWrap = false;
            // 
            // UIBins
            // 
            this.UIBins.Location = new System.Drawing.Point(99, 103);
            this.UIBins.Name = "UIBins";
            this.UIBins.Size = new System.Drawing.Size(62, 20);
            this.UIBins.TabIndex = 9;
            this.UIBins.Text = "3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Bins:";
            // 
            // UIBinSize
            // 
            this.UIBinSize.Location = new System.Drawing.Point(99, 128);
            this.UIBinSize.Name = "UIBinSize";
            this.UIBinSize.Size = new System.Drawing.Size(62, 20);
            this.UIBinSize.TabIndex = 11;
            this.UIBinSize.Text = "50";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(60, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Binsize:";
            // 
            // TEMPHEIGHT
            // 
            this.TEMPHEIGHT.Location = new System.Drawing.Point(714, 1);
            this.TEMPHEIGHT.Name = "TEMPHEIGHT";
            this.TEMPHEIGHT.Size = new System.Drawing.Size(57, 465);
            this.TEMPHEIGHT.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(292, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(391, 52);
            this.label5.TabIndex = 15;
            this.label5.Text = resources.GetString("label5.Text");
            // 
            // Histograms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 469);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TEMPHEIGHT);
            this.Controls.Add(this.UIBinSize);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.UIBins);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textDump);
            this.Controls.Add(this.UIEndTime);
            this.Controls.Add(this.UIBeginTime);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.performAction);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboVariables2);
            this.Controls.Add(this.comboVariables1);
            this.Name = "Histograms";
            this.Text = "Histograms - I only do auto first order right now, need to stick frequency in but" +
                " thats at home";
            this.Load += new System.EventHandler(this.performAction_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboVariables1;
        private System.Windows.Forms.ComboBox comboVariables2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button performAction;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox UIBeginTime;
        private System.Windows.Forms.TextBox UIEndTime;
        private System.Windows.Forms.TextBox textDump;
        private System.Windows.Forms.TextBox UIBins;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox UIBinSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel TEMPHEIGHT;
        private System.Windows.Forms.Label label5;
    }
}