using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;

namespace ApolloFinal.Tools.Histograms
{
    public class MultiOrderCorrelation : CorrelationBase
    {
        private SpikeVariable var1;
        public SpikeVariable Var1
        {
            get
            {
                return this.var1;
            }
        }

        private SpikeVariable var2;
        public SpikeVariable Var2
        {
            get
            {
                return this.var2;
            }
        }

        //TODO Incorporate in redo
        public MultiOrderCorrelation(float binwidth, int numberOfBins, SpikeVariable var1, SpikeVariable var2, double sdMult, double offset)
            : base(binwidth, numberOfBins, sdMult, offset)
        {
            this.var1 = var1;
            this.var2 = var2;

            if (this.var1 != var2)
            {
                this.StatCallback += new HistogramStats.HistogramStatDelegate(HistogramStats.CCStrength);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start">In milliseconds</param>
        /// <param name="end">In milliseconds</param>
        public override void CalculateHistograms(float start, float end)
        {
            this.startTime = start;
            this.endTime = end;
            List<float> vals1 = new List<float>();
            List<float> vals2 = new List<float>();


            (var1[0] as DataFrame).Pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        float val = (float)(data[i] * this.var1.Resolution * 1000.0);
                        if (val  < start)
                        {
                            continue;
                        }

                        if (val > end)
                        {
                            continue;
                        }

                        vals1.Add(val);
                    }
                }
             );

            (var2[0] as DataFrame).Pool.GetData(null, null,
                   delegate(float[] data, uint[] indices, uint[] counts)
                   {
                       for (int i = 0; i < data.Length; i++)
                       {
                           float val = (float)(data[i] * this.var2.Resolution * 1000.0);

                           if (val < start || val > end)
                           {
                               continue;
                           }
                           vals2.Add(val);
                       }
                   }
               );

           


            int firstGood = 0;
            for (int i = 0; i < vals1.Count; i++)
            {
                double origin = vals1[i] + this.Offset;
                for (int j = firstGood; j < vals2.Count; j++)
                {
                    if (vals2[j] <= origin)
                    {
                        if (j < vals2.Count - 1 && vals2[j] == vals2[j + 1])
                        {
                            j += 2;
                            firstGood += 3;
                        }
                        else firstGood++;
                        continue;
                    }
                    if (vals2[j] > origin + this.TotalTime) break;
                    double val = vals2[j] - origin;
                    //if(val < mean - sd*this.SDMult || val > mean + sd*this.SDMult)
                    //    continue;
                    
                    int bin = (int)(val / this.BinWidth);
                    if(bin < this.binCounts.Length)
                        this.binCounts[bin]++;

                    if (j < vals2.Count - 1 && vals2[j] == vals2[j + 1])
                    {
                        j += 2;
                    }
                }

                if (i < vals1.Count - 1 && vals1[i] == vals1[i + 1])
                {
                    i += 2;
                }
            }

            if (this.StatCallback != null)
            {
                this.StatCallback(this, vals1.ToArray(), vals2.ToArray());
            }

        }	
    }
}
