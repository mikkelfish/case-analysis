using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools.Histograms
{
    public static class HistogramStats
    {
        public delegate void HistogramStatDelegate(CorrelationBase histogram, float[] timeData, float[] otherData);
        public static void EtaSquared(CorrelationBase histogram, float[] timeData, float[] otherData)
        {
            if (!(histogram is NormalizedHistogram)) return;
            NormalizedHistogram hist = histogram as NormalizedHistogram;
            if (!hist.FromCell) return;
            float[][] cycleCounts = new float[20][];
            for (int i = 0; i < 20; i++)
            {
                cycleCounts[i] = new float[hist.TotalCycles];
            }

            for (int i = 0; i < otherData.Length; i++)
            {
                int div = (int)(otherData[i] / hist.Mean);
                double offset = otherData[i] - hist.Mean * div;
                double perc = offset / hist.Mean;
                int bin = (int)Math.Round(perc * 19, 0);
                cycleCounts[bin][div]++;

            }
            float[] binMeans = new float[20];
            float totalMeans = 0;
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < hist.TotalCycles; j++)
                {
                    binMeans[i] += cycleCounts[i][j];
                    totalMeans += cycleCounts[i][j];
                }
                binMeans[i] /= hist.TotalCycles;
            }
            totalMeans /= (20 * hist.TotalCycles);

            float intraVariance = 0;
            float interVariance = 0;
            for (int i = 0; i < 20; i++)
            {
                interVariance += (binMeans[i] - totalMeans) * (binMeans[i] - totalMeans);
                for (int j = 0; j < hist.TotalCycles; j++)
                {
                    intraVariance += (cycleCounts[i][j] - binMeans[i]) * (cycleCounts[i][j] - binMeans[i]);
                }
            }
            interVariance /= (19F);
            intraVariance /= ((hist.TotalCycles * 20) - 20);
            float etaSquared = interVariance / (interVariance + intraVariance);
            hist.Statistics.Add("Eta^2", Math.Round(etaSquared,2).ToString());
        }

        public static void MaxFiring(CorrelationBase histogram, float[] timeData, float[] otherData)
        {
            if (!(histogram is NormalizedHistogram)) return;
            NormalizedHistogram hist = histogram as NormalizedHistogram;
            hist.Statistics.Add("Max Firing", Math.Round(( hist.LargestBinCount / (histogram.TotalTime / 1000.0)),2).ToString());
        }

        public static void IncludeExclude(CorrelationBase histogram, float[] timeData, float[] otherData)
        {
            histogram.Statistics.Add("Included Cycles", histogram.TotalCycles.ToString());
            histogram.Statistics.Add("Excluded Cycles", histogram.ExcludedCycles.ToString());
        }

        public static void CCStrength(CorrelationBase histogram, float[] timeData, float[] otherData)
        {
            if (!(histogram is MultiOrderCorrelation)) return;
            MultiOrderCorrelation hist = histogram as MultiOrderCorrelation;

            double maxTime = 15;

            double beforeRate = 0;
            double afterRate = 0;

            int firingBin = (int)Math.Round((-hist.Offset) / hist.BinWidth,0);

            if (firingBin <= 0) return;

            double beforePerc = (-hist.Offset) / hist.TotalTime;

            for (int i = 0; i < firingBin; i++)
            {
                beforeRate += hist.BinCounts[i];
            }
            beforeRate /= (hist.EndTime - hist.StartTime) * beforePerc;

            for (int i = firingBin; i < hist.BinCounts.Length; i++)
            {
                afterRate += hist.BinCounts[i];
            }
            afterRate /= (hist.EndTime - hist.StartTime)* (1.0 - beforePerc);

            double e = afterRate * beforeRate * hist.BinWidth * (hist.EndTime - hist.StartTime);

            int numBinsToSearch = (int)(maxTime/hist.BinWidth);

            int firstBigBin = -1;
            int lastBigBin = -1;
            for (int i = firingBin; i < firingBin + numBinsToSearch; i++)
            {
                if (i >= hist.BinCounts.Length) break;
                if ((hist.BinCounts[i] - e) > 2 * Math.Sqrt(e))
                {
                    if (firstBigBin == -1)
                        firstBigBin = i;
                    lastBigBin = i;
                }
                else if (firstBigBin != -1) break;
            }
           
           
            double posAlpha = double.MaxValue;
            if(firstBigBin != -1)
            {
                double A = 0;
                for (int i = firstBigBin; i <= lastBigBin; i++)
			    {
			        A += (hist.BinCounts[i] - e);
			    }
                posAlpha = A * afterRate / e;

            }

            int minFiring = int.MaxValue;
            for (int i = firingBin; i < numBinsToSearch + firingBin; i++)
            {
                if (i >= hist.BinCounts.Length) break;
                if ((e - hist.BinCounts[i]) > 2 * Math.Sqrt(e))
                {
                    if (hist.BinCounts[i] < minFiring)
                        minFiring = (int)hist.BinCounts[i];
                }
            }
            double minAlpha = double.MaxValue;
            if (minFiring != int.MaxValue)
            {
                minAlpha = -(e - minFiring) / e;
            }

            if (posAlpha == double.MaxValue)
            {
                hist.Statistics.Add("Excit. alpha", "N/A");
            }
            else hist.Statistics.Add("Excit. alpha", Math.Round(posAlpha, 2).ToString());

            if (minAlpha == double.MaxValue)
            {
                hist.Statistics.Add("Inhib. alpha", "N/A");
            }
            else hist.Statistics.Add("Inhib. alpha", Math.Round(minAlpha, 2).ToString());

            //double posAlpha = (
        }
    }
}
