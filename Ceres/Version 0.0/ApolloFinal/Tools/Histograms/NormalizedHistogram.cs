using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;

namespace ApolloFinal.Tools.Histograms
{
    public class NormalizedHistogram : CorrelationBase
    {
        private Variable timeLine;
        private Variable otherVar;

        private double mean;
        public double Mean
        {
            get { return mean; }
        }

        private double sd;
        public double SD
        {
            get { return sd; }
        }


        private bool fromCell;
        public bool FromCell
        {
            get { return fromCell; }
        }
	
        public NormalizedHistogram(float binWidth, int numBins, SpikeVariable timeLine, SpikeVariable otherVar, double sdMult, double offset, bool fromCell)
            : base(binWidth, numBins, sdMult, offset)
        {
            this.timeLine = timeLine;
            this.otherVar = otherVar;
            this.fromCell = fromCell;

            if (fromCell)
            {
                this.StatCallback += new HistogramStats.HistogramStatDelegate(HistogramStats.EtaSquared);
                this.StatCallback += new HistogramStats.HistogramStatDelegate(HistogramStats.MaxFiring);
            }
            else this.StatCallback += new HistogramStats.HistogramStatDelegate(HistogramStats.IncludeExclude);

        }

        public override void CalculateHistograms(float start, float end)
        {
            this.startTime = start;
            this.endTime = end;
            List<float> tData = new List<float>();
            double timeRes = (timeLine.Header as SpikeVariableHeader).Resolution;


            this.timeLine[0].Pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        float val = (float)(data[i] * timeRes * 1000.0);
                        if (val < start || val > end) continue;
                        tData.Add(val);
                    }
                }
            );


            this.mean = 0;
            int cycles = 0;
            for (int i = 0; i < tData.Count - 1; i++)
            {
                if (tData[i] == tData[i + 1])
                {
                    this.mean += (tData[i + 2] - tData[i]);
                    i += 2;
                }
                else this.mean += (tData[i + 1] - tData[i]);
                cycles++;               
            }

            this.mean = this.mean / cycles;

            float[] mult = new float[cycles];
            this.sd = 0;
            for (int i = 0, realSpot = 0; i < tData.Count - 1; i++, realSpot++)
            {
                float width = tData[i + 1] - tData[i];
                if (tData[i] == tData[i + 1])
                {
                    width = tData[i + 2] - tData[i];
                    i += 2;
                }
                //Also do SD calculation here
                mult[realSpot] = (float)(this.mean / width);

                this.sd += (width - mean) * (width - mean);
            }
            this.sd /= mult.Length - 1;
            this.sd = Math.Sqrt(this.sd);

            bool[] include = new bool[mult.Length];
            int realCycles = 0;
            for (int i = 0, realSpot = 0; i < tData.Count - 1; i++, realSpot++)
            {
                float width = tData[i + 1] - tData[i];
                if (tData[i + 1] == tData[i])
                {
                    width = tData[i + 2] - tData[i];
                    i += 2;
                }
                if (width < this.mean - this.sd * this.SDMult || width > this.mean + this.sd * this.SDMult)
                {
                    include[realSpot] = false;
                }
                else
                {
                    include[realSpot] = true;
                    realCycles++;
                }
            }

            this.Statistics.Add("Num Breaths", realCycles.ToString());

            this.numTotalCycles = realCycles;
            this.excludedCycles = include.Length - realCycles;

            List<float> otherData = new List<float>();
            double otherRes = (otherVar.Header as SpikeVariableHeader).Resolution;
            int curMarker = 0;
            int realMarker = 0;
            float amtToAdd = 0;
            this.otherVar[0].Pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        float val = (float)(data[i] * otherRes * 1000.0);
                        if (i < data.Length - 1 && data[i] == data[i + 1])
                        {
                            i += 2;
                        }
                        if (curMarker >= tData.Count) continue;

                        //val < start || val > end || 

                        bool cont = false;
                        while (true)
                        {
                            if (curMarker + 1>= tData.Count)
                                break;

                            if (val < tData[curMarker])
                            {
                                cont = true;
                                break;
                            }

                            if (val >= tData[curMarker + 1])
                            {
                                if (tData[curMarker + 1] == tData[curMarker])
                                    curMarker += 3;
                                else curMarker++;

                                if (curMarker < tData.Count - 1 && include[realMarker])
                                {
                                    amtToAdd += (float)mean;
                                    //amtToSub += tData[curMarker + 1] - tData[curMarker];
                                }
                                realMarker++;
                            }
                            else break;
                        }
                        if (cont) continue;

                        if (curMarker + 1 >= tData.Count)
                        {
                            break;
                        }

                        if(include[realMarker])
                            otherData.Add(((val - tData[curMarker]) * mult[realMarker] + amtToAdd));
                    }
                }
            );

          

          //  this.averageFiringRate = otherData.Count / (realCycles * mean/1000.0);


            int firstGood = 0;
            double reference = this.Offset;
            for (int i = 0; i < realCycles; i++)
            {
                for (int j = firstGood; j < otherData.Count; j++)
                {
                    if (reference > otherData[j])
                    {
                        firstGood++;
                        continue;
                    }
                    if (otherData[j] > reference + this.TotalTime) break;

                    int bin = (int)Math.Round(((otherData[j] - reference) / this.BinWidth), 0);
                    if(bin < this.binCounts.Length)
                        this.binCounts[bin]++;
                }
                reference += mean;
            }

            if (this.StatCallback != null)
            {
                this.StatCallback(this, tData.ToArray(), otherData.ToArray());
            }

            if(this.FromCell) this.Statistics.Add("Max Bin Count", this.LargestBinCount.ToString());
        }
    }
}
