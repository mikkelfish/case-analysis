using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;

namespace ApolloFinal.Tools.Histograms
{
    class CycleTriggeredAverage : CorrelationBase
    {
        private Variable timeLine;
        private Variable otherVar;

        private double mean;
        public double Mean
        {
            get { return mean; }
        }

        private double sd;
        public double SD
        {
            get { return sd; }
        }

        private bool subOffset;

        public bool SubOffset
        {
            get { return subOffset; }
            set { subOffset = value; }
        }
	

        public CycleTriggeredAverage(float binWidth, int numBins, SpikeVariable timeLine, SpikeVariable otherVar, double sdMult, double offset, bool subOffset)
            : base(binWidth, numBins, sdMult, offset)
        {
            this.timeLine = timeLine;
            this.otherVar = otherVar;
            this.subOffset = subOffset;
            this.StatCallback += new HistogramStats.HistogramStatDelegate(HistogramStats.IncludeExclude);
        }

        public override void CalculateHistograms(float start, float end)
        {
            this.startTime = start;
            this.endTime = end;
            List<float> tData = new List<float>();
            double timeRes = (timeLine.Header as SpikeVariableHeader).Resolution;


            this.timeLine[0].Pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        float val = (float)(data[i] * timeRes * 1000.0);
                        if (val < start || val > end) continue;
                        tData.Add(val);
                    }
                }
            );


            this.mean = 0;
            int cycles = 0;
            for (int i = 0; i < tData.Count - 1; i++)
            {
                if (tData[i] == tData[i + 1])
                {
                    this.mean += (tData[i + 2] - tData[i]);
                    i += 2;
                }
                else this.mean += (tData[i + 1] - tData[i]);
                cycles++;
            }

            this.mean = this.mean / cycles;

            float[] mult = new float[cycles];
            this.sd = 0;
            for (int i = 0, realSpot = 0; i < tData.Count - 1; i++, realSpot++)
            {
                float width = tData[i + 1] - tData[i];
                if (tData[i] == tData[i + 1])
                {
                    width = tData[i + 2] - tData[i];
                    i += 2;
                }
                //Also do SD calculation here
                mult[realSpot] = (float)(this.mean / width);

                this.sd += (width - mean) * (width - mean);
            }
            this.sd /= mult.Length - 1;
            this.sd = Math.Sqrt(this.sd);

            bool[] include = new bool[mult.Length];
            int realCycles = 0;
            for (int i = 0, realSpot = 0; i < tData.Count - 1; i++, realSpot++)
            {
                float width = tData[i + 1] - tData[i];
                if (tData[i] == tData[i + 1])
                {
                    width = tData[i + 2] - tData[i];
                    i += 2;
                }
                if (width < this.mean - this.sd * this.SDMult || width > this.mean + this.sd * this.SDMult)
                {
                    include[realSpot] = false;
                }
                else
                {
                    include[realSpot] = true;
                    realCycles++;
                }
            }

            this.numTotalCycles = realCycles;
            this.excludedCycles = include.Length - realCycles;

            List<float> otherData = new List<float>();
            List<float> times = new List<float>();
            
            double otherRes = (otherVar.Header as SpikeVariableHeader).Resolution*1000.0;
            int curMarker = 0;
            int realMarker = 0;

            float amtToAdd = 0;

            float curTime = 0;
            this.otherVar[0].Pool.GetData(new int[] { 0 }, new int[] { this.otherVar[0].Pool.DimensionLengths[0] },
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    for (int i = 0; i < data.Length; i++, curTime += (float)otherRes)
                    {
                        if (curTime < start || curTime > end || curMarker >= tData.Count) continue;

                        bool cont = false;
                        while (true)
                        {
                            if (curMarker + 1 >= tData.Count)
                                break;

                            if (curTime < tData[curMarker])
                            {
                                cont = true;
                                break;
                            }

                            if (curTime >= tData[curMarker + 1])
                            {
                                if (tData[curMarker] == tData[curMarker + 1])
                                {
                                    curMarker += 3;
                                }
                                else curMarker++;

                                if (curMarker < tData.Count - 1 && include[realMarker])
                                {
                                    amtToAdd += (float)mean;
                                    //amtToSub += tData[curMarker + 1] - tData[curMarker];
                                }
                                realMarker++;
                            }
                            else break;
                        }
                        if (cont) continue;

                        if (curMarker + 1 >= tData.Count)
                        {
                            break;
                        }

                        if (include[realMarker])
                        {
                            otherData.Add(data[i]);
                            float time = (curTime - tData[curMarker]) * mult[realMarker] + amtToAdd;
                            times.Add(time);
                            
                            //otherData.Add(((val - tData[curMarker]) * mult[curMarker] + amtToAdd));
                        }
                    }
                }
            );


            float[] binSums = new float[this.binCounts.Length];


            this.binSDS = new float[this.binCounts.Length];
            float min = float.MaxValue;
            //if (this.subOffset)
            //{
            //    for (int i = 0; i < otherData.Count; i++)
            //    {
            //        if (otherData[i] < min)
            //            min = otherData[i];
            //    }

            //    for (int i = 0; i < otherData.Count; i++)
            //    {
            //        otherData[i] -= min;
            //    }
            //}

            for (int loop = 0; loop < 2; loop++)
            {
                int firstGood = 0;
                double reference = this.Offset;
                for (int i = 0; i < realCycles; i++)
                {
                    for (int j = firstGood; j < otherData.Count; j++)
                    {
                        if (reference > times[j])
                        {
                            firstGood++;
                            continue;
                        }
                        if (times[j] > reference + this.TotalTime) break;
                        int bin = (int)Math.Round((times[j] - reference) / this.BinWidth, 0);
                        if (bin < binSums.Length)
                        {
                            if (loop == 0)
                            {
                                binSums[bin] += otherData[j];
                                this.binCounts[bin]++;
                            }
                            else
                            {
                                this.binSDS[bin] += (otherData[j] - binSums[bin]) * (otherData[j] - binSums[bin]);
                            }
                        }
                    }
                    reference += mean;
                }

                if (loop == 0)
                {
                    for (int i = 0; i < this.binCounts.Length; i++)
                    {
                        if (this.binCounts[i] != 0)
                        {
                            binSums[i] = (binSums[i]) / this.binCounts[i];
                           // if (this.binCounts[i] < min) min = this.binCounts[i];                         
                        }
                    }

                    //if (this.subOffset)
                    //{
                    //    for (int i = 0; i < this.binCounts.Length; i++)
                    //    {
                    //        this.binCounts[i] -= min;
                    //    }
                    //}
                    //else min = 0;
                }
            }

          

            for (int i = 0; i < this.binCounts.Length; i++)
            {
                if (this.binCounts[i] != 0)
                {
                    this.binSDS[i] /= this.binCounts[i];
                    if (this.binSDS[i] < 0)
                    {
                        int bas = 0;
                    }
                    this.binSDS[i] = (float)Math.Sqrt(this.binSDS[i]);
                   
                }
            }


            for (int i = 0; i < binSums.Length; i++)
            {
                this.binCounts[i] = binSums[i];
            }

            //this.Statistics.Add("Max Bin Count", this.LargestBinCount.ToString());

        }

    }
}
