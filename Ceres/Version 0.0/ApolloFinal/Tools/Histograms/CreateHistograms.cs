using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CeresBase.Data;

namespace ApolloFinal.Tools.Histograms
{
    public partial class CreateHistograms : Form
    {
        SpikeVariable[] addedVars;

        private void fillComboBoxes()
        {
            object var1 = this.comboBoxVar1.SelectedItem;
            object var2 = this.comboBoxVar2.SelectedItem;
            object var3 = this.comboBoxVar3.SelectedItem;

            this.comboBoxVar1.Items.Clear();
            this.comboBoxVar2.Items.Clear();
            this.comboBoxVar3.Items.Clear();

            List<SpikeVariable> vars = new List<SpikeVariable>();
            if (addedVars != null) vars.AddRange(addedVars);
            foreach (SpikeVariable var in VariableSource.Variables)
            {
                vars.Add(var);
            }

            foreach (Variable var in vars)
            {
                if (!(var is SpikeVariable))
                {
                    continue;
                }

                SpikeVariable spikeVar = var as SpikeVariable;
                if (spikeVar.SpikeType == SpikeVariableHeader.SpikeType.Integrated)
                {
                    if (this.comboBoxType.SelectedIndex == 3)
                    {
                        this.comboBoxVar2.Items.Add(var);
                        this.comboBoxVar3.Items.Add(var);
                    }
                }
                else
                {
                    this.comboBoxVar1.Items.Add(var);
                    this.comboBoxVar2.Items.Add(var);
                    this.comboBoxVar3.Items.Add(var);
                }
            }


            if (var1 != null && this.comboBoxVar1.Items.Contains(var1))
            {
                this.comboBoxVar1.SelectedItem = var1;
            }
            else if (this.comboBoxVar1.Items.Count > 0) this.comboBoxVar1.SelectedIndex = 0;

            if (var2 != null && this.comboBoxVar2.Items.Contains(var2))
            {
                this.comboBoxVar2.SelectedItem = var2;
            }
            else if (this.comboBoxVar2.Items.Count > 0) this.comboBoxVar2.SelectedIndex = 0;

            if (var3 != null && this.comboBoxVar3.Items.Contains(var3))
            {
                this.comboBoxVar3.SelectedItem = var3;
            }
            else if(this.comboBoxVar3.Items.Count > 0) this.comboBoxVar3.SelectedIndex = 0;

        }

        public CreateHistograms() : this(null)
        {
            
           
        }

        public CreateHistograms(SpikeVariable[] addedVars)
        {
            this.addedVars = addedVars;

            InitializeComponent();

            this.fillComboBoxes();

            this.numericUpDown1.Increment = 1;
            this.numericUpDown1.InterceptArrowKeys = true;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.flowLayoutPanel1.Controls.Clear();
            this.flowLayoutPanel1.Controls.Add(new CorrelationControl());
            this.flowLayoutPanel1.Controls[0].Size = new Size(this.flowLayoutPanel1.Width, this.flowLayoutPanel1.Height);
            this.numericUpDown1.Minimum = 1;
            this.numericUpDown1.Maximum = 1;
        }

        private void flowLayoutPanel1_Resize(object sender, EventArgs e)
        {
            if (this.flowLayoutPanel1.Controls.Count == 1)
            {
                this.flowLayoutPanel1.Controls[0].Size = new Size(this.flowLayoutPanel1.Width, this.flowLayoutPanel1.Height);
            }
            else if (this.flowLayoutPanel1.Controls.Count == 2)
            {
                this.flowLayoutPanel1.Controls[0].Size = new Size(this.flowLayoutPanel1.Width, this.flowLayoutPanel1.Height / 2 - 5);
                this.flowLayoutPanel1.Controls[1].Size = new Size(this.flowLayoutPanel1.Width, this.flowLayoutPanel1.Height / 2 - 5);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.flowLayoutPanel1.Controls.Clear();
            this.flowLayoutPanel1.Controls.Add(new CorrelationControl());
            this.flowLayoutPanel1.Controls.Add(new CorrelationControl());
            this.flowLayoutPanel1.Controls[0].Size = new Size(this.flowLayoutPanel1.Width, this.flowLayoutPanel1.Height /2 -5);
            this.flowLayoutPanel1.Controls[1].Size = new Size(this.flowLayoutPanel1.Width, this.flowLayoutPanel1.Height / 2 - 5);
            this.numericUpDown1.Minimum = 1;
            this.numericUpDown1.Maximum = 2;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.flowLayoutPanel1.Controls.Clear();
            for (int i = 0; i < 4; i++)
            {
                this.flowLayoutPanel1.Controls.Add(new CorrelationControl());
                this.flowLayoutPanel1.Controls[i].Size = new Size(this.flowLayoutPanel1.Width / 2 - 15, this.flowLayoutPanel1.Height / 2 - 15);                                
            }
            this.numericUpDown1.Minimum = 1;
            this.numericUpDown1.Maximum = 4;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.flowLayoutPanel1.Controls.Clear();
            for (int i = 0; i < 8; i++)
            {
                this.flowLayoutPanel1.Controls.Add(new CorrelationControl());
                this.flowLayoutPanel1.Controls[i].Size = new Size(this.flowLayoutPanel1.Width / 4 - 15, this.flowLayoutPanel1.Height / 2 - 15);
            }
            this.numericUpDown1.Minimum = 1;
            this.numericUpDown1.Maximum = 8;
        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBoxType.SelectedIndex == 0)
            {
                this.comboBoxVar2.Enabled = false;
                this.comboBoxVar3.Enabled = false;

                this.labelvar1.Text = "Var1";
                this.labelvar2.Text = "N/A";
                this.labelVar3.Text = "N/A";

            }
            else if(this.comboBoxType.SelectedIndex == 1)
            {
                this.comboBoxVar2.Enabled = false;
                this.comboBoxVar3.Enabled = false;

                this.labelvar1.Text = "Var1";
                this.labelvar2.Text = "N/A";
                this.labelVar3.Text = "N/A";
            }
            else if (this.comboBoxType.SelectedIndex == 2)
            {
                this.comboBoxVar2.Enabled = true;
                this.comboBoxVar3.Enabled = false;

                this.labelvar1.Text = "Var1";
                this.labelvar2.Text = "Var2";
                this.labelVar3.Text = "N/A";

            }
            else if (this.comboBoxType.SelectedIndex == 3)
            {
                this.comboBoxVar2.Enabled = true;
                this.comboBoxVar3.Enabled = true;

                this.labelvar1.Text = "Trigger";
                this.labelvar2.Text = "Var1";
                this.labelVar3.Text = "Var2 (Opt)";
            }
         
            this.fillComboBoxes();

        }

        private int lastVal = -1;
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (this.lastVal != -1)
            {
                CorrelationControl lastControl = (CorrelationControl)this.flowLayoutPanel1.Controls[this.lastVal-1];
                lastControl.BinWidth = float.Parse(this.textBoxWidth.Text);
                lastControl.NumberBins = int.Parse(this.textBoxNumBins.Text);
                lastControl.Start = float.Parse(this.textBoxStart.Text);
                lastControl.End = float.Parse(this.textBoxEnd.Text);
                lastControl.CorrelationType = this.comboBoxType.SelectedIndex;
                lastControl.SDRange = double.Parse(this.textBoxSD.Text);
                lastControl.Variable1 = (SpikeVariable)this.comboBoxVar1.SelectedItem;
                lastControl.Variable2 = (SpikeVariable)this.comboBoxVar2.SelectedItem;
                lastControl.Variable3 = (SpikeVariable)this.comboBoxVar3.SelectedItem;
                lastControl.Offset = double.Parse(this.textBoxOffset.Text);
                
                lastControl.DisplaySDs = this.checkBoxSD.Checked;

            }

            CorrelationControl control = (CorrelationControl)this.flowLayoutPanel1.Controls[(int)this.numericUpDown1.Value-1];
            this.textBoxWidth.Text = control.BinWidth.ToString();
            this.textBoxNumBins.Text = control.NumberBins.ToString();
            this.textBoxStart.Text = control.Start.ToString();
            this.textBoxEnd.Text = control.End.ToString();
            this.comboBoxType.SelectedIndex = control.CorrelationType;
            this.comboBoxVar1.SelectedItem = control.Variable1;
            this.comboBoxVar2.SelectedItem = control.Variable2;
            this.comboBoxVar3.SelectedItem = control.Variable3;
            this.textBoxSD.Text = control.SDRange.ToString();
            this.textBoxOffset.Text = control.Offset.ToString();
            this.lastVal = (int)this.numericUpDown1.Value;
            this.checkBoxSD.Checked = control.DisplaySDs;

        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            CorrelationControl lastControl = (CorrelationControl)this.flowLayoutPanel1.Controls[(int)this.numericUpDown1.Value-1];

            lastControl.DisplaySDs = this.checkBoxSD.Checked;

            lastControl.BinWidth = float.Parse(this.textBoxWidth.Text);
            lastControl.NumberBins = int.Parse(this.textBoxNumBins.Text);
            lastControl.Start = float.Parse(this.textBoxStart.Text);
            lastControl.End = float.Parse(this.textBoxEnd.Text);
            lastControl.CorrelationType = this.comboBoxType.SelectedIndex;
            lastControl.Variable1 = (SpikeVariable)this.comboBoxVar1.SelectedItem;
            lastControl.Variable2 = (SpikeVariable)this.comboBoxVar2.SelectedItem;
            lastControl.Variable3 = (SpikeVariable)this.comboBoxVar3.SelectedItem;
            lastControl.SDRange = double.Parse(this.textBoxSD.Text);
            lastControl.Offset = double.Parse(this.textBoxOffset.Text);

            lastControl.Normalize = this.checkBoxNorm.Checked;


            foreach (CorrelationControl control in this.flowLayoutPanel1.Controls)
            {
                control.CalcHistograms();
            }
        }

        private void labelHist_Click(object sender, EventArgs e)
        {
            foreach (CorrelationControl control in this.flowLayoutPanel1.Controls)
            {
                control.CorrelationType = this.comboBoxType.SelectedIndex;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            if (!this.label1.Enabled) return;
            foreach (CorrelationControl control in this.flowLayoutPanel1.Controls)
            {
                control.BinWidth = float.Parse(this.textBoxWidth.Text);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            foreach (CorrelationControl control in this.flowLayoutPanel1.Controls)
            {
                control.NumberBins = int.Parse(this.textBoxNumBins.Text);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            foreach (CorrelationControl control in this.flowLayoutPanel1.Controls)
            {
                control.Variable1 = (SpikeVariable)this.comboBoxVar1.SelectedItem;
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            foreach (CorrelationControl control in this.flowLayoutPanel1.Controls)
            {
                control.Variable2 = (SpikeVariable)this.comboBoxVar2.SelectedItem;
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {
            foreach (CorrelationControl control in this.flowLayoutPanel1.Controls)
            {
                control.Start = float.Parse(this.textBoxStart.Text);
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            foreach (CorrelationControl control in this.flowLayoutPanel1.Controls)
            {
                control.End = float.Parse(this.textBoxEnd.Text);
            }
        }

        private void comboBoxVar1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.flowLayoutPanel1.Controls.Count != 0 &&  this.flowLayoutPanel1.Controls.Count > (int)this.numericUpDown1.Value - 1)
            {
                CorrelationControl corCont = (CorrelationControl)this.flowLayoutPanel1.Controls[(int)this.numericUpDown1.Value - 1];
                corCont.Variable1 = (SpikeVariable)this.comboBoxVar1.SelectedItem;
            }
        }

        private void comboBoxVar2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.flowLayoutPanel1.Controls.Count != 0 && this.flowLayoutPanel1.Controls.Count > (int)this.numericUpDown1.Value - 1)
            {
                CorrelationControl corCont = (CorrelationControl)this.flowLayoutPanel1.Controls[(int)this.numericUpDown1.Value - 1];
                corCont.Variable2 = (SpikeVariable)this.comboBoxVar2.SelectedItem;
            }
        }

        private void comboBoxVar3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.flowLayoutPanel1.Controls.Count != 0 && this.flowLayoutPanel1.Controls.Count > (int)this.numericUpDown1.Value - 1)
            {
                CorrelationControl corCont = (CorrelationControl)this.flowLayoutPanel1.Controls[(int)this.numericUpDown1.Value - 1];
                corCont.Variable3 = (SpikeVariable)this.comboBoxVar3.SelectedItem;
            }
        }

        private void labelOffset_Click(object sender, EventArgs e)
        {
            foreach (CorrelationControl control in this.flowLayoutPanel1.Controls)
            {
                control.Offset = float.Parse(this.textBoxOffset.Text);
            }
        }

        private void checkBoxSD_CheckedChanged(object sender, EventArgs e)
        {
            
        }



    }
}