using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CeresBase.Data;
using CeresBase.IO.ImageCreation;
using CeresBase.IO;

namespace ApolloFinal.Tools.Histograms
{
    public partial class CorrelationControl : UserControl, IGenImage
    {
        private CorrelationBase[] correlations;
        public CorrelationBase[] Correlations
        {
            get { return correlations; }
            set 
            { 
                correlations = value;
                if(this.correlations.Length > 0)
                    this.selectedCorrelation = correlations[0];
            }
        }

        private CorrelationBase selectedCorrelation;
        public CorrelationBase SelectedCorrelation
        {
            get { return selectedCorrelation; }
            set { selectedCorrelation = value; }
        }

        private double offset;

        public double Offset
        {
            get { return offset; }
            set { offset = value; }
        }
	

        private int numbins;
        public int NumberBins
        {
            get { return numbins; }
            set { numbins = value; this.setLabel(); }
        }

        private float binWidth;
        public float BinWidth
        {
            get { return binWidth; }
            set { binWidth = value; this.setLabel(); }
        }

        private float start;
        public float Start
        {
            get { return start; }
            set { start = value; this.setLabel(); }
        }

        private float end;
        public float End
        {
            get { return end; }
            set { end = value; this.setLabel(); }
        }

        private int correlationType;
        public int CorrelationType
        {
            get { return correlationType; }
            set { correlationType = value; this.setLabel(); }
        }

        private SpikeVariable var1;
        public SpikeVariable Variable1
        {
            get { return var1; }
            set { var1 = value; this.setLabel(); }
        }

        private SpikeVariable var2;
        public SpikeVariable Variable2
        {
            get { return var2; }
            set { var2 = value; this.setLabel(); }
        }

        private SpikeVariable var3;
        public SpikeVariable Variable3
        {
            get { return var3; }
            set { var3 = value; }
        }
	

        private double sdMult;
        public double SDRange
        {
            get { return sdMult; }
            set { sdMult = value; }
        }

        private bool normal;

        public bool Normalize
        {
            get { return normal; }
            set { normal = value; }
        }

        public bool DisplaySDs { get; set; }

        private void setLabel()
        {
            this.labelDesc.Text = "Description: " +
                "$CORDESC$" + " NumBins: " + this.numbins.ToString() +
                " BinWidth: " + this.binWidth.ToString();

            if(this.var1 != null)
                this.labelDesc.Text += " Var1: " + this.var1.Header.Description;

            if (this.var2 != null) this.labelDesc.Text += " Var2: " + this.var2.Header.Description;

            if(this.var3 != null) this.labelDesc.Text += " Var3: " + this.var3.Header.Description;

            this.labelDesc.Text +=    " Start: " + this.start.ToString() +
                " End: " + this.end.ToString();

            if (this.correlationType == 0) this.labelDesc.Text = this.labelDesc.Text.Replace("$CORDESC$", "1st Order Auto");
            else if (this.correlationType == 1) this.labelDesc.Text = this.labelDesc.Text.Replace("$CORDESC$", "MultiOrder Auto");
            else if (this.correlationType == 2) this.labelDesc.Text = this.labelDesc.Text.Replace("$CORDESC$", "Cross Correlation");
            else if (this.correlationType == 3) this.labelDesc.Text = this.labelDesc.Text.Replace("$CORDESC$", "Normalized Cycle Triggered");
        }

        public CorrelationControl()
        {
            InitializeComponent();
            this.binWidth = 50;
            this.numbins = 100;
            this.start = 0;
            this.end = 0;
            this.correlationType = 0;
            this.sdMult = 1.5;
        }

        public void MakeImage(ImageCreator creator, Rectangle clipRectangle)
        {
            if (this.correlations == null) return;

            int leftPadding = 10;
            int rightPadding = 5;
            int topPadding = 10;
            int bottomPadding = 10;

            int drawableHeight = clipRectangle.Height - bottomPadding - topPadding;

            int labelY = topPadding;
            int maxX = int.MinValue;

            ImageContainer container = new ImageContainer(null, clipRectangle);
            creator.AddImageElement(container);

            Font font = new Font(FontFamily.GenericSansSerif, 12);
            float[] minYs = new float[this.correlations.Length];
            float[] maxYs = new float[this.correlations.Length];

            
            List<string> allDrawn = new List<string>();

            for (int corrnum = 0; corrnum < this.correlations.Length; corrnum++ )
            {
                CorrelationBase corr = this.correlations[corrnum];

                foreach (string stat in corr.Statistics.Keys)
                {
                    if (allDrawn.Contains(stat)) continue;
                    string toDraw = stat + ": " + corr.Statistics[stat];
                    creator.AddImageElement(new TextElement(container, font, toDraw, new Point(5, labelY)));
                    labelY += 15;
                    allDrawn.Add(stat);
                }

                for (int i = 0; i < corr.BinCounts.Length; i++)
                {
                    if (!this.DisplaySDs || corr.BinSDS == null || corr != this.selectedCorrelation)
                    {
                        if (corr.BinCounts[i] > maxYs[corrnum])
                        {
                            maxYs[corrnum] = corr.BinCounts[i];
                        }

                        if (corr.BinCounts[i] < minYs[corrnum])
                        {
                            minYs[corrnum] = corr.BinCounts[i];
                        }
                    }
                    else
                    {
                        if (corr.BinCounts[i] + corr.BinSDS[i] > maxYs[corrnum])
                        {
                            maxYs[corrnum] = corr.BinCounts[i] + corr.BinSDS[i];
                        }

                        if (corr.BinCounts[i] - corr.BinSDS[i] < minYs[corrnum])
                        {
                            minYs[corrnum] = corr.BinCounts[i] - corr.BinSDS[i];
                        }
                    }
                }

            }


            if (!this.Normalize)
            {
                float allMin = float.MaxValue;
                float allMax = float.MinValue;

                for (int i = 0; i < minYs.Length; i++)
                {
                    if (minYs[i] < allMin) allMin = minYs[i];
                    if (maxYs[i] > allMax) allMax = maxYs[i];
                }

                for (int i = 0; i < minYs.Length; i++)
                {
                    minYs[i] = allMin;
                    maxYs[i] = allMax;
                }
            }


            leftPadding = 180;


            for (int corrnum = 0; corrnum < this.correlations.Length; corrnum++ )
            {
                CorrelationBase corr = this.correlations[corrnum];
                if (corr.LargestBinCount == 0) continue;

                float widthPerBin = (clipRectangle.Width - leftPadding - rightPadding) / (float)corr.NumberOfBins;

                if (this.correlations.Length > 1)
                {

                    Lines lines = new Lines(container);
                    Lines sd1line = new Lines(container);
                    Lines sd2line = new Lines(container);
                    sd1line.Color = Color.Red;
                    sd2line.Color = Color.Red;


                    for (int i = 0; i < corr.NumberOfBins; i++)
                    {
                        int height = (int)((corr.BinCounts[i] - minYs[corrnum]) / (maxYs[corrnum] - minYs[corrnum]) * drawableHeight);

                        lines.AddPoint(new Point((int)(leftPadding + (i * widthPerBin)), (int)(clipRectangle.Height - bottomPadding - height)));
                        lines.AddPoint(new Point((int)(leftPadding + ((i + 1) * widthPerBin)), (int)(clipRectangle.Height - bottomPadding - height)));

                        if (this.DisplaySDs && corr.BinSDS != null && corr == this.selectedCorrelation)
                        {

                            int sdHeight1 = (int)((corr.BinCounts[i] + corr.BinSDS[i] - minYs[corrnum]) / (maxYs[corrnum] - minYs[corrnum]) * drawableHeight);
                            int sdHeight2 = (int)((corr.BinCounts[i] - corr.BinSDS[i] - minYs[corrnum]) / (maxYs[corrnum] - minYs[corrnum]) * drawableHeight);

                            sd1line.AddPoint(new PointF(leftPadding + (i * widthPerBin), clipRectangle.Height - bottomPadding - sdHeight1));
                            sd1line.AddPoint(new PointF(leftPadding + ((i + 1) * widthPerBin), clipRectangle.Height - bottomPadding - sdHeight1));

                            sd2line.AddPoint(new PointF(leftPadding + (i * widthPerBin), clipRectangle.Height - bottomPadding - sdHeight2));
                            sd2line.AddPoint(new PointF(leftPadding + ((i + 1) * widthPerBin), clipRectangle.Height - bottomPadding - sdHeight2));

                        }
                    }

                    Pen pen = null;
                    if (corr == this.selectedCorrelation)
                    {
                        pen = Pens.Black;
                    }
                    else pen = Pens.Gray;

                    if (corr == this.selectedCorrelation)
                    {
                        lines.Color = Color.Black;
                    }
                    else lines.Color = Color.Gray;

                    creator.AddImageElement(lines);

                    if (this.DisplaySDs && corr.BinSDS != null && this.selectedCorrelation == corr)
                    {
                        creator.AddImageElement(sd1line);
                        creator.AddImageElement(sd2line);
                    }
                }
                else
                {
                    for (int i = 0; i < corr.NumberOfBins; i++)
                    {
                        int height = (int)((corr.BinCounts[i] - minYs[corrnum]) / (maxYs[corrnum] - minYs[corrnum]) * drawableHeight);                        

                        RectangleElement ele = new RectangleElement(container, new PointF(leftPadding + (i * widthPerBin) - 1, clipRectangle.Height - bottomPadding - height), widthPerBin + 2, height);
                        ele.Fill = true;
                        ele.Color = Color.Black;
                        creator.AddImageElement(ele);
                    }

                }
            }

            float perc = -(float)this.selectedCorrelation.Offset / this.selectedCorrelation.TotalTime;
            if (perc >= 0)
            {
                Lines arrow = new Lines(container);
                arrow.AddPoint(new PointF(perc * (clipRectangle.Width - rightPadding - leftPadding) + leftPadding, clipRectangle.Height - bottomPadding));
                arrow.AddPoint(new PointF(perc * (clipRectangle.Width - rightPadding - leftPadding) + leftPadding + 8, clipRectangle.Height - bottomPadding + 8));
                arrow.AddPoint(new PointF(perc * (clipRectangle.Width - rightPadding - leftPadding) + leftPadding - 8, clipRectangle.Height - bottomPadding + 8));
                arrow.AddPoint(new PointF(perc * (clipRectangle.Width - rightPadding - leftPadding) + leftPadding, clipRectangle.Height - bottomPadding));
                creator.AddImageElement(arrow);
            }

            creator.AddImageElement(new Line(container, new Point(leftPadding, topPadding), new Point(leftPadding, clipRectangle.Height - bottomPadding)));
            creator.AddImageElement(new Line(container, new Point(leftPadding, clipRectangle.Height - bottomPadding),
                new Point(clipRectangle.Width - rightPadding, clipRectangle.Height - bottomPadding)));


            float stringPosY = topPadding;
            float sum = 0;
            for (int i = 0; i < correlations.Length; i++)
            {
                TextElement label = new TextElement(container, font, Math.Round(maxYs[i], 2).ToString(), new PointF(leftPadding - 65, stringPosY));
                creator.AddImageElement(label);
                stringPosY += 25;
                sum += 25;
            }

            stringPosY = clipRectangle.Height - bottomPadding - sum;
            for (int i = 0; i < correlations.Length; i++)
            {
                TextElement label = new TextElement(container, font, Math.Round(minYs[i], 2).ToString(), new PointF(leftPadding - 65, stringPosY));
                creator.AddImageElement(label);
                stringPosY += 25;
            }

            
        }

        private void CorrelationControl_Paint(object sender, PaintEventArgs e)
        {
            if (this.correlations == null) return;

            int leftPadding = 10;
            int rightPadding = 5;
            int topPadding = 10;
            int bottomPadding = 10;

            Control toPaint = (sender as Control);

            int drawableHeight = toPaint.Height - bottomPadding - topPadding;

            int labelY = topPadding;
            int maxX = int.MinValue;


            float[] minYs = new float[this.correlations.Length];
            float[] maxYs = new float[this.correlations.Length];
                Font font = new Font(FontFamily.GenericSansSerif, 12);

            List<string> allDrawn = new List<string>();
            for (int corrnum = 0; corrnum < this.correlations.Length; corrnum++)
            {
                CorrelationBase corr = this.correlations[corrnum];
                foreach (string stat in corr.Statistics.Keys)
                {
                    if (allDrawn.Contains(stat)) continue;

                    string toDraw = stat + ": " + corr.Statistics[stat];
                    e.Graphics.DrawString(toDraw, font, Brushes.Black, new PointF(5, labelY));
                  //  creator.AddImageElement(new TextElement(container, font, toDraw, new Point(5, labelY)));
                    labelY += (int)e.Graphics.MeasureString(toDraw,font).Height;
                    int width = (int)e.Graphics.MeasureString(toDraw, font).Width;
                    if (width > maxX)
                    {
                        maxX = width;
                    }
                    allDrawn.Add(stat);
                }

                minYs[corrnum] = float.MaxValue;
                maxYs[corrnum] = float.MinValue;

                for (int i = 0; i < corr.BinCounts.Length; i++)
                {
                    if (!this.DisplaySDs || corr.BinSDS == null || corr != this.selectedCorrelation)
                    {
                        if (corr.BinCounts[i] > maxYs[corrnum])
                        {
                            maxYs[corrnum] = corr.BinCounts[i];
                        }

                        if (corr.BinCounts[i] < minYs[corrnum])
                        {
                            minYs[corrnum] = corr.BinCounts[i];
                        }
                    }
                    else
                    {
                        if (corr.BinCounts[i] + corr.BinSDS[i] > maxYs[corrnum])
                        {
                            maxYs[corrnum] = corr.BinCounts[i] + corr.BinSDS[i];
                        }

                        if (corr.BinCounts[i] - corr.BinSDS[i] < minYs[corrnum])
                        {
                            minYs[corrnum] = corr.BinCounts[i] - corr.BinSDS[i];
                        }
                    }
                }
            }

            if (!this.Normalize)
            {
                float allMin = float.MaxValue;
                float allMax = float.MinValue;

                for (int i = 0; i < minYs.Length; i++)
                {
                    if (minYs[i] < allMin) allMin = minYs[i];
                    if (maxYs[i] > allMax) allMax = maxYs[i];
                }

                for (int i = 0; i < minYs.Length; i++)
                {
                    minYs[i] = allMin;
                    maxYs[i] = allMax;
                }
            }
            

            leftPadding = maxX + 80;

            for (int corrnum = 0; corrnum < this.correlations.Length; corrnum++ )
            {
                CorrelationBase corr = this.correlations[corrnum];
                if (corr.LargestBinCount == 0) continue;

                float widthPerBin = (toPaint.Width - leftPadding - rightPadding) / (float)corr.NumberOfBins;

                float largest = corr.LargestBinCount;

                if (this.DisplaySDs && corr.BinSDS != null)
                {
                    for (int i = 0; i < corr.NumberOfBins; i++)
                    {
                        if (corr.BinCounts[i] + corr.BinSDS[i] > largest)
                        {
                            largest = corr.BinCounts[i] + corr.BinSDS[i];
                        }
                    }
                }

                List<PointF> points = new List<PointF>();

                List<PointF> sd1points = new List<PointF>();
                List<PointF> sd2points = new List<PointF>();

                for (int i = 0; i < corr.NumberOfBins; i++)
                {
                    //    int height = (int)((float)corr.BinCounts[i] / largest * drawableHeight);

                    int height = (int)((corr.BinCounts[i] - minYs[corrnum]) / (maxYs[corrnum] - minYs[corrnum]) * drawableHeight);

                    if (this.correlations.Length == 1)
                    {
                        Brush brush = null;
                        if (corr == this.selectedCorrelation)
                        {
                            brush = Brushes.Black;
                        }
                        else brush = Brushes.Gray;

                        e.Graphics.FillRectangle(brush, leftPadding + (i * widthPerBin), toPaint.Height - bottomPadding - height, widthPerBin, height);
                    }
                    else
                    {
                        points.Add(new PointF(leftPadding + (i * widthPerBin), toPaint.Height - bottomPadding - height));
                        points.Add(new PointF(leftPadding + ((i + 1) * widthPerBin), toPaint.Height - bottomPadding - height));


                        //e.Graphics.DrawLine(pen, leftPadding + (i * widthPerBin), toPaint.Height - bottomPadding - height, 

                        // e.Graphics.DrawRectangle(pen, leftPadding + (i * widthPerBin), toPaint.Height - bottomPadding - height, widthPerBin, height);
                    }


                    if (this.DisplaySDs && corr.BinSDS != null && corr == this.selectedCorrelation)
                    {
                        //int sdHeight1 = (int)((float)(corr.BinCounts[i] + corr.BinSDS[i]) / largest * drawableHeight);
                        //int sdHeight2 = (int)((float)(corr.BinCounts[i] - corr.BinSDS[i]) / largest * drawableHeight);

                        int sdHeight1 = (int)((corr.BinCounts[i] + corr.BinSDS[i] - minYs[corrnum]) / (maxYs[corrnum] - minYs[corrnum]) * drawableHeight);
                        int sdHeight2 = (int)((corr.BinCounts[i] - corr.BinSDS[i] - minYs[corrnum]) / (maxYs[corrnum] - minYs[corrnum]) * drawableHeight);

                        sd1points.Add(new PointF(leftPadding + (i * widthPerBin), toPaint.Height - bottomPadding - sdHeight1));
                        sd1points.Add(new PointF(leftPadding + ((i + 1) * widthPerBin), toPaint.Height - bottomPadding - sdHeight1));

                        sd2points.Add(new PointF(leftPadding + (i * widthPerBin), toPaint.Height - bottomPadding - sdHeight2));
                        sd2points.Add(new PointF(leftPadding + ((i + 1) * widthPerBin), toPaint.Height - bottomPadding - sdHeight2));

                    }
                }

                if (this.correlations.Length != 1)
                {
                    Pen pen = null;
                    if (corr == this.selectedCorrelation)
                    {
                        pen = Pens.Black;
                    }
                    else pen = Pens.Gray;
                    e.Graphics.DrawLines(pen, points.ToArray());

                    if (this.DisplaySDs && corr.BinSDS != null && corr == this.selectedCorrelation)
                    {
                        e.Graphics.DrawLines(Pens.Red, sd1points.ToArray());
                        e.Graphics.DrawLines(Pens.Red, sd2points.ToArray());

                    }
                }
            }

            float perc = -(float)this.selectedCorrelation.Offset / this.selectedCorrelation.TotalTime;
            if (perc >= 0)
            {
                PointF[] points = new PointF[]{new PointF(perc*(toPaint.Width - rightPadding - leftPadding) + leftPadding, toPaint.Height - bottomPadding),
                    new PointF(perc*(toPaint.Width - rightPadding - leftPadding) + leftPadding + 8, toPaint.Height - bottomPadding + 8),
                    new PointF(perc*(toPaint.Width - rightPadding - leftPadding) + leftPadding - 8, toPaint.Height - bottomPadding +8)};
                e.Graphics.FillPolygon(Brushes.Black, points);
            }

            e.Graphics.DrawLine(Pens.Black, leftPadding, topPadding, leftPadding, toPaint.Height - bottomPadding);
            e.Graphics.DrawLine(Pens.Black, leftPadding, toPaint.Height - bottomPadding, toPaint.Width - rightPadding, toPaint.Height - bottomPadding);

            float stringPosY = topPadding;
            float sum = 0;
            for (int i = 0; i < correlations.Length; i++)
            {
                e.Graphics.DrawString(Math.Round(maxYs[i], 2).ToString(), font, Brushes.Black, leftPadding - 65, stringPosY);
                stringPosY += e.Graphics.MeasureString(Math.Round(maxYs[i], 2).ToString(), font).Height + 5;
                sum += e.Graphics.MeasureString(Math.Round(maxYs[i], 2).ToString(), font).Height + 5;
            }

            stringPosY = toPaint.Height - bottomPadding - sum;
            for (int i = 0; i < correlations.Length; i++)
            {
                e.Graphics.DrawString(Math.Round(minYs[i], 2).ToString(), font, Brushes.Black, leftPadding - 65, stringPosY);
                stringPosY += e.Graphics.MeasureString(Math.Round(minYs[i], 2).ToString(), font).Height + 5;
               // sum += e.Graphics.MeasureString(Math.Round(maxYs[i], 2), font).Height + 5;
            }
        }

        public void CalcHistograms()
        {
            

            if (this.correlationType == 0)
            {
                AutoSingleOrderHistogram hist = new AutoSingleOrderHistogram(this.binWidth, this.numbins,
                    this.var1, this.SDRange, this.Offset);
                hist.CalculateHistograms(this.start * 1000, this.end * 1000);
                this.correlations = new CorrelationBase[] { hist };
                this.selectedCorrelation = hist;
            }
            else if (this.correlationType == 1)
            {
                MultiOrderCorrelation cor = new MultiOrderCorrelation(this.binWidth, this.numbins,
                    this.var1 as SpikeVariable, this.var1 as SpikeVariable, this.SDRange, this.Offset);
                cor.CalculateHistograms(this.start * 1000, this.end * 1000);
                this.correlations = new CorrelationBase[] { cor };
                this.selectedCorrelation = cor;

            }
            else if (this.correlationType == 2)
            {
                MultiOrderCorrelation cor = new MultiOrderCorrelation(this.binWidth, this.numbins,
                    this.var1, this.var2, this.SDRange, this.Offset);
                cor.CalculateHistograms(this.start * 1000, this.end * 1000);
                this.correlations = new CorrelationBase[] { cor };
                this.selectedCorrelation = cor;
            }
            else if (this.correlationType == 3)
            {
                List<CorrelationBase> corrs = new List<CorrelationBase>();
                CorrelationBase hist = null;
                if (var2.SpikeType == SpikeVariableHeader.SpikeType.Integrated || var2.SpikeType == SpikeVariableHeader.SpikeType.Raw)
                {
                    hist = new CycleTriggeredAverage(this.binWidth, this.numbins, this.var1, this.var2, this.SDRange, this.Offset, this.Normalize);
                }
                else
                {
                    hist = new NormalizedHistogram(this.binWidth, this.numbins, this.var1, this.var2, this.SDRange, this.Offset, false);
                }

                //NormalizedHistogram hist = new NormalizedHistogram(this.binWidth, this.numbins, this.var1, this.var2, this.SDRange, this.Offset, false);
                hist.CalculateHistograms(this.start * 1000, this.end * 1000);
                corrs.Add(hist);
                this.selectedCorrelation = hist;

                if (this.var3 != null)
                {
                    CorrelationBase hist2 = null;
                    if (var3.SpikeType == SpikeVariableHeader.SpikeType.Integrated || var3.SpikeType == SpikeVariableHeader.SpikeType.Raw)
                    {
                        hist2 = new CycleTriggeredAverage(this.binWidth, this.numbins, this.var1, this.var3, this.SDRange, this.Offset, this.normal);
                    }
                    else
                    {
                        hist2 = new NormalizedHistogram(this.binWidth, this.numbins, this.var1, this.var3, this.SDRange, this.Offset, true);
                    }

                    //NormalizedHistogram hist2 = new NormalizedHistogram(this.binWidth, this.numbins, this.var1, this.var3, this.SDRange, this.Offset, true);
                    hist2.CalculateHistograms(this.start * 1000, this.end * 1000);
                    corrs.Add(hist2);
                    this.selectedCorrelation = hist2;
                }

                this.correlations = corrs.ToArray();
            }
            //else if (this.correlationType == 4)
            //{
            //    List<CorrelationBase> corrs = new List<CorrelationBase>();
            //    CycleTriggeredAverage hist = new CycleTriggeredAverage(this.binWidth, this.numbins, this.var1, this.var2, this.SDRange, this.Offset);
            //    hist.CalculateHistograms(this.start * 1000, this.end * 1000);
            //    corrs.Add(hist);
            //    this.selectedCorrelation = hist;

            //    if (this.var3 != null)
            //    {
            //        CycleTriggeredAverage hist2 = new CycleTriggeredAverage(this.binWidth, this.numbins, this.var1, this.var3, this.SDRange, this.Offset);
            //        hist2.CalculateHistograms(this.start * 1000, this.end * 1000);
            //        corrs.Add(hist2);
            //        this.selectedCorrelation = hist2;
            //    }

            //    this.correlations = corrs.ToArray();
            //}

            this.panel1.Invalidate();            
        }

        private void panel1_Resize(object sender, EventArgs e)
        {
            this.panel1.Invalidate();
        }


        private void savePictureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SVG svg = new SVG();
            Rectangle rect = new Rectangle(0, 0, 600, 400);
            this.MakeImage(svg, rect);
            SaveFileDialog diag = new SaveFileDialog();
            diag.AddExtension = true;
            diag.DefaultExt = ".svg";
            diag.Filter = "Scalable Vectors(*.svg)|*.svg|All files (*.*)|*.*";
            if (diag.ShowDialog() == DialogResult.Cancel)
                return;
            try
            {
                svg.Create(new object[] { diag.FileName });
            }
            catch
            {
                MessageBox.Show("There was an error trying to create the image. Please try again or give up.");
            }
        }

        private void saveToExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog diag = new SaveFileDialog();
            diag.Filter = "CSV(*.csv)|*.csv|All Files(*.*)|*.*";
            diag.AddExtension = true;
            diag.DefaultExt = ".csv";
            if (diag.ShowDialog() == DialogResult.Cancel) return;
            object[][] toWrite = new object[this.selectedCorrelation.BinCounts.Length + 1][];
            for (int i = 0; i < toWrite.Length; i++)
            {
                toWrite[i] = new object[this.correlations.Length + 3];
            }

            for (int i = 0; i < this.correlations.Length; i++)
            {
                for (int j = 0; j < this.correlations[i].BinCounts.Length; j++)
                {
                    toWrite[j + 1][i] = this.correlations[i].BinCounts[j];

                    if (this.correlations[i].BinSDS != null)
                    {
                        toWrite[j + 1][i + this.correlations.Length + 1] = this.correlations[i].BinSDS[j];
                    }
                }
            }

            toWrite[0][0] = this.var1.Header.ExperimentName;
            toWrite[0][1] = this.labelDesc.Text;

            CSVWriter.WriteCSV(diag.FileName, toWrite);


        }
    }
}
