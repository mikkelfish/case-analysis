using System;
using System.Collections;//.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using CeresBase.Data;
using ApolloFinal.Tools.Histograms;
using System.Drawing.Drawing2D;

/*
 * IF DBMANAGER QUITS WORKING
 *  IO SON sun32.dll
 * CeresWorking CeresWorking dbTemplate (2 sql controls)
 * 
 * copies to
 * apoolofinalhosting bin debug
 */

//His scopecontrol class with scope master is excellent example of how to draw stuff

//http://www.geekpedia.com/tutorial50_Drawing-with-Csharp.html DRAWING TUTORIAL
//http://www.thescripts.com/forum/thread224581.html
namespace ApolloFinal.Tools.Histograms
{
    public partial class Histograms : Form
    {
        private int beginTime,    // First time to look at (start 1 after this time)
                    endTime,      // Last time to look at (end 1 before this time)
                    binSize,      // Size that each bin should be, in milliseconds
                    bins;         // How many bins there should be
        private bool changeTooLarge=false;
                                    // May delete this - tells the program that it counted a bin that had a change that was too large to store anywhere
                                    // Ask if this feature is wanted, probably not

        public Histograms()
        {
            InitializeComponent();
        }

        private void performAction_Click(object sender, EventArgs e)
        {
            try
            {
                // Convert times from input (String format) to an integer. Throws exception
                // if it is not a proper integer or if other circumstances exist.
                beginTime = Convert.ToInt32(UIBeginTime.Text);
                endTime = Convert.ToInt32(UIEndTime.Text);
                bins = Convert.ToInt32(UIBins.Text);
                binSize = Convert.ToInt32(UIBinSize.Text);

                if (beginTime < 0) { throw new System.ApplicationException("The beginning time must be a positive integer."); }
                else if (beginTime > endTime) { throw new System.ApplicationException("The end time must be greater than the beginning time."); }
                else if (beginTime > endTime) { throw new System.ApplicationException("The end time must be greater than the beginning time."); }
                else if (bins <= 0) { throw new System.ApplicationException("There must be a positive, non zero input for number of bins."); }
                else if (binSize <= 0) { throw new System.ApplicationException("Bin Size must be a positive, non zero input."); }
                //CHECK IF END TIME IS GREATER THAN THE POSSIBLY LENGTH OF TIME TOO SOMEHOW -> probably has to happen in the call

                
                
                /* Probably not the most efficient way to go.
                    This code selects the variable based off of if it's
                 * the same name as the one in the combo box.
                 * Dont know any other way to do it. */
                /*
                foreach (SpikeVariable var in VariableSource.Variables)
                {
                    if (var.FullDescription.ToString().Equals(comboVariables1.SelectedItem))
                    {
                        AutoCorrelation.CreateBins(beginTime, endTime, binSize, bins, var);
                        break;
                    }
                }
                textDump.Text = AutoCorrelation.RETURNDATA;*/
                DrawHistogram();
            }
            catch (System.ApplicationException f)
            {
                CeresBase.Global.ErrorBox(f.Message.ToString());
            }
            catch (System.FormatException)
            {
                CeresBase.Global.ErrorBox("One of the input boxes does not contain a proper integer.\n\nPossible invalid input includes decimal places and non numeric characters.");
            }
            catch (Exception f)
            {
                CeresBase.Global.ErrorBox(f);
            }
            finally { }
        }



        
        //Problem with this method is it goes away when screen is overridden
        private void DrawHistogram()
        {
            Graphics g = CreateGraphics();
            Pen p = new Pen(Color.Blue, 1);

            int[] tempArray = new int[6] { 36,12,15,29,31,2 };
            bins = tempArray.Length; //TEMP!!!
            
            Color color = Color.Blue;
            const int LEFT = 16;      //Position first line should be at
            int BOTTOM = TEMPHEIGHT.Height - 16; // Need to somehow make sure the bottom is slightly above the bottom of form

            
            
            int width = 10;  //For now, later it must resize with window
            int x1 = LEFT,
                x2 = x1 + width,
                y1 = BOTTOM,
                y2 = BOTTOM - tempArray[0];//AutoCorrelation.binArray[0];   //Keep track of current positions

            Rectangle rectBorder = new Rectangle(16, 215, 200, 150);
            Rectangle rectFill = new Rectangle(17, 216, 198, 148);
            SolidBrush fill = new SolidBrush(Color.White);
            Pen border = new Pen(Color.Black, 2);
            g.DrawRectangle(border, rectBorder);       //change numbers slightly
            g.FillRectangle(fill, rectFill);
            //maybe should start binarray at element 1 instead because 0*2 will not work out right.
            for (int i = 0; i < tempArray.Length/*AutoCorrelation.binArray.Length*/; i++)
            {
                y2 = BOTTOM - tempArray[i];
                g.DrawLine(p, x1, y1, x1, y2);    // Vertical Line
                g.DrawLine(p, x1, y2, x2, y2);    // Horizontal Line
                x1 = x2; 
                x2 = x2 + width;
                y1 = y2;
            }
            g.DrawLine(p, x1, y1, x1, BOTTOM);    // Last Vertical Line


            
            /*
             * STEPS IN FOR LOOP FOR ARRAY
             * 
             * 1. Find height, draw a line there
             * 2. Find height of next item, draw line up or down to that point
             * 
             * 
             */
        }


















    }
}
