using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CeresBase.Data;

namespace ApolloFinal.Tools
{
    public partial class MakeSpikeSortable : Form
    {
        public MakeSpikeSortable()
        {
            InitializeComponent();
            foreach (Variable var in VariableSource.Variables)
            {
                this.listBoxLoadedVars.Items.Add(var);
            }

            worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null)
            {
                MessageBox.Show("Done");

            }
            else MessageBox.Show("Error: " + e.Error);
            this.Close();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            List<Variable> varsToClean = new List<Variable>();
            foreach (Variable var in this.listBoxToClean.Items)
            {
                varsToClean.Add(var);
            }

            if (varsToClean.Count != 0)
            {
                Variable[] cleanedVariables = CleanDataTool.CleanData(varsToClean.ToArray());
                IO.DataMax.DataMaxWriter writer = new ApolloFinal.IO.DataMax.DataMaxWriter();
                writer.WriteVariables(cleanedVariables, this.textBox1.Text);
            }
            List<Variable> varsToKeep = new List<Variable>();
            foreach (Variable var in this.listBoxToLeave.Items)
            {
                varsToKeep.Add(var);
            }
            if (varsToKeep.Count != 0)
            {
                IO.DataMax.DataMaxWriter writer = new ApolloFinal.IO.DataMax.DataMaxWriter();
                writer.WriteVariables(varsToKeep.ToArray(), this.textBox1.Text);
            }
        }
        BackgroundWorker worker;


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonConvert_Click(object sender, EventArgs e)
        {
            this.worker.RunWorkerAsync();


        }

        private void listBoxLoadedVars_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.listBoxLoadedVars.DoDragDrop(this.listBoxLoadedVars.SelectedItems, DragDropEffects.Copy);
            }
        }

        private void buttonRemoveClean_Click(object sender, EventArgs e)
        {
            if (this.listBoxToClean.SelectedItems == null)
            {
                return;
            }

            List<object> objsToRemove = new List<object>();
            foreach (object obj in this.listBoxToClean.SelectedItems)
            {
                objsToRemove.Add(obj);
            }

            foreach (object obj in objsToRemove)
            {
                this.listBoxToClean.Items.Remove(obj);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.listBoxToLeave.SelectedItems == null)
            {
                return;
            }

            List<object> objsToRemove = new List<object>();
            foreach (object obj in this.listBoxToLeave.SelectedItems)
            {
                objsToRemove.Add(obj);
            }

            foreach (object obj in objsToRemove)
            {
                this.listBoxToLeave.Items.Remove(obj);
            }
        }

        private void listBox_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListBox.SelectedObjectCollection)))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void listBox_DragDrop(object sender, DragEventArgs e)
        {
            ListBox.SelectedObjectCollection collection = (ListBox.SelectedObjectCollection)e.Data.GetData(typeof(ListBox.SelectedObjectCollection));
            if (collection == null) return;

            ListBox box = sender as ListBox;
            foreach (object obj in collection)
            {
                if (!box.Items.Contains(obj))
                {
                    box.Items.Add(obj);
                }
            }
        }

        private void buttonPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog test = new FolderBrowserDialog();
            if (test.ShowDialog() != DialogResult.Cancel)
            {
                this.textBox1.Text = test.SelectedPath;
            }
        }


    }
}