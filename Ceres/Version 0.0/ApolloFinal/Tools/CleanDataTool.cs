using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using MathWorks.MATLAB.NET.Arrays;
using System.IO;

namespace ApolloFinal.Tools
{
    static class CleanDataTool
    {
        public static Variable[] CleanData(Variable[] varsToClean)
        {
            

            //(varsToClean[0][0] as DataFrame).GetData(new object[] { beginningTime, endTime });

            if (varsToClean.Length == 0) return null;

            int bufferSize = 250000;
            bufferSize -= bufferSize % varsToClean.Length;
            bool cont = true;

            IDataPool[] pools = new IDataPool[varsToClean.Length + 1];
            float[] rawData = new float[varsToClean.Length * bufferSize];

            int minPoolSize = int.MaxValue;
            for (int i = 0; i < varsToClean.Length; i++)
            {
                if (varsToClean[i][0].Pool.DimensionLengths[0] < minPoolSize)
                {
                    minPoolSize = varsToClean[i][0].Pool.DimensionLengths[0];
                }
            }

            for (int i = 0; i < pools.Length; i++)
            {
                pools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { minPoolSize });
            }

            GersteinCleaner.MatlabGerstein gerstein = new GersteinCleaner.MatlabGerstein();
            int index = 0;
            while (cont)
            {
                int remaining = minPoolSize - index;
                if (remaining < bufferSize)
                {
                     bufferSize = remaining;
                }

  
                long buffSpot = 0;

                float min = float.MaxValue;
                float max = float.MinValue;
                for (int i = 0; i < varsToClean.Length; i++)
                {
                    (varsToClean[i][0] as DataFrame).Pool.GetData(new int[] { index }, new int[] { bufferSize },
                        delegate(float[] data, uint[] indexes, uint[] count)
                        {
                                Array.Copy(data, 0, rawData, buffSpot, (int)count[0]);
                                buffSpot += (int)count[0];
                                if (i == varsToClean.Length - 1) index += (int)count[0];

                                for (int j = 0; j < data.Length; j++)
                                {
                                    if (data[j] < min) min = data[j];
                                    if (data[j] > max)
                                    {
                                        if (data[j] > 300000)
                                        {
                                            int s = 0;
                                        }
                                        max = data[j];
                                    }
                                }

                            // if ((int)count[0] < bufferSize) cont = false;
                        }
                    );
                }

                if (index >= minPoolSize)
                {
                    cont = false;
                    float[] tempData = new float[buffSpot];
                    Array.Copy(rawData, tempData, buffSpot);
                    rawData = tempData;
                }

                int perChan = (int)(buffSpot / varsToClean.Length);
                //double dPerChan = bufferSize

                try
                {

                    MWNumericArray array = new MWNumericArray(varsToClean.Length, perChan, rawData, false, true);
                    MWNumericArray answer = (MWNumericArray)gerstein.CleanData(array);
                    Array ret = answer.ToVector(MWArrayComponent.Real);
                    array.Dispose();
                    answer.Dispose();

                    for (int i = 0; i < pools.Length; i++)
                    {
                        float[] storeData = new float[perChan];
                        Array.Copy(ret, i * perChan, storeData, 0, perChan);
                        using (MemoryStream stream = CeresBase.Data.DataUtilities.ArrayToMemoryStream(storeData))
                        {
                            pools[i].SetData(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    int y = 0;
                }


                GC.Collect();

            }

            List<Variable> vars = new List<Variable>();
            for (int i = 0; i < varsToClean.Length; i++)
            {
                SpikeVariableHeader old = varsToClean[i].Header as SpikeVariableHeader;
                SpikeVariableHeader header = new SpikeVariableHeader(old.VarName + " Cleaned", old.UnitsName,
                    old.ExperimentName, old.Frequency, old.SpikeDesc);
                SpikeVariable var = new SpikeVariable(header);
                DataFrame frame = new DataFrame(var, pools[i], CeresBase.General.Constants.Timeless);
                var.AddDataFrame(frame);
                vars.Add(var);
            }

            SpikeVariableHeader cleanHeader = new SpikeVariableHeader("Removed Noise", "Removed Noise", varsToClean[0].Header.ExperimentName,
                (varsToClean[0].Header as SpikeVariableHeader).Frequency, SpikeVariableHeader.SpikeType.Raw);
            SpikeVariable cleaned = new SpikeVariable(cleanHeader);
            DataFrame cleanFrame = new DataFrame(cleaned, pools[pools.Length - 1], CeresBase.General.Constants.Timeless);
            cleaned.AddDataFrame(cleanFrame);
            vars.Add(cleaned);

            VariableSource.AddVariables(vars.ToArray());
            return vars.ToArray();
        }
    }
}
