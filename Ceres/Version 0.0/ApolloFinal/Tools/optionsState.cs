using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools
{
    public class optionsState
    {
        private float binWidth;
        public float BinWidth
        {
            get { return binWidth; }
        }

        private int binCount;
        public int BinCount
        {
            get { return binCount; }
        }

        private double sd;
        public double SD
        {
            get { return sd; }
        }

        private double offset;
        public double Offset
        {
            get { return offset; }
        }

        private float start;
        public float Start
        {
            get { return start; }
        }

        private float end;
        public float End
        {
            get { return end; }
        }



        public optionsState(float binWidth, int binCount, double sd, double offset, float start, float end)
        {
            this.binCount = binCount;
            this.binWidth = binWidth;
            this.sd = sd;
            this.offset = offset;
            this.start = start;
            this.end = end;
        }

        public override bool Equals(object obj)
        {
            return this.GetHashCode().Equals(obj.GetHashCode());
        }

        public override int GetHashCode()
        {
            return (binWidth.ToString() + " " + binCount.ToString() + " " + sd.ToString() + " " + offset.ToString() + " " + start.ToString() + " " + end.ToString()).GetHashCode();
        }

        public override string ToString()
        {
            return "Bin Width: " + binWidth.ToString() + ", Bin Count: " + binCount + ", SD: " + sd + ", Offset: " + offset + ", Start: " + start + ", End: " + end;
        }
    }
}
