//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;
//using System.Windows.Forms;

//using System.Reflection.Emit;
//using System.Reflection;
//using System.CodeDom.Compiler;
//using Microsoft.CSharp;
//using AviFile;
//using CeresBase.IO.ImageCreation;

//namespace ApolloFinal.Tools
//{
//    public partial class PoincarePlot : Form
//    {
//        private float[] times;
//        public float[] Times
//        {
//            get
//            {
//                return this.times;
//            }
//            set
//            {
//                this.times = value;
//            }
//        }

//        private float[] rawTimes;
//        public float[] RawTimes
//        {
//            get { return rawTimes; }
//            set { rawTimes = value; }
//        }

//        private List<HotSpot> hotSpots;
//        public List<HotSpot> HotSpots
//        {
//            get { return hotSpots; }
//            set { hotSpots = value; }
//        }
	

//        private string description;
//        public string Description
//        {
//            get { return description; }
//            set { description = value; }
//        }
	

//        public PoincarePlot()
//        {
//            InitializeComponent();
//            this.numericUpDownSpeed.DataBindings.Add("Value", this.poincareControl1, "AnimateTime");
//            this.numericUpDown1.DataBindings.Add("Value", this.poincareControl1, "EveryNth");
//            Binding binding = this.textBoxCluster.DataBindings.Add("Text", this.poincareControl1, "ClusterPoints", true);
//            binding.BindingComplete += new BindingCompleteEventHandler(binding_BindingComplete);


//            Binding binding2 = this.textBoxSD.DataBindings.Add("Text", this.poincareControl1, "SDMult", true);
//            binding2.BindingComplete += new BindingCompleteEventHandler(this.binding_BindingComplete);

//            binding = this.textBoxXMax.DataBindings.Add("Text", this.poincareControl1, "MaxXRange", true);
//            binding2 = this.textBoxYMax.DataBindings.Add("Text", this.poincareControl1, "MaxYRange", true);
//            binding.BindingComplete += new BindingCompleteEventHandler(binding_BindingComplete);
//            binding2.BindingComplete += new BindingCompleteEventHandler(this.binding_BindingComplete);

//            this.checkBoxAutoRange.DataBindings.Add("Checked", this.poincareControl1, "AutoRange");
//            this.checkBox1.DataBindings.Add("Checked", this.poincareControl1, "Cluster");

//        }

//        void binding_BindingComplete(object sender, BindingCompleteEventArgs e)
//        {
//            if (e.BindingCompleteState != BindingCompleteState.Success)
//            {
//                MessageBox.Show(e.ErrorText);
//            }
//        }

            

//        private void buttonCreate_Click(object sender, EventArgs e)
//        {
//            //this.poincareControl1.MaxXRange = float.Parse(this.textBoxXMax.Text);
//            //this.poincareControl1.MaxYRange = float.Parse(this.textBoxYMax.Text);
//            //this.poincareControl1.AutoRange = this.checkBoxAutoRange.Checked;
//        //    this.poincareControl1.EveryNth = (int)this.numericUpDown1.Value;
//            this.poincareControl1.CreatePlot(this.times, this.rawTimes);
//        }

//        private void buttonPlay_Click(object sender, EventArgs e)
//        {
//            if (!this.poincareControl1.Animate)
//            {
//              //  this.poincareControl1.AnimateTime = (double)this.numericUpDownSpeed.Value;
//                //this.poincareControl1.Animate = true;
//            //    this.poincareControl1.ClusterPoints = int.Parse(this.textBoxCluster.Text);
//                this.poincareControl1.Start();
//            }
//            else this.poincareControl1.Stop();
//        }

//        private void buttonCluster_Click(object sender, EventArgs e)
//        {
//            this.poincareControl1.RunClustering();
//            this.listBox1.Items.Clear();
//            for (int i = 0; i < this.poincareControl1.Partitions.Length; i++)
//            {
//                this.listBox1.Items.Add(i);
//            }
//        }

//        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
//        {
//            List<int> selected = new List<int>();
//            foreach (int i in this.listBox1.SelectedItems)
//            {
//                selected.Add(i);
//            }
//            this.poincareControl1.ClustersToDraw = selected.ToArray();
//        }

//        private void PoincarePlot_Load(object sender, EventArgs e)
//        {

//        }

//        private void button1_Click(object sender, EventArgs e)
//        {
//            SaveFileDialog diag = new SaveFileDialog();
//            diag.AddExtension = true;
//            diag.DefaultExt = ".avi";
//            if (diag.ShowDialog() == DialogResult.Cancel) return;

//            this.poincareControl1.CreateMovie(diag.FileName, 800, 600, 4.0/(double)this.numericUpDownSpeed.Value);           
            
//            //manager.AddVideoStream(
//        }

//        private void buttonStatistics_Click(object sender, EventArgs e)
//        {
//            if (!this.checkBox1.Checked)
//            {
//                PoincareStats.PoincareStats stats = new ApolloFinal.Tools.PoincareStats.PoincareStats();
//                stats.Data = this.times;
//                stats.Show();

//            }
//        }

//        private void saveImageToolStripMenuItem_Click(object sender, EventArgs e)
//        {
//            SaveFileDialog diag = new SaveFileDialog();
//            diag.AddExtension = true;
//            diag.DefaultExt = ".svg";
//            diag.Filter = "SVG (*.svg)|*.svg|All Files (*.*)|*.*";
//            if (diag.ShowDialog() == DialogResult.Cancel) return;

//            this.poincareControl1.SaveImage(diag.FileName, 400, 400);
//        }
//    }
//}