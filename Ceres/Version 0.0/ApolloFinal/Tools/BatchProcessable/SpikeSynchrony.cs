//using System;
//using System.Collections.Generic;
//using System.Text;
//using CeresBase.UI;
//using System.Windows.Forms;
//using CeresBase.Projects.Flowable;

//namespace ApolloFinal.Tools.BatchProcessable
//{
//    class SpikeSynchrony : IBatchViewable
//    {
//        public override string ToString()
//        {
//            return "Spike Synchrony";
//        }

//        private SpikeVariable[] variables;
//        private double[][][] spikeBoundaries;
//        private float start = 0;
//        private float end = 0;
//        private int windowWidth;

//        #region IBatchViewable Members

//        public IBatchViewable[] View(IBatchViewable[] viewable)
//        {
//            return this.View(viewable, false);
//        }

//        public IBatchViewable[] View(IBatchViewable[] viewable, bool useManager)
//        {
//            List<IBatchViewable> toRet = new List<IBatchViewable>();
//            foreach (IBatchViewable view in viewable)
//            {
//                if (view is SpikeSynchrony)
//                {
//                    SynchronyViewer viewer = new SynchronyViewer();
//                    SpikeSynchrony synch = view as SpikeSynchrony;
//                    viewer.Spikes = synch.spikeBoundaries;
//                    viewer.Variables = synch.variables;
//                    viewer.StartMS = (int)(synch.start * 1000.0F);
//                    viewer.EndMS = (int)(synch.end * 1000.0F);
//                    viewer.WindowWidth = synch.windowWidth;
//                    viewer.Show();
//                }
//                else toRet.Add(view);
//            }


//            return toRet.ToArray();
//        }

//        #endregion
//    }
//}
