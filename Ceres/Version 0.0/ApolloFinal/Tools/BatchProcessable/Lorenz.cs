//using System;
//using System.Collections.Generic;
//using System.Text;
//using ApolloFinal.Tools.BatchProcessing;
//using CeresBase.UI;
//using CeresBase.Data;
//using MathWorks.MATLAB.NET.Arrays;

//namespace ApolloFinal.Tools.BatchProcessable
//{
//    class Lorenz : IBatchProcessable
//    {

//        public override string ToString()
//        {
//            return "Lorenz";
//        }
//        public bool SuppportsMultiple
//        {
//            get { return false; }
//        }

//        public object[] GetArguments(SpikeVariable[] varsToUse, out string[] names)
//        {
//            RequestInformation req = new RequestInformation();
//            req.Grid.SetInformationToCollect(
//                new Type[] { typeof(double), typeof(double), typeof(double), typeof(string), typeof(int), typeof(float), typeof(float), typeof(bool) },
//                new string[] { "Parameters", "Parameters", "Parameters", "Parameters", "Length", "Length", "Noise", "Noise" },
//                new string[] { "Sigma", "Rho", "Beta", "Output (x,y,z)", "Steps", "Time", "Noise", "Add to Creation" },
//                new string[] { "Prandtl number", "Rayleigh number", "Beta", "The variable to output.", "The number of steps to calculate.", "Time (sec) of output variable.", "Noise Amount", "Add to Creation" },
//                null,
//                new object[] { 10, 28, 8D / 3D, "x", 25000, 100, .01, false });

//            if (req.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
//            {
//                names = null;
//                return null;
//            }
//            double sigma = (double)req.Grid.Results["Sigma"];
//            double rho = (double)req.Grid.Results["Rho"];
//            double beta = (double)req.Grid.Results["Beta"];
//            string output = (string)req.Grid.Results["Output (x,y,z)"];
//            int steps = (int)req.Grid.Results["Steps"];
//            float length = (float)req.Grid.Results["Time"];
//            float noise = (float)req.Grid.Results["Noise"];
//            bool addCreation = (bool)req.Grid.Results["Add to Creation"];


//            names = new string[1];
//            names[0] = "Lorenz out- " + output + length.ToString() + " " + sigma.ToString() + " " + rho.ToString() + " " + beta.ToString() + " " + noise.ToString();
//            return new object[] { sigma, rho, beta, output, steps, length, noise, addCreation };


//        }

//        private delegate double rungeFunction(double[] vals, double sigma, double rho, double beta);

//        //Lorenz
//        //dx/dt=sigma*(y-x)
//        //dy/dt=x(rho-z)-y
//        //dz/dt=xy-beta*z
//        private double xPrime(double[] vals, double sigma, double rho, double beta)
//        {
//            return sigma * (vals[1] - vals[0]);
//        }

//        private double yPrime(double[] vals, double sigma, double rho, double beta)
//        {
//            return vals[0] * (rho - vals[2]) - vals[1];
//        }

//        private double zPrime(double[] vals, double sigma, double rho, double beta)
//        {
//            return vals[0] * vals[1] - beta*vals[2];
//        }



//        //Runge-Kutta = yn+1 = yn + h/6(k1+2k2+2k3+k4)
//        //k1 = f(tn,yn)
//        //k2 = f(tn + h/2, yn+h/2*k1)
//        //k3 = f(tn+h/2, yn+h/2*k2)
//        //k4 = f(tn+h, yn+h*k3)
//        private double rungeStep(int which, double step, double[] vals, double sigma, double rho, double beta)
//        {
//            rungeFunction func = null;
//            double[] toPass = new double[3];
//            vals.CopyTo(toPass, 0);
//            switch(which)
//            {
//                case 0:
//                    func=new rungeFunction(this.xPrime);
//                    break;
//                case 1:
//                    func = new rungeFunction(this.yPrime);
//                    break;
//                case 2:
//                    func = new rungeFunction(this.zPrime);
//                    break;
//                default:
//                    throw new Exception("Wrong which");
//            }

//            double k1 = func(toPass, sigma, rho, beta);
//            toPass[which] = vals[which] + step * k1 / 2.0;
//            double k2 = func(toPass, sigma, rho, beta);
//            toPass[which] = vals[which] + step * k2 / 2.0;
//            double k3 = func(toPass, sigma, rho, beta);
//            toPass[which] = vals[which] + step * k3;
//            double k4 = func(toPass, sigma, rho, beta);
//            return vals[which] + step * (k1 + k2 + k3 + k4) / 6.0; 

//        }

//        public object[] Process(SpikeVariable[] varsToUse, object[] args)
//        {
//            double sigma = (double)args[0];
//            double rho = (double)args[1];
//            double beta = (double)args[2];
//            string output = (string)args[3];
//            int steps = (int)args[4];
//            float length = (float)args[5];
//            float noise = (float)args[6];
//            bool addToCreation = (bool)args[7];

//            CeresBase.MathConstructs.RealRandom rand = new CeresBase.MathConstructs.RealRandom();

//            IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { steps });

//            int beginning = 5000;
//            double h = .01;


//            double[] ranges = new double[]{
//                18.044532573926379 + 17.817482620069875, 
//                24.263206191958872 + 23.838149344143897,
//                44.776427787021376 - 6.1647753745487863};

//            double[] vals = new double[] { 1, 1, 1 };
//            for (int i = 0; i < beginning; i++)
//            {
//                for (int j = 0; j < vals.Length; j++)
//                {
//                    vals[j] = this.rungeStep(j, h, vals, sigma, rho, beta);
//                }
//            }

//            int toSave = 0;
//            if (output == "y") toSave = 1;
//            if (output == "z") toSave = 2;

//            float[] buffer = new float[256000];
//            int bufferSpot = 0;
//            int tot = 0;
//            for (int i = 0; i < steps; i++)
//            {
//                for (int j = 0; j < vals.Length; j++)
//                {
//                    vals[j] = this.rungeStep(j, h, vals, sigma, rho, beta);
//                    if (addToCreation) vals[j] += (float)((rand.NextDouble() - .5) * ranges[j] * noise);

               
//                    if (j == toSave)
//                    {
//                        buffer[bufferSpot] = (float)vals[j];
//                        if(!addToCreation) buffer[bufferSpot] += (float)((rand.NextDouble() - .5) * ranges[j] * noise);


//                        bufferSpot++;
//                        if (bufferSpot == buffer.Length)
//                        {
//                            pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(buffer));
//                            tot += bufferSpot;                         
//                            bufferSpot = 0;
//                        }
//                    }
//                }
//            }

//            List<float> testing = new List<float>();
//            if (bufferSpot != 0)
//            {
//                float[] tempBuff = new float[bufferSpot];
//                Array.Copy(buffer, 0, tempBuff, 0, bufferSpot);
//                testing.AddRange(tempBuff);
//                pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(tempBuff));
//                tot += bufferSpot;

//            }

//            //MatlabNLSuite.MatlabNonLinearSuite suite = new MatlabNLSuite.MatlabNonLinearSuite();
//            //MWArray[] result = (MWArray[])suite.MutInf(4, (MWNumericArray)testing.ToArray(),(MWNumericArray)40, (MWNumericArray)100);
//            //Array[] tawe = new Array[4];
//            //for (int i = 0; i < 4; i++)
//            //{
//            //    tawe[i] = (result[i] as MWNumericArray).ToArray(MWArrayComponent.Real);
//            //}
//            //            Array eep = result.ToArray(MWArrayComponent.Real);

//            double freq = (float)steps/length;
//            SpikeVariableHeader header = new SpikeVariableHeader("Lorenz out- " + output + length.ToString() + " " + sigma.ToString() + " " + rho.ToString() + " " + beta.ToString() + " " + noise.ToString(),
//                "None", "Lorenzes", freq, SpikeVariableHeader.SpikeType.Raw);
//            SpikeVariable var = new SpikeVariable(header);
//            DataFrame frame = new DataFrame(var, pool, new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless);
//            var.AddDataFrame(frame);

//            return new object[] { var };

//        }

//        public Type OutputType
//        {
//            get { return typeof(SpikeVariable); }
//        }

//        #region ICloneable Members

//        public object Clone()
//        {
//            return new Lorenz();
//        }

//        #endregion
//    }
//}
