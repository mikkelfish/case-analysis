using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.UI;
using CeresBase.Data;
using System.Collections;
using ApolloFinal.UI;
using System.Windows.Forms;
using System.Linq;
using CeresBase.Projects;
using CeresBase.Projects.Flowable;
using System.Windows.Controls.Primitives;
using CeresBase.FlowControl.Adapters;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Interval")]
    [AdapterDescription("Raw")]
    class AutoCorrelation :  IBatchFlowable
    {
       

        public override string ToString()
        {
            return "AutoCorrelation";
        }


        //public static MatlabPlot MakePlot(float[] data, int lags, bool subMean)
        //{
        //    IList[] calculated = runAutoCorr(data, lags, subMean);

        //    UI.MatlabPlot plot = new ApolloFinal.UI.MatlabPlot();
        //    plot.XData = calculated[1];
        //    plot.YData = calculated[0];
        //    plot.Label = "AutoCorrelation";

        //    return plot;
        //}

        private static double[][] runAutoCorr(float[] data, int numIter, bool subMean)
        {

            double[] vals = CrossCorrelation.AutoCorrelation(data, numIter, subMean);


            double[] finLags = new double[vals.Length];
            for (int i = 0; i < finLags.Length; i++)
            {
                finLags[i] = i;
            }

            return new double[][] { vals, finLags };
        }


        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            return
                new BatchProcessableParameter[] { 
                    new BatchProcessableParameter() 
                    { 
                        Name = "Lags", 
                        Val = 100,
                        Validator=new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                        {
                            CompareTo=0,
                            Operator = MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                            TreatNonComparableAsSuccess=true
                        }
                    },
                    new BatchProcessableParameter()
                    {
                        Name="SubtractMean", 
                        Val=true
                    },
                    new BatchProcessableParameter(){Name="Label", Val="Enter Here"},
                    new BatchProcessableParameter()
                    {
                        Name="Data",
                        TargetType = typeof(float[]),
                        Editable=false, 
                        CanLinkFrom = false, 
                        Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator()}
                };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            BatchProcessableParameter lagsP = parameters.Single(p => p.Name == "Lags");
            BatchProcessableParameter subMeanP = parameters.Single(p => p.Name == "SubtractMean");
            BatchProcessableParameter dataP = parameters.Single(p => p.Name == "Data");
            BatchProcessableParameter labelP = parameters.Single(p => p.Name == "Label");

            

            float[] data = dataP.Val as float[];

            int lags = (int)lagsP.Val;
            bool subMean = (bool)subMeanP.Val;

            double[][] calculated = runAutoCorr(data, lags, subMean);

            string label = labelP.Val as string;


            GraphableOutput output = new GraphableOutput() { 
                SourceDescription="AutoCorrelation",
                Label = label, 
                XData = calculated[1],
                XLabel = "Lag",
                YLabel = "AutoCorrelation Function", 
                YData = new double[][]{calculated[0]} };
            return new object[]{output};
        }       

        public string[] OutputDescription
        {
            get { return new string[] { "Graph of Autocorr Function" }; }
        }

        #endregion
    }
}
