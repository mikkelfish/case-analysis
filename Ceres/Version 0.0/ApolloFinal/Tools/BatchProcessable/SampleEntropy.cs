using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.UI;
using MathWorks.MATLAB.NET.Arrays;
using NSort;
using System.Collections;
using ApolloFinal.Tools.AttractorReconstruction;
using System.Windows.Forms;
using ApolloFinal.CUDA;
using CeresBase.Projects.Flowable;
using System.Linq;
using CeresBase.FlowControl.Adapters;
using MesosoftCommon.Utilities.Validations;
using CeresBase.FlowControl.Adapters.Matlab;
using CeresBase.FlowControl.Adapters.Permutations;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Interval")]
    [AdapterDescription("Raw")]
    class SampleEntropy : IBatchFlowable
    {
        public override string ToString()
        {
            return "Sample Entropy";
        }



        private class orderedSortStuff : ISwap<orderedVal>, IComparer<orderedVal>
        {

            #region IComparer Members

            public int Compare(orderedVal x, orderedVal y)
            {
                float val1 = x.val;
                float val2 = y.val;
                if (val1 == val2) return 0;
                if (val1 < val2) return -1;
                return 1;
            }

            #endregion

            #region ISwap<orderedVal> Members

            public void Set(IList<orderedVal> array, int left, orderedVal obj)
            {
                array[left] = obj;
                array[left].index = left;

                if (left == 14)
                {
                    int s = 0;
                }
            }

            public void Set(IList<orderedVal> array, int left, int right)
            {
                array[left] = array[right];
                array[left].index = left;

                if (left == 14)
                {
                    int s = 0;
                }
            }

            public void Swap(IList<orderedVal> array, int left, int right)
            {
                orderedVal swap = array[left];
                array[left] = array[right];
                array[right] = swap;

                array[right].index = right;
                array[left].index = left;

                if (right == 14 || left == 14)
                {
                    int s = 0;
                }
            }

            #endregion
        }

        private class orderedVal
        {
            public float val;
            public int id;
            public int index;

            private int numBins;
            private int dim;

            public orderedVal(int dim, float val, int id, int numBins)
            {
                this.dim = dim;
                this.numBins = numBins;

                this.val = val;
                this.id = id;
                this.A = new int[dim];
                this.B = new int[dim];

                this.F1 = new int[dim];
                this.F = new int[dim];

                this.matchCount = new int[numBins, dim];

                this.R1 = new int[dim * 2];

                this.index = -1;
                this.matched = new bool[dim * 2];
            }


            public void Reset()
            {
                this.A = new int[dim];
                this.B = new int[dim];

                this.F1 = new int[dim];
                this.F = new int[dim];

                this.matchCount = new int[numBins, dim];

                this.R1 = new int[dim * 2];

                this.index = -1;

                this.matched = new bool[dim * 2];
            }

            public int[] A;
            public int[] B;



            public int[] F1;
            public int[] F;

            public int[] R1;

            public int[,] matchCount;


            public bool[] matched;
        }


        /* sampen2 calculates an estimate of sample entropy and the variance of the
   estimate. */
        object[] sampen2(float[] y, int mm, double r)
        {
            int n = y.Length;
            //mm++;
            int MM = 2 * mm;

            double[] p = new double[mm];
            double[] v1 = new double[mm];
            double[] v2 = new double[mm];
            double[] s1 = new double[mm];
            double dv;
            int[] R1 = new int[n*MM];
            int[] R2 = new int[n*MM];
            int[] F2 = new int[n*mm];
            int[] F1 = new int[n*mm];
            int[] F = new int[n*MM];
            int FF;
            int[] run = new int[n];
            int[] run1 = new int[n];
            double[] A = new double[mm];
            double[] B = new double[mm];
            double[] K = new double[(mm+1)*mm];
            double[] n1 = new double[mm];
            double[] n2 = new double[mm];

            int m, m1, i, j, nj, jj, d, d2, i1, i2, dd;
            int nm1, nm2, nm3, nm4;
            double y1;

            

            //if ((run = (int *) calloc(n, sizeof(int))) == NULL)
            //exit(1);
            //if ((run1 = (int *) calloc(n, sizeof(int))) == NULL)
            //exit(1);
            //if ((R1 = (int *) calloc(n * MM, sizeof(int))) == NULL)
            //exit(1);
            //if ((R2 = (int *) calloc(n * MM, sizeof(int))) == NULL)
            //exit(1);
            //if ((F = (int *) calloc(n * MM, sizeof(int))) == NULL)
            //exit(1);
            //if ((F1 = (int *) calloc(n * mm, sizeof(int))) == NULL)
            //exit(1);
            //if ((F2 = (int *) calloc(n * mm, sizeof(int))) == NULL)
            //exit(1);
            //if ((K = (double *) calloc((mm + 1) * mm, sizeof(double))) == NULL)
            //exit(1);
            //if ((A = (double *) calloc(mm, sizeof(double))) == NULL)
            //exit(1);
            //if ((B = (double *) calloc(mm, sizeof(double))) == NULL)
            //exit(1);
            //if ((p = (double *) calloc(mm, sizeof(double))) == NULL)
            //exit(1);
            //if ((v1 = (double *) calloc(mm, sizeof(double))) == NULL)
            //exit(1);
            //if ((v2 = (double *) calloc(mm, sizeof(double))) == NULL)
            //exit(1);
            //if ((s1 = (double *) calloc(mm, sizeof(double))) == NULL)
            //exit(1);
            //if ((n1 = (double *) calloc(mm, sizeof(double))) == NULL)
            //exit(1);
            //if ((n2 = (double *) calloc(mm, sizeof(double))) == NULL)
            //exit(1);
            for (i = 0; i < n - 1; i++) 
            {
                nj = n - i - 1;
                y1 = y[i];
                for (jj = 0; jj < nj; jj++) 
                {
                    j = jj + i + 1;


                    if (((y[j] - y1) < r) && ((y1 - y[j]) < r)) 
                    {
                        run[jj] = run1[jj] + 1;
                        m1 = (mm < run[jj]) ? mm : run[jj];
                        for (m = 0; m < m1; m++) 
                        {
                            A[m]++;
                            if (j < n - 1) B[m]++;
                            F1[i + m * n]++;
                            F[i + n * m]++;
                            F[j + n * m]++;
                        }
                    }
                    else
                    run[jj] = 0;
                }			/* for jj */

                for (j = 0; j < MM; j++) 
                {
                    run1[j] = run[j];
                    R1[i + n * j] = run[j];
                }
               
                if (nj > MM - 1)
                {
                    for (j = MM; j < nj; j++) run1[j] = run[j];
                }
            }				/* for i */

            for (i = 1; i < MM; i++)
                for (j = 0; j < i - 1; j++)
                    R2[i + n * j] = R1[i - j - 1 + n * j]; //just move back by j
            for (i = MM; i < n; i++)
                for (j = 0; j < MM; j++)
                    R2[i + n * j] = R1[i - j - 1 + n * j];
            for (i = 0; i < n; i++)
            {
                for (m = 0; m < mm; m++) 
                {
                    FF = F[i + n * m];
                    F2[i + n * m] = FF - F1[i + n * m];
                    K[(mm + 1) * m] += FF * (FF - 1);
                }
            }

            for (m = mm - 1; m > 0; m--) B[m] = B[m - 1];
            
            B[0] = (double) n *(n - 1) / 2;
            for (m = 0; m < mm; m++) 
            {
                p[m] = (double) A[m] / B[m];
                v2[m] = p[m] * (1 - p[m]) / B[m];
            }


            double[] tempK = (double[])K.Clone();


            dd = 1;
            for (m = 0; m < mm; m++) 
            {
                d2 = m + 1 < mm - 1 ? m + 1 : mm - 1;
                for (d = 0; d < d2 + 1; d++) 
                {
                    for (i1 = d + 1; i1 < n; i1++) 
                    {
                        i2 = i1 - d - 1;
                        nm1 = F1[i1 + n * m];
                        nm3 = F1[i2 + n * m];
                        nm2 = F2[i1 + n * m];
                        nm4 = F2[i2 + n * m];
                        for (j = 0; j < (dd - 1); j++) {
                            if (R1[i1 + n * j] >= m + 1)
                            nm1--;
                            if (R2[i1 + n * j] >= m + 1)
                            nm4--;
                        }
                        for (j = 0; j < 2 * (d + 1); j++)
                            if (R2[i1 + n * j] >= m + 1)
                                nm2--;
                        for (j = 0; j < (2 * d + 1); j++)
                            if (R1[i2 + n * j] >= m + 1)
                                nm3--;
                        K[d + 1 + (mm + 1) * m] +=
                            (double) 2 *(nm1 + nm2) * (nm3 + nm4);
                    }
                }
            }

            n1[0] = (double) n *(n - 1) * (n - 2);
            for (m = 0; m < mm - 1; m++)
            for (j = 0; j < m + 2; j++)
                n1[m + 1] += K[j + (mm + 1) * m];
            for (m = 0; m < mm; m++) 
            {
                for (j = 0; j < m + 1; j++)
                {
                    n2[m] += K[j + (mm + 1) * m];
                }
            }

            for (m = 0; m < mm; m++) 
            {
                v1[m] = v2[m];
                dv = (n2[m] - n1[m] * p[m] * p[m]) / (B[m] * B[m]);
                if (dv > 0)
                    v1[m] += dv;
                s1[m] = (double) Math.Sqrt((double) (v1[m]));
            }


            return new object[] { p, s1, R1, R2, F, F1, v1, K, tempK, F2, A, B, n1, n2 };
        }

        private List<double[]>[] calcEntropy(float[] data, int window, int step, int m, int tau, float r, double freq, bool calcSD)
        {
            

            DateTime now = DateTime.Now;

            if (data.Length < window) return null;

            int numBins = window / step;

            orderedVal[] bigArray = new orderedVal[window];
            for (int i = 0; i < bigArray.Length; i++)
            {
                bigArray[i] = new orderedVal(m, data[i], i, numBins);
            }

            int counts = 0;
            for (int i = 1; i < data.Length; i++)
            {
                if (Math.Abs(data[i] - data[0]) < r) counts++;
            }

            orderedVal[] sorted = (orderedVal[])bigArray.Clone();
            orderedSortStuff stuff = new orderedSortStuff();
            QuickSorter<orderedVal> sort = new QuickSorter<orderedVal>(stuff, stuff);
            sort.Sort(sorted);

            for (int i = 0; i < bigArray.Length; i++)
            {
                if (bigArray[i].index == -1)
                {
                    bigArray[i].index = i;
                }
            }

            int firstSpot = bigArray.Length;
            bool first = true;

            List<double[]> As = new List<double[]>();
            List<double[]> Bs = new List<double[]>();
            List<double[]> Es = new List<double[]>();
            List<double[]> SDs = new List<double[]>();

            List<int> firstSpotA = new List<int>();

            
            int startBin = 0;

            while (firstSpot <= data.Length)
            {            
                int start = 0; //bigArray.Length - step; TODO CHANGE LATER
                if (first)
                {
                    start = 0;
                    
                }

                firstSpotA.Add(firstSpot-bigArray.Length);

                for (int i = start; i < bigArray.Length; i++)
                {
                    int totA = 0;
                    for (int j = 0; j < bigArray.Length; j++)
                    {
                        totA += bigArray[j].A[0];
                    }

                    float[] tempVals = new float[m];
                    for (int dim = 0; dim < m; dim++)
                    {
                        if (i + dim * tau >= bigArray.Length) break;
                        tempVals[dim] = bigArray[i + dim * tau].val;
                    }

                    //Go forwards
                    #region Forward
                    for (int j = bigArray[i].index + 1; j < bigArray.Length; j++)
                    {
                        int index = sorted[j].id-bigArray[0].id;

                        //if (index == bigArray.Length - 1) 
                        //    continue;

                        bool matchedone = false;

                        int origIndex = index;

                        if (Math.Abs(bigArray[i].val - bigArray[index].val) <= r)
                        {
                            if (i < origIndex)
                            {
                                int diff = origIndex - i - 1;
                                if (diff % tau == 0)
                                {
                                    int whichSpot = diff / tau;
                                    if (whichSpot < m * 2)
                                    {
                                        bigArray[i].matched[whichSpot] = true;
                                    }

                                }
                            }
                            else
                            {
                                int diff = i - origIndex - 1;
                                if (diff % tau == 0)
                                {
                                    int whichSpot = diff / tau;
                                    if (whichSpot < m * 2)
                                    {
                                        bigArray[origIndex].matched[whichSpot] = true;
                                    }

                                }

                            }
                        }

                        for (int dim = 0; dim < m; dim++, index += tau)
                        {
                            if (index >= bigArray.Length) break;

                            double val = Math.Abs(tempVals[dim] - bigArray[index].val);
                            if (val <= r)
                            {
                                matchedone = true;
                                if (i < origIndex)
                                {


                                    bigArray[i].A[dim]++;
                                    if (index < sorted.Length - 1)
                                    {
                                        bigArray[i].B[dim]++;
                                    }

                                    int whichBin = (i / step + startBin) % numBins;                                    
                                    bigArray[origIndex].matchCount[whichBin, dim]++;
                                }
                                else 
                                {
                                    if (i + dim < bigArray.Length)
                                    {
                                        bigArray[origIndex].A[dim]++;
                                        if (i + dim * tau < sorted.Length - 1)
                                        {
                                            bigArray[origIndex].B[dim]++;
                                        }

                                        int whichBin = (origIndex / step + startBin) % numBins;
                                        bigArray[i].matchCount[whichBin, dim]++;
                                    }
                                }
                            }
                            else break;
                        }

                        //This will only be triggered if no points were matched, and if that is the case we know to break the loop
                        if (!matchedone)
                            break;
                    }
                    #endregion

                    //Go backwards
                    #region Backward
                    if (!first)
                    {
                        for (int j = bigArray[i].index - 1; j >= 0; j--)
                        {

                            int index = sorted[j].id - bigArray[0].id;

                            bool matchedone = false;

                            int origIndex = index;

                            if (Math.Abs(bigArray[i].val - bigArray[index].val) <= r)
                            {
                                if (i < origIndex)
                                {
                                    int diff = origIndex - i - 1;
                                    if (diff % tau == 0)
                                    {
                                        int whichSpot = diff / tau;
                                        if (whichSpot < m * 2)
                                        {
                                            bigArray[i].matched[whichSpot] = true;
                                        }

                                    }
                                }
                                else
                                {
                                    int diff = i - origIndex - 1;
                                    if (diff % tau == 0)
                                    {
                                        int whichSpot = diff / tau;
                                        if (whichSpot < m * 2)
                                        {
                                            bigArray[origIndex].matched[whichSpot] = true;
                                        }

                                    }

                                }
                            }


                            for (int dim = 0; dim < m; dim++, index += tau)
                            {
                                if (index >= bigArray.Length) break;

                                double val = Math.Abs(tempVals[dim] - bigArray[index].val);
                                if (val <= r)
                                {
                                    matchedone = true;
                                    if (i < origIndex)
                                    {


                                        bigArray[i].A[dim]++;
                                        if (index < sorted.Length - 1)
                                        {
                                            bigArray[i].B[dim]++;
                                        }

                                        int whichBin = (i / step + startBin) % numBins;
                                        bigArray[origIndex].matchCount[whichBin, dim]++;
                                    }
                                    else
                                    {
                                        if (i + dim < bigArray.Length)
                                        {
                                            bigArray[origIndex].A[dim]++;
                                            if (i + dim * tau < sorted.Length - 1)
                                            {
                                                bigArray[origIndex].B[dim]++;
                                            }

                                            int whichBin = (origIndex / step + startBin) % numBins;
                                            bigArray[i].matchCount[whichBin, dim]++;
                                        }
                                    }
                                }
                                else break;
                            }

                            //This will only be triggered if no points were matched, and if that is the case we know to break the loop
                            if (!matchedone)
                                break;
                        }
                    }
                    #endregion
                       
                }               

                if (!first)
                {
                    for (int i = start; i < bigArray.Length; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            bigArray[i].A[j] /= 2;
                            bigArray[i].B[j] /= 2;
                        }
                    }
                }


                As.Add(new double[m]);
                Bs.Add(new double[m]);
                int N1 = bigArray.Length * (bigArray.Length - 1) / 2;

                double[] A = As[As.Count - 1];
                double[] B = Bs[Bs.Count - 1];
                for (int i = 0; i < bigArray.Length; i++)
                {
                    for (int j = 0; j < m; j++)
                    {
                        A[j] += sorted[i].A[j];
                        B[j] += sorted[i].B[j];
                    }
                }
                for (int i = B.Length - 2; i >= 0; i--)
                {
                    B[i + 1] = B[i];
                }
                B[0] = N1;


                Es.Add(new double[m]);
                double[] e = Es[Es.Count - 1];
                for (int i = 0; i < e.Length; i++)
                {
                    e[i] = (double)A[i] / (double)B[i];
                }

                double[] std = new double[m];


                #region Calculate uncertainty
                if (calcSD)
                {

                    int[] run = new int[m * 2];
                    for (int i = 0; i < bigArray.Length - 1; i++)
                    {
                        for (int j = 0; j < m * 2; j++)
                        {
                            if (bigArray[i].matched[j]) run[j]++;
                            else if(i + j + 1 < bigArray.Length) run[j] = 0;

                            bigArray[i].R1[j] = run[j];
                        }
                    }


                    ////Test to make sure R1 is the same
                    //for (int i = 0; i < bigArray.Length; i++)
                    //{
                    //    for (int j = 0; j < m * 2; j++)
                    //    {
                    //        if (bigArray[i].R1[j] != R1[i + j * bigArray.Length])
                    //        {

                    //            int s = 0;
                    //        }
                    //    }
                    //}

                    for (int i = 0; i < m; i++)
                    {
                        for (int j = i + 1; j < m; j++)
                        {
                            bigArray[i].F[j] = 0;
                            bigArray[i].F1[j] = 0;
                        }
                    }

                    for (int i = 0; i < bigArray.Length; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            int targetIndex = i + j * tau;
                            if (targetIndex >= bigArray.Length) break;

                            bigArray[targetIndex].F1[j] = bigArray[i].A[j];
                            bigArray[targetIndex].F[j] = bigArray[targetIndex].F1[j];
                            for (int bin = 0; bin < numBins; bin++)
                            {
                                bigArray[targetIndex].F[j] += bigArray[i].matchCount[bin, j];

                            }
                        }
                    }
                    ////TEST TO MAKE SURE F and F1 are the same
                    //for (int i = 0; i < bigArray.Length; i++)
                    //{
                    //    for (int j = 0; j < m; j++)
                    //    {
                    //        if (bigArray[i].F1[j] != F1[i + j * bigArray.Length] ||
                    //            bigArray[i].F[j] != F[i + j * bigArray.Length]
                    //            )
                    //        {
                    //            int s = 0;
                    //        }
                    //    }
                    //}


                    int[,] aR2 = new int[bigArray.Length, m * 2];
                    for (int i = 1; i < m * 2; i++)
                    {
                        for (int j = 0; j < i - 1; j++)
                        {
                            aR2[i, j] = bigArray[i - j - 1].R1[j];
                        }
                    }

                    for (int i = m * 2; i < bigArray.Length; i++)
                    {
                        for (int j = 0; j < m * 2; j++)
                        {
                            aR2[i, j] = bigArray[i - j - 1].R1[j];
                        }
                    }

                    ////TEST R2, it's the same
                    //for (int i = 0; i < bigArray.Length; i++)
                    //{
                    //    for (int j = 0; j < m * 2; j++)
                    //    {
                    //        if (aR2[i, j] != R2[i + j * bigArray.Length])
                    //        {
                    //            int s = 0;
                    //        }
                    //    }
                    //}

                    double[] K = new double[(m + 1) * m];
                    int[,] F2 = new int[bigArray.Length, m];
                    for (int i = 0; i < bigArray.Length; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            int FF = bigArray[i].F[j];
                            F2[i, j] = FF - bigArray[i].F1[j];
                            K[(m + 1) * j] += FF * (FF - 1); //K checks out at this stage
                        }
                    }

                    //for (int i = 0; i < K.Length; i++)
                    //{
                    //    if (K[i] != tempK[i])
                    //    {
                    //        int s = 0;
                    //    }
                    //}


                    ////Check F2 F2 checks out!!!
                    //for (int i = 0; i < bigArray.Length; i++)
                    //{
                    //    for (int j = 0; j < m; j++)
                    //    {
                    //        if (F2[i, j] != pF2[i + j * bigArray.Length])
                    //        {
                    //            int s = 0;
                    //        }
                    //    }
                    //}


                    double[] v1 = new double[m];
                    double[] v2 = new double[m];

                    for (int i = 0; i < m; i++)
                    {
                        double ratio = (double)A[i] / (double)B[i];
                        v2[i] = ratio * (1 - ratio) / B[i];
                    }


                    int dd = 1;
                    for (int i = 0; i < m; i++)
                    {
                        int d2 = i + 1 < m - 1 ? i + 1 : m - 1;
                        for (int d = 0; d < d2 + 1; d++)
                        {
                            for (int i1 = d + 1; i1 < bigArray.Length; i1++)
                            {
                                int i2 = i1 - d - 1;
                                int nm1 = bigArray[i1].F1[i];
                                int nm3 = bigArray[i2].F1[i];
                                int nm2 = F2[i1, i];
                                int nm4 = F2[i2, i];
                                for (int j = 0; j < dd - 1; j++)
                                {
                                    if (bigArray[i1].R1[j] >= i + 1) nm1--;
                                    if (aR2[i1, j] >= i + 1) nm4--;
                                }

                                for (int j = 0; j < 2 * (d + 1); j++)
                                {
                                    if (aR2[i1, j] >= i + 1)
                                        nm2--;
                                }

                                for (int j = 0; j < 2 * d + 1; j++)
                                {
                                    if (bigArray[i2].R1[j] >= i + 1)
                                        nm3--;
                                }

                                K[d + 1 + (m + 1) * i] += 2.0 * (nm1 + nm2) * (nm3 + nm4);
                            }
                        }
                    }

                    ////Check Ks check out!!
                    //for (int i = 0; i < K.Length; i++)
                    //{
                    //    if (K[i] != pK[i])
                    //    {
                    //        int s = 0;
                    //    }
                    //}


                    double[] n1 = new double[m];
                    double[] n2 = new double[m];

                    n1[0] = (double)bigArray.Length * (double)(bigArray.Length - 1) * (double)(bigArray.Length - 2);
                    for (int i = 0; i < m - 1; i++)
                    {
                        for (int j = 0; j < i + 2; j++)
                        {
                            n1[i + 1] += K[j + (m + 1) * i];
                        }
                    }

                    for (int i = 0; i < m; i++)
                    {
                        for (int j = 0; j < i + 1; j++)
                        {
                            n2[i] += K[j + (m + 1) * i];
                        }
                    }


                    for (int i = 0; i < m; i++)
                    {
                        v1[i] = v2[i];
                        double dv = (n2[i] - n1[i] * e[i] * e[i]) / (B[i] * B[i]);
                        if (dv > 0) v1[i] += dv;
                        std[i] = Math.Sqrt(v1[i]);
                    }

                    SDs.Add(std);

                    ////TEST SD
                    //for (int i = 0; i < std.Length; i++)
                    //{
                    //    if (std[i] != s1[i])
                    //    {
                    //        int s = 0;
                    //    }
                    //}
                }

                #endregion

                for (int i = 0; i < e.Length; i++)
                {
                    e[i] = -Math.Log(e[i]);
                }

                //double[] asef = this.simpleSamp(data, m, tau, r);

                //Clear out matches from the bin that is going away
                for (int i = 0; i < bigArray.Length; i++)
                {
                    for (int j = 0; j < m; j++)
                    {
                        bigArray[i].matchCount[startBin, j] = 0;
                    }
                }


                if(!first) startBin++;
                if (startBin >= numBins)
                    startBin = 0;

                //if (step != window) first = false;
                for (int i = 0; i < bigArray.Length; i++)
                {
                    bigArray[i].Reset();
                }
                

                int copy = step < data.Length - firstSpot ? step : data.Length - firstSpot;
                if (copy == 0) break;
                Array.Copy(bigArray, copy, bigArray, 0, bigArray.Length - copy);
                for (int i = 0; i < copy; i++)
                {
                    bigArray[bigArray.Length - copy + i] =
                        new orderedVal(m, data[firstSpot + i], firstSpot + i, numBins);
                }


                firstSpot += copy;
                sorted = (orderedVal[])bigArray.Clone();
                sort.Sort(sorted);

                for (int i = 0; i < bigArray.Length; i++)
                {
                    if (bigArray[i].index == -1)
                    {
                        bigArray[i].index = i;
                    }
                }
            }


            TimeSpan a = DateTime.Now.Subtract(now);
            now = DateTime.Now;

            double[] times = new double[firstSpotA.Count];
            for (int i = 0; i < firstSpotA.Count; i++)
            {
                times[i] = (firstSpotA[i] / freq);
            }

            List<double[]> timesA = new List<double[]>();
            timesA.Add(times);

            return new List<double[]>[] { As, Bs, Es, timesA, SDs};

        }

        public static double[][] simpleSamp(float[] data, int m, int tau, double r)
        {
            int MM = m * 2;

            int n = data.Length;
            double[] p = new double[m];
            //double[] v1 = new double[m];
            //double[] v2 = new double[m];
            double[] s1 = new double[m];
            //int[] R1 = new int[n * MM];
            //int[] R2 = new int[n * MM];
            //int[] F2 = new int[n * m];
            //int[] F1 = new int[n * m];
            //int[] F = new int[n * MM];
            //int[] run = new int[n];
            //int[] run1 = new int[n];
            double[] A = new double[m];
            double[] B = new double[m];
            //double[] K = new double[(m + 1) * m];
            //double[] n1 = new double[m];
            //double[] n2 = new double[m];

          //  bool[] storage = new bool[data.Length * data.Length];
            int numRun = 0;

            int[] tempR = new int[MM];
            for (int i = 0; i < data.Length; i++)
            {
                float[] vals = new float[m];
                for (int j = 0; j < m; j++)
                {
                    vals[j] = float.MinValue;
                    if(i + tau * j < data.Length)
                        vals[j] = data[i + tau * j];
                }

                for (int j = i+1; j < data.Length; j++)
                {
                    numRun++;

                    //if ((j - i - 1) % tau == 0)
                    //{
                    //    int count = (j - i - 1) / tau;
                    //    if (count < MM)
                    //    {
                    //        if (Math.Abs(data[i] - data[j]) <= r)
                    //        {
                    //            tempR[count]++;
                    //        }
                    //        else tempR[count] = 0;
                    //    }
                    //}

                    for (int k = 0; k < m; k++)
                    {
                        int index = j + tau * k;

                        if (vals[k] == float.MinValue)
                            break;

                        if (index >= data.Length) 
                            break;
                        B[k]++;

                        //if (k == m - 1)
                        //{
                        //    storage[i*data.Length + j] = true;
                        //}
                    
                        if (Math.Abs(vals[k] - data[index]) <= r)
                        {
                            

                            A[k]++;
                            //F1[i + k * data.Length + k]++;
                            //F[i + data.Length * k + k]++;
                            //F[j + data.Length * k + k]++;
                        }
                        else break;
                    }
                }

                //if (i == data.Length - 1)
                //{
                //    tempR = new int[MM];
                //}
                //for (int j = 0; j < MM; j++)
                //{
                //    R1[i + j * data.Length] = tempR[j];
                //}
            }

            //int[] Atest = new int[m * 32];
            //int[] Btest = new int[m * 32];
            //int[] R1test = new int[n * MM * 32];
            //int[] F1test = new int[n * m * 32];
            //int[] Ftest = new int[n * MM * 32];

            //#region Error calculation

            //for (int i = 1; i < MM; i++)
            //    for (int j = 0; j < i - 1; j++)
            //        R2[i + n * j] = R1[i - j - 1 + n * j]; //just move back by j
            
            //for (int i = MM; i < n; i++)
            //    for (int j = 0; j < MM; j++)
            //        R2[i + n * j] = R1[i - j - 1 + n * j];
            //for (int i = 0; i < n; i++)
            //{
            //    for (int mi = 0; mi < m; mi++)
            //    {
            //        int FF = F[i + n * mi];
            //        F2[i + n * mi] = FF - F1[i + n * mi];
            //        K[(m + 1) * mi] += FF * (FF - 1);
            //    }
            //}
         
            //for (int mi = 0; mi < m; mi++)
            //{
            //    p[mi] = (double)A[mi] / B[mi];
            //    v2[mi] = p[mi] * (1.0 - p[mi]) / B[mi];
            //}

            //int dd = 1;
            //for (int mi = 0; mi < m; mi++)
            //{
            //    int d2 = mi + 1 < m - 1 ? mi + 1 : m - 1;
            //    for (int d = 0; d < d2 + 1; d++)
            //    {
            //        for (int i1 = d + 1; i1 < n; i1++)
            //        {
            //            int i2 = i1 - d - 1;
            //            int nm1 = F1[i1 + n * mi];
            //            int nm3 = F1[i2 + n * mi];
            //            int nm2 = F2[i1 + n * mi];
            //            int nm4 = F2[i2 + n * mi];
            //            for (int j = 0; j < (dd - 1); j++)
            //            {
            //                if (R1[i1 + n * j] >= mi + 1)
            //                    nm1--;
            //                if (R2[i1 + n * j] >= mi + 1)
            //                    nm4--;
            //            }
            //            for (int j = 0; j < 2 * (d + 1); j++)
            //                if (R2[i1 + n * j] >= mi + 1)
            //                    nm2--;
            //            for (int j = 0; j < (2 * d + 1); j++)
            //                if (R1[i2 + n * j] >= mi + 1)
            //                    nm3--;
            //            K[d + 1 + (m + 1) * mi] +=
            //                (double)2 * (nm1 + nm2) * (nm3 + nm4);
            //        }
            //    }
            //}

            //n1[0] = (double)n * (n - 1) * (n - 2);
            //for (int mi = 0; mi < m - 1; mi++)
            //    for (int j = 0; j < mi + 2; j++)
            //        n1[mi + 1] += K[j + (m + 1) * mi];
            //for (int mi = 0; mi < m; mi++)
            //{
            //    for (int j = 0; j < mi + 1; j++)
            //    {
            //        n2[mi] += K[j + (m + 1) * mi];
            //    }
            //}

            //for (int mi = 0; mi < m; mi++)
            //{
            //    v1[mi] = v2[mi];
            //    double dv = (n2[mi] - n1[mi] * p[mi] * p[mi]) / (B[mi] * B[mi]);
            //    if (dv > 0)
            //        v1[mi] += dv;
            //    s1[mi] = (double)Math.Sqrt((double)(v1[mi]));
            //}

            //#endregion

            double[] e = new double[m];
            for (int i = 0; i < m; i++)
            {
                 e[i] = -Math.Log((double)A[i] / (double)B[i]);
            }

            return new double[][]{e, s1};
        }

       

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter sampleSize = new BatchProcessableParameter() 
            { 
                Name = "Sample Size", 
                Val = 10.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };
            BatchProcessableParameter dims = new BatchProcessableParameter() 
            { 
                Name = "Dimensions", 
                Val = 3,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };
            BatchProcessableParameter radius = new BatchProcessableParameter() 
            { 
                Name = "Radius", 
                Val = 0.2,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };
            BatchProcessableParameter startTau = new BatchProcessableParameter()
            {
                Name = "Start Tau",
                Val = 1,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter endTau = new BatchProcessableParameter()
            {
                Name = "End Tau",
                Val = 50,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=startTau,
                    Path="Val",
                    Operator=ComparisonValidator.Comparison.GreaterThanEqual,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter numSurrogates = new BatchProcessableParameter() 
            { 
                Name = "Number Surrogates", 
                Val = 19 
            };
            BatchProcessableParameter sdmult = new BatchProcessableParameter() 
            { 
                Name = "Std Dev Multiplier", 
                Val = 1.0 
            };
            BatchProcessableParameter pval = new BatchProcessableParameter() 
            { 
                Name = "P-Value", 
                Val = 0.20 
            };
            
            BatchProcessableParameter label = new BatchProcessableParameter() { Name = "Label", Val = "Enter Here" };
            BatchProcessableParameter data = new BatchProcessableParameter() 
            { 
                Name = "Data", 
                Editable=false,
                CanLinkFrom=false,
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                TargetType = typeof(float[])
            };
            BatchProcessableParameter resolution = new BatchProcessableParameter()
            {
                Name = "Data Resolution",
                Editable = false,
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=0.0,
                    Operator=ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };


            return new BatchProcessableParameter[] { sampleSize, dims, radius, startTau, endTau, numSurrogates, sdmult, pval, label, data, resolution };
        }

        private static object lockable = new object();

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            double sampleSize = Convert.ToDouble(parameters.Single(p => p.Name == "Sample Size").Val);
            int dims = Convert.ToInt32(parameters.Single(p => p.Name == "Dimensions").Val);
            double radius = Convert.ToDouble(parameters.Single(p => p.Name == "Radius").Val);
            int startTau = Convert.ToInt32(parameters.Single(p => p.Name == "Start Tau").Val);
            int endTau = Convert.ToInt32(parameters.Single(p => p.Name == "End Tau").Val);
            int numsur = Convert.ToInt32(parameters.Single(p => p.Name == "Number Surrogates").Val);
            double passMult = Convert.ToDouble(parameters.Single(p => p.Name == "Std Dev Multiplier").Val);
            double pval = Convert.ToDouble(parameters.Single(p => p.Name == "P-Value").Val);

            string label = parameters.Single(p => p.Name == "Label").Val as string;
            float[] data = parameters.Single(p => p.Name == "Data").Val as float[];
            double resolution = Convert.ToDouble(parameters.Single(p => p.Name == "Data Resolution").Val);


            double sd = radius * CeresBase.MathConstructs.Stats.StandardDev(data);
            int totTaus = endTau - startTau + 1;

            int sampleCount = (int)Math.Ceiling(sampleSize / resolution);
            if (sampleCount > data.Length)
            {
                sampleCount = data.Length;
            }

            List<double[]> E = new List<double[]>();
            List<double[]> errors = new List<double[]>();
            List<double[]>[] surr = new List<double[]>[numsur];

            for (int surrI = 0; surrI < numsur; surrI++)
            {
                surr[surrI] = new List<double[]>();
            }

            int begIndex = 0;
            while (begIndex + sampleCount <= data.Length)
            {
                float[] fData = new float[sampleCount];
                Array.Copy(data, begIndex, fData, 0, sampleCount);

                int repeat = numsur + 1;

                for (int thisrep = 0; thisrep < repeat; thisrep++)
                {
                    float[] rData = fData;
                    if (thisrep != 0)
                    {
                        rData = SurrogatePermutation.CreateSurrogate(fData);
                    }



                    double[] es = new double[totTaus];
                    double[] er = new double[totTaus];

                    bool one, two;
                    bool use = true;

                    lock (lockable)
                    {
                        use = SampEnWrapper.Available(out one, out two);
                    }

                    if (!use)
                    {
                        for (int tauI = 0; tauI < totTaus; tauI++)
                        {
                            int tau = startTau + tauI;
                            double[][] res = SampleEntropy.simpleSamp(rData, dims, tau, sd);

                            es[tauI] = res[0][dims - 1];
                            er[tauI] = res[1][dims - 1];

                        }
                    }
                    else
                    {
                        int mult = (int)Math.Ceiling(totTaus / 32.0) * 32;
                        double[] tempEs = new double[mult];

                        lock (lockable)
                        {

                            SampEnWrapper.CalculateSampEnCuda(rData, dims, (float)sd, startTau, endTau, tempEs);
                            Array.Copy(tempEs, 0, es, 0, totTaus);
                        }

                    }

                    if (thisrep == 0)
                    {
                        E.Add(es);
                        errors.Add(er);
                    }
                    else
                    {
                        surr[thisrep - 1].Add(es);
                    }

                }

                begIndex += sampleCount;
            }

            double[] surrMeans = new double[totTaus];
            double[] surrSD = new double[totTaus];
            double[] eMeans = new double[totTaus];
            double[] errMeans = new double[totTaus];

            int[] passed = new int[totTaus];



            for (int j = 0; j < E.Count; j++)
            {
                for (int k = 0; k < totTaus; k++)
                {
                    eMeans[k] += E[j][k] / E.Count;
                    errMeans[k] += errors[j][k] / E.Count;

                    double thisSurrMean = 0;
                    double thisSurrSD = 0;
                    for (int l = 0; l < numsur; l++)
                    {
                        thisSurrMean += surr[l][j][k] / numsur;
                    }
                    for (int l = 0; l < numsur; l++)
                    {
                        thisSurrSD += (surr[l][j][k] - thisSurrMean) * (surr[l][j][k] - thisSurrMean);
                    }
                    thisSurrSD /= numsur + 1;
                    thisSurrSD = Math.Sqrt(thisSurrSD);

                    if (E[j][k] + errors[j][k] < thisSurrMean - thisSurrSD * passMult ||
                        E[j][k] - errors[j][k] > thisSurrMean + thisSurrSD * passMult)
                    {
                        passed[k]++;
                    }

                    surrMeans[k] += thisSurrMean / E.Count;
                    surrSD[k] += thisSurrSD / E.Count;
                }
            }

            int[] totPassed = new int[totTaus];
            for (int j = 0; j < totTaus; j++)
            {
                double prob = CeresBase.MathConstructs.Stats.BinomialTest(0.5, passed[j], E.Count, false);
                if (1.0 - prob < pval)
                {
                    totPassed[j] = 1;
                }
            }

            GraphableOutput[] output = new GraphableOutput[2];

            double[] x = new double[totTaus];
            double[][] y1 = new double[6][];
            for (int i = 0; i < 6; i++)
            {
                y1[i] = new double[totTaus];
            }

            double[][] y2 = new double[1][];
            y2[0] = new double[totTaus];

            for (int j = 0; j < x.Length; j++)
            {
                x[j] = startTau + j;

                y1[0][j] = eMeans[j];
                y1[1][j] = eMeans[j] + errMeans[j];
                y1[2][j] = eMeans[j] - errMeans[j];

                y1[3][j] = surrMeans[j];
                y1[4][j] = surrMeans[j] + surrSD[j] * passMult;
                y1[5][j] = surrMeans[j] - surrSD[j] * passMult;

                y2[0][j] = totPassed[j];
            }

            output[0] = new GraphableOutput() { 
                SourceDescription = "Sample Entropy Results",
                XLabel = "Tau",
                YLabel= "Entropy",
                YUnits = "Bits",
                SeriesLabels = new string[]{"Signal", "Signal + Error", "Signal - Erorr", 
                    "Surrogate Means", "Surrogate + " + Math.Round(passMult,2) + " SDs",
                    "Surrogate - " + Math.Round(passMult,2) + " SDs"},
                Label = label, 
                XData = x, 
                YData = y1 };
            output[1] = new GraphableOutput() { 
                SourceDescription = "SampEn Passed Bionomial Test",
                XLabel="Tau",
                YLabel="Passed",
                YUnits="Yes or No",
                Label = label, 
                XData = x, 
                YData = y2 };

            //GraphableOutputCollection collection = new GraphableOutputCollection();
            //collection.Graphs.Add(output[0]);
            //collection.Graphs.Add(output[1]);
            return output;
        }


        #endregion

        #region IBatchFlowable Members


        public string[] OutputDescription
        {
            get { return new string[]{"Sample Entropy Graph", "Binomial Test"}; }
        }

        #endregion
    }
}
