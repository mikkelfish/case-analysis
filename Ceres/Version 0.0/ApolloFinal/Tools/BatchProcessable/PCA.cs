using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.UI;
using MathWorks.MATLAB.NET.Arrays;
using System.Windows.Forms;
using CeresBase.FlowControl.Adapters.Matlab;


namespace ApolloFinal.Tools.BatchProcessable
{
    public class PCARun /*: IBatchProcessable*/
    {
        
        
        public static float[,] RunPCA(float[][] data, double[] projectOnto, int maxVectors, bool rawPCA)
        {

            AllMatlabNative.All matlab = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;

            //MWNumericArray array = createArray(data);

            try
            {
                lock (MatlabLoader.MatlabLockable)
                {
                   // MWArray[] output = matlab.runPCA(2, array, new MWNumericArray(projectOnto), new MWNumericArray(maxVectors));
                    object[] output = matlab.runPCA(2, data, projectOnto, maxVectors);

                    int which = 0;
                    if (!rawPCA)
                        which = 1;
                    //Array reformed1 = (output[which] as MWNumericArray).ToArray(MWArrayComponent.Real);
                    //float[,] reformed = reformed1 as float[,];
                    float[,] reformed = output[which] as float[,];

                    //output[0].Dispose();
                    //output[1].Dispose();
                    return reformed;
                }
            }
            catch
            {
                MessageBox.Show("There was an error in Matlab. The datachunk was probably too big.");
            }
            finally
            {

                //array.Dispose();
            }

            return null;

        }

        public static double[] CalculateVariances(float[][] data)
        {
            return CalculateVariances(data, data.Length);
        }

        //private static MWNumericArray createArray(float[][] data)
        //{
        //    if (data.Length == 0) return null;
        //    int amount = data.Length * data[0].Length;
        //    float[] allData = new float[amount];
        //    for (int i = 0; i < data.Length; i++)
        //    {
        //        for (int j = 0; j < data[i].Length; j++)
        //        {
        //            allData[i * data[i].Length + j] = data[i][j];
        //        }
        //    }


        //    MWNumericArray array = new MWNumericArray(data.Length, data[0].Length, allData, false, true);
        //    return array;
        //}

        public static double[] CalculateVariances(float[][] data, int maxVectors)
        {
            AllMatlabNative.All matlab = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;
            
        //    MWNumericArray array = createArray(data);

            try
            {
                lock (MatlabLoader.MatlabLockable)
                {
                    object[] output = matlab.pc_evectors(3, data, maxVectors);

                    //MWArray[] output = matlab.pc_evectors(3, array, new MWNumericArray(maxVectors));
                    //Array vals1 = (output[1] as MWNumericArray).ToArray(MWArrayComponent.Real);
                    //float[,] vals = vals1 as float[,];
                    float[,] vals = output[1] as float[,];
                    double[] variances = new double[vals.Length];

                    double sum = 0;
                    for (int i = 0; i < variances.Length; i++)
                    {
                        sum += vals[i, 0];
                        variances[i] = sum;
                    }
                    for (int i = 0; i < variances.Length; i++)
                    {
                        variances[i] /= sum;
                    }

                    //output[0].Dispose();
                    //output[1].Dispose();
                    //output[2].Dispose();

                    return variances;
                }

            }
            catch
            {
                MessageBox.Show("There was an error in Matlab. The datachunk was probably too big.");                
            }
            finally
            {                
                //array.Dispose();
            }

            return null;
        }

        //#region IBatchProcessable Members

        //public bool SuppportsMultiple
        //{
        //    get { return true; }
        //}

        ////private class pcaPart
        ////{
        ////    public SpikeVariable var;
        ////    public double start;
        ////}

        //public object[] GetArguments(SpikeVariable[] varsToUse, out string[] names)
        //{
        //    RequestInformation req = new RequestInformation();

        //    List<Type> types = new List<Type>();
        //    List<string> categories = new List<string>();
        //    //List<string> names = new List<string>();
        //    List<string> desc = new List<string>();
        //    List<object> defaults = new List<object>();

        //    types.Add(typeof(float));
        //    categories.Add("Parameters");
        //    // names.Add("Width");
        //    desc.Add("The time width to use for each breath.");
        //    defaults.Add(1);

        //    //types.Add(typeof(bool));
        //    //categories.Add("Parameters");
        //    //names.Add("Show variances");
        //    //desc.Add("Show variances b

        //    // req.Grid.SetInformationToCollect(types.ToArray(), categories.ToArray(), names.ToArray(), desc.ToArray(), null, defaults.ToArray());
        //    if (req.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
        //    {
        //        names = null;
        //        return null;
        //    }

        //    names = null;
        //    return null;
        //}

        //public object[] Process(SpikeVariable[] varsToUse, object[] args)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public Type OutputType
        //{
        //    get { return typeof(MultiViewer); }
        //}

        //#endregion

        //#region ICloneable Members

        //public object Clone()
        //{
        //    return new PCA();
        //}

        //#endregion
    }
}
