using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.UI;
using System.Collections;
using System.Windows.Forms;
using CeresBase.Projects.Flowable;
using System.Linq;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl.Adapters.Matlab;
using MathWorks.MATLAB.NET.Arrays;


namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Raw")]
    class FastFourier : IBatchFlowable
    {
        public override string ToString()
        {
            return "Fast Fourier";
        }

        public bool SuppportsMultiple
        {
            get { return true; }
        }

       

        public static double[][] RunFastFourier(int numbins, float minfreq, float maxfreq, bool normal, double freq, float[] toPass)
        {
            //MatlabAdapter ff = MatlabLoader.GetAdapter("Internal", "powerSpectrum");

            //AllMatlabNative.All all = MatlabLoader.MatlabFunctions;
            //object[] total = all.powerSpectrum(2,toPass, freq);
            //if (ff == null) return null;
           // object[] total = ff.Run(new object[] { toPass, freq });

            object[] total = null;
            lock (MatlabLoader.MatlabLockable)
            {
                AllMatlabNative.All all = MatlabLoader.MatlabFunctions;

                //MWNumericArray dataA = new MWNumericArray(toPass);
                //MWNumericArray freqAs = new MWNumericArray(freq);

                total = all.powerSpectrum(2, toPass, 1.0 / freq);
            }

            double[,] freqA = total[1] as double[,]; //(total[1] as MWNumericArray).ToArray() as double[,];
            double[,] fftA = total[0] as double[,];//(total[0] as MWNumericArray).ToArray(MWArrayComponent.Real) as double[,];

            //dataA.Dispose();6
            //freqAs.Dispose();


            //MWNumericArray dataA = new MWNumericArray(toPass);
            //MWNumericArray freqArray = (MWNumericArray)(freq);

            //EverythingMatlab.EverythingMatlabclass suite = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;

            //MWNumericArray freqs = null;
            //MWNumericArray FFT = null;
            //lock (MatlabLoader.MatlabLockable)
            //{
            //    MWArray[] mwarray = suite.FastFourier(2, dataA, freqArray, (MWNumericArray)numbins);
            //    freqs = mwarray[0] as MWNumericArray;
            //    FFT = mwarray[1] as MWNumericArray;
            //}
            //double[,] freqA = (double[,])freqs.ToArray(MWArrayComponent.Real);
            //double[,] fftA = (double[,])FFT.ToArray(MWArrayComponent.Real);


            //double[] x = new double[freqA.GetLength(1)];
            //double[] y = new double[fftA.GetLength(1)];

            //for (int i = 0; i < x.Length; i++)
            //{
            //    x[i] = freqA[0, i];
            //    y[i] = fftA[0, i];
            //}

            double step = freqA[0, 1] - freqA[0, 0];
            double amountPerBin = (maxfreq - minfreq) / numbins;
         //   int numStepsPerBin = (int)(amountPerBin / step);

            int curCount = 0;
            double curVal = 0;

            List<double> x = new List<double>();
            List<double> y = new List<double>();
            for (int i = 0; i < freqA.GetLength(1); i++)
            {
                float what = (float)Math.Round(freqA[0, i], 5);

                if (what >= minfreq && what <= maxfreq)
                {
                    if (x.Count == 0)
                    {
                        x.Add(freqA[0, i]);
                    }

                    curVal += fftA[0, i];

                    if (i < freqA.GetLength(1) - 1 && freqA[0,i + 1] > x[x.Count-1] + amountPerBin)
                    {
                        y.Add(curVal);
                        x.Add(freqA[0, i]);
                        curVal = 0;
                    }

                    //x.Add(freqA[0, i]);
                    //y.Add(fftA[0, i]);
                }
                else if (freqA[0, i] >= maxfreq)
                {
                    y.Add(curVal);
                    break;
                }
                //else if (i + 1 < freqA.GetLength(1) && freqA[0, i] < minfreq && freqA[0, i + 1] > minfreq)
                //{
                //    double perc = (minfreq - freqA[0, i]) / (freqA[0, i + 1] - freqA[0, i]);
                //    double val = fftA[0, i] + perc * (fftA[0, i + 1] - fftA[0, i]);
                //    x.Add(minfreq);
                //    y.Add(val);
                //}
                //else if (i > 0 && freqA[0, i - 1] < maxfreq && freqA[0, i] > maxfreq)
                //{
                //    double perc = (maxfreq - freqA[0, i - 1]) / (freqA[0, i] - freqA[0, i - 1]);
                //    double val = fftA[0, i - 1] + perc * (fftA[0, i] - fftA[0, i - 1]);
                //    x.Add(maxfreq);
                //    y.Add(val);
                //}
            }

            if (y.Count != x.Count)
            {
                x.RemoveAt(x.Count - 1);
            }

            //if (normal)
            //{
                double sum = 0;
                for (int i = 0; i < y.Count; i++)
                {
                    sum += y[i];
                }

                for (int i = 0; i < y.Count; i++)
                {
                    y[i] /= sum;
                }
            //}


            return new double[][] { y.ToArray(), x.ToArray() };
        }

    

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            //req.Grid.SetInformationToCollect(
            //    new Type[] { typeof(int), typeof(float), typeof(float), typeof(bool) },
            //    new string[] { "FF", "Range", "Range", "Normalize" },
            //    new string[] { "Num Bins", "Min Freq", "Max Freq", "Normalize" },
            //    new string[] { "Number of bins", "Minium (maybe) frequency", "Maximum (maybe) frequency", "Normalize to 1" },
            //    null, new object[] { 512, .1, 15, true });

            BatchProcessableParameter numbins = new BatchProcessableParameter()
            { 
                Name = "Number Bins",
                Val = 512
            };
            BatchProcessableParameter minfreq = new BatchProcessableParameter()
            {
                Name = "Minimum Frequency",
                Val = .1,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=0.0,
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter maxfreq = new BatchProcessableParameter()
            {
                Name = "Maximum Frequency",
                Val = 15.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=minfreq,
                    Path="Val",
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter normalize = new BatchProcessableParameter() 
            { 
                Name = "Normalize", 
                Val = true
            };
            BatchProcessableParameter label = new BatchProcessableParameter() { Name = "Label", Val = "Enter Here" };
            BatchProcessableParameter data = new BatchProcessableParameter() 
            { 
                Name = "Data", 
                Editable=false,
                CanLinkFrom=false,
                Validator=new MesosoftCommon.Utilities.Validations.NotNullValidator()
            };
            BatchProcessableParameter res = new BatchProcessableParameter()
            {
                Name = "Data Resolution",
                Editable = false,
                Val=0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=0.0,
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };

            return new BatchProcessableParameter[] { numbins, minfreq, maxfreq, normalize, data, res, label };



        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            BatchProcessableParameter numbinsP = parameters.Single(p => p.Name == "Number Bins");
            BatchProcessableParameter minfreqP = parameters.Single(p => p.Name == "Minimum Frequency");
            BatchProcessableParameter maxfreqP = parameters.Single(p => p.Name == "Maximum Frequency");
            BatchProcessableParameter normalizeP = parameters.Single(p => p.Name == "Normalize");
            BatchProcessableParameter labelP = parameters.Single(p => p.Name == "Label");
            BatchProcessableParameter dataP = parameters.Single(p => p.Name == "Data");
            BatchProcessableParameter resP = parameters.Single(p => p.Name == "Data Resolution");

            int numbins = Convert.ToInt32(numbinsP.Val);
            double minfreq = Convert.ToDouble(minfreqP.Val);
            double maxfreq = Convert.ToDouble(maxfreqP.Val);
            bool normalize = Convert.ToBoolean(normalizeP.Val);
            string label = labelP.Val as string;

            float[] data = dataP.Val as float[];
            double res = Convert.ToDouble(resP.Val);

            double[][] results = RunFastFourier(numbins, (float)minfreq, (float)maxfreq, normalize, 1.0 / res, data);

            GraphableOutput output = new GraphableOutput() { 
                SourceDescription = "Fast Fourier",
                Label = label, 
                XData = results[1], 
                XLabel = "Frequency",
                XUnits = "Hz",
                YLabel = normalize ? "Normalized Power" : "Power",
                YData = new double[1][]{results[0]} };
            return new object[]{output};
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Graph of Power Spectrum" }; }
        }

        #endregion
    }
}
