using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using CeresBase.UI;
using CeresBase.Projects.Flowable;
using System.Linq;
using CeresBase.FlowControl.Adapters;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Raw")]
    public class Differentiate :  IBatchFlowable
    {     
        public override string ToString()
        {
            return "Differentiate";
        }

        public static float[] Derivative(float[] data, double resolution, int hammingWindowOrder)
        {
            double[] hammingMults = new double[hammingWindowOrder * 2 + 1];
            for (int i = 0; i < hammingMults.Length; i++)
            {
                hammingMults[i] = .54 - .46 * Math.Cos(2.0 * Math.PI * (i + 1) / (2.0 * hammingWindowOrder + 1));
            }

            List<float> sums = new List<float>();
            for (int i = 0; i < data.Length - 2 * hammingWindowOrder - 1; i++)
            {
                float sum = 0;

                //mth order Hamming
                //a(j)  =  0.54  -  0.46  cos[2pi(j  +  1)/(2m  +  l)] 
                //d'X = [x(k  +  1)  -  x(k)]/T (T = sampling) 
                // dX(k)  =  Sum j=0->2m a(j)[d'X(k  + j)]
                for (int j = 0; j <= 2 * hammingWindowOrder; j++)
                {
                    sum += (float)(hammingMults[j] * ((data[i + j + 1] - data[i + j]) / resolution));
                }
                sums.Add(sum);
            }
            return sums.ToArray();
        }

        public static double[] Derivative(double[] data, double resolution, int hammingWindowOrder)
        {
            double[] hammingMults = new double[hammingWindowOrder * 2 + 1];
            for (int i = 0; i < hammingMults.Length; i++)
            {
                hammingMults[i] = .54 - .46 * Math.Cos(2.0 * Math.PI * (i + 1) / (2.0 * hammingWindowOrder + 1));
            }

            List<double> sums = new List<double>();
            for (int i = 0; i < data.Length - 2 * hammingWindowOrder - 1; i++)
            {
                double sum = 0;

                //mth order Hamming
                //a(j)  =  0.54  -  0.46  cos[2pi(j  +  1)/(2m  +  l)] 
                //d'X = [x(k  +  1)  -  x(k)]/T (T = sampling) 
                // dX(k)  =  Sum j=0->2m a(j)[d'X(k  + j)]
                for (int j = 0; j <= 2 * hammingWindowOrder; j++)
                {
                    sum += hammingMults[j] * ((data[i + j + 1] - data[i + j]) /resolution);
                }
                sums.Add(sum);
            }
            return sums.ToArray();
        }

       
        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            return new BatchProcessableParameter[] {
                    new BatchProcessableParameter()
                    {
                        Name="Hamming Window Order", 
                        Val=4,
                        Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                        {
                            CompareTo=0,
                            Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                            TreatNonComparableAsSuccess=true
                        }
                    },
                    new BatchProcessableParameter()
                    {
                        Name="Data", 
                        Editable=false, 
                        CanLinkFrom = false,
                        Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator()
                    },
                    new BatchProcessableParameter(){
                        Name="Data Resolution", 
                        Editable=false,
                        Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator()
                    }
                };
        }



        public object[] Run(BatchProcessableParameter[] parameters)
        {

            BatchProcessableParameter hamming = parameters.Single(p => p.Name == "Hamming Window Order");
            BatchProcessableParameter dataP = parameters.Single(p => p.Name == "Data");
            BatchProcessableParameter dataResP = parameters.Single(p => p.Name == "Data Resolution");


            int hammingWindowOrder = (int)hamming.Val;
            float[] data = dataP.Val as float[];
            double res = Convert.ToDouble(dataResP.Val);

            if (data == null) throw new Exception("Error getting data.");

            return new object[] { Differentiate.Derivative(data, res, hammingWindowOrder) };
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Differentiated Data" }; }
        }

        #endregion
    }
}
