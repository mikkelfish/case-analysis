using System;
using System.Collections.Generic;
using System.Text;
using MathWorks.MATLAB.NET.Arrays;
using CeresBase.UI;
using System.Collections;
using System.Windows.Forms;
using CeresBase.Projects.Flowable;
using System.Linq;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl.Adapters.Matlab;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Interval")]
    [AdapterDescription("Raw")]
    class MutualInformation : IBatchFlowable
    {

        public override string ToString()
        {
            return "Mutual Information";
        }

      

        //public static UI.MatlabPlot GetMutual(float[] data, int maxTau)
        //{
        //    double[][] val = runMutual(data, maxTau);

        //    UI.MatlabPlot plot = new ApolloFinal.UI.MatlabPlot();
        //    plot.XData = val[0];
        //    plot.YData = val[1];
        //    plot.Label = "Mutual Information";
        //    return plot;
        //}

        private static double[][] runMutual(float[] data, int maxTau)
        {
            AllMatlabNative.All suite = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;
            lock (MatlabLoader.MatlabLockable)
            {

                //MWArray[] result = suite.MutInf(4, (MWNumericArray)data, (MWNumericArray)maxTau,
                //        (MWNumericArray)100);

                object[] result = suite.MutInf(4, data, maxTau, 100);

                //MWNumericArray I = (MWNumericArray)result[0];
                //MWNumericArray taugrid = (MWNumericArray)result[1];
                //double[,] iD = (double[,])I.ToArray(MWArrayComponent.Real);
                //double[,] tauD = (double[,])taugrid.ToArray(MWArrayComponent.Real);

                double[,] iD = result[0] as double[,];
                double[,] tauD = result[1] as double[,];

                double[] finI = new double[iD.Length];
                double[] finLags = new double[tauD.Length];
                for (int i = 0; i < finI.Length; i++)
                {
                    finI[i] = iD[0, i];
                    finLags[i] = tauD[0, i];
                    //    finCor[i, 1] = 0.0;
                }

                //I.Dispose();
                //taugrid.Dispose();


                return new double[][] { finLags, finI };
            }

        }      

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            //req.Grid.SetInformationToCollect(
            //   new Type[] { typeof(int), typeof(float), typeof(float) },
            //   new string[] { "Mutual Information", "Time", "Time" },
            //   new string[] { "Max Tau", "Start", "End" },
            //   new string[] { "The maximum number of delays.", "Start time.", "End time." },
            //   null,
            //   new object[] { 100, 0, -1 });

            BatchProcessableParameter maxtau = new BatchProcessableParameter()
            {
                Name = "Max Tau",
                Val = 100,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=0,
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter data = new BatchProcessableParameter() 
            { 
                Name = "Data", 
                Editable=false,
                CanLinkFrom=false,
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                TargetType=typeof(float[])
            };
            BatchProcessableParameter label = new BatchProcessableParameter() { Name = "Label", Val = "Enter Here" };


            return new BatchProcessableParameter[] { maxtau, data, label };

        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            BatchProcessableParameter tauP = parameters.Single(p => p.Name == "Max Tau");
            BatchProcessableParameter dataP = parameters.Single(p => p.Name == "Data");
            BatchProcessableParameter labelP = parameters.Single(p => p.Name == "Label");


            int tau = Convert.ToInt32(tauP.Val);
            float[] data = dataP.Val as float[];
           

            double[][] results = runMutual(data, tau);

            GraphableOutput output = new GraphableOutput() { 
                SourceDescription = "Mutual Information",
                XData = results[0], 
                XLabel = "Lags",
                YLabel = "Information",
                YUnits= "Bits",
                YData = new double[][]{results[1]}, 
                Label = labelP.Val as string };
            return new object[]{output};

        }

        public string[] OutputDescription
        {
            get { return new string[] { "Mutual Information Graph" }; }
        }

        #endregion
    }
}
