//using System;
//using System.Collections.Generic;
//using System.Text;
//using ApolloFinal.Tools.BatchProcessing;
//using CeresBase.UI;
//using MathWorks.MATLAB.NET.Arrays;
//using CeresBase.FlowControl.Adapters.Matlab;

//namespace ApolloFinal.Tools.BatchProcessable
//{
//    class IntervalEnt : IBatchProcessable
//    {
//        #region IBatchProcessable Members

//        public bool SuppportsMultiple
//        {
//            get { return false; }
//        }


//        public object[] GetArguments(SpikeVariable[] varsToUse, out string[] names)
//        {
//            names = new string[varsToUse.Length];
//            for (int i = 0; i < names.Length; i++)
//            {
//                names[i] = varsToUse[i].FullDescription + " Interval Entropy";
//            }

//            RequestInformation req = new RequestInformation();
//            req.Grid.SetInformationToCollect(
//                new Type[]{typeof(float),typeof(float)},
//                new string[]{"Time", "Time"},
//                new string[]{"Start", "End"},
//                new string[]{"Start Time", "End Time"},
//                null, new object[]{0, -1});
//            if (req.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
//            {
//                names = null;
//                return null;
//            }

//            return new object[] { req.Grid.Results["Start"], req.Grid.Results["End"] };
//        }

//        public object[] Process(SpikeVariable[] varsToUse, object[] args)
//        {
//            EverythingMatlab.EverythingMatlabclass suite = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;
           

//            float start = (float)args[0];
//            float end = (float)args[1];

//            UI.MatlabPlot[] plots = new ApolloFinal.UI.MatlabPlot[varsToUse.Length];
//            for (int i = 0; i < varsToUse.Length; i++)
//            {
//                plots[i] = new ApolloFinal.UI.MatlabPlot();
//                plots[i].Label = varsToUse[i].FullDescription + " Interval Entropy " + start + "-" + end;
//                plots[i].XData = new double[] { 0 };

//                float realEnd = end;
//                if (end == -1) realEnd = (float)varsToUse[i].TotalLength;

//                int startI = (int)(start / varsToUse[i].Resolution);
//                int endI = (int)(realEnd / varsToUse[i].Resolution);
//                varsToUse[0][0].Pool.GetData(new int[] { startI }, new int[] { endI - startI },
//                    delegate(float[] data, uint[] counts, uint[] indices)
//                    {
//                        lock (MatlabLoader.MatlabLockable)
//                        {

//                            MWNumericArray dataA = new MWNumericArray(data);
//                            MWArray[] mwarray = suite.IntEntropy(2, dataA);
//                            MWNumericArray e = mwarray[0] as MWNumericArray;
//                            MWNumericArray hist = mwarray[1] as MWNumericArray;
//                            double eVal = ((double[,])e.ToArray(MWArrayComponent.Real))[0, 0];
//                            double histVal = ((double[,])hist.ToArray(MWArrayComponent.Real))[0, 0];
//                            double[,] y = new double[2, 1];
//                            y[0, 0] = eVal;
//                            y[1, 0] = histVal;
//                            plots[i].YData = y;
//                        }
//                    }
//                );			 
//            }
//            return plots;

//        }

//        public Type OutputType
//        {
//            get { return typeof(UI.MatlabPlot); }
//        }

//        #endregion

//        #region ICloneable Members

//        public object Clone()
//        {
//            return new IntervalEnt();
//        }

//        #endregion
//    }
//}
