﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl.Adapters.Matlab;
using CeresBase.UI;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Common")]
    public class DFA : IBatchFlowable
    {
        #region IBatchFlowable Members

        public override string ToString()
        {
            return "DFA";
        }

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter label = new BatchProcessableParameter()
            {
                Name = "Label",
                Val = "Enter Here"
            };
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                Name = "Data",
                Editable = false,
                CanLinkFrom = false,
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                TargetType = typeof(float[])
            };

            BatchProcessableParameter tau = new BatchProcessableParameter()
            {
                Name = "Scaling",
                Val = 40,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0,
                    Operator = MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };

            return new BatchProcessableParameter[] { data, tau, label };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            float[] data = parameters.Single(s => s.Name == "Data").Val as float[];
            int scaling = Convert.ToInt32(parameters.Single(s => s.Name == "Scaling").Val);
            string label = parameters.Single(s => s.Name == "Label").Val as string;

            lock (MatlabLoader.MatlabLockable)
            {
                object[] output = MatlabLoader.MatlabFunctions.DFA(5, data, scaling);
                double[,] slopes = output[0] as double[,];
                double[,] area = output[3] as double[,];
                double[,] x = output[1] as double[,];
                double[,] fn = output[2] as double[,];
                double[,] fit = output[4] as double[,];

                GraphableOutput graph = new GraphableOutput();
                graph.XData = new double[fn.GetLength(1)];
                graph.YData = new double[3][] { new double[fit.GetLength(1)], new double[fit.GetLength(1)], new double[fit.GetLength(1)] };
                for (int i = 0; i < graph.XData.Length; i++)
                {
                    graph.XData[i] = x[0, i];
                    graph.YData[0][i] = fn[0, i];
                    graph.YData[1][i] = fit[0, i];

                }

                graph.SourceDescription = "DFA";

                for (int i = 0; i < area.GetLength(1); i++)
                {
                    for (int j = (int)area[0,i] - 1 ; j <= (int)area[1,i]-1; j++)
                    {
                        graph.YData[2][j] = slopes[0, i];
                    }
                }

                graph.SeriesLabels = new string[] { "Vals", "Slopes" };
                return new object[] { graph };

            }

            return null;
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Slope Estimate"}; }
        }

        #endregion
    }
}
