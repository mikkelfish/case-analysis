//using System;
//using System.Collections.Generic;
//using System.Text;
//using ApolloFinal.Tools.BatchProcessing;
//using CeresBase.Data;
//using CeresBase.UI;

//namespace ApolloFinal.Tools.BatchProcessable
//{
//    class Rossler : IBatchProcessable
//    {
//        public override string ToString()
//        {
//            return "Rossler";
//        }
//        public bool SuppportsMultiple
//        {
//            get { return false; }
//        }

//        public object[] GetArguments(SpikeVariable[] varsToUse, out string[] names)
//        {
//            RequestInformation req = new RequestInformation();
//            req.Grid.SetInformationToCollect(
//                new Type[] { typeof(double), typeof(double), typeof(double), typeof(string), typeof(int), typeof(float), typeof(bool), typeof(double) },
//                new string[] { "Parameters", "Parameters", "Parameters", "Parameters", "Length", "Length", "Include Z", "Noise" },
//                new string[] { "a", "b", "c", "Output (x,y,z)", "Steps", "Time", "Include Z", "Noise" },
//                new string[] { "a", "b", "c", "The variable to output.", "The number of steps to calculate.", "Time (sec) of output variable.", "", "Noise Percentage" },
//                null,
//                new object[] { .1, .1, 14, "x", 25000, 100, true, 0.0 });

//            if (req.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
//            {
//                names = null;
//                return null;
//            }
//            double a = (double)req.Grid.Results["a"];
//            double b = (double)req.Grid.Results["b"];
//            double c = (double)req.Grid.Results["c"];
//            string output = (string)req.Grid.Results["Output (x,y,z)"];
//            int steps = (int)req.Grid.Results["Steps"];
//            float length = (float)req.Grid.Results["Time"];
//            bool includez = (bool)req.Grid.Results["Include Z"];
//            double noise = (double)req.Grid.Results["Noise"];


//            names = new string[1];
//            names[0] = "Rossler out- " + output + length.ToString() + " " + a.ToString() + " " + b.ToString() + " " + c.ToString();
//            return new object[] { a, b, c, output, steps, length, includez, noise };
//        }

//        private delegate double rungeFunction(double[] vals, double a, double b, double c);

//        //Rossler
//        //dx/dt=-y-z
//        //dy/dt=x+ay
//        //dz/dt=b+z*(x-c)
//        private double xPrime(double[] vals, double a, double b, double c)
//        {
//            return -vals[1] - vals[2];
//        }

//        private double yPrime(double[] vals, double a, double b, double c)
//        {
//            return vals[0] + a * vals[1];
//        }

//        private double zPrime(double[] vals, double a, double b, double c)
//        {
//            return b + vals[2] * (vals[0] - c);
//        }


//        //Runge-Kutta = yn+1 = yn + h/6(k1+2k2+2k3+k4)
//        //k1 = f(tn,yn)
//        //k2 = f(tn + h/2, yn+h/2*k1)
//        //k3 = f(tn+h/2, yn+h/2*k2)
//        //k4 = f(tn+h, yn+h*k3)
//        private double rungeStep(int which, double step, double[] vals, double a, double b, double c)
//        {
//            rungeFunction func = null;
//            double[] toPass = new double[3];
//            vals.CopyTo(toPass, 0);
//            switch (which)
//            {
//                case 0:
//                    func = new rungeFunction(this.xPrime);
//                    break;
//                case 1:
//                    func = new rungeFunction(this.yPrime);
//                    break;
//                case 2:
//                    func = new rungeFunction(this.zPrime);
//                    break;
//                default:
//                    throw new Exception("Wrong which");
//            }

//            double k1 = func(toPass, a, b, c);
//            toPass[which] = vals[which] + step * k1 / 2.0;
//            double k2 = func(toPass, a, b, c);
//            toPass[which] = vals[which] + step * k2 / 2.0;
//            double k3 = func(toPass, a, b, c);
//            toPass[which] = vals[which] + step * k3;
//            double k4 = func(toPass, a, b, c);
//            return vals[which] + step * (k1 + k2 + k3 + k4) / 6.0;

//        }

//        public object[] Process(SpikeVariable[] varsToUse, object[] args)
//        {
//            double a = (double)args[0];
//            double b = (double)args[1];
//            double c = (double)args[2];
//            string output = (string)args[3];
//            int steps = (int)args[4];
//            float length = (float)args[5];
//            bool includez = (bool)args[6];
//            double noise = (double)args[7];

//            IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { steps });

//            int beginning = 5000;
//            double h = .01;

//            double[] vals = new double[] { 1, 1, 1 };
//            if (!includez)
//            {
//                vals[2] = 0;
//                b = 0;
//                c = 0;
//            }

//            CeresBase.MathConstructs.RealRandom rand = new CeresBase.MathConstructs.RealRandom();

//            for (int i = 0; i < beginning; i++)
//            {
//                for (int j = 0; j < vals.Length; j++)
//                {
//                    vals[j] = this.rungeStep(j, h, vals, a, b, c);
//                    double rval = rand.NextDouble();
//                    rval -= .5;
//                    rval *= noise;
//                    vals[j] += vals[j] * rval;

//                }
//            }

//            int toSave = 0;
//            if (output == "y") toSave = 1;
//            if (output == "z") toSave = 2;

//            float[] buffer = new float[256000];
//            int bufferSpot = 0;
//            int tot = 0;
//            for (int i = 0; i < steps; i++)
//            {
//                for (int j = 0; j < vals.Length; j++)
//                {
//                    vals[j] = this.rungeStep(j, h, vals, a, b, c);
//                    double rval = rand.NextDouble();
//                    rval -= .5;
//                    rval *= noise;
//                    vals[j] += vals[j] * rval;

//                    if (j == toSave)
//                    {
//                        buffer[bufferSpot] = (float)vals[j];
//                        bufferSpot++;
//                        if (bufferSpot == buffer.Length)
//                        {
//                            pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(buffer));
//                            tot += bufferSpot;
//                            bufferSpot = 0;
//                        }
//                    }
//                }
//            }

//            List<float> testing = new List<float>();
//            if (bufferSpot != 0)
//            {
//                float[] tempBuff = new float[bufferSpot];
//                Array.Copy(buffer, 0, tempBuff, 0, bufferSpot);
//                testing.AddRange(tempBuff);
//                pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(tempBuff));
//                tot += bufferSpot;

//            }

//            double freq = (float)steps / length;
//            SpikeVariableHeader header = new SpikeVariableHeader("Rossler out- " + output + length.ToString() + " " + a.ToString() + " " + b.ToString() + " " + c.ToString(),
//                "None", "Rosslers", freq, SpikeVariableHeader.SpikeType.Raw);
//            SpikeVariable var = new SpikeVariable(header);
//            DataFrame frame = new DataFrame(var, pool, new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless);
//            var.AddDataFrame(frame);

//            return new object[] { var };

//        }

//        public Type OutputType
//        {
//            get { return typeof(SpikeVariable); }
//        }

//        #region ICloneable Members

//        public object Clone()
//        {
//            return new Rossler();
//        }

//        #endregion
//    }
//}
