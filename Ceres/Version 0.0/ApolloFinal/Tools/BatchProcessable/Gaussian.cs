//using System;
//using System.Collections.Generic;
//using System.Text;
//using ApolloFinal.Tools.BatchProcessing;
//using CeresBase.UI;
//using CeresBase.Data;
//using CeresBase.MathConstructs;

//namespace ApolloFinal.Tools.BatchProcessable
//{
//    class Gaussian : IBatchProcessable
//    {
//        #region IBatchProcessable Members

//        public bool SuppportsMultiple
//        {
//            get { return false; }
//        }

//        public object[] GetArguments(SpikeVariable[] varsToUse, out string[] names)
//        {
//            RequestInformation req = new RequestInformation();
//            req.Grid.SetInformationToCollect(
//                new Type[] { typeof(double), typeof(double), typeof(int)},
//                new string[] { "Params", "Params", "Number Points"},
//                new string[] { "Mean", "SD", "Number Points" },
//                new string[] { "Mean", "SD", "Number Points" },
//                null,
//                new object[] { 0, 1, 10000});
//            if (req.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
//            {
//                names = null;
//                return null;
//            }

//            names = new string[1] { "Gaussian " + req.Results["Mean"] + req.Results["SD"] };
//            return new object[] { req.Results["Mean"], req.Results["SD"], req.Results["Number Points"] };
//        }

//        private double y2 = 0;
//        private bool use_last = false;

//        private double box_muller(Random rand, double m, double s)	/* normal random variate generator */
//        {				        /* mean m, standard deviation s */
//            double x1, x2, w, y1;


//            if (use_last)		        /* use value from previous call */
//            {
//                y1 = y2;
//                use_last = false;
//            }
//            else
//            {
//                do
//                {
//                    x1 = 2.0 * rand.NextDouble() - 1.0;
//                    x2 = 2.0 * rand.NextDouble() - 1.0;
//                    w = x1 * x1 + x2 * x2;
//                } while (w >= 1.0);

//                w = Math.Sqrt((-2.0 * Math.Log(w)) / w);
//                y1 = x1 * w;
//                y2 = x2 * w;
//                use_last = true;
//            }

//            return (m + y1 * s);
//        }

//        public object[] Process(SpikeVariable[] varsToUse, object[] args)
//        {
//            double mean = (double)args[0];
//            double sd = (double)args[1];
//            int numpoints = (int)args[2];

//            IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[]{numpoints});
//            RealRandom rand = new RealRandom();
            
//            float[] pts = new float[numpoints];

//            float running = 0;
//            for (int i = 0; i < numpoints; i++)
//            {
//                double nextval = this.box_muller(rand, mean, sd);
//                pts[i] = (float)nextval + running;
//              //  running += (float)nextval;
//            }

//            pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(pts));
//            SpikeVariable ran = new SpikeVariable(new SpikeVariableHeader("Gaussian Var" + " " + mean + " " + sd, "none", "ra", 1, SpikeVariableHeader.SpikeType.Raw));
//            DataFrame frame = new DataFrame(ran, pool, new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless);
//            ran.AddDataFrame(frame);
//            return new object[] { ran };


//        }

//        public Type OutputType
//        {
//            get { return typeof(SpikeVariable); }
//        }

//        #endregion

//        #region ICloneable Members

//        public object Clone()
//        {
//            return new Gaussian();
//        }

//        #endregion
//    }
//}
