using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.UI;
using MathWorks.MATLAB.NET.Arrays;
using ApolloFinal.Tools.AttractorReconstruction;
using CeresBase.Projects.Flowable;
using System.Linq;
using CeresBase.FlowControl.Adapters;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Raw")]
    class NDimPlot : IBatchFlowable
    {
        public override string ToString()
        {
            return "Embedding Plot";
        }

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            //req.Grid.SetInformationToCollect(
            //   new Type[] { typeof(int), typeof(int), typeof(float), typeof(float), typeof(bool), typeof(int) },
            //   new string[] { "Embedding", "Embedding", "Time", "Time", "PCA", "PCA" },
            //   new string[] { "Num Dims", "Delay", "Start", "End", "Project onto PCA", "PCA Dims" },
            //   new string[] { "The maximum number of dimensions.", "Time delay", "Start time.", "End time.", "Project onto PCA", "PCA projection" },
            //   null,
            //   new object[] { 2, 10, 0, 0, false, 3 });

            BatchProcessableParameter numdims = new BatchProcessableParameter()
            {
                Name = "Num Dims",
                Val = 2,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=2,
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThanEqual,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter delay = new BatchProcessableParameter()
            {
                Name = "Time Delay",
                Val = 10,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=0,
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter project = new BatchProcessableParameter() { Name = "Project onto PCA", Val = false };
            BatchProcessableParameter projectDims = new BatchProcessableParameter()
            {
                Name = "PCA Dimensions",
                Val = 3,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=2,
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThanEqual,
                    TreatNonComparableAsSuccess=true
                }
            };

            BatchProcessableParameter data = new BatchProcessableParameter() 
            {
                Name = "Data", 
                Editable=false,
                CanLinkFrom=false,
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator()
            };

            return new BatchProcessableParameter[] { numdims, delay, project, projectDims, data };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            BatchProcessableParameter numdimsP = parameters.Single(p => p.Name=="Num Dims");
            BatchProcessableParameter delayP = parameters.Single(p => p.Name == "Time Delay");
            BatchProcessableParameter projectP = parameters.Single(p => p.Name == "Project onto PCA");
            BatchProcessableParameter projectDimsP = parameters.Single(p => p.Name == "PCA Dimensions");

            BatchProcessableParameter dataP = parameters.Single(p => p.Name == "Data");

            int numDims = (int)numdimsP.Val;
            int delay = (int)delayP.Val;
            bool project = (bool)projectP.Val;
            int projectDims = (int)projectDimsP.Val;

            float[] data = dataP.Val as float[];


            NPoint[] points = AttractorReconstruction.AttractorReconstruction.CreateTimeEmbedding(data, numDims, delay);

            if (project)
            {
                float[][] pointsdata = new float[numDims][];
                for (int i = 0; i < pointsdata.Length; i++)
                {
                    pointsdata[i] = new float[points.Length];
                    for (int j = 0; j < points.Length; j++)
                    {
                        pointsdata[i][j] = points[j].Points[i];
                    }
                }

                double[] dimArray = new double[projectDims];
                for (int i = 0; i < dimArray.Length; i++)
                {
                    dimArray[i] = i + 2;
                }
                float[,] pdata = PCARun.RunPCA(pointsdata, dimArray, numDims, true);

                points = new NPoint[points.Length];
                for (int i = 0; i < points.Length; i++)
                {
                    points[i] = new NPoint(projectDims);

                    for (int j = 0; j < projectDims; j++)
                    {
                        points[i].Points[j] = pdata[j, i];
                    }
                }
            }

            double[] x = new double[points.Length];
            double[] y = new double[points.Length];

            for (int i = 0; i < points.Length; i++)
            {
                x[i] = points[i].Points[0];
                y[i] = points[i].Points[1];
            }

            if (points[0].Points.Length == 2)
            {
                GraphableOutput output = new GraphableOutput();
                output.XData = x;
                output.YData = new double[][] { y };
                return new object[] { output };
            }

            return new object[]{points};
        }

        public string[] OutputDescription
        {
            get { return new string[] { "N-Dimensional Plot" }; }
        }

        #endregion
    }
}
