﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters.Matlab;
using CeresBase.UI;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Interval")]
    class IntervalFastFourier : IBatchFlowable
    {
        public override string ToString()
        {
            return "Interval Power";
        }

        public bool SuppportsMultiple
        {
            get { return true; }
        }



        public static double[][] RunFastFourier(int numbins, float minfreq, float maxfreq, float[] toPass)
        {
           
            AllMatlabNative.All all = MatlabLoader.MatlabFunctions;
            object[] total = null;
            lock (MatlabLoader.MatlabLockable)
            {
                //FFTofIntervals(data,interpolationRate, maxFreq)
                total = all.FFTofIntervals(2, toPass, 4, maxfreq);
            }

            double[,] freqA = total[0] as double[,]; //(total[1] as MWNumericArray).ToArray() as double[,];
            double[,] fftA = total[1] as double[,];//(total[0] as MWNumericArray).ToArray(MWArrayComponent.Real) as double[,];

            

            double step = freqA[1, 0] - freqA[0, 0];
            double amountPerBin = (maxfreq - minfreq) / numbins;
            //   int numStepsPerBin = (int)(amountPerBin / step);

            int curCount = 0;
            double curVal = 0;

            List<double> x = new List<double>();
            List<double> y = new List<double>();
            for (int i = 0; i < freqA.GetLength(0); i++)
            {
                float what = (float)Math.Round(freqA[i, 0], 5);

                if (what >= minfreq && what <= maxfreq)
                {
                    if (x.Count == 0)
                    {
                        x.Add(freqA[i,0]);
                    }

                    curVal += fftA[i,0];

                    if (i < freqA.GetLength(0) - 1 && freqA[i + 1,0] > x[x.Count - 1] + amountPerBin)
                    {
                        y.Add(curVal);
                        x.Add(freqA[i,0]);
                        curVal = 0;
                    }
                }
                else if (freqA[i,0] >= maxfreq)
                {
                    y.Add(curVal);
                    break;
                }
            }

            if (y.Count != x.Count)
            {
                x.RemoveAt(x.Count - 1);
            }

            //if (normal)
            //{
            double sum = 0;
            for (int i = 0; i < y.Count; i++)
            {
                sum += y[i];
            }

            for (int i = 0; i < y.Count; i++)
            {
                y[i] /= sum;
            }
            //}


            return new double[][] { y.ToArray(), x.ToArray() };
        }



        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            //req.Grid.SetInformationToCollect(
            //    new Type[] { typeof(int), typeof(float), typeof(float), typeof(bool) },
            //    new string[] { "FF", "Range", "Range", "Normalize" },
            //    new string[] { "Num Bins", "Min Freq", "Max Freq", "Normalize" },
            //    new string[] { "Number of bins", "Minium (maybe) frequency", "Maximum (maybe) frequency", "Normalize to 1" },
            //    null, new object[] { 512, .1, 15, true });

            BatchProcessableParameter numbins = new BatchProcessableParameter()
            {
                Name = "Number Bins",
                Val = 512
            };
            BatchProcessableParameter minfreq = new BatchProcessableParameter()
            {
                Name = "Minimum Frequency",
                Val = .1,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };
            BatchProcessableParameter maxfreq = new BatchProcessableParameter()
            {
                Name = "Maximum Frequency",
                Val = 15.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = minfreq,
                    Path = "Val",
                    Operator = MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };

            BatchProcessableParameter label = new BatchProcessableParameter() { Name = "Label", Val = "Enter Here" };
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                Name = "Data",
                Editable = false,
                CanLinkFrom = false,
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator()
            };

            BatchProcessableParameter ranges = new BatchProcessableParameter() { Name = "Ranges", Val = "" };

            return new BatchProcessableParameter[] { numbins, minfreq, maxfreq, ranges, data, label };



        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            BatchProcessableParameter numbinsP = parameters.Single(p => p.Name == "Number Bins");
            BatchProcessableParameter minfreqP = parameters.Single(p => p.Name == "Minimum Frequency");
            BatchProcessableParameter maxfreqP = parameters.Single(p => p.Name == "Maximum Frequency");
            BatchProcessableParameter labelP = parameters.Single(p => p.Name == "Label");
            BatchProcessableParameter dataP = parameters.Single(p => p.Name == "Data");
            BatchProcessableParameter rangesP = parameters.Single(p => p.Name == "Ranges");

            int numbins = Convert.ToInt32(numbinsP.Val);
            double minfreq = Convert.ToDouble(minfreqP.Val);
            double maxfreq = Convert.ToDouble(maxfreqP.Val);
            string label = labelP.Val as string;
            string ranges = rangesP.Val as string;

            float[] data = dataP.Val as float[];

            

            double[][] results = RunFastFourier(numbins, (float)minfreq, (float)maxfreq, data);

            string allSums = "";
            string[] rangeParts = ranges.Split(',');
            for (int i = 0; i < rangeParts.Length; i++)
            {
                string[] minmax = rangeParts[i].Split('-');
                float min = float.Parse(minmax[0]);
                float max = float.Parse(minmax[1]);

                double sum = 0;
                for (int j = 0; j < results[1].Length; j++)
                {
                    if (results[1][j] >= min && results[1][j] <= max)
                    {
                        sum += results[0][j];
                    }
                }

                allSums += "Sum " + min.ToString("F2") + "-" + max.ToString("F2") + " : " + sum.ToString("F2") + " ";
            }

            GraphableOutput output = new GraphableOutput()
            {
                SourceDescription = "Fast Fourier",
                Label = label,
                XData = results[1],
                XLabel = "Frequency",
                XUnits = "Hz",
                YLabel = "Normalized Power",
                YData = new double[1][] { results[0] },
                ExtraInfo = new object[]{allSums}
            };
            return new object[] { output };
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Graph of Power Spectrum" }; }
        }

        #endregion
    }
}
