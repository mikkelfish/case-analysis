﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Raw")]
    public class RawDataAdapter : DataAdapter
    {
        public override string Category
        {
            get
            {
                return "Data Sources";
            }
            set
            {
                
            }
        }

        public override string Name
        {
            get
            {
                return "Raw Data Source";
            }
            set
            {
                
            }
        }

        private SpikeVariable var;
        public SpikeVariable Variable
        {
            get
            {
                return this.var;
            }
            set
            {
                this.var = value;
                if (this.var != null)
                {
                    this.VariableName = this.var.Header.Description;
                    this.ExperimentName = this.var.Header.ExperimentName;
                    this.Resolution = this.var.Resolution;
                }
            }
        }


        [AssociateWithProperty("Experiment Name", IsOutput=true)]
        public string ExperimentName { get; private set; }

        [AssociateWithProperty("Variable Name", IsOutput=true)]
        public string VariableName { get; private set; }

        [AssociateWithProperty("Data", IsOutput=true)]
        public float[] Data 
        {
            get
            {
                if (this.data == null && this.Variable != null)
                {
                    this.data = this.Variable.GetTimeSeriesData(this.StartTime, this.EndTime);
                }

                return this.data;
            }            
        }

        [AssociateWithProperty("Resolution", ReadOnly=true, IsOutput=true)]
        public double Resolution { get; private set; }

        [AssociateWithProperty("Start Time", ReadOnly=true, IsOutput=true)]
        public float StartTime { get; set; }

        [AssociateWithProperty("End Time", ReadOnly=true, IsOutput=true)]
        public float EndTime { get; set; }

        [AssociateWithProperty("Epoch Desc.", ReadOnly = true, IsOutput = true)]
        public string EpochDescription
        {
            get;
            private set;
        }


        public Epoch Epoch
        {
            set
            {
                if (value != null)
                {
                    this.Variable = value.Variable as SpikeVariable;
                    this.StartTime = value.BeginTime;
                    this.EndTime = value.EndTime;
                    this.EpochDescription = value.FullDescription;
                }
            }
        }

        private float[] data;

        //public override bool CanReceivePropertyLinks(string targetPropertyname)
        //{
        //    if (targetPropertyname == "Variable Name" || targetPropertyname == "Start Time" ||
        //        targetPropertyname == "End Time" || targetPropertyname == "Resolution" || targetPropertyname == "Data" || targetPropertyname == "Experiment Name"
        //        ) return false;
        //    return base.CanReceivePropertyLinks(targetPropertyname);
        //}

        //public override bool CanEditProperty(string targetPropertyname)
        //{
        //    if (targetPropertyname == "Variable Name" || targetPropertyname == "Start Time" ||
        //        targetPropertyname == "End Time" || targetPropertyname == "Resolution" || targetPropertyname == "Data" || targetPropertyname == "Experiment Name"
        //        )
        //        return false;


        //    return base.CanEditProperty(targetPropertyname);
        //}

        //public override bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        //{
        //    if (targetPropertyName == "Variable Name" && propertyValue is string)
        //        return true;
        //    if (targetPropertyName == "Resolution" && propertyValue is double)
        //        return true;
        //    if (targetPropertyName == "Start Time" && propertyValue is float)
        //        return true;
        //    if (targetPropertyName == "End Time" && propertyValue is float)
        //        return true;
        //    if (targetPropertyName == "Experiment Name" && propertyValue is string)
        //        return true;
        //    if (targetPropertyName == "Descriptor Type") return true;

        //    if (targetPropertyName == "Data") return false;


        //    return base.CanReceivePropertyValue(propertyValue, targetPropertyName);
        //}

         

        //public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        //{
        //    if (targetPropertyName == "Variable Name")
        //        this.VariableName = propertyValue as string;
        //    else if (targetPropertyName == "Resolution")
        //        this.Resolution = (double)propertyValue;
        //    else if (targetPropertyName == "Start Time")
        //        this.StartTime = (float)propertyValue;
        //    else if (targetPropertyName == "End Time")
        //        this.EndTime = (float)propertyValue;
        //    else if (targetPropertyName == "Experiment Name")
        //        this.ExperimentName = propertyValue as string;
        //    else if (targetPropertyName == "Descriptor Type")
        //        this.DescriptorType = propertyValue;

        //    base.ReceivePropertyValue(propertyValue, targetPropertyName);
        //}

        //public override object SupplyPropertyValue(string sourcePropertyName)
        //{
        //    if (sourcePropertyName == "Variable Name")
        //        return this.VariableName;
        //    else if (sourcePropertyName == "Resolution")
        //        return this.Resolution;
        //    else if (sourcePropertyName == "Start Time")
        //        return this.StartTime;
        //    else if (sourcePropertyName == "End Time")
        //        return this.EndTime;
        //    else if (sourcePropertyName == "Data")
        //    {
        //        if (this.data == null && this.var != null)
        //        {
        //            this.data = this.var.GetTimeSeriesData(this.StartTime, this.EndTime); //SpikeVariable.ReadData(this.var, this.StartTime, this.EndTime);
        //        }
        //        return this.data;
        //    }
        //    else if (sourcePropertyName == "Experiment Name")
        //        return this.ExperimentName;
        //    else if (sourcePropertyName == "Descriptor Type")
        //        return this.DescriptorType;

        //    return base.SupplyPropertyValue(sourcePropertyName);
        //}

        //public override string[] SupplyPropertyNames()
        //{
        //    List<string> names = new List<string>();
        //    names.Add("Data");
        //    names.Add("Variable Name");
        //    names.Add("Resolution");
        //    names.Add("Start Time");
        //    names.Add("End Time");
        //    names.Add("Experiment Name");

        //    string[] baseNames = base.SupplyPropertyNames();
        //    if (baseNames != null)
        //        names.AddRange(baseNames);

           

        //    return names.ToArray();
        //}

        //public override bool ShouldSerialize(string targetPropertyname)
        //{
        //    if (targetPropertyname == "Variable Name" || targetPropertyname == "Start Time" ||
        //        targetPropertyname == "End Time" || targetPropertyname == "Resolution" || targetPropertyname == "Data" || targetPropertyname == "Experiment Name"
        //        )
        //        return true;
        //    return base.ShouldSerialize(targetPropertyname);
        //}


        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public override object Clone()
        {
            RawDataAdapter adapter = new RawDataAdapter();

            this.cloneHelper(adapter);

            adapter.Variable = this.Variable;
            adapter.StartTime = this.StartTime;
            adapter.EndTime = this.EndTime;
            
            return adapter;
        }
    }
}
