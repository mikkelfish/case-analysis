//using System;
//using System.Collections.Generic;
//using System.Text;
//using ApolloFinal.Tools.BatchProcessing;
//using CeresBase.Data;
//using CeresBase.MathConstructs;

//namespace ApolloFinal.Tools.BatchProcessable
//{
//    class RandomVar : IBatchProcessable
//    {
//        #region IBatchProcessable Members

//        public bool SuppportsMultiple
//        {
//            get { return false; }
//        }

//        public object[] GetArguments(SpikeVariable[] varsToUse, out string[] names)
//        {
//            names = new string[] { "Random Var" };
//            return new object[] { 10000, 0.0 };
//        }

//        public object[] Process(SpikeVariable[] varsToUse, object[] args)
//        {
//            int numPts = (int)args[0];
//            double mean = (double)args[1];

//            //Random rand = new Random();
//            RealRandom rand = new RealRandom();
//            IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { numPts });
//            float[] pts = new float[numPts];

//            float running = 0;
//            for (int i = 0; i < numPts; i++)
//            {
//                float next = (float)(rand.NextDouble() - (0.5-mean));
//                pts[i] = next + running;
//               // running += next;
//            }
//            pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(pts));
//            SpikeVariable ran = new SpikeVariable(new SpikeVariableHeader("Random Var " + mean, "none", "ra", 1, SpikeVariableHeader.SpikeType.Raw));
//            DataFrame frame = new DataFrame(ran, pool, new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless);
//            ran.AddDataFrame(frame);
//            return new object[] { ran };
//        }

//        public Type OutputType
//        {
//            get { return typeof(SpikeVariable); }
//        }

//        #endregion

//        #region ICloneable Members

//        public object Clone()
//        {
//            return new RandomVar();
//        }

//        #endregion
//    }
//}
