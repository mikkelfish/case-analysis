using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.UI;
using ApolloFinal.Tools.AttractorReconstruction;
using System.Collections;
using CeresBase.Data;
using CeresBase.Projects.Flowable;
using CeresBase.Projects;
using System.Linq;
using CeresBase.FlowControl.Adapters;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Raw")]    
    class CorrelationDimension : IBatchFlowable
    {
        #region IBatchProcessable Members

        public bool SuppportsMultiple
        {
            get { return false; }
        }

        private double[] fastestCorrelation(NPoint[] points, double minr, double maxr, int numRSteps, int w)
        {
            double[] counts = new double[numRSteps];
           // float rStep = (maxr - minr) / (float)numRSteps;
            double rStepLg = (maxr - minr) / (double)numRSteps;
            double rStepReciprical = 1.0 / rStepLg;


            //C(r,N,W) = 2/N^2 Sig n=W -> N Sig i=1 -> N -n (H(r-|(Vi+n -Vi)|)
            for (int n = w; n < points.Length; n++)
            {
                for (int i = 0; i < points.Length - n; i++)
                {
                    double dist = points[i + n].CalcDist(points[i]);

                    dist = Math.Log(dist, 2.0);

                    int targetIndex = (int)((dist - minr) * rStepReciprical) + 1;
                    if (targetIndex >= counts.Length) continue;

                    if (targetIndex < 0) targetIndex = 0;

                    counts[targetIndex]++;
                }
            }

            double[] toRet = new double[numRSteps];
            double total = 0;
            double mult = 2.0 / ((double)points.Length * (double)points.Length);
            for (int i = 0; i < numRSteps; i++)
            {
                total += counts[i];
                toRet[i] = total * mult;
            }
            return toRet;

        }

        private double[][] finalCorrelation(NPoint[] points, int numVars, int minM, int maxM, double minr, double maxr, int numRSteps, int w)
        {
            int numM = maxM - minM + 1;
            double[,] counts = new double[numM, numRSteps];
            double rStepLg = (maxr - minr) / (double)numRSteps;
            double rStepReciprical = 1.0 / rStepLg;
            for (int n = w; n < points.Length; n++)
            {
                for (int i = 0; i < points.Length - n; i++)
                {
                    double squaredDist = 0;


                    for (int m = 0; m < minM - 1; m++)
                    {
                        for (int v = 0; v < numVars; v++)
                        {
                            squaredDist += (points[i + n].Points[numVars * m + v] - points[i].Points[numVars * m + v]) *
                                (points[i + n].Points[numVars * m + v] - points[i].Points[numVars * m + v]);                            
                        }
                    }

                    for (int m = minM -1, which = 0; m < maxM; m++, which++)
                    {
                        for (int v = 0; v < numVars; v++)
                        {
                            squaredDist += (points[i + n].Points[numVars * m + v] - points[i].Points[numVars * m + v]) *
                                (points[i + n].Points[numVars * m + v] - points[i].Points[numVars * m + v]);                            
                        }

                        double dist = Math.Log(squaredDist, 4.0);                        
                        int targetIndex = (int)((dist - minr) * rStepReciprical) + 1;
                        if (targetIndex >= numRSteps)
                            break;
                        if (targetIndex < 0) targetIndex = 0;

                        counts[which, targetIndex]++;
                    }                    
                }
            }

            double[][] toRet = new double[numM][];
            double mult = 2.0 / ((double)points.Length * (double)points.Length);

            for (int i = 0; i < numM; i++)
            {
                toRet[i] = new double[numRSteps];
                double total = 0;
                for (int j = 0; j < numRSteps; j++)
                {
                    total += counts[i,j];
                    toRet[i][j] = total * mult;
                }
            }
            return toRet;

        }


       
        private object[][] runCorrelations(float[][] dataValues, int minm, int maxm, int[] timeDelay, float minLg, float maxLg, int w)
        {


            int numSteps = 100;

            double[][] results = null;
            //bool oldway = false;
            //if (!oldway)
            //{
                NPoint[] points = AttractorReconstruction.AttractorReconstruction.CreateMultiTimeEmbedding(dataValues, maxm, timeDelay);
                results = this.finalCorrelation(points, dataValues.Length, minm, maxm, minLg, maxLg, numSteps, w);
                for (int i = 0; i < results.Length; i++)
                {
                    for (int j = 0; j < results[i].Length; j++)
                    {
                        if (results[i][j] == 0)
                        {
                            int toAdd = 1;
                            while (results[i][j + toAdd] == 0)
                            {
                                toAdd++;
                            }
                            results[i][j] = results[i][j + toAdd];
                        }
                        results[i][j] = Math.Log(results[i][j], 2.0);
                    }
                }
         //   }
            //else
            //{
            //    results = new double[maxm - minm + 1][];
            //    for (int i = 0; i <= maxm - minm; i++)
            //    {
            //        NPoint[] points = AttractorReconstruction.AttractorReconstruction.CreateMultiTimeEmbedding(dataValues, minm + i, timeDelay);
            //        results[i] = this.fastestCorrelation(points, minLg, maxLg, numSteps, w);
            //      //  results[i] = this.yACorrelation(points, minLg, maxLg, numSteps, w);
            //        // GC.Collect();

            //        for (int j = 0; j < results[i].Length; j++)
            //        {
            //            if (results[i][j] == 0)
            //            {
            //                int toAdd = 1;
            //                while (results[i][j + toAdd] == 0)
            //                {
            //                    toAdd++;
            //                }
            //                results[i][j] = results[i][j + toAdd];
            //            }
            //            results[i][j] = Math.Log(results[i][j], 2.0);
            //        }
            //        // results[i] = this.correlationIntegrals(points, (float)var.Min, (float)var.Max, .003F, maxR, 100, w);
            //    }
            //}


            object[][] toRet = new object[4][];
            for (int pass = 0; pass <= 3; pass++)
            {
                toRet[pass] = new object[2];

                double[,] yValues = null;
                double[] xValues = new double[results[results.Length - 1].Length];


                double curR = minLg;
                //float rStep = (maxR - minR) / (float)numSteps;
                double rStep = (maxLg - minLg) / (double)numSteps;
                for (int i = 0; i < xValues.Length; i++)
                {
                    xValues[i] = curR;

                    curR += rStep;
                }

                int numHSteps = 1;
                double[] xdervs = Differentiate.Derivative(xValues, rStep, numHSteps);


                if (pass == 0)
                {
                    for (int i = 0; i < results.GetLength(0); i++)
                    {
                        double[] theseRes = Differentiate.Derivative(results[i], rStep, numHSteps);
                        if (yValues == null)
                        {
                            yValues = new double[maxm - minm + 1, theseRes.Length];
                            double[] realXVals = new double[theseRes.Length];
                            for (int j = 0; j < theseRes.Length; j++)
                            {
                                realXVals[j] = xValues[j + numHSteps];//Math.Log(,2);
                            }
                            xValues = realXVals;
                        }

                        for (int j = 0; j < xdervs.Length; j++)
                        {
                            yValues[i, j] = theseRes[j] / xdervs[j];
                        }
                    }
                }
                else if (pass == 1)
                {
                    yValues = new double[maxm - minm + 1, xValues.Length];
                    for (int i = 0; i < xValues.Length; i++)
                    {
                        // xValues[i] = Math.Log(xValues[i]);
                        for (int j = 0; j < results.GetLength(0); j++)
                        {
                            yValues[j, i] = results[j][i];
                        }
                    }
                }
                else if (pass == 2)
                {
                    for (int i = 0; i < results.GetLength(0); i++)
                    {
                        double[] theseRes = Differentiate.Derivative(results[i], rStep, numHSteps);
                        for (int j = 0; j < theseRes.Length; j++)
                        {
                            theseRes[j] /= xdervs[j];
                        }

                        theseRes = Differentiate.Derivative(theseRes, rStep, numHSteps);
                        if (yValues == null)
                        {
                            yValues = new double[maxm - minm + 1, theseRes.Length];
                            double[] realXVals = new double[theseRes.Length];
                            for (int j = 0; j < theseRes.Length; j++)
                            {
                                realXVals[j] = xValues[j + numHSteps * 2];//Math.Log(,2);
                            }
                            xValues = realXVals;
                        }

                        for (int j = 0; j < theseRes.Length; j++)
                        {
                            yValues[i, j] = theseRes[j];
                        }
                    }
                }
                else
                {

                    double threshold = 10.0;
                    double[,] dervs = (double[,])toRet[2][1];
                    double[,] realVals = (double[,])toRet[0][1];

                    int last = dervs.GetLength(0) - 1;
                    int numPoints = dervs.GetLength(1);

                    double[,] flats = new double[1,dervs.GetLength(0)];


                    double sum = 0;
                    int numValidPts = 0;
                    int inarow = 0;
                    int firstValidPt = -1;
                    double keepSum = 0;
                    int keepValidPts = 0;
                    for (int i = numPoints-1; i >= 0; i--)
                    {
                        if (dervs[last, i] == 0) continue;

                        if (inarow > 10 && Math.Abs(dervs[last, i]) > threshold)
                        {
                            keepSum = sum;
                            keepValidPts = numValidPts;
                        }
                        else if (Math.Abs(dervs[last, i]) < threshold)
                        {
                            sum += dervs[last, i];
                            numValidPts++;
                            inarow++;
                            if (firstValidPt == -1)
                                firstValidPt = i;
                        }
                        else
                        {
                            inarow = 0;
                            firstValidPt = -1;
                        }
                    }

                    keepSum/= keepValidPts;
                    keepSum = Math.Abs(keepSum);
                   // sum /= 3.0;

                    for (int which = 0; which < dervs.GetLength(0); which++)
                    {
                        List<int> greatest = null;
                        List<List<int>> regions = new List<List<int>>();
                        bool first = false;
                        bool isZero = false;
                        for (int i = numPoints-1; i >= 0; i--)
                        {
                            if (!isZero && Math.Abs(dervs[which, i]) < sum)
                            {
                                if (!first)
                                {
                                    regions.Add(new List<int>());
                                    first = true;
                                }
                                if(!isZero) regions[regions.Count - 1].Add(i);

                                if (dervs[which, i] == 0)
                                {
                                    isZero = true;
                                }
                                else isZero = false;

                                if (greatest == null)
                                {
                                    greatest = regions[regions.Count - 1];
                                }
                                else if (regions[regions.Count - 1].Count >= 6)
                                {
                                    greatest = regions[regions.Count - 1];
                                }
                            }
                            else
                            {
                                first = false;
                                isZero = false;
                            }
                        }

                        if (greatest == null)
                            greatest = new List<int>();

                        double val = 0;
                        for (int i = 0; i < greatest.Count; i++)
                        {
                            val += realVals[which,greatest[i]+2];
                        }
                        val /= greatest.Count;
                        flats[0,which] = val;
                    }
                    yValues = flats;
                    xValues = new double[yValues.Length];
                    for (int i = 0; i < xValues.Length; i++)
                    {
                        xValues[i] = i;
                    }


                }

                toRet[pass][0] = xValues;
                toRet[pass][1] = yValues;
            }

            return toRet;
        }

     

        public override string ToString()
        {
            return "Correlation Dimension";
        }

        #endregion

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter minR = new BatchProcessableParameter() 
            { 
                Name = "Min R.", 
                Val = -4.0 
            };

            BatchProcessableParameter maxR = new BatchProcessableParameter() 
            { 
                Name = "Max R.", 
                Val = 4.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=minR,
                    Path="Val",
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter minDim = new BatchProcessableParameter()
            {
                Name = "Min Dimension",
                Val = 3,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=0,
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }

            };
            BatchProcessableParameter maxDim = new BatchProcessableParameter()
            {
                Name = "Max Dimension",
                Val = 10,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=minDim,
                    Path="Val",
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThanEqual,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter tau = new BatchProcessableParameter()
            {
                Name = "Tau",
                Val = 10,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=0,
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter theiler = new BatchProcessableParameter()
            {
                Name = "Theiler Window",
                Val = 40,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=tau,
                    Path="Val",
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };

            BatchProcessableParameter label = new BatchProcessableParameter() 
            { 
                Name = "Label", 
                Val = "Enter Here"
            };
            BatchProcessableParameter data = new BatchProcessableParameter() 
            { 
                Name = "Data", 
                Editable=false, 
                CanLinkFrom=false,
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                TargetType = typeof(float[])
            };


            return new BatchProcessableParameter[] { minR, maxR, minDim, maxDim, tau, theiler, data, label };

        }



        public object[] Run(BatchProcessableParameter[] parameters)
        {

            BatchProcessableParameter minR = parameters.Single(p => p.Name == "Min R.");
            BatchProcessableParameter maxR = parameters.Single(p => p.Name == "Max R.");
            BatchProcessableParameter minDim = parameters.Single(p => p.Name == "Min Dimension");
            BatchProcessableParameter maxDim = parameters.Single(p => p.Name == "Max Dimension");
            BatchProcessableParameter tau = parameters.Single(p => p.Name == "Tau");
            BatchProcessableParameter theiler = parameters.Single(p => p.Name == "Theiler Window");
            BatchProcessableParameter dataP = parameters.Single(p => p.Name == "Data");
            BatchProcessableParameter labelP = parameters.Single(p => p.Name == "Label");



            float[][] data = new float[1][];
            data[0] = dataP.Val as float[];

            object[][] res = this.runCorrelations(data, Convert.ToInt32(minDim.Val), 
                Convert.ToInt32(maxDim.Val), 
                    new int[]{Convert.ToInt32(tau.Val)}, 
                    Convert.ToSingle(minR.Val), 
                    Convert.ToSingle(maxR.Val), 
                    Convert.ToInt32(theiler.Val));

            GraphableOutput output = new GraphableOutput();
            output.Label = labelP.Val as string;
            output.SourceDescription = "Correlation Dimension";
            output.XLabel = "Lg Radius";
            output.YLabel = "Derivative of Correlation Curve";

            List<string> seriesLabels = new List<string>();
            for (int i = Convert.ToInt32(minDim.Val); i <= Convert.ToInt32(maxDim.Val); i++)
            {
                seriesLabels.Add("Dim " + i);
            }
            output.SeriesLabels = seriesLabels.ToArray();

            object[] first = res[0] as object[];
            output.XData = first[0] as double[];
            double[,] y = first[1] as double[,];
            output.YData = new double[y.GetLength(0)][];
            for (int i = 0; i < output.YData.Length; i++)
            {
                output.YData[i] = new double[y.GetLength(1)];
                for (int j = 0; j < y.GetLength(1); j++)
                {
                    output.YData[i][j] = y[i, j];
                }
            }

            return new object[]{output};
        }

      

        #endregion

        #region IBatchFlowable Members


        public string[] OutputDescription
        {
            get { return new string[] { "Graph of CD Curves" }; }
        }

        #endregion
    }
}
