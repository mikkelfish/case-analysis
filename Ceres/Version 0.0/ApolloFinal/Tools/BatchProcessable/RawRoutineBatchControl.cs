﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl;
using CeresBase.FlowControl.Adapters;
using CeresBase.Data;
using CeresBase.Projects;
using MesosoftCommon.DataStructures;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters.Matlab;
using ApolloFinal.Tools.IntervalTools;

namespace ApolloFinal.Tools.BatchProcessable
{
    public class RawRoutineBatchControl : IRoutineController
    {
        public RawRoutineBatchControl(RoutineBatchControl control)
        {
            this.BatchControl = control;
        }

        public BeginInvokeObservableCollection<Epoch> EpochList
        {
            get
            {
                return EpochCentral.BindToManager(this.over.Dispatcher);
            }
        }
        public ObservableCollection<Variable> VariableList
        {
            get
            {
                return VariableSource.Variables;
            }
        }

        private class VariablePackage : IComparable
        {
            public SpikeVariable Variable { get; set; }
            public float BeginTime { get; set; }
            public float EndTime { get; set; }

            public override string ToString()
            {
                if (this.Variable == null) return "ERROR";
                return Variable.ToString() + "Time from : " + BeginTime.ToString() + " to : " + EndTime.ToString();
            }

            public override bool Equals(object obj)
            {
                if (obj == null || !(obj is VariablePackage)) return false;

                VariablePackage other = obj as VariablePackage;
                return other.ToString() == this.ToString();
            }

            public override int GetHashCode()
            {
                return this.ToString().GetHashCode();
            }

            #region IComparable Members

            public int CompareTo(object obj)
            {
                if (obj == null) return 0;
                return this.ToString().CompareTo(obj.ToString());
            }

            #endregion
        }

        private class EpochPackage : IComparable
        {
            public Epoch Epoch { get; set; }

            public override string ToString()
            {
                if (this.Epoch == null) return "ERROR";
                return this.Epoch.ToString();
            }

            //public override bool Equals(object obj)
            //{
            //    if (obj == null || !(obj is EpochPackage)) return false;

            //    EpochPackage other = obj as EpochPackage;
            //    return other.ToString() == this.ToString();
            //}

            //public override int GetHashCode()
            //{
            //    return this.ToString().GetHashCode();
            //}

            #region IComparable Members

            public int CompareTo(object obj)
            {
                if (obj == null) return 0;
                return this.ToString().CompareTo(obj.ToString());
            }

            #endregion
        }

 
        private RoutineBatchControlOverride over;

  
        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            if (e.Item is Epoch)
            {
                Epoch eEpoch = e.Item as Epoch;
                if (eEpoch.BeginTime == eEpoch.EndTime || eEpoch.AppliesToExperiment == true)
                    e.Accepted = false;
            }
            else
            {
                e.Accepted = true;
            }
        }

        #region IRoutineController Members

        public FrameworkElement GetContentOverride()
        {
            this.over = new RoutineBatchControlOverride();

            CollectionViewSource epochsource = new CollectionViewSource();
            epochsource.Source = this.EpochList;
            epochsource.Filter += new FilterEventHandler(this.CollectionViewSource_Filter);
            over.EpochListDisplay.ItemsSource = epochsource.View;

            CollectionViewSource varsource = new CollectionViewSource();
            varsource.Source = this.VariableList;
            varsource.Filter += new FilterEventHandler(varsource_Filter);
            over.VariableListDisplay.ItemsSource = varsource.View;
            return over.Content as FrameworkElement;
        }

        void varsource_Filter(object sender, FilterEventArgs e)
        {
            SpikeVariable var = e.Item as SpikeVariable;
            if (var.SpikeType == SpikeVariableHeader.SpikeType.Pulse)
            {
                e.Accepted = false;
            }
            else e.Accepted = true;
        }

        public void AdaptersInitialized(Routine routine, AdapterInitPair[] pairs)
        {
            var sortedAdapters = from AdapterInitPair p in pairs
                                 orderby p.Adapter.ID
                                 select p;

            foreach (AdapterInitPair adapter in sortedAdapters)
            {
                if (adapter.Adapter is RawDataAdapter)
                {
                    RawDataAdapter desc = adapter.Adapter as RawDataAdapter;

                    if (routine.Args is VariablePackage)
                    {
                        VariablePackage package = routine.Args as VariablePackage;

                        desc.Variable = package.Variable;
                        desc.StartTime = package.BeginTime;
                        desc.EndTime = package.EndTime;

                    }
                    else if (routine.Args is EpochPackage)
                    {
                        desc.Epoch = (routine.Args as EpochPackage).Epoch;

                    }
                }
            }
        }

        public void AddAvailableAdapters(RoutineManager rm)
        {
            MatlabLoader.BuildAdapterInfos("Raw");

            MatlabAdapter[] matlabs = MatlabLoader.CreateAdapters("Raw");
            if (matlabs != null)
            {
                foreach (MatlabAdapter mlab in matlabs)
                {
                    rm.AvailableAdapters.Add(mlab);
                }
            }

            IFlowControlAdapter[] adapters = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IFlowControlAdapter>(null);
            foreach (IFlowControlAdapter adapter in adapters)
            {
                AdapterDescriptionAttribute[] attr = MesosoftCommon.Utilities.Reflection.Common.GetAttributes<AdapterDescriptionAttribute>(adapter);
                if (attr == null) continue;
                if (attr.Any(at => at.Description == "Raw") && !rm.AvailableAdapters.Any(ad => ad.Name == adapter.Name && ad.Category == adapter.Category))
                {
                    rm.AvailableAdapters.Add(adapter);
                }
            }

            IBatchFlowable[] flows = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IBatchFlowable>(null);
            foreach (IBatchFlowable adapter in flows)
            {
                AdapterDescriptionAttribute[] attr = MesosoftCommon.Utilities.Reflection.Common.GetAttributes<AdapterDescriptionAttribute>(adapter);
                if (attr == null) continue;
                if (attr.Any(at => at.Description == "Raw") && !rm.AvailableAdapters.Any(ad => ad.Name == adapter.ToString() && ad.Category == adapter.ToString()))
                {
                    rm.AvailableAdapters.Add(new FlowableBatchProcessorAdapter(adapter));
                }
            }
        }

        public void AddButtonClicked()
        {
            if (this.over.VariablesTab.IsSelected)
            {
                VariablePackage pack = new VariablePackage();
                pack.Variable = this.over.VariableListDisplay.SelectedItem as SpikeVariable;

                float begin = 0.0F;
                float end = 0.0F;
                bool beginOK = float.TryParse(this.over.VariableBeginTime.Text, out begin);
                bool endOK = float.TryParse(this.over.VariableEndTime.Text, out end);
                pack.BeginTime = begin;
                pack.EndTime = end;

                string errorString = "";
                if (!beginOK && !endOK)
                    errorString = "There was a problem with the formatting of both your beginning and ending times.";
                else if (!beginOK)
                    errorString = "There was a problem with the formatting of your beginning time.";
                else if (!endOK)
                    errorString = "There was a problem with the formatting of your ending time.";

                if (errorString != "")
                {
                    MessageBox.Show(errorString, "Failed to add the selected variable.", MessageBoxButton.OK, MessageBoxImage.Stop);
                    return;
                }

                this.BatchControl.ToRunList.Add(pack);
            }
            else if (this.over.EpochsTab.IsSelected)
            {
                foreach (Epoch selected in this.over.EpochListDisplay.SelectedItems)
                {
                    EpochPackage pack = new EpochPackage() { Epoch = selected };
                    this.BatchControl.ToRunList.Add(pack);
                }
            }


        }

        public RoutineBatchControl BatchControl
        {
            get;
            private set;
        }

        #endregion
    }
}
