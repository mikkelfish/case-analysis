using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.UI;

using CeresBase.Data;
using ApolloFinal.Tools.AttractorReconstruction;
using CeresBase.Projects.Flowable;
using CeresBase.Projects;
using System.Linq;
using CeresBase.FlowControl.Adapters;
using MesosoftCommon.Utilities.Validations;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Raw")]
    class FalseNearestNeighborsCreator :  IBatchFlowable
    {

        public override string ToString()
        {
            return "False Nearest Neighbors";
        }

      

        private class nearestOne
        {
            private NPoint point;
            public NPoint Point
            {
                get
                {
                    return this.point;
                }
            }
            
            public NPoint nearest;
            //public int[] checkSurroundingSquares = null;
            public float distance = float.MaxValue;

            private int[,] currentIndexes;
            public int[,] CurrentIndexes
            {
                get
                {
                    return this.currentIndexes;
                }
            }

            public nearestOne(NPoint point)
            {
                this.point = point;
                this.currentIndexes = new int[point.Points.Length, 2];
            }
        }


        private int comparePoints(NPoint point1, NPoint point2)
        {
            if (point1.Points[dimToCompare] > point2.Points[dimToCompare]) return 1;
            if (point1.Points[dimToCompare] < point2.Points[dimToCompare]) return -1;
            return 0;
        }

        private int dimToCompare = -1;

  
        private nearestOne[] findNearestChunks(NPoint[] points, double variance, double min, double max)
        {
            if (points == null || points.Length == 0) return null;
            int targetPoints = 1000;
            int numDims = points[0].Points.Length - 1;

            double ratio = (double)points.Length / (double)targetPoints;
            int splitDims = (int)Math.Round(Math.Pow(ratio, 1.0D / numDims), 0);

            List<NPoint>[] chopped = new List<NPoint>[(int)Math.Pow(splitDims, numDims)];
            for (int i = 0; i < chopped.Length; i++)
            {
                chopped[i] = new List<NPoint>();
            }
            foreach (NPoint point in points)
            {
                int index = 0;
                int mult = 1;
                for (int i = 0; i < numDims; i++, mult *= splitDims)
                {

                    double perc = (point.Points[i] - min) / (max - min);
                    int dimIndex = (int)(perc * splitDims);
                    if (dimIndex == splitDims) dimIndex--;
                    index += mult * dimIndex;

                }
                chopped[index].Add(point);
            }

            nearestOne[] nearestA = new nearestOne[points.Length];
            int nearIndex = 0;
            for (int i = 0; i < chopped.Length; i++)
            {
                for (int j = 0; j < chopped[i].Count; j++)
                {
                    nearestOne nearest = new nearestOne(chopped[i][j]);
                    //nearest.point = chopped[i][j];
                    for (int k = 0; k < chopped[i].Count; k++)
                    {
                        if (k == j) continue;
                        float dist = 0;
                        for (int dimNum = 0; dimNum < numDims; dimNum++)
                        {
                            float toAdd = chopped[i][j].Points[dimNum] - chopped[i][k].Points[dimNum];
                            dist += toAdd * toAdd;
                        }
                        if (dist < nearest.distance)
                        {
                            nearest.nearest = chopped[i][k];
                            nearest.distance = dist;
                        }
                    }
                    nearestA[nearIndex] = nearest;
                    nearIndex++;

                    //for (int k = 0; k < numDims; k++)
                    //{

                    //}

                }
            }

            return nearestA;

        }


        private nearestOne[] ANNNeighbors(NPoint[] points)
        {
            nearestOne[] toRet = new nearestOne[points.Length];
            int dim = points[0].Points.Length - 1;

            double[,] pts = new double[points.Length, dim];
            for (int i = 0; i < points.Length; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    pts[i, j] = points[i].Points[j];
                }
            }

            int[] nearest = ANNWrapper.ANNWrapper.FindNearestNeighbors(pts, 0);
            for (int i = 0; i < points.Length; i++)
            {
                toRet[i] = new nearestOne(points[i]);
                toRet[i].nearest = points[nearest[i]];

                float dist = 0;
                for (int j = 0; j < dim; j++)
                {
                    dist += (points[i].Points[j] - points[nearest[i]].Points[j]) * (points[i].Points[j] - points[nearest[i]].Points[j]);
                }
                toRet[i].distance = dist;

                //toRet[i].distance = points[i].CalcDist(points[nearest[i]]);
            }

            return toRet;
        }

        private object[] findNearest(float[] data, object[] args)
        {
            int minDim = (int)args[0];
            int maxDim = (int)args[1];
            int minTau = (int)args[2];
            int maxTau = (int)args[3];
            int tauD = (int)args[6];

            int rTol = 10;
            int aTol = 10;
            double variance = Sharp3D.Math.Core.MathFunctions.Variance(data);

            List<List<float>> ydata = new List<List<float>>();
            List<float> xdata = new List<float>();
            for (int i = minDim; i <= maxDim; i++)
            {
                xdata.Add(i);
                ydata.Add(new List<float>());

                for (int j = minTau; j <= maxTau; j += tauD)
                {
                    NPoint[] points = AttractorReconstruction.AttractorReconstruction.CreateTimeEmbedding(data, i + 1, j);
                    // nearestOne[] nearest = this.findNearestChunks(points, variance, var.Min, var.Max);
                    nearestOne[] nearest = this.ANNNeighbors(points);
                    //this.findNearestNeighbors(points);

                    int totalFalse = 0;
                    foreach (nearestOne one in nearest)
                    {
                        if (one.nearest == null) continue;

                        float nextDim = (one.Point.Points[i] - one.nearest.Points[i]);
                        nextDim = nextDim * nextDim;
                        double rTest = Math.Sqrt(nextDim) / Math.Sqrt(one.distance);
                        if (rTest > rTol)
                            totalFalse++;
                        double aTest = Math.Sqrt((one.distance + nextDim)) / Math.Sqrt(variance);
                        if (aTest > aTol)
                            totalFalse++;
                    }
                    ydata[ydata.Count - 1].Add((float)totalFalse / (float)nearest.Length);
                }
            }

            return new object[] { xdata, ydata };
        }


        private object[] findNearest(float[] data, int minDim, int maxDim, int minTau, int maxTau, int tauD)
        {
            //int minDim = (int)args[0];
            //int maxDim = (int)args[1];
            //int minTau = (int)args[2];
            //int maxTau = (int)args[3];
            //int tauD = (int)args[6];

            int rTol = 10;
            int aTol = 10;
            double variance = Sharp3D.Math.Core.MathFunctions.Variance(data);

            List<List<float>> ydata = new List<List<float>>();
            List<float> xdata = new List<float>();
            for (int i = minDim; i <= maxDim; i++)
            {
                xdata.Add(i);
                ydata.Add(new List<float>());

                for (int j = minTau; j <= maxTau; j += tauD)
                {
                    NPoint[] points = AttractorReconstruction.AttractorReconstruction.CreateTimeEmbedding(data, i + 1, j);
                    // nearestOne[] nearest = this.findNearestChunks(points, variance, var.Min, var.Max);
                    nearestOne[] nearest = this.ANNNeighbors(points);
                    //this.findNearestNeighbors(points);

                    int totalFalse = 0;
                    foreach (nearestOne one in nearest)
                    {
                        if (one.nearest == null) continue;

                        float nextDim = (one.Point.Points[i] - one.nearest.Points[i]);
                        nextDim = nextDim * nextDim;
                        double rTest = Math.Sqrt(nextDim) / Math.Sqrt(one.distance);
                        if (rTest > rTol)
                            totalFalse++;
                        double aTest = Math.Sqrt((one.distance + nextDim)) / Math.Sqrt(variance);
                        if (aTest > aTol)
                            totalFalse++;
                    }
                    ydata[ydata.Count - 1].Add((float)totalFalse / (float)nearest.Length);
                }
            }

            return new object[] { xdata, ydata };
        }


   

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter mindim = new BatchProcessableParameter()
                {
                    Name = "Minimum Dimension",
                    Val = 2,
                    Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                    {
                        CompareTo=0,
                        Operator= MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                        TreatNonComparableAsSuccess=true
                    }
                };
            BatchProcessableParameter maxdim = new BatchProcessableParameter()
                {
                    Name = "Maximum Dimension",
                    Val = 4,
                    Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                    {
                        CompareTo=mindim,
                        Path="Val",
                        Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThanEqual,
                        TreatNonComparableAsSuccess=true
                    }
                };
            BatchProcessableParameter mintau = new BatchProcessableParameter()
            {
                Name = "Minimum Time Delay",
                Val = 10,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0,
                    Operator=MesosoftCommon.Utilities.Validations.ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }

            };
            BatchProcessableParameter maxtau = new BatchProcessableParameter()
            {
                Name = "Maximum Time Delay",
                Val = 35,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo=mintau,
                    Path="Val",
                    Operator=ComparisonValidator.Comparison.GreaterThanEqual,
                    TreatNonComparableAsSuccess=true
                }
            };
            BatchProcessableParameter stepSize = new BatchProcessableParameter()
            {
                Name = "Time Delay Step Size",
                Val = 5,
                Validator = new ComparisonValidator()
                {
                    CompareTo=0,
                    Operator=ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess=true
                }
            };

            BatchProcessableParameter label = new BatchProcessableParameter() { Name = "Label", Val = "Enter Here" };

            BatchProcessableParameter data = new BatchProcessableParameter() 
            { 
                Name = "Data", 
                Editable=false, 
                CanLinkFrom=false,
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator()
            };



            return new BatchProcessableParameter[] { mindim, maxdim, mintau, maxtau, stepSize, data, label };
        }



        public object[] Run(BatchProcessableParameter[] parameters)
        {
            BatchProcessableParameter minDim = parameters.Single(p => p.Name == "Minimum Dimension");
            BatchProcessableParameter maxdim = parameters.Single(p => p.Name == "Maximum Dimension");
            BatchProcessableParameter mintau = parameters.Single(p => p.Name == "Minimum Time Delay");
            BatchProcessableParameter maxtau = parameters.Single(p => p.Name == "Maximum Time Delay");
            BatchProcessableParameter stepSize = parameters.Single(p => p.Name == "Time Delay Step Size");
            BatchProcessableParameter dataP = parameters.Single(p => p.Name == "Data");
            BatchProcessableParameter labelP = parameters.Single(p => p.Name == "Label");

            int minD = (int)minDim.Val;
            int maxD = (int)maxdim.Val;
            int minT = (int)mintau.Val;
            int maxT = (int)maxtau.Val;
            int step = (int)stepSize.Val;
            float[] data = dataP.Val as float[];

            GraphableOutput output = new GraphableOutput();
            output.SourceDescription = "False Nearest Neighbors";
            output.XLabel = "Dimensions";
            output.YLabel = "False Nearest Neighbors";
            output.YUnits = "%";

            List<string> seriesLabels = new List<string>();
            for (int i = minD; i <= maxD; i++)
            {
                seriesLabels.Add("Dim " + i);
            }
            output.SeriesLabels = seriesLabels.ToArray();


            object[] calc = this.findNearest(data, minD, maxD, minT, maxT, step);

            List<float> xdata = calc[0] as List<float>;
            List<List<float>> ydata = calc[1] as List<List<float>>;
            output.XData = new double[xdata.Count];
            for (int i = 0; i < xdata.Count; i++)
            {
                output.XData[i] = xdata[i];
            }

            float[,] passY = new float[ydata[0].Count, ydata.Count];
            for (int i = 0; i < ydata[0].Count; i++)
            {
                for (int j = 0; j < ydata.Count; j++)
                {
                    passY[i, j] = ydata[j][i];
                }
            }

            output.YData = new double[passY.GetLength(0)][];
            for (int i = 0; i < output.YData.Length; i++)
            {
                output.YData[i] = new double[passY.GetLength(1)];
                for (int j = 0; j < output.YData[i].Length; j++)
                {
                    output.YData[i][j] = passY[i, j];
                }
            }

            return new object[]{output};
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Graph of FNN" }; }
        }

        #endregion
    }
}
