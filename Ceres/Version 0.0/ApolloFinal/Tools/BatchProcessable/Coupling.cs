﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using ApolloFinal.Tools.BatchProcessing;
//using System.Windows.Forms;
//using CeresBase.UI;
//using CeresBase.Data;
//using MathWorks.MATLAB.NET.Arrays;
//using System.Threading;
//using CeresBase.Projects.Flowable;
//using CeresBase.FlowControl.Adapters.Matlab;

//namespace ApolloFinal.Tools.BatchProcessable
//{
//    class Coupling : IBatchProcessable
//    {
//        #region IBatchProcessable Members

//        public bool SuppportsMultiple
//        {
//            get { return false; }
//        }

//        public object[] GetArguments(SpikeVariable[] varsToUse, out string[] name)
//        {
//            if (varsToUse.Length != 2)
//            {
//                MessageBox.Show("Coupling measurements require two variables.");
//                name = null;
//                return null;
//            }

//            List<Type> types = new List<Type>(new Type[] { typeof(float), typeof(float), typeof(bool), typeof(bool), typeof(int), 
//                typeof(int), typeof(int), typeof(int[]), typeof(bool), typeof(float), typeof(int) });
//            List<string> cats = new List<string>(new string[] { "Time", "Time", "Couple", "Couple", "TauCombo", "TauCombo", "TauCombo", "TauSurr", 
//                "Switch", "Tolerance", "M" });
//            List<string> names = new List<string>(new string[] { "Start", "End", "Surrogate", "Combo", "Tau1", "Tau2", "TauBoth", "TauSurr",
//                "Switch", "Tolerance", "M" });
//            List<string> desc = new List<string>(new string[] { "Start time", "End time", "Use surrogate shuffling method", "Use combination method",
//                "Enter a value if there is a specific tau to use for the combination", "Enter a value if there is a specific tau to use for the combination",
//                "Enter a value if there is a specific tau to use for the combination", "Enter a value if there is a specific tau to use for the surrogate series", 
//                "Couple order", "Tolerance", "M"});
//            List<object> defaults = new List<object>(new object[] { 0, -1, true, false, -1, -1, -1, new int[] { -1, -1, -1 }, false, 1F, 3 });

//            if (varsToUse[0].SpikeType != SpikeVariableHeader.SpikeType.Pulse)
//            {
//                types.Add(typeof(float));
//                cats.Add("Time");
//                names.Add("Chunk Size");
//                desc.Add("Enter the length of chunks to calculate");
//                defaults.Add(20);
//            }

//            RequestInformation request = new RequestInformation();
//            request.Grid.SetInformationToCollect(
//                types.ToArray(),
//                cats.ToArray(),
//                names.ToArray(),
//                desc.ToArray(),
//                null,
//                defaults.ToArray()
//               );

//            if (request.ShowDialog() == DialogResult.Cancel)
//            {
//                name = null;
//                return null;
//            }

//            name = new string[1];
//            name[0] = "Coupling " + varsToUse[0].FullDescription + " " + varsToUse[1].FullDescription;

//            float chunkSize = float.MaxValue;
//            if (request.Grid.Results.ContainsKey("Chunk Size"))
//                chunkSize = (float)request.Grid.Results["Chunk Size"];

//            return new object[]{request.Grid.Results["Start"], request.Grid.Results["End"],
//                request.Grid.Results["Surrogate"], request.Grid.Results["Combo"],
//                request.Grid.Results["Tau1"], request.Grid.Results["Tau2"], request.Grid.Results["TauBoth"], 
//                request.Grid.Results["TauSurr"], chunkSize,
//                request.Grid.Results["Switch"], request.Grid.Results["Tolerance"], request.Grid.Results["M"]};
//        }



//        public object[] Process(SpikeVariable[] varsToUse, object[] args)
//        {
//            float start = (float)args[0];
//            float end = (float)args[1];


//            bool surrogate = (bool)args[2];
//            bool combo = (bool)args[3];

//            int tau1 = (int)args[4];
//            int tau2 = (int)args[5];
//            int tauboth = (int)args[6];
            
//            int[] tausurr = (int[])args[7];

//            float chunkSize = (float)args[8];

//            bool switchit = (bool)args[9];
//            float tolerance = 0.1F; 
//            if(args.Length == 11) 
//                tolerance = (float)args[10];
//            int m = 3;
//            if (args.Length == 12)
//                m = (int)args[11];

//            if (chunkSize == float.MaxValue || chunkSize < 0 )
//            {
//                chunkSize = end - start;
//            }

//            MultiViewer viewer = new MultiViewer();
//            List<UI.MatlabPlot> plots = new List<ApolloFinal.UI.MatlabPlot>();

//            SpikeVariable var1 = varsToUse[0];
//            SpikeVariable var2 = varsToUse[1];

//            if (end == -1)
//            {
//                end = var1.TotalLength < var2.TotalLength ? (float)var1.TotalLength : (float)var2.TotalLength;
//            }


//            if (surrogate)
//            {
//                //Create a new variable that is the difference between the two
//                //then show mutual information to pick the tau of the new series and calculate entropy
//                //sort variable 1 and redo the difference and entropy calculation

//                if (var1.SpikeType == SpikeVariableHeader.SpikeType.Pulse)
//                {
//                    #region Calculate with pulse

//                    //If pulse, then just get the full data for both variables.
//                    float var1start = -1;
//                    float var2start = -1;

//                    List<float> var1data = new List<float>();
//                    List<float> var2data = new List<float>();

//                    //int firstIndex1 = (int)(start / var1.Resolution);
//                    //int lastIndex1 = (int)(end / var1.Resolution);
//                    var1[0].Pool.GetData(null, null,
//                        delegate(float[] data, uint[] indexes, uint[] count)
//                        {
//                            for (int i = 0; i < data.Length - 1; i++)
//                            {
//                                float val = data[i] * (float)var1.Resolution;
//                                if (val < start || val > end) continue;

//                                if (var1start == -1) var1start = val;

//                                var1data.Add((data[i + 1] - data[i]) * (float)var1.Resolution);
//                            }
//                        }
//                    );

//                    //int firstIndex2 = (int)(start / var2.Resolution);
//                    //int lastIndex2 = (int)(end / var2.Resolution);
//                    var2[0].Pool.GetData(null, null,
//                        delegate(float[] data, uint[] indexes, uint[] count)
//                        {
//                            for (int i = 0; i < data.Length - 1; i++)
//                            {
//                                float val = data[i] * (float)var2.Resolution;
//                                if (val < start || val > end) continue;

//                                if (var2start == -1) var2start = val;


//                                var2data.Add((data[i + 1] - data[i]) * (float)var2.Resolution);
//                            }
//                        }
//                    );

//                    bool switched = false;
//                    if (var1data.Count > var2data.Count)
//                    {
//                        switched = true;
//                        List<float> temp = var1data;
//                        var1data = var2data;
//                        var2data = temp;

//                        float tempStart = var1start;
//                        var1start = var2start;
//                        var2start = tempStart;
//                    }


//                    for (int whichToDo = 0; whichToDo < 3; whichToDo++)
//                    {
//                        double[] xs;
//                        double[,] res = doSurrogateStuff(m, out xs, whichToDo, switchit, true, 1.0, tausurr, var1data, var2data, var1start, var2start, tolerance);

//                        plots.Add(new ApolloFinal.UI.MatlabPlot());
//                        plots[plots.Count-1].XData = xs;
//                        plots[plots.Count - 1].YData = res;

//                        string label = "";
//                        if (whichToDo == 0)
//                        {
//                            label = "All through next trigger";
//                        }
//                        else if (whichToDo == 1)
//                        {
//                            label = "To next interval";
//                        }
//                        else label = "To previous interval";

//                        plots[plots.Count - 1].Label = label;
//                    }

                    


//                    #endregion
//                }
//                else
//                {
//                    #region Calculate with raw

//                    float curTime = start;
//                    int curIndex = (int)(curTime / var1.Resolution);
//                    int chunkIndex = (int)(chunkSize/var1.Resolution);
//                    plots.Add(new ApolloFinal.UI.MatlabPlot());

//                    int lastIndex = (int)(end / var1.Resolution);

//                    List<double> xdata = new List<double>();
//                    List<double[,]> ydata = new List<double[,]>();
//                    while (true)
//                    {
//                        float max = float.MinValue;
//                        float min = float.MaxValue;

//                        //Get the raw data for both variables
//                        List<float> var1data = new List<float>();
//                        var1[0].Pool.GetData(new int[] { curIndex }, new int[] { chunkIndex },
//                            delegate(float[] data, uint[] indices, uint[] counts)
//                            {
//                                for (int i = 0; i < data.Length; i++)
//                                {
//                                    if (data[i] < min) min = data[i];
//                                    if (data[i] > max) max = data[i];
//                                }

//                                var1data.AddRange(data);
//                            }
//                        );

//                        for (int i = 0; i < var1data.Count; i++)
//                        {
//                            var1data[i] = (var1data[i] - min) / (max - min);
//                        }

//                        max = float.MinValue;
//                        min = float.MaxValue;

//                        List<float> var2data = new List<float>();
//                        var2[0].Pool.GetData(new int[] { curIndex }, new int[] { chunkIndex },
//                            delegate(float[] data, uint[] indices, uint[] counts)
//                            {
//                                for (int i = 0; i < data.Length; i++)
//                                {
//                                    if (data[i] < min) min = data[i];
//                                    if (data[i] > max) max = data[i];
//                                }

//                                var2data.AddRange(data);
//                            }
//                       );

//                        for (int i = 0; i < var1data.Count; i++)
//                        {
//                            var2data[i] = (var2data[i] - min) / (max - min);
//                        }

//                        double[] x;
//                        double[,] res = doSurrogateStuff(m, out x, 0, switchit, false, 1.0/var1.Resolution, tausurr, var1data, var2data, 0, 0, tolerance);                       
                        
//                        xdata.Add(curIndex * var1.Resolution);
//                        ydata.Add(res);

//                        //double[] tempRes = new double[3];
//                        //for (int i = 0; i < 3; i++)
//                        //{
//                        //    tempRes[i] = res[i, 0];
//                        //}
//                        //ydata.Add(tempRes);

//                        curIndex += chunkIndex;
//                        if (curIndex >= lastIndex) break;
//                    }

//                    if (xdata.Count == 1)
//                    {
//                        xdata.Add(xdata[0] + 1);
//                        ydata.Add(ydata[0]);
//                    }

//                    double[,] ys = new double[6, ydata[0].GetLength(1)];
//                    for (int i = 0; i < ydata.Count; i++)
//                    {
//                        for (int j = 0; j < 6; j++)
//                        {
//                            for (int k = 0; k < ydata[i].GetLength(1); k++)
//                            {
//                                ys[j, k] += ydata[i][j, k] / ydata.Count;

//                            }
//                        }
//                    }

//                    plots[0].XData = xdata.ToArray();
//                    plots[0].YData = ys;

//                    #endregion
//                }
//            }

//            if (combo && var1.SpikeType != SpikeVariableHeader.SpikeType.Pulse)
//            {
//                //#region Added

//                //SampleEntropy entropy = new SampleEntropy();

//                //if (tau1 == -1)
//                //{
//                //    MutualInformation info = new MutualInformation();
//                //    UI.MatlabPlot plot = info.Process(new SpikeVariable[] { var1 }, new object[] { 100, start, end })[0] as UI.MatlabPlot;
//                //    plot.View(new IBatchViewable[] { plot });
//                //    RequestInformation getMutual = new RequestInformation();
//                //    getMutual.Grid.SetInformationToCollect(
//                //        new Type[] { typeof(int) },
//                //        new string[] { "Tau" },
//                //        new string[] { "Tau" },
//                //        new string[] { "Enter the appropriate tau value for the new series." },
//                //        null, null);

//                //    Thread.Sleep(300);


//                //    if (getMutual.ShowDialog() == DialogResult.Cancel) return null;
//                //    tau1 = (int)getMutual.Grid.Results["Tau"];
//                //}

//                //if (tau2 == -1)
//                //{
//                //    MutualInformation info = new MutualInformation();
//                //    UI.MatlabPlot plot = info.Process(new SpikeVariable[] { var2 }, new object[] { 100, start, end })[0] as UI.MatlabPlot;
//                //    plot.View(new IBatchViewable[] { plot });
//                //    RequestInformation getMutual = new RequestInformation();
//                //    getMutual.Grid.SetInformationToCollect(
//                //        new Type[] { typeof(int) },
//                //        new string[] { "Tau" },
//                //        new string[] { "Tau" },
//                //        new string[] { "Enter the appropriate tau value for the new series." },
//                //        null, null);

//                //    Thread.Sleep(300);


//                //    if (getMutual.ShowDialog() == DialogResult.Cancel) return null;
//                //    tau2 = (int)getMutual.Grid.Results["Tau"];
//                //}

//                ////int minm = (int)args[0];
//                ////int maxm = (int)args[1];
//                ////int[] timeDelay = (int[])args[2];
//                ////float maxLg = (float)args[3];
//                ////int w = (int)args[4];
//                ////float startTime = (float)args[5];
//                ////float endTime = (float)args[6];
//                ////float minLg = (float)args[7];

//                //CorrelationDimension dim1 = new CorrelationDimension();
//                //dim1.Process(new SpikeVariable[] { var1 },
//                //    new object[]
//                //    {


//                //    }
//                //);

//                ////MultiViewer view1 = entropy.Process(new SpikeVariable[] { var1 },
//                ////    new object[]{
//                ////            start, end, chunkSize, 3, .2F, tau1, false, -1F, false, 0
//                ////        })[0] as MultiViewer;

//                ////MultiViewer view2 = entropy.Process(new SpikeVariable[] { var2 },
//                ////    new object[]{
//                ////            start, end, chunkSize, 3, .2F, tau2, false, -1F, false, 0
//                ////        })[0] as MultiViewer;

//                ////MultiSampleEntropy multi = new MultiSampleEntropy();
//                ////UI.MatlabPlot combined = multi.Process(new SpikeVariable[] { var1, var2 },
//                ////    new object[]{
//                ////        start, end, chunkSize, 3, 0.2F, tau1, tau2
//                ////    }
//                ////)[0] as UI.MatlabPlot;

//                ////double[,] ydata = new double[view1.Plots[0].XData.Count, 4];
//                ////for (int i = 0; i < view1.Plots[0].XData.Count; i++)
//                ////{
//                ////    ydata[0, i] = (view1.Plots[0].YData as double[,])[2, i];
//                ////    ydata[1, i] = (view2.Plots[0].YData as double[,])[2, i];
//                ////    ydata[2, i] = (combined.YData as double[,])[2, i];

//                ////    double maxVal = ydata[0, i];
//                ////    double minVal = ydata[1, i];

//                ////    if (minVal > maxVal)
//                ////    {
//                ////        double temp = minVal;
//                ////        minVal = maxVal;
//                ////        maxVal = temp;
//                ////    }

//                ////    ydata[3, i] = 1.0 - (ydata[2, i] - maxVal) / minVal;


//                ////}
               
            
//                ////UI.MatlabPlot newplot = new ApolloFinal.UI.MatlabPlot();
//                ////newplot.XData = view1.Plots[0].XData;
//                ////newplot.YData = ydata;
//                ////plots.Add(newplot);

//                //#endregion

//                //PlottingTools.PlotUtils.AddPlots(new ApolloFinal.UI.MatlabPlot
//            }

//            viewer.Plots = plots.ToArray();
//            return new object[] { viewer };
//        }

//        private static double[,] doSurrogateStuff(int m, out double[] xs, int whichToDo, bool switchit, bool intervals, double freq, int[] tausurr, 
//            List<float> var1data, List<float> var2data, float var1start, float var2start, float tolerance)
//        {
//            float[] differences = null;
//            if (intervals)
//            {
//                differences = getDifferences(var1data.ToArray(), var2data.ToArray(), var1start, var2start, whichToDo);
//            }
//            else
//            {
//                differences = getSubtracted(var1data.ToArray(), var2data.ToArray());
//            }

//            //bool add = tausurr == -1;

//            //Now the differences have been calculated, calculate and show the mutual information
//            IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { differences.Length });
//            pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(differences));
//            SpikeVariable var = null; 
//            if(intervals)
//                var = new SpikeVariable(new SpikeVariableHeader("Throwaway", "", "throw", 1, SpikeVariableHeader.SpikeType.Raw));
//            else
//                var = new SpikeVariable(new SpikeVariableHeader("Throwaway", "", "throw", freq, SpikeVariableHeader.SpikeType.Raw));
//            DataFrame frame = new DataFrame(var, pool, new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless);
//            var.AddDataFrame(frame);

//            //if (add)
//            //    CeresBase.Data.VariableSource.AddVariable(var);

//            if (tausurr[whichToDo] == -1)
//            {

//                MutualInformation info = new MutualInformation();
//                UI.MatlabPlot plot = info.Process(new SpikeVariable[] { var }, new object[] { 100, 0F, -1F })[0] as UI.MatlabPlot;
//                plot.View(new IBatchViewable[] { plot });

//                Thread.Sleep(300);

//                RequestInformation getMutual = new RequestInformation();
//                getMutual.Grid.SetInformationToCollect(
//                    new Type[] { typeof(int) },
//                    new string[] { "Tau" },
//                    new string[] { "Tau" },
//                    new string[] { "Enter the appropriate tau value for the new series." },
//                    null, null);

//                if (getMutual.ShowDialog() == DialogResult.Cancel)
//                {
//                    xs = null;
//                    return null;
//                }
//                tausurr[whichToDo] = (int)getMutual.Grid.Results["Tau"];
//            }

//            SampleEntropy entropy = new SampleEntropy();

//            EverythingMatlab.EverythingMatlabclass suite = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;
            
//            MWNumericArray array = null;
//            float[] otherData = var2data.ToArray();

//            if (!switchit) array = new MWNumericArray(var1data.ToArray());
//            else
//            {
//                array = new MWNumericArray(var2data.ToArray());
//                otherData = var1data.ToArray();

//                float tempStart = var1start;
//                var1start = var2start;
//                var2start = tempStart;
//            }

//            int totTaus = 11;
//            if (tausurr[whichToDo] - 5 < 1)
//            {
//                totTaus -= 5 - tausurr[whichToDo] + 1;
//            }

//            totTaus = 50;
//            double[,] toRet = new double[6, totTaus];
//            int whichPass = 0;
//            int startTau = tausurr[whichToDo] - 5;
//            int endTau = tausurr[whichToDo] + 5;

//            if (!intervals)
//            {
//                startTau = tausurr[whichToDo];
//                endTau = tausurr[whichToDo];
//                toRet = new double[6, totTaus];
//            }

//            List<double> taux = new List<double>();
//            for (int tau =1; tau <= 50; tau++)
//            {
//                if (tau < 1) continue;

//                #region calc

//                MultiViewer entropyViewer = entropy.Process(new SpikeVariable[] { var },
//                        new object[]{
//                            0F, -1F, -1F, m, .2F, tau, false, -1F, 1, 0.05,1.0
//                        })[0] as MultiViewer;

//                double entropyVal = (entropyViewer.Plots[0].YData as double[,])[0, 0];
//                double entropyError1 = (entropyViewer.Plots[0].YData as double[,])[1, 0];
//                double entropyError2 = (entropyViewer.Plots[0].YData as double[,])[2, 0];


//                int numMore = 0;
//                double surrAvg = 0;

//                double[] surrEs = new double[19];


//                //Got entropy of the real coupling now do a surrogate and repeat
//                for (int i = 0; i < 19; i++)
//                {
//                    lock (MatlabLoader.MatlabLockable)
//                    {
//                        MWArray[] surrogates = suite.ItSurrDat(2, array, (MWNumericArray)(500));
//                        MWNumericArray surr = surrogates[0] as MWNumericArray;
//                        double[,] dataSur = (double[,])surr.ToArray(MWArrayComponent.Real);
//                        float[] realDataSur = new float[dataSur.GetLength(1)];
//                        for (int k = 0; k < realDataSur.Length; k++)
//                        {
//                            realDataSur[k] = Convert.ToSingle(dataSur[0, k]);
//                        }
//                        surr.Dispose();


//                        //Select which method to combine the data
//                        if (intervals)
//                            differences = getDifferences(realDataSur, otherData, var1start, var2start, whichToDo);
//                        else differences = getSubtracted(realDataSur, otherData);

//                        pool = new CeresBase.Data.DataPools.DataPoolNetCDF(new int[] { differences.Length });
//                        pool.SetData(CeresBase.Data.DataUtilities.ArrayToMemoryStream(differences));
//                        if (intervals)
//                            var = new SpikeVariable(new SpikeVariableHeader("Throwaway2", "", "throw", 1, SpikeVariableHeader.SpikeType.Raw));
//                        else
//                            var = new SpikeVariable(new SpikeVariableHeader("Throwaway2", "", "throw", freq, SpikeVariableHeader.SpikeType.Raw));
//                        frame = new DataFrame(var, pool, new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless);
//                        var.AddDataFrame(frame);
//                    }

                   

//                    //if (add && i == 0)
//                    //    CeresBase.Data.VariableSource.AddVariable(var);


//                    MultiViewer entropyViewerSurrogate = entropy.Process(new SpikeVariable[] { var },
//                        new object[]{
//                            0F, -1F, -1F, m, .2F, tau, false, -1F, 1, 0.05,1.0
//                        })[0] as MultiViewer;

//                    double surrEntropyVal = (entropyViewerSurrogate.Plots[0].YData as double[,])[0, 0];

//                    if (double.IsInfinity(surrEntropyVal))
//                    {
//                        i--;
//                        continue;
//                    }

//                    //if (surrEntropyVal - entropyVal * tolerance > entropyVal)
//                    //{
//                    //    numMore++;
//                    //}
//                    surrAvg += surrEntropyVal / 19.0;
//                    surrEs[i] = surrEntropyVal;
//                }


//                toRet[0, whichPass] = entropyVal;
//                toRet[1, whichPass] = entropyError1;
//                toRet[2, whichPass] = entropyError2;
//                toRet[3, whichPass] = surrAvg;
//                toRet[4, whichPass] = surrAvg + CeresBase.MathConstructs.Stats.StandardDev(surrEs) * tolerance;
//                toRet[5, whichPass] = surrAvg - CeresBase.MathConstructs.Stats.StandardDev(surrEs) * tolerance;

//                whichPass++;
//                taux.Add(tau);

//                #endregion
//            }

//            array.Dispose();

//            xs = taux.ToArray();
//            return toRet;
//        }

//        private static float[] getSubtracted(float[] var1data, float[] var2data)
//        {
//            float[] toRet = new float[var1data.Length];
//            for (int i = 0; i < toRet.Length; i++)
//            {
//                toRet[i] = var1data[i] + var2data[i];
//            }
//            return toRet;
//        }

//        private static float[] getDifferences(float[] var1data, float[] var2data, float var1start, float var2start, int which)
//        {
//            List<float> differences = new List<float>();

//            if (var1data.Length > var2data.Length)
//            {
//                float[] temp = var1data;
//                var1data = var2data;
//                var2data = temp;

//                float tempStart = var1start;
//                var1start = var2start;
//                var2start = tempStart;
//            }

//            //Now data is collected, so make the dummy variable that is the difference between the two
//            float last1 = var1start;
//            float last2 = var2start;

//          //  float running = 0;

//            int lastIndex = 0;

//            //Use slow time series as trigger, get differences to all fast marks until next slow trigger
//            if (which == 0)
//            {

//                //Hold the slower and then go through the faster
//                for (int i = 0; i < var1data.Length; i++)
//                {
//                    float nextTime = float.MaxValue;
                    

//                    if (i < var1data.Length)
//                    {
//                        nextTime = last1 + var1data[i];
//                    }

//                    for (int j = lastIndex; j < var2data.Length; j++)
//                    {
//                        float val = last2 - last1;
//                        differences.Add(val);
//                       // running += val;

//                        last2 += var2data[j];
//                        lastIndex++;


//                        if (last2 > nextTime) break;

//                    }
//                    if (i < var1data.Length)
//                        last1 += var1data[i];
//                }

//            }
//            else if (which == 1)
//            {
//                bool first = true;
//                //Get differences to very next interval
//                for (int i = 0; i < var1data.Length; i++)
//                {
//                    for (int j = lastIndex; j < var2data.Length; j++)
//                    {
//                        if (first)
//                        {
//                            first = false;

//                            if (last2 > last1)
//                            {
//                                float fval = last2 - last1;
//                                differences.Add(fval);
//                                break;
//                            }
//                        }


//                        last2 += var2data[j];
//                        lastIndex++;

//                        if (last2 < last1) continue;
//                        float val = last2 - last1;
//                        differences.Add(val);
//                        break;
//                    }
//                    last1 += var1data[i];
//                }
//            }
//            else
//            {
//                float prevlast = 0;
//                //Get differences to very previous interval
//                for (int i = 0; i < var1data.Length; i++)
//                {
//                    last1 += var1data[i];

//                    for (int j = lastIndex; j < var2data.Length; j++)
//                    {
//                        if (last2 >= last1)
//                            break;

//                        prevlast = last2;

//                        last2 += var2data[j];
//                        lastIndex++;
//                    }

//                    float val = last1 - prevlast;
//                    differences.Add(val);
//                }
//            }

//            return differences.ToArray();
//        }

//        public Type OutputType
//        {
//            get { return typeof(MultiViewer); }
//        }

//        #endregion

//        #region ICloneable Members

//        public object Clone()
//        {
//            return new Coupling();
//        }

//        #endregion
//    }
//}
