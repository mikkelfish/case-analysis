﻿


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.Projects.Flowable;
using MesosoftCommon.Utilities.Validations;
using CeresBase.FlowControl.Adapters.Permutations;
using ApolloFinal.CUDA;
using CeresBase.UI;

namespace ApolloFinal.Tools.BatchProcessable
{
    [AdapterDescription("Interval")]
    [AdapterDescription("Raw")]
    class MultiscaleSampleEntropy : IBatchFlowable
    {
        private static object lockable = new object();

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter sampleSize = new BatchProcessableParameter()
            {
                Name = "Sample Size",
                Val = 10.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };
            BatchProcessableParameter dims = new BatchProcessableParameter()
            {
                Name = "Dimensions",
                Val = 3,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };
            BatchProcessableParameter radius = new BatchProcessableParameter()
            {
                Name = "Radius",
                Val = 0.2,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };

            BatchProcessableParameter multiScaleStep = new BatchProcessableParameter()
            {
                Name = "Multiscale Step",
                Val = 10,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 1,
                    Operator = ComparisonValidator.Comparison.GreaterThanEqual,
                    TreatNonComparableAsSuccess = true
                }
            };
            BatchProcessableParameter numSurrogates = new BatchProcessableParameter()
            {
                Name = "Number Surrogates",
                Val = 19
            };
            BatchProcessableParameter sdmult = new BatchProcessableParameter()
            {
                Name = "Std Dev Multiplier",
                Val = 1.0
            };
            BatchProcessableParameter pval = new BatchProcessableParameter()
            {
                Name = "P-Value",
                Val = 0.20
            };

            BatchProcessableParameter label = new BatchProcessableParameter() { Name = "Label", Val = "Enter Here" };
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                Name = "Data",
                Editable = false,
                CanLinkFrom = false,
                Validator = new MesosoftCommon.Utilities.Validations.NotNullValidator(),
                TargetType = typeof(float[])
            };
            BatchProcessableParameter resolution = new BatchProcessableParameter()
            {
                Name = "Data Resolution",
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };


            return new BatchProcessableParameter[] { sampleSize, dims, radius, multiScaleStep, numSurrogates, sdmult, pval, label, data, resolution };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            double sampleSize = Convert.ToDouble(parameters.Single(p => p.Name == "Sample Size").Val);
            int dims = Convert.ToInt32(parameters.Single(p => p.Name == "Dimensions").Val);
            double radius = Convert.ToDouble(parameters.Single(p => p.Name == "Radius").Val);
            int msStep = Convert.ToInt32(parameters.Single(p => p.Name == "Multiscale Step").Val);
            int numsur = Convert.ToInt32(parameters.Single(p => p.Name == "Number Surrogates").Val);
            double passMult = Convert.ToDouble(parameters.Single(p => p.Name == "Std Dev Multiplier").Val);
            double pval = Convert.ToDouble(parameters.Single(p => p.Name == "P-Value").Val);

            string label = parameters.Single(p => p.Name == "Label").Val as string;
            float[] data = parameters.Single(p => p.Name == "Data").Val as float[];
            double resolution = Convert.ToDouble(parameters.Single(p => p.Name == "Data Resolution").Val);

            double sd = radius * CeresBase.MathConstructs.Stats.StandardDev(data);

            int sampleCount = (int)Math.Ceiling(sampleSize / resolution);
            if (sampleCount > data.Length)
            {
                sampleCount = data.Length;
            }

            List<double[]> E = new List<double[]>();
            List<double[]> errors = new List<double[]>();
            List<double[]>[] surr = new List<double[]>[numsur];

            for (int surrI = 0; surrI < numsur; surrI++)
            {
                surr[surrI] = new List<double[]>();
            }

            int begIndex = 0;
            while (begIndex + sampleCount <= data.Length)
            {
                float[] fData = new float[sampleCount];
                Array.Copy(data, begIndex, fData, 0, sampleCount);

                int repeat = numsur + 1;

                for (int thisrep = 0; thisrep < repeat; thisrep++)
                {
                    float[] rData = fData;
                    if (thisrep != 0)
                    {
                        rData = SurrogatePermutation.CreateSurrogate(fData);
                    }

                    double[] es = new double[msStep];
                    double[] er = new double[msStep];

                    bool one, two;
                    bool use = true;

                    lock (lockable)
                    {
                        use = SampEnWrapper.Available(out one, out two);
                        
                    }

                    for (int thisStep = 0; thisStep < msStep; thisStep++)
                    {
                        float[] scaled = new float[rData.Length / (thisStep + 1)];

                        for (int scaledIndex = 0; scaledIndex < scaled.Length; scaledIndex++)
                        {
                            float sum = 0;
                            for (int sumPoint = 0; sumPoint < thisStep + 1; sumPoint++)
                            {
                                int rIndex = scaledIndex*(thisStep+1)  + sumPoint;
                                sum += rData[rIndex];
                            }

                            sum /= (thisStep + 1);
                            scaled[scaledIndex] = sum;
                        }

                        if (!use || scaled.Length > 10000)
                        {
                            double[][] res = SampleEntropy.simpleSamp(scaled, dims, 1, sd);

                            es[thisStep] = res[0][dims - 1];
                            er[thisStep] = res[1][dims - 1];

                        }
                        else
                        {
                            double[] tempEs = new double[32];

                            lock (lockable)
                            {

                                SampEnWrapper.CalculateSampEnCuda(scaled, dims, (float)sd, 1, 1, tempEs);
                                Array.Copy(tempEs, 0, es, thisStep, 1);
                            }

                        }
                    }

                   

                    if (thisrep == 0)
                    {
                        E.Add(es);
                        errors.Add(er);
                    }
                    else
                    {
                        surr[thisrep - 1].Add(es);
                    }
                }
                begIndex += sampleCount;
            }


            double[] surrMeans = new double[msStep];
            double[] surrSD = new double[msStep];
            double[] eMeans = new double[msStep];
            double[] errMeans = new double[msStep];

            int[] passed = new int[msStep];



            for (int j = 0; j < E.Count; j++)
            {
                for (int k = 0; k < msStep; k++)
                {
                    eMeans[k] += E[j][k] / E.Count;
                    errMeans[k] += errors[j][k] / E.Count;

                    double thisSurrMean = 0;
                    double thisSurrSD = 0;
                    for (int l = 0; l < numsur; l++)
                    {
                        thisSurrMean += surr[l][j][k] / numsur;
                    }
                    for (int l = 0; l < numsur; l++)
                    {
                        thisSurrSD += (surr[l][j][k] - thisSurrMean) * (surr[l][j][k] - thisSurrMean);
                    }
                    thisSurrSD /= numsur + 1;
                    thisSurrSD = Math.Sqrt(thisSurrSD);

                    if (E[j][k] + errors[j][k] < thisSurrMean - thisSurrSD * passMult ||
                        E[j][k] - errors[j][k] > thisSurrMean + thisSurrSD * passMult)
                    {
                        passed[k]++;
                    }

                    surrMeans[k] += thisSurrMean / E.Count;
                    surrSD[k] += thisSurrSD / E.Count;
                }
            }

            int[] totPassed = new int[msStep];
            for (int j = 0; j < msStep; j++)
            {
                double prob = CeresBase.MathConstructs.Stats.BinomialTest(0.5, passed[j], E.Count, false);
                if (1.0 - prob < pval)
                {
                    totPassed[j] = 1;
                }
            }

            GraphableOutput[] output = new GraphableOutput[2];

            double[] x = new double[msStep];
            double[][] y1 = new double[6][];
            for (int i = 0; i < 6; i++)
            {
                y1[i] = new double[msStep];
            }

            double[][] y2 = new double[1][];
            y2[0] = new double[msStep];

            for (int j = 0; j < x.Length; j++)
            {
                x[j] = 1 + j;

                y1[0][j] = eMeans[j];
                y1[1][j] = eMeans[j] + errMeans[j];
                y1[2][j] = eMeans[j] - errMeans[j];

                y1[3][j] = surrMeans[j];
                y1[4][j] = surrMeans[j] + surrSD[j] * passMult;
                y1[5][j] = surrMeans[j] - surrSD[j] * passMult;

                y2[0][j] = totPassed[j];
            }

            output[0] = new GraphableOutput()
            {
                SourceDescription = "Sample Entropy Results",
                XLabel = "Tau",
                YLabel = "Entropy",
                YUnits = "Bits",
                SeriesLabels = new string[]{"Signal", "Signal + Error", "Signal - Erorr", 
                    "Surrogate Means", "Surrogate + " + Math.Round(passMult,2) + " SDs",
                    "Surrogate - " + Math.Round(passMult,2) + " SDs"},
                Label = label,
                XData = x,
                YData = y1
            };
            output[1] = new GraphableOutput()
            {
                SourceDescription = "SampEn Passed Bionomial Test",
                XLabel = "Tau",
                YLabel = "Passed",
                YUnits = "Yes or No",
                Label = label,
                XData = x,
                YData = y2
            };

            //GraphableOutputCollection collection = new GraphableOutputCollection();
            //collection.Graphs.Add(output[0]);
            //collection.Graphs.Add(output[1]);
            return output;
        }

        public override string ToString()
        {
            return "Multiscale SME";
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Sample Entropy Graph", "Binomial Test" }; }
        }

        #endregion
    }
}
