//using System;
//using System.Collections.Generic;
//using System.Text;
//using ApolloFinal.Tools.BatchProcessing;
//using CeresBase.UI;
//using MathWorks.MATLAB.NET.Arrays;
//using CeresBase.FlowControl.Adapters.Matlab;

//namespace ApolloFinal.Tools.BatchProcessable
//{
//    class CrossMutualInformation : IBatchProcessable
//    {
//        #region IBatchProcessable Members

//        public bool SuppportsMultiple
//        {
//            get { return false; }
//        }

//        public object[] GetArguments(SpikeVariable[] varsToUse, out string[] names)
//        {
//            names = new string[1];
//            // names

//            List<string> categories = new List<string>();
//            List<string> name = new List<string>();
//            List<string> descs = new List<string>();
//            List<Type> types = new List<Type>();
//            List<object> vals = new List<object>();

//            for (int i = 0; i < varsToUse.Length; i++)
//            {
//                categories.Add(varsToUse[i].FullDescription);
//                name.Add("Start " + i);
//                descs.Add("Variable start");
//                types.Add(typeof(float));
//                vals.Add(-1);

//                categories.Add(varsToUse[i].FullDescription);
//                name.Add("End " + i);
//                descs.Add("Variable end");
//                types.Add(typeof(float));
//                vals.Add(0);
//            }

//            RequestInformation info = new RequestInformation();
//            info.Grid.SetInformationToCollect(
//                types.ToArray(),
//                categories.ToArray(),
//                name.ToArray(),
//                descs.ToArray(),
//                null, vals.ToArray());

//            if (info.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
//            {
//                names = null;
//                return null;
//            }

//            List<object> res = new List<object>();
//            foreach (string n in name)
//            {
//                res.Add(info.Grid.Results[n]);
//            }
//            names[0] = varsToUse[0].FullDescription + " "
//               + varsToUse[1].FullDescription + " Mutual Information";
//            return res.ToArray();
//        }


//        public object[] Process(SpikeVariable[] varsToUse, object[] args)
//        {
//            float start1 = (float)args[0];
//            float end1 = (float)args[1];
//            float start2 = (float)args[2];
//            float end2 = (float)args[3];

//            List<float> data1 = new List<float>();
//            List<float> data2 = new List<float>();

//            if (end1 < 0) end1 = (float)varsToUse[0].TotalLength;
//            if (end2 < 0) end2 = (float)varsToUse[1].TotalLength;

//            int begIndex1 = (int)Math.Floor(start1 / varsToUse[0].Resolution);
//            int endIndex1 = (int)Math.Floor(end1 / varsToUse[0].Resolution);

//            int begIndex2 = (int)Math.Floor(start2 / varsToUse[1].Resolution);
//            int endIndex2 = (int)Math.Floor(end2 / varsToUse[1].Resolution);

//            varsToUse[0][0].Pool.GetData(
//                new int[] { begIndex1 }, new int[] { endIndex1 - begIndex1 },
//                delegate(float[] data, uint[] indices, uint[] count)
//                {
//                    data1.AddRange(data);
//                }
//            );

//            varsToUse[1][0].Pool.GetData(
//                new int[] { begIndex2 }, new int[] { endIndex2 - begIndex2 },
//                delegate(float[] data, uint[] indices, uint[] count)
//                {
//                    data2.AddRange(data);
//                }
//            );

//            EverythingMatlab.EverythingMatlabclass suite = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;

//            lock (MatlabLoader.MatlabLockable)
//            {
//                MWNumericArray array1 = new MWNumericArray(data1.ToArray());
//                MWNumericArray array2 = new MWNumericArray(data2.ToArray());
//                MWArray[] res = suite.information(4, array1, array2,
//                    (MWNumericArray)2.0);
//                array1.Dispose();
//                array2.Dispose();

//                double[,] mI = (double[,])(res[0] as MWNumericArray).ToArray(MWArrayComponent.Real);
//                double[,] e = (double[,])(res[2] as MWNumericArray).ToArray(MWArrayComponent.Real);

//                BatchVals bVals = new BatchVals();
//                bVals.Title = varsToUse[0].FullDescription + " "
//                   + varsToUse[1].FullDescription + " Mutual Information";
//                bVals.Descriptions = new string[] { "Val", "E" };
//                bVals.Values = new object[] { mI[0, 0], e[0, 0] };

//                return new object[] { bVals };
//            }
//        }

//        public Type OutputType
//        {
//            get { return typeof(UI.MatlabPlot); }
//        }

//        #endregion

//        #region ICloneable Members

//        public object Clone()
//        {
//            return new CrossMutualInformation();
//        }

//        #endregion
//    }
//}
