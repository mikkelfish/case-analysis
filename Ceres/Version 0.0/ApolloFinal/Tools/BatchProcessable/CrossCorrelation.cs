using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.UI;
using System.Windows.Forms;

namespace ApolloFinal.Tools.BatchProcessable
{
    class CrossCorrelation
    {
        //#region IBatchProcessable Members

        //public bool SuppportsMultiple
        //{
        //    get { return false; }
        //}

        //public object[] GetArguments(SpikeVariable[] varsToUse, string hotspotName, out string[] names)
        //{
        //    throw new NotImplementedException();
        //}

        //public object[] GetArguments(SpikeVariable[] varsToUse, out string[] names)
        //{
        //    if (varsToUse.Length != 2)
        //    {
        //        MessageBox.Show("This requires selecting two variables.");
        //        names = null;
        //        return null;
        //    }

        //    RequestInformation req = new RequestInformation();
        //    req.Grid.SetInformationToCollect(
        //        new Type[] { typeof(int), typeof(bool), typeof(float), typeof(float), typeof(float), typeof(float) },
        //        new string[] { "Params", "Params", "Time", "Time", "Time", "Time" },
        //        new string[] { "MaxLags", "Both Sides", "Start1", "End1", "Start2", "End2" },
        //        new string[] { "Maximum lags", "Do both negative and positive lags", "Start Time", "End Time", "Start2 Time", "End2 Time" },
        //        null,
        //        new object[] { 100, true, 0, -1, 0, -1 });
        //    if (req.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
        //    {
        //        names = null;
        //        return null;
        //    }

        //    int lags = (int)req.Grid.Results["MaxLags"];
        //    bool bothsides = (bool)req.Grid.Results["Both Sides"];
        //    float start1 = (float)req.Grid.Results["Start1"];
        //    float end1 = (float)req.Grid.Results["End1"];
        //    float start2 = (float)req.Grid.Results["Start2"];
        //    float end2 = (float)req.Grid.Results["End2"];

        //    names = new string[1];
        //    names[0] = "Cross Correlation: " + varsToUse[0].FullDescription + " " + varsToUse[1].FullDescription + " lags " + lags + " " + start1
        //        + "-" + end1;

        //    return new object[] { start1, end1, start2, end2, lags, bothsides };
        //}

        //public object[] Process(SpikeVariable[] varsToUse, object[] args)
        //{
        //    float start1 = (float)args[0];
        //    float end1 = (float)args[1];
        //    float start2 = (float)args[2];
        //    float end2 = (float)args[3];
        //    int lags = (int)args[4];
        //    bool both = (bool)args[5];



        //    int start1I = (int)(start1 / varsToUse[0].Resolution);
        //    int start2I = (int)(start2 / varsToUse[1].Resolution);

        //    if (end1 < 0)
        //        end1 = (float)varsToUse[0].TotalLength;

        //    if (end2 < 0)
        //        end2 = (float)varsToUse[0].TotalLength;

        //    int end1I = (int)(end1 / varsToUse[0].Resolution);
        //    int end2I = (int)(end2 / varsToUse[1].Resolution);

        //    List<float> data1 = new List<float>();
        //    List<float> data2 = new List<float>();

        //    varsToUse[0][0].Pool.GetData(new int[] { start1I }, new int[] { end1I - start1I },
        //        delegate(float[] data, uint[] indices, uint[] counts)
        //        {
        //            data1.AddRange(data);
        //        }
        //    );

        //    varsToUse[1][0].Pool.GetData(new int[] { start2I }, new int[] { end2I - start2I },
        //        delegate(float[] data, uint[] indices, uint[] counts)
        //        {
        //            data2.AddRange(data);
        //        }
        //    );

        //    object[] graph = RunCrossCorrelation(data1.ToArray(), data2.ToArray(), lags, both);
        //    (graph[0] as UI.MatlabPlot).Label = varsToUse[0].Header.Description + " cross " + varsToUse[1].Header.Description +
        //        " " + start1 + "-" + end1 + " " + start2 + "-" + end2;
        //    return graph;
        //}

        //public Type OutputType
        //{
        //    get { return typeof(UI.MatlabPlot); }
        //}

        //#endregion

        //private static MatlabAutoCorr.MatlabAutoCorr autoCorr;


        //public static object[] RunCrossCorrelation(float[] data1, float[] data2, int maxLags, bool bothsides)
        //{
        //    float avg1 = CeresBase.MathConstructs.Stats.Average(data1);
        //    float sd1 = CeresBase.MathConstructs.Stats.StandardDev(data1);

        //    float avg2 = CeresBase.MathConstructs.Stats.Average(data2);
        //    float sd2 = CeresBase.MathConstructs.Stats.StandardDev(data2);

        //    double[] auto1 = AutoCorrelation(data1, maxLags, false);
        //    double[] auto2 = AutoCorrelation(data2, maxLags, false);

        //    //if (autoCorr == null)
        //    //{
        //    //    autoCorr = new MatlabAutoCorr.MatlabAutoCorr();
        //    //}

        //    //MWNumericArray d1 = (MWNumericArray)data1;
        //    //MWNumericArray d2 = (MWNumericArray)data2;

        //    //MWArray[] result = autoCorr.AutoCorr(2, d1, d2, (MWNumericArray)maxLags, (MWCharArray)"none");
        //    //MWNumericArray corrA = (MWNumericArray)result[0];
        //    //MWNumericArray lagsA = (MWNumericArray)result[1];
        //    //float[,] corrD = (float[,])corrA.ToArray(MWArrayComponent.Real);
        //    //double[,] lagsD = (double[,])lagsA.ToArray(MWArrayComponent.Real);

        //    int numvals = maxLags;
        //    if (bothsides) numvals *= 2;

        //    numvals++;
        //    double[] vals = new double[numvals];

        //    int offset = 0;

        //    if (bothsides)
        //    {
        //        for (int i = 0; i <= maxLags; i++)
        //        {
        //            for (int j = 0; j < data1.Length - i; j++)
        //            {
        //                vals[maxLags - i] += (data2[j + i] - avg2) * (data1[j] - avg1);
        //            }

        //            vals[maxLags - i] /= (sd1 * sd2 * (data1.Length - 2 * i));

        //        }

        //        offset = maxLags;
        //    }

        //    for (int i = 1; i <= maxLags; i++)
        //    {
        //        for (int j = 0; j < data2.Length - i; j++)
        //        {
        //            vals[i + offset] += (data1[j + i] - avg1) * (data2[j] - avg2);
        //        }

        //        vals[i + offset] /= (sd1 * sd2 * (data1.Length - 2 * i));
        //    }

        //    //Estimate of autocorrelation variance^2 = (1/N)(1 + 2 * SIG l=1 -> maxLags AC(1)*AC(2))
        //    //Cutoff at 1.96 * SQRT(variance/N)
        //    double ccVar = 0;
        //    for (int i = 0; i < auto1.Length; i++)
        //    {
        //        ccVar += auto1[i] * auto2[i];
        //    }

        //    ccVar *= 2;
        //    ccVar++;
        //    ccVar /= data1.Length * data1.Length;

        //    //1.96 = 95% confidence
        //    double repMeasures = 1.0 - 0.05 / numvals;
        //    double sd = CeresBase.MathConstructs.SpecialFunction.erf(repMeasures / Math.Sqrt(2));

        //    double cutoff = sd * Math.Sqrt(ccVar);
        //    if (cutoff >= 1.0)
        //    {
        //        cutoff = Math.Tanh(3.0 * (1.0 * Math.Tanh(ccVar)));
        //    }

        //    bool[] beatsCutoff = new bool[vals.Length];
        //    for (int i = 0; i < beatsCutoff.Length; i++)
        //    {
        //        if (vals[i] >= cutoff) beatsCutoff[i] = true;
        //    }

        //    UI.MatlabPlot plot = new ApolloFinal.UI.MatlabPlot();

        //    int[] xs = new int[vals.Length];
        //    double[,] ys = new double[4, vals.Length];
        //    for (int i = 0; i < vals.Length; i++)
        //    {
        //        ys[0, i] = vals[i];
        //        ys[1, i] = cutoff;
        //        ys[2, i] = -cutoff;
        //        ys[3, i] = beatsCutoff[i] ? 1 : 0;

        //        if (!bothsides)
        //            xs[i] = i;
        //        else
        //        {
        //            xs[i] = i - maxLags;
        //        }
        //    }

        //    plot.XData = xs;
        //    plot.YData = ys;

        //    return new UI.MatlabPlot[] { plot };
        //}

        public static double[] AutoCorrelation(float[] pdata, int maxLags, bool subMean)
        {
            float[] data1 = pdata;
            if (subMean)
            {
                data1 = new float[pdata.Length];
                pdata.CopyTo(data1, 0);
                float avg = CeresBase.MathConstructs.Stats.Average(data1);
                for (int i = 0; i < data1.Length; i++)
                {
                    data1[i] -= avg;
                }
            }

            double firstVal = 0;
            double[] vals = new double[maxLags + 1];
            for (int i = 0; i < vals.Length; i++)
            {
                for (int j = i; j < data1.Length; j++)
                {
                    vals[i] += data1[j] * data1[j - i];
                }

                if (i == 0) firstVal = vals[0];
                vals[i] /= firstVal;
            }

            return vals;
        }
    }
}
