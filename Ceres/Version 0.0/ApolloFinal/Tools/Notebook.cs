using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using Microsoft.Win32;

//FOR FILE SAVING
using CeresBase.Settings;
using Razor.Configuration;
 
//Author: David T. Allen
namespace ApolloFinal.Tools
{
    /// <summary>
    /// Utility that allows the users to easily record notes.
    /// </summary>
    [XMLSetting("Projects", "Notebook", true, true)]
    public partial class Notebook : Form
    {
        #region Declarations from [Design]
        //This portion is automatically updated when I edit the [Design] part.
        //menuItem# is a separator
        private System.Windows.Forms.MenuItem menuItem13;
        private System.Windows.Forms.MenuItem menuItem21;
        private System.Windows.Forms.MainMenu MenuBar;
        private System.Windows.Forms.MenuItem File;
        private System.Windows.Forms.MenuItem fileMenuPageSetup;
        private System.Windows.Forms.MenuItem fileMenuPrint;
        private System.Windows.Forms.MenuItem fileMenuExit;
        private System.Windows.Forms.MenuItem Edit;
        private System.Windows.Forms.MenuItem Format;
        private System.Windows.Forms.MenuItem editMenuCut;
        private System.Windows.Forms.MenuItem editMenuCopy;
        private System.Windows.Forms.MenuItem editMenuPaste;
        private System.Windows.Forms.MenuItem editMenuDelete;
        private System.Windows.Forms.MenuItem editMenuFind;
        private System.Windows.Forms.MenuItem editMenuFindNext;
        private System.Windows.Forms.MenuItem editMenuReplace;
        private System.Windows.Forms.MenuItem editMenuGoTo;
        private System.Windows.Forms.MenuItem editMenuSelectAll;
        private System.Windows.Forms.MenuItem editMenuTimeDate;
        private System.Windows.Forms.MenuItem formatMenuWordWrap;
        private System.Windows.Forms.TextBox TextArea;
        private System.Windows.Forms.MenuItem formatMenuFont;

        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.StatusBar statusBar1;
        private System.Windows.Forms.StatusBarPanel statusBarPanel1;
        private System.Windows.Forms.StatusBarPanel statusBarPanel2;
        private MenuItem fileMenuExport;
        private MenuItem menuItem2;
        private Panel listPanel;
        private Button bRename;
        private ListBox listFile;
        private Button bDelete;
        private Button bNew;
        private MenuItem menuItem1;
        private MenuItem fileMenuImport;
        private MenuItem fileMenuNew;
        private MenuItem fileMenuRename;
        private MenuItem fileMenuDelete;
        private IContainer components;
        #endregion

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.MenuBar = new System.Windows.Forms.MainMenu(this.components);
            this.File = new System.Windows.Forms.MenuItem();
            this.fileMenuSave = new System.Windows.Forms.MenuItem();
            this.fileMenuImport = new System.Windows.Forms.MenuItem();
            this.fileMenuExport = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.fileMenuPageSetup = new System.Windows.Forms.MenuItem();
            this.fileMenuPrint = new System.Windows.Forms.MenuItem();
            this.menuItem13 = new System.Windows.Forms.MenuItem();
            this.fileMenuExit = new System.Windows.Forms.MenuItem();
            this.fileMenuNew = new System.Windows.Forms.MenuItem();
            this.fileMenuRename = new System.Windows.Forms.MenuItem();
            this.fileMenuDelete = new System.Windows.Forms.MenuItem();
            this.Edit = new System.Windows.Forms.MenuItem();
            this.editMenuSelectAll = new System.Windows.Forms.MenuItem();
            this.editMenuTimeDate = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.editMenuFind = new System.Windows.Forms.MenuItem();
            this.editMenuFindNext = new System.Windows.Forms.MenuItem();
            this.editMenuReplace = new System.Windows.Forms.MenuItem();
            this.editMenuGoTo = new System.Windows.Forms.MenuItem();
            this.menuItem21 = new System.Windows.Forms.MenuItem();
            this.editMenuCut = new System.Windows.Forms.MenuItem();
            this.editMenuCopy = new System.Windows.Forms.MenuItem();
            this.editMenuPaste = new System.Windows.Forms.MenuItem();
            this.editMenuDelete = new System.Windows.Forms.MenuItem();
            this.Format = new System.Windows.Forms.MenuItem();
            this.formatMenuWordWrap = new System.Windows.Forms.MenuItem();
            this.formatMenuFont = new System.Windows.Forms.MenuItem();
            this.TextArea = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.statusBar1 = new System.Windows.Forms.StatusBar();
            this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
            this.statusBarPanel2 = new System.Windows.Forms.StatusBarPanel();
            this.listPanel = new System.Windows.Forms.Panel();
            this.bNew = new System.Windows.Forms.Button();
            this.bRename = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.listFile = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel2)).BeginInit();
            this.listPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuBar
            // 
            this.MenuBar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.File,
            this.Edit,
            this.Format});
            // 
            // File
            // 
            this.File.Index = 0;
            this.File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.fileMenuSave,
            this.fileMenuImport,
            this.fileMenuExport,
            this.menuItem2,
            this.fileMenuPageSetup,
            this.fileMenuPrint,
            this.menuItem13,
            this.fileMenuExit,
            this.fileMenuNew,
            this.fileMenuRename,
            this.fileMenuDelete});
            this.File.Text = "&File";
            // 
            // fileMenuSave
            // 
            this.fileMenuSave.Index = 0;
            this.fileMenuSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.fileMenuSave.Text = "Save Notebook";
            this.fileMenuSave.Click += new System.EventHandler(this.fileMenuSave_Click);
            // 
            // fileMenuImport
            // 
            this.fileMenuImport.Index = 1;
            this.fileMenuImport.Text = "&Import";
            this.fileMenuImport.Click += new System.EventHandler(this.fileMenuImport_Click);
            // 
            // fileMenuExport
            // 
            this.fileMenuExport.Enabled = false;
            this.fileMenuExport.Index = 2;
            this.fileMenuExport.Text = "&Export";
            this.fileMenuExport.Click += new System.EventHandler(this.fileMenuExport_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 3;
            this.menuItem2.Text = "-";
            // 
            // fileMenuPageSetup
            // 
            this.fileMenuPageSetup.Index = 4;
            this.fileMenuPageSetup.Text = "Page Se&tup";
            this.fileMenuPageSetup.Click += new System.EventHandler(this.PageSetup_Click);
            // 
            // fileMenuPrint
            // 
            this.fileMenuPrint.Enabled = false;
            this.fileMenuPrint.Index = 5;
            this.fileMenuPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.fileMenuPrint.Text = "&Print CHECK IF WORKS";
            this.fileMenuPrint.Click += new System.EventHandler(this.Print_Click);
            // 
            // menuItem13
            // 
            this.menuItem13.Index = 6;
            this.menuItem13.Text = "-";
            // 
            // fileMenuExit
            // 
            this.fileMenuExit.Index = 7;
            this.fileMenuExit.Text = "E&xit";
            this.fileMenuExit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // fileMenuNew
            // 
            this.fileMenuNew.Index = 8;
            this.fileMenuNew.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            this.fileMenuNew.Text = "HIDDEN New";
            this.fileMenuNew.Visible = false;
            this.fileMenuNew.Click += new System.EventHandler(this.fileMenuNew_Click);
            // 
            // fileMenuRename
            // 
            this.fileMenuRename.Index = 9;
            this.fileMenuRename.Shortcut = System.Windows.Forms.Shortcut.CtrlE;
            this.fileMenuRename.Text = "HIDDEN Rename";
            this.fileMenuRename.Visible = false;
            this.fileMenuRename.Click += new System.EventHandler(this.fileMenuRename_Click);
            // 
            // fileMenuDelete
            // 
            this.fileMenuDelete.Index = 10;
            this.fileMenuDelete.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.fileMenuDelete.Text = "HIDDEN Delete";
            this.fileMenuDelete.Visible = false;
            this.fileMenuDelete.Click += new System.EventHandler(this.fileMenuDelete_Click);
            // 
            // Edit
            // 
            this.Edit.Index = 1;
            this.Edit.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.editMenuSelectAll,
            this.editMenuTimeDate,
            this.menuItem1,
            this.editMenuFind,
            this.editMenuFindNext,
            this.editMenuReplace,
            this.editMenuGoTo,
            this.menuItem21,
            this.editMenuCut,
            this.editMenuCopy,
            this.editMenuPaste,
            this.editMenuDelete});
            this.Edit.Text = "&Edit";
            // 
            // editMenuSelectAll
            // 
            this.editMenuSelectAll.Enabled = false;
            this.editMenuSelectAll.Index = 0;
            this.editMenuSelectAll.Shortcut = System.Windows.Forms.Shortcut.CtrlA;
            this.editMenuSelectAll.Text = "Sellect &All";
            this.editMenuSelectAll.Click += new System.EventHandler(this.SelectAll_Click);
            // 
            // editMenuTimeDate
            // 
            this.editMenuTimeDate.Enabled = false;
            this.editMenuTimeDate.Index = 1;
            this.editMenuTimeDate.Shortcut = System.Windows.Forms.Shortcut.F5;
            this.editMenuTimeDate.Text = "&Insert Date and Time";
            this.editMenuTimeDate.Click += new System.EventHandler(this.TimeDate_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 2;
            this.menuItem1.Text = "-";
            // 
            // editMenuFind
            // 
            this.editMenuFind.Enabled = false;
            this.editMenuFind.Index = 3;
            this.editMenuFind.Shortcut = System.Windows.Forms.Shortcut.CtrlF;
            this.editMenuFind.Text = "&Find";
            this.editMenuFind.Click += new System.EventHandler(this.Find_Click);
            // 
            // editMenuFindNext
            // 
            this.editMenuFindNext.Enabled = false;
            this.editMenuFindNext.Index = 4;
            this.editMenuFindNext.Shortcut = System.Windows.Forms.Shortcut.F3;
            this.editMenuFindNext.Text = "Find &Next";
            this.editMenuFindNext.Click += new System.EventHandler(this.FindNext_Click);
            // 
            // editMenuReplace
            // 
            this.editMenuReplace.Enabled = false;
            this.editMenuReplace.Index = 5;
            this.editMenuReplace.Shortcut = System.Windows.Forms.Shortcut.CtrlR;
            this.editMenuReplace.Text = "&Replace...";
            this.editMenuReplace.Click += new System.EventHandler(this.Replace_Click);
            // 
            // editMenuGoTo
            // 
            this.editMenuGoTo.Enabled = false;
            this.editMenuGoTo.Index = 6;
            this.editMenuGoTo.Shortcut = System.Windows.Forms.Shortcut.CtrlG;
            this.editMenuGoTo.Text = "&Go To Line";
            this.editMenuGoTo.Click += new System.EventHandler(this.GoTo_Click);
            // 
            // menuItem21
            // 
            this.menuItem21.Index = 7;
            this.menuItem21.Text = "-";
            // 
            // editMenuCut
            // 
            this.editMenuCut.Enabled = false;
            this.editMenuCut.Index = 8;
            this.editMenuCut.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
            this.editMenuCut.Text = "Cu&t";
            this.editMenuCut.Click += new System.EventHandler(this.Cut_Click);
            // 
            // editMenuCopy
            // 
            this.editMenuCopy.Enabled = false;
            this.editMenuCopy.Index = 9;
            this.editMenuCopy.Shortcut = System.Windows.Forms.Shortcut.CtrlC;
            this.editMenuCopy.Text = "&Copy";
            this.editMenuCopy.Click += new System.EventHandler(this.Copy_Click);
            // 
            // editMenuPaste
            // 
            this.editMenuPaste.Enabled = false;
            this.editMenuPaste.Index = 10;
            this.editMenuPaste.Shortcut = System.Windows.Forms.Shortcut.CtrlV;
            this.editMenuPaste.Text = "&Paste";
            this.editMenuPaste.Click += new System.EventHandler(this.Paste_Click);
            // 
            // editMenuDelete
            // 
            this.editMenuDelete.Enabled = false;
            this.editMenuDelete.Index = 11;
            this.editMenuDelete.Shortcut = System.Windows.Forms.Shortcut.Del;
            this.editMenuDelete.Text = "HIDDEN Delete";
            this.editMenuDelete.Visible = false;
            this.editMenuDelete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Format
            // 
            this.Format.Index = 2;
            this.Format.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.formatMenuWordWrap,
            this.formatMenuFont});
            this.Format.Text = "F&ormat";
            // 
            // formatMenuWordWrap
            // 
            this.formatMenuWordWrap.Checked = true;
            this.formatMenuWordWrap.Index = 0;
            this.formatMenuWordWrap.Text = "&Word Wrap";
            this.formatMenuWordWrap.Click += new System.EventHandler(this.WordWrap_Click);
            // 
            // formatMenuFont
            // 
            this.formatMenuFont.Index = 1;
            this.formatMenuFont.Text = "Change &Font";
            this.formatMenuFont.Click += new System.EventHandler(this.font_Click);
            // 
            // TextArea
            // 
            this.TextArea.AcceptsReturn = true;
            this.TextArea.AcceptsTab = true;
            this.TextArea.Enabled = false;
            this.TextArea.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextArea.HideSelection = false;
            this.TextArea.Location = new System.Drawing.Point(0, 4);
            this.TextArea.Multiline = true;
            this.TextArea.Name = "TextArea";
            this.TextArea.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextArea.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TextArea.Size = new System.Drawing.Size(413, 290);
            this.TextArea.TabIndex = 5;
            this.TextArea.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TextArea_MouseDown);
            this.TextArea.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextArea_KeyUp);
            this.TextArea.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextArea_KeyPress);
            this.TextArea.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TextArea_MouseUp);
            this.TextArea.TextChanged += new System.EventHandler(this.TextArea_TextChanged);
            this.TextArea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextArea_KeyDown);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "txt";
            this.openFileDialog1.Filter = "\"Text files|*.txt|All files|*.*\"";
            this.openFileDialog1.ShowHelp = true;
            this.openFileDialog1.ShowReadOnly = true;
            // 
            // fontDialog1
            // 
            this.fontDialog1.Font = new System.Drawing.Font("Lucida Console", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fontDialog1.ShowEffects = false;
            this.fontDialog1.ShowHelp = true;
            // 
            // printDialog1
            // 
            this.printDialog1.AllowSomePages = true;
            this.printDialog1.Document = this.printDocument1;
            this.printDialog1.ShowHelp = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "txt";
            this.saveFileDialog1.Filter = "\"TEXT files|*.txt|All files|*.*\"";
            this.saveFileDialog1.ShowHelp = true;
            // 
            // statusBar1
            // 
            this.statusBar1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.statusBar1.Location = new System.Drawing.Point(0, 299);
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.statusBarPanel1,
            this.statusBarPanel2});
            this.statusBar1.ShowPanels = true;
            this.statusBar1.Size = new System.Drawing.Size(592, 21);
            this.statusBar1.TabIndex = 1;
            // 
            // statusBarPanel1
            // 
            this.statusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.statusBarPanel1.Name = "statusBarPanel1";
            this.statusBarPanel1.Width = 425;
            // 
            // statusBarPanel2
            // 
            this.statusBarPanel2.Name = "statusBarPanel2";
            this.statusBarPanel2.Text = "Ln 1, Col 1";
            this.statusBarPanel2.Width = 150;
            // 
            // listPanel
            // 
            this.listPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listPanel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.listPanel.Controls.Add(this.bNew);
            this.listPanel.Controls.Add(this.bRename);
            this.listPanel.Controls.Add(this.bDelete);
            this.listPanel.Controls.Add(this.listFile);
            this.listPanel.Location = new System.Drawing.Point(419, 3);
            this.listPanel.Name = "listPanel";
            this.listPanel.Size = new System.Drawing.Size(172, 296);
            this.listPanel.TabIndex = 6;
            // 
            // bNew
            // 
            this.bNew.Location = new System.Drawing.Point(110, 2);
            this.bNew.Name = "bNew";
            this.bNew.Size = new System.Drawing.Size(51, 24);
            this.bNew.TabIndex = 1;
            this.bNew.Text = "&New";
            this.bNew.UseVisualStyleBackColor = true;
            this.bNew.Click += new System.EventHandler(this.bNew_Click);
            // 
            // bRename
            // 
            this.bRename.Location = new System.Drawing.Point(58, 3);
            this.bRename.Name = "bRename";
            this.bRename.Size = new System.Drawing.Size(46, 24);
            this.bRename.TabIndex = 2;
            this.bRename.Text = "&Rename";
            this.bRename.UseVisualStyleBackColor = true;
            this.bRename.Click += new System.EventHandler(this.bRename_Click);
            // 
            // bDelete
            // 
            this.bDelete.Location = new System.Drawing.Point(6, 2);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(46, 24);
            this.bDelete.TabIndex = 3;
            this.bDelete.Text = "&Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // listFile
            // 
            this.listFile.FormattingEnabled = true;
            this.listFile.Location = new System.Drawing.Point(2, 27);
            this.listFile.Name = "listFile";
            this.listFile.Size = new System.Drawing.Size(167, 264);
            this.listFile.TabIndex = 0;
            this.listFile.DoubleClick += new System.EventHandler(this.listFile_DoubleClick);
            this.listFile.SelectedIndexChanged += new System.EventHandler(this.listFile_SelectedIndexChanged);
            // 
            // Notebook
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(592, 320);
            this.Controls.Add(this.listPanel);
            this.Controls.Add(this.statusBar1);
            this.Controls.Add(this.TextArea);
            this.Menu = this.MenuBar;
            this.MinimumSize = new System.Drawing.Size(475, 194);
            this.Name = "Notebook";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Notebook";
            this.Resize += new System.EventHandler(this.Form_Resize);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form_Closing);
            this.Load += new System.EventHandler(this.Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel2)).EndInit();
            this.listPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        bool MCcheck;                           // For find / replace window, stands for Match Case
        bool checkText = false;
        private string entryName = "";          // Stores the currently opened entry
        bool notebookModified = false;          /* Set to true if TextArea ever changes,
                                                    set to false if you just recently saved Notebook. */
        static string SearchText = "";          // When using Find / Replace
        static string ReplaceText = "";         // When using Replace 
        static bool IsDirectionDownward = true; // When using Find / Replace
        XMLSettingAttribute attr;
        XmlConfigurationCategory cat;


        /// <summary>
        /// Where it all begins.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.Run(new Notebook());
        }

        #region Everything Important
        String tfileName = "Untitled";
        /// <summary>
        /// Constructor.
        /// </summary>
        public Notebook()
        {
            InitializeComponent();
            GetSettings();
        }

        /// <summary>
        /// On load, set up objects for writing to file, align everything, and populate
        /// the list box with data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Form_Load(object sender, System.EventArgs e)
        {
            attr = CeresBase.General.Utilities.GetAttribute<XMLSettingAttribute>(this.GetType(), true);
            cat = attr.Category.Categories["ProjectName", true]; //IN NEW VERSION JUST DELETE CONFIGURATION

            TextArea.Text = "Double click on an entry to the right or press New (Ctrl+N) to start.";

            populateList();

            bNew.Width = listPanel.Width / 3 - 2;
            bDelete.Width = bNew.Width;
            bRename.Width = bNew.Width;

            bNew.Left = 3;
            bRename.Left = bNew.Right;
            bDelete.Left = bRename.Right;

            //SAME STUFF THATS IN Resize
            //Vertical                
            bNew.Top = 0;
            bRename.Top = bNew.Top;
            bDelete.Top = bNew.Top;
            listPanel.Top = 0;
            listFile.Height = statusBar1.Top - listFile.Top - 1;
            TextArea.Top = 0;
            TextArea.Height = listFile.Height + listFile.Top;

            //Horizontal                
            TextArea.Width = this.Width - listPanel.Width - 7;
        }


        /// <summary>
        /// On resize, make everything flow.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Form_Resize(object sender, System.EventArgs e)
        {
            //Vertical                
            listFile.Height = statusBar1.Top - listFile.Top - 1;
            TextArea.Height = listFile.Height + listFile.Top;//listPanel.Bottom - fileNamePanel.Height - 5;

            //Horizontal                
            TextArea.Width = this.Width - listPanel.Width - 7;
        }

        /// <summary>
        /// On close, check if anything was modified since it was last saved and ask user to save.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Form_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (notebookModified)
            {
                DialogResult click = MessageBox.Show("Notebook has unsaved changes.\n\nSave them now?", "Closing Notebook", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (click == DialogResult.Yes)
                {
                    saveFile();
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Methods


        /// <summary>
        /// Lists all the entries by name into the list box for selection.
        /// </summary>
        private void populateList()
        {
            try
            {
                foreach (XmlConfigurationOption categ in attr.Category.Categories["ProjectName", true].Options)
                {
                    this.listFile.Items.Add(categ.DisplayName);
                }
                listFile.Sorted = true;
            }
            catch (Exception f) { CeresBase.Global.ErrorBox(f); }
            finally { }
        }

        /// <summary>
        /// Saves the font name and size to the registry.
        /// </summary>
        private void SaveSettings()
        {
            /*
            try
            {
                RegistryKey Reg = Registry.LocalMachine.OpenSubKey(@"Software\Apollo\Log", true);
                if (Reg == null)
                {
                    Reg = Registry.LocalMachine.CreateSubKey(@"Software\Apollo\Log");
                }
                // Now set it to specifications from TextArea
                Reg.SetValue("FontName", TextArea.Font.FontFamily.GetName(0));
                Reg.SetValue("FontSize", Convert.ToString(TextArea.Font.Size));
            }
            catch { }
            finally { }
             */
        }

        /// <summary>
        /// Loads the font name and size from the registry.
        /// </summary>
        private void GetSettings()
        {
            /*
            try
            {
                RegistryKey Reg = Registry.LocalMachine;
                Reg = Reg.OpenSubKey(@"Software\Apollo\Log", true);
                TextArea.Font = new System.Drawing.Font(Reg.GetValue("FontName").ToString(), Convert.ToSingle(Reg.GetValue("FontSize")));
            }
            catch { }
            finally { }
             */
        }

        /// <summary>
        /// Enables everything whenever a file is loaded
        /// </summary>
        private void enableAll()
        {
            TextArea.Enabled = true;
            fileMenuExport.Enabled = true;
            fileMenuPrint.Enabled = true;
            editMenuSelectAll.Enabled = true;
            editMenuTimeDate.Enabled = true;
            editMenuFind.Enabled = true;
            editMenuFindNext.Enabled = true;
            editMenuReplace.Enabled = true;
            editMenuGoTo.Enabled = true;


            if ((Clipboard.GetDataObject().GetDataPresent(DataFormats.Text)))
            { editMenuPaste.Enabled = true; }
            else { editMenuPaste.Enabled = false; }
        }

        /// <summary>
        /// Disables everything whenever a file is not loaded
        /// </summary>
        private void disableAll()
        {
            TextArea.Text = "Double click on an entry to the right or press New (Ctrl+N) to start.";
            TextArea.Enabled = false;
            fileMenuExport.Enabled = false;
            fileMenuPrint.Enabled = false;
            editMenuSelectAll.Enabled = false;
            editMenuTimeDate.Enabled = false;
            editMenuFind.Enabled = false;
            editMenuFindNext.Enabled = false;
            editMenuReplace.Enabled = false;
            editMenuGoTo.Enabled = false;
            editMenuPaste.Enabled = false;
            statusBar1.Panels[0].Text = "";
            entryName = "";
        }

        /// <summary>
        /// Method for creating a new file
        /// </summary>
        private void newFile()
        {
            XmlConfigurationOption entryName;// = cat.Options["EntryName" + TEMPVAR, true];  //can be an actual string that is modifiable

            try
            {
                CeresBase.InputBox Input = new CeresBase.InputBox(" New Entry", "Input name of new file.");
                DialogResult click2 = DialogResult.No;   // Needed a predefined value, I picked an arbitray one
                DialogResult click1;
                do
                {
                    click1 = Input.ShowDialog();
                    if (listFile.Items.IndexOf(Input.Value) != -1)
                    {
                        click2 = MessageBox.Show("File already exists.", "Notebook", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                        if (click2 == DialogResult.Cancel) { click1 = DialogResult.Cancel; }
                        else { click1 = DialogResult.Retry; }    // Necessary for the loop to continue
                    }
                    else { click2 = DialogResult.OK; }// Arbitray value, just necessary to keep things moving
                } while ((click1 != DialogResult.Cancel) && (click2 == DialogResult.Retry));
                if (click1 == DialogResult.OK)
                {
                    entryName = cat.Options[Input.Value, true];
                    listFile.Items.Add(Input.Value);
                    listFile.Sorted = true;
                    notebookModified = true;
                }

            }
            catch (Exception f) { CeresBase.Global.ErrorBox(f); }
            finally { }
        }

        /// <summary>
        /// Renames the entry that is currently selected
        /// </summary>
        private void renameFile()
        {
            try
            {
                CeresBase.InputBox Input = new CeresBase.InputBox(" Rename File", "Rename '" + listFile.SelectedItems[0].ToString() + "' to...");
                DialogResult click = Input.ShowDialog();
                if (click == DialogResult.OK)
                {
                    if (listFile.Items.IndexOf(Input.Value) != -1)
                    { CeresBase.Global.ErrorBox("File name already exists. Try again."); }
                    else
                    {
                        //Replace current entry name if it's the one that just got renamed and is open
                        if (entryName == listFile.SelectedItems[0].ToString())
                        { 
                            entryName = Input.Value;
                            statusBar1.Panels[0].Text = "Entry Name: " + entryName ;
                        }

                        XmlConfigurationOption option = cat.Options[listFile.SelectedItems[0].ToString()];
                        string tempContents = option.Value.ToString();
                        attr.Category.Categories["ProjectName", true].Options.Remove(option);
                        option = cat.Options[Input.Value, true];
                        option.Value = tempContents;

                        listFile.Sorted = true;

                        listFile.Items.Remove(this.listFile.SelectedItem);
                        listFile.Items.Add(Input.Value);
                        listFile.SelectedIndex = listFile.Items.IndexOf(Input.Value);
                        notebookModified = true;
                    }
                }
            }
            catch (System.IndexOutOfRangeException e) { } // User did not select an item from the list, so don't bug them
            catch (Exception f) { CeresBase.Global.ErrorBox(f); }
            finally { }
        }

        /// <summary>
        /// Deletes the entry that is currently selected.
        /// </summary>
        private void deleteFile()
        {
            try
            {
                DialogResult click;
                click = MessageBox.Show(this, "Are you sure you want to destroy '" + listFile.SelectedItems[0].ToString() + "'? \n\n", "Notebook", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (click == DialogResult.Yes)
                {
                    XmlConfigurationOption option = attr.Category.Categories["ProjectName", true].Options[listFile.SelectedItems[0].ToString()];
                    attr.Category.Categories["ProjectName", true].Options.Remove(option);

                    if (entryName == listFile.SelectedItems[0].ToString())
                    { disableAll(); }

                    this.listFile.Items.Remove(this.listFile.SelectedItem);
                    //TODO make it point to the next item on list  
                    notebookModified = true;
                }
            }
            catch (System.IndexOutOfRangeException) { } // User did not select an item from the list, so don't bug them
            catch (Exception f) { CeresBase.Global.ErrorBox(f); }
            finally { }
        }

        /// <summary>
        /// Saves all changes to file.
        /// </summary>
        private void saveFile()
        {
            notebookModified = false;
            attr.WriteToDisk();
        }

        /// <summary>
        /// Code for exiting the program properly
        /// </summary>
        private void exitProgram()
        {
            SaveSettings();
            if ((checkText) && (TextArea.Enabled))
            {
                DialogResult click = AskSaveChanges();
                if (click == DialogResult.Yes)
                {
                    saveFile();
                    this.Dispose();
                }
                // NOTE: putting this if branch above the last one makes it so that it
                //  doesnt close when cancel is pressed, even when all of them are else ifs
                else if (click == DialogResult.No)
                { this.Dispose(); }
                else
                { return; }
            }
            else
            { this.Dispose(); }
        }

        /// <summary>
        /// Ask user if they want to save changes
        /// Returns either DialogResult.Yes, No, or Cancel 
        /// </summary>
        /// <returns></returns>
        private DialogResult AskSaveChanges()
        {
            return MessageBox.Show(this, "The entry '" + entryName + "' may contain unsaved modifications.\n\nSave changes?", "Notebook", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
        }

        ///////////////////////////////
        // CHEK FOR MOUSE AND KEYBOARD
        ///////////////////////////////
        /// <summary>
        /// If TextArea was changed, set a boolean value true to alert the program of changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextArea_TextChanged(object sender, System.EventArgs e)
        { checkText = true; }

        /// <summary>
        /// Update position in status bar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextArea_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                notebookModified = true;
                int ColCount = 1;
                int RowCount = 1;
                int Pos;
                if (TextArea.SelectionStart > -1)
                {
                    Pos = TextArea.Text.LastIndexOf("\n", TextArea.SelectionStart);
                    if (Pos > -1)
                    {
                        if (Pos != TextArea.SelectionStart)
                            ColCount = TextArea.SelectionStart - Pos;
                        else
                        {
                            Pos = TextArea.Text.LastIndexOf("\n", TextArea.SelectionStart - 1);
                            ColCount = TextArea.SelectionStart - Pos;
                        }
                    }
                    else
                    {
                        ColCount = TextArea.SelectionStart + 1;
                    }
                    while (Pos > -1)
                    {
                        RowCount++;
                        Pos = TextArea.Text.LastIndexOf("\n", Pos - 1);
                    }
                }
                statusBar1.Panels[1].Text = "Ln " + RowCount.ToString() + ", " + "Col " + ColCount.ToString();
            }
            catch { }
            finally { }
        }

        /// <summary>
        /// Update position in status bar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextArea_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextArea_KeyUp(null, null);
        }

        /// <summary>
        /// Checks ifcertain keys were pressed, such as for selecting text, 
        /// and enable Cut, Copy and Delete from the Edit tool bar if some text
        /// is seleected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextArea_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1 && e.Modifiers == Keys.Shift ||
                e.KeyCode == Keys.Home && e.Modifiers == Keys.Shift ||
                e.KeyCode == Keys.End && e.Modifiers == Keys.Shift ||
                e.KeyCode == Keys.Right && e.Modifiers == Keys.Shift ||
                e.KeyCode == Keys.Left && e.Modifiers == Keys.Shift ||
                e.KeyCode == Keys.Up && e.Modifiers == Keys.Shift ||
                e.KeyCode == Keys.Down && e.Modifiers == Keys.Shift ||
                e.Modifiers == Keys.Alt || e.Modifiers == Keys.Control)
            {
                if (TextArea.SelectionLength > -1)
                {
                    editMenuCut.Enabled = true;
                    editMenuCopy.Enabled = true;
                    editMenuDelete.Enabled = true;
                }
            }
            else
            {
                editMenuCut.Enabled = false;
                editMenuCopy.Enabled = false;
                editMenuDelete.Enabled = false;
            }
        }

        /// <summary>
        /// Update position in status bar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextArea_MouseDown(object sender, MouseEventArgs e)
        {
            TextArea_KeyUp(null, null);
        }

        /// <summary>
        /// Enable Cut / Copy / Delete in Edit tool bar if selection was made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextArea_MouseUp(object sender, MouseEventArgs e)
        {
            if (TextArea.SelectionLength > 0)
            {
                editMenuCut.Enabled = true;
                editMenuCopy.Enabled = true;
                editMenuDelete.Enabled = true;
            }
            else
            {
                editMenuCut.Enabled = false;
                editMenuCopy.Enabled = false;
                editMenuDelete.Enabled = false;
            }
            TextArea_KeyUp(null, null);
        }


        ////////////////
        // FIND METHODS
        ////////////////
        /// <summary>
        /// Creates a window for finding text
        /// </summary>
        private void TextFindForm()
        {
            if (TextArea.Enabled)
            {
                Application.EnableVisualStyles();
                Label LabelFind = new Label();
                LabelFind.Location = new Point(5, 5);
                LabelFind.Text = "Search for text:";
                LabelFind.AutoSize = true;

                TextBox TextBoxFind = new TextBox();
                TextBoxFind.Location = new Point(LabelFind.Left + LabelFind.Width + 5, LabelFind.Top);
                TextBoxFind.Size = new Size(150, TextBoxFind.Height);
                TextBoxFind.Text = SearchText;
                TextBoxFind.TextChanged += new EventHandler(TextBoxFind_Change);

                Button ButtonFindNext = new Button();
                ButtonFindNext.Text = "Find Next";
                ButtonFindNext.DialogResult = DialogResult.OK;
                ButtonFindNext.Location = new Point(TextBoxFind.Left + TextBoxFind.Width + 5, LabelFind.Top - 2);
                ButtonFindNext.Click += new EventHandler(ButtonFindNext_Click);

                Button FindCancel = new Button();
                FindCancel.Text = "Cancel";
                FindCancel.DialogResult = DialogResult.Cancel;
                FindCancel.Location = new Point(ButtonFindNext.Left, ButtonFindNext.Top + ButtonFindNext.Height + 5);
                FindCancel.Click += new EventHandler(FindCancel_Click);

                GroupBox GroupDirection = new GroupBox();
                GroupDirection.Location = new Point(TextBoxFind.Left, FindCancel.Top);
                GroupDirection.Size = new Size(110, 47);
                GroupDirection.Text = "Direction";

                RadioButton OptionUp = new RadioButton();
                OptionUp.Location = new Point(10, 15);
                OptionUp.Click += new EventHandler(Direction_Click);
                OptionUp.Text = "Up";
                OptionUp.Width = 40;

                RadioButton OptionDown = new RadioButton();
                OptionDown.Location = new Point(50, 15);
                OptionDown.Text = "Down";
                OptionDown.Width = 55;
                OptionDown.Click += new EventHandler(Direction_Click);
                OptionDown.Checked = true;

                GroupDirection.Controls.Add(OptionUp);
                GroupDirection.Controls.Add(OptionDown);

                CheckBox CheckMatch = new CheckBox();
                CheckMatch.Location = new Point(LabelFind.Left, GroupDirection.Top);
                CheckMatch.Text = "Match case";
                CheckMatch.Checked = false;
                CheckMatch.Click += new EventHandler(CheckMatch_Click);

                if (TextBoxFind.Text.Trim().Length == 0)
                    ButtonFindNext.Enabled = false;

                Form FindDialog = new Form();
                FindDialog.Location = new Point(250, 350);
                FindDialog.Size = new Size(ButtonFindNext.Right + 15, GroupDirection.Bottom + 45);
                FindDialog.Text = " Find";
                FindDialog.MaximizeBox = false;
                FindDialog.MinimizeBox = false;
                FindDialog.ShowInTaskbar = false;
                FindDialog.HelpButton = true;
                FindDialog.FormBorderStyle = FormBorderStyle.FixedDialog;

                FindDialog.Controls.Add(LabelFind);
                FindDialog.Controls.Add(TextBoxFind);
                FindDialog.Controls.Add(ButtonFindNext);
                FindDialog.Controls.Add(FindCancel);
                FindDialog.Controls.Add(GroupDirection);
                FindDialog.Controls.Add(CheckMatch);
                FindDialog.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
                FindDialog.Show();
            }
        }

        /// <summary>
        /// Method for finding the next object that matches search parameters.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eArgs"></param>
        private void ButtonFindNext_Click(object sender, EventArgs eArgs)
        {
            int Pos = -1;

            if (IsDirectionDownward)
            {
                if (TextArea.SelectionLength == 0)
                    Pos = TextArea.Text.IndexOf(SearchText, TextArea.SelectionStart);
                else
                    Pos = TextArea.Text.IndexOf(SearchText, TextArea.SelectionStart + TextArea.SelectionLength);
            }
            else
            {
                if (TextArea.SelectionStart > 0)
                    Pos = TextArea.Text.LastIndexOf(SearchText, TextArea.SelectionStart - 1);
            }
            if (Pos != -1)
            {
                TextArea.SelectionStart = Pos;
                TextArea.SelectionLength = SearchText.Length;
                if (sender != null)
                    ((Control)sender).Focus();
            }
            else
            { CeresBase.Global.WarningBox("\"" + SearchText + "\" was not found."); }
        }

        private void TextBoxFind_Change(object sender, EventArgs eArgs)
        {
            SearchText = ((TextBox)sender).Text;
            Form FormTemp = (Form)(((Control)sender).Parent);
            for (int i = 0; i < FormTemp.Controls.Count; i++)
            {
                if (FormTemp.Controls[i].GetType() == typeof(Button) && FormTemp.Controls[i].Text != "&Cancel")
                {
                    FormTemp.Controls[i].Enabled = (((TextBox)sender).Text.Length > 0);
                }
            }
            FormTemp = null;
        }

        /// <summary>
        /// In find window, checks if up or down radio button is selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eArgs"></param>
        private void Direction_Click(object sender, EventArgs eArgs)
        {
            string Caption = ((RadioButton)sender).Text;
            if (Caption == "&Up")
                IsDirectionDownward = false;
            else
                IsDirectionDownward = true;
        }

        /// <summary>
        /// Closes Find window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FindCancel_Click(object sender, EventArgs e)
        {
            ((Form)((Control)sender).Parent).Close();
        }

        private void TextBoxReplace_Change(object sender, EventArgs eArgs)
        {
            ReplaceText = ((TextBox)sender).Text;
        }

        /// <summary>
        /// Replacements are made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eArgs"></param>
        private void ReplaceString_Click(object sender, EventArgs eArgs)
        {
            ButtonFindNext_Click(null, null);
            if (TextArea.SelectionLength > 0)
            {
                TextArea.SelectedText = ReplaceText;
                TextArea.SelectionStart = TextArea.SelectionStart + ReplaceText.Length;
            }
        }

        /// <summary>
        /// All replacements are made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eArgs"></param>
        private void ReplaceALL_Click(object sender, EventArgs eArgs)
        {
            TextArea.Text = ReplaceString(TextArea.Text, SearchText, ReplaceText);
        }

        /// <summary>
        /// Replace the actual string
        /// </summary>
        /// <param name="StrSource"></param>
        /// <param name="StrFind"></param>
        /// <param name="StrReplace"></param>
        /// <returns></returns>
        private string ReplaceString(string StrSource, string StrFind, string StrReplace)
        {
            int iPos = StrSource.IndexOf(StrFind);
            String StrReturn = "";

            while (iPos != -1)
            {
                StrReturn += StrSource.Substring(0, iPos) + StrReplace;
                StrSource = StrSource.Substring(iPos + StrFind.Length);
                iPos = StrSource.IndexOf(StrFind);
            }
            if (StrSource.Length > 0)
                StrReturn += StrSource;
            return StrReturn;
        }

        /// <summary>
        /// If "Match Case" is checked in the find form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckMatch_Click(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                MCcheck = true;
            else
                MCcheck = false;
        }

        /// <summary>
        /// Creates a form that asks which line the user would like to go to.
        /// </summary>
        /// <param name="LineNumber"></param>
        /// <returns></returns>
        public int GoToLine(string LineNumber)
        {
            Form GoToDialog = new Form();

            TextBox LineBox = new TextBox();
            LineBox.Text = LineNumber;
            LineBox.Location = new Point(10, 10);
            if (LineBox.Text.Length == 0)
                LineBox.Text = "0";

            Button GoToOK = new Button();
            GoToOK.Text = "OK";
            GoToOK.DialogResult = DialogResult.OK;
            GoToOK.Location = new Point(LineBox.Left + LineBox.Width + 20, 10);
            GoToOK.Click -= new EventHandler(Find_Click);

            Button GoToCancel = new Button();
            GoToCancel.Text = "Cancel";
            GoToCancel.Location = new Point(GoToOK.Left, GoToOK.Top + GoToOK.Height + 10);
            GoToCancel.DialogResult = DialogResult.Cancel;

            GoToDialog.Size = new Size(230, 100);
            GoToDialog.Text = "Go to Line";
            GoToDialog.AcceptButton = GoToOK;
            GoToDialog.CancelButton = GoToCancel;
            GoToDialog.MaximizeBox = false;
            GoToDialog.MinimizeBox = false;
            GoToDialog.FormBorderStyle = FormBorderStyle.FixedDialog;
            GoToDialog.Location = new Point(this.Left + Math.Abs((this.Width - GoToDialog.Width)) / 2, this.Top + 100);

            GoToDialog.Controls.Add(LineBox);
            GoToDialog.Controls.Add(GoToOK);
            GoToDialog.Controls.Add(GoToCancel);
            GoToDialog.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            GoToDialog.ShowDialog(this);

            if (GoToDialog.DialogResult == DialogResult.OK)
            {
                try
                {
                    GoToDialog.Dispose();
                    return Int32.Parse(LineBox.Text);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
            return 0;
        }

        /// <summary>
        /// Moves cursor to line specified
        /// </summary>
        /// <param name="LineNumber"></param>
        public void MoveToLine(int LineNumber)
        {
            int CurrentPos = TextArea.Text.IndexOf("\n");
            int LineCount = 0;
            bool IsMoved = false;
            while (CurrentPos > -1 && !IsMoved)
            {
                LineCount++;
                if (LineNumber == LineCount)
                {
                    TextArea.SelectionStart = CurrentPos - 1;
                    TextArea_KeyUp(null, null);
                    IsMoved = true;
                }
                CurrentPos = TextArea.Text.IndexOf("\n", CurrentPos + 1);
            }
            if (!IsMoved)
            {
                if (LineNumber == LineCount + 1)
                    TextArea.SelectionStart = TextArea.Text.Length;
                else
                    MessageBox.Show("Line was out of range.", "Notebook", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Shows replace form
        /// </summary>
        private void ShowReplace()
        {
            TextBox TextBoxFind = new TextBox();
            TextBox TextBoxReplace = new TextBox();
            Label LabelFind = new Label();
            Label LabelReplace = new Label();
            CheckBox CheckBoxMatch = new CheckBox();
            GroupBox GroupDirection = new GroupBox();
            RadioButton OptionUp = new RadioButton();
            RadioButton OptionDown = new RadioButton();
            Button ReplaceOK = new Button();
            Button ReplaceReplaceALL = new Button();
            Button ReplaceCancel = new Button();

            ReplaceOK.Text = "Replace";
            ReplaceOK.DialogResult = DialogResult.OK;
            ReplaceOK.Click += new EventHandler(ReplaceString_Click);

            ReplaceReplaceALL.Text = "Replace All";
            ReplaceReplaceALL.DialogResult = DialogResult.OK;
            ReplaceReplaceALL.Click += new EventHandler(ReplaceALL_Click);

            ReplaceCancel.Text = "Cancel";
            ReplaceCancel.DialogResult = DialogResult.Cancel;
            ReplaceCancel.Click += new EventHandler(FindCancel_Click);

            Form ReplaceDialog = new Form();
            ReplaceDialog.Size = new Size(360, 150);
            ReplaceDialog.Text = "Replace";
            ReplaceDialog.MaximizeBox = false;
            ReplaceDialog.MinimizeBox = false;
            ReplaceDialog.FormBorderStyle = FormBorderStyle.FixedDialog;

            LabelFind.Location = new Point(10, 15);
            LabelFind.Text = "Find:";
            LabelFind.AutoSize = true;

            TextBoxFind.Location = new Point(55, 15);
            TextBoxFind.Size = new Size(200, TextBoxFind.Height);
            TextBoxFind.Text = SearchText;
            TextBoxFind.TextChanged += new EventHandler(TextBoxFind_Change);

            LabelReplace.Location = new Point(10, 43);
            LabelReplace.Text = "Replace:";
            LabelReplace.AutoSize = true;

            TextBoxReplace.Location = new Point(55, 43);
            TextBoxReplace.Size = new Size(200, TextBoxFind.Height);
            TextBoxFind.Left = TextBoxReplace.Left;
            TextBoxReplace.Text = ReplaceText;
            TextBoxReplace.TextChanged += new EventHandler(TextBoxReplace_Change);


            ReplaceOK.Location = new Point(265, 15);
            ReplaceReplaceALL.Location = new Point(265, 43);
            ReplaceCancel.Location = new Point(265, 70);

            GroupDirection.Location = new Point(135, 70);
            GroupDirection.Size = new Size(120, 45);
            GroupDirection.Text = "Match Direction";

            OptionUp.Location = new Point(10, 15);
            OptionUp.Click += new EventHandler(Direction_Click);
            OptionUp.Text = "&Up";
            OptionUp.Width = 45;

            OptionDown.Text = "&Down";
            OptionDown.Width = 55;
            OptionDown.Click += new EventHandler(Direction_Click);
            OptionDown.Checked = true;
            OptionDown.Location = new Point(OptionUp.Left + OptionUp.Width, OptionUp.Top);

            GroupDirection.Controls.Add(OptionUp);
            GroupDirection.Controls.Add(OptionDown);

            CheckBoxMatch.Location = new Point(10, 85);
            CheckBoxMatch.Text = "Match Case";
            CheckBoxMatch.Checked = true;

            if (TextBoxFind.Text.Trim().Length == 0)
            {
                ReplaceOK.Enabled = false;
                ReplaceReplaceALL.Enabled = false;
            }

            ReplaceDialog.Controls.Add(TextBoxFind);
            ReplaceDialog.Controls.Add(LabelFind);
            ReplaceDialog.Controls.Add(TextBoxReplace);
            ReplaceDialog.Controls.Add(LabelReplace);
            ReplaceDialog.Controls.Add(GroupDirection);
            ReplaceDialog.Controls.Add(CheckBoxMatch);
            ReplaceDialog.Controls.Add(ReplaceOK);
            ReplaceDialog.Controls.Add(ReplaceReplaceALL);
            ReplaceDialog.Controls.Add(ReplaceCancel);
            ReplaceDialog.ShowInTaskbar = false;
            ReplaceDialog.TopMost = true;
            ReplaceDialog.HelpButton = true;
            ReplaceDialog.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ReplaceDialog.Show();

        }

        #endregion

        #region MENU STRIP: File
        /// <summary>
        /// File Menu -> Save
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileMenuSave_Click(object sender, EventArgs e)
        { saveFile(); }

        /// <summary>
        /// File -> New (disabled but allows shortcut)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileMenuNew_Click(object sender, EventArgs e)
        { newFile(); }

        /// <summary>
        /// File -> Rename (disabled but allows shortcut)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileMenuRename_Click(object sender, EventArgs e)
        { renameFile(); }

        /// <summary>
        /// File -> Delete (disabled but allows shortcut)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileMenuDelete_Click(object sender, EventArgs e)
        { deleteFile(); }

        /// <summary>
        /// Imports any document from a user specified location. 
        /// Changes extension to .txt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileMenuImport_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Filter = "Text Document (*.txt)|*.txt|All files|*.*";
                DialogResult resVal = openFileDialog1.ShowDialog();
                if (resVal == DialogResult.Cancel) { return; }
                string entryName = openFileDialog1.FileName,     // Manipulated to get entryName
                    fileNameUnedited = entryName;                // Unedited file name for reading
                do
                {
                    entryName = entryName.Remove(0, entryName.IndexOf(@"\") + 1);
                } while (entryName.IndexOf(@"\") != -1);
                // Just pray that they only have one period in the file name for the extension


                entryName = entryName.Remove(entryName.IndexOf("."), entryName.Length - entryName.IndexOf("."));

                DialogResult click = DialogResult.Retry;   // Arbitrary initial value just to make it happy
                CeresBase.InputBox Input = new CeresBase.InputBox("New Name", "Input new name.");
                while ((listFile.Items.IndexOf(entryName) != -1) && (click == DialogResult.Retry))
                {
                    if (listFile.Items.IndexOf(entryName) != -1)  // If entry name already exists
                    {
                        click = MessageBox.Show("Error: Entry name already exists.\nPress 'Retry' to rename it.", "Notebook", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                        if (click == DialogResult.Retry)
                        {
                            Input.ShowDialog();
                            entryName = Input.Value;
                        }
                        else
                        {
                            MessageBox.Show("Import cancelled.", "Notebook", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            goto ENDOFMETHOD;
                        }
                    }
                }

                StreamReader streamIn = new StreamReader(fileNameUnedited);
                string tempContents = streamIn.ReadToEnd();
                streamIn.Close();

                //CREATE ACTUAL ENTRY HERE
                XmlConfigurationOption option = cat.Options[entryName, true];
                option.Value = tempContents;

                this.listFile.Items.Add(entryName);
                listFile.Sorted = true;
                notebookModified = true;

            ENDOFMETHOD: ;
            }
            catch (Exception f)
            { CeresBase.Global.ErrorBox(f); }
            finally { }
        }

        /// <summary>
        /// Exports entry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileMenuExport_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.Filter = "Text Document (*.txt)|*.txt|All files|*.*";
                DialogResult resVal = saveFileDialog1.ShowDialog();
                if (resVal == DialogResult.Cancel) { return; }

                StreamWriter stWriter = new StreamWriter(saveFileDialog1.FileName);
                stWriter.WriteLine(TextArea.Text);
                stWriter.Flush();
                stWriter.Close();
                checkText = false;
            }
            catch
            {
                MessageBox.Show(this, "The entry could not be exported.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally { }
        }

        /// <summary>
        /// Allows you to set up margins and what not for printing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageSetup_Click(object sender, System.EventArgs e)
        {
            printDocument1.DocumentName = tfileName;
            pageSetupDialog1.Document = printDocument1;
            pageSetupDialog1.ShowDialog();
        }

        /// <summary>
        /// Ability to print the current document
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Print_Click(object sender, System.EventArgs e)
        {
            printDocument1.DocumentName = tfileName;
            printDialog1.Document = printDocument1;
            printDialog1.ShowDialog();
        }

        /// <summary>
        /// Exit program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit_Click(object sender, System.EventArgs e)
        { exitProgram(); }
        #endregion

        #region MENU STRIP: Edit
        /// <summary>
        /// Can use DEL key to delete text. Is invisible in the Edit box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Delete_Click(object sender, System.EventArgs e)
        {
            if (TextArea.SelectionLength > 0)
            {
                TextArea.SelectedText = "";
                TextArea_KeyUp(null, null);
                checkText = true;
            }
        }

        /// <summary>
        /// Selects all text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectAll_Click(object sender, System.EventArgs e)
        {
            TextArea.SelectAll();
            checkText = false;
        }

        /// <summary>
        /// Inserts the date and time at current location.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimeDate_Click(object sender, System.EventArgs e)
        {
            TextArea.Text = TextArea.Text + DateTime.Now.ToLongDateString() + " at " + DateTime.Now.ToLongTimeString();
            TextArea.SelectionLength = TextArea.Text.Length;
            TextArea.SelectionStart = TextArea.Text.Length;
            checkText = true;
        }

        /// <summary>
        /// Edit -> Cut
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cut_Click(object sender, System.EventArgs e)
        {
            if (TextArea.SelectedText != "" && editMenuCut.Enabled == true)
            {
                TextArea.Cut();
                TextArea_KeyUp(null, null);
                checkText = true;
                editMenuPaste.Enabled = true;
            }
        }

        /// <summary>
        /// Edit -> Copy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Copy_Click(object sender, System.EventArgs e)
        {
            if (TextArea.SelectionLength > 0)
            {
                TextArea.Copy();
                TextArea_KeyUp(null, null);
                checkText = false;
                editMenuPaste.Enabled = true;
            }
        }

        /// <summary>
        /// Edit -> Paste
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Paste_Click(object sender, System.EventArgs e)
        {
            if (Clipboard.GetDataObject().GetDataPresent(DataFormats.Text) == true)
            {
                TextArea.Paste();
            }
            TextArea_KeyUp(null, null);
            checkText = true;
        }

        /// <summary>
        /// Edit -> Find
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Find_Click(object sender, System.EventArgs e)
        {
            TextFindForm();
        }

        /// <summary>
        /// Edit -> Find Next
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FindNext_Click(object sender, System.EventArgs e)
        {
            ButtonFindNext_Click(null, null);
        }

        /// <summary>
        /// Edit -> Replace
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Replace_Click(object sender, System.EventArgs e)
        {
            ShowReplace();
        }

        /// <summary>
        /// Edit -> Goto line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoTo_Click(object sender, System.EventArgs e)
        {
            int LineNumber = GoToLine("");
            if (LineNumber > 0)
            {
                MoveToLine(LineNumber);
            }
            else
            {
                if (LineNumber <= 0) MessageBox.Show("Line number must be a non zero positive integer.", "Notebook", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            TextArea_KeyUp(null, null);
        }
        #endregion

        #region MENU STRIP: Format

        /// <summary>
        /// Change font
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void font_Click(object sender, System.EventArgs e)
        {
            fontDialog1.ShowColor = true;
            fontDialog1.ShowDialog();
            TextArea.Font = fontDialog1.Font;
            TextArea.ForeColor = fontDialog1.Color;
        }

        /// <summary>
        /// Toggle wordwrap
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WordWrap_Click(object sender, System.EventArgs e)
        {
            if (!(formatMenuWordWrap.Checked))
            {
                formatMenuWordWrap.Checked = true;
                TextArea.WordWrap = true;
            }
            else
            {
                formatMenuWordWrap.Checked = false;
                TextArea.WordWrap = false;
            }
        }
        #endregion


        #region Form Items
        /// <summary>
        /// Delete button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bDelete_Click(object sender, EventArgs e)
        { deleteFile(); }

        /// <summary>
        /// Rename button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bRename_Click(object sender, EventArgs e)
        { renameFile(); }

        /// <summary>
        /// New button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bNew_Click(object sender, EventArgs e)
        { newFile(); }

        /// <summary>
        /// Entry is double clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listFile_DoubleClick(object sender, System.EventArgs e)
        {
            XmlConfigurationOption entryNameOption;
            try
            {
                // If entry was loaded, save it to memory
                if (TextArea.Enabled == true)
                {
                    entryNameOption = cat.Options[entryName, true];
                    entryNameOption.Value = TextArea.Text;    //if there were changes to something, save it}
                }

                entryName = listFile.SelectedItems[0].ToString();
                entryNameOption = cat.Options[entryName, true];
                statusBar1.Panels[0].Text = "Entry Name: " + entryName;
                // Load document into TextArea            
                TextArea.Text = entryNameOption.Value.ToString();
                //attr.Configuration.Categories["ProjectName", true].Options[listFile.SelectedItems[0]].Value;

                checkText = false;
                enableAll();
                editMenuCopy.Enabled = false;
                editMenuCut.Enabled = false;
            }
            catch (System.IndexOutOfRangeException) { }  // User did not click on an item, so just don't do anything
            catch (Exception f) { CeresBase.Global.ErrorBox(f); disableAll(); }
            finally { }
        }
        #endregion

        #endregion

        private void listFile_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
