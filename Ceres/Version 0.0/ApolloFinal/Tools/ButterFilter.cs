using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using MathWorks.MATLAB.NET.Arrays;
using System.IO;

namespace ApolloFinal.Tools
{
    public static partial class TemporaryDataTools
    {
        public enum FilterType { HighPass, LowPass };

        public static SpikeVariable[] ButterFilter(SpikeVariable[] var, float thresholdFreq, FilterType filterType)
        {
            SpikeVariable[] toRet = new SpikeVariable[var.Length];

            for (int i = 0; i < var.Length; i++)
            {
                double freq = (var[i].Header as SpikeVariableHeader).Frequency;
                IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF((var[i][0] as DataFrame).Pool.DimensionLengths);

                try
                {
                    ButterworthFilter.ButterworthFilterclass filter = new ButterworthFilter.ButterworthFilterclass();
                    (var[i][0] as DataFrame).Pool.GetData(new int[] { 0 }, new int[] { (var[i][0] as DataFrame).Pool.DimensionLengths[0] },
                        delegate(float[] data, uint[] indices, uint[] counts)
                        {
                            MWNumericArray array = new MWNumericArray(data, false, true);
                            MWNumericArray threshold = new MWNumericArray(thresholdFreq);
                            MWNumericArray frequency = new MWNumericArray(freq);
                            MWCharArray type = null;
                            switch (filterType)
                            {
                                case FilterType.HighPass:
                                    type = new MWCharArray("high");
                                    break;
                                case FilterType.LowPass:
                                    type = new MWCharArray("low");
                                    break;
                            }


                            MWNumericArray filtered = (MWNumericArray)filter.ButterworthFilter(array, frequency, threshold, type);
                            float[] filteredF = (float[])filtered.ToVector(MWArrayComponent.Real);
                            using (MemoryStream stream = CeresBase.Data.DataUtilities.ArrayToMemoryStream(filteredF))
                            {
                                pool.SetData(stream);
                            }
                            filtered.Dispose();
                        }
                    );

                    SpikeVariableHeader header = new SpikeVariableHeader(var[i].Header.VarName + " Filtered", var[i].Header.UnitsName,
                        var[i].Header.ExperimentName, (var[i].Header as SpikeVariableHeader).Frequency,
                        (var[i].Header as SpikeVariableHeader).SpikeDesc);
                    header.Description = var[i].Header.Description + " Filtered";
                    toRet[i] = new SpikeVariable(header);
                    DataFrame frame = new DataFrame(toRet[i], pool, new CeresBase.Data.DataShapes.NoShape(), CeresBase.General.Constants.Timeless);
                    toRet[i].AddDataFrame(frame);
                }
                catch (Exception ex)
                {
                    int s = 0;
                }
            }
            return toRet;            
        }
    }
}
