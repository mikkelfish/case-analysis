using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools.PhaseSpace
{
    public class NPoint
    {
        private float[] points;
        public float[] Points
        {
            get { return points; }
        }

        public NPoint(int numDims)
        {
            this.points = new float[numDims];
        }
    }
}
