using System;
using System.Collections.Generic;
using System.Text;
using Sharp3D.Math.Core;

namespace ApolloFinal.Tools.PhaseSpace
{
    public class ReturnMap
    {
        private Matrix4F transformMatrix;
        public Matrix4F TransformMatrix
        {
            get { return transformMatrix; }
            set { transformMatrix = value; }
        }

        private int length;

        public int Length
        {
            get { return length; }
            set { length = value; }
        }
	

        private Sharp3D.Math.Core.Vector4F returnVector;
        public Vector4F ReturnVector
        {
            get
            {
                return this.returnVector;
            }
            set
            {
                this.returnVector = value;
            }
        }
    }
}
