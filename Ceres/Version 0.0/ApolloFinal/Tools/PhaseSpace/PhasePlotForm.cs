using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CeresBase.IO.ImageCreation;
using System.Reflection;
using System.Drawing.Drawing2D;
using Sharp3D.Math.Core;

namespace ApolloFinal.Tools.PhaseSpace
{
    public partial class PhasePlotForm : Form
    {
        private float begPercX = 0F;
        private float endPercX = 1.0F;
        private float begPercY = 0F;
        private float endPercY = 1.0F;

        private Bitmap map;

        private PhasePlot plot;
        public PhasePlot Plot
        {
            get { return plot; }
            set 
            { 
                plot = value;
                this.panel1.Invalidate();
            }
        }

        private int xDim;
        public int XDim
        {
            get { return xDim; }
            set { xDim = value; }
        }

        private int yDim;
        public int YDim
        {
            get { return yDim; }
            set { yDim = value; }
        }

         private ReturnMap retMap;
	
        private void draw(ImageCreator creator, ImageContainer container)
        {
            int topMargin = 5;
            int leftMargin = 30;
            int rightMargin = 5;
            int bottomMargin = 20;
            RectangleElement leftBlocker = new RectangleElement(container, new PointF(0,0), 
                leftMargin, this.panel1.Height);
            leftBlocker.Fill = true;
            leftBlocker.Color = Color.White;
            RectangleElement botBlocker = new RectangleElement(container, new PointF(0,this.panel1.Height-bottomMargin), 
                this.panel1.Width, bottomMargin);
            botBlocker.Fill = true;
            botBlocker.Color = Color.White;
            RectangleElement rightBlocker = new RectangleElement(container, 
                new PointF(this.panel1.Width-rightMargin, 0),
                rightMargin, this.panel1.Height);
            rightBlocker.Fill = true;
            rightBlocker.Color = Color.White;

            Line leftLine = new Line(container, new PointF(leftMargin, topMargin), 
                new PointF(leftMargin, this.panel1.Height - bottomMargin - topMargin));
            Line botLine =
                new Line(container, new PointF(leftMargin, this.panel1.Height - bottomMargin),
                    new PointF(this.panel1.Width - rightMargin, this.panel1.Height - bottomMargin));


            ImageContainer plotContainer = new ImageContainer(container, new Rectangle(leftMargin, topMargin, this.panel1.Width - leftMargin - rightMargin,
                this.panel1.Height - topMargin - bottomMargin));
            creator.AddImageElement(plotContainer);
            this.plot.Create2DPlot(creator, plotContainer, this.xDim, this.yDim, this.begPercX, this.endPercX, this.begPercY, this.endPercY);

            creator.AddImageElement(leftBlocker);
            creator.AddImageElement(botBlocker);
            creator.AddImageElement(rightBlocker);


            creator.AddImageElement(leftLine);
            creator.AddImageElement(botLine);

        }

        public PhasePlotForm()
        {
            InitializeComponent();
            PropertyInfo prop = typeof(Panel).GetProperty("DoubleBuffered",BindingFlags.NonPublic | BindingFlags.Instance);
            prop.SetValue(this.panel1, true, null);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            bool draw = false;
            lock (this)
            {
                draw = map == null;
            }

            if (draw)
            {
                GDI gdi = new GDI();
                ImageContainer container = new ImageContainer(null, new Rectangle(0,0,this.panel1.Size.Width, this.panel1.Size.Height));
                gdi.AddImageElement(container);
                this.draw(gdi, container);
                lock (this)
                {
                    map = new Bitmap(this.panel1.Width, this.panel1.Height);
                }
                Graphics g = Graphics.FromImage(map);
                gdi.Create(new object[] { g });
            }

            lock (this)
            {
                e.Graphics.DrawImage(map, 0, 0);
            }

            if (this.buttonCreate.BackColor == SystemColors.Control && this.beginningPoint != null && this.curPoint != null)
            {
                int begX = this.beginningPoint.X;
                int begY = this.beginningPoint.Y;
                int endX = this.curPoint.X;
                int endY = this.curPoint.Y;

                if (endX < begX)
                {
                    int temp = begX;
                    begX = endX;
                    endX = temp;
                }

                if (endY < begY)
                {
                    int temp = begY;
                    begY = endY;
                    endY = temp;
                }

                if (endY - begY != 0 && endX - begX != 0)
                {
                    Pen red = new Pen(Color.Red, 2F);
                    e.Graphics.DrawRectangle(red, begX, begY,
                        endX - begX, endY - begY);
                }
            }

            if (this.retMap != null)
            {
                if (this.retMap.ReturnVector != null && this.retMap.TransformMatrix != null)
                {                    
                    Pen orange = new Pen(Color.Orange, 5F);
                    //Matrix mat = new Matrix();
                    //MatrixMatrix3F tempMat = this.retMap.TransformMatrix.Minor(2, 2);
                    Vector4F vector = Matrix4F.Transform(this.retMap.TransformMatrix, this.retMap.ReturnVector);
                    e.Graphics.DrawLine(orange, this.retMap.TransformMatrix.M14, this.retMap.TransformMatrix.M24,
                        vector.X, vector.Y);
                    //  mat = new Matrix(

                  //  e.Graphics.DrawLine(orange,
                }
            }
        }

        private Point beginningPoint;
        private Point curPoint;

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {

            this.beginningPoint = e.Location;
            this.curPoint = e.Location;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {

                if (buttonCreate.BackColor == SystemColors.ControlDark)
                {

                    if (Form.ModifierKeys == Keys.Control)
                    {
                        Vector4F ret = this.retMap.ReturnVector;
                        if (this.curPoint.Y < e.Location.Y)
                            ret.X += 3;
                        else ret.X -= 3;
                        this.retMap.ReturnVector = ret;
                        
                    }
                    else
                    {
                        Matrix4F trans = this.retMap.TransformMatrix;
                        trans.M14 = e.Location.X;
                        trans.M24 = e.Location.Y;
                        this.retMap.TransformMatrix = trans;
                    }
                }

                this.curPoint = e.Location;                
                this.panel1.Invalidate();
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (buttonCreate.BackColor == SystemColors.ControlDark)
                {
                    Matrix4F rot = new Matrix4F();
                    if (this.curPoint.Y < e.Location.Y)
                        rot = Sharp3D.Math.Geometry3D.Transformation.RotateZ(Math.PI / 32);
                    else rot = Sharp3D.Math.Geometry3D.Transformation.RotateZ(-Math.PI / 32);
                    this.retMap.TransformMatrix = Matrix4F.Multiply(this.retMap.TransformMatrix, rot);

                    this.curPoint = e.Location;
                    this.panel1.Invalidate();
                
                }

            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
           
        }

        private void buttonZoom_Click(object sender, EventArgs e)
        {
            float tempEndX = this.endPercX;
            float tempEndY = this.endPercY;
            this.endPercX = this.begPercX + ((float)this.curPoint.X / (float)this.panel1.Width)*(this.endPercX - this.begPercX);
            this.endPercY = this.begPercY + ((float)this.curPoint.Y / (float)this.panel1.Height)*(this.endPercY - this.begPercY);
            this.begPercX = this.begPercX + ((float)this.beginningPoint.X / (float)this.panel1.Width)*(tempEndX - this.begPercX);
            this.begPercY = this.begPercY + ((float)this.beginningPoint.Y / (float)this.panel1.Height)*(tempEndY - this.begPercY);

            if (this.endPercX < this.begPercX)
            {
                float temp = this.endPercX;
                this.endPercX = this.begPercX;
                this.begPercX = temp;
            }

            if (this.endPercY < this.begPercY)
            {
                float temp = this.endPercY;
                this.endPercY = this.begPercY;
                this.begPercY = temp;
            }

            this.curPoint = new Point();
            this.beginningPoint = new Point();

            lock (this)
            {
                this.map = null;
            }

            this.panel1.Invalidate();            
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            this.begPercX = 0F;
            this.begPercY = 0F;
            this.endPercX = 1.0F;
            this.endPercY = 1.0F;
            lock (this)
            {
                this.map = null;
            }

            this.panel1.Invalidate(); 
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            lock (this)
            {
                this.map = null;
            }

            this.panel1.Invalidate(); 
        }

        private void buttonCreateReturn_Click(object sender, EventArgs e)
        {
            if (this.buttonCreate.BackColor == SystemColors.Control)
            {
                this.retMap = new ReturnMap();
                this.retMap.ReturnVector = new Sharp3D.Math.Core.Vector4F(this.panel1.Width / 10, 0, 0, 1);
                Matrix4F trans = Matrix4F.Identity;
                trans[0, 3] = this.panel1.Width / 2;
                trans[7] = this.panel1.Height / 2;
                this.retMap.TransformMatrix = trans;
                this.retMap.Length = 25;
                lock (this)
                {
                    this.map = null;
                }
                this.panel1.Invalidate();

                this.buttonCreate.BackColor = SystemColors.ControlDark;
            }
            else this.buttonCreate.BackColor = SystemColors.Control;

            // Sharp3D.Math.Core

         //    Vector4F place = new Vector4F(this.panel1.Width / 2, this.panel1.Height / 2, 0, 1);
             
            //this.retMap.TransformMatrix = this.retMap.TransformMatrix.;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            this.retMap = null;
            this.panel1.Invalidate();
        }

        private void buttonRunMap_Click(object sender, EventArgs e)
        {
            if(this.retMap == null || this.retMap.ReturnVector == null || this.retMap.TransformMatrix == null) return;
            Vector4F retVector = Matrix4F.Transform(this.retMap.TransformMatrix, this.retMap.ReturnVector);
            //put into y=mx+b
            float vectSlope = (retVector.Y - this.retMap.TransformMatrix.M24) / (retVector.X - this.retMap.TransformMatrix.M14);
            //y-y1=m(x-x1) y = mx - mx1 + y1 where y1-mx = b
            float b = this.retMap.TransformMatrix.M24 - vectSlope * this.retMap.TransformMatrix.M14;
            

          //  float y = 
        }

    }
}