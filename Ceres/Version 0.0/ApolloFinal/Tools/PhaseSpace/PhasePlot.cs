using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using System.Drawing;
using CeresBase.IO.ImageCreation;
using ApolloFinal.Tools.BatchProcessing;
using CeresBase.Projects.Flowable;

namespace ApolloFinal.Tools.PhaseSpace
{
    public class PhasePlot : IBatchViewable
    {
        private int dimensions;
        public int Dimensions
        {
            get { return dimensions; }
        }
	

        private IDataPool pool;
        public IDataPool Pool
        {
            get { return pool; }
        }

        public PhasePlot(int dimensions, IDataPool pool)
        {
            this.pool = pool;
            this.dimensions = dimensions;
        }

        public void Create2DPlot(ImageCreator creator, ImageContainer parent, int xdim, int ydim, float begXPerc, float endXPerc,
            float begYPerc, float endYPerc)
        {
            Lines lines = new Lines(parent);
            float[] buffer = new float[dimensions - 1];
            int bufferIndex = 0;

            float[] mins = new float[this.dimensions];
            float[] maxes = new float[this.dimensions];
            for (int i = 0; i < mins.Length; i++)
            {
                mins[i] = float.MaxValue;
                maxes[i] = float.MinValue;
            }

            int curDim = 0;
            this.pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        int dim = curDim % this.dimensions;
                        if (data[i] > maxes[dim])
                        {
                            maxes[dim] = data[i];
                        }
                        if (data[i] < mins[dim])
                        {
                            mins[dim] = data[i];
                        }
                        curDim++;
                    }
                }
            );

            float minx = mins[xdim];
            float maxx = maxes[xdim];
            mins[xdim] = minx + begXPerc * (maxx - minx);
            maxes[xdim] = minx + endXPerc * (maxx - minx);

            float miny = mins[ydim];
            float maxy = maxes[ydim];
            mins[ydim] = miny + begYPerc * (maxy - miny);
            maxes[ydim] = miny + endYPerc * (maxy - miny);

            

            curDim = 0;
            this.pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    int curSpot = 0;
                    while(true)
                    {
                        if (curSpot + dimensions > data.Length)
                        {
                            while (curSpot < data.Length)
                            {
                                buffer[bufferIndex] = data[curSpot];
                                bufferIndex++;
                                curSpot++;
                            }
                            break;
                        }
                        else
                        {
                            PointF point = new PointF();
                            if (bufferIndex > 0)
                            {
                                for (int j = 0; j < bufferIndex; j++)
                                {
                                    float perc = (buffer[j] - mins[curDim]) / (maxes[curDim] - mins[curDim]);
                                    if (curDim == xdim)
                                    {
                                        point.X = parent.Location.X + parent.Size.Width * perc;
                                    }
                                    if (curDim == ydim)
                                    {
                                        point.Y = parent.Location.Y + parent.Size.Height * perc;
                                    }
                                    curDim++;
                                    if (curDim == dimensions)
                                    {
                                        curDim = 0;
                                        break;
                                    }
                                }
                                bufferIndex = 0;
                            }

                            for (int j = curDim; j < dimensions; j++)
                            {
                                float perc = (data[curSpot] - mins[curDim]) / (maxes[curDim] - mins[curDim]);
                                if (curDim == xdim) point.X = parent.Location.X + parent.Size.Width * perc;
                                if (curDim == ydim) point.Y = parent.Location.Y + parent.Size.Height * perc;
                                curSpot++;
                                curDim++;
                                if (curDim == dimensions)
                                {
                                    curDim = 0;
                                    break;
                                }
                            }
                            lines.AddPoint(point);
                        }
                    }

                }
            );

            creator.AddImageElement(lines);
        }

        public IBatchViewable[] View(IBatchViewable[] viewable)
        {
            return this.View(viewable, false);
        }

        public IBatchViewable[] View(IBatchViewable[] viewable, bool useManager)
        {
            foreach (IBatchViewable view in viewable)
            {
                if (view is PhasePlot)
                {
                    PhasePlot plot = view as PhasePlot;
                    PhasePlotForm form = new PhasePlotForm();
                    form.Plot = plot;
                    form.XDim = 0;
                    form.YDim = 1;
                    form.Show();
                    
                }
            }

            return null;
        }
    }
}
