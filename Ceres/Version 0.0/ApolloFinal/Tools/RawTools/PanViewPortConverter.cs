﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace ApolloFinal.Tools.RawTools
{
    public class PanViewPortConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
                                    //            <Binding ElementName="Control" Path="StartTime" />
                                    //<Binding ElementName="Control" Path="EndTime" />
                                    //<Binding ElementName="Control" Path="ScreenWidth" />
                                    //<Binding ElementName="Control" Path="OriginalStart" />
                                    //<Binding ElementName="Control" Path="OriginalEnd" />
            float start = (float)values[0];
            float end = (float)values[1];
            float width = (float)values[2];
            float origstart = (float)values[3];
            float origend = (float)values[4];

            if (start > end || width <= 0 || origstart > origend || start < origstart || end > origend) return new TranslateTransform();

            double percResize = (origend - origstart) / (end - start);
            ScaleTransform scale = new ScaleTransform(percResize, 1.0);

            return scale;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
