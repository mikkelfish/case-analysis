﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;

namespace ApolloFinal.Tools.RawTools
{
    [ValueConversion(typeof(float[]), typeof(PointCollection))]
    public class RawDataToPointsConverter : IValueConverter
    {
        public double Width { get; set; }
        public double Height { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is float[]) || targetType != typeof(PointCollection)) return null;

            float[] data = value as float[];

            PointCollection toReturn = new PointCollection();
            double perPt = this.Width / data.Length;
            float min = data.Min();
            float max = data.Max();


            for (int i = 0; i < data.Length; i++)
            {
                toReturn.Add(new Point((double)i * perPt, this.Height * (max - data[i]) / (max - min)));
            }

            return toReturn;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
