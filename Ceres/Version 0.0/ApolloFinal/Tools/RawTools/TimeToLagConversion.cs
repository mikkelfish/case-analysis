﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.Projects.Flowable;
using MesosoftCommon.Utilities.Validations;

namespace ApolloFinal.Tools.RawTools
{
    [AdapterDescription("Raw")]
    class TimeToLagConversion : IBatchFlowable, IRoutineCategoryProvider
    {

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            return new BatchProcessableParameter[]{
                new BatchProcessableParameter()
                {
                    Name = "Time",
                    Val = 0.0,
                    Validator = new ComparisonValidator()
                    {
                        CompareTo = 0.0,
                        Operator = ComparisonValidator.Comparison.GreaterThan,
                        TreatNonComparableAsSuccess = true
                    }

                },
                new BatchProcessableParameter()
                {
                    Name = "Data Res.",
                    Editable = false
                }
            };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            double time = (double)parameters.Single(p => p.Name == "Time").Val;
            double res = (double)parameters.Single(p => p.Name == "Data Res.").Val;

            return new object[] { (int)(time / res) };
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Lag" }; }
        }

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Raw Utilities"; }
        }

        public override string ToString()
        {
            return "Time To Lag Conversion";
        }

        #endregion
    }
}
