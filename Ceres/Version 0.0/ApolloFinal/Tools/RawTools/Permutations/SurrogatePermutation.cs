﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;

namespace ApolloFinal.Tools.RawTools.Permutations
{
    [AdapterDescription("Raw")]
    public class SurrogatePermutation : AssociateAdapterBase, IPermutationAdapter
    {

        public override string Name
        {
            get
            {
                return "Surrogate Permutation";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Raw Data Permutations";
            }
            set
            {
                
            }
        }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            throw new NotImplementedException();
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            SurrogatePermutation perm = new SurrogatePermutation();
            return perm;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            throw new NotImplementedException();
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }

        #region IPermutationAdapter Members

        public int IterationCount
        {
            get { throw new NotImplementedException(); }
        }

        public void SetIteration(int iteration)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
