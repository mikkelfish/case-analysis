﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;

namespace ApolloFinal.Tools.RawTools.Transforms
{
    [AdapterDescription("Raw")]
    public class ChangeRange : AssociateAdapterBase
    {

        public override string Name
        {
            get
            {
                return "Scale to Low/High";
            }
            set
            {
               
            }
        }

        public override string Category
        {
            get
            {
                return "Data Transforms";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("Data")]
        public float[] Data { get; set; }

        [AssociateWithProperty("Low")]
        public float Min { get; set; }

        [AssociateWithProperty("High")]
        public float Max { get; set; }

        [AssociateWithProperty("Output", IsOutput=true)]
        public float[] Output { get; set; }

        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            if (this.Data == null) return;

            float[] output = new float[this.Data.Length];
            for (int i = 0; i < output.Length; i++)
            {
                output[i] = (float)(this.Data[i] - this.Min) / (this.Max - this.Min);
            }
      

            this.Output = output;
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            ChangeRange rang = new ChangeRange();
            rang.Data = this.Data;
            rang.Min = this.Min;
            rang.Max = this.Max;
            return rang;
        }

        public override CeresBase.FlowControl.ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Connect to data source.");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
