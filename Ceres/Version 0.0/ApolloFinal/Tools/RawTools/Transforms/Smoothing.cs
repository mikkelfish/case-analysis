﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using MesosoftCommon.Utilities.Validations;

namespace ApolloFinal.Tools.RawTools
{
    [AdapterDescription("Raw")]
    class Smoothing : IBatchFlowable, IRoutineCategoryProvider
    {
        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                CanLinkFrom = false,
                Description = "Data",
                Editable = false,
                Name = "Data",
                Validator = new NotNullValidator(),
                TargetType = typeof(float[])
            };

            BatchProcessableParameter inputFrequency = new BatchProcessableParameter()
            {
                Name = "Input Resolution",
                Editable = false,
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true,
                    Message = "Link to \"Resolution\" output from data adapter."
                }
            };

            BatchProcessableParameter outputFrequency = new BatchProcessableParameter()
            {
                Name = "Output Frequency",
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true
                }
            };

            BatchProcessableParameter timeWindow = new BatchProcessableParameter()
            {
                Name = "Falloff",
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 1.0,
                    Operator = ComparisonValidator.Comparison.LessThan,
                    TreatNonComparableAsSuccess = true,
                    Message = "Falloff must between 0 and 1"
                }
            };

            //BatchProcessableParameter simple = new BatchProcessableParameter()
            //{
            //    Name = "Simple MA",
            //    Val = false
            //};

            return new BatchProcessableParameter[] { data, inputFrequency, outputFrequency, timeWindow };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            double inputFrequency = 1.0 / Convert.ToDouble(parameters.Single(p => p.Name == "Input Resolution").Val);
            double outputFrequncy = Convert.ToDouble(parameters.Single(p => p.Name == "Output Frequency").Val);
            double falloff = Convert.ToDouble(parameters.Single(p => p.Name == "Falloff").Val);
            //bool simple = (bool)parameters.Single(p => p.Name == "Simple MA").Val;
            float[] data = parameters.Single(p => p.Name == "Data").Val as float[];


            double writeEvery = inputFrequency / outputFrequncy;
            double runningAvg = 0;
            int count = 0;
            double current = writeEvery;

            int ilength = (int)Math.Floor(data.Length / writeEvery);
            double dlength = data.Length / writeEvery;
            int length;
            if (ilength == dlength) length = ilength;
            else length = ilength;

            float[] outputData = new float[length];
            int wrote = 0;

            for (int i = 0; i < data.Length; i++)
            {
                runningAvg = runningAvg * falloff + (1.0 - falloff) * data[i];
                count++;
                if (count >= current)
                {
                    outputData[wrote] = (float)runningAvg;
                    wrote++;
                    current += writeEvery;
                }
            }

            return new object[] { outputData, 1.0/outputFrequncy };

            //if (simple)
            //{
            //    int numberOfPoints = (int)(inputFrequency * timeWindow);

            //    int count = 0;
            //    float average = 0;
            //    for (int i = 0; i < data.Length; i++)
            //    {
            //        if (count == numberOfPoints)
            //        {
            //            toRet.Add(average / (float)numberOfPoints);
            //            count = 0;
            //            average = 0;
            //        }

            //        average += numberOfPoints;
            //        count++;
            //    }
            //}
            //else
            //{


            //}

            
        }

        public override string ToString()
        {
            return "Smoothing";
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Smoothed Data", "Output Res." }; }
        }

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Data Transforms"; }
        }

        #endregion
    }
}
