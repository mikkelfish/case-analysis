﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;

namespace ApolloFinal.Tools.RawTools.Transforms
{
    [AdapterDescription("Mixed")]
    public class AverageRaw : AssociateAdapterBase
    {
        public enum AverageMode { Add, Multiply, Subtract };

        public override string Name
        {
            get
            {
                return "Combine Raw";
            }
            set
            {
            }
        }

        public override string Category
        {
            get
            {
                return "Data Transforms";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("Data")]
        public float[][] Data { get; set; }

        [AssociateWithProperty("Mode")]
        public AverageMode Mode { get; set; }

        [AssociateWithProperty("Resolution")]
        public double[] Resolution { get; set; }

        [AssociateWithProperty("Start Time")]
        public double StartTime { get; set; }

        [AssociateWithProperty("Length")]
        public double Length { get; set; }

        [AssociateWithProperty("Output", IsOutput=true)]
        public float Output { get; set; }

        public override event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            //First average the data    
            float[] total = new float[this.Data.Length];
            for (int i = 0; i < this.Data.Length; i++)
            {
                int startPlace = (int)(this.StartTime / this.Resolution[i]);
                int amount = (int)(this.Length / this.Resolution[i]);
                float avg = 0;
                int realAmount = 0;
                for (int j = 0; j < amount; j++)
                {
                    if (j + startPlace >= this.Data[i].Length)
                        break;

                    avg += this.Data[i][j + startPlace];
                    realAmount++;
                }

                avg /= realAmount;
                total[i] = avg;
            }

            float toRet = 0;
            if (this.Mode == AverageMode.Add)
            {
                for (int i = 0; i < total.Length; i++)
                {
                    toRet += total[i];
                }
            }
            else if (this.Mode == AverageMode.Multiply)
            {
                toRet = total[0];
                for (int i = 1; i < total.Length; i++)
                {
                    toRet *= total[i];
                }
            }
            else if (this.Mode == AverageMode.Subtract)
            {
                toRet = total[0];
                for (int i = 1; i < total.Length; i++)
                {
                    toRet -= total[i];
                }
            }

            toRet /= total.Length;

            this.Output = toRet;
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
           
        }

        public override object Clone()
        {
            AverageRaw raw = new AverageRaw();
            raw.Data = this.Data;
            raw.Length = this.Length;
            raw.Mode = this.Mode;
            raw.Resolution = this.Resolution;
            raw.StartTime = this.StartTime;
            return raw;
        }

        public override CeresBase.FlowControl.ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Connect to data adapter");
            }

            if (targetPropertyname == "Resolution" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Connect to resolution");
            }

            if (targetPropertyname == "Length")
            {
                if(targetValue is double && (double)targetValue == 0)
                     return new ValidationStatus(ValidationState.Fail, "Value must be greater than 0");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
