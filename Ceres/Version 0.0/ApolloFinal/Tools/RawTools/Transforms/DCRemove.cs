﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using MesosoftCommon.Utilities.Validations;

namespace ApolloFinal.Tools.RawTools
{
    [AdapterDescription("Raw")]
    class DCRemove : IBatchFlowable, IRoutineCategoryProvider
    {

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                CanLinkFrom = false,
                Description = "Data",
                Editable = false,
                Name = "Data",
                Validator = new NotNullValidator()
            };

            BatchProcessableParameter inputRes = new BatchProcessableParameter()
            {
                Name = "Input resolution",
                Val = null,
                Validator = new NotNullValidator(),
                Editable=false
            };

            BatchProcessableParameter window = new BatchProcessableParameter()
            {
                Name = "Time Window (sec)",
                Val = 0.0,
                Validator = new NotNullValidator()
            };

            return new BatchProcessableParameter[] { data, inputRes, window };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            float[] data = parameters.Single(p => p.Name == "Data").Val as float[];
            double res = (double)parameters.Single(p => p.Name == "Input resolution").Val;
            double window = (double)parameters.Single(p => p.Name == "Time Window (sec)").Val;

            float[] toRet = new float[data.Length];

            int numPointsToAvg = (int)(window / res);
            if (window == -1)
                numPointsToAvg = toRet.Length;
            
            
            float lastAvg = 0;
            for (int i = 0; i < data.Length; i+=numPointsToAvg)
            {
                int realNumToAvg = numPointsToAvg;
                if (i + numPointsToAvg > data.Length)
                {
                    realNumToAvg = data.Length - i;
                }
                else
                {
                    float avg = 0;
                    for (int j = 0; j < numPointsToAvg; j++)
                    {
                        avg += data[i + j];
                    }
                    avg /= numPointsToAvg;
                    lastAvg = avg;
                }
                for (int j = 0; j < realNumToAvg; j++)
                {
                    toRet[i + j] = data[i + j] - lastAvg;
                }                
            }

            return new object[] { toRet };
        }

        public string[] OutputDescription
        {
            get { return new string[] { "DC Removed Data" }; }
        }

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Data Transforms"; }
        }

        public override string ToString()
        {
            return "DC Remove";
        }

        #endregion
    }
}
