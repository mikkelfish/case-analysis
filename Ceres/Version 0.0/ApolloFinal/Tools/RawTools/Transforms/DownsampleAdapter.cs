﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.Projects.Flowable;
using MesosoftCommon.Utilities.Validations;

namespace ApolloFinal.Tools.RawTools
{
    [AdapterDescription("Raw")]
    public class DownsampleAdapter : IBatchFlowable, IRoutineCategoryProvider
    {

        public string Category
        {
            get
            {
                return "Data Transforms";
            }
            set
            {
                
            }
        }

        public override string ToString()
        {
            return "Subsample";
        }


        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                CanLinkFrom = false,
                Description = "Data",
                Editable = false,
                Name = "Data",
                Validator = new NotNullValidator()
            };

            BatchProcessableParameter subsample = new BatchProcessableParameter()
            {
                Name = "Subsample param (F for freq)",
                Val = "1",                
            };

            BatchProcessableParameter inputRes = new BatchProcessableParameter()
            {
                Name = "Input resolution",
                Val = null,
                Validator = new NotNullValidator(),
                Editable = false
            };

            BatchProcessableParameter startPosition = new BatchProcessableParameter()
            {
                Name = "Start position",
                Val = 0,
            };

            return new BatchProcessableParameter[] { data, inputRes, subsample, startPosition };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            float[] data = parameters.Single(p => p.Name == "Data").Val as float[];

            string substring = (string)parameters.Single(p => p.Name == "Subsample param (F for freq)").Val;
            int start = (int)parameters.Single(p => p.Name == "Start position").Val;
            double res = (double)parameters.Single(p => p.Name == "Input resolution").Val;

            double sub = 0;
            double outputRes = 0;
            if (!substring.ToLower().Contains("f"))
            {
                sub = double.Parse(substring);
                outputRes = res * sub;
            }
            else
            {
                substring = substring.Substring(0, substring.ToLower().IndexOf('f'));
                outputRes = 1.0/double.Parse(substring);

                sub = outputRes / res;

            }

            

            List<float> subValues = new List<float>();
            for (double i = start; i < data.Length; i+=sub)
            {
                subValues.Add(data[(int)i]);
            }

            return new object[] { subValues.ToArray(), outputRes };


        }

        public string[] OutputDescription
        {
            get { return new string[] { "Sampled Data", "Output Resolution" }; }
        }

        #endregion
    }
}
