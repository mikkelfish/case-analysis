﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;
using CeresBase.FlowControl;
using CeresBase.Projects.Flowable;
using MesosoftCommon.Utilities.Validations;
using CeresBase.FlowControl.Adapters.Matlab;

namespace ApolloFinal.Tools.RawTools
{
    [AdapterDescription("Raw")]
    class Integrator : IBatchFlowable, IRoutineCategoryProvider
    {
        public override string ToString()
        {
            return "Integration";
        }

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                CanLinkFrom = false,
                Description = "Data",
                Editable = false,
                Name = "Data",
                Validator = new NotNullValidator(),
                TargetType=typeof(float[])
            };

            BatchProcessableParameter inputFrequency = new BatchProcessableParameter()
            {
                Name = "Input Resolution",
                Editable = false,
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true,
                    Message = "Link to \"Resolution\" output from data adapter."
                }
            };

            BatchProcessableParameter outputFrequency = new BatchProcessableParameter()
            {
                Name = "Output Frequency",
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
               {
                   CompareTo = 0.0,
                   Operator = ComparisonValidator.Comparison.GreaterThan,
                   TreatNonComparableAsSuccess = true
               }
            };

            BatchProcessableParameter timeConstant = new BatchProcessableParameter()
            {
                Name = "Time Constant",
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
              {
                  CompareTo = 0.0,
                  Operator = ComparisonValidator.Comparison.GreaterThan,
                  TreatNonComparableAsSuccess = true
              }
            };

            return new BatchProcessableParameter[] { data, inputFrequency, timeConstant, outputFrequency };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            bool isSingle = false;
            float[][] data = parameters.Single(p => p.Name == "Data").Val as float[][];
            double[] inputFrequency = null; 
            if (data == null)
            {
                data = new float[1][] { parameters.Single(p => p.Name == "Data").Val as float[] };
                isSingle = true;
            }

            if (!(parameters.Single(p => p.Name == "Input Resolution").Val is double[]))
            {
                inputFrequency = new double[1] { 1.0 / Convert.ToDouble(parameters.Single(p => p.Name == "Input Resolution").Val) };

            }
            else
            {
                double[] res = parameters.Single(p => p.Name == "Input Resolution").Val as double[];
                inputFrequency = new double[res.Length];
                for (int i = 0; i < res.Length; i++)
                {
                    inputFrequency[i] = 1.0 / res[i];
                }
            }

            
            double outputFrequncy = Convert.ToDouble(parameters.Single(p => p.Name == "Output Frequency").Val);
            double timeConstant = Convert.ToDouble(parameters.Single(p => p.Name == "Time Constant").Val);

          

            List<float[]> totalOuts = new List<float[]>();
            List<double> totalRes = new List<double>();

            outputFrequncy = calcIntegration(data, inputFrequency, outputFrequncy, timeConstant, totalOuts, totalRes);

            if (isSingle)
                return new object[] { totalOuts.ToArray()[0], totalRes.ToArray()[0] };

            return new object[] { totalOuts.ToArray(), totalRes.ToArray() };
        }

        private static double calcIntegration(float[][] data, double[] inputFrequency, double outputFrequncy, double timeConstant, List<float[]> totalOuts, List<double> totalRes)
        {
            for (int i = 0; i < data.Length; i++)
            {
                    float decay = (float)Math.Pow(Math.E, -(1000.0 / inputFrequency[i] / timeConstant));
                    double writeEvery = inputFrequency[i] / outputFrequncy;
                    double curIndex = 0;

                    int ilength = (int)Math.Floor(data[i].Length / writeEvery);
                    double dlength = data[i].Length / writeEvery;
                    int length;
                    if (ilength == dlength) length = ilength - 1;
                    else length = ilength;


                    int readCount = 0;
                    float analogValue = 0;
                    double current = writeEvery;
                    int wrote = 0;

                    float avg = 0;
                    for (int j = 0; j < data[i].Length; j++)
                    {
                        avg += data[i][j];
                    }
                    avg /= data[i].Length;

                    int buildup = (int)((1000.0 * timeConstant / inputFrequency[i]) * 1000);


                    for (int j = 0; j < buildup; j++)
                    {
                        analogValue = analogValue * decay + (float)Math.Abs(data[i][j] - avg);
                    }

                    //    analogValue /= buildup;

                    List<float> outputData = new List<float>();


                    CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.BuildAdapterInfos("Raw");
                    MatlabAdapter adapter = MatlabLoader.GetAdapter("Data Transforms", "resampleData");

                    if (adapter != null)
                    {
                        List<float> vals = new List<float>();
                        for (int j = buildup; j < data[i].Length; j++)
                        {
                            analogValue = analogValue * decay + (float)Math.Abs(data[i][j] - avg);
                            vals.Add(analogValue);
                        }

                        object[] output = adapter.Run(new object[] { vals.ToArray(), (int)writeEvery, 1.0 / inputFrequency[i] });
                        totalOuts.Add(output[0] as float[]);
                        outputFrequncy = 1.0 / (float)output[1];
                    }
                    else
                    {

                        for (int j = buildup; j < data[i].Length; j++, readCount++)
                        {
                            analogValue = analogValue * decay + (float)Math.Abs(data[i][j] - avg);
                            if (readCount >= current)
                            {
                                outputData.Add(analogValue);
                                current += writeEvery;
                            }
                        }
                        totalOuts.Add(outputData.ToArray());
                    }

                totalRes.Add(1.0 / outputFrequncy);
            }
            return outputFrequncy;
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Output Data", "Output Resolution" }; }
        }

 

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Data Transforms"; }
        }

        #endregion
    }
}
