﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using MesosoftCommon.Utilities.Validations;

namespace ApolloFinal.Tools.RawTools.Transforms
{
    [AdapterDescription("Raw")]
    class StraightInt : IBatchFlowable, IRoutineCategoryProvider
    {
        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                CanLinkFrom = false,
                Description = "Data",
                Editable = false,
                Name = "Data",
                Validator = new NotNullValidator(),
                TargetType = typeof(float[])
            };

            return new BatchProcessableParameter[] { data };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            float[] data = parameters.Single(p => p.Name == "Data").Val as float[];
            float[] intData = new float[data.Length];
            float sum = 0;
            for (int i = 0; i < data.Length; i++)
            {
                sum += data[i];
                intData[i] = sum;
            }

            return new object[] { intData };
        }

        public string[] OutputDescription
        {
            get { return new string[]{"Integrated Data"}; }
        }

        public override string ToString()
        {
            return "Real Integration";
        }

        

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Data Transforms"; }
        }

        #endregion
    }
}
