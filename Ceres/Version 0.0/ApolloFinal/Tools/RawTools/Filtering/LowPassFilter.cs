﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using MesosoftCommon.Utilities.Validations;

namespace ApolloFinal.Tools.RawTools
{
    [AdapterDescription("Raw")]
    class LowPassFilter : IBatchFlowable, IRoutineCategoryProvider
    {
        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                CanLinkFrom = false,
                Description = "Data",
                Editable = false,
                Name = "Data",
                Validator = new NotNullValidator(),
                TargetType = typeof(float[])
            };

            BatchProcessableParameter inputFrequency = new BatchProcessableParameter()
            {
                Name = "Input Resolution",
                Editable = false,
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true,
                    Message = "Link to \"Resolution\" output from data adapter."
                }
            };


            BatchProcessableParameter lowFrequency = new BatchProcessableParameter()
            {
                Name = "High Cutoff",
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true,
                    Message = "Must be greater than 0"
                }
            };



            return new BatchProcessableParameter[] { data, inputFrequency, lowFrequency };
        }

        public override string ToString()
        {
            return "Low Pass Filter";
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            double inputFrequency = 1.0 / Convert.ToDouble(parameters.Single(p => p.Name == "Input Resolution").Val);
            double highCut = Convert.ToDouble(parameters.Single(p => p.Name == "High Cutoff").Val);

            float[] data = parameters.Single(p => p.Name == "Data").Val as float[];

            AllMatlabNative.All mat = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;
            lock (CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabLockable)
            {
                double[,] output = mat.ButterworthFilter(data, inputFrequency, highCut, "low") as double[,];
                return new object[] { CeresBase.General.Utilities.SquareArrayToDoubleArray<double>(output)[0] };
            }
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Filtered Output" }; }
        }

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Filtering Tools"; }
        }

        #endregion
    }
}
