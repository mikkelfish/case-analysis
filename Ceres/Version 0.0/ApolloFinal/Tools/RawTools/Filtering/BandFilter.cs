﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl.Adapters;
using MesosoftCommon.Utilities.Validations;

namespace ApolloFinal.Tools.RawTools
{
    [AdapterDescription("Raw")]
    class BandFilter : IBatchFlowable, IRoutineCategoryProvider
    {

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                CanLinkFrom = false,
                Description = "Data",
                Editable = false,
                Name = "Data",
                Validator = new NotNullValidator(),
                TargetType = typeof(float[])
            };

            BatchProcessableParameter inputFrequency = new BatchProcessableParameter()
            {
                Name = "Input Resolution",
                Editable = false,
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true,
                    Message = "Link to \"Resolution\" output from data adapter."
                }
            };

            BatchProcessableParameter lowFrequency = new BatchProcessableParameter()
            {
                Name = "Min Cutoff",
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0.0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true,
                    Message = "Must be greater than 0"
                }
            };

            BatchProcessableParameter highFrequency = new BatchProcessableParameter()
            {
                Name = "Max Cutoff",
                Val = 0.0,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = lowFrequency,
                    Path="Val",
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true,
                    Message = "Must be greater than low cutoff"
                }
            };

            BatchProcessableParameter order = new BatchProcessableParameter()
            {
                Name = "Filter Order",
                Val = 4,
                Validator = new MesosoftCommon.Utilities.Validations.ComparisonValidator()
                {
                    CompareTo = 0,
                    Operator = ComparisonValidator.Comparison.GreaterThan,
                    TreatNonComparableAsSuccess = true,
                    Message = "Must be greater than 0"
                }
            };

            BatchProcessableParameter stop = new BatchProcessableParameter()
            {
                Name = "Bandstop",
                Val = false
            };

            return new BatchProcessableParameter[] { data, inputFrequency, lowFrequency, highFrequency, stop, order};

            
        }

        public override string ToString()
        {
            return "Band Filter";
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            double inputFrequency = 1.0 / Convert.ToDouble(parameters.Single(p => p.Name == "Input Resolution").Val);
            double lowCut = Convert.ToDouble(parameters.Single(p => p.Name == "Min Cutoff").Val);
            double highCut = Convert.ToDouble(parameters.Single(p => p.Name == "Max Cutoff").Val);
            int order = Convert.ToInt32(parameters.Single(p => p.Name == "Filter Order").Val);
            bool stop = (bool)parameters.Single(p => p.Name == "Bandstop").Val;

            float[] data = parameters.Single(p => p.Name == "Data").Val as float[];

            AllMatlabNative.All mat = CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabFunctions;
            lock (CeresBase.FlowControl.Adapters.Matlab.MatlabLoader.MatlabLockable)
            {

                double[,] output = null;

                if (!stop)
                    output = mat.BandButterworthFilter(data, inputFrequency, new double[] { lowCut, highCut }, order) as double[,];
                else 
                    output = mat.ButterworthFilter(data, inputFrequency, new double[] { lowCut, highCut }, "stop") as double[,];
               return new object[]{CeresBase.General.Utilities.SquareArrayToDoubleArray<double>(output)[0]};
            }            
        }

        public string[] OutputDescription
        {
            get { return new string[] { "Filtered Signal" }; }
        }

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Filtering Tools"; }
        }

        #endregion
    }
}
