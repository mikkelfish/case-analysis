using System;
using System.Collections.Generic;
using System.Text;

namespace ApolloFinal.Tools.SpikeTriggeredAverage
{
    class SpikeTriggeredAverage
    {
        private SpikeVariable rawSignal;
        public SpikeVariable RawSignal
        {
            get
            {
                return this.rawSignal;
            }
        }

        private SpikeVariable cell;
        public SpikeVariable Cell
        {
            get { return cell; }
        }

        private float time;
        public float Time
        {
            get { return time; }
        }

        private bool rectified;
        public bool Rectified
        {
            get { return rectified; }
        }

        private float[] averages;
        public float[] Averages
        {
            get { return averages; }
        }

        private int amountPerWindow;
        public int PerWindow
        {
            get { return amountPerWindow; }
        }

        private float min;
        public float Min
        {
            get { return min; }
        }

        private float max;
        public float Max
        {
            get { return max; }
        }

        private float offset;
        public float Offset
        {
            get { return offset; }
        }
	
        public SpikeTriggeredAverage(SpikeVariable rawSignal, SpikeVariable cell, float time, bool rectified, float offset)
        {
            this.rawSignal = rawSignal;
            this.cell = cell;
            this.time = time;
            this.rectified = rectified;
            this.offset = offset;
        }

        public void Calculate(float start, float end)
        {
            int bufferSize = 5000000;
            float[] buffer = new float[bufferSize];

            List<float> cellData = new List<float>();
            double timeRes = (cell.Header as SpikeVariableHeader).Resolution;
            this.cell[0].Pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] count)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        float val = (float)(data[i] * timeRes);
                        if (val < start || val > end) continue;
                        cellData.Add(val);
                    }
                }
            );

            double signalRes = (rawSignal.Header as SpikeVariableHeader).Resolution;
            //number of data points per window
            this.amountPerWindow = (int)Math.Ceiling(time / signalRes);

            this.averages = new float[amountPerWindow];
            int[] counts = new int[amountPerWindow];

            int bufferSpot = 0;
            int bufferFilled = 0;
            int[] index = new int[] { (int)Math.Ceiling(cellData[0] / signalRes) };
            //index[0] = (int)(start / signalRes);
            for (int i = 0; i < cellData.Count; i++)
            {
                int timeSpot = (int)Math.Ceiling((cellData[i] + offset) / signalRes);
                if (timeSpot < 0) continue;

                //if (index == null)
                //    index = new int[] { timeSpot };

                //bufferFilled - bufferSpot < amountPerWindow

                if (timeSpot + amountPerWindow > this.rawSignal[0].Pool.DimensionLengths[0])
                {
                    break;
                }

                bufferSpot = timeSpot - (index[0] - bufferFilled);
                if (bufferSpot < 0) continue;

                if (bufferSpot > bufferFilled)
                {
                    bufferSpot = bufferFilled;
                }


                if (timeSpot + amountPerWindow > index[0])
                {
                    Array.Copy(buffer, bufferSpot, buffer, 0, bufferFilled - bufferSpot);
                    bufferFilled = bufferFilled - bufferSpot;
                    bufferSpot = 0;
                    if (index[0] < (int)Math.Ceiling(end / signalRes))
                    {
                        this.rawSignal[0].Pool.GetData(index, new int[] { bufferSize - bufferFilled },
                            delegate(float[] data, uint[] indices, uint[] count)
                            {
                                Array.Copy(data, 0, buffer, bufferFilled, count[0]);
                                index[0] += (int)count[0];
                                bufferFilled += (int)count[0];
                            }
                        );
                    }
                }

                if (this.rectified)
                {
                    for (int j = 0; j < amountPerWindow; j++)
                    {
                        this.averages[j] += Math.Abs(buffer[bufferSpot + j]);
                    }
                }
                else
                {
                    for (int j = 0; j < amountPerWindow; j++)
                    {
                        this.averages[j] += buffer[bufferSpot + j];
                    }
                }
            }

            this.min = float.MaxValue;
            this.max = float.MinValue;
            for (int i = 0; i < amountPerWindow; i++)
            {
                //this.averages[i] /= amountPerWindow;
                if (this.averages[i] < min)
                {
                    min = this.averages[i];
                }
                if (this.averages[i] > max)
                {
                    max = this.averages[i];
                }
            }
        }
	
    }
}
