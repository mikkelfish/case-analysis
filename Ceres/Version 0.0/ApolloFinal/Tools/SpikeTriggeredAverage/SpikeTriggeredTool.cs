using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CeresBase.Data;
using CeresBase.IO.ImageCreation;

namespace ApolloFinal.Tools.SpikeTriggeredAverage
{
    public partial class SpikeTriggeredTool : Form
    {
        public SpikeTriggeredTool()
        {
            InitializeComponent();
        }

        private SpikeTriggeredAverage avg;

        private void SpikeTriggeredTool_Load(object sender, EventArgs e)
        {
            foreach (Variable var in VariableSource.Variables)
            {
                if (!(var is SpikeVariable))
                {
                    continue;
                }

                if ((var as SpikeVariable).SpikeType == SpikeVariableHeader.SpikeType.Raw)
                {
                    this.comboBoxRaw.Items.Add(var);
                }
                else if ((var as SpikeVariable).SpikeType == SpikeVariableHeader.SpikeType.Pulse)
                {
                    this.comboBoxTrigger.Items.Add(var);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float time = float.Parse(this.textBoxTime.Text) / 1000.0F;
            float offset = float.Parse(this.textBox1.Text) / 1000.0F;
            this.avg = new SpikeTriggeredAverage(this.comboBoxRaw.SelectedItem as SpikeVariable,
                this.comboBoxTrigger.SelectedItem as SpikeVariable, time, this.checkBox1.Checked,
                offset);
            avg.Calculate(float.Parse(this.textBoxStart.Text), float.Parse(this.textBoxEnd.Text));
            //lock (this)
            //{
            //    this.image = null;
            //}
            this.panel1.Invalidate();
        }

        //private Bitmap image;

        private void saveImage(ImageCreator creator, ImageContainer parent)
        {

            float perX = (float)parent.Size.Width / this.avg.PerWindow;
            //List<Point> points = new List<Point>();
            Lines lines = new Lines(parent);
            for (int i = 0; i < this.avg.Averages.Length; i++)
            {
                int y = (int)(parent.Size.Height - (int)((this.avg.Averages[i] - this.avg.Min) * (float)parent.Size.Height / (this.avg.Max - this.avg.Min)));
                lines.AddPoint(new PointF((i * perX), y));
                //   points.Add(new Point((int)(i * perX), y));
            }
            creator.AddImageElement(lines);
            
            //e.Graphics.DrawLines(Pens.Black, points.ToArray());

            float offsetX = (-this.avg.Offset / this.avg.Time)*parent.Size.Width;
            if (offsetX < 0 || offsetX > parent.Size.Width) return;
            Line line = new Line(parent, new PointF(offsetX, 0), new PointF(offsetX, parent.Size.Height));
            line.Color = Color.Red;
            creator.AddImageElement(line);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (this.avg == null) return;
            //lock (this)
            //{
            //    if (this.image == null)
            //    {
            //        this.image = new Bitmap(this.panel1.Width, this.panel1.Height);
                  //  Graphics g = Graphics.FromImage(this.image);

                    GDI gdi = new GDI();
                    ImageContainer container = new ImageContainer(null, new Rectangle(0, 0, this.panel1.Width, this.panel1.Height));
                    gdi.AddImageElement(container);                    
                    this.saveImage(gdi, container);

                    gdi.Create(new object[] { e.Graphics });
                    
                    //this.saveImage(gdi, e.ClipRectangle, new PaintEventArgs(g, e.ClipRectangle));
                //}

            //    e.Graphics.DrawImage(this.image, 0, 0);
            //}
        }

        private void saveImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.avg == null) return;
            SaveFileDialog diag = new SaveFileDialog();
            diag.AddExtension = true;
            diag.DefaultExt = ".svg";
            diag.Filter = "Scalable Vector Graphics(*.svg)|*.svg|All Files(*.*)|*.*";
            if (diag.ShowDialog() == DialogResult.Cancel) return;

            SVG svg = new SVG();
            Font font = new Font(FontFamily.GenericSerif, 12);
            TextElement cell = new TextElement(new ImageContainer(null, new Rectangle(5, 0, 600, 15)),
               font,"Cell: " + avg.Cell.FullDescription, new PointF(0,0) );
            TextElement raw = new TextElement(new ImageContainer(null, new Rectangle(5, 20, 600, 15)),
                font, "Raw Signal: " + avg.RawSignal.FullDescription, new PointF(0, 0));
            svg.AddImageElement(cell);
            svg.AddImageElement(raw);

            ImageContainer container = new ImageContainer(null, new Rectangle(0, 45, 700, 300));
            svg.AddImageElement(container);
            this.saveImage(svg, container);
            svg.Create(new object[] { diag.FileName });
        }
    }
}