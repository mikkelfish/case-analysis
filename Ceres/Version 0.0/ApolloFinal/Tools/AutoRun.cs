using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CeresBase.Data;
using System.IO;
using ApolloFinal.Tools.Histograms;
//using Razor.Configuration;

namespace ApolloFinal.Tools
{


    public partial class AutoRun : Form
    {

        public AutoRun()
        {
            InitializeComponent();
        }

        //private void AutoRun_Load(object sender, EventArgs e)
        //{
        //    foreach (Variable var in VariableSource.Variables)
        //    {
        //        if (!(var is SpikeVariable)) continue;
        //        SpikeVariable sVar = (SpikeVariable)var;
        //        if (sVar.SpikeType == SpikeVariableHeader.SpikeType.Pulse)
        //        {
        //            this.comboBoxBase.Items.Add(sVar);
        //            this.comboBoxTrigger.Items.Add(sVar);
        //            ListViewItem item = new ListViewItem(sVar.FullDescription);
        //            item.Tag = sVar;
        //            this.listViewLoaded.Items.Add(item);
        //        }
        //        else if (sVar.SpikeType == SpikeVariableHeader.SpikeType.Raw)
        //        {
        //            this.comboBoxRaw.Items.Add(sVar);
        //        }
        //        else
        //        {
        //            this.comboBoxBase.Items.Add(sVar);
        //        }
        //    }

        //    if (this.comboBoxRaw.Items.Count == 0)
        //    {
        //        this.checkBoxSTA.Checked = false;
        //    }
        //    else this.checkBoxSTA.Checked = true;
        //}

        //private void checkBoxCTH_CheckedChanged(object sender, EventArgs e)
        //{
        //    this.groupBoxCTO.Enabled = this.checkBoxCTH.Checked;
        //}

        //private void checkBoxCC_CheckedChanged(object sender, EventArgs e)
        //{
        //    this.groupBoxCCOptions.Enabled = this.checkBoxCC.Checked;
        //}

        //private void checkBoxSTA_CheckedChanged(object sender, EventArgs e)
        //{
        //    this.groupBoxSTA.Enabled = this.checkBoxSTA.Checked;
        //}

        //private void buttonRun_Click(object sender, EventArgs e)
        //{
        //    //if (!Directory.Exists(this.textBoxDirectory.Text))
        //    //{
        //    //    MessageBox.Show("Please enter a valid directory as the save directory.");
        //    //    return;
        //    //}

        //    //TODO more checks

        //    Dictionary<optionsState, CorrelationBase> cycleTriggeredBase = new Dictionary<optionsState, CorrelationBase>();
        //    Dictionary<optionsState, Dictionary<SpikeVariable, NormalizedHistogram>> cycleTriggered = new Dictionary<optionsState, Dictionary<SpikeVariable, NormalizedHistogram>>();
        //    Dictionary<optionsState, Dictionary<SpikeVariable, List<MultiOrderCorrelation>>> crossCorrs = new Dictionary<optionsState, Dictionary<SpikeVariable, List<MultiOrderCorrelation>>>();

        //    SpikeVariable baseSig = this.comboBoxBase.SelectedItem as SpikeVariable;
        //    SpikeVariable timelineVar = this.comboBoxTrigger.SelectedItem as SpikeVariable;

        //    if (this.checkBoxCTH.Checked)
        //    {

        //        #region Figure out combinations
        //        List<float> binwidths = new List<float>();
        //        string[] binWstrs = this.textBoxCTOBinW.Text.Split(',');
        //        for (int i = 0; i < binWstrs.Length; i++)
        //        {
        //            binwidths.Add(float.Parse(binWstrs[i].Trim()));
        //        }

        //        List<int> bincounts = new List<int>();
        //        string[] binCtsstrs = this.textBoxCTOBCts.Text.Split(',');
        //        for (int i = 0; i < binCtsstrs.Length; i++)
        //        {
        //            bincounts.Add(int.Parse(binCtsstrs[i].Trim()));
        //        }

        //        List<double> sds = new List<double>();
        //        string[] binSDstrs = this.textBoxCTOSDs.Text.Split(',');
        //        for (int i = 0; i < binSDstrs.Length; i++)
        //        {
        //            sds.Add(double.Parse(binSDstrs[i].Trim()));
        //        }

        //        List<double> offsets = new List<double>();
        //        string[] binOffstrs = this.textBoxCTOoff.Text.Split(',');
        //        for (int i = 0; i < binOffstrs.Length; i++)
        //        {
        //            offsets.Add(double.Parse(binOffstrs[i].Trim()));
        //        }

        //        List<float> times = new List<float>();
        //        string[] timestrs = this.textBoxCTOTimes.Text.Split(' ');
        //        for (int i = 0; i < timestrs.Length; i++)
        //        {
        //            string[] split = timestrs[i].Split(',');
        //            times.Add(float.Parse(split[0]) * 1000.0F);
        //            times.Add(float.Parse(split[1]) * 1000.0F);
        //        }

        //        List<optionsState> optionStates = new List<optionsState>();
        //        for (int i = 0; i < binwidths.Count; i++)
        //        {
        //            for (int j = 0; j < bincounts.Count; j++)
        //            {
        //                for (int k = 0; k < sds.Count; k++)
        //                {
        //                    for (int l = 0; l < offsets.Count; l++)
        //                    {
        //                        for (int m = 0; m < times.Count / 2; m++)
        //                        {
        //                            try
        //                            {
        //                                optionStates.Add(new optionsState(binwidths[i], bincounts[j], sds[k], offsets[l], times[m * 2], times[m * 2 + 1]));
        //                            }
        //                            catch (Exception ex)
        //                            {
        //                                int y = 0;
        //                            }

        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        #endregion

        //        // float binWidth = float.Parse(this.textBoxCTOBinW.Text);
        //        // int numBins = int.Parse(this.textBoxCTOBCts.Text);


        //        foreach (optionsState state in optionStates)
        //        {
        //            CorrelationBase baseCorr;
        //            if (baseSig.SpikeType == SpikeVariableHeader.SpikeType.Integrated)
        //            {
        //                baseCorr = new CycleTriggeredAverage(state.BinWidth, state.BinCount, timelineVar, baseSig, state.SD, state.Offset, true);
        //            }
        //            else baseCorr = new NormalizedHistogram(state.BinWidth, state.BinCount, timelineVar, baseSig, state.SD, state.Offset, false);
        //            baseCorr.CalculateHistograms(state.Start, state.End);
        //            cycleTriggeredBase.Add(state, baseCorr);

        //            cycleTriggered.Add(state, new Dictionary<SpikeVariable, NormalizedHistogram>());
        //            foreach (ListViewItem item in this.listViewAnalyze.Items)
        //            {
        //                SpikeVariable var = item.Tag as SpikeVariable;
        //                //  cycleTriggered[state].Add(var, new CorrelationBase[2]);


        //                NormalizedHistogram cellCorr = new NormalizedHistogram(state.BinWidth, state.BinCount, timelineVar, var, state.SD, state.Offset, true);
        //                cellCorr.CalculateHistograms(state.Start, state.End);
        //                cycleTriggered[state].Add(var, cellCorr);
        //                //  cycleTriggered[state][var][0] = baseCorr;
        //                //cycleTriggered[state][var] = cellCorr;
        //            }
        //        }
        //    }

        //    if (this.checkBoxCC.Checked)
        //    {
        //        #region Figure out combinations
        //        List<float> binwidths = new List<float>();
        //        string[] binWstrs = this.textBoxCCbinws.Text.Split(',');
        //        for (int i = 0; i < binWstrs.Length; i++)
        //        {
        //            binwidths.Add(float.Parse(binWstrs[i].Trim()));
        //        }

        //        List<int> bincounts = new List<int>();
        //        string[] binCtsstrs = this.textBoxCCbincts.Text.Split(',');
        //        for (int i = 0; i < binCtsstrs.Length; i++)
        //        {
        //            bincounts.Add(int.Parse(binCtsstrs[i].Trim()));
        //        }

        //        List<double> sds = new List<double>();
        //        string[] binSDstrs = this.textBoxCCsds.Text.Split(',');
        //        for (int i = 0; i < binSDstrs.Length; i++)
        //        {
        //            sds.Add(double.Parse(binSDstrs[i].Trim()));
        //        }

        //        List<double> offsets = new List<double>();
        //        string[] binOffstrs = this.textBoxCCoffs.Text.Split(',');
        //        for (int i = 0; i < binOffstrs.Length; i++)
        //        {
        //            offsets.Add(double.Parse(binOffstrs[i].Trim()));
        //        }

        //        List<float> times = new List<float>();
        //        string[] timestrs = this.textBoxCCtimes.Text.Split(' ');
        //        for (int i = 0; i < timestrs.Length; i++)
        //        {
        //            string[] split = timestrs[i].Split(',');
        //            times.Add(float.Parse(split[0]));
        //            times.Add(float.Parse(split[1]));
        //        }

        //        List<optionsState> optionStates = new List<optionsState>();
        //        for (int i = 0; i < binwidths.Count; i++)
        //        {
        //            for (int j = 0; j < bincounts.Count; j++)
        //            {
        //                for (int k = 0; k < sds.Count; k++)
        //                {
        //                    for (int l = 0; l < offsets.Count; l++)
        //                    {
        //                        for (int m = 0; m < times.Count / 2; m++)
        //                        {
        //                            optionStates.Add(new optionsState(binwidths[i], bincounts[j], sds[k], offsets[l], times[m * 2] * 1000.0F, times[m * 2 + 1] * 1000.0F));

        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        #endregion

        //        foreach (optionsState state in optionStates)
        //        {
        //            crossCorrs.Add(state, new Dictionary<SpikeVariable, List<MultiOrderCorrelation>>());
        //            foreach (ListViewItem item in this.listViewAnalyze.Items)
        //            {
        //                SpikeVariable firstVar = item.Tag as SpikeVariable;
        //                crossCorrs[state].Add(firstVar, new List<MultiOrderCorrelation>());
        //                foreach (ListViewItem item2 in this.listViewAnalyze.Items)
        //                {
        //                    SpikeVariable secondVar = item2.Tag as SpikeVariable;
        //                    MultiOrderCorrelation corr = new MultiOrderCorrelation(state.BinWidth, state.BinCount, firstVar, secondVar, state.SD, state.Offset);
        //                    corr.CalculateHistograms(state.Start, state.End);
        //                    crossCorrs[state][firstVar].Add(corr);
        //                }
        //            }
        //        }
        //    }

        //    string path = this.textBoxDirectory.Text;




        //    XmlConfiguration configuration;
        //    ConfigurationEngine.ReadOrCreateConfiguration(false, "AutoRun", path, out configuration, null, null);
        //    configuration.Categories.Clear();
        //    if (this.checkBoxSTA.Checked)
        //    {

        //    }


        //    if (this.checkBoxCTH.Checked)
        //    {
        //        XmlConfigurationCategory cthCat = configuration.Categories["CTH", true];

        //        int optionsCount = 0;
        //        foreach (optionsState state in cycleTriggered.Keys)
        //        {
        //            XmlConfigurationCategory stateCat = cthCat.Categories[optionsCount.ToString(), true];
        //            XmlConfigurationOption sdOpt = stateCat.Options["SD", true, state.SD];
        //            XmlConfigurationOption binCoutn = stateCat.Options["NumBins", true, state.BinCount];
        //            XmlConfigurationOption binWidth = stateCat.Options["BinWidth", true, state.BinWidth];
        //            XmlConfigurationOption offset = stateCat.Options["Offset", true, state.Offset];
        //            XmlConfigurationOption start = stateCat.Options["Start", true, state.Start];
        //            XmlConfigurationOption end = stateCat.Options["End", true, state.End];

        //            // XmlConfigurationCategory baseCat = stateCat.Categories["BASESIGNAL: baseSig.FullDescription", true];
        //            XmlConfigurationOption excludedCycles = stateCat.Options["Excluded", true, cycleTriggeredBase[state].ExcludedCycles];
        //            XmlConfigurationOption totalCycles = stateCat.Options["TotalCycles", true, cycleTriggeredBase[state].TotalCycles];
        //            string baseBinCounts = "";
        //            for (int i = 0; i < cycleTriggeredBase[state].BinCounts.Length; i++)
        //            {
        //                baseBinCounts += cycleTriggeredBase[state].BinCounts[i] + " ";
        //            }
        //            XmlConfigurationOption baseBins = stateCat.Options["BaseBinCounts", true, baseBinCounts];

        //            foreach (SpikeVariable var in cycleTriggered[state].Keys)
        //            {
        //                XmlConfigurationCategory varCat = stateCat.Categories[var.FullDescription, true];
        //                string binCounts = "";
        //                for (int i = 0; i < cycleTriggered[state][var].BinCounts.Length; i++)
        //                {
        //                    binCounts += cycleTriggered[state][var].BinCounts[i].ToString() + " ";
        //                }

        //                XmlConfigurationOption binCountsOption = varCat.Options["BinCounts", true, binCounts];

        //                XmlConfigurationCategory statCat = varCat.Categories["Statistics", true];
        //                foreach (string key in cycleTriggered[state][var].Statistics.Keys)
        //                {
        //                    XmlConfigurationOption option = statCat.Options[key, true, cycleTriggered[state][var].Statistics[key]];
        //                }


        //                //XmlConfigurationOption eta = varCat.Options["Eta2", true, cycleTriggered[state][var].EtaSquared];
        //            }

        //            optionsCount++;
        //        }

        //    }

        //    if (this.checkBoxCC.Checked)
        //    {
        //        XmlConfigurationCategory crossCorrelations = configuration.Categories["CrossCorrelations", true];
        //        int optionsCount = 0;
        //        foreach (optionsState state in crossCorrs.Keys)
        //        {
        //            XmlConfigurationCategory stateCat = crossCorrelations.Categories[optionsCount.ToString(), true];
        //            XmlConfigurationOption sdOpt = stateCat.Options["SD", true, state.SD];
        //            XmlConfigurationOption binCoutn = stateCat.Options["NumBins", true, state.BinCount];
        //            XmlConfigurationOption binWidth = stateCat.Options["BinWidth", true, state.BinWidth];
        //            XmlConfigurationOption offset = stateCat.Options["Offset", true, state.Offset];
        //            XmlConfigurationOption start = stateCat.Options["Start", true, state.Start];
        //            XmlConfigurationOption end = stateCat.Options["End", true, state.End];


        //            //  Dictionary<optionsState, Dictionary<SpikeVariable, List<MultiOrderCorrelation>>> crossCorrs = new Dictionary<optionsState, Dictionary<SpikeVariable, List<MultiOrderCorrelation>>>();


        //            foreach (SpikeVariable var in crossCorrs[state].Keys)
        //            {
        //                XmlConfigurationCategory varCat = stateCat.Categories[var.FullDescription, true];
        //                foreach (MultiOrderCorrelation corr in crossCorrs[state][var])
        //                {
        //                    string binCounts = "";
        //                    for (int i = 0; i < corr.BinCounts.Length; i++)
        //                    {
        //                        binCounts += corr.BinCounts[i].ToString() + " ";
        //                    }

        //                    XmlConfigurationOption binCountsOption = varCat.Options[corr.Var2.FullDescription, true, binCounts];

        //                    XmlConfigurationCategory statCat = varCat.Categories["Statistics " + corr.Var2.FullDescription, true];
        //                    foreach (string key in corr.Statistics.Keys)
        //                    {
        //                        XmlConfigurationOption option = statCat.Options[key, true, corr.Statistics[key]];
        //                    }

        //                }
        //            }

        //            optionsCount++;
        //        }
        //    }

        //    ConfigurationEngine.WriteConfiguration(null, configuration, configuration.Path);

        //    MessageBox.Show("Run completed.");
        //    this.Close();
        //}

        //private void listViewLoaded_MouseDown(object sender, MouseEventArgs e)
        //{
        //}

        //private void listViewAnalyze_DragEnter(object sender, DragEventArgs e)
        //{
        //    if (e.Data.GetDataPresent(typeof(ListView.SelectedListViewItemCollection))) e.Effect = DragDropEffects.Copy;
        //}

        //private void listViewAnalyze_DragDrop(object sender, DragEventArgs e)
        //{
        //    ListView.SelectedListViewItemCollection collection = (ListView.SelectedListViewItemCollection)e.Data.GetData(typeof(ListView.SelectedListViewItemCollection));
        //    foreach (ListViewItem item in collection)
        //    {
        //        ListViewItem insertItem = new ListViewItem(item.Text, item.Text);
        //        insertItem.Tag = item.Tag;
        //        insertItem.Name = item.Text;

        //        if (!this.listViewAnalyze.Items.ContainsKey(insertItem.Text))
        //        {
        //            this.listViewAnalyze.Items.Add(insertItem);
        //        }
        //    }
        //}

        //private void listViewLoaded_MouseMove(object sender, MouseEventArgs e)
        //{
        //    if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
        //    {
        //        this.listViewLoaded.DoDragDrop(this.listViewLoaded.SelectedItems, DragDropEffects.Copy);
        //    }
        //}

        //private void listViewAnalyze_MouseDown(object sender, MouseEventArgs e)
        //{
        //    if (e.Button == MouseButtons.Right)
        //    {
        //        if (MessageBox.Show("Remove the selected items from analysis?") == DialogResult.Cancel) return;

        //        ListViewItem[] items = new ListViewItem[this.listViewAnalyze.SelectedItems.Count];
        //        this.listViewAnalyze.SelectedItems.CopyTo(items, 0);
        //        for (int i = 0; i < items.Length; i++)
        //        {
        //            this.listViewAnalyze.Items.Remove(items[i]);
        //        }
        //    }
        //}

        //private void buttonDir_Click(object sender, EventArgs e)
        //{
        //    SaveFileDialog diag = new SaveFileDialog();
        //    diag.Filter = "Xml File(*.xml)|*.xml|All files(*.*)|*.*";
        //    diag.AddExtension = true;
        //    diag.DefaultExt = ".xml";
        //    if (diag.ShowDialog() == DialogResult.Cancel) return;
        //    this.textBoxDirectory.Text = diag.FileName;
        //}

        //private void buttonCancel_Click(object sender, EventArgs e)
        //{
        //    this.Close();
        //}


    }
}