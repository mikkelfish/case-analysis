//namespace ApolloFinal.Tools
//{
//    partial class SighProject
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.label1 = new System.Windows.Forms.Label();
//            this.label3 = new System.Windows.Forms.Label();
//            this.buttonRun = new System.Windows.Forms.Button();
//            this.listBox1 = new System.Windows.Forms.ListBox();
//            this.label2 = new System.Windows.Forms.Label();
//            this.labelFile = new System.Windows.Forms.Label();
//            this.buttonGetFile = new System.Windows.Forms.Button();
//            this.listBox2 = new System.Windows.Forms.ListBox();
//            this.SuspendLayout();
//            // 
//            // label1
//            // 
//            this.label1.AutoSize = true;
//            this.label1.Location = new System.Drawing.Point(12, 9);
//            this.label1.Name = "label1";
//            this.label1.Size = new System.Drawing.Size(28, 13);
//            this.label1.TabIndex = 0;
//            this.label1.Text = "Files";
//            // 
//            // label3
//            // 
//            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this.label3.AutoSize = true;
//            this.label3.Location = new System.Drawing.Point(12, 292);
//            this.label3.Name = "label3";
//            this.label3.Size = new System.Drawing.Size(45, 13);
//            this.label3.TabIndex = 5;
//            this.label3.Text = "Variable";
//            // 
//            // buttonRun
//            // 
//            this.buttonRun.Location = new System.Drawing.Point(154, 483);
//            this.buttonRun.Name = "buttonRun";
//            this.buttonRun.Size = new System.Drawing.Size(75, 23);
//            this.buttonRun.TabIndex = 6;
//            this.buttonRun.Text = "Run";
//            this.buttonRun.UseVisualStyleBackColor = true;
//            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
//            // 
//            // listBox1
//            // 
//            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
//                        | System.Windows.Forms.AnchorStyles.Left)
//                        | System.Windows.Forms.AnchorStyles.Right)));
//            this.listBox1.FormattingEnabled = true;
//            this.listBox1.Location = new System.Drawing.Point(15, 25);
//            this.listBox1.Name = "listBox1";
//            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
//            this.listBox1.Size = new System.Drawing.Size(355, 264);
//            this.listBox1.TabIndex = 8;
//            // 
//            // label2
//            // 
//            this.label2.AutoSize = true;
//            this.label2.Location = new System.Drawing.Point(12, 449);
//            this.label2.Name = "label2";
//            this.label2.Size = new System.Drawing.Size(23, 13);
//            this.label2.TabIndex = 9;
//            this.label2.Text = "File";
//            // 
//            // labelFile
//            // 
//            this.labelFile.AutoSize = true;
//            this.labelFile.Location = new System.Drawing.Point(12, 462);
//            this.labelFile.Name = "labelFile";
//            this.labelFile.Size = new System.Drawing.Size(0, 13);
//            this.labelFile.TabIndex = 10;
//            // 
//            // buttonGetFile
//            // 
//            this.buttonGetFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
//            this.buttonGetFile.Location = new System.Drawing.Point(347, 452);
//            this.buttonGetFile.Name = "buttonGetFile";
//            this.buttonGetFile.Size = new System.Drawing.Size(23, 23);
//            this.buttonGetFile.TabIndex = 11;
//            this.buttonGetFile.Text = ".";
//            this.buttonGetFile.UseVisualStyleBackColor = true;
//            this.buttonGetFile.Click += new System.EventHandler(this.buttonGetFile_Click);
//            // 
//            // listBox2
//            // 
//            this.listBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
//                        | System.Windows.Forms.AnchorStyles.Right)));
//            this.listBox2.FormattingEnabled = true;
//            this.listBox2.Location = new System.Drawing.Point(15, 308);
//            this.listBox2.Name = "listBox2";
//            this.listBox2.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
//            this.listBox2.Size = new System.Drawing.Size(355, 134);
//            this.listBox2.TabIndex = 12;
//            // 
//            // SighProject
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.ClientSize = new System.Drawing.Size(382, 539);
//            this.Controls.Add(this.listBox2);
//            this.Controls.Add(this.buttonGetFile);
//            this.Controls.Add(this.labelFile);
//            this.Controls.Add(this.label2);
//            this.Controls.Add(this.listBox1);
//            this.Controls.Add(this.buttonRun);
//            this.Controls.Add(this.label3);
//            this.Controls.Add(this.label1);
//            this.Name = "SighProject";
//            this.Text = "SighProject";
//            this.Load += new System.EventHandler(this.SighProject_Load);
//            this.ResumeLayout(false);
//            this.PerformLayout();

//        }

//        #endregion

//        private System.Windows.Forms.Label label1;
//        private System.Windows.Forms.Label label3;
//        private System.Windows.Forms.Button buttonRun;
//        private System.Windows.Forms.ListBox listBox1;
//        private System.Windows.Forms.Label label2;
//        private System.Windows.Forms.Label labelFile;
//        private System.Windows.Forms.Button buttonGetFile;
//        private System.Windows.Forms.ListBox listBox2;
//    }
//}