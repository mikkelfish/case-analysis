namespace ApolloFinal.Tools
{
    partial class MakeSpikeSortable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxLoadedVars = new System.Windows.Forms.ListBox();
            this.listBoxToClean = new System.Windows.Forms.ListBox();
            this.listBoxToLeave = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonPath = new System.Windows.Forms.Button();
            this.buttonConvert = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonRemoveClean = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxLoadedVars
            // 
            this.listBoxLoadedVars.FormattingEnabled = true;
            this.listBoxLoadedVars.Location = new System.Drawing.Point(12, 27);
            this.listBoxLoadedVars.Name = "listBoxLoadedVars";
            this.listBoxLoadedVars.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxLoadedVars.Size = new System.Drawing.Size(214, 433);
            this.listBoxLoadedVars.TabIndex = 0;
            this.listBoxLoadedVars.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBoxLoadedVars_MouseDown);
            // 
            // listBoxToClean
            // 
            this.listBoxToClean.AllowDrop = true;
            this.listBoxToClean.FormattingEnabled = true;
            this.listBoxToClean.Location = new System.Drawing.Point(232, 27);
            this.listBoxToClean.Name = "listBoxToClean";
            this.listBoxToClean.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxToClean.Size = new System.Drawing.Size(233, 199);
            this.listBoxToClean.TabIndex = 1;
            this.listBoxToClean.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBoxToClean.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            // 
            // listBoxToLeave
            // 
            this.listBoxToLeave.AllowDrop = true;
            this.listBoxToLeave.FormattingEnabled = true;
            this.listBoxToLeave.Location = new System.Drawing.Point(232, 284);
            this.listBoxToLeave.Name = "listBoxToLeave";
            this.listBoxToLeave.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxToLeave.Size = new System.Drawing.Size(233, 147);
            this.listBoxToLeave.TabIndex = 2;
            this.listBoxToLeave.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBoxToLeave.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Variables Loaded";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Variables To Clean";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(232, 268);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Variables To Leave Raw";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(472, 99);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(254, 20);
            this.textBox1.TabIndex = 6;
            // 
            // buttonPath
            // 
            this.buttonPath.Location = new System.Drawing.Point(543, 125);
            this.buttonPath.Name = "buttonPath";
            this.buttonPath.Size = new System.Drawing.Size(110, 23);
            this.buttonPath.TabIndex = 7;
            this.buttonPath.Text = "Choose Dest";
            this.buttonPath.UseVisualStyleBackColor = true;
            this.buttonPath.Click += new System.EventHandler(this.buttonPath_Click);
            // 
            // buttonConvert
            // 
            this.buttonConvert.Location = new System.Drawing.Point(496, 163);
            this.buttonConvert.Name = "buttonConvert";
            this.buttonConvert.Size = new System.Drawing.Size(211, 161);
            this.buttonConvert.TabIndex = 8;
            this.buttonConvert.Text = "Run";
            this.buttonConvert.UseVisualStyleBackColor = true;
            this.buttonConvert.Click += new System.EventHandler(this.buttonConvert_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(566, 330);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 9;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonRemoveClean
            // 
            this.buttonRemoveClean.Location = new System.Drawing.Point(298, 232);
            this.buttonRemoveClean.Name = "buttonRemoveClean";
            this.buttonRemoveClean.Size = new System.Drawing.Size(96, 23);
            this.buttonRemoveClean.TabIndex = 10;
            this.buttonRemoveClean.Text = "Remove Items";
            this.buttonRemoveClean.UseVisualStyleBackColor = true;
            this.buttonRemoveClean.Click += new System.EventHandler(this.buttonRemoveClean_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(298, 437);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Remove Items";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MakeSpikeSortable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 472);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonRemoveClean);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonConvert);
            this.Controls.Add(this.buttonPath);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxToLeave);
            this.Controls.Add(this.listBoxToClean);
            this.Controls.Add(this.listBoxLoadedVars);
            this.Name = "MakeSpikeSortable";
            this.Text = "Make Spike Sortable";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxLoadedVars;
        private System.Windows.Forms.ListBox listBoxToClean;
        private System.Windows.Forms.ListBox listBoxToLeave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonPath;
        private System.Windows.Forms.Button buttonConvert;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonRemoveClean;
        private System.Windows.Forms.Button button1;
    }
}