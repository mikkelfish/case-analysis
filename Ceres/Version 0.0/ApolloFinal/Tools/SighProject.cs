//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;
//using System.Windows.Forms;
//using System.IO;
//using CeresBase.Data;
//using CeresBase.IO;
//using CeresBase.UI;

//namespace ApolloFinal.Tools
//{
//    public partial class SighProject : Form
//    {
//        private class sighEntry
//        {
//            private string file;

//            public string File
//            {
//                get { return file; }
//                set { file = value; }
//            }

//            private SpikeVariable[,] vars;

//            public SpikeVariable[,] Vars
//            {
//                get { return vars; }
//                set { vars = value; }
//            }

//            private List<double>[] startTimes;
//            public List<double>[] StartTimes
//            {
//                get { return startTimes; }
//                set { this.startTimes = value; }
//            }

//            private List<double>[] endTimes;
//            public List<double>[] EndTimes
//            {
//                get { return endTimes; }
//                set { this.endTimes = value; }
//            }

//            private List<string> names = new List<string>();

//            public List<string> Names
//            {
//                get { return names; }
//                set { names = value; }
//            }
	
	 

//            public override string ToString()
//            {
//                return this.file;
//            }
	
//        }

//        public SighProject()
//        {
//            InitializeComponent();
//        }

//        private void getAvgSd(Dictionary<string, List<List<float[]>>> cv, bool withinWeeks, bool withinExpts, string week, int expt, 
//            out float mean, out float sd,
//            ref float storedCVmean, ref float storedCVSD,
//            ref Dictionary<string, float> weekMeans, ref Dictionary<string, float> weekSDs,
//            ref Dictionary<string, List<float>> exptMeans, ref Dictionary<string, List<float>> exptSD)
//        {
//            //If not within weeks or expts then comparing all values
//            if (!withinWeeks && !withinExpts)
//            {
//                if (storedCVmean != float.MaxValue && storedCVSD != float.MaxValue)
//                {
//                    mean = storedCVmean;
//                    sd = storedCVSD;
//                    return;
//                }

//                #region Calculate SD/Mean for all
//                float cvmean = 0;
//                int total = 0;

//                foreach (string name in cv.Keys)
//                {
//                    for (int i = 0; i < cv[name].Count; i++)
//                    {
//                        for (int j = 0; j < cv[name][i].Count; j++)
//                        {
//                            for (int k = 0; k < cv[name][i][j].Length; k++)
//                            {
//                                cvmean += cv[name][i][j][k];
//                                total++;
//                            }
//                        }
//                    }
//                }

//                cvmean /= total;

//                float cvsd = 0;
//                foreach (string name in cv.Keys)
//                {
//                    for (int i = 0; i < cv[name].Count; i++)
//                    {
//                        for (int j = 0; j < cv[name][i].Count; j++)
//                        {
//                            for (int k = 0; k < cv[name][i][j].Length; k++)
//                            {
//                                cvsd += (cv[name][i][j][k] - cvmean) * (cv[name][i][j][k] - cvmean);
//                            }
//                        }
//                    }
//                }

//                cvsd /= total;
//                cvsd = (float)Math.Sqrt(cvsd);

//                storedCVmean = cvmean;
//                storedCVSD = cvsd;

//                mean = cvmean;
//                sd = cvsd;
//                #endregion
//                return;
//            }
//            else if (!withinWeeks && withinExpts) //if not within weeks, but within expts, then sort by expt
//            {
//                throw new NotSupportedException("Not yet");

//            }
//            else if (withinWeeks && !withinExpts) //if not within expts, but within weeks, calc by key
//            {
//                if (weekMeans == null)
//                {
//                    weekMeans = new Dictionary<string, float>();
//                    weekSDs = new Dictionary<string, float>();
//                }

//                if (weekMeans.ContainsKey(week))
//                {
//                    mean = weekMeans[week];
//                    sd = weekSDs[week];
//                    return;
//                }

//                #region Calculate mean/sd based on the week

//                float calcMean = 0;
//                int totalCount = 0;
//                for (int i = 0; i < cv[week].Count; i++)
//                {
//                    for (int j = 0; j < cv[week][i].Count; j++)
//                    {
//                        for (int k = 0; k < cv[week][i][j].Length; k++)
//                        {
//                            calcMean += cv[week][i][j][k];
//                            totalCount++;
//                        }
//                    }
//                }

//                calcMean /= totalCount;

//                float calcSD = 0;
//                for (int i = 0; i < cv[week].Count; i++)
//                {
//                    for (int j = 0; j < cv[week][i].Count; j++)
//                    {
//                        for (int k = 0; k < cv[week][i][j].Length; k++)
//                        {
//                            calcSD += (cv[week][i][j][k] - calcMean) * (cv[week][i][j][k] - calcMean);
//                        }
//                    }
//                }

//                calcSD /= totalCount;
//                calcSD = (float)Math.Sqrt(calcSD);

//                weekMeans.Add(week, calcMean);
//                weekSDs.Add(week, calcSD);

//                mean = calcMean;
//                sd = calcSD;
//                #endregion
//                return;

//            }

//            //Calculate within each expt
//                if (exptMeans == null)
//                {
//                    exptMeans = new Dictionary<string, List<float>>();
//                    exptSD = new Dictionary<string, List<float>>();
//                }

//                if (!exptMeans.ContainsKey(week))
//                {
//                    exptMeans.Add(week, new List<float>());
//                    exptSD.Add(week, new List<float>());
//                }

//                if (exptMeans[week].Count > expt)
//                {
//                    mean = exptMeans[week][expt];
//                    sd = exptSD[week][expt];
//                    return;
//                }

//                #region Calc based within each expt

//                float calcMean2 = 0;
//                int totalCount2 = 0;
//                for (int i = 0; i < cv[week][expt].Count; i++)
//                {
//                    for (int j = 0; j < cv[week][expt][i].Length; j++)
//                    {
//                        calcMean2 += cv[week][expt][i][j];
//                        totalCount2++;
//                    }
//                }

//                calcMean2 /= totalCount2;

//                float calcSD2 = 0;
//                for (int i = 0; i < cv[week][expt].Count; i++)
//                {
//                    for (int j = 0; j < cv[week][expt][i].Length; j++)
//                    {
//                        calcSD2 += (cv[week][expt][i][j] - calcMean2) * (cv[week][expt][i][j] - calcMean2);
//                    }
//                }

//                calcSD2 /= totalCount2;
//                calcSD2 = (float)Math.Sqrt(calcSD2);

//                exptMeans[week].Add(calcMean2);
//                exptSD[week].Add(calcSD2);

//                mean = calcMean2;
//                sd = calcSD2;

//                #endregion

//        }

//        private void buttonRun_Click(object sender, EventArgs args)
//        {
//            RequestInformation info = new RequestInformation();
//            info.Grid.SetInformationToCollect(
//                new Type[] { typeof(double), typeof(double), typeof(int), typeof(bool), typeof(bool) },
//                new string[] { "Params", "Params", "Params", "Stats", "Stats" },
//                new string[] { "SD Multiplier", "Interval Length", "Number Intervals", "Run MI", "Run Entropy" },
//                new string[] { "", "", "", "", "" },
//                null,
//                new object[] { 1.0, 5.0, 3, false, false });
//            if (info.ShowDialog() == DialogResult.Cancel) return;

//            double sd = (double)info.Grid.Results["SD Multiplier"];
//            double length = (double)info.Grid.Results["Interval Length"];
//            int numintervals = (int)info.Grid.Results["Number Intervals"];
//            bool mi = (bool)info.Grid.Results["Run MI"];
//            bool entropy = (bool)info.Grid.Results["Run Entropy"];

//            bool withinWeeks = false;
//            bool withinExpts = false;

//            Dictionary<string, List<List<List<float>[]>>> i = new Dictionary<string, List<List<List<float>[]>>>();
//            Dictionary<string, List<List<List<float>[]>>> e = new Dictionary<string, List<List<List<float>[]>>>();

//            Dictionary<string, List<List<float[]>>> meansI = new Dictionary<string, List<List<float[]>>>();
//            Dictionary<string, List<List<float[]>>> sdsI = new Dictionary<string, List<List<float[]>>>();

//            Dictionary<string, List<List<float[]>>> meansE = new Dictionary<string, List<List<float[]>>>();
//            Dictionary<string, List<List<float[]>>> sdsE = new Dictionary<string, List<List<float[]>>>();

//            Dictionary<string, List<List<float[]>>> meansT = new Dictionary<string, List<List<float[]>>>();
//            Dictionary<string, List<List<float[]>>> sdsT = new Dictionary<string, List<List<float[]>>>();


//            Dictionary<string, List<List<float[]>>> cvI = new Dictionary<string, List<List<float[]>>>();
//            Dictionary<string, List<List<float[]>>> cvE = new Dictionary<string, List<List<float[]>>>();
//            Dictionary<string, List<List<float[]>>> cvT = new Dictionary<string, List<List<float[]>>>();

//            Dictionary<string, List<string>> extractedNames = new Dictionary<string, List<string>>();

//            foreach (sighEntry entry in this.listBox1.Items)
//            {
//                #region Get I/E

//                for (int n = 0; n < entry.Names.Count; n++)
//                {
//                    //Test this
//                    if (entry.Vars[n, 0] == null || entry.Vars[n, 1] == null) continue;

//                    string name = entry.Names[n];
//                    if (!i.ContainsKey(name))
//                    {
//                        i.Add(name, new List<List<List<float>[]>>());
//                        e.Add(name, new List<List<List<float>[]>>());
//                        extractedNames.Add(name, new List<string>());
//                    }

//                    i[name].Add(new List<List<float>[]>());
//                    e[name].Add(new List<List<float>[]>());

//                    //Add the first sigh
//                    i[name][i[name].Count - 1].Add(new List<float>[numintervals * 2]);
//                    e[name][e[name].Count - 1].Add(new List<float>[numintervals * 2]);

//                    extractedNames[name].Add(entry.Vars[n,0].FullDescription);

//                    int sighCount = 0;

//                    //Add the intervals
//                    for (int j = 0; j < numintervals * 2; j++)
//                    {
//                        i[name][i[name].Count - 1][sighCount][j] = new List<float>();
//                        e[name][e[name].Count - 1][sighCount][j] = new List<float>();
//                    }

//                    sighCount = getData(length, numintervals, i, e, entry, n, name, sighCount);

                    

//                    if (!meansI.ContainsKey(name))
//                    {
//                        meansI.Add(name, new List<List<float[]>>());
//                        sdsI.Add(name, new List<List<float[]>>());

//                        meansE.Add(name, new List<List<float[]>>());
//                        sdsE.Add(name, new List<List<float[]>>());

//                        meansT.Add(name, new List<List<float[]>>());
//                        sdsT.Add(name, new List<List<float[]>>());

//                        cvI.Add(name, new List<List<float[]>>());
//                        cvE.Add(name, new List<List<float[]>>());
//                        cvT.Add(name, new List<List<float[]>>());

//                    }

//                    meansI[name].Add(new List<float[]>());
//                    sdsI[name].Add(new List<float[]>());

//                    meansE[name].Add(new List<float[]>());
//                    sdsE[name].Add(new List<float[]>());

//                    meansT[name].Add(new List<float[]>());
//                    sdsT[name].Add(new List<float[]>());

//                    cvI[name].Add(new List<float[]>());
//                    cvE[name].Add(new List<float[]>());
//                    cvT[name].Add(new List<float[]>());



//                    for (int j = 0; j < i[name][e[name].Count - 1].Count; j++)
//                    {
//                        meansI[name][meansI[name].Count - 1].Add(new float[numintervals * 2]);
//                        sdsI[name][sdsI[name].Count - 1].Add(new float[numintervals * 2]);

//                        meansE[name][meansI[name].Count - 1].Add(new float[numintervals * 2]);
//                        sdsE[name][sdsI[name].Count - 1].Add(new float[numintervals * 2]);

//                        meansT[name][meansI[name].Count - 1].Add(new float[numintervals * 2]);
//                        sdsT[name][sdsI[name].Count - 1].Add(new float[numintervals * 2]);

//                        cvI[name][cvI[name].Count - 1].Add(new float[numintervals * 2]);
//                        cvE[name][cvE[name].Count - 1].Add(new float[numintervals * 2]);
//                        cvT[name][cvE[name].Count - 1].Add(new float[numintervals * 2]);

//                        for (int k = 0; k < numintervals * 2; k++)
//                        {
//                            int numPts = i[name][i[name].Count - 1][j][k].Count;
//                            if (numPts == 0) continue;

//                            if (k == numintervals - 1 || k == numintervals * 2 - 1) numPts--;

//                            for (int l = 0; l < i[name][i[name].Count - 1][j][k].Count - 1; l++)
//                            {
//                                //The I times are from E - I
//                                i[name][i[name].Count - 1][j][k][l] = e[name][e[name].Count - 1][j][k][l] - i[name][i[name].Count - 1][j][k][l];

//                                //The E times are from I - E
//                                e[name][e[name].Count - 1][j][k][l] = i[name][i[name].Count - 1][j][k][l + 1] - e[name][e[name].Count - 1][j][k][l];


//                                //Add to means
//                                meansI[name][meansI[name].Count - 1][j][k] += i[name][i[name].Count - 1][j][k][l] / numPts;
//                                meansE[name][meansI[name].Count - 1][j][k] += e[name][e[name].Count - 1][j][k][l] / numPts;
//                                meansT[name][meansI[name].Count - 1][j][k] += (i[name][i[name].Count - 1][j][k][l] + e[name][e[name].Count - 1][j][k][l]) / numPts;
//                            }

//                            //Now do the last breath at the edge
//                            if (k != numintervals - 1 && k != numintervals * 2 - 1)
//                            {
//                                if (i[name][i[name].Count - 1][j][k + 1].Count == 0) continue;

//                                    i[name][i[name].Count - 1][j][k][i[name][i[name].Count - 1][j][k].Count - 1] =
//                                        e[name][e[name].Count - 1][j][k][i[name][i[name].Count - 1][j][k].Count - 1] -
//                                            i[name][i[name].Count - 1][j][k][i[name][i[name].Count - 1][j][k].Count - 1];

//                                        //This is looking at the first inspiration from the next bin
//                                        e[name][e[name].Count - 1][j][k][e[name][e[name].Count - 1][j][k].Count - 1] =
//                                            i[name][i[name].Count - 1][j][k + 1][0] - e[name][e[name].Count - 1][j][k][e[name][e[name].Count - 1][j][k].Count - 1];
   
//                                meansI[name][meansI[name].Count - 1][j][k] += i[name][i[name].Count - 1][j][k][i[name][i[name].Count - 1][j][k].Count - 1] / numPts;
//                                meansE[name][meansI[name].Count - 1][j][k] += e[name][e[name].Count - 1][j][k][e[name][e[name].Count - 1][j][k].Count - 1] / numPts;
//                                meansT[name][meansI[name].Count - 1][j][k] += 
//                                    ( i[name][i[name].Count - 1][j][k][i[name][i[name].Count - 1][j][k].Count - 1] +
//                                    e[name][e[name].Count - 1][j][k][e[name][e[name].Count - 1][j][k].Count - 1])/numPts;

//                            }
//                            else
//                            {
//                                //We can't get the first inspiration from the next bin because it's out of range, so just throw away this breath
//                                i[name][i[name].Count - 1][j][k].RemoveAt(i[name][i[name].Count - 1][j][k].Count - 1);
//                                e[name][e[name].Count - 1][j][k].RemoveAt(e[name][e[name].Count - 1][j][k].Count - 1);
//                            }

//                            //Calc SD
//                            for (int l = 0; l < i[name][i[name].Count - 1][j][k].Count; l++)
//                            {
//                                float total = i[name][i[name].Count - 1][j][k][l] + e[name][e[name].Count - 1][j][k][l];

//                                //This is (yi - mean)*(yi-mean)/N
//                                sdsI[name][sdsI[name].Count - 1][j][k] += (i[name][i[name].Count - 1][j][k][l] - meansI[name][meansI[name].Count - 1][j][k]) *
//                                    (i[name][i[name].Count - 1][j][k][l] - meansI[name][meansI[name].Count - 1][j][k]) / i[name][i[name].Count - 1][j][k].Count;

//                                sdsE[name][sdsE[name].Count - 1][j][k] += (e[name][e[name].Count - 1][j][k][l] - meansE[name][meansE[name].Count - 1][j][k]) *
//                                    (e[name][e[name].Count - 1][j][k][l] - meansE[name][meansE[name].Count - 1][j][k]) / i[name][i[name].Count - 1][j][k].Count;

//                                sdsT[name][sdsT[name].Count - 1][j][k] += (total - meansT[name][meansT[name].Count - 1][j][k]) * (total - meansT[name][meansT[name].Count - 1][j][k]) /
//                                    i[name][i[name].Count - 1][j][k].Count;
//                            }

//                            //Take the square roots to complete the SD
//                            sdsI[name][sdsI[name].Count - 1][j][k] = (float)Math.Sqrt(sdsI[name][sdsI[name].Count - 1][j][k]);
//                            sdsE[name][sdsE[name].Count - 1][j][k] = (float)Math.Sqrt(sdsE[name][sdsE[name].Count - 1][j][k]);
//                            sdsT[name][sdsT[name].Count - 1][j][k] = (float)Math.Sqrt(sdsT[name][sdsT[name].Count - 1][j][k]);

//                            //CV = SD/Mean
//                            cvI[name][cvI[name].Count - 1][j][k] = sdsI[name][sdsI[name].Count - 1][j][k] / meansI[name][meansI[name].Count - 1][j][k];
//                            cvE[name][cvE[name].Count - 1][j][k] = sdsE[name][sdsI[name].Count - 1][j][k] / meansE[name][meansE[name].Count - 1][j][k];
//                            cvT[name][cvE[name].Count - 1][j][k] = sdsT[name][sdsI[name].Count - 1][j][k] / meansT[name][meansT[name].Count - 1][j][k];

//                        }
//                    }
//                }

//                #endregion
//            }

//            if (i.Count == 0 || e.Count == 0) return;

//            SaveFileDialog dia = new SaveFileDialog();
//            dia.DefaultExt = ".csv";
//            dia.AddExtension = true;
//            dia.Filter = "CSV(*.csv)|*.csv|All Files(*.*)|*.*";
//            if (dia.ShowDialog() == DialogResult.Cancel) return;

//            for (int sortBy = 0; sortBy < 3; sortBy++)
//            {
//                Dictionary<string, List<List<float[]>>> toSortBy = cvI;
//                if (sortBy == 1)
//                {
//                    toSortBy = cvE;
//                }
//                else if (sortBy == 2)
//                {
//                    toSortBy = cvT;
//                }


//                for (int which = 0; which < 4; which++)
//                {
//                    List<List<object>> toWrite = new List<List<object>>();

//                    if (which == 0)
//                    {
//                        withinWeeks = false;
//                        withinExpts = false;
//                    }
//                    else if (which == 1)
//                    {
//                        withinWeeks = true;
//                        withinExpts = false;
//                    }
//                    else if (which == 2)
//                    {
//                        withinExpts = true;
//                        withinWeeks = true;
//                    }

//                    Dictionary<string, List<float>> exptMeans = null;
//                    Dictionary<string, List<float>> exptSD = null;

//                    Dictionary<string, float> weekMean = null;
//                    Dictionary<string, float> weekSD = null;

//                    float totMean = float.MaxValue;
//                    float totSD = float.MaxValue;

//                    int firstLine = 0;
//                    int firstColumn = 0;

//                    //For each week
//                    foreach (string name in toSortBy.Keys)
//                    {
//                        #region Calculate based on groups

//                        toWrite.Add(new List<object>());
//                        toWrite[firstLine].Add(name);
//                        toWrite[firstLine].Add("SD Multiplier: " + sd);
//                        toWrite[firstLine].Add("Interval Length: " + length);

//                        firstLine++;

//                        toWrite.Add(new List<object>());
//                        toWrite[firstLine].Add("");
//                        firstLine++;

//                        toWrite.Add(new List<object>());

//                        //For each variable
//                        for (int v = 0; v < toSortBy[name].Count; v++)
//                        {

//                            toWrite[firstLine].Add(extractedNames[name][v]);

//                            float smean = 0;
//                            float ssd = 0;

//                            if (which != 3)
//                            {
//                                this.getAvgSd(toSortBy, withinWeeks, withinExpts, name, v, out smean, out ssd,
//                                    ref totMean, ref totSD, ref weekMean, ref weekSD, ref exptMeans, ref exptSD);

//                                toWrite[firstLine - 1].Add("CV Mean: " + smean);
//                                toWrite[firstLine - 1].Add("CV SD: " + ssd);
//                            }
//                            else
//                            {

//                                toWrite[firstLine - 1].Add("");
//                                toWrite[firstLine - 1].Add("");
//                            }


//                            //For each sigh
//                            for (int j = 0; j < toSortBy[name][v].Count; j++)
//                            {
//                                if (which == 3)
//                                {
//                                    for (int k = 0; k < toSortBy[name][v][j].Length; k++)
//                                    {
//                                        smean += toSortBy[name][v][j][k];
//                                    }

//                                    smean /= toSortBy[name][v][j].Length;

//                                    for (int k = 0; k < toSortBy[name][v][j].Length; k++)
//                                    {
//                                        ssd += (toSortBy[name][v][j][k] - smean) * (toSortBy[name][v][j][k] - smean);
//                                    }

//                                    ssd /= toSortBy[name][v][j].Length;
//                                    ssd = (float)Math.Sqrt(ssd);
//                                }

//                                //For each partition
//                                for (int k = 0; k < toSortBy[name][v][j].Length; k++)
//                                {


//                                    //Mark the partition number if it's the first sigh
//                                    if (j == 0)
//                                    {
//                                        if (k < toSortBy[name][v][j].Length / 2)
//                                            toWrite[firstLine].Add(-toSortBy[name][v][j].Length / 2 + k);
//                                        else toWrite[firstLine].Add(k - toSortBy[name][v][j].Length / 2 + 1);
//                                    }

//                                    //If this is the first variable, then create the row and write the sigh number
//                                    if (k == 0 && firstColumn == 0)
//                                    {
//                                        toWrite.Add(new List<object>());
//                                        toWrite[firstLine + j + 1].Add(j + 1);
//                                    }

//                                    float val = toSortBy[name][v][j][k];


//                                    //statistically large CV
//                                    if (val > smean + ssd * sd)
//                                    {
//                                        toWrite[firstLine + j + 1].Add(1);

//                                    }
//                                    else if (val < smean - ssd * sd) //statistically small CV
//                                    {
//                                        toWrite[firstLine + j + 1].Add(-1);

//                                    }
//                                    else
//                                    {
//                                        toWrite[firstLine + j + 1].Add(0);
//                                    }
//                                }

//                                //At the end of all paritions, write a blank
//                                toWrite[firstLine + j + 1].Add("");
//                            }

//                            for (int l = 0; l < toSortBy[name][v][0].Length - 1; l++)
//                            {
//                                toWrite[firstLine - 1].Add("");
//                            }

//                            toWrite[firstLine].Add("");
//                        }

//                        //After all variables, write a blank line to move onto the next week
//                        toWrite.Add(new List<object>());
//                        toWrite.Add(new List<object>());
//                        firstLine = toWrite.Count;

//                        #endregion

//                    }

//                    object[][] toWriteA = new object[toWrite.Count][];
//                    int maxWidth = int.MinValue;
//                    for (int a = 0; a < toWrite.Count; a++)
//                    {
//                        if (toWrite[a].Count > maxWidth)
//                        {
//                            maxWidth = toWrite[a].Count;
//                        }
//                    }

//                    for (int a = 0; a < toWrite.Count; a++)
//                    {
//                        toWriteA[a] = new object[maxWidth];
//                        for (int b = 0; b < toWrite[a].Count; b++)
//                        {
//                            toWriteA[a][b] = toWrite[a][b];
//                        }
//                    }


//                    string ext = dia.FileName.Substring(0, dia.FileName.LastIndexOf('.'));
//                    if (which == 0)
//                    {
//                        ext += " all";
//                    }
//                    else if (which == 1)
//                    {
//                        ext += " weeks";
//                    }
//                    else if (which == 2) ext += " expt and weeks";
//                    else
//                    {
//                        ext += " within sighs";
//                    }

//                    ext += ".csv";

//                    CSVWriter.WriteCSV(ext, toWriteA);
//                }
//            }
//        }

//        private static int getData(double length, int numintervals, Dictionary<string, List<List<List<float>[]>>> i, Dictionary<string, List<List<List<float>[]>>> e, sighEntry entry, int n, string name, int sighCount)
//        {
//            float endTime = (float)entry.StartTimes[n][0];
//            float startTime = (float)(endTime - numintervals * length);

//            float res = (float)entry.Vars[n, 0].Resolution;

//            bool first = true;
//            bool go = true;

//            float lastVal = -1;

//            entry.Vars[n, 0][0].Pool.GetData(null, null,
//                delegate(float[] data, uint[] indices, uint[] counts)
//                {
//                    if (go)
//                    {
//                        for (int j = 0; j < data.Length; j++)
//                        {
//                            float time = data[j] * res;
//                            if (time < startTime) continue;

//                            if (time >= startTime && time <= endTime)
//                            {
//                                int bin = (int)((time - startTime) / length);

//                                if (!first) bin += numintervals;

//                                if (time == lastVal)
//                                {
//                                    continue;
//                                }

//                                i[name][i[name].Count - 1][sighCount][bin].Add(time);
//                                lastVal = time;
//                            }

//                            if (time > endTime)
//                            {
//                                if (first)
//                                {
//                                    startTime = (float)entry.EndTimes[n][sighCount];
//                                    endTime = (float)(startTime + numintervals * length);
//                                    first = false;
//                                }
//                                else
//                                {
//                                    sighCount++;

//                                    if (sighCount >= entry.StartTimes[n].Count)
//                                    {
//                                        go = false;
//                                        break;
//                                    }

//                                    endTime = (float)entry.StartTimes[n][sighCount];
//                                    startTime = (float)(endTime - numintervals * length);

//                                    //Add the sigh
//                                    i[name][i[name].Count - 1].Add(new List<float>[numintervals * 2]);

//                                    //Add the intervals
//                                    for (int k = 0; k < numintervals * 2; k++)
//                                    {
//                                        i[name][i[name].Count - 1][sighCount][k] = new List<float>();
//                                    }
//                                    first = true;
//                                }


//                            }
//                        }
//                    }
//                }
//            );

//            go = true;
//            sighCount = 0;
//            endTime = (float)entry.StartTimes[n][0];
//            startTime = (float)(endTime - numintervals * length);
//            first = true;

//            int ebin = 0;

//            float lastDiff = 0;
//            entry.Vars[n, 1][0].Pool.GetData(null, null,
//                delegate(float[] data, uint[] indices, uint[] counts)
//                {
//                    if (go)
//                    {
//                        for (int j = 0; j < data.Length; j++)
//                        {
//                            float time = data[j] * res;
//                            if (time < startTime) continue;

//                            if (time >= startTime && time <= endTime)
//                            {
//                                //Match the e to the i
//                                if (e[name][e[name].Count - 1][sighCount][ebin].Count ==
//                                    i[name][e[name].Count - 1][sighCount][ebin].Count)
//                                {
//                                    ebin++;
//                                }

//                                if (time == lastVal)
//                                {
//                                    continue;
//                                }

//                                // int bin = (int)((time - startTime) / length);

//                                // if (!first) bin += numintervals;

//                                if (time - i[name][e[name].Count - 1][sighCount][ebin][e[name][e[name].Count - 1][sighCount][ebin].Count] < 0)
//                                {
//                                    continue;
//                                }

//                                lastDiff = time - i[name][e[name].Count - 1][sighCount][ebin][e[name][e[name].Count - 1][sighCount][ebin].Count];

//                                if (ebin < e[name][e[name].Count - 1][sighCount].Length)
//                                    e[name][e[name].Count - 1][sighCount][ebin].Add(time);
//                                lastVal = time;



//                            }

//                            if (time > endTime)
//                            {
//                                if (first)
//                                {
//                                    startTime = (float)entry.EndTimes[n][sighCount];
//                                    endTime = (float)(startTime + numintervals * length);
//                                    first = false;
//                                }
//                                else
//                                {
//                                    if (ebin != i[name][e[name].Count - 1][sighCount].Length)
//                                    {
//                                        while (e[name][e[name].Count - 1][sighCount][ebin].Count !=
//                                            i[name][e[name].Count - 1][sighCount][ebin].Count)
//                                        {
//                                            i[name][e[name].Count - 1][sighCount][ebin].RemoveAt(
//                                                i[name][e[name].Count - 1][sighCount][ebin].Count - 1);
//                                        }
//                                    }

//                                    sighCount++;

//                                    if (sighCount >= entry.StartTimes[n].Count)
//                                    {
//                                        go = false;
//                                        break;
//                                    }

//                                    endTime = (float)entry.StartTimes[n][sighCount];
//                                    startTime = (float)(endTime - numintervals * length);

//                                    //Add the sigh
//                                    e[name][e[name].Count - 1].Add(new List<float>[numintervals * 2]);

//                                    //Add the intervals
//                                    for (int k = 0; k < numintervals * 2; k++)
//                                    {
//                                        e[name][e[name].Count - 1][sighCount][k] = new List<float>();
//                                    }
//                                    first = true;
//                                    ebin = 0;

//                                }


//                            }
//                        }
//                    }
//                }
//            );
//            return sighCount;
//        }

//        private bool setup(sighEntry entry)
//        {
//            object[][] csv = CSVReader.ReadCSV(entry.File, 10);
//            if (csv == null) return false;

//            int numcols = csv.Length;

//            string[] names = new string[numcols - 1];
//            string[] vars = new string[this.listBox2.Items.Count + 1];

//            for (int i = 0; i < names.Length; i++)
//            {
//                names[i] = csv[i+1][1] as string;
//            }

//            entry.Names.AddRange(names);

//            vars[0] = "None";
//            for (int i = 1; i < vars.Length; i++)
//            {
//                vars[i] = (this.listBox2.Items[i-1] as Variable).FullDescription;
//            }

//            Type enumTest = RequestInformationPropertyGrid.CreateEnum(vars);

//            Type[] types = new Type[names.Length];
//            string[] cats = new string[names.Length];
//            string[] descrips = new string[names.Length];
//            for (int i = 0; i < names.Length; i++)
//            {
//                types[i] = enumTest;
//                cats[i] = "Vars";
//                descrips[i] = names[i];
//            }


//            RequestInformation info = new RequestInformation();
//            info.Grid.SetInformationToCollect(
//                types,
//                cats,
//                names,
//                descrips,
//                null,
//                null);
//            if (info.ShowDialog() == DialogResult.Cancel) return false;

//            entry.Vars = new SpikeVariable[names.Length, 2];

//            //Associate the selected names with the variables
//            for (int i = 0; i < names.Length; i++)
//            {
//                int which = (int)info.Grid.Results[names[i]];
//                if (which == 0) continue;
//                bool found = false;
//                SpikeVariable v = this.listBox2.Items[which - 1] as SpikeVariable;
//                if (v.Header.VarName[v.Header.VarName.Length - 1] == 'I')
//                {
//                    //Set the variable to the one selected
//                    entry.Vars[i, 0] = (this.listBox2.Items[which - 1] as SpikeVariable);
                    
//                    //This goes through all the variables and looks for the matching one
//                    for (int j = 0; j < this.listBox2.Items.Count; j++)
//                    {
//                        SpikeVariable test = this.listBox2.Items[j] as SpikeVariable;
//                        if (test.FullVarName.Contains(v.FullVarName.Substring(0, v.FullVarName.Length - 1)) && test != v)
//                        {
//                            entry.Vars[i, 1] = test;
//                            found = true;
//                            break;
//                        }
//                    }
//                }
//                else
//                {
//                    entry.Vars[i, 1] = (this.listBox2.Items[which - 1] as SpikeVariable);
//                    for (int j = 0; j < this.listBox2.Items.Count; j++)
//                    {
//                        SpikeVariable test = this.listBox2.Items[j] as SpikeVariable;
//                        if (test.FullVarName.Contains(v.FullVarName.Substring(0, v.FullVarName.Length - 1)) && test != v)
//                        {
//                            entry.Vars[i, 0] = test;
//                            found = true;
//                            break;
//                        }
//                    }
//                }

//                if (!found) return false;
//            }

//            entry.StartTimes = new List<double>[numcols - 1];
//            entry.EndTimes = new List<double>[numcols - 1];

//            for (int r = 2; r < csv[0].Length; r++)
//            {
//                for (int i = 1; i < numcols; i++)
//                {
//                    int index = i - 1;
//                    if (entry.StartTimes[index] == null) entry.StartTimes[index] = new List<double>();
//                    if (entry.EndTimes[index] == null) entry.EndTimes[index] = new List<double>();

//                    string sigh = csv[i][r] as string;
//                    if (sigh == null) continue;

//                    string[] splits = sigh.Split('-');
//                    if (splits.Length != 2) continue;

//                    float startval = 0;
//                    float endval = 0;
//                    bool good = float.TryParse(splits[0], out startval);
//                    good = float.TryParse(splits[1], out endval) || good;

//                    if (!good) continue;

//                    entry.StartTimes[index].Add(startval);
//                    entry.EndTimes[index].Add(endval);
//                }
//            }

//            return true;
//        }

//        private void SighProject_Load(object sender, EventArgs e)
//        {
//            this.listBox2.Items.AddRange(VariableSource.Variables.ToString());
//        }

//        private void buttonGetFile_Click(object sender, EventArgs e)
//        {
//            OpenFileDialog dialog = new OpenFileDialog();
//            dialog.AddExtension = true;
//            dialog.DefaultExt = ".csv";
//            dialog.Filter = "CSV(*.csv)|*.csv|All Files(*.*)|*.*";
//            if (dialog.ShowDialog() == DialogResult.Cancel) return;
//            this.labelFile.Text = dialog.FileName;

//            sighEntry entry = new sighEntry();
//            entry.File = this.labelFile.Text;

//            if (!this.setup(entry))
//            {
//                MessageBox.Show("There was an error setting up the file");
//                return;
//            }

//            //entry.Var = this.comboBox1.SelectedItem as SpikeVariable;
//            this.listBox1.Items.Add(entry);
//        }

//        private void button1_Click(object sender, EventArgs e)
//        {
//            //if (this.listBox1.SelectedItem == null || !File.Exists(this.labelFile.Text)) return;
//            sighEntry entry = new sighEntry();
//            entry.File = this.labelFile.Text;

//            if (!this.setup(entry))
//            {
//                MessageBox.Show("There was an error setting up the file");
//                return;
//            }
            
//            //entry.Var = this.comboBox1.SelectedItem as SpikeVariable;
//            this.listBox1.Items.Add(entry);
//        }
//    }
//}