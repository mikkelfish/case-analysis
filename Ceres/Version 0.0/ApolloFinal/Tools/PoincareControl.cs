using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CeresBase.IO.ImageCreation;
using AviFile;

namespace ApolloFinal.Tools
{
    public partial class PoincareControl : UserControl
    {
        private float maxXRange;
        [System.ComponentModel.DefaultValue(1)]
        public float MaxXRange
        {
            get { return maxXRange; }
            set { maxXRange = value; }
        }

        private float maxYRange;
        [System.ComponentModel.DefaultValue(1)]
        public float MaxYRange
        {
            get { return maxYRange; }
            set { maxYRange = value; }
        }

        private int everyNth = 1;
        [System.ComponentModel.DefaultValue(1)]
        public int EveryNth
        {
            get { return everyNth; }
            set { everyNth = value; }
        }

        private bool autoRange;
        public bool AutoRange
        {
            get { return autoRange; }
            set { autoRange = value; }
        }

        private bool animate;
        public bool Animate
        {
            get { return animate; }
        }

        private double animTime = .5;   
        [System.ComponentModel.DefaultValue(0.5)]
        public double AnimateTime
        {
            get { return animTime; }
            set 
            {
                animTime = value;
                this.timer.Interval = (int)(this.animTime * 500.0);
            }
        }

        private float[] times;
	

        private bool loop;
        public bool Loop
        {
            get { return loop; }
            set { loop = value; }
        }

        private List<PointF> points = new List<PointF>();
        private int curAnimPoint = 0;
        private Timer timer = new Timer();
        private readonly object lockable = new object();
        private int curPart;
        private int curColor;

        private Color[] Colors = new Color[]{
            Color.Black,
            Color.Blue,
            Color.Maroon,
            Color.Orange,
            Color.Green,
            Color.DarkGray,
            Color.DarkGreen};
				
        public PoincareControl()
        {
            InitializeComponent();
            this.timer.Tick += new EventHandler(timer_Tick);
        }

        bool advanceCluster()
        {
            bool toRet = false;
            int last = this.points.Count - 1;
            if (this.cluster && this.clustersToDraw[curPart] + 1 < this.partitions.Count)
            {
                last = this.partitions[this.clustersToDraw[curPart] + 1];
            }

            if (this.curAnimPoint < last)
            {
                this.curAnimPoint++;
            }
            else
            {
                int first = 0;
                if (this.cluster)
                {
                    curPart++;
                    if (curPart == this.clustersToDraw.Length)
                    {
                        curPart = 0;
                    }
                    first = this.partitions[this.clustersToDraw[curPart]];

                    this.curColor++;
                    if (this.curColor == this.Colors.Length)
                    {
                        this.curColor = 0;
                    }
                }

                if (!loop)
                {
                    if (!this.cluster)
                    {
                        if (this.timer.Enabled)
                        {
                            this.Stop();
                        }
                        toRet = true;

                    }
                    else if (curPart == 0)
                    {
                        if (this.timer.Enabled)
                        {
                            this.Stop();
                        }
                        toRet = true;

                    }
                    else this.curAnimPoint = first;
                }
                else this.curAnimPoint = first;
            }

            return toRet;
        }


        void timer_Tick(object sender, EventArgs e)
        {
            if (this.animate)
            {
                this.advanceCluster();
                this.Invalidate();
            }
        }

        private Bitmap image = null;
        private Ellipse centroid;
        private float[] prevXs;
        private float[] prevYs;
        private int curXYPoint = 0;
        private bool drawCentroids = false;

        private PointF[] prevCentroids;
        private int curCentroid;
        private bool calcSD = false;

        private int clusterPoints = 15;
        [System.ComponentModel.DefaultValue(15)]
        public int ClusterPoints
        {
            get { return clusterPoints; }
            set { clusterPoints = value; }
        }

        private float sdMult = 3F;
        [System.ComponentModel.DefaultValue(3)]
        public float SDMult
        {
            get { return sdMult/2; }
            set { this.sdMult = value*2; }
        }

        private bool cluster;
        public bool Cluster
        {
            get { return cluster; }
            set { cluster = value;            
            }
        }
	

        private List<double> clusteringScores = new List<double>();
        private int[] clustersToDraw;
        public int[] ClustersToDraw
        {
            get
            {
                return this.clustersToDraw;
            }
            set
            {
                this.clustersToDraw = value;
                lock (this.lockable)
                {
                    this.image = null;
                }
                this.Invalidate();
            }
        }

        private List<int> partitions;
        public int[] Partitions
        {
            get
            {
                return this.partitions.ToArray();
            }
        }

        public void SaveImage(string filename, int width, int height)
        {
            SVG svg = new SVG();
            ImageContainer container = new ImageContainer(null, new Rectangle(0, 0, width, height));
            if (this.animate)
            {
                this.timer.Stop();
                this.animate = false;
            }
            
            
            svg.AddImageElement(container);
            this.drawImage(svg, container);
            svg.Create(new object[] { filename });
        }

        private const float pointSize = 4F;

        private GDI createNewAnimGDI(Graphics g, out ImageContainer parent, int width, int height)
        {
            GDI gdi = new GDI();
            parent = new ImageContainer(null, new Rectangle(0, 0, width, height));
            gdi.AddImageElement(parent);
            
            return gdi;
        }

        public void CreateMovie(string filename, int width, int height, double frameRate)
        {
            this.Stop();
            this.reset();

            this.animate = true;

            AviManager manager = new AviManager(filename, false);
            VideoStream stream = null;

            bool cont = true;
            Bitmap newMap = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(newMap);

            ImageContainer parent;
            GDI gdi = this.createNewAnimGDI(g, out parent, width, height);
            RectangleElement rect = new RectangleElement(parent, new PointF(0, 0), width, height);
            rect.Color = Color.White;
            rect.Fill = true;
            gdi.AddImageElement(rect);
            gdi.Create(new object[] { g });

            int skip = 0;
            while (cont)
            {
               // GDI gdi2 = new GDI();
               // gdi2.AddImageElement(parent);                
                this.drawImage(gdi, parent);
                gdi.Create(new object[] { g });
               // gdi = this.createNewAnimGDI(g, out parent, width, height);

                //Font font = new Font(FontFamily.GenericSerif, 12);
                //TextElement text = new TextElement(parent, font, "Time: " + this.times[this.curAnimPoint],
                //    new PointF(parent.Size.Width - 150, 5));
                //RectangleElement re = new RectangleElement(parent, new PointF(parent.Size.Width - 150, 5),
                //    150, 15);
                //re.Color = Color.White;
                //re.Fill = true;
                //gdi.AddImageElement(re);
                
                //gdi.AddImageElement(text);

                if (stream == null)
                {
                    stream = manager.AddVideoStream(true, frameRate, newMap);
                    
                }
                
                if(skip > 2) stream.AddFrame(newMap);
                skip++;

                cont = !this.advanceCluster();
            }

            g.Dispose();
            newMap.Dispose();


            //stream.Close();
            manager.Close();


            this.animate = false;
        }

        public void RunClustering()
        {
            this.Stop();
            this.reset();

            this.animate = true;
            ImageContainer container = new ImageContainer(null, new Rectangle(0, 0, this.Width, this.Height));

            for (int i = 0; i < this.points.Count - 1; i++)
            {
                this.drawImage(null, container);
                this.drawCentroid(null, container);
                this.curAnimPoint++;
            }

            double SD = 0;
            double mean = 0;
            for (int i = 0; i < this.clusteringScores.Count; i++)
            {
                mean += this.clusteringScores[i];
            }
            mean /= this.clusteringScores.Count;

            for (int i = 0; i < this.clusteringScores.Count; i++)
            {
                SD += (this.clusteringScores[i] - mean) * (this.clusteringScores[i] - mean);
            }
            SD /= (this.clusteringScores.Count - 1);
            SD = Math.Sqrt(SD);

            bool newPart = true;
            partitions = new List<int>();
            partitions.Add(0);
            for (int i = 1; i < this.clusteringScores.Count; i++)
            {
                if (!newPart && this.clusteringScores[i] > mean + SD * this.SDMult)
                {
                    newPart = true;
                    partitions.Add(i);
                }
                else if (this.clusteringScores[i] <= mean + SD * this.SDMult)
                {
                    newPart = false;
                }
            }
            this.animate = false;
        }

        private void drawCentroid(ImageCreator creator, ImageContainer parent)
        {
            float xSum = 0;
            float ySum = 0;
            for (int j = 0; j < this.prevXs.Length; j++)
            {
                xSum += this.prevXs[j];
                ySum += this.prevYs[j];
            }

            this.centroid = new Ellipse(parent, xSum / (float)(this.prevYs.Length) - 5,
                ySum / (float)(this.prevYs.Length) - 5, PoincareControl.pointSize, PoincareControl.pointSize);
            this.centroid.Fill = true;
            this.centroid.Color = Color.Yellow;

            this.prevCentroids[this.curCentroid] = this.centroid.Location;
            this.curCentroid++;
            if (this.curCentroid == this.prevCentroids.Length)
            {
                    this.curCentroid = 0;
                    this.calcSD = true;
            }

            if (this.calcSD)
            {
                    float ySD = 0;
                    float xSD = 0;
                    float yMean = 0;
                    float xMean = 0;

                    for (int j = 0; j < this.prevCentroids.Length; j++)
                    {
                        xMean += this.prevCentroids[j].X;
                        yMean += this.prevCentroids[j].Y;
                    }

                    xMean /= this.prevCentroids.Length;
                    yMean /= this.prevCentroids.Length;

                    for (int j = 0; j < this.prevCentroids.Length; j++)
                    {
                        ySD += (this.prevCentroids[j].Y - yMean) * (this.prevCentroids[j].Y - yMean);
                        xSD += (this.prevCentroids[j].X - xMean) * (this.prevCentroids[j].X - xMean);
                    }

                    ySD /= (this.prevCentroids.Length - 1);
                    xSD /= (this.prevCentroids.Length - 1);
                    ySD = (float)Math.Sqrt(ySD);
                    xSD = (float)Math.Sqrt(xSD);

                    Ellipse sd = new Ellipse(parent, xMean - xSD * this.sdMult / 2,
                        yMean - ySD * this.sdMult / 2F, xSD * this.sdMult, ySD * this.sdMult);
                    sd.Color = Color.Orange;
                   
                
                    if(creator != null) creator.AddImageElement(sd);

                    float relX = this.centroid.Location.X - (sd.Location.X + xSD * this.sdMult / 2);
                    float relY = this.centroid.Location.Y - (sd.Location.Y + ySD * this.sdMult / 2);
                    float xAxis = (xSD * this.sdMult) / 2;
                    float yAxis = (ySD * this.sdMult) / 2;
                    if ((relX * relX) / (xAxis * xAxis) + (relY * relY) / (yAxis * yAxis) > 1)
                    {
                        this.centroid.Color = Color.Brown;
                    }

                    double val = (relX * relX) + (relY * relY)/((xAxis*yAxis)*(xAxis*yAxis));
                    this.clusteringScores.Add(val);   
                // labelScore.Text = val.ToString();

                    //if ((this.centroid.Location.X < sd.Location.X ||
                    //    this.centroid.Location.X > sd.Size.X + sd.Size.Width) &&
                    //    (this.centroid.Location.Y < sd.Location.Y ||
                    //    this.centroid.Location.Y > sd.Location.Y + sd.Size.Height)
                    //    )
                    //{
                    //    this.centroid.Color = Color.Brown;
                    //}
                }
                if (creator != null) creator.AddImageElement(this.centroid);
        }

        private void drawImage(ImageCreator creator, ImageContainer parent)
        {
            lock (this.lockable)
            {
                        int bottom = 20;
                        int left = 20;
                        int right = 15;
                        int top = 5 + this.labelTime.Size.Height + this.labelTime.Location.Y;

                if (this.animate)
                {
                    for (int i = this.curAnimPoint - 1; i <= this.curAnimPoint; i++)
                    {
                        if (i < 0) continue;

                        PointF point = this.points[i];
                        float x = point.X / this.maxXRange;
                        float y = point.Y / this.maxYRange;
                        if (x > 1 || y > 1) continue;

                        x = x * (parent.Size.Width - right - left) + left;
                        y = parent.Size.Height - (y * (parent.Size.Height - top - bottom) + bottom);

                        //if (this.centroid != null)
                        //{
                        //    this.centroid = new Ellipse(parent, this.xSums / (float)(this.curAnimPoint),
                        //                                this.ySums / (float)(this.curAnimPoint), 10, 10);
                        //    this.centroid.Fill = true;
                        //    this.centroid.Color = Color.Gray;
                        //   creator.AddImageElement(this.centroid);
                        //}

                        if (i == this.curAnimPoint)
                        {
                            this.prevYs[this.curXYPoint] = y;
                            this.prevXs[this.curXYPoint] = x;
                            this.curXYPoint++;
                            if (this.curXYPoint == this.prevXs.Length)
                            {
                                this.curXYPoint = 0;
                                this.drawCentroids = true;
                            }
                        }


                        //if (i == this.curAnimPoint)
                        //{
                        //    this.xSums += x;
                        //    this.ySums += y;
                        //}
                        //if (i != 0)
                        //{
                        //    this.centroid = new Ellipse(parent, this.xSums / (float)(i),
                        //        this.ySums / (float)(i), 10, 10);
                        //    this.centroid.Fill = true;
                        //    if (i == this.curAnimPoint) this.centroid.Color = Color.Yellow;
                        //    else this.centroid.Color = Color.Gray;
                        //    creator.AddImageElement(this.centroid);
                        //}

                        Ellipse e = new Ellipse(parent, x - 5, y - 5, PoincareControl.pointSize, PoincareControl.pointSize);
                        e.Fill = true;
                        if (i == this.curAnimPoint)
                        {
                            e.Color = Color.Red;
                        }
                        else e.Color = this.Colors[this.curColor];
                        if (creator != null) creator.AddImageElement(e);
                    }
                }
                else
                {
                    foreach (PointF point in this.points)
                    {
                        float x = point.X / this.maxXRange;
                        float y = point.Y / this.maxYRange;
                        if (x > 1 || y > 1) continue;

                        x = x * (parent.Size.Width - right - left) + left;
                        y = parent.Size.Height - (y * (parent.Size.Height - top - bottom) + bottom);

                        Ellipse e = new Ellipse(parent, x - 5, y - 5, PoincareControl.pointSize, PoincareControl.pointSize);
                        e.Fill = true;
                        if (creator != null) creator.AddImageElement(e);
                    }
                }

                Line yline = new Line(parent, new PointF(left, parent.Size.Height - bottom), new PointF(left, top));
                Line xline = new Line(parent, new PointF(left, parent.Size.Height - bottom), new PointF(parent.Size.Width - right, parent.Size.Height - bottom));
                if (creator != null) creator.AddImageElement(xline);
                if (creator != null) creator.AddImageElement(yline);

            }
        }

        private void drawClusters(ImageCreator creator, ImageContainer parent)
        {
            lock (this.lockable)
            {
                int bottom = 20;
                int left = 20;
                int right = 15;
                int top = 5 + this.labelTime.Size.Height + this.labelTime.Location.Y;

                Line yline = new Line(parent, new PointF(left, parent.Size.Height - bottom), new PointF(left, top));
                Line xline = new Line(parent, new PointF(left, parent.Size.Height - bottom), new PointF(parent.Size.Width - right, parent.Size.Height - bottom));
                if (creator != null) creator.AddImageElement(xline);
                if (creator != null) creator.AddImageElement(yline);

                this.curColor = 0;
                foreach (int index in this.clustersToDraw)
                {
                    if (index < this.partitions.Count - 1)
                    {
                        for (int i = this.partitions[index]; i < this.partitions[index + 1]; i++)
                        {
                            float x = this.points[i].X / this.maxXRange;
                            float y = this.points[i].Y / this.maxYRange;
                            if (x > 1 || y > 1) continue;

                            x = x * (parent.Size.Width - right - left) + left;
                            y = parent.Size.Height - (y * (parent.Size.Height - top - bottom) + bottom);

                            Ellipse e = new Ellipse(parent, x - 5, y - 5, PoincareControl.pointSize, PoincareControl.pointSize);
                            e.Fill = true;
                            if (creator != null) creator.AddImageElement(e);
                            e.Color = this.Colors[this.curColor];
                        }
                    }
                    else
                    {
                        for (int i = this.partitions[index]; i < this.points.Count; i++)
                        {
                            float x = this.points[i].X / this.maxXRange;
                            float y = this.points[i].Y / this.maxYRange;
                            if (x > 1 || y > 1) continue;

                            x = x * (parent.Size.Width - right - left) + left;
                            y = parent.Size.Height - (y * (parent.Size.Height - top - bottom) + bottom);

                            Ellipse e = new Ellipse(parent, x - 5, y - 5, PoincareControl.pointSize, PoincareControl.pointSize);
                            e.Fill = true;
                            if (creator != null) creator.AddImageElement(e);
                            e.Color = this.Colors[this.curColor];
                        }
                    }
                    this.curColor++;
                    if (this.curColor == this.Colors.Length)
                    {
                        this.curColor = 0;
                    }
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            bool wasNull = this.image == null;
            lock (this.lockable)
            {
                if (this.image == null)
                {
                    this.image = new Bitmap(this.Width, this.Height);
                }
            }

            Bitmap centMap = null;

            if (this.animate || wasNull)
            {
                GDI gdi = new GDI();
                Graphics g = Graphics.FromImage(this.image);

                ImageContainer container = new ImageContainer(null, new Rectangle(0, 0, this.Width, this.Height));
                gdi.AddImageElement(container);
                if ((!this.animate && !this.cluster) || this.animate) this.drawImage(gdi, container);
                else this.drawClusters(gdi, container);

                gdi.Create(new object[] { g });

                if (this.drawCentroids)
                {
                    GDI cent = new GDI();
                    centMap = new Bitmap(this.image.Width, this.image.Height);
                    centMap.MakeTransparent(Color.White);
                    Graphics centG = Graphics.FromImage(centMap);
                    cent.AddImageElement(container);
                    this.drawCentroid(cent, container);
                    cent.Create(new object[] { centG });
                }

                if (this.times != null && this.curAnimPoint < this.times.Length)
                {
                    this.labelTime.Text = "Time: " + this.times[this.curAnimPoint];
                }
            }

            lock (this.lockable)
            {
                e.Graphics.DrawImage(this.image, 0, 0);
                if (centMap != null) e.Graphics.DrawImage(centMap, 0, 0);
            }
        }

        private void reset()
        {
            if (!this.cluster) this.curAnimPoint = 0;
            else
            {
                this.curAnimPoint = this.partitions[this.clustersToDraw[0]];
            }

            this.prevXs = new float[this.clusterPoints];
            this.prevYs = new float[this.clusterPoints];
            this.drawCentroids = false;
            this.curCentroid = 0;
            this.calcSD = false;
            this.prevCentroids = new PointF[this.clusterPoints];
            this.curXYPoint = 0;
            this.curPart = 0;
            this.curColor = 0;
        }

        public void Start()
        {
            if (this.points.Count == 0)
            {
                MessageBox.Show("Please create plot first.");
                return;
            }

            this.animate = true;

            this.reset();

            lock (this.lockable)
            {
                this.image = null;
            }
            this.timer.Start();
        }

        public void Stop()
        {
            if (this.animate)
            {
                this.timer.Stop();
                this.animate = false;
            }
        }

        public void CreatePlot(float[] times, float[] rawTimes)
        {
            if (this.animate)
            {
                this.timer.Stop();
                this.animate = false;
            }

            this.times = rawTimes;

            float maxTime = float.MinValue;
            for (int i = 0; i < times.Length; i++)
            {
                if (times[i] > maxTime)
                {
                    maxTime = times[i];
                }
            }

            if (this.autoRange)
            {
                this.maxXRange = maxTime;
                this.maxYRange = maxTime;
            }

            lock (this.lockable)
            {
                this.points.Clear();
                for (int i = 0; i < times.Length - this.everyNth - 1; i++)
                {
                    this.points.Add(new PointF(times[i], times[i + this.everyNth]));
                }
                this.image = null;
            }
            this.Invalidate();
        }
    }
}
