﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.DataBase;
using System.ComponentModel;

namespace ApolloFinal
{
    public class SpikeVariableEntryParameter : FileDataBaseVariableEntryParameter
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]                    
        public VariableEntryParameterItem<double> Frequency { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]                    
        public VariableEntryParameterItem<ApolloFinal.SpikeVariableHeader.SpikeType> SpikeType { get; set; }

        public SpikeVariableEntryParameter()
        {
            this.Frequency = new VariableEntryParameterItem<double>("Frequency") { IsRequired = true };
            this.SpikeType = new VariableEntryParameterItem<SpikeVariableHeader.SpikeType>("SpikeType") { IsRequired = true };
        }


        public override CeresBase.Data.VariableHeader CreateHeader(string experiment)
        {
            return new SpikeVariableHeader(this.VariableName.Value, this.Units.Value, experiment, this.Frequency.Value, this.SpikeType.Value) { Description = this.DisplayName.Value, Comments = this.Comments.Value };
        }
    }
}
