﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace ApolloFinal.CUDA
{
    public static class SampEnWrapper
    {
        //[DllImport("CUDASampleEntropy.dll", EntryPoint = "CUDASampEn"), SuppressUnmanagedCodeSecurity]
        //private extern unsafe static void CUDASampEn(float* data, int dataSize, int m, float r, int startTau, int endTau, double* E);

        //[DllImport("CUDASampleEntropy.dll", EntryPoint = "Supports", ExactSpelling=false ), SuppressUnmanagedCodeSecurity]
        //private extern unsafe static bool Supports(int device);

        [DllImport("CUDASampEn32.dll", EntryPoint = "CUDASampEn"), SuppressUnmanagedCodeSecurity]
        private extern unsafe static void CUDASampEn(float* data, int dataSize, int m, float r, int startTau, int endTau, double* E, StringBuilder error);

        [DllImport("CUDASampEn32.dll", EntryPoint = "Supports", ExactSpelling = false), SuppressUnmanagedCodeSecurity]
        private extern unsafe static bool Supports(int device, StringBuilder error);


        [DllImport("CUDASampEn32Multi.dll", EntryPoint = "CUDAEntryCall"), SuppressUnmanagedCodeSecurity]
        private extern unsafe static void CUDASampEnMulti(float* data, int dataSize, int m, float r, int startTau, int endTau, double* E, StringBuilder error, bool multi);


        [DllImport("CUDASampEn32Multi.dll", EntryPoint = "Supports", ExactSpelling = false), SuppressUnmanagedCodeSecurity]
        private extern unsafe static bool SupportsMulti(int device, StringBuilder error);

        public static bool Available(out bool first, out bool second)
        {
          //  MethodInfo info = typeof(SampEnWrapper).GetMethod("Supports", BindingFlags.Static | BindingFlags.NonPublic);
         //   object[] obj = info.GetCustomAttributes(true);
            first = false;
            second = false;
            try
            {
                first = Win32Constructs.SystemHelpers.CheckForDLL("cudart.dll");
                if(first)
                {
                    StringBuilder error = new StringBuilder("Testing", 1024);
                    
                    second = SupportsMulti(0, error);
                }

            

                return first && second;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " +
                    
               AppDomain.CurrentDomain.BaseDirectory
                    );
                }
                return false;
        }
        
        public static void CalculateSampEnCuda(float[] data, int m, float r, int startTau, int endTau, double[] E)
        {
            StringBuilder error = new StringBuilder("Testing", 1024);

            double[] tempE = new double[E.Length];

            unsafe
            {
                fixed (double* Ep = E, tempP = tempE)
                {
                    fixed (float* dataP = data)
                    {
                        //DateTime one = DateTime.Now;
                        //CUDASampEnMulti(dataP, data.Length, m, r, startTau, endTau, tempP, error, false);
                        //TimeSpan span = DateTime.Now.Subtract(one);

                        //one = DateTime.Now;
                        CUDASampEnMulti(dataP, data.Length, m, r, startTau, endTau, Ep, error, true);

                        if (error.ToString() != "")
                            throw new Exception("Error in sample entropy " + error);
                        //TimeSpan span2 = DateTime.Now.Subtract(one);                        
                    }
                }
            }
        }
    }
}
