﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernatePoweredDB.Collections;
using System.Collections;

namespace NHibernatePoweredDB.Mappings
{
    public class TestDict : HibernateDictionary<string, int>
    {
        public virtual IDictionary<string, int> Items
        {
            get
            {
                return (this as IHibernateCollection).Items as IDictionary<string, int>;
            }
            set
            {
                (this as IHibernateCollection).Items = value as ICollection;
            }

        }
    }
}
