﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernatePoweredDB.Collections;
using System.Collections;

namespace NHibernatePoweredDB.Mappings
{
    public class IntList : HibernateList<int>
    {
        public virtual IList<int> Items
        {
            get
            {
                return (this as IHibernateCollection).Items as IList<int>;
            }
            set
            {
                (this as IHibernateCollection).Items = value as ICollection;
            }
        }

       // private 
    }
}
