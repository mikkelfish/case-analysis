﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace NHibernatePoweredDB.Collections
{
    public abstract class HibernateList<T> : IHibernateCollection
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        //public virtual IList<T> Items { get; set; }
        System.Collections.ICollection IHibernateCollection.Items { get; set; }

        public HibernateList()
        {
            (this as IHibernateCollection).Items = new List<T>();
        }

        #region IHibernateCollection Members

        //System.Collections.ICollection IHibernateCollection.Items
        //{
        //    get
        //    {
        //        return this.Items as ICollection;
        //    }
        //    set
        //    {
        //        this.Items = value as IList<T>;
        //    }
        //}

        #endregion

        #region IHibernateCollection Members


        public virtual bool IsValidCollectionType(Type[] types)
        {
            if (types == null) return false;
            if (types.Length != 1) return false;
            return types[0] == typeof(T);
        }

        #endregion
    }
}
