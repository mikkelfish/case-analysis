﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace NHibernatePoweredDB.Collections
{
    public abstract class HibernateDictionary<U,T> : IHibernateCollection
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        //public virtual IDictionary<U, T> Items { get; set; }

        

        public HibernateDictionary()
        {
            (this as IHibernateCollection).Items = new Dictionary<U, T>();
        }

        #region IHibernateCollection Members


        System.Collections.ICollection IHibernateCollection.Items
        {
            get;
            set;
        }

        #endregion

        #region IHibernateCollection Members


        public virtual bool IsValidCollectionType(Type[] types)
        {
            if (types == null) return false;
            if (types.Length != 2) return false;
            return types[0] == typeof(U) && types[1] == typeof(T);
        }

        #endregion
    }
}
