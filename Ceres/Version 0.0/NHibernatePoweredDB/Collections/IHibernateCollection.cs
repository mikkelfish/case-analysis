﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace NHibernatePoweredDB.Collections
{
    public interface IHibernateCollection
    {
        int ID { get; set; }
        string Name { get; set; }
        ICollection Items { get; set; }
        bool IsValidCollectionType(Type[] types);
    }
}
