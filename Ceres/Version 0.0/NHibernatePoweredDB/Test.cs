﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernatePoweredDB.Mappings;
using NHibernate.Criterion;
using System.Collections;

namespace NHibernatePoweredDB
{
    public static class Test
    {
        public static void Testing()
        {
           ISession session = SetupHibernate.Initialize("testing", false);
           List<TestObject> list2 = new List<TestObject>();
            

           using (var transaction = session.BeginTransaction())
           {
               try
               {
                   var list = SetupHibernate.InitializeHibernateCollection(session, new Type[] { typeof(int) });
                   list.Name = "NewTest";
                   List<int> il = list.Items as List<int>;
                   il.Add(125);
                   il.Add(195);


                   var dict = SetupHibernate.InitializeHibernateCollection(session, new Type[] { typeof(string), typeof(int) });
                   dict.Name = "testdict";
                   Dictionary<string,int> idict = dict.Items as Dictionary<string, int>;
                   idict.Add("twher", 1523);

                  session.Save(list);
                   session.Save(dict);

                   //dict.Items = new Dictionary<string, int>();
                   //session.Save(dict);

//                   session.SaveOrUpdate("est", meep);
                   transaction.Commit();
               }
               catch (Exception ex)
               {
                   int s = 0;
               }
           }

           List<IntList> intlist = null;
           //Dictionary<string, TestObject> dict = null;
            try
           {
               using (var transaction = session.BeginTransaction())
               {
                   var se = session.Get(typeof(IntList),1);
                   var ser = session.Load(typeof(IntList), 1);
                   intlist = session.CreateCriteria(typeof(IntList)).List<IntList>() as List<IntList>;
                   intlist = session.CreateCriteria(typeof(IntList)).Add(Restrictions.Eq("Name", "NewTest")).List<IntList>() as List<IntList>;

                   var whee = session.CreateCriteria(typeof(TestDict)).List<TestDict>();
                   
               }

              // list[0].Name = "wowzee";

               //using (var transaction = session.BeginTransaction())
               //{
               //    session.SaveOrUpdate(list[0]);
               //    transaction.Commit();
               //}
           }
           catch (Exception ex)
           {
               int s = 0;
           }

           SetupHibernate.ShutDown();
        }

    }
}
