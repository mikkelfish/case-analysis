﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernatePoweredDB.Collections;
using System.Collections;
using NHibernate.Criterion;

namespace NHibernatePoweredDB
{
    public class HibernateDB : IDisposable
    {
        public string DatabaseName { get; set; }
        public string DisplayName { get; set; }

        private List<IHibernateCollection> collections = new List<IHibernateCollection>();


        public HibernateDB(string databaseName, string displayName, HibernateMappingSpace[] spaces)
        {
            this.DisplayName = displayName;
            this.DatabaseName = databaseName;
            SetupHibernate.Initialize(this.DatabaseName, null, false);
        }

        public void SaveCollection(ICollection collection, string name)
        {
            IHibernateCollection hibernate = this.collections.SingleOrDefault(d => d.Name == name);
            ISession session = SetupHibernate.GetSession(this.DatabaseName);

            if (hibernate == null)
            {
                hibernate = SetupHibernate.InitializeHibernateCollection(session, collection);
                hibernate.Name = name;
            }

            hibernate.Items = collection;                


            using (var transaction = session.BeginTransaction())
            {
                try
                {                    
                    session.SaveOrUpdate(hibernate);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    int s = 0;
                }
            }

        }

        public void ReadCollection(ICollection collection, string name)
        {
            ISession session = SetupHibernate.GetSession(this.DatabaseName);

            using (var transaction = session.BeginTransaction())
            {
                Type queryType = SetupHibernate.GetHibernateCollectionType(session, collection); 
                IHibernateCollection hibernate = session.CreateCriteria(queryType).Add(Restrictions.Eq("Name", name)).UniqueResult() as IHibernateCollection;

                if (hibernate == null) return;
                if (!this.collections.Any(d => d.Name == name))
                    this.collections.Add(hibernate);

                if (collection is IDictionary)
                {
                    IDictionary dict = collection as IDictionary;
                    dict.Clear();

                    IDictionary hDict = hibernate.Items as IDictionary;
                    foreach (object key in hDict.Keys)
                    {
                        dict.Add(key, hDict[key]);
                    }
                }
                else if (collection is IList)
                {
                    IList list = collection as IList;
                    list.Clear();

                    IList hList = hibernate.Items as IList;
                    foreach (object value in hList)
                    {
                        list.Add(value);
                    }
                }
            } 
        }


        #region IDisposable Members

        public void Dispose()
        {
            SetupHibernate.ShutdownSession(this.DatabaseName);
        }

        #endregion
    }
}
