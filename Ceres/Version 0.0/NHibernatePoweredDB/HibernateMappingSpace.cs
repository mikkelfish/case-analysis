﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace NHibernatePoweredDB
{
    public class HibernateMappingSpace
    {
        public Assembly Assembly { get; set; }
        public string NameSpace { get; set; }
        public bool IsAuto { get; set; }
    }
}
