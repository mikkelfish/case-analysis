﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using System.IO;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Automapping;
using System.Reflection;
using NHibernatePoweredDB.Collections;
using System.Collections;

namespace NHibernatePoweredDB
{
    

    public static class SetupHibernate
    {
        //TODO put locks
        private static string currentDBFile;
        private static Dictionary<string, ISession> sessions = new Dictionary<string,ISession>();

        public static ISession Initialize(string name, bool rebuild)
        {
            return SetupHibernate.Initialize(name, null, rebuild);
        }

        public static ISession Initialize(string name, HibernateMappingSpace[] spaces, bool rebuild)
        {
            if (sessions.ContainsKey(name))
                return sessions[name];
            try
            {
                if (spaces == null)
                    spaces = new HibernateMappingSpace[] { new HibernateMappingSpace() { Assembly = Assembly.GetExecutingAssembly(), NameSpace = "Mappings", IsAuto=true } };

                ISessionFactory factory = doSetup(name + ".db", spaces, rebuild);
                ISession session = factory.OpenSession();
                sessions.Add(name, session);
                return session;
            }
            catch (Exception ex)
            {
                int s = 0;
            }
            return null;
        }

        public static ISession GetSession(string name)
        {
            if (!sessions.ContainsKey(name))
                return null;
            return sessions[name];
        }

        public static Type GetHibernateCollectionType(ISession session, ICollection collection)
        {
            Type[] types = collection.GetType().GetGenericArguments();
            return GetHibernateCollectionType(session, types);
        }

        public static Type GetHibernateCollectionType(ISession session, Type[] types)
        {
            var meta = session.SessionFactory.GetAllClassMetadata();
            foreach (var item in meta.Values)
            {
                Type t = item.GetMappedClass(EntityMode.Poco);
                if (t.GetInterface("NHibernatePoweredDB.Collections.IHibernateCollection") != null)
                {
                    PropertyInfo collectionProp = t.GetProperty("Items");
                    Type[] args = collectionProp.PropertyType.GetGenericArguments();
                    if (args.Length != types.Length)
                        continue;

                    bool ok = true;
                    for (int i = 0; i < args.Length; i++)
                    {
                        if (args[i] != types[i])
                        {
                            ok = false;
                            break;
                        }
                    }

                    if (ok)
                    {
                        return t;
                    }
                }
            }

            return null;
        }

        public static IHibernateCollection InitializeHibernateCollection(ISession session, ICollection collection)
        {
            Type[] types = collection.GetType().GetGenericArguments();
            return InitializeHibernateCollection(session, types);
        }

        //TODO check performance
        public static IHibernateCollection InitializeHibernateCollection(ISession session, Type[] types)
        {
            Type t = GetHibernateCollectionType(session, types);
            if(t != null)
                return t.InvokeMember("", BindingFlags.CreateInstance, null, null, null) as IHibernateCollection;

            return null;
        }

        public static void ShutdownSession(string name)
        {
            ISession session = GetSession(name);
            if (session == null) return;
            session.Close();
            sessions.Remove(name);
        }

        public static void ShutDown()
        {
            foreach (string sessionStr in sessions.Keys)
            {
                ISession session = sessions[sessionStr];
                session.Close();
                session.SessionFactory.Close();
            }
        }

        private static ISessionFactory doSetup(string file, HibernateMappingSpace[] spaces, bool rebuild)
        {
            SetupHibernate.currentDBFile = file;

            

            if (!File.Exists(file))
                rebuild = true;
            if (rebuild && File.Exists(file))
            {
                File.Delete(file);
            }

            FluentConfiguration config = Fluently.Configure()
               .Database(
                 SQLiteConfiguration.Standard
                   .UsingFile(file)
               );

             foreach (HibernateMappingSpace space in spaces)
             {

                 config.Mappings(m =>
                   {
                      
                           if(space.Assembly == null)
                               space.Assembly = Assembly.GetExecutingAssembly();

                           if (space.IsAuto)
                           {
                               if (space.NameSpace == null)
                               {
                                   m.AutoMappings.Add(AutoMap.Assembly(space.Assembly));
                               }
                               else
                               {
                                   m.AutoMappings.Add(AutoMap.Assembly(space.Assembly).Where(type => type.Namespace.EndsWith(space.NameSpace)));
                               }
                           }
                           else
                           {
                               m.FluentMappings.AddFromAssembly(space.Assembly);
                           }
                       
                   });                
             }

             if (rebuild)
             {
                 config.ExposeConfiguration(buildSchema);
             }
               
              return config.BuildSessionFactory();
        }

        //private static ISessionFactory doSetupWithRebuild(string file, HibernateMappingSpace[] spaces)
        //{
        //    SetupHibernate.currentDBFile = file;
        //    return Fluently.Configure()
        //       .Database(
        //         SQLiteConfiguration.Standard
        //           .UsingFile(file)
        //       )
        //      .Mappings(m =>
        //           {
        //               m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly());
        //               m.AutoMappings.Add(AutoMap.Assembly(Assembly.GetExecutingAssembly()).Where(type => type.Namespace.EndsWith("Mappings")));
        //           }
        //       )
        //        .ExposeConfiguration(buildSchema)        
        //       .BuildSessionFactory();
        //}

        private static void buildSchema(NHibernate.Cfg.Configuration config)
        {
            // delete the existing db on each run
            if (File.Exists(SetupHibernate.currentDBFile))
                File.Delete(SetupHibernate.currentDBFile);

            // this NHibernate tool takes a configuration (with mapping info in)
            // and exports a database schema from it
            new SchemaExport(config).Create(false, true);
        }
    }
}
