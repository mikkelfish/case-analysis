﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Collections.ObjectModel;
using CeresBase.IO.Serialization.PostgreSQL;

namespace DBManager
{
    public enum ManagerAction { DBAdded, DBChanged, DBRemoved, UserInfoChanged };

    public class DBManagerEventArgs : EventArgs
    {
        public string ClientTrigger { get; set; }
        public ManagerAction Action { get; set; }
        public string Args { get; set; }
    }

    // NOTE: If you change the class name "Service1" here, you must also update the reference to "Service1" in App.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]    
    public class DBManager : IDBManager
    {
        private delegate object SpinDelegate(object info);

        private static readonly object lockable = new object();
        private static List<Database> dbs = new List<Database>();
        private static List<User> users = new List<User>();
        private static List<Group> groups = new List<Group>();
        private static Dictionary<Group, List<User>> userGroups = new Dictionary<Group, List<User>>();
        private static Dictionary<string, EventHandler<DBManagerEventArgs>> knownClients = new Dictionary<string, EventHandler<DBManagerEventArgs>>();
        //private static Dictionary<string, string> clientToUid = new Dictionary<string, string>();

        public static event EventHandler<DBManagerEventArgs> UpdateEvent;


        private EventHandler<DBManagerEventArgs> thisHandler = null;
        private IDBCallBack callback = null;
        private string client;
        private User user;

        static DBManager()
        {
            string name = PostgreSQLDBEngine.GetComputerName(PostgreSQLDBEngine.UtilityConnection);
            if (name == null)
            {
                PostgreSQLDBEngine.SetComputerName("hostdb", PostgreSQLDBEngine.UtilityConnection);
            }

            string[] allDBs = PostgreSQLDBEngine.ListDBNames(PostgreSQLDBEngine.UtilityConnection);
            foreach (string db in allDBs)
            {
                string desc = PostgreSQLDBEngine.GetDatabaseComment(db, PostgreSQLDBEngine.UtilityConnection);
                if (desc == "ERROR DB NOT FOUND")
                    desc = db;
                dbs.Add(new Database() { Name = db, Description = desc });
            }

            User postgres = new User() { Name = "postgres", Institution = "system", Password = "postgres" };
            users.Add(postgres);
        }

        private void sendUpdate(DBManagerEventArgs e)
        {
            EventHandler<DBManagerEventArgs> handler = UpdateEvent;
            if (handler != null)
            {
                foreach (EventHandler<DBManagerEventArgs> hand in handler.GetInvocationList())
                {
                    hand.Invoke(this, e);
                    //hand.BeginInvoke(this, e, new AsyncCallback(EndAsync), null);
                }
            }
        }

        //private void EndAsync(IAsyncResult ar)
        //{
        //    EventHandler<DBManagerEventArgs> d = null;

        //    try
        //    {
        //        //get the standard System.Runtime.Remoting.Messaging.AsyncResult,and then
        //        //cast it to the correct delegate type, and do an end invoke
        //        System.Runtime.Remoting.Messaging.AsyncResult asres = (System.Runtime.Remoting.Messaging.AsyncResult)ar;
        //        d = ((EventHandler<DBManagerEventArgs>)asres.AsyncDelegate);
        //        d.EndInvoke(ar);
        //    }
        //    catch
        //    {
        //        UpdateEvent -= d;
        //    }
        //}

        #region IDBManager Members

        private object userAdd(object args)
        {
            User user = args as User;
            return user;
        }

        private void userEnd(IAsyncResult res)
        {
            SpinDelegate spin = (SpinDelegate)res.AsyncState;
            User user = spin.EndInvoke(res) as User;
            sendUpdate(new DBManagerEventArgs() { Action = ManagerAction.UserInfoChanged, Args = "Added:" + user.Name, ClientTrigger = this.client });
        }

        public void AddUser(User user)
        {
            SpinDelegate spin = new SpinDelegate(userAdd);
            spin.BeginInvoke(user, new AsyncCallback(userEnd), spin);
        }

        public void AddGroup(Group group)
        {
        }

        public void RemoveUser(User user)
        {
        }

        public void RemoveGroup(Group group)
        {
        }

        public bool IsUserInGroup(User user, Group group)
        {
            return false;
        }

        public User[] GetUsers()
        {
            return null;
        }

        public Group[] GetGroups()
        {
            return null;
        }

        public void AddUserToGroup(User user, Group group)
        {

        }

        private object addLocalDatabase(object args)
        {
            Database name = args as Database;
            PostgreSQLDBEngine.CreateDatabase(name.Name, PostgreSQLDBEngine.UtilityConnection);
            PostgreSQLDBEngine.SetDatabaseComment(name.Name, name.Description, PostgreSQLDBEngine.UtilityConnection);
            return name;
        }

        private void endLocalDatabaseAdd(IAsyncResult res)
        {
            SpinDelegate spin = (SpinDelegate)res.AsyncState;
            Database name = spin.EndInvoke(res) as Database;

            sendUpdate(new DBManagerEventArgs() { Action = ManagerAction.DBAdded, ClientTrigger = this.client, Args = name.Name + ":" + name.Description });            
        }

        public void AddDatabase(Database db)
        {
            lock (lockable)
            {
                if (dbs.Any(other => other.Name == db.Name)) return;
                dbs.Add(db);
            }

            SpinDelegate spin = new SpinDelegate(addLocalDatabase);
            spin.BeginInvoke(db, new AsyncCallback(endLocalDatabaseAdd), spin);
        }

        public void RemoveDatabase(Database db)
        {
        }

        public void GiveDatabasePermissionToUser(Database database, User user)
        {
        }

        public void GiveDatabasePermissionToGroup(Database database, Group group)
        {
        }

        public void RemoveDatabasePermissionToUser(Database database, User user)
        {
        }

        public void RemoveDatabasePermissionToGroup(Database database, Group group)
        {
        }

        public Database[] GetDatabases(User user)
        {
            Database[] toRet = null;
            lock (lockable)
            {
                toRet = dbs.ToArray();
            }

            return toRet;
        }

        public string RegisterClient(string client, User loginUser)
        {
            string toRet = "";
            if (client == null)
                return "Client is null";

            toRet += client;

            if (loginUser == null)
                return toRet + " User is null";

            bool added = false;
            this.thisHandler = new EventHandler<DBManagerEventArgs>(handlerCallback);
           

            lock (lockable)
            {
            
                
                this.user = users.SingleOrDefault(use => use.Name == loginUser.Name);
                if (this.user == null)
                    return "User does not exist";

                if (this.user.Password != loginUser.Password)
                {
                    this.user = null;
                    return "User password is not valid";
                }

                if (!knownClients.ContainsKey(client))
                {
                    this.client = client;
                    this.user = loginUser;
                    knownClients.Add(client, this.thisHandler);
                    added = true;
                }
            }

            if (!added)
            {
                this.user = null;
                return "This client already is registered.";
            }


            //string id = null;
            //if (clientToUid.ContainsKey(client))
            //{
            //    id = clientToUid[client];
            //}
            //else
            //{
            //    id = Guid.NewGuid().ToString();
            //    lock (lockable)
            //    {
            //        clientToUid.Add(client, id);
            //    }
            //}

            this.callback = OperationContext.Current.GetCallbackChannel<IDBCallBack>();

            UpdateEvent += this.thisHandler;

            return UpdateEvent.GetInvocationList().Length.ToString();
        }

        public void DeregisterClient(string client)
        {
            if (this.client == null) return;

             EventHandler<DBManagerEventArgs> handler = null;
            lock (lockable)
            {
                if (knownClients.ContainsKey(client))
                {
                    handler = knownClients[client];
                    knownClients.Remove(client);
                }
            }

            if(handler != null)
                UpdateEvent -= handler;
            this.client = null;
        }


        private object triggerUpdate(object args)
        {
            return args;   
        }

        private void endTriggerUpdate(IAsyncResult res)
        {
            SpinDelegate spin = (SpinDelegate)res.AsyncState;
            object[] passed = spin.EndInvoke(res) as object[];
            string client = passed[0] as string;
            Database db = passed[1] as Database;

            sendUpdate(new DBManagerEventArgs() { Action = ManagerAction.DBChanged, ClientTrigger = this.client, Args = db.Name });            

        }

        public void NeedToUpdate(string client, Database db)
        {
            SpinDelegate spin = new SpinDelegate(triggerUpdate);
            spin.BeginInvoke(new object[]{client, db}, new AsyncCallback(endTriggerUpdate), spin);
        }

        private void handlerCallback(object sender, DBManagerEventArgs e)
        {
            try
            {
                this.callback.Updated(e.ClientTrigger, e.Action.ToString(), e.Args);
            }
            catch
            {

            }
        }

        #endregion

    }
}
