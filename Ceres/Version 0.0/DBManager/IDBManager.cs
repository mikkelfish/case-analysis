﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DBManager
{
    // NOTE: If you change the interface name "IService1" here, you must also update the reference to "IService1" in App.config.
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IDBCallBack))]
    public interface IDBManager
    {
        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void AddUser(User user);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void AddGroup(Group group);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void RemoveUser(User user);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void RemoveGroup(Group group);

        [OperationContract(IsInitiating=false, IsTerminating=false)]
        bool IsUserInGroup(User user, Group group);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        User[] GetUsers();

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        Group[] GetGroups();

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void AddUserToGroup(User user, Group group);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void AddDatabase(Database database);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void RemoveDatabase(Database database);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void GiveDatabasePermissionToUser(Database database, User user);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void GiveDatabasePermissionToGroup(Database database, Group group);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void RemoveDatabasePermissionToUser(Database database, User user);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        void RemoveDatabasePermissionToGroup(Database database, Group group);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        Database[] GetDatabases(User user);

        [OperationContract(IsInitiating = true, IsTerminating = false)]
        string RegisterClient(string client, User user);

        [OperationContract(IsInitiating = false, IsTerminating = true)]
        void DeregisterClient(string client);

        [OperationContract(IsInitiating=false, IsTerminating=false)]
        void NeedToUpdate(string client, Database database);
    }

    public interface IDBCallBack
    {
        [OperationContract(IsOneWay=true)]
        void Updated(string client, string message, string args);
    }

    [DataContract]
    public class User
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Password { get; set; }
        
        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Institution { get; set; }

        [DataMember]
        public string Lab { get; set; }

        [DataMember]
        public bool SuperUser { get; set; }
    }

    [DataContract]
    public class Group
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool  SuperUser{ get; set; }

        [DataMember]
        public string Password { get; set; }
    }

    [DataContract]
    public class Database
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }
    }



}
