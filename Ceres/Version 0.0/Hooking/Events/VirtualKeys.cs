using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Win32
{
    public enum VirtualKeys
    {
        VK_LBUTTON = 0x01,
        VK_RBUTTON = 0x02,
        VK_CANCEL = 0x03,
        VK_MBUTTON = 0x04,
        VK_BACK = 0x08, 
        VK_TAB = 0x09,
        VK_CLEAR = 0x0C,
        VK_RETURN = 0x0D,
        VK_SHIFT = 0x10, 
        VK_CONTROL = 0x11, 
        VK_MENU = 0x12,
        VK_PAUSE = 0x13,
        VK_CAPITAL = 0x14,
        VK_ESCAPE = 0x1B,
        VK_SPACE = 0x20,
        VK_PRIOR = 0x21,
        VK_NEXT = 0x22,
        VK_END = 0x23,
        VK_HOME = 0x24,
        VK_LEFT = 0x25,
        VK_UP = 0x26,
        VK_RIGHT = 0x27,
        VK_DOWN = 0x28,
        VK_SELECT = 0x29,
        VK_PRINT = 0x2A,
        VK_EXECUTE = 0x2B,
        VK_SNAPSHOT = 0x2C, // 	PRINT SCREEN key
        VK_INSERT = 0x2D, //INS key
        VK_DELETE = 0x2E, //DEL key
        VK_HELP = 0x2F, //HELP key
        VK_NUMPAD0 = 0x60, 	//Numeric keypad 0 key
        VK_NUMPAD1 = 0x61, 	//Numeric keypad 1 key
        VK_NUMPAD2 = 0x62, 	//Numeric keypad 2 key
        VK_NUMPAD3 = 0x63, 	//Numeric keypad 3 key
        VK_NUMPAD4 = 0x64, 	//Numeric keypad 4 key
        VK_NUMPAD5 = 0x65, 	//Numeric keypad 5 key
        VK_NUMPAD6 = 0x66, 	//Numeric keypad 6 key
        VK_NUMPAD7 = 0x67, 	//Numeric keypad 7 key
        VK_NUMPAD8 = 0x68, 	//Numeric keypad 8 key
        VK_NUMPAD9 = 0x69, 	//Numeric keypad 9 key
        VK_SEPARATOR = 0x6C, //	Separator key
        VK_SUBTRACT = 0x6D, 	//Subtract key
        VK_DECIMAL = 0x6E, 	//Decimal key
        VK_DIVIDE = 0x6F, 	//Divide key
        VK_F1 = 0x70, 	//F1 key
        VK_F2 = 0x71, 	//F2 key
        VK_F3 = 0x72, 	//F3 key
        VK_F4 = 0x73, 	//F4 key
        VK_F5 = 0x74, 	//F5 key
        VK_F6 = 0x75, 	//F6 key
        VK_F7 = 0x76, 	//F7 key
        VK_F8 = 0x77, 	//F8 key
        VK_F9 = 0x78, 	//F9 key
        VK_F10 = 0x79, 	//F10 key
        VK_F11 = 0x7A,	//F11 key
        VK_F12 = 0x7B, 	//F12 key
        VK_F13 = 0x7C, 	//F13 key
        VK_F14 = 0x7D, 	//F14 key
        VK_F15 = 0x7E, 	//F15 key
        VK_F16 = 0x7F, 	//F16 key
        VK_NUMLOCK = 0x90, 	//NUM LOCK key
        VK_SCROLL = 0x91, 	//SCROLL LOCK key
        VK_LSHIFT = 0xA0, 	//Left SHIFT key
        VK_RSHIFT = 0xA1, 	//Right SHIFT key
        VK_LCONTROL = 0xA2, 	//Left CONTROL key
        VK_RCONTROL = 0xA3, 	//Right CONTROL key
        VK_LMENU = 0xA4, 	//Left MENU key
        VK_RMENU = 0xA5, 	//Right MENU key
        VK_PLAY  = 0xFA, 	//Play key
        VK_ZOOM  = 0xFB 	 //Zoom key

    //Same
    //30 0 key
    //31 	1 key
    //32 	2 key
    //33 	3 key
    //34 	4 key
    //35 	5 key
    //36 	6 key
    //37 	7 key
    //38 	8 key
    //39 	9 key
    //41 	A key
    //42 	B key
    //43 	C key
    //44 	D key
    //45 	E key
    //46 	F key
    //47 	G key
    //48 	H key
    //49 	I key
    //4A 	J key
    //4B 	K key
    //4C 	L key
    //4D 	M key
    //4E 	N key
    //4F 	O key
    //50 	P key
    //51 	Q key
    //52 	R key
    //53 	S key
    //54 	T key
    //55 	U key
    //56 	V key
    //57 	W key
    //58 	X key
    //59 	Y key
    //5A 	Z key
    }
}
