using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Microsoft.Win32
{
    public static class Imports
    {
        [DllImport("user32.dll")]
        private static extern short GetKeyState(int nVirtKey);
        public static bool IsKeyDown(Keys key)
        {
            short state = 0;

            if (key == Keys.Alt || key == Keys.Control || key == Keys.ControlKey)
            {
                state = GetKeyState((int)VirtualKeys.VK_MENU);
                state = GetKeyState((int)VirtualKeys.VK_CONTROL);
            }
            else
            {
                state = GetKeyState((int)key);
            }

            return ((state & 0x10000) == 0x10000);
        }
    }
}
