using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.MathConstructs
{
    //http://www.ms.uky.edu/~mai/RandomNumber
    //#define znew  ((z=36969*(z&65535)+(z>>16))<<16)
    //#define wnew  ((w=18000*(w&65535)+(w>>16))&65535)
    //#define IUNI  (znew+wnew)
    //#define UNI   (znew+wnew)*2.328306e-10
    //static unsigned long z=362436069, w=521288629;
    //void setseed(unsigned long i1,unsigned long i2){z=i1; w=i2;}
    /* Use of IUNI in an expression will produce a 32-bit unsigned
        random integer, while UNI will produce a random real in [0,1).
        The static variables z and w can be reassigned to i1 and i2
        by setseed(i1,i2);
       You may replace the two constants 36969 and 18000 by any
       pair of distinct constants from this list:
       18000 18030 18273 18513 18879 19074 19098 19164 19215 19584
       19599 19950 20088 20508 20544 20664 20814 20970 21153 21243
       21423 21723 21954 22125 22188 22293 22860 22938 22965 22974
       23109 23124 23163 23208 23508 23520 23553 23658 23865 24114
       24219 24660 24699 24864 24948 25023 25308 25443 26004 26088
       26154 26550 26679 26838 27183 27258 27753 27795 27810 27834
       27960 28320 28380 28689 28710 28794 28854 28959 28980 29013
       29379 29889 30135 30345 30459 30714 30903 30963 31059 31083
       (or any other 16-bit constants k for which both k*2^16-1
       and k*2^15-1 are prime)*/

    public class RealRandom : Random
    {
        private uint[] seeds = new uint[]{
            18000,18030,18273,18513,18879,19074,19098,19164,19215,19584,
            19599,19950,20088,20508,20544,20664,20814,20970,21153,21243,
            21423,21723,21954,22125,22188,22293,22860,22938,22965,22974,
            23109,23124,23163,23208,23508,23520,23553,23658,23865,24114,
            24219,24660,24699,24864,24948,25023,25308,25443,26004,26088,
            26154,26550,26679,26838,27183,27258,27753,27795,27810,27834,
            27960,28320,28380,28689,28710,28794,28854,28959,28980,29013,
            29379,29889,30135,30345,30459,30714,30903,30963,31059,31083};

        private uint z = 362436069;
        private uint w = 521288629;
        private uint seed1;
        private uint seed2;

        public RealRandom()
        {
            Random rand = new Random();
            int in1 = rand.Next(0, seeds.Length-1);
            int in2 = rand.Next(0, seeds.Length - 1);
            while (in2 == in1)
            {
                in2 = rand.Next(0, seeds.Length - 1);
            }
            seed1 = seeds[in1];
            seed2 = seeds[in2];
        }

        //#define znew  ((z=36969*(z&65535)+(z>>16))<<16)
        //#define wnew  ((w=18000*(w&65535)+(w>>16))&65535)
        //#define IUNI  (znew+wnew)
        //#define UNI   (znew+wnew)*2.328306e-10
        //static unsigned long z=362436069, w=521288629;
        protected override double Sample()
        {
            z = (seed1 * (z & 65535) + (z >> 16));
            w = (seed2 * (w & 65535) + (w >> 16));
            uint znew = z << 16;
            uint wnew = w & 65535;
            return (znew + wnew) * 2.38306e-10;
        }
    }
}
