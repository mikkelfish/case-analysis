using System;
using System.Collections.Generic;
using System.Text;
using Lambda.Generic.Arithmetic;

namespace CeresBase.MathConstructs
{
    public class Utilities
    {
        //FROM VIS5D
        private const double SEARCH_FUZZ = 1e-05;
        public static double BinarySearch<T>(T searchFor, T[] array)
        {
            if (array.Length == 0) return -1.0;
            int low, high, mid;
            double x;
            int size = array.Length;

            IMath<T> calc = CeresBase.General.Utilities.GetCalculator<T>();
            T tFuzz = calc.ConvertFrom(SEARCH_FUZZ);

            /* MJK 12.15.98 begin */
            //searchFor < (array[0] - SEARCH_FUZZ)
            if (calc.Compare(searchFor, calc.Subtract(array[0], tFuzz)) < 0)
            {
                return -1.0;
            }
            else if (size == 1)
            {
                return 0.0;
            }
            //searchFor > array[size-1] + tFuzz
            else if (calc.Compare(searchFor, calc.Add(array[size - 1], tFuzz)) > 0)
            {
                /* return something larger than normal */
                /* so that is count's it as missing when needed */
                return (double)(size + 1);
            }
            else
            {
                /* do a binary search of array[] for value */
                low = 0;
                high = size - 1;

                while (low <= high)
                {
                    mid = (low + high) / 2;
                    //searchFor < array[mid]
                    if (calc.Compare(searchFor, array[mid]) < 0)
                        high = mid - 1;
                    //searchFor > array[mid]
                    else if (calc.Compare(searchFor, array[mid]) > 0)
                        low = mid + 1;
                    else
                        return (double)mid;  /* TODO: check this */
                }

                if (low > high)
                {
                    int temp = low;
                    low = high;
                    high = temp;
                }

                /* interpolate a value between high and low */
                //(searchFor - array[high])/(array[low] - array[high]);
                double numerator = System.Convert.ToDouble(calc.Subtract(array[high], searchFor));



                double denominator = System.Convert.ToDouble(calc.Subtract(array[high], array[low]));
                x = numerator / denominator;
                return (1.0 - x) + low;
            }
        }
    }
}
