using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CeresBase.MathConstructs
{
    public static class Stats
    {


        public static float Average(float[] data)
        {
            float sum = 0;
            for (int i = 0; i < data.Length; i++)
            {
                sum += data[i];
            }
            return sum / data.Length;
        }

        public static float StandardDev(float[] data)
        {
            float avg = Average(data);
            float sd = 0;
            for (int i = 0; i < data.Length; i++)
            {
                sd += (data[i] - avg) * (data[i] - avg);
            }

            sd = sd / (data.Length - 1);
            return (float)Math.Sqrt(sd);
        }

        public static double Average(double[] data)
        {
            double sum = 0;
            for (int i = 0; i < data.Length; i++)
            {
                sum += data[i];
            }
            return sum / data.Length;
        }

        public static double StandardDev(double[] data)
        {
            if (data.Length == 1) return 0;
            double avg = Average(data);
            double sd = 0;
            for (int i = 0; i < data.Length; i++)
            {
                sd += (data[i] - avg) * (data[i] - avg);
            }

            sd = sd / (data.Length - 1);
            return Math.Sqrt(sd);
        }

        public static double Entropy(double[] data, double logBase)
        {
            double sum = data.Sum();
            double[] normalizedData = data.Select(val => val / sum).ToArray();
            double result = -normalizedData.Aggregate((entropy, val) => entropy + (val == 0 ? 0 : val * Math.Log(val, logBase)));
            return result;
        }

        public static float Entropy(float[] data, double logBase)
        {
            float sum = data.Sum();
            float[] normalizedData = data.Select(val => val / sum).ToArray();
            float result = -normalizedData.Aggregate((entropy, val) => entropy +  (val == 0 ? 0 : val * (float)Math.Log(val, logBase)));
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="successes"></param>
        /// <param name="total"></param>
        /// <returns>Probability from 0->successes</returns>
        public static double BinomialTest(double p, int successes, int total, bool twoway)
        {
            //f(k; n,p) = (n!/(k!(n-k)!)) p^k * (1-p)^(n-k)
            double expected = (double)(p * total);

            BigInteger nbang = new BigInteger(1);
            for (int i = 2; i <= total; i++)
            {
                nbang *= i;
            }

            double sum = 0;
           
            double runningP = 1;
            double runningNKP = Math.Pow((1.0 - p), total);
            BigInteger nkbang = new BigInteger(nbang);
            BigInteger kbang = new BigInteger(1);
            for (int k = 0; k <= successes; k++)
            {
                int numDigits = (int)(-Math.Floor(Math.Log10(runningP * runningNKP))  + 5);
                double toUse = runningP * runningNKP;
                double toMult = Math.Pow(10, numDigits);
                long scale = (long)(toUse * toMult);
                BigInteger mult = new BigInteger((long)(scale));
                BigInteger num = nbang;
                BigInteger div = num / (kbang * nkbang);

                BigInteger tot = div * new BigInteger(scale);

                BigInteger bigDen = new BigInteger(1);

                int divDigits = numDigits - 10 > 1 ? numDigits - 10 : 1;
                for (int j = 0; j < divDigits; j++)
                {
                    bigDen *= 10;
                }

                tot /= bigDen;

                double cont = (double)tot.LongValue()/Math.Pow(10, numDigits - divDigits);

                if (total != k)
                {
                    sum += cont;
                    runningP *= p;
                    runningNKP /= (1.0 - p);
                    nkbang /= (total - k);
                    kbang *= (k + 1);
                }
            }

            if (twoway)
            {
                if (successes < expected)
                {
                    return sum;
                }

                return 1.0 - sum;
            }

            return sum;
        }
    }
}
