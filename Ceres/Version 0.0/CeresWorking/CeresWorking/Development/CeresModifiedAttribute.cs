using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.Development
{
    /// <summary>
    /// Apply when you make a modification to an existing item.
    /// </summary>
     [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public class CeresModifiedAttribute : CeresDevelopmentAttribute
    {
        #region Private Variables
        private CeresDocumentedStage documented = CeresDocumentedStage.None;
        #endregion

        #region Private Functions

        #endregion

        #region Properties
        /// <summary>
        /// What stage has this been documented
        /// </summary>
        public CeresDocumentedStage DocumentedStatus
        {
            get
            {
                return this.documented;
            }
            set
            {
                this.documented = value;
            }
        }
        #endregion

        #region Constructors
        public CeresModifiedAttribute(string author, string date)
            : base(author, date)
        {

        }
        #endregion

        #region Public Functions

        #endregion
    }
}
