using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.Development
{
    public enum CeresDocumentedStage { None, Complete, VisStudioOnly, FlowChartOnly, Accepted };
   
    /// <summary>
    /// Apply when you create an item
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public class CeresCreatedAttribute : CeresDevelopmentAttribute
    {
        #region Private Variables
        private CeresDocumentedStage documented = CeresDocumentedStage.None;
        #endregion

        #region Private Functions

        #endregion

        #region Properties
        /// <summary>
        /// What stage has this been documented
        /// </summary>
        public CeresDocumentedStage DocumentedStatus
        {
            get
            {
                return this.documented;
            }
            set
            {
                this.documented = value;
            }
        }
        #endregion

        #region Constructors
        public CeresCreatedAttribute(string author, string date)
            : base(author, date)
        {

        }
        #endregion

        #region Public Functions

        #endregion
    }
}
