using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.Development
{
    /// <summary>
    /// The base for all Ceres Development Attributes. These attributes will help the automated
    /// management system keep track of changes (once it's developed....which will be a while).
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
    public abstract class CeresDevelopmentAttribute : Attribute
    {
        #region Private Variables
        private string author;
        private string time;
        private string version;
        private string comments;
        #endregion

        #region Private Functions

        #endregion

        #region Properties
        /// <summary>
        /// The author of this attribute
        /// </summary>
        public string Author { get { return this.author; } }
        /// <summary>
        /// The date the attribute was created
        /// </summary>
        public string Date { get { return this.time; } }
        /// <summary>
        /// The version the attribute was created
        /// </summary>
        public string Version 
        { 
            get 
            { 
                return this.version; 
            }
            set
            {
                this.version = value;
            }
        }
        /// <summary>
        /// Miscellaneous comments
        /// </summary>
        public string Comments 
        { 
            get 
            { 
                return this.comments; 
            }
            set
            {
                this.comments = value;
            }
        }
        #endregion

        #region Constructors
        public CeresDevelopmentAttribute(string author, string date)
        {
            this.author = author;
            this.time = date;
        }
        #endregion

        #region Public Functions

        #endregion
    }
}
