//using System;
//using System.Collections.Generic;
//using System.Text;
//using Razor.Attributes;
//using Razor.SnapIns;

//namespace CeresBase
//{
//    [SnapInTitle("Ceres Scientific Visualization Core")]
//    [SnapInDescription("Generic and customizable visualization framework.")]
//    [SnapInCompany("Mesosoft")]
//    [SnapInDevelopers("Mikkel Fishman")]
//    [SnapInVersion("1.0.0.0")]
//    public class CeresCorePlugin : Plugins.CeresPluginBase
//    {
//        public static CeresCorePlugin Instance { get { return General.SingletonProvider<CeresCorePlugin>.Instance; } }

//        private Plugins.CeresApplication currentApp;
//        public Plugins.CeresApplication CurrentApplication
//        {
//            get { return currentApp; }
//        }
	

//        public CeresCorePlugin()
//        {
//            SnapInHostingEngine.Instance.AfterSnapInsStarted += new System.ComponentModel.CancelEventHandler(Instance_AfterSnapInsStarted);
//        }

//        void Instance_AfterSnapInsStarted(object sender, System.ComponentModel.CancelEventArgs e)
//        {
//            this.StartMyServices();

//            Plugins.CeresApplication[] apps = CeresBase.General.Utilities.CreatePlugins<Plugins.CeresApplication>();
//            Instance.currentApp = apps[0];
//            apps[0].RunApplication();
//            //e.Cancel = true;
//        }
//    }
//}
