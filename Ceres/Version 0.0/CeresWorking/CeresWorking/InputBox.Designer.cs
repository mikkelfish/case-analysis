namespace CeresBase
{
    partial class InputBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tInput = new System.Windows.Forms.TextBox();
            this.labelInputBox = new System.Windows.Forms.Label();
            this.bOK = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.labelTextArea = new System.Windows.Forms.Label();
            this.TextArea = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tInput
            // 
            this.tInput.Location = new System.Drawing.Point(6, 20);
            this.tInput.Name = "tInput";
            this.tInput.Size = new System.Drawing.Size(125, 20);
            this.tInput.TabIndex = 1;
            this.tInput.TextChanged += new System.EventHandler(this.tInput_TextChanged);
            this.tInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tInput_KeyDown);
            // 
            // labelInputBox
            // 
            this.labelInputBox.AutoSize = true;
            this.labelInputBox.Location = new System.Drawing.Point(3, 4);
            this.labelInputBox.Name = "labelInputBox";
            this.labelInputBox.Size = new System.Drawing.Size(127, 13);
            this.labelInputBox.TabIndex = 4;
            this.labelInputBox.Text = "Input name for new entry.";
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.Location = new System.Drawing.Point(77, 206);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(52, 19);
            this.bOK.TabIndex = 4;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(12, 206);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(52, 19);
            this.bCancel.TabIndex = 3;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // labelTextArea
            // 
            this.labelTextArea.AutoSize = true;
            this.labelTextArea.Location = new System.Drawing.Point(3, 43);
            this.labelTextArea.Name = "labelTextArea";
            this.labelTextArea.Size = new System.Drawing.Size(127, 13);
            this.labelTextArea.TabIndex = 10;
            this.labelTextArea.Text = "Input name for new entry.";
            // 
            // TextArea
            // 
            this.TextArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TextArea.Location = new System.Drawing.Point(6, 59);
            this.TextArea.Multiline = true;
            this.TextArea.Name = "TextArea";
            this.TextArea.Size = new System.Drawing.Size(145, 141);
            this.TextArea.TabIndex = 2;
            // 
            // InputBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCancel;
            this.ClientSize = new System.Drawing.Size(175, 244);
            this.ControlBox = false;
            this.Controls.Add(this.TextArea);
            this.Controls.Add(this.labelTextArea);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.tInput);
            this.Controls.Add(this.labelInputBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InputBox";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Title";
            this.Load += new System.EventHandler(this.InputBox_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tInput;
        private System.Windows.Forms.Label labelInputBox;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label labelTextArea;
        private System.Windows.Forms.TextBox TextArea;
    }
}