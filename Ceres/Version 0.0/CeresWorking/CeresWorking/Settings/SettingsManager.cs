using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using CeresBase.Development;

namespace CeresBase.Settings
{
    /// <summary>
    /// This class reads from the settings file or is plugged into a statistical engine to figure out the default
    /// values for all fields marked with the CeresAutoAssignAttribute in an object.
    /// </summary>
    [CeresCreated("Mikkel", "03/14/06")]
    public static class SettingsManager
    {
        #region Private Variables
        private static Dictionary<Type, object> currentSettings;
        #endregion

        #region Private Functions
        private static object getDefaultInterface(Type t, object[] args)
        {
            if (!SettingsManager.currentSettings.ContainsKey(t))
            {
                MethodInfo info = typeof(SettingsManager).GetMethod("GetDefaultInterface");
                object def = MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod(info, new Type[] { t }, null, args);
                if (def == null) return null;
            }
            return SettingsManager.currentSettings[t];
        }
        #endregion

        static SettingsManager()
        {
            SettingsManager.currentSettings = new Dictionary<Type, object>();
        }

        #region Public Functions
        /// <summary>
        /// Get the default interface for the given type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public static T GetDefaultInterface<T>() where T : class
        {
            //TODO right now just pick the first one. later read the settings
            if (!SettingsManager.currentSettings.ContainsKey(typeof(T)))
            {
                T[] interfaces = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<T>(null);
                if (interfaces == null) return null;
                SettingsManager.currentSettings.Add(typeof(T), interfaces[0]);
            }

            return (T)SettingsManager.currentSettings[typeof(T)];
        }

        /// <summary>
        /// Set all the defaults for a given object. It looks for all the fields with
        /// the CeresAutoAssignAttribute assigned.
        /// </summary>
        /// <param name="obj"></param>
        [CeresCreated("Mikkel", "03/14/06")]
        public static void SetDefaults(object obj)
        {
            CeresAutoAssignAttribute[] attrs;
            FieldInfo[] fields = MesosoftCommon.Utilities.Reflection.Common.GetFieldsWithAttribute<CeresAutoAssignAttribute>(obj,
                out attrs);
            if (fields == null || attrs == null || fields.Length == 0 || attrs.Length == 0)
                return;
            for (int i = 0; i < fields.Length; i++)
            {
                FieldInfo info = fields[i];
                object def = SettingsManager.getDefaultInterface(info.FieldType, null);
                if (def == null) continue;
                info.SetValue(obj, def);
            }
        }

        #endregion
    }
}
