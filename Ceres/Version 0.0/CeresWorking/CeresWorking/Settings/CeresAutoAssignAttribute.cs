using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;

namespace CeresBase.Settings
{
    /// <summary>
    /// Attribute marks fields that should be automatically assigned through the settings manager
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    [CeresCreated("Mikkel", "03/14/06")]
    public sealed class CeresAutoAssignAttribute : Attribute
    {
        private bool saveValue;
        
        /// <summary>
        /// Gets or sets whether the settings engine should save the changes made
        /// </summary>
        public bool SaveValue
        {
            get
            {
                return this.saveValue;
            }
            set
            {
                this.saveValue = true;
            }
        }
    }

}
