//using System;
//using System.Collections.Generic;
//using System.Text;
//using Razor.Configuration;
//using Razor.SnapIns;

//namespace CeresBase.Settings
//{
//    [global::System.AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
//    public class XMLSettingAttribute : Attribute
//    {
//        private static Dictionary<string, XmlConfiguration> configurations = new Dictionary<string, XmlConfiguration>();

//        private static int count = 0;
//        private static object lockable = new object();

//        private static XmlConfiguration GetConfiguration(string configName)
//        {
//            if (!configurations.ContainsKey(configName)) return null;
//            return configurations[configName];
//        }

//        static XMLSettingAttribute()
//        {
//            System.Windows.Forms.Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
//        }

//        static void Application_ApplicationExit(object sender, EventArgs e)
//        {
//            foreach (string cname in configurations.Keys)
//            {
//                XmlConfiguration config = configurations[cname];
//                if (config.Fullpath == SnapInHostingEngine.Instance.LocalUserConfiguration.Fullpath ||
//                    config.Fullpath == SnapInHostingEngine.Instance.CommonConfiguration.Fullpath)
//                {
//                    continue;
//                }

//                ConfigurationEngine.WriteConfiguration(null, config, config.Path);
//            }
//        }

//        //~XMLSettingAttribute()
//        //{
//        //    lock (lockable)
//        //    {
//        //        count--;
//        //        if (count == 0)
//        //        {
//        //            foreach (string cname in configurations.Keys)
//        //            {
//        //                XmlConfiguration config = configurations[cname];
//        //                if (config.Fullpath == SnapInHostingEngine.Instance.LocalUserConfiguration.Fullpath ||
//        //                    config.Fullpath == SnapInHostingEngine.Instance.CommonConfiguration.Fullpath)
//        //                {
//        //                    continue;
//        //                }

//        //                ConfigurationEngine.WriteConfiguration(null, config, config.Path);
//        //            }
//        //        }
//        //    }
//        //}

//        public void WriteToDisk()
//        {
//            ConfigurationEngine.WriteConfiguration(null, this.configuration, this.configuration.Path);
//        }

//        public const string UserConfiguration = "UserConfiguration";
//        public const string SharedConfiguration = "SharedConfiguration";

//        private XmlConfiguration configuration;
//        //public XmlConfiguration Configuration
//        //{
//        //    get { return configuration; }
//        //}

//        private string category;
//        public XmlConfigurationCategory Category
//        {
//            get 
//            {
//                if (this.configuration == null) return null;
//                if(this.configuration.Categories.Contains(category))
//                    return this.configuration.Categories[category];
//                return null;
//            }
//        }

//        private bool promptIfNotThere;
//        public bool PromptForInput
//        {
//            get { return promptIfNotThere; }
//        }
	
	

//        public XMLSettingAttribute(string config, string category, bool isCommon, bool promptIfNotThere)
//        {
//            this.category = category;
//            this.promptIfNotThere = promptIfNotThere;

//            if (config == XMLSettingAttribute.SharedConfiguration)
//            {
//                this.configuration = SnapInHostingEngine.Instance.CommonConfiguration;
//            }
//            else if (config == XMLSettingAttribute.UserConfiguration)
//            {
//                this.configuration = SnapInHostingEngine.Instance.LocalUserConfiguration;
//            }
//            else
//            {

//                this.configuration = XMLSettingAttribute.GetConfiguration(config);
//                if (this.configuration == null)
//                {
//                    string path = (isCommon ? SnapInHostingEngine.CommonDataPath : SnapInHostingEngine.LocalUserDataPath)
//                        + "\\" + config + ".xml";
//                    ConfigurationEngine.ReadOrCreateConfiguration(false, config,
//                         path, out this.configuration, null, new XmlConfigurationEventHandler(this.configurationHandler));
//                }
//            }

//            XmlConfigurationCategory cat = this.configuration.Categories[this.category, true];

//            lock (lockable)
//            {
//                if (!configurations.ContainsKey(config))
//                {
//                    configurations.Add(config, this.configuration);
//                }
//                count++;
//            }
//        }

//        private void configurationHandler(object sender, XmlConfigurationEventArgs e)
//        {

//        }

//    }
//}
