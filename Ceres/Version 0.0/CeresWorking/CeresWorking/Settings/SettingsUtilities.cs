//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Reflection;


//namespace CeresBase.Settings
//{
//    public static class SettingsUtilities
//    {
//        //private static Dictionary<Type, XMLSettingAttribute> settingAttrs = new Dictionary<Type, XMLSettingAttribute>();


//        private const string methodSeparator = "METHOD:";
//        private const string paramSeparator = "PARAM:";
//        private const string keySeparator = "KEY:";
//        private const string valSeparator = "VALUE:";

//        //public static XMLSettingAttribute GetXMLSettingAttr(Type type)
//        //{
//        //    if (SettingsUtilities.settingAttrs.ContainsKey(type))
//        //        return SettingsUtilities.settingAttrs[type];
//        //    XMLSettingAttribute attr = CeresBase.General.Utilities.GetAttribute<XMLSettingAttribute>(type, true);
//        //    settingAttrs.Add(type, attr);
//        //    return attr;
//        //}

//        public static string DictionaryToString<TKey, TVal>(Dictionary<TKey, TVal> dictionary)
//        {
//            string toRet = "";
//            foreach (TKey key in dictionary.Keys)
//            {
//                string str =  "";
//                Razor.Configuration.EncodingEngine.Base64Encode(key, typeof(TKey), out str);
//                toRet += SettingsUtilities.keySeparator + str;

//                Razor.Configuration.EncodingEngine.Base64Encode(dictionary[key], typeof(TVal), out str);
//                toRet += SettingsUtilities.valSeparator + str;

//                //toRet += SettingsUtilities.keySeparator + key.ToString();
//               // toRet += SettingsUtilities.valSeparator + dictionary[key].ToString();
//            }
//            return toRet;
//        }

//        public static Dictionary<TKey, TVal> StringToDictionary<TKey, TVal>(string str)
//        {            
//            string[] keySplits = str.Split(new string[] { SettingsUtilities.keySeparator }, StringSplitOptions.RemoveEmptyEntries);
//            Dictionary<TKey, TVal> toRet = new Dictionary<TKey, TVal>();
//            foreach (string key in keySplits)
//            {
//                int valIndex = key.IndexOf(SettingsUtilities.valSeparator);

//                object okey;
//                Razor.Configuration.EncodingEngine.Base64Decode(key.Substring(0, valIndex),
//                    out okey);

//                object oval;
//                Razor.Configuration.EncodingEngine.Base64Decode(key.Substring(valIndex + SettingsUtilities.valSeparator.Length),
//                    out oval);
//                toRet.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, toRet,
//                    new object[] { okey, oval });
//            }
//            return toRet;
//        }

//        public static string MethodToString(MethodInfo info)
//        {
//            return info.ReflectedType.AssemblyQualifiedName +
//                    SettingsUtilities.methodSeparator + info.Name;
//        }

//        public static MethodInfo StringToMethod(string value)
//        {
//            int index = value.IndexOf(SettingsUtilities.methodSeparator);
//            if (index < 0) return null;
//            Type type = Type.GetType(value.Substring(0, index));
//            return type.GetMethod(value.Substring(index + SettingsUtilities.methodSeparator.Length));

//        }

//        public static string ConstructorInfoToString(ConstructorInfo info)
//        {
//            if (info == null) return "";
//            string fullConsstring = info.ReflectedType.AssemblyQualifiedName;
//            ParameterInfo[] infos = info.GetParameters();
//            for (int i = 0; i < infos.Length; i++)
//            {
//                fullConsstring += SettingsUtilities.paramSeparator + infos[i].ParameterType.AssemblyQualifiedName;
//            }
//            return fullConsstring;
//        }

//        public static ConstructorInfo StringToConstructorInfo(string consString)
//        {
//            if (consString == "") return null;
//            int index = consString.IndexOf(SettingsUtilities.paramSeparator);
//            List<Type> types = new List<Type>();
//            string[] splits = consString.Split(new string[] { SettingsUtilities.paramSeparator }, StringSplitOptions.RemoveEmptyEntries);
//            foreach (string split in splits)
//            {
//                Type t = Type.GetType(split);
//                types.Add(t);
//            }


//            Type type = types[0];
//            types.RemoveAt(0);
//            return type.GetConstructor(types.ToArray());
//        }

//        public static string ListToString(List<string> list)
//        {
//            string full = "";
//            foreach (string str in list)
//            {
//                full += SettingsUtilities.valSeparator + str;
//            }
//            return full;
//        }

//        public static List<string> StringToList(string listStr)
//        {
//            List<string> list = new List<string>();
//            string[] splits = listStr.Split(new string[] { SettingsUtilities.valSeparator }, StringSplitOptions.RemoveEmptyEntries);
//            foreach (string split in splits)
//            {
//                list.Add(split);
//            }
//            return list;
//        }

//    }
//}
