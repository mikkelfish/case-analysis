using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace CeresBase.UI
{
    class RequestInformationItem<TTag>
    {
        private string name;

        private TTag tag;
        public TTag Tag
        {
            get { return tag; }
            set 
            {
                tag = value;
                this.link = null;
                this.name = null;
            }
        }

        private bool readOnly;
        public bool ReadOnly
        {
            get { return readOnly; }
            set
            {
                this.readOnly = value;
            }
        }
	

        private object link;
        public object Link
        {
            get { return link; }
            set 
            {
                link = value;
                this.name = (string)link.GetType().InvokeMember("Name", BindingFlags.GetProperty, null, link, null);
            }
        }

        public object Output
        {
            get
            {
                if (IsLinked) return link;
                return tag;
            }
        }

        public bool IsLinked
        {
            get
            {
                return link != null;
            }
        }

        public override string ToString()
        {
            if(!IsLinked) return tag.ToString();
            return this.name;
        }
    }
}
