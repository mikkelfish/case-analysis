﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hardcodet.Wpf.GenericTreeView;
using System.IO;
using System.Windows;
using System.Windows.Data;

namespace CeresBase.UI.Navigation
{
    public class FileSystemTree : TreeViewBase<FileSystemItem>
    {
        public FileSystemTree()
        {
            this.SelectedItemChanged += new RoutedTreeItemEventHandler<FileSystemItem>(FileSystemTree_SelectedItemChanged);
        }

        void FileSystemTree_SelectedItemChanged(object sender, RoutedTreeItemEventArgs<FileSystemItem> e)
        {
           
           
        }

        public bool DisplayNodes
        {
            get { return (bool)GetValue(DisplayNodesProperty); }
            set { SetValue(DisplayNodesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DisplayNodes.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayNodesProperty =
            DependencyProperty.Register("DisplayNodes", typeof(bool), typeof(FileSystemTree), new UIPropertyMetadata(false));




        public override ICollection<FileSystemItem> GetChildItems(FileSystemItem parent)
        {
            return parent.Children;
        }

        public override string GetItemKey(FileSystemItem item)
        {
            return item.InternalPath;
        }

        public override FileSystemItem GetParentItem(FileSystemItem item)
        {
            return item.Parent;
        }
    }
}
