﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;

namespace CeresBase.UI.Navigation
{
    public class DirectoryItem : FileSystemItem
    {

        public string fullPath;

        public DirectoryItem(string name, DirectoryItem parent) : base(name, parent, parent == null ? null : parent.Tree)
        {
            this.fullPath = parent.ExternalPath + "\\" + name;
        }

        public DirectoryItem(string fullPath, FileSystemTree tree)
            : base((fullPath.Substring(fullPath.LastIndexOf('\\') + 1) == String.Empty) ? fullPath : fullPath.Substring(fullPath.LastIndexOf('\\') + 1), tree)
        {
            this.fullPath = fullPath;

            
        }

        public void RefreshChildren()
        {
            base.Children.Clear();
            try
            {
                string[] children = Directory.GetDirectories(this.ExternalPath);
                foreach (string child in children)
                {
                    DirectoryItem item = new DirectoryItem(child.Substring(child.LastIndexOf('\\') + 1), this);
                    base.Children.Add(item);
                }


                if (this.Tree != null)
                {
                    if (this.Tree.DisplayNodes)
                    {
                        string[] files = Directory.GetFiles(this.ExternalPath);
                        foreach (string file in files)
                        {
                            FileItem item = new FileItem(file.Substring(file.LastIndexOf('\\') + 1), this);
                            base.Children.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        public System.Collections.ObjectModel.ObservableCollection<FileSystemItem> RawChildren
        {
            get
            {
                return base.Children;
            }
        }

        public override System.Collections.ObjectModel.ObservableCollection<FileSystemItem> Children
        {
            get
            {
                if (base.Children.Count == 0)
                {
                    this.RefreshChildren();
                }

                return base.Children;
            }
        }

        protected override bool internalValidation()
        {
            return Directory.Exists(this.ExternalPath);
        }

        public override string ExternalPath
        {
            get
            {
                return this.fullPath;
            }
        }

        private bool autoLoad;
        public bool AutoLoad 
        {
            get
            {
                return this.autoLoad;
            }
            set
            {
                this.autoLoad = value;
                base.OnPropertyChanged("AutoLoad");
            }
        }

 

        public override string ToString()
        {
            return this.Name;
        }

       
    }
}
