﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using CeresBase.General;
using System.IO;

namespace CeresBase.UI.Navigation
{
    public class FileItem : FileSystemItem
    {
        private static readonly ObservableCollection<FileSystemItem> Empty;

        private string fullPath;

        static FileItem()
        {
            FileItem.Empty = new ObservableCollection<FileSystemItem>();
        }

        public FileItem(string name, DirectoryItem parent)
            : base(name, parent, parent == null ? null : parent.Tree)
        {
            if (parent != null)
            {
                this.fullPath = parent.ExternalPath + "\\" + name;
            }
        }

        public override string ExternalPath
        {
            get
            {
                return this.fullPath;
            }
        }

        public override System.Collections.ObjectModel.ObservableCollection<FileSystemItem> Children
        {
            get
            {
                return FileItem.Empty;
            }
        }

        protected override bool internalValidation()
        {
            return File.Exists(this.ExternalPath);
        }

        public object Tag { get; set; }
    }
}
