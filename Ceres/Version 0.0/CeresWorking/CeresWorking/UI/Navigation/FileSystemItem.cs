﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace CeresBase.UI.Navigation
{
    public class FileSystemItem : INotifyPropertyChanged
    {
        private readonly ObservableCollection<FileSystemItem> children = new ObservableCollection<FileSystemItem>();

        public bool Valid { get; private set; }

        public FileSystemTree Tree { get; private set; }

        protected virtual bool internalValidation()
        {
            return true;
        }

        public void Validate()
        {
            this.Valid = this.internalValidation();
            this.OnPropertyChanged("Valid");
        }

        public FileSystemItem(string name, FileSystemTree tree)
        {
            this.Name = name;
            this.Tree = tree;
        }

        public FileSystemItem(string name, FileSystemItem parent, FileSystemTree tree)
        {
            this.Name = name;
            this.Parent = parent;
            this.Tree = tree;
        }


        public string Name { get; protected set; }

        //TODO figure out path
        public string InternalPath 
        {
            get
            {
                string begin = this.Name;
                FileSystemItem parent = this.Parent;
                while (parent != null)
                {
                    begin = this.Parent.Name + "/" + begin;
                    parent = parent.Parent;
                }
                return begin;
            }
        }


        public virtual string ExternalPath { get; private set; }

        public virtual FileSystemItem Parent { get; private set; }

        public virtual ObservableCollection<FileSystemItem> Children 
        {
            get
            {
                return this.children;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
