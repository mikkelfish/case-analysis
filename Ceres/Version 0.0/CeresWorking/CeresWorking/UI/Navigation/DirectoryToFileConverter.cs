﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.IO;

namespace CeresBase.UI.Navigation
{
    [ValueConversion(typeof(DirectoryItem), typeof(IEnumerable<FileSystemItem>))]
    public class DirectoryToFileConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value == null || !(value is DirectoryItem)) return null;

                DirectoryItem item = value as DirectoryItem;
                if (!Directory.Exists(item.ExternalPath))
                    return null;

                string[] files = Directory.GetFiles(item.ExternalPath);
                List<FileItem> toRet = new List<FileItem>();
                foreach (string file in files)
                {
                    toRet.Add(new FileItem(file.Substring(file.LastIndexOf('\\') + 1), item));
                }

                return toRet;
            }
            catch
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
