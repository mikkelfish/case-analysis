﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Collections.ObjectModel;
//using CeresBase.Projects.Flowable;

//namespace CeresBase.UI
//{
//    public class GraphableOutputCollection : IBatchWritable, IBatchViewable
//    {
//        private ObservableCollection<GraphableOutput> graphs = new ObservableCollection<GraphableOutput>();
//        public ObservableCollection<GraphableOutput> Graphs { get { return this.graphs; } }

//        #region IBatchViewable Members

//        public IBatchViewable[] View(IBatchViewable[] viewable, bool saveToManager)
//        {
//            IBatchViewable[] notCol = viewable.Where(w => !(w is GraphableOutputCollection)).ToArray();
//            if (viewable.Any(w => w is GraphableOutput))
//            {
//                GraphableOutputCollection[] collections = viewable.Where(w => w is GraphableOutputCollection).Cast<GraphableOutputCollection>().ToArray();

//                List<IBatchViewable> toret = new List<IBatchViewable>(notCol);
//                foreach (GraphableOutputCollection col in collections)
//                {
//                    toret.AddRange(col.Graphs.Cast<IBatchViewable>());
//                }
//                return toret.ToArray();
//            }

//            GraphableOutputCollection[] vcollections = viewable.Where(w => w is GraphableOutputCollection).Cast<GraphableOutputCollection>().ToArray();
//            List<GraphableOutput> toview = new List<GraphableOutput>();
//            foreach (GraphableOutputCollection col in vcollections)
//            {
//                toview.AddRange(col.Graphs);
//            }

//            if (toview.Count > 0) toview[0].View(toview.ToArray());


//            return notCol;
//        }

//        public IBatchViewable[] View(IBatchViewable[] viewable)
//        {
//            return this.View(viewable, false);
//        }

//        #endregion

//        #region IBatchWritable Members

//        public IBatchWritable[] WriteToDisk(IBatchWritable[] toWrite)
//        {
//            IBatchWritable[] notCol = toWrite.Where(w => !(w is GraphableOutputCollection)).ToArray();
//            if (toWrite.Any(w => w is GraphableOutput))
//            {
//                GraphableOutputCollection[] collections = toWrite.Where(w => w is GraphableOutputCollection).Cast<GraphableOutputCollection>().ToArray();

//                List<IBatchWritable> toret = new List<IBatchWritable>(notCol);
//                foreach (GraphableOutputCollection col in collections)
//                {
//                    toret.AddRange(col.Graphs.Cast<IBatchWritable>());
//                }
//                return toret.ToArray();
//            }

//            GraphableOutputCollection[] wcollections = toWrite.Where(w => w is GraphableOutputCollection).Cast<GraphableOutputCollection>().ToArray();
//            List<GraphableOutput> towrite = new List<GraphableOutput>();
//            foreach (GraphableOutputCollection col in wcollections)
//            {
//                towrite.AddRange(col.Graphs);
//            }

//            if (towrite.Count > 0) towrite[0].WriteToDisk(towrite.ToArray());


//            return notCol;
//        }

//        #endregion
//    }
//}
