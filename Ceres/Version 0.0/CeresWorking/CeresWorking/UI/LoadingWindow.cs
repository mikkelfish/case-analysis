using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CeresBase.UI
{
    public partial class LoadingWindow : Form
    {
        public LoadingWindow()
        {
            InitializeComponent();
        }

        private string text;
        public string LabelText
        {
            get { return text; }
            set 
            { 
                text = value;
                 if(this.IsHandleCreated)
                     this.Invoke(new CeresBase.General.Utilities.EmptyHandler(this.setText));
            }
        }

        private int percent;

        public int Percent
        {
            get { return percent; }
            set 
            { 
                percent = value;
                if(this.IsHandleCreated)
                    this.Invoke(new CeresBase.General.Utilities.EmptyHandler(this.setPercent));
            }
        }

        private void setPercent()
        {
            this.progressBar1.Value = this.percent;
        }

        private void setText()
        {
            this.label1.Text = this.text;
        }
	

        private CeresBase.General.Utilities.EmptyHandler cancelAction;
        public CeresBase.General.Utilities.EmptyHandler CancelAction
        {
            get { return cancelAction; }
            set { cancelAction = value; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.cancelAction != null)
            {
                this.cancelAction();
            }
            this.Close();
        }
	
    }
}