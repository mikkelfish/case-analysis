﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MesosoftCommon.Layout.BasicPanels;
using CeresBase.Data;
using System.Globalization;
using System.Collections;
using System.ComponentModel;
using MesosoftCommon.Layout.BasicPanels.SupportControls;
using MesosoftCommon.Utilities.Converters;
using MesosoftCommon.Utilities.Converters.ValueConverters;
using MesosoftCommon.ExtensionMethods;
using System.Reflection;
using System.Collections.ObjectModel;

namespace CeresBase.UI
{
    /// <summary>
    /// Interaction logic for SimpleGraph.xaml
    /// </summary>
    public partial class SimpleGraph : UserControl, INotifyPropertyChanged
    {
        enum AxisSide
        {
            Top,
            Bottom,
            Left,
            Right
        }

        private MesoLayoutLength leftMargin = new MesoLayoutLength(20);
        public MesoLayoutLength LeftMargin
        {
            get
            {
                return leftMargin;
            }
            set
            {
                leftMargin = value;
                this.OnPropertyChanged("LeftMargin");
            }
        }

        private MesoLayoutLength topMargin = new MesoLayoutLength(20);
        public MesoLayoutLength TopMargin
        {
            get
            {
                return topMargin;
            }
            set
            {
                topMargin = value;
                this.OnPropertyChanged("TopMargin");
            }
        }

        private MesoLayoutLength rightMargin = new MesoLayoutLength(20);
        public MesoLayoutLength RightMargin
        {
            get
            {
                return rightMargin;
            }
            set
            {
                rightMargin = value;
                this.OnPropertyChanged("RightMargin");
            }
        }

        private MesoLayoutLength bottomMargin = new MesoLayoutLength(20);
        public MesoLayoutLength BottomMargin
        {
            get
            {
                return bottomMargin;
            }
            set
            {
                bottomMargin = value;
                this.OnPropertyChanged("BottomMargin");
            }
        }

        private SimpleGraphAxis leftAxis = new SimpleGraphAxis(SimpleGraphPresets.StandardTwoAxis);
        public SimpleGraphAxis LeftAxis
        {
            get
            {
                return leftAxis;
            }
            set
            {
                leftAxis = value;
            }
        }

        private SimpleGraphAxis bottomAxis = new SimpleGraphAxis(SimpleGraphPresets.StandardTwoAxis);
        public SimpleGraphAxis BottomAxis
        {
            get
            {
                return bottomAxis;
            }
            set
            {
                bottomAxis = value;
            }
        }

        private SimpleGraphAxis rightAxis = new SimpleGraphAxis(SimpleGraphPresets.StandardTwoAxis);
        public SimpleGraphAxis RightAxis
        {
            get
            {
                return rightAxis;
            }
            set
            {
                rightAxis = value;
            }
        }

        private SimpleGraphAxis topAxis = new SimpleGraphAxis(SimpleGraphPresets.StandardTwoAxis);
        public SimpleGraphAxis TopAxis
        {
            get
            {
                return topAxis;
            }
            set
            {
                topAxis = value;
            }
        }

        public string XUnits { get; set; }
        public string YUnits { get; set; }
        public double XMax { get; set; }
        public double YMax { get; set; }
        public double XMin { get; set; }
        public double YMin { get; set; }

        private Point desiredPixelsBetweenTicks = new Point(30, 30);
        public Point DesiredPixelsBetweenTicks
        {
            get
            {
                return desiredPixelsBetweenTicks;
            }
            set
            {
                desiredPixelsBetweenTicks = value;
            }
        }

        public SimpleGraphQuadrants Quadrants { get; set; }

        public bool SquareRatioLock { get; set; }

        private Rect GraphSpace;

        private double pixPerPointX;
        private double pixPerPointY;

        private ObservableCollection<DataSeries> seriesList = new ObservableCollection<DataSeries>();
        public ObservableCollection<DataSeries> SeriesList { get { return seriesList; } }

        public void RenderSeriesList()
        {
            foreach (DataSeries series in SeriesList)
            {
                if (series.GraphType == GraphingType.Line)
                {
                    for (int i = 0; i < series.YData.Length; i++)
                    {
                        Polyline seriesLine = new Polyline();

                        seriesLine.Points = TranslateGraphCoordinatesListsToPointCollection(series.XData, series.YData[i]);
                        seriesLine.Stroke = Brushes.Black;
                        seriesLine.StrokeThickness = 1.4;
                        seriesLine.FillRule = FillRule.EvenOdd;

                        GraphDisplay.Children.Add(seriesLine);
                        MesoGrid.SetRow(seriesLine, 1);
                        MesoGrid.SetColumn(seriesLine, 1);
                    }
                }
                else if (series.GraphType == GraphingType.Scatter)
                {
                    for (int i = 0; i < series.YData.Length; i++)
                    {
                        PointCollection points = this.TranslateGraphCoordinatesListsToPointCollection(series.XData, series.YData[i]);
                        GeometryGroup geo = new GeometryGroup();

                        foreach (Point point in points)
                        {
                            EllipseGeometry ellipse = new EllipseGeometry();
                            ellipse.Center = point;
                            ellipse.RadiusX = 2;
                            ellipse.RadiusY = 2;
                            geo.Children.Add(ellipse);
                        }

                        Path path = new Path();
                        path.Data = geo;
                        path.Fill = new SolidColorBrush(Colors.Black);


                        GraphDisplay.Children.Add(path);
                        MesoGrid.SetRow(path, 1);
                        MesoGrid.SetColumn(path, 1);

                    }

                }
                else if (series.GraphType == GraphingType.Column)
                {
                    for (int i = 0; i < series.YData.Length; i++)
                    {
                        Polygon poly = new Polygon();
                        PointCollection polypoints = new PointCollection();
                        PointCollection points = TranslateGraphCoordinatesListsToPointCollection(series.XData, series.YData[i]);
                        polypoints.Add(new Point(points[0].X, 0));
                        for(int j =0; j < points.Count; j++)
                        {                          
                            polypoints.Add(new Point(points[j].X, points[j].Y));
                            if (j < points.Count - 1)
                            {
                                polypoints.Add(new Point(points[j + 1].X, points[j].Y));
                            }
                        }

                        polypoints.Add(new Point(polypoints[polypoints.Count-1].X, 0));
                        polypoints.Add(new Point(polypoints[0].X, polypoints[0].Y));
                        
                        poly.Points = polypoints;

                        poly.Stroke = Brushes.Black;
                        poly.StrokeThickness = 1.4;
                        poly.FillRule = FillRule.EvenOdd;

                        GraphDisplay.Children.Add(poly);
                        MesoGrid.SetRow(poly, 1);
                        MesoGrid.SetColumn(poly, 1);
                    }
                }

              
            }
        }

//        bool showXTicks, bool showYTicks, int tickUnitSpacing, double tickSize, bool useMajorTicks,
//                            int majorTickUnitSpacing, double majorTickSize, bool showXLine, bool showYLine, double lineWidth,
//                            bool showXDigits, bool showYDigits, int digitUnitSpacing, int digitFontSize

        public double RoundOutwards(double value)
        {
            double ret = 0.0;

            if (value < 0)
            {
                ret = Math.Floor(value);
            }
            else
            {
                ret = Math.Ceiling(value);
            }

            return ret;
        }

        private List<DraggableText> draggableTexts = new List<DraggableText>();
        public List<DraggableText> DraggableTexts { get { return draggableTexts; } }

        public void AddDraggableText(string text, Point startingLocation)
        {
            DraggableText dt = new DraggableText();
            dt.Text = text;
            dt.Location = startingLocation;
            draggableTexts.Add(dt);
            UpdateGraph();
        }

        public void RenderDraggableTexts()
        {
            foreach (DraggableText dt in draggableTexts)
            {
                MesoGrid textGrid = new MesoGrid();               
                
                TextBlock tb = new TextBlock();
                tb.Text = dt.Text;
                tb.Measure(new Size(GraphDisplay.ActualWidth, GraphDisplay.ActualWidth));
                textGrid.Width = tb.DesiredSize.Width;
                textGrid.Height = tb.DesiredSize.Height;

                tb.Tag = draggableTexts.IndexOf(dt);

                MesoGrid.SetXCoordinate(textGrid, dt.Location.X);
                MesoGrid.SetYCoordinate(textGrid, dt.Location.Y);
                MesoGrid.SetIsDraggable(textGrid, true);
                MesoGrid.SetDragGripTarget(tb, textGrid);
                MesoGrid.SetIsConstrained(tb, false);
                MesoGrid.SetIsConstrained(textGrid, false);

                textGrid.Children.Add(tb);
                GraphDisplay.Children.Add(textGrid);
            }
        }

        private void updateDraggableTexts()
        {
            foreach (UIElement elem in GraphDisplay.Children)
            {
                if (!(elem is MesoGrid)) continue;

                MesoGrid mg = elem as MesoGrid;
                if (mg.Children.Count != 1) continue;
                if (!(mg.Children[0] is TextBlock)) continue;

                TextBlock tb = mg.Children[0] as TextBlock;

                DraggableText dt = draggableTexts[(int)tb.Tag];
                dt.Location = new Point(MesoGrid.GetXCoordinate(mg), MesoGrid.GetYCoordinate(mg));
                //dt.Location.X = MesoGrid.GetXCoordinate(mg);
                //dt.Location.Y = MesoGrid.GetYCoordinate(mg);
            }
        }

        public void UpdateGraph()
        {
            this.updateDraggableTexts();
            //GraphDisplay.Children.Clear();
            this.clearNonGridElements();
            GraphDisplay.InvalidateArrange();
            GraphDisplay.UpdateLayout();
            this.RefreshGraph();
            this.SizeToData();
            this.RenderAxes();
            this.RenderSeriesList();
            this.RenderDraggableTexts();
            this.RenderTitle();
            this.RenderLegend();
        }

        public void RenderLegend()
        {

        }

        private string title = "";
        public string Title
        {
            get { return this.title; }
            set { this.title = value; }
        }

        public void RenderTitle()
        {
            TextBlock titleText = new TextBlock();
            titleText.Text = this.Title;
            titleText.FontFamily = new FontFamily("Arial");
            titleText.FontSize = 24;
            MesoGrid.SetRow(titleText, 1);
            MesoGrid.SetColumn(titleText, 1);
            MesoGrid.SetYCoordinate(titleText, 5);
            titleText.Measure(GraphDisplay.RenderSize);
            MesoGrid.SetXCoordinate(titleText, (CentralGraphDisplay.ActualWidth / 2.0) - (titleText.DesiredSize.Width / 2.0));
            MesoGrid.SetIsConstrained(titleText, false);
            GraphDisplay.Children.Add(titleText);
            GraphDisplay.UpdateLayout();
            //Center it

        }

        private void clearNonGridElements()
        {
            List<UIElement> toRemove = new List<UIElement>();

            foreach (UIElement elem in GraphDisplay.Children)
            {
                if (elem is Grid)
                    continue;
                toRemove.Add(elem);
            }

            foreach (UIElement elem in toRemove)
            {
                GraphDisplay.Children.Remove(elem);
            }
        }

        public void SizeToData()
        {
            double minX = 0.0;
            double minY = 0.0;
            double maxX = 0.0;
            double maxY = 0.0;
            bool xInit = false;
            bool yInit = false;

            foreach (DataSeries series in SeriesList)
            {
                foreach (double xData in series.XData)
                {
                    //First things first : set the min/max to the first data point
                    if (!xInit)
                    {
                        minX = xData;
                        maxX = xData;
                        xInit = true;
                        continue;
                    }

                    if (xData < minX)
                        minX = xData;
                    else if (xData > maxX)
                        maxX = xData;
                }

                foreach (double[] dataArray in series.YData)
                {
                    foreach (double yData in dataArray)
                    {
                        //First things first : set the min/max to the first data point
                        if (!yInit)
                        {
                            minY = yData;
                            maxY = yData;
                            yInit = true;
                            continue;
                        }

                        if (yData < minY)
                            minY = yData;
                        else if (yData > maxY)
                            maxY = yData;
                    }
                }
            }

            //Clamp the units to the nearest appropriate multiple.
            //Appropriate multiples are hardcoded as 1, 2.5, and 5.
            //double dist1 = distanceFromMultiple(GraphSpace.Width, 1.0);
            //double dist25 = distanceFromMultiple(GraphSpace.Width, 2.5);
            //double dist5 = distanceFromMultiple(GraphSpace.Width, 5.0);






            //using the Unit size of the smaller of the two axes for each side, clamp the min/max outwards to appropriate numbers
            double xUnit = (LeftAxis.Units < RightAxis.Units ? LeftAxis.Units : RightAxis.Units);
            double yUnit = (TopAxis.Units < BottomAxis.Units ? TopAxis.Units : BottomAxis.Units);

            double minXUnitRem = minX / xUnit;
            double maxXUnitRem = maxX / xUnit;
            double minYUnitRem = minY / yUnit;
            double maxYUnitRem = maxY / yUnit;

            double minXTrunc = Math.Truncate(minXUnitRem);
            double maxXTrunc = Math.Truncate(maxXUnitRem);
            double minYTrunc = Math.Truncate(minYUnitRem);
            double maxYTrunc = Math.Truncate(maxYUnitRem);

            if (minXUnitRem != minXTrunc)
                minX = Math.Floor(minXUnitRem) * xUnit;
            if (maxXUnitRem != maxXTrunc)
                maxX = Math.Ceiling(maxXUnitRem) * xUnit;
            if (minYUnitRem != minYTrunc)
                minY = Math.Floor(minYUnitRem) * yUnit;
            if (maxYUnitRem != maxYTrunc)
                maxY = Math.Ceiling(maxYUnitRem) * yUnit;

            GraphSpace.Location = new Point(minX, minY);
            GraphSpace.Width = maxX - minX;
            GraphSpace.Height = maxY - minY;

            //GraphSpace can NEVER be a height or width of 0.0.  In this case set it to a default of 2.0.
            if (GraphSpace.Width == 0.0)
                GraphSpace.Width = 2.0;
            if (GraphSpace.Height == 0.0)
                GraphSpace.Height = 2.0;

            XMax = maxX;
            XMin = minX;
            YMax = maxY;
            YMin = minY;

            //Handle the ticks!
            double xSpace = this.GraphDisplay.ColumnDefinitions[1].ActualLength;
            double ySpace = this.GraphDisplay.RowDefinitions[1].ActualLength;

            int xTicksCount = (int)Math.Floor(xSpace / DesiredPixelsBetweenTicks.X);
            int yTicksCount = (int)Math.Floor(ySpace / DesiredPixelsBetweenTicks.Y);

            double xPointsPerTick = GraphSpace.Width / xTicksCount;
            double yPointsPerTick = GraphSpace.Height / yTicksCount;

            BottomAxis.Units = Math.Round(xPointsPerTick, 1);
            LeftAxis.Units = Math.Round(yPointsPerTick, 1);




            //DesiredPixelsBetweenTicks
        }

        private double distanceFromMultiple(double number, double multipleBase)
        {
            if (number == multipleBase)
            {
                return 0.0;
            }

            double specificBase = multipleBase;

            while (number < multipleBase)
            {
                multipleBase /= 10;
            }

            double multiplier = Math.Round(number / multipleBase, 0);
            return Math.Abs(number - multipleBase * multiplier);
        }

        public bool DividesEvenly(double value, double divisor)
        {
            double result = value / divisor;
            if (result == Math.Truncate(result))
                return true;
            return false;
        }

        public void RenderAxes()
        {
            //LeftAxis.MinorTicks.ShowLabel = true;
            //LeftAxis.Units = 0.5;
            //RightAxis.MinorTicks.ShowLabel = true;
            renderAxis(LeftAxis, AxisSide.Left);
            renderAxis(BottomAxis, AxisSide.Bottom);
            //renderAxis(RightAxis, AxisSide.Right);
            //renderAxis(TopAxis, AxisSide.Top);
        }
        
        private void renderAxis(SimpleGraphAxis axisToRender, AxisSide side)
        {
            //Set up variables by side
            double beginX = 0.0, beginY = 0.0, endX = 0.0, endY = 0.0;
            int row = 0, column = 0;
            Point directionality = new Point(0, 0);
            
            switch (side)
            {
                case AxisSide.Left:
                    beginX = GraphDisplay.ColumnDefinitions[0].ActualLength - axisToRender.AxisLineWidth / 2;
                    beginY = GraphDisplay.RowDefinitions[1].ActualLength;
                    endX = beginX;
                    endY = 0;

                    row = 1;
                    column = 0;

                    directionality.X = -1;
                    directionality.Y = 0;
                    break;
                case AxisSide.Bottom:
                    beginX = 0;
                    beginY = 0.0 + axisToRender.AxisLineWidth / 2;
                    endX = GraphDisplay.ColumnDefinitions[1].ActualLength;
                    endY = beginY;

                    row = 2;
                    column = 1;

                    directionality.X = 0;
                    directionality.Y = 1;
                    break;
                case AxisSide.Right:
                    beginX = 0.0 + axisToRender.AxisLineWidth / 2;
                    beginY = GraphDisplay.RowDefinitions[1].ActualLength;
                    endX = beginX;
                    endY = 0.0;

                    row = 1;
                    column = 2;

                    directionality.X = 1;
                    directionality.Y = 0;
                    break;
                case AxisSide.Top:
                    beginX = 0;
                    beginY = GraphDisplay.RowDefinitions[0].ActualLength - axisToRender.AxisLineWidth / 2;
                    endX = GraphDisplay.ColumnDefinitions[1].ActualLength;
                    endY = beginY;

                    row = 0;
                    column = 1;

                    directionality.X = 0;
                    directionality.Y = -1;
                    break;
            }

            //Render base Axis Line
            Line axisLine = new Line();

            axisLine.X1 = beginX;
            axisLine.Y1 = beginY;
            axisLine.X2 = endX;
            axisLine.Y2 = endY;
            axisLine.Stroke = Brushes.Black;
            axisLine.StrokeThickness = axisToRender.AxisLineWidth;
            GraphDisplay.Children.Add(axisLine);
            MesoGrid.SetRow(axisLine, row);
            MesoGrid.SetColumn(axisLine, column);

            //-------------------
            //Render Ticks

            double start = 0.0;
            double end = 0.0;

            if (directionality.X == 0)
            {
                //Top or Bottom
                start = GraphSpace.Left;
                end = GraphSpace.Right;
            }
            else
            {
                //Left or Right
                start = GraphSpace.Top;
                end = GraphSpace.Bottom;
            }

            //Clamp the start and end to multiples of the unit away from 0 (rounding up)
            double sRem = start / axisToRender.Units;
            double eRem = end / axisToRender.Units;
            double sTruncRem = Math.Truncate(sRem);
            double eTruncRem = Math.Truncate(eRem);

            if (sRem != sTruncRem)
                start = RoundOutwards(sRem) *  axisToRender.Units;

            if (eRem != eTruncRem)
                end = RoundOutwards(eRem) * axisToRender.Units;                

            //because the double type is inherently inaccurate, we have to do some fudging here.  Awww yeah, fudge.
            double i = start;
            while (i <= end)
            {
                i = Math.Round(i, 4);

                SimpleGraphAxisTicks ticks;
                if (axisToRender.MajorTicks != null && (DividesEvenly(Math.Round(i / axisToRender.Units, 4), axisToRender.MajorTicks.UnitSpacing)))
                    ticks = axisToRender.MajorTicks;
                else if (axisToRender.MinorTicks != null && (DividesEvenly(Math.Round(i / axisToRender.Units, 4), axisToRender.MinorTicks.UnitSpacing)))
                    ticks = axisToRender.MinorTicks;
                else if (axisToRender.SubMinorTicks != null && (DividesEvenly(Math.Round(i / axisToRender.Units, 4), axisToRender.SubMinorTicks.UnitSpacing)))
                    ticks = axisToRender.SubMinorTicks;
                else
                {
                    i += axisToRender.Units;
                    continue;
                }

                Point beginPoint = new Point(0, 0);
                Point endPoint = new Point(0, 0);

                if (directionality.X == 0)
                {
                    beginPoint = TranslateGraphCoordinateToPoint(new Point(i, 0));
                    beginPoint.Y = beginY;
                    endPoint = TranslateGraphCoordinateToPoint(new Point(i, 0));
                    endPoint.Y = beginY + (ticks.Length * directionality.Y);
                }
                else
                {
                    beginPoint = TranslateGraphCoordinateToPoint(new Point(0, i));
                    beginPoint.X = beginX;
                    endPoint = TranslateGraphCoordinateToPoint(new Point(0, i));
                    endPoint.X = beginX + (ticks.Length * directionality.X);
                }

                Line tickLine = new Line();

                tickLine.X1 = beginPoint.X;
                tickLine.Y1 = beginPoint.Y;
                tickLine.X2 = endPoint.X;
                tickLine.Y2 = endPoint.Y;
                tickLine.Stroke = Brushes.Black;
                tickLine.StrokeThickness = ticks.Width;
                GraphDisplay.Children.Add(tickLine);
                MesoGrid.SetRow(tickLine, row);
                MesoGrid.SetColumn(tickLine, column);

                
                //Put this up here to allow us to store the tick label while still incrementing properly before the (potential) continue
                string tickTextText = i.ToString();

                i += axisToRender.Units;

                if (!ticks.ShowLabel) continue;

                TextBlock tickText = new TextBlock();
                tickText.Text = tickTextText;
                tickText.FontSize = ticks.LabelFontSize;
                tickText.FontFamily = ticks.LabelFont;
                tickText.HorizontalAlignment = HorizontalAlignment.Center;
                tickText.VerticalAlignment = VerticalAlignment.Center;
                tickText.Measure(new Size(GraphDisplay.ActualWidth, GraphDisplay.ActualHeight));

                Point textPoint = new Point(endPoint.X, endPoint.Y);

                textPoint.X -= (tickText.DesiredSize.Width / 2);
                textPoint.Y -= (tickText.DesiredSize.Height / 2);

                if (directionality.X == 0)
                {
                    textPoint.Y += (ticks.LabelFontSize / 2) * directionality.Y;
                }
                else
                {
                    textPoint.X += (ticks.LabelFontSize / 2) * directionality.X;
                }

                Point freePoint = GraphDisplay.GetNonConstrainedPoint(textPoint, row, column);

                GraphDisplay.Children.Add(tickText);
                MesoGrid.SetIsConstrained(tickText, false);
                MesoGrid.SetXCoordinate(tickText, freePoint.X);
                MesoGrid.SetYCoordinate(tickText, freePoint.Y);
            }
        }

        public void RenderQuadrant(SimpleGraphQuadrant quad)
        {
            if (quad.ShowXLine)
            {
                Line xLine = new Line();
                Point p1 = TranslateGraphCoordinateToPoint(new Point(0, 0));
                Point p2;
                if(quad.Directionality.X > 0)
                    p2 = TranslateGraphCoordinateToPoint(new Point(XMax, 0));
                else
                    p2 = TranslateGraphCoordinateToPoint(new Point(XMin, 0));
                xLine.X1 = p1.X;
                xLine.Y1 = p1.Y;
                xLine.X2 = p2.X;
                xLine.Y2 = p2.Y;
                xLine.Stroke = Brushes.Black;
                xLine.StrokeThickness = quad.LineWidth;
                GraphDisplay.Children.Add(xLine);
            }
            if (quad.ShowYLine)
            {
                Line yLine = new Line();
                Point p1 = TranslateGraphCoordinateToPoint(new Point(0, 0));
                Point p2;
                if(quad.Directionality.Y > 0)
                    p2 = TranslateGraphCoordinateToPoint(new Point(0, YMax));
                else
                    p2 = TranslateGraphCoordinateToPoint(new Point(0, YMin));
                yLine.X1 = p1.X;
                yLine.Y1 = p1.Y;
                yLine.X2 = p2.X;
                yLine.Y2 = p2.Y;
                yLine.Stroke = Brushes.Black;
                yLine.StrokeThickness = quad.LineWidth;
                GraphDisplay.Children.Add(yLine);
            }

            if (quad.ShowXTicks)
            {
                for (double i = 0; true; i += quad.TickUnitSpacing)
                {
                    if (quad.Directionality.X > 0 && i > XMax)
                        break;
                    if (quad.Directionality.X < 0 && (i * -1) < XMin)
                        break;

                    Line xLine = new Line();
                    Point p1 = TranslateGraphCoordinateToPoint(new Point((i * quad.Directionality.X), 0));
                    Point p2 = TranslateGraphCoordinateToPoint(new Point((i * quad.Directionality.X), (quad.TickSize * quad.Directionality.Y)));
                    xLine.X1 = p1.X;
                    xLine.Y1 = p1.Y;
                    xLine.X2 = p2.X;
                    xLine.Y2 = p2.Y;
                    xLine.Stroke = Brushes.Black;
                    xLine.StrokeThickness = 1.0;
                    GraphDisplay.Children.Add(xLine);
                }
            }
            if (quad.ShowYTicks)
            {
                for (double i = 0; true; i += quad.TickUnitSpacing)
                {
                    if (quad.Directionality.Y > 0 && i > YMax)
                        break;
                    if (quad.Directionality.Y < 0 && (i * -1) < YMin)
                        break;

                    Line yLine = new Line();
                    Point p1 = TranslateGraphCoordinateToPoint(new Point(0, (i * quad.Directionality.Y)));
                    Point p2 = TranslateGraphCoordinateToPoint(new Point((quad.TickSize * quad.Directionality.X), (i * quad.Directionality.Y)));
                    yLine.X1 = p1.X;
                    yLine.Y1 = p1.Y;
                    yLine.X2 = p2.X;
                    yLine.Y2 = p2.Y;
                    yLine.Stroke = Brushes.Black;
                    yLine.StrokeThickness = 1.0;
                    GraphDisplay.Children.Add(yLine);
                }
            }

            if (quad.ShowXDigits)
            {
                for (double i = 0; true; i += quad.DigitUnitSpacing)
                {
                    if (quad.Directionality.X > 0 && i > XMax)
                        break;
                    if (quad.Directionality.X < 0 && (i * -1) < XMin)
                        break;

                    TextBlock xText = new TextBlock();

                    xText.Text = i.ToString();
                    xText.FontSize = quad.DigitFontSize;
                    xText.FontFamily = new FontFamily("Arial");
                    xText.HorizontalAlignment = HorizontalAlignment.Center;
                    xText.VerticalAlignment = VerticalAlignment.Center;
                    xText.Measure(new Size(GraphDisplay.ActualWidth, GraphDisplay.ActualHeight));

                    Point textPoint = TranslateGraphCoordinateToPoint(new Point(i, (quad.TickSize + 0.2) * quad.Directionality.Y));
                    //Do this to "center" the text properly such that the textPoint is in the middle of the text.
                    textPoint.X -= xText.DesiredSize.Width / 2;
                    textPoint.Y -= xText.DesiredSize.Height / 2;

                    GraphDisplay.Children.Add(xText);
                    MesoGrid.SetIsConstrained(xText, false);
                    MesoGrid.SetXCoordinate(xText, textPoint.X);
                    MesoGrid.SetYCoordinate(xText, textPoint.Y);
                }
            }
            if (quad.ShowYDigits)
            {
                for (double i = 0; true; i += quad.DigitUnitSpacing)
                {
                    if (quad.Directionality.Y > 0 && i > YMax)
                        break;
                    if (quad.Directionality.Y < 0 && (i * -1) < YMin)
                        break;

                    TextBlock yText = new TextBlock();

                    yText.Text = i.ToString();
                    yText.FontSize = quad.DigitFontSize;
                    yText.FontFamily = new FontFamily("Arial");
                    yText.HorizontalAlignment = HorizontalAlignment.Center;
                    yText.VerticalAlignment = VerticalAlignment.Center;
                    yText.Measure(new Size(GraphDisplay.ActualWidth, GraphDisplay.ActualHeight));

                    Point textPoint = TranslateGraphCoordinateToPoint(new Point((quad.TickSize + 0.2) * quad.Directionality.X, i));
                    //Do this to "center" the text properly such that the textPoint is in the middle of the text.
                    textPoint.X -= yText.DesiredSize.Width / 2;
                    textPoint.Y -= yText.DesiredSize.Height / 2;

                    GraphDisplay.Children.Add(yText);
                    MesoGrid.SetIsConstrained(yText, false);
                    MesoGrid.SetXCoordinate(yText, textPoint.X);
                    MesoGrid.SetYCoordinate(yText, textPoint.Y);
                }
            }
        }

        public void RefreshGraph()
        {
            double displayWidth = GraphDisplay.ColumnDefinitions[1].ActualLength;
            double displayHeight = GraphDisplay.RowDefinitions[1].ActualLength;

            double newPPPX = displayWidth / GraphSpace.Width;
            double newPPPY = displayHeight / GraphSpace.Height;

            if (SquareRatioLock)
            {
                if (newPPPX > newPPPY)
                    newPPPX = newPPPY;
                else
                    newPPPY = newPPPX;
            }

            if (GraphSpace.Width == 0)
                newPPPX = 0.0;
            if (GraphSpace.Height == 0)
                newPPPY = 0.0;

            pixPerPointY = newPPPY;
            pixPerPointX = newPPPX;
        }

        //Translate a graph point into a graphical point.  Therefore the origin is flipped from 0,0 (central) to top-left.
        //The graphical point can then be used directly in the GraphDisplay.
        //(only works properly when the PPPY and PPPX have been updated correctly)
        public Point TranslateGraphCoordinateToPoint(Point originalPoint)
        {
            double dX = originalPoint.X - GraphSpace.Left;
            double dY = GraphSpace.Bottom - originalPoint.Y;
            Point distanceFromOrigin = new Point(dX, dY);

            distanceFromOrigin.X *= pixPerPointX;
            distanceFromOrigin.Y *= pixPerPointY;

            return distanceFromOrigin;
        }

        public Point TranslatePointToGraphCoordinate(Point originalPoint)
        {
            Point distanceFromOrigin = originalPoint;

            //distanceFromOrigin.X -= this.GraphDisplay.ColumnDefinitions[0].Length.Value;
            //distanceFromOrigin.Y -= this.GraphDisplay.RowDefinitions[0].Length.Value;

            distanceFromOrigin.X /= pixPerPointX;
            distanceFromOrigin.Y /= pixPerPointY;

            Point ret = new Point(0, 0);
            ret.X = distanceFromOrigin.X - GraphSpace.Left;
            ret.Y = GraphSpace.Bottom - distanceFromOrigin.Y;

            //this.GraphDisplay.ColumnDefinitions[0]

            return ret;
        }

        public PointCollection TranslateGraphCoordinatesCollectionToPointCollection(PointCollection originalCollection)
        {
            PointCollection ret = new PointCollection(originalCollection.Count);

            foreach (Point point in originalCollection)
            {
                ret.Add(TranslateGraphCoordinateToPoint(point));
            }

            return ret;
        }

        public PointCollection TranslateGraphCoordinatesListsToPointCollection(double[] xList, double[] yList)
        {
            PointCollection ret = new PointCollection(xList.Length);

            for (int i = 0; i < xList.Length; i++)
            {
                ret.Add(TranslateGraphCoordinateToPoint(new Point(xList[i], yList[i])));
            }

            return ret;
        }

        private void initGraph()
        {
            InitializeComponent();

            SquareRatioLock = false;
            Quadrants = new SimpleGraphQuadrants(SimpleGraphPresets.StandardTwoAxis);
            XUnits = "Xunits";
            YUnits = "Yunits";
            XMax = 100;
            YMax = 3;
            XMin = -1;
            YMin = -1;
            GraphSpace = new Rect(0, -1, 125, 2);
            //SeriesList = new List<DataSeries>();
            this.SizeChanged += new SizeChangedEventHandler(SimpleGraph_SizeChanged);
            this.IsCoordinateBoxLocked = false;
            this.DisplayCoordinateBox = false;
            this.DisplayCoordinatePoint = new Point();
        }

        void SimpleGraph_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //Clean up delays with a timer
            //System.Windows.Threading.DispatcherTimer
            //this.SizeToData();
            this.UpdateGraph();
        }

        public SimpleGraph()
        {
            initGraph();
            this.seriesList.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(seriesList_CollectionChanged);
        }

        void seriesList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.UpdateGraph();
        }

        public SimpleGraph(double[] xData, double[][] yData) : this()
        {
            //initGraph();
            //be = BindingOperations.GetBindingExpression(testing, MesoLayoutDefinition.LengthProperty);

            //Binding b = new Binding("LeftMargin");
            //b.Source = this;
            //b.Converter = new TypeCoverterAsValueConverter() { Converter = new StringMesoLayoutLengthConverter() };
            //BindingOperations.SetBinding(testing, MesoLayoutDefinition.LengthProperty, b);
           // be.UpdateTarget();
            //testing.Length = new MesoLayoutLength(400);

            DataSeries ds = new DataSeries(xData, yData);
            SeriesList.Add(ds);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //this.SizeToData();
            this.UpdateGraph();
        }

        public bool DisplayCoordinateBox { get; set; }
        public bool IsCoordinateBoxLocked { get; set; }

        private Point displayCoordinatePoint;
        public Point DisplayCoordinatePoint
        {
            get
            {
                return this.displayCoordinatePoint;
            }
            set
            {
                this.displayCoordinatePoint = value;
                OnPropertyChanged("DisplayCoordinatePoint");
            }
        }

        private Point coordinateBoxPoint;
        public Point CoordinateBoxPoint 
        {
            get
            {
                return this.coordinateBoxPoint;
            }
            set
            {
                this.coordinateBoxPoint = value;
                OnPropertyChanged("CoordinateBoxPoint");
            }
        }

        private void GraphDisplay_MouseMove(object sender, MouseEventArgs e)
        {
            if (!this.DisplayCoordinateBox)
                return;

            if (!this.IsCoordinateBoxLocked)
                this.UpdateCoordinate(e.GetPosition(this.CentralGraphDisplay));
        }

        public void UpdateAdornerLayer()
        {
            //handle the adorner action
            AdornerLayer al = AdornerLayer.GetAdornerLayer(this.CentralGraphDisplay);

            if (al != null)
            {
                Adorner[] toUpdateArray = al.GetAdorners(this.CentralGraphDisplay);
                if (toUpdateArray != null)
                {
                    foreach (Adorner toUpdate in toUpdateArray)
                    {
                        if (toUpdate is SimpleGraphCoordinateBoxAdorner)
                        {
                            //(toUpdate as SimpleGraphCoordinateBoxAdorner).CentralPoint = mousePosition;
                            (toUpdate as SimpleGraphCoordinateBoxAdorner).CentralPoint = this.TranslateGraphCoordinateToPoint(this.DisplayCoordinatePoint);
                            //Clamp Graph Coordinates to the desired decimal accuracy
                            Point newPoint = new Point();
                            newPoint.X = Math.Round(this.DisplayCoordinatePoint.X, pointPrecision);
                            newPoint.Y = Math.Round(this.DisplayCoordinatePoint.Y, pointPrecision);
                            (toUpdate as SimpleGraphCoordinateBoxAdorner).DisplayText = newPoint.ToString();
                            (toUpdate as SimpleGraphCoordinateBoxAdorner).ShowDisplayText = true;
                            (toUpdate as SimpleGraphCoordinateBoxAdorner).UseCustomDisplayTextPosition = true;
                            (toUpdate as SimpleGraphCoordinateBoxAdorner).CustomDisplayTextPosition = new Point(0, CentralGraphDisplay.ActualHeight - 18);
                        }
                    }
                }
                else
                {
                    al.Add(new SimpleGraphCoordinateBoxAdorner(this.CentralGraphDisplay, this.TranslateGraphCoordinateToPoint(this.DisplayCoordinatePoint), this.DisplayCoordinatePoint.ToString()));
                }

                al.Update(this.CentralGraphDisplay);
            }
        }

        public void UpdateCoordinate(Point targetPosition)
        {
            Point graphCoord = this.TranslatePointToGraphCoordinate(targetPosition);
            //Alter the graphCoord to point at the spot on the selected line corresponding with the location
            int xIndex = this.findNearestXCoordinate(graphCoord.X, this.SeriesList[0]);
            graphCoord.X = this.SeriesList[0].XData[xIndex];
            graphCoord.Y = this.SeriesList[0].YData[SelectedLine][xIndex];
            this.CoordinateBoxPoint = graphCoord;

            this.DisplayCoordinatePoint = graphCoord;

            this.UpdateAdornerLayer();
        }

        private int pointPrecision = 2;
        public int PointPrecision
        {
            get { return this.pointPrecision; }
            set { this.pointPrecision = value; }
        }

        private int selectedLine = 0;
        public int SelectedLine
        {
            get { return this.selectedLine; }
            set { this.selectedLine = value; }
        }

        private int findNearestXCoordinate(double coordinate, DataSeries series)
        {
            //First see if the coord is closer to the beginning or the ending and work from there
            //double distance = Math.Abs(coordinate - series.XData[0]);
            //double temp;
            //for (int i = 1; i < series.XData.Length; i++)
            //{
            //    temp = Math.Abs(coordinate - series.XData[i]);
            //    if (i == 50)
            //    {
            //        int j = 0;
            //    }
            //    if (temp > distance)
            //    {

            //        return i - 1;
            //    }
            //    distance = temp;
            //}
            //return series.XData.Length - 1;
            double distance = Math.Abs(coordinate - series.XData[0]);
            double temp = Math.Abs(coordinate - series.XData[series.XData.Length - 1]);
            if (temp < distance)
            {
                distance = temp;
                //Check from the back
                for (int i = series.XData.Length - 2; i >= 0; i--)
                {
                    temp = Math.Abs(coordinate - series.XData[i]);
                    if (temp > distance) return i + 1;
                    distance = temp;
                }

                return 0;
            }
            else
            {
                //Check from the front
                for (int i = 1; i < series.XData.Length; i++)
                {
                    temp = Math.Abs(coordinate - series.XData[i]);
                    if (temp > distance) return i - 1;
                    distance = temp;
                }

                return series.XData.Length - 1;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion
        
        private void CentralGraphDisplay_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.IsCoordinateBoxLocked = !this.IsCoordinateBoxLocked;

            if (!this.IsCoordinateBoxLocked)
                this.UpdateCoordinate(e.GetPosition(this.CentralGraphDisplay));
        }

    }

    public class DraggableText
    {
        public string Text { get; set; }
        public Point Location { get; set; }
    }
}
