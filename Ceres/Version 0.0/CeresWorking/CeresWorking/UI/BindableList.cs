using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace CeresBase.UI
{
    public class BindableList<T> : List<T>, IBindingList 
    {
        #region IBindingList Members

        public new void Add(T item)
        {
            base.Add(item);
            ListChangedEventHandler handler = this.ListChanged;
            if (handler != null)
            {
                handler(this, new ListChangedEventArgs(ListChangedType.Reset, this.Count - 1));
            }
        }

        public new void Insert(int index, T item)
        {
            base.Insert(index, item);
            ListChangedEventHandler handler = this.ListChanged;
            if (handler != null)
            {
                handler(this, new ListChangedEventArgs(ListChangedType.Reset, index));
            }
        }

        public new void AddRange(IEnumerable<T> collection)
        {
            int count = this.Count;
            base.AddRange(collection);
            ListChangedEventHandler handler = this.ListChanged;
            if (handler != null)
            {
                IEnumerator<T> enumerator = collection.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    handler(this, new ListChangedEventArgs(ListChangedType.Reset, count));
                    count++;
                }                
            }
        }

        public new void Clear()
        {
            ListChangedEventHandler handler = this.ListChanged;
            if (handler != null)
            {
                handler(this, new ListChangedEventArgs(ListChangedType.Reset,0));
            }
        }

        public new void Remove(T item)
        {
            int index = base.IndexOf(item);
            base.Remove(item);

            if (index >= 0)
            {
                ListChangedEventHandler handler = this.ListChanged;
                if (handler != null)
                {
                    handler(this, new ListChangedEventArgs(ListChangedType.ItemDeleted, index));
                }
            }

        }

        public new void RemoveAt(int index)
        {
            base.RemoveAt(index);
            if (index >= 0)
            {
                ListChangedEventHandler handler = this.ListChanged;
                if (handler != null)
                {
                    handler(this, new ListChangedEventArgs(ListChangedType.ItemDeleted, index));
                }
            }
        }

        public void Edited(int index)
        {
            if (index >= 0)
            {
                ListChangedEventHandler handler = this.ListChanged;
                if (handler != null)
                {
                    handler(this, new ListChangedEventArgs(ListChangedType.Reset, index));
                }
            }

        }


        public void AddIndex(PropertyDescriptor property)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public object AddNew()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool SupportsChangeNotification
        {
            get { return true; }
        }        

        public bool AllowNew
        {
            get { return true; }
        }

        public bool AllowRemove
        {
            get { return true; }
        }

       

        public event ListChangedEventHandler ListChanged;

        public void RemoveIndex(PropertyDescriptor property)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public bool AllowEdit
        {
            get { return true; }
        }


        public void ApplySort(PropertyDescriptor property, ListSortDirection direction)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int Find(PropertyDescriptor property, object key)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool IsSorted
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public void RemoveSort()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ListSortDirection SortDirection
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public PropertyDescriptor SortProperty
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }



        public bool SupportsSearching
        {
            get { return false; }
        }

        public bool SupportsSorting
        {
            get { return false; }
        }

        #endregion
    }
}
