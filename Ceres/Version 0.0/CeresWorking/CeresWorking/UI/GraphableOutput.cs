﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using CeresBase.Data;
using Microsoft.Win32;
using CeresBase.IO;
using CeresBase.General;
using System.Windows.Controls;
using CeresBase.FlowControl.Adapters.Matlab;


namespace CeresBase.UI
{
    public class GraphableOutput : CombinedBatchOutput, IGraphable, IBatchViewable
    {
        public GraphableOutput()
        {
            this.Label = "";
        }


        public override string ToString()
        {
            string toRet = "";
            if (this.SourceDescription != null)
                toRet = this.SourceDescription;
            if (this.Label != null)
                toRet += " " + this.Label;

            return toRet;
        }

        #region IGraphable Members

        public GraphingType GraphType { get; set; }
        
        public double[] XData
        {
            get;
            set;
        }

        public double[][] YData
        {
            get;
            set;
        }

        public double[] OtherAxis
        {
            get;
            set;
        }

        public string Label
        {
            get;
            set;
        }

        public string XLabel { get; set; }
        public string YLabel { get; set; }

        public string XUnits { get; set; }
        public string YUnits { get; set; }

        public string SourceDescription { get; set; }

        public string[] SeriesLabels { get; set; }

        public object[] ExtraInfo { get; set; }

        #endregion

        #region IBatchViewable Members

        //public IBatchViewable[] View(IBatchViewable[] viewable, bool saveToManager)
        //{
        //    return this.View(viewable);
        //}

        public System.Windows.Controls.Control[] View(IBatchViewable[] viewable, out IBatchViewable[] notUsed)
        {
            //IBatchViewable[] notCollection = viewable.Where(view => !(view is GraphableOutputCollection)).ToArray();
            //GraphableOutputCollection[] collections = viewable.Where(view => view is GraphableOutputCollection).Cast<GraphableOutputCollection>().ToArray();
            //List<IBatchViewable> fullView = new List<IBatchViewable>(notCollection);
            //foreach (GraphableOutputCollection collection in collections)
            //{
            //    fullView.AddRange(collection.Graphs.Cast<IBatchViewable>());
            //}

            //viewable = fullView.ToArray();


            List<IBatchViewable> fullView = new List<IBatchViewable>();

            List<Control> toRet = new List<Control>();

            GraphableOutput[] outputs = viewable.Where(view => view is GraphableOutput).Cast<GraphableOutput>().ToArray();
            foreach (GraphableOutput output in outputs)
            {
                if (output.OtherAxis == null)
                {
                    SimpleGraph control = new SimpleGraph();

                    DataSeries ds = new DataSeries(output.XData, output.YData) { GraphType = output.GraphType };


                    control.SeriesList.Add(ds);

                    string label = output.SourceDescription;
                    if (label == null) label = output.Label;
                    else label = output.SourceDescription + " " + output.Label;

                    control.AddDraggableText(label, new System.Windows.Point(60, 20));

                    if (this.ExtraInfo != null)
                    {
                        int y = 30;
                        foreach (object obj in this.ExtraInfo)
                        {
                            control.AddDraggableText(obj.ToString(), new System.Windows.Point(60, y));
                            y += 10;
                        }
                    }

                    toRet.Add(control);
                }
                else
                {
                    MatlabGraphHost host = new MatlabGraphHost();
                    host.MajorAxis = this.XData;
                    host.MinorAxis = this.OtherAxis;
                    host.Data = this.YData;

                    toRet.Add(host);
                }

                //ContentBox.Show(control, System.Windows.MessageBoxButton.OK, true, output.Label);
                
                
                //ContentBox cb = new ContentBox();
                //cb.ContentElement = control;
                //cb.Buttons = ContentBoxButton.OK;
                //cb.Title = label;

                //cb.ShowDialog();
            }

            notUsed = viewable.Where(view => !(view is GraphableOutput)).ToArray();
            return toRet.ToArray();
        }

        #endregion

        #region IBatchWritable Members

        protected override object[][] GetWrittenFormat(ref CombinedBatchOutput[] toWrite)
        {
            List<IBatchWritable> fullWrite = new List<IBatchWritable>();

            List<List<object[]>> allOutput = new List<List<object[]>>();


            GraphableOutput[] outputs = toWrite.Where(view => view is GraphableOutput).Cast<GraphableOutput>().ToArray();


            GraphableOutput[] multiples = outputs;
            foreach (GraphableOutput output in multiples)
            {
                List<object[]> finalOutput = new List<object[]>();

                List<object> firstLine = new List<object>();
                firstLine.Add(output.SourceDescription);
                firstLine.Add(output.Label);
                if (output.ExtraInfo != null)
                {
                    firstLine.Add(" ");
                    firstLine.Add("Attached Info:");
                    foreach (object item in output.ExtraInfo)
                    {
                        firstLine.Add(item);
                    }
                }

                finalOutput.Add(firstLine.ToArray());

                string xaxis = output.XLabel == null ? "X-Axis" : output.XLabel;
                xaxis += " (" + (output.XUnits == null ? "None" : output.XUnits) + ")";

                string yaxis = output.YLabel == null ? "Y-Axis" : output.YLabel;
                yaxis += " (" + (output.YUnits == null ? "None" : output.YUnits) + ")";

                finalOutput.Add(new object[] { xaxis, yaxis });


                if (output.SeriesLabels != null)
                {
                    List<object> labelSeries = new List<object>();
                    labelSeries.Add("");
                    labelSeries.AddRange(output.SeriesLabels);
                    finalOutput.Add(labelSeries.ToArray());
                }

                for (int i = 0; i < output.XData.Length; i++)
                {
                    List<object> numbers = new List<object>();
                    numbers.Add(output.XData[i]);
                    for (int j = 0; j < output.YData.Length; j++)
                    {
                        numbers.Add(output.YData[j][i]);
                    }
                    finalOutput.Add(numbers.ToArray());
                }

                allOutput.Add(finalOutput);
            }


            List<object[]> writer = new List<object[]>();
            int[] maxSpacing = new int[allOutput.Count];
            int maxLines = -1;
            for (int i = 0; i < allOutput.Count; i++)
            {
                if (allOutput[i].Count > maxLines)
                {
                    maxLines = allOutput[i].Count;
                }

                for (int j = 0; j < allOutput[i].Count; j++)
                {
                    if (maxSpacing[i] < allOutput[i][j].Length + 1)
                    {
                        maxSpacing[i] = allOutput[i][j].Length + 1;
                    }
                }
            }

            for (int i = 0; i < maxLines; i++)
            {
                List<object> thisLine = new List<object>();
                for (int j = 0; j < allOutput.Count; j++)
                {
                    for (int k = 0; k < maxSpacing[j]; k++)
                    {
                        if (allOutput[j].Count <= i ||
                            allOutput[j][i].Length <= k)
                        {
                            thisLine.Add(" ");
                        }
                        else thisLine.Add(allOutput[j][i][k]);
                    }
                }
                writer.Add(thisLine.ToArray());
            }

            toWrite = toWrite.Where(view => !(view is GraphableOutput)).ToArray();

            return writer.ToArray();

        }

        #endregion
    }
}
