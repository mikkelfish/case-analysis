﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.UI
{
    public interface ILabel
    {
        string Label { get; set; }
    }
}
