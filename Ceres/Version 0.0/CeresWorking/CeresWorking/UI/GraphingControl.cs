﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.Layout.BasicPanels;
using System.Windows;
using System.Windows.Media;

namespace CeresBase.UI
{
    public enum SimpleGraphPresets
    {
        StandardTwoAxis
    }

    public class SimpleGraphQuadrant
    {
        public bool ShowXTicks { get; set; }
        public bool ShowYTicks { get; set; }

        public double TickUnitSpacing { get; set; }
        public double TickSize { get; set; }
        public bool UseMajorTicks { get; set; }
        public double MajorTickUnitSpacing { get; set; }
        public double MajorTickSize { get; set; }

        public bool ShowXLine { get; set; }
        public bool ShowYLine { get; set; }

        public double LineWidth { get; set; }

        public bool ShowXDigits { get; set; }
        public bool ShowYDigits { get; set; }

        public double DigitUnitSpacing { get; set; }
        public int DigitFontSize { get; set; }
        //Digit Font?

        public Point Directionality { get; set; }

        public SimpleGraphQuadrant()
        {
            ShowXTicks = true;
            ShowYTicks = true;

            TickUnitSpacing = 1;
            TickSize = 1.0;
            UseMajorTicks = false;

            ShowXLine = true;
            ShowYLine = true;

            LineWidth = 1.0;

            ShowXDigits = true;
            ShowYDigits = true;
            DigitUnitSpacing = 10;
            DigitFontSize = 8;
        }

        public SimpleGraphQuadrant(bool showXTicks, bool showYTicks, double tickUnitSpacing, double tickSize, bool useMajorTicks,
                            double majorTickUnitSpacing, double majorTickSize, bool showXLine, bool showYLine, double lineWidth,
                            bool showXDigits, bool showYDigits, double digitUnitSpacing, int digitFontSize, Point directionality)
        {
            ShowXTicks = showXTicks;
            ShowYTicks = showYTicks;
            TickUnitSpacing = tickUnitSpacing;
            TickSize = tickSize;
            UseMajorTicks = useMajorTicks;
            MajorTickUnitSpacing = majorTickUnitSpacing;
            MajorTickSize = majorTickSize;
            ShowXLine = showXLine;
            ShowYLine = showYLine;
            LineWidth = lineWidth;
            ShowXDigits = showXDigits;
            ShowYDigits = showYDigits;
            DigitUnitSpacing = digitUnitSpacing;
            DigitFontSize = digitFontSize;
            Directionality = directionality;
        }

        //public const SimpleGraphQuadrant StandardTwoAxisQuad1 = new SimpleGraphQuadrant(false, false, 1, 1, false, 1, 1, true, true, 1.0, false, false, 1, 10);
        //public const SimpleGraphQuadrant StandardTwoAxisQuad2 = new SimpleGraphQuadrant(false, true, 5, 3.0, false, 1, 1, false, true, 1.0, false, true, 5, 10);
        //public const SimpleGraphQuadrant StandardTwoAxisQuad3 = new SimpleGraphQuadrant(false, false, 1, 1, false, 1, 1, false, false, 1, false, false, 1, 10);
        //public const SimpleGraphQuadrant StandardTwoAxisQuad4 = new SimpleGraphQuadrant(true, false, 5, 3.0, false, 1, 1, true, false, 1.0, true, false, 5, 10);
    }

    /* Quadrants :
     *        |
     *    2   |   1
     *        |
     * -----------------
     *        |
     *    3   |   4
     *        |
     */
    public class SimpleGraphQuadrants
    {
        public SimpleGraphQuadrant Quadrant1 { get; set; }
        public SimpleGraphQuadrant Quadrant2 { get; set; }
        public SimpleGraphQuadrant Quadrant3 { get; set; }
        public SimpleGraphQuadrant Quadrant4 { get; set; }

        public SimpleGraphQuadrants()
        {
        }

        public SimpleGraphQuadrants(SimpleGraphPresets preset)
        {
            switch (preset)
            {
                case SimpleGraphPresets.StandardTwoAxis:
                    Quadrant1 = new SimpleGraphQuadrant(false, false, 1, 1, false, 1, 1, true, true, 1.0, false, false, 1, 10, new Point(1,1));
                    Quadrant2 = new SimpleGraphQuadrant(false, true, 0.2, 0.5, false, 1, 1, false, false, 1.0, false, true, 0.4, 10, new Point(-1,1));
                    Quadrant3 = new SimpleGraphQuadrant(false, false, 1, 1, false, 1, 1, false, false, 1, false, false, 1, 10, new Point(-1,-1));
                    Quadrant4 = new SimpleGraphQuadrant(true, false, 10, 0.5, false, 1, 1, false, false, 1.0, true, false, 20, 10, new Point(1,-1));
                    break;
                default:
                    break;
            }
        }
    }

    public class SimpleGraphAxisTicks
    {
        public enum SimpleGraphTickPresets
        {
            SimpleTwoAxisMinor,
            SimpleTwoAxisMajor
        }

        public string Name { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public bool ShowLabel { get; set; }
        public FontFamily LabelFont { get; set; }
        public int LabelFontSize { get; set; }
        public int UnitSpacing { get; set; }

         public SimpleGraphAxisTicks()
        {
        }

        public SimpleGraphAxisTicks(SimpleGraphTickPresets preset)
        {
            switch (preset)
            {
                case SimpleGraphTickPresets.SimpleTwoAxisMinor:
                    this.Length = 4.0;
                    this.Width = 1.0;
                    this.ShowLabel = false;
                    this.LabelFont = new FontFamily("Arial");
                    this.LabelFontSize = 6;
                    this.UnitSpacing = 1;
                    break;
                case SimpleGraphTickPresets.SimpleTwoAxisMajor:
                    this.Length = 8.0;
                    this.Width = 1.5;
                    this.ShowLabel = true;
                    this.LabelFont = new FontFamily("Arial");
                    this.LabelFontSize = 10;
                    this.UnitSpacing = 5;
                    break;
                default:
                    break;
            }
        }
    }

    public class SimpleGraphAxis
    {
        public SimpleGraphAxisTicks MajorTicks { get; set; }
        public SimpleGraphAxisTicks MinorTicks { get; set; }
        public SimpleGraphAxisTicks SubMinorTicks { get; set; }

        public double AxisLineWidth { get; set; }
        public double Units { get; set; }

        public SimpleGraphAxis()    
        {
            AxisLineWidth = 2.0;
            Units = 1.0;
        }

        public SimpleGraphAxis(SimpleGraphPresets preset)
        {
            AxisLineWidth = 2.0;
            Units = 1.0;

            switch (preset)
            {
                case SimpleGraphPresets.StandardTwoAxis:
                    this.MajorTicks = new SimpleGraphAxisTicks(SimpleGraphAxisTicks.SimpleGraphTickPresets.SimpleTwoAxisMajor);
                    this.MinorTicks = new SimpleGraphAxisTicks(SimpleGraphAxisTicks.SimpleGraphTickPresets.SimpleTwoAxisMinor);
                    break;
                default:
                    break;
            }
        }
    }

    public class GraphingControl
    {
        //List<SimpleGraph> SimpleGraphs;



        GraphingControl()
        {
            //SimpleGraphs = new List<SimpleGraph>();
        }
    }
}
