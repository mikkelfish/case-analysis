using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Collections;

namespace CeresBase.UI
{
    public partial class ExpandableListEditor : UserControl
    {
        private IWindowsFormsEditorService editor;
        public IWindowsFormsEditorService Editor
        {
            get
            {
                return this.editor;
            }
            set
            {
                this.editor = value;
            }
        }

        private object selectedObject;
        public object SelectedObject
        {
            get { return selectedObject; }
        }



	

        private CeresBase.UI.BindableList<object> objects;
        public object[] Objects
        {            
            get 
            {
                if (this.objects.Count == 1) return null;
                object[] toRet = new object[this.objects.Count - 1];
                this.objects.CopyTo(1, toRet, 0, this.objects.Count - 1);
                return toRet;
            }
        }
	
        
        public void SetOriginalList(object[] collection)
        {
            this.objects.AddRange(collection);
        }

        private Type listType;
        public Type ListType
        {
            get { return listType; }
            set 
            {
                this.listType = value; 
                this.objects.Clear();
                object obj = this.listType.InvokeMember("", System.Reflection.BindingFlags.CreateInstance, null, null, null);
                this.objects.Add(obj);
                
                this.listBox1.SelectedIndex = 0;
            }
        }


	

        public ExpandableListEditor()
        {
            InitializeComponent();
            this.objects = new CeresBase.UI.BindableList<object>();

            this.listBox1.DataSource = this.objects;
            
           // this.listBox1.DataBindings.Add("Items", this.objects, "");
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.listBox1.SelectedIndex == 0)
            {
                this.selectedObject = null;
            }
            else
            {
                this.selectedObject = this.listBox1.SelectedItem;
            }

            if (this.editor != null)
            {
                this.editor.CloseDropDown();
            }
        }

        private int lastSelectedIndex = -1;
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lastSelectedIndex == this.listBox1.SelectedIndex) return;
                
            if(lastSelectedIndex != 0) this.objects.Edited(lastSelectedIndex);
            lastSelectedIndex = this.listBox1.SelectedIndex;

            if (this.listBox1.SelectedIndex == 0)
            {
                this.buttonSet.Enabled = true;
                this.buttonRemove.Enabled = false;
            }
            else
            {
                this.buttonSet.Enabled = false;
                this.buttonRemove.Enabled = true;
            }

            this.propertyGrid1.SelectedObject = this.listBox1.SelectedItem;
            this.propertyGrid1.SelectedGridItemChanged += new SelectedGridItemChangedEventHandler(propertyGrid1_SelectedGridItemChanged);
        }

        void propertyGrid1_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
        {
            if (this.listBox1.SelectedIndex == 0) return;
            if (e.OldSelection == e.NewSelection) return;
            this.objects.Edited(this.listBox1.SelectedIndex);
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            object newObj = this.ListType.InvokeMember("", System.Reflection.BindingFlags.CreateInstance, null, null, null);
            this.objects.Insert(0, newObj);
            this.listBox1.SelectedIndex = 0;
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            this.objects.Remove(this.listBox1.SelectedItem);
        }
    }
}
