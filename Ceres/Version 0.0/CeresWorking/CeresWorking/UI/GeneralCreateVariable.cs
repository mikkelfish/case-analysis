//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;
//using System.Windows.Forms;
//using System.Reflection;
//using CeresBase.UI.MethodEditor;
//using CeresBase.Data;
//using CeresBase.IO.Controls;

//namespace CeresBase.UI
//{
//    public partial class GeneralCreateVariable : Form
//    {

//        public GeneralCreateVariable()
//        {
//            InitializeComponent();
//        }

//        private ConstructorInfo constructor;
//        private Type[][] types;
//        private MethodInfo method;

//        private void GeneralCreateVariable_Load(object sender, EventArgs e)
//        {
//            CeresBase.Plugins.CeresPluginBase[] plugins =
//                           CeresBase.General.Utilities.CreatePlugins<CeresBase.Plugins.CeresPluginBase>();
//            foreach (CeresBase.Plugins.CeresPluginBase plugin in plugins)
//            {
//                Type[] types = plugin.GetType().Assembly.GetTypes();
//                foreach (Type type in types)
//                {
//                    UI.MethodEditor.EditorInfoAttribute[] attrList;
//                    MethodInfo[] infos =
//                        CeresBase.General.Utilities.GetFunctionsWithAttribute<UI.MethodEditor.EditorInfoAttribute>(type, out attrList);
//                    if (infos == null) continue;

//                    for (int i = 0; i < infos.Length; i++)
//                    {
//                        MethodInfo info = infos[i];
//                        ListViewItem item = new ListViewItem();
//                        item.Name = info.Name;
//                        item.Text = item.Name;
//                        item.Tag = info;
//                        this.listViewMethods.Items.Add(item);
//                    }

//                }
//            }

//            foreach (VariableHeader header in VariableSource.AllVariablesKnown)
//            {
//                ListViewItem item = new ListViewItem(header.ToString());
//                item.Tag = header;
//                this.listViewVariables.Items.Add(item);
//            }
//        }

//        private void listViewMethods_ItemActivate(object sender, EventArgs e)
//        {
//            ListView view = (sender as ListView);
//            ListViewItem item = view.SelectedItems[0];
//            EditorInfoAttribute attr = (item.Tag as MethodInfo).GetCustomAttributes(typeof(EditorInfoAttribute), true)[0] as EditorInfoAttribute;
//            this.panel1.Controls.Clear();
//            int width = this.panel1.Width / attr.VariableDescriptions.Length - 10;
//            int height = this.panel1.Height - 25;
//            for (int i = 0; i < attr.VariableDescriptions.Length; i++ )
//            {
//                string desc = attr.VariableDescriptions[i];
//                ListBox box = new ListBox();
//                box.Width = width;
//                box.Height = height;
//                box.Location = new Point(i*width + 5, 25);
//                box.DragDrop += new DragEventHandler(this.dragDrop);
//                box.DragEnter += new DragEventHandler(this.dragEnter);
//                box.AllowDrop = true;
//                this.panel1.Controls.Add(box);
//                Label label = new Label();
//                label.Text = desc;
//                label.Location = new Point(i * width + 5, 5);
//                label.Width = 55;
//                label.Height = 15;
//                this.panel1.Controls.Add(label);
//            }
//        }

//        private void listViewVariables_ItemActivate(object sender, EventArgs e)
//        {
//            ListView view = sender as ListView;
//            view.DoDragDrop(view.SelectedItems[0].Tag, DragDropEffects.Copy);
//        }

//        private void dragEnter(object sender, DragEventArgs e)
//        {
//            string[] test = e.Data.GetFormats(true);

//            Type dataType = IO.IOFactory.CreateType(e.Data.GetFormats()[0]);
//            if (!dataType.IsSubclassOf(typeof(VariableHeader)) &&
//                typeof(VariableHeader) != dataType)
//            {
//                e.Effect = DragDropEffects.None;
//                return;
//            }
//            e.Effect = DragDropEffects.Copy;
//        }

//        private void dragDrop(object sender, DragEventArgs e)
//        {
//            VariableHeader var = (VariableHeader)e.Data.GetData(e.Data.GetFormats()[0]);
//            ListBox box = sender as ListBox;
//            box.Items.Add(var);
//        }

//        private void button1_Click(object sender, EventArgs e)
//        {
//            MethodAssociation assoc = new MethodAssociation();
//            Type[][] setTypes = new Type[this.panel1.Controls.Count / 2][];
//            int cur = 0;
//            for(int i = 0; i < this.panel1.Controls.Count; i++)
//            {
//                if (!(this.panel1.Controls[i] is ListBox))
//                {
//                    continue;
//                }
//                ListBox box = this.panel1.Controls[i] as ListBox;
//                List<Type> types = new List<Type>();
//                foreach (VariableHeader var in box.Items)
//                {
//                    if (!types.Contains(var.GetType()))
//                    {
//                        types.Add(var.GetType());
//                    }
//                }
//                setTypes[cur] = types.ToArray();
//                cur++;
//            }

//            //TODO TEMP
//            VariableHeader consheader = (this.panel1.Controls[0] as ListBox).Items[0] as VariableHeader;
//            ConstructorInfo[] constructors = consheader.GetType().GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
//            ConstructorInfo constructor = null;
//            foreach (ConstructorInfo cInfo in constructors)
//            {
//                object[] args = cInfo.GetCustomAttributes(typeof(UI.MethodEditor.EditorConstructorAttribute), true);
//                if (args == null || args.Length == 0) continue;
//                constructor = cInfo;
//            }

//            assoc.Build(setTypes, this.listViewMethods.SelectedItems[0].Tag as MethodInfo, constructor);
//            assoc.ShowDialog();
//            this.setupRequestInformation(constructor, setTypes, this.listViewMethods.SelectedItems[0].Tag as MethodInfo);
//            this.constructor = constructor;
//            this.method = this.listViewMethods.SelectedItems[0].Tag as MethodInfo;
//            this.types = setTypes;
//           // this.Close();
//        }

//        private void setupRequestInformation(ConstructorInfo constructor, Type[][] types, MethodInfo method)
//        {
//            List<Type> ptypes = new List<Type>();
//            List<string> category = new List<string>();
//            List<string> descriptions = new List<string>();
//            List<string> names = new List<string>();
//            List<object> startingValues = new List<object>();
//            for (int i = 0; i < 2; i++)
//            {
//                bool which = i != 0;

//                 VariableProcessingSetup.ProcessingAssociation assoc = 
//                    VariableProcessingSetup.GetAssociation(method, constructor, types, which);

//                foreach (ParameterInfo para in assoc.Parameters.Keys)
//                {
//                    if (assoc.Parameters[para].ReadOnly) continue;

//                    if(i == 0) category.Add("Function Parameters");
//                    else category.Add("Variable Parameters");

//                    ptypes.Add(para.ParameterType);
//                    names.Add(para.Name);
//                    startingValues.Add(assoc.Parameters[para].Value);

//                    descriptions.Add("");
//                }
//            }

//            this.requestInformationPropertyGrid1.SetInformationToCollect(ptypes.ToArray(), category.ToArray(),
//                names.ToArray(), descriptions.ToArray(), null, startingValues.ToArray());
//        }

//        private void button3_Click(object sender, EventArgs e)
//        {
//            VariableProcessingSetup.ProcessingAssociation meth = 
//                    VariableProcessingSetup.GetAssociation(method, constructor, types, false);
//            VariableProcessingSetup.ProcessingAssociation var = 
//                    VariableProcessingSetup.GetAssociation(method, constructor, types, true);
           
//            VariableHeader[][] headers = new VariableHeader[this.panel1.Controls.Count / 2][];
//            int cur = 0;
//            for(int i = 0; i < this.panel1.Controls.Count; i++)
//            {
//                if (!(this.panel1.Controls[i] is ListBox))
//                {
//                    continue;
//                }
//                ListBox box = this.panel1.Controls[i] as ListBox;
//                List<VariableHeader> hList = new List<VariableHeader>();
//                foreach (VariableHeader header in box.Items)
//                {
//                    hList.Add(header);
//                }
//                headers[cur] = hList.ToArray();
//                cur++;
//            };

//            VariableHeader[] make = 
//                VariableProcessingCreation.AddVariablesToCreate(meth, var, headers, this.requestInformationPropertyGrid1.Results);

//            VariableSource.AddVariableHeadersToMake(make);
//            foreach (VariableHeader header in make)
//            {
//                ListViewItem item = new ListViewItem(header.ToString());
//                item.Tag = header;
//                this.listViewVariables.Items.Add(item);
//            }
//        }


//        private void button4_Click(object sender, EventArgs e)
//        {
//            VariableProcessingSetup.WriteAssociations();
//            this.Close();
//        }
//    }
//}