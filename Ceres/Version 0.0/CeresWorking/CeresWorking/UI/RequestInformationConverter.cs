using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace CeresBase.UI
{
    class RequestInformationConverter : TypeConverter
    {
        object requestInfoItem = null;
        PropertyInfo prop;
        PropertyInfo link;

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            try
            {
                object converted = Convert.ChangeType(value, prop.PropertyType);
                prop.SetValue(requestInfoItem, converted, null);
                return requestInfoItem;
            }
            catch
            {
                return requestInfoItem;
            }
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (!(value.GetType().GetGenericTypeDefinition() == typeof(RequestInformationItem<>)))
            {
                return base.ConvertTo(context, culture, value, destinationType);
            }

            if (requestInfoItem == null)
            {
                this.requestInfoItem = value;
                this.prop = requestInfoItem.GetType().GetProperty("Tag");
                this.link = requestInfoItem.GetType().GetProperty("Link");
            }
            return value.ToString();   
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string)) return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return base.CanConvertTo(context, destinationType);
        }
    }
}
