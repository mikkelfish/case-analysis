using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Design;
using System.Drawing;
using System.Windows.Forms.Design;
using System.ComponentModel.Design;
using CeresBase.Data;

namespace CeresBase.UI
{
    public class RequestInformationEditor : UITypeEditor
    {
        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            RequestInformationPropertyGrid ownerGrid = (RequestInformationPropertyGrid)context.GetType().InvokeMember("ownerGrid", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.GetField,
                null, context, null);
            object curVal = value.GetType().InvokeMember("Tag", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.GetProperty,
                null, value, null);
            // Attempts to obtain an IWindowsFormsEditorService.
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (edSvc == null)
                return null;

            RequestInformationEditorUI ui = new RequestInformationEditorUI();

            throw new Exception("Um I took this out but it was needed");

            //ui.Setup(ownerGrid.Tag as VariableProcessingSetup.ProcessingAssociation, curVal, edSvc);

            edSvc.DropDownControl(ui);

            if (!ui.Canceled)
            {
                if (ui.Link == null)
                {
                    value.GetType().InvokeMember("Tag", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.SetProperty,
                        null, value, new object[] { ui.Default });
                }
                else
                {
                    value.GetType().InvokeMember("Link", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.SetProperty,
                        null, value, new object[] { ui.Link });
                }
                value.GetType().InvokeMember("ReadOnly", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.SetProperty,
                    null, value, new object[] { ui.ReadOnly });
            }
            return value;
        }

        public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        public override bool IsDropDownResizable
        {
            get
            {
                return false;
            }
        }
    }
}
