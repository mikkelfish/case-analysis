using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace CeresBase.UI
{
    public partial class BaseWindowing : Form
    {
        private bool isTopWindow;
        public bool IsTopWindow
        {
            get { return isTopWindow; }
        }

        public BaseWindowing()
            : this(true)
        {

        }

        public BaseWindowing(bool isTop)
        {
            InitializeComponent();
            this.isTopWindow = isTop;
            this.setupWindow();
        }

        /// <summary>
        /// Reflect upon the starting executable and snag its properties
        /// </summary>
        protected void MorphAttributesToReflectStartingExecutable()
        {
            //try
            //{
            //    Assembly assembly = SnapInHostingEngine.GetExecutingInstance().StartingExecutable;
            //    if (assembly != null)
            //    {
            //        // snag the name of the file minus path and extention and set it as the heading
            //        string filename = System.IO.Path.GetFileName(assembly.Location);
            //        filename = filename.Replace(System.IO.Path.GetExtension(assembly.Location), null);

            //        AssemblyAttributeReader reader = new AssemblyAttributeReader(assembly);

            //        //					// snag the company that made the assembly, and set it in the title
            //        //					System.Reflection.AssemblyCompanyAttribute[] companyAttributes = reader.GetAssemblyCompanyAttributes();
            //        //					if (companyAttributes != null)
            //        //						if (companyAttributes.Length > 0)
            //        //							if (companyAttributes[0].Company != null && companyAttributes[0].Company != string.Empty)
            //        //								this.Text = filename; // companyAttributes[0].Company + " " + filename;

            //        // snag the image from the assembly, it should be an executable so...
            //        Icon icon = ShellInformation.GetIconFromPath(assembly.Location, IconSizes.SmallIconSize, IconStyles.NormalIconStyle, FileAttributes.Normal);
            //        if (icon != null)
            //            this.Icon = icon;
            //    }
            //}
            //catch (System.Exception systemException)
            //{
            //    System.Diagnostics.Trace.WriteLine(systemException);

            //}
        }

        #region My Instance Manager Events

        /// <summary>
        /// Handles the event when a command line is received from another instance of ourself
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void OnCommandLineReceivedFromAnotherInstance(object sender, ApplicationInstanceManagerEventArgs e)
        //{
        //    try
        //    {
        //        // flash the window
        //        WindowFlasher.FlashWindow(this.Handle);
        //    }
        //    catch (System.Exception systemException)
        //    {
        //        System.Diagnostics.Trace.WriteLine(systemException);
        //    }
        //}

        #endregion

        private void setupWindow()
        {
            try
            {
                // assimilate the proprties of the starting executable
                this.MorphAttributesToReflectStartingExecutable();

                //// add ourself as a top level window to the hosting engine's application context
                //if (this.isTopWindow)
                //{
                //    SnapInHostingEngine.Instance.ApplicationContext.AddTopLevelWindow(this);
                //    // show ourself
                //    this.Show();
                //}
                //else SnapInHostingEngine.Instance.ApplicationContext.AddOtherWindow(this);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex);
            }

        }
    }
}