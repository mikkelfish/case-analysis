﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.ComponentModel;

namespace CeresBase.UI
{
    /// <summary>
    /// Interaction logic for ContentBox.xaml
    /// </summary>
    public partial class ContentBox : Window, INotifyPropertyChanged
    {
        public event EventHandler<ContentBoxEventArgs> ContentBoxClosed;

        private ContentBoxButton buttons = ContentBoxButton.OK;
        public ContentBoxButton Buttons 
        { 
            get 
            { 
                return buttons; 
            } 
            set 
            { 
                buttons = value;
                OnPropertyChanged("Buttons");
                OnPropertyChanged("ShowOK");
                OnPropertyChanged("ShowCancel");
                OnPropertyChanged("ShowYes");
                OnPropertyChanged("ShowNo");
                OnPropertyChanged("ShowAdd");
 
            } 
        }

        private void resize()
        {
            if (this.ContentElement != null)
            {
                if(!double.IsNaN( this.contentElement.Width))
                    this.Width = contentElement.Width + 10;
                if(!double.IsNaN(this.contentElement.Height))
                    this.Height = contentElement.Height + 56;
            }
        }

        private System.Windows.HorizontalAlignment buttonAlignment = System.Windows.HorizontalAlignment.Center;
        public System.Windows.HorizontalAlignment ButtonAlignment
        {
            get
            {
                return buttonAlignment;
            }
            set
            {
                buttonAlignment = value;
                OnPropertyChanged("ButtonAlignment");
            }
        }

        public bool ShowOK
        {
            get
            {
                if (Buttons == ContentBoxButton.OK || Buttons == ContentBoxButton.OKCancel || Buttons == ContentBoxButton.OKAddCancel)
                    return true;
                return false;
            }
        }

        public bool ShowCancel
        {
            get
            {
                if (Buttons == ContentBoxButton.OKCancel || Buttons == ContentBoxButton.YesNoCancel || Buttons == ContentBoxButton.OKAddCancel)
                    return true;
                return false;
            }
        }
        
        public bool ShowYes
        {
            get
            {
                if (Buttons == ContentBoxButton.YesNo || Buttons == ContentBoxButton.YesNoCancel)
                    return true;
                return false;
            }
        }
        
        public bool ShowNo 
        { 
            get 
            {
                if (Buttons == ContentBoxButton.YesNo || Buttons == ContentBoxButton.YesNoCancel)
                    return true;
                return false;
            } 
        }

        public bool ShowAdd
        {
            get
            {
                if (Buttons == ContentBoxButton.OKAddCancel)
                    return true;
                return false;
            }
        }

        private FrameworkElement contentElement;
        public FrameworkElement ContentElement
        {
            get
            {
                return contentElement;
            }
            set
            {
                contentElement = value;                
                this.Display.Content = this.ContentElement;

                this.resize();

            }
        }

        public ContentBox()
        {
            InitializeComponent();
            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(this);
            
        }

       

        //static public void Show(FrameworkElement content)
        //{
        //    ContentBox CB = new ContentBox();
        //    System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(CB);
        //    CB.Buttons = MessageBoxButton.OK;
        //    CB.MainGrid.Children.Add(content);
        //    Grid.SetColumn(content, 2);
        //    Grid.SetRow(content, 2);
        //    CB.DisplayedContent = content;
        //    CB.ShowDialog();
        //}

        //static public void Show(FrameworkElement content, string title)
        //{
        //    ContentBox CB = new ContentBox();
        //    System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(CB);
        //    CB.Buttons = MessageBoxButton.OK;
        //    CB.MainGrid.Children.Add(content);
        //    Grid.SetColumn(content, 2);
        //    Grid.SetRow(content, 2);
        //    CB.TitleString = title;
        //    CB.DisplayedContent = content;
        //    CB.ShowDialog();
        //}

        //static public void Show(FrameworkElement content, MessageBoxButton showButtons)
        //{
        //    ContentBox CB = new ContentBox();
        //    System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(CB);
        //    CB.Buttons = showButtons;
        //    CB.MainGrid.Children.Add(content);
        //    Grid.SetColumn(content, 2);
        //    Grid.SetRow(content, 2);
        //    CB.DisplayedContent = content;
        //    CB.ShowDialog();
        //}

        //static public void Show(FrameworkElement content, MessageBoxButton showButtons, string title)
        //{
        //    ContentBox CB = new ContentBox();
        //    System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(CB);
        //    CB.Buttons = showButtons;
        //    CB.MainGrid.Children.Add(content);
        //    Grid.SetColumn(content, 2);
        //    Grid.SetRow(content, 2);
        //    CB.TitleString = title;
        //    CB.DisplayedContent = content;
        //    CB.ShowDialog();
        //}

        //static public void Show(FrameworkElement content, MessageBoxButton showButtons, bool isDialog)
        //{
        //    ContentBox CB = new ContentBox();
        //    System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(CB);
        //    CB.Buttons = showButtons;
        //    CB.MainGrid.Children.Add(content);
        //    Grid.SetColumn(content, 2);
        //    Grid.SetRow(content, 2);
        //    CB.DisplayedContent = content;
        //    if (isDialog)
        //        CB.ShowDialog();
        //    else
        //        CB.Show();
        //}

        //static public void Show(FrameworkElement content, MessageBoxButton showButtons, bool isDialog, string title)
        //{
        //    ContentBox CB = new ContentBox();
        //    System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(CB);
        //    CB.Buttons = showButtons;
        //    CB.MainGrid.Children.Add(content);
        //    Grid.SetColumn(content, 2);
        //    Grid.SetRow(content, 2);
        //    CB.TitleString = title;
        //    CB.DisplayedContent = content;
        //    if (isDialog)
        //        CB.ShowDialog();
        //    else
        //        CB.Show();
        //}

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            ContentBoxEventArgs args = new ContentBoxEventArgs(ContentBoxResult.Cancel);
            if (ContentBoxClosed != null) 
                ContentBoxClosed(this, args);
            if (args.Cancel != true)            
                this.Close();
        }

        private void No_Click(object sender, RoutedEventArgs e)
        {
            ContentBoxEventArgs args = new ContentBoxEventArgs(ContentBoxResult.No);
            if (ContentBoxClosed != null) 
                ContentBoxClosed(this, args);
            if (args.Cancel != true)            
                this.Close();
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            ContentBoxEventArgs args = new ContentBoxEventArgs(ContentBoxResult.Yes);
            if (ContentBoxClosed != null) 
                ContentBoxClosed(this, args);
            if (args.Cancel != true)
                this.Close();
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            ContentBoxEventArgs args = new ContentBoxEventArgs(ContentBoxResult.OK);
            if (ContentBoxClosed != null)
                ContentBoxClosed(this, args);
            
            if (args.Cancel != true)
                this.Close();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            ContentBoxEventArgs args = new ContentBoxEventArgs(ContentBoxResult.Add);
            if (ContentBoxClosed != null)
                ContentBoxClosed(this, args);

            if(args.Cancel != true)
                 this.Close();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        private void TitleBacking_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
            
        }

        private void TitleText_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private enum ResizeEnum
	    {
    	    Top, Left, Right, Bottom, TopLeft, TopRight, BottomLeft, BottomRight
	    }

        private bool resizeDragActive = false;
        private Point origPosition = new Point();
        private Point origWindowPosition = new Point();
        private Point origWindowSize = new Point();

        private void ResizeBegin(object sender, MouseButtonEventArgs e, ResizeEnum type)
        {
            resizeDragActive = true;

            origPosition = PointToScreen(e.GetPosition(this));

            origWindowPosition.X = this.Left;
            origWindowPosition.Y = this.Top;
            origWindowSize.X = this.ActualWidth;
            origWindowSize.Y = this.ActualHeight;

            FrameworkElement elem = sender as FrameworkElement;

            elem.CaptureMouse();
        }

        private void ResizeActive(object sender, System.Windows.Input.MouseEventArgs e, ResizeEnum type)
        {
            FrameworkElement elem = sender as FrameworkElement;

            if (!resizeDragActive) return;

            Point newPos = PointToScreen(e.GetPosition(this));

            Point diff = new Point(newPos.X - origPosition.X, newPos.Y - origPosition.Y);

            double newHeight = 0;
            double newWidth = 0;
            double newTop = origWindowPosition.Y;
            double newLeft = origWindowPosition.X;

            switch (type)
            {
                case ResizeEnum.Top:
                    newTop = origWindowPosition.Y + diff.Y;
                    newHeight = origWindowSize.Y - diff.Y;
                    //newTop = (this.ActualHeight - newHeight) + this.Top;
                    break;
                case ResizeEnum.Left:
                    newWidth = origWindowSize.X - diff.X;
                    newLeft = origWindowPosition.X + diff.X;
                    //newLeft = (this.ActualWidth - newWidth) + this.Left;
                    break;
                case ResizeEnum.Right:
                    newWidth = origWindowSize.X + diff.X;
                    break;
                case ResizeEnum.Bottom:
                    newHeight = origWindowSize.Y + diff.Y;
                    break;
                case ResizeEnum.TopLeft:
                    newHeight = origWindowSize.Y - diff.Y;
                    newWidth = origWindowSize.X - diff.X;
                    newTop = (this.ActualHeight - newHeight) + this.Top;
                    newLeft = (this.ActualWidth - newWidth) + this.Left;
                    break;
                case ResizeEnum.TopRight:
                    newHeight = origWindowSize.Y - diff.Y;
                    newWidth = origWindowSize.X + diff.X;
                    newTop = (this.ActualHeight - newHeight) + this.Top;
                    break;
                case ResizeEnum.BottomLeft:
                    newHeight = origWindowSize.Y + diff.Y;
                    newWidth = origWindowSize.X - diff.X;
                    newLeft = (this.ActualWidth - newWidth) + this.Left;
                    break;
                case ResizeEnum.BottomRight:
                    newHeight = origWindowSize.Y + diff.Y;
                    newWidth = origWindowSize.X + diff.X;
                    break;
                default:
                    break;
            }

            if (newHeight > 0)
                this.Height = newHeight;
            if (newWidth > 0)
                this.Width = newWidth;

            this.Left = newLeft;
            this.Top = newTop;
        }

        private void ResizeEnd(object sender, MouseButtonEventArgs e, ResizeEnum type)
        {
            resizeDragActive = false;

            FrameworkElement elem = sender as FrameworkElement;

            elem.ReleaseMouseCapture();
        }

        #region Big Batch of Events, yeehaw.
        private void TopLeftSide_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ResizeActive(sender, e , ResizeEnum.TopLeft);
        }

        private void TopLeftSide_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ResizeBegin(sender, e, ResizeEnum.TopLeft);
        }

        private void TopLeftSide_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ResizeEnd(sender, e, ResizeEnum.TopLeft);
        }

        private void TopRightSide_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ResizeActive(sender, e, ResizeEnum.TopRight);
        }

        private void TopRightSide_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ResizeBegin(sender, e, ResizeEnum.TopRight);
        }

        private void TopRightSide_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ResizeEnd(sender, e, ResizeEnum.TopRight);
        }

        private void BottomLeftSide_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ResizeActive(sender, e, ResizeEnum.BottomLeft);
        }

        private void BottomLeftSide_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ResizeBegin(sender, e, ResizeEnum.BottomLeft);
        }

        private void BottomLeftSide_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ResizeEnd(sender, e, ResizeEnum.BottomLeft);
        }

        private void BottomRightSide_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ResizeActive(sender, e, ResizeEnum.BottomRight);
        }

        private void BottomRightSide_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ResizeBegin(sender, e, ResizeEnum.BottomRight);
        }

        private void BottomRightSide_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ResizeEnd(sender, e, ResizeEnum.BottomRight);
        }

        private void BottomSide_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ResizeActive(sender, e, ResizeEnum.Bottom);
        }

        private void BottomSide_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ResizeBegin(sender, e, ResizeEnum.Bottom);
        }

        private void BottomSide_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ResizeEnd(sender, e, ResizeEnum.Bottom);
        }

        private void LeftSide_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ResizeActive(sender, e, ResizeEnum.Left);
        }

        private void LeftSide_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ResizeBegin(sender, e, ResizeEnum.Left);
        }

        private void LeftSide_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ResizeEnd(sender, e, ResizeEnum.Left);
        }

        private void RightSide_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ResizeActive(sender, e, ResizeEnum.Right);
        }

        private void RightSide_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ResizeBegin(sender, e, ResizeEnum.Right);
        }

        private void RightSide_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ResizeEnd(sender, e, ResizeEnum.Right);
        }

        private void TopSide_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ResizeActive(sender, e, ResizeEnum.Top);
        }

        private void TopSide_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ResizeBegin(sender, e, ResizeEnum.Top);
        }

        private void TopSide_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ResizeEnd(sender, e, ResizeEnum.Top);
        }
        #endregion
    }

    public class ContentBoxEventArgs : EventArgs
    {
        private ContentBoxResult result;
        public ContentBoxResult Result
        {
            get
            {
                return result;
            }
        }

        public bool Cancel { get; set; }

        public ContentBoxEventArgs(ContentBoxResult result)
        {
            this.result = result;
        }
    }

    public enum ContentBoxResult
    {
        Cancel,
        No,
        None,
        OK,
        Yes,
        Add
    }

    public enum ContentBoxButton
    {
        OK,
        OKCancel,
        YesNo,
        YesNoCancel,
        OKAddCancel
    }


}
