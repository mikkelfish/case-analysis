using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Windows.Forms.Design;
using CeresBase.Data;

namespace CeresBase.UI
{
    public partial class RequestInformationEditorUI : UserControl
    {
        private IWindowsFormsEditorService editor;

        public bool ReadOnly
        {
            get
            {
                return (bool)this.requestInformationPropertyGrid1.Results["ReadOnly"];
            }
        }

        public object Default
        {
            get
            {
                return this.requestInformationPropertyGrid1.Results["Value"];
            }
        }

        private object link;
        public object Link
        {
            get
            {
                if (this.lastSel == null) return null;
                return this.link;
            }
        }

        private bool canceled = false;
        public bool Canceled
        {
            get { return canceled = false; }
        }


        private TreeNode lastSel = null;

        //public bool Setup(VariableProcessingSetup.ProcessingAssociation association, object target, IWindowsFormsEditorService editor)
        //{
        //    //this.editor = editor;
        //    //this.requestInformationPropertyGrid1.SetInformationToCollect(
        //    //    new Type[] { target.GetType(), typeof(bool) }, null, new string[] { "Value", "ReadOnly" },
        //    //    new string[] { "Test", "Should this value be readonly once its default is set?" },
        //    //    null, new object[] { target, false });
        //    //this.requestInformationPropertyGrid1.SelectedGridItemChanged += new SelectedGridItemChangedEventHandler(requestInformationPropertyGrid1_SelectedGridItemChanged);
        //    //if (!association.MethodToVar)
        //    //{
        //    //    for (int i = 0; i < association.Attribute.VariableDescriptions.Length; i++)
        //    //    {
        //    //        string varDesc = association.Attribute.VariableDescriptions[i];
        //    //        TreeNode root = this.treeView1.Nodes.Add(varDesc, varDesc);
        //    //        for (int j = 0; j < association.VarTypes.GetLength(i); j++)
        //    //        {
        //    //            Type type = association.VarTypes[i][j];
        //    //            TreeNode node = root.Nodes.Add(type.Name, type.Name);
        //    //            PropertyInfo[] props = type.GetProperties();
        //    //            foreach (PropertyInfo prop in props)
        //    //            {
        //    //                if (!General.Utilities.CanConvertStrictly(prop.PropertyType, target.GetType()))
        //    //                    continue;
        //    //                TreeNode child = node.Nodes.Add(prop.Name, prop.Name);
        //    //                child.Tag = prop;
        //    //            }
        //    //            if (node.Nodes.Count == 0)
        //    //                root.Nodes.Remove(node);
        //    //        }
        //    //        root.ExpandAll();
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    ParameterInfo[] paras = association.Method.GetParameters();
        //    //    foreach (ParameterInfo pInfo in paras)
        //    //    {
        //    //        if (!General.Utilities.CanConvertStrictly(pInfo.ParameterType, target.GetType()))
        //    //            continue;
        //    //        TreeNode node = this.treeView1.Nodes.Add(pInfo.Name, pInfo.Name);
        //    //        node.Tag = pInfo;
        //    //    }
        //    //}

        //    //if (this.treeView1.Nodes.Count == 0)
        //    //{
        //    //    this.treeView1.Nodes.Add("No valid links.");
        //    //    this.treeView1.Enabled = false;
        //    //    return false;
        //    //}

        //    return true;
        //}

        void requestInformationPropertyGrid1_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
        {
            RequestInformation request = sender as RequestInformation;
            if (requestInformationPropertyGrid1.SelectedGridItem.Label == "Value")
            {
                if (this.lastSel != null)
                {
                    this.lastSel.BackColor = Color.White;
                    this.lastSel = null;
                }
            }
        }

        public RequestInformationEditorUI()
        {
            InitializeComponent();
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode node = e.Node;
            if (node == null || node.Tag == null)
            {
                this.link = null;
                return;
            }

            if (node.Tag is PropertyInfo)
            {
                object[] attrs = (node.Tag as PropertyInfo).GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    this.textBox1.Text = (attrs[0] as DescriptionAttribute).Description;
                }
            }
            else if (node.Tag is ParameterInfo)
            {
                object[] attrs = (node.Tag as ParameterInfo).GetCustomAttributes(typeof(UI.MethodEditor.EditorParamAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    this.textBox1.Text = (attrs[0] as UI.MethodEditor.EditorParamAttribute).Description;
                }
            }
            this.link = node.Tag;
            node.BackColor = Color.LightGreen;
            //   node.BackColor = Color.Black;
            if (this.lastSel != null)
            {
                this.lastSel.BackColor = Color.White;
            }
            this.lastSel = node;

        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.editor.CloseDropDown();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.canceled = true;
            this.editor.CloseDropDown();
        }
    }
}
