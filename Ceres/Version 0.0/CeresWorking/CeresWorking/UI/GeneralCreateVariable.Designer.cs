//namespace CeresBase.UI
//{
//    partial class GeneralCreateVariable
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.listViewVariables = new System.Windows.Forms.ListView();
//            this.label1 = new System.Windows.Forms.Label();
//            this.label2 = new System.Windows.Forms.Label();
//            this.label3 = new System.Windows.Forms.Label();
//            this.button1 = new System.Windows.Forms.Button();
//            this.listViewMethods = new System.Windows.Forms.ListView();
//            this.panel1 = new System.Windows.Forms.Panel();
//            this.button3 = new System.Windows.Forms.Button();
//            this.requestInformationPropertyGrid1 = new CeresBase.UI.RequestInformationPropertyGrid();
//            this.buttonOK = new System.Windows.Forms.Button();
//            this.SuspendLayout();
//            // 
//            // listViewVariables
//            // 
//            this.listViewVariables.Location = new System.Drawing.Point(12, 255);
//            this.listViewVariables.Name = "listViewVariables";
//            this.listViewVariables.Size = new System.Drawing.Size(362, 242);
//            this.listViewVariables.TabIndex = 2;
//            this.listViewVariables.UseCompatibleStateImageBehavior = false;
//            this.listViewVariables.View = System.Windows.Forms.View.List;
//            this.listViewVariables.ItemActivate += new System.EventHandler(this.listViewVariables_ItemActivate);
//            // 
//            // label1
//            // 
//            this.label1.AutoSize = true;
//            this.label1.Location = new System.Drawing.Point(9, 239);
//            this.label1.Name = "label1";
//            this.label1.Size = new System.Drawing.Size(89, 13);
//            this.label1.TabIndex = 3;
//            this.label1.Text = "Loaded Variables";
//            // 
//            // label2
//            // 
//            this.label2.AutoSize = true;
//            this.label2.Location = new System.Drawing.Point(8, 8);
//            this.label2.Name = "label2";
//            this.label2.Size = new System.Drawing.Size(90, 13);
//            this.label2.TabIndex = 4;
//            this.label2.Text = "Creation Methods";
//            // 
//            // label3
//            // 
//            this.label3.AutoSize = true;
//            this.label3.Location = new System.Drawing.Point(218, 9);
//            this.label3.Name = "label3";
//            this.label3.Size = new System.Drawing.Size(85, 13);
//            this.label3.TabIndex = 5;
//            this.label3.Text = "Creation Targets";
//            // 
//            // button1
//            // 
//            this.button1.Location = new System.Drawing.Point(428, 503);
//            this.button1.Name = "button1";
//            this.button1.Size = new System.Drawing.Size(94, 23);
//            this.button1.TabIndex = 7;
//            this.button1.Text = "Enter Info";
//            this.button1.UseVisualStyleBackColor = true;
//            this.button1.Click += new System.EventHandler(this.button1_Click);
//            // 
//            // listViewMethods
//            // 
//            this.listViewMethods.Location = new System.Drawing.Point(12, 25);
//            this.listViewMethods.Name = "listViewMethods";
//            this.listViewMethods.Size = new System.Drawing.Size(200, 211);
//            this.listViewMethods.TabIndex = 9;
//            this.listViewMethods.UseCompatibleStateImageBehavior = false;
//            this.listViewMethods.View = System.Windows.Forms.View.List;
//            this.listViewMethods.ItemActivate += new System.EventHandler(this.listViewMethods_ItemActivate);
//            // 
//            // panel1
//            // 
//            this.panel1.AllowDrop = true;
//            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
//            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
//            this.panel1.Location = new System.Drawing.Point(221, 25);
//            this.panel1.Name = "panel1";
//            this.panel1.Size = new System.Drawing.Size(562, 211);
//            this.panel1.TabIndex = 10;
//            // 
//            // button3
//            // 
//            this.button3.Location = new System.Drawing.Point(328, 503);
//            this.button3.Name = "button3";
//            this.button3.Size = new System.Drawing.Size(94, 23);
//            this.button3.TabIndex = 11;
//            this.button3.Text = "Add To Queue";
//            this.button3.UseVisualStyleBackColor = true;
//            this.button3.Click += new System.EventHandler(this.button3_Click);
//            // 
//            // requestInformationPropertyGrid1
//            // 
//            this.requestInformationPropertyGrid1.Location = new System.Drawing.Point(380, 255);
//            this.requestInformationPropertyGrid1.Name = "requestInformationPropertyGrid1";
//            this.requestInformationPropertyGrid1.Size = new System.Drawing.Size(403, 242);
//            this.requestInformationPropertyGrid1.TabIndex = 6;
//            // 
//            // buttonOK
//            // 
//            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
//            this.buttonOK.Location = new System.Drawing.Point(228, 503);
//            this.buttonOK.Name = "buttonOK";
//            this.buttonOK.Size = new System.Drawing.Size(94, 23);
//            this.buttonOK.TabIndex = 12;
//            this.buttonOK.Text = "OK";
//            this.buttonOK.UseVisualStyleBackColor = true;
//            this.buttonOK.Click += new System.EventHandler(this.button4_Click);
//            // 
//            // GeneralCreateVariable
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.ClientSize = new System.Drawing.Size(795, 536);
//            this.Controls.Add(this.buttonOK);
//            this.Controls.Add(this.button3);
//            this.Controls.Add(this.panel1);
//            this.Controls.Add(this.listViewMethods);
//            this.Controls.Add(this.button1);
//            this.Controls.Add(this.requestInformationPropertyGrid1);
//            this.Controls.Add(this.label3);
//            this.Controls.Add(this.label2);
//            this.Controls.Add(this.label1);
//            this.Controls.Add(this.listViewVariables);
//            this.Name = "GeneralCreateVariable";
//            this.Text = "GeneralCreateVariable";
//            this.Load += new System.EventHandler(this.GeneralCreateVariable_Load);
//            this.ResumeLayout(false);
//            this.PerformLayout();

//        }

//        #endregion

//        private System.Windows.Forms.ListView listViewVariables;
//        private System.Windows.Forms.Label label1;
//        private System.Windows.Forms.Label label2;
//        private System.Windows.Forms.Label label3;
//        private RequestInformationPropertyGrid requestInformationPropertyGrid1;
//        private System.Windows.Forms.Button button1;
//        private System.Windows.Forms.ListView listViewMethods;
//        private System.Windows.Forms.Panel panel1;
//        private System.Windows.Forms.Button button3;
//        private System.Windows.Forms.Button buttonOK;
//    }
//}