﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.UI
{
    public enum GraphingType
    {
        Line = 0,
        Scatter = 1,
        Column
    }
}
