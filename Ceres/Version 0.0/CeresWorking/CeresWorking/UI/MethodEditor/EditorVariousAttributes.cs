using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.UI.MethodEditor
{
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public class EditorInfoNumberVarsAttribute : Attribute
    {
        private int minimum;
        public int Minimum
        {
            get { return minimum; }
        }

        private int maximum;
        public int Maximum
        {
            get { return maximum; }
        }

        public EditorInfoNumberVarsAttribute(int minimum, int maximum)
        {
            this.minimum = minimum;
            this.maximum = maximum;
        }	
    }
}
