using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.UI.MethodEditor
{
    public class EditorFunctionAttribute : EditorInfoAttribute
    {
        private string length;
        public string GetLengthFunction
        {
            get { return length; }
        }

        private string numVarsFunction;
        public string GetNumberVarsCreatedFunction
        {
            get
            {
                return numVarsFunction;
            }
        }

        public EditorFunctionAttribute(string description, string lengthFunction, string numVarsFunction,  string callback)
            : base(description, callback)
        {
            this.length = lengthFunction;
            this.numVarsFunction = numVarsFunction;
        }

        public EditorFunctionAttribute(string description, string lengthFunction,  string numVarsFunction, string callback,
            string finalize) : base(description, callback, finalize)
        {
            this.length = lengthFunction;
            this.numVarsFunction = numVarsFunction;
        }
    }
}
