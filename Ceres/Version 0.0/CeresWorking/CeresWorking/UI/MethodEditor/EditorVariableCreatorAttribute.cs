using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.UI.MethodEditor
{
    public class EditorVariableCreatorAttribute : EditorInfoAttribute
    {
        public EditorVariableCreatorAttribute(string description, string callback): base(description, callback)
        {

        }

        public EditorVariableCreatorAttribute(string description, string callback, string finalize)
            : base(description, callback, finalize)
        {

        }
    }
}
