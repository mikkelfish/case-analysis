using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.UI.MethodEditor
{
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public abstract class EditorInfoAttribute : Attribute
    {
        private string description;
        public string Description
        {
            get { return description; }
        }

        private string callback;
        public string Callback
        {
            get { return callback; }
        }

        private string finalize;
        public string Finalize
        {
            get { return finalize; }
        }

	
        private bool supportsStrided;
        public bool SupportsStrides
        {
            get { return supportsStrided; }
            set { supportsStrided = value; }
        }

     
        private Type[] constrictToTypes;
        public Type[] LimitToTypes
        {
            get { return constrictToTypes; }
            set { this.constrictToTypes = value; }
        }

        private string[] variableDescriptions;
        public string[] VariableDescriptions
        {
            get { return variableDescriptions; }
        }

        public int NumInputVariables
        {
            get { return this.variableDescriptions.Length; }
        }
	

        public EditorInfoAttribute(string description, string callback)
            : this(description, callback, "")
        {
        }

        public EditorInfoAttribute(string description, string callback, string finalize, params string[] variableDescriptions)
        {
            this.description = description;
            this.callback = callback;
            this.finalize = finalize;
            this.variableDescriptions = variableDescriptions;
            if (this.variableDescriptions == null || this.VariableDescriptions.Length == 0)
            {
                this.variableDescriptions = new string[] { "Variables" };
            }
        }
    }
}
