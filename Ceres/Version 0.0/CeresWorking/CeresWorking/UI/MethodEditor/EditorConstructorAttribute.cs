using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.UI.MethodEditor
{
    [global::System.AttributeUsage(AttributeTargets.Constructor, Inherited = false, AllowMultiple = false)]
    public sealed class EditorConstructorAttribute : Attribute
    {
	
    }
}
