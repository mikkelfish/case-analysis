using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace CeresBase.UI.MethodEditor
{
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public class EditorParamAttribute : Attribute
    {
        public const string VariableDescription = "Variable Name";
        public const string UnitsDescription = "Units Name";
        public const string ExperimentDescription = "Experiment Name";

        private string description;
        public string Description
        {
            get { return description; }
        }

        private Type editorType;
        public Type EditorType
        {
            get 
            { 
                return editorType; 
            }
            set
            {
                this.editorType = value;
            }
        }

        private object[] args;
        public object[] Arguments
        {
            get { return args; }
        }

        private bool useDefault;
        public bool UseDefault
        {
            get { return useDefault; }
            set { useDefault = value; }
        }

        private bool readOnly;
        public bool ReadOnly
        {
            get { return readOnly; }
            set
            {
                this.readOnly = value;
            }
        }
	
	
        public EditorParamAttribute(string description, params object[] args)
        {
            this.useDefault = true;
            this.description = description;
            this.args = args;
        }
	
    }
}
