using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CeresBase.UI
{
    public partial class RequestInformation : Form
    {
        public RequestInformation()
        {
            InitializeComponent();

            this.Grid.KeyHandler += new KeyEventHandler(Grid_KeyHandler);
        }

        void Grid_KeyHandler(object sender, KeyEventArgs e)
        {
            if (e.Handled == true) return;

            if (e.KeyCode == Keys.C && e.Control)
            {
                this.buttonCancel_Click(this, new EventArgs());
            }
            else if (e.KeyCode == Keys.S && e.Control)
            {
                this.buttonOK_Click(this, new EventArgs());
            }

        }

        public Dictionary<string, object> Results
        {
            get { return this.requestInformationPropertyGrid1.Results; }
        }

        public RequestInformationPropertyGrid Grid
        {
            get
            {
                return this.requestInformationPropertyGrid1;
            }
        }


        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void RequestInformation_Shown(object sender, EventArgs e)
        {
            this.Grid.FocusFirstItem();
        }

        private void RequestInformation_Activated(object sender, EventArgs e)
        {
            //this.Grid.FocusFirstItem();
        }
    }
}