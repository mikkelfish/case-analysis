using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Reflection.Emit;
using System.Drawing.Design;
using CeresBase.Data;

namespace CeresBase.UI
{
    public partial class RequestInformationPropertyGrid : PropertyGrid
    {
        private static ModuleBuilder builder;
        private static int id = 0;
        private static readonly object lockable = new object();
        private static Dictionary<int, Type> constructedTypes;
        private static Dictionary<string, Type> enumTypes = new Dictionary<string, Type>();

        private GridItemCollection gridItems;
        private int rowHeight = 0;
        private int labelWidth = 0;
        private object view = null;
        //private object[] pInfos;


        private readonly object lockableInstance = new object();



        private string[] names;
        //public string[] Names
        //{
        //    get
        //    {
        //        return this.names;
        //    }
        //}

        public Dictionary<string, object> Results
        {
            get
            {
                Dictionary<string, object> results = new Dictionary<string, object>();

                for (int i = 0; i < this.names.Length; i++)
                {
                    object obj = this.SelectedObject.GetType().InvokeMember(names[i], BindingFlags.Public |
                         BindingFlags.Instance | BindingFlags.GetField, null, this.SelectedObject, null);

                    if (obj != null)
                    {
                        if (obj.GetType().IsGenericType)
                        {
                            if (obj.GetType().GetGenericTypeDefinition() == typeof(RequestInformationItem<>))
                            {
                                obj = obj.GetType().InvokeMember("Output", BindingFlags.Instance | BindingFlags.Public |
                                    BindingFlags.GetProperty, null, obj, null);
                            }
                        }
                    }
                    results.Add(names[i], obj);

                }
                return results;
            }
        }

        public Dictionary<string, bool> ReadOnlyDefaults
        {
            get
            {
                Dictionary<string, bool> readOnlys = new Dictionary<string, bool>();
                object[] tempObjects = new object[this.names.Length];
                for (int i = 0; i < this.names.Length; i++)
                {
                    tempObjects[i] = this.SelectedObject.GetType().InvokeMember(names[i], BindingFlags.Public |
                        BindingFlags.Instance | BindingFlags.GetField, null, this.SelectedObject, null);

                    if (tempObjects[i].GetType().IsGenericType)
                    {
                        if (tempObjects[i].GetType().GetGenericTypeDefinition() == typeof(RequestInformationItem<>))
                        {
                            readOnlys[this.names[i]] = (bool)tempObjects[i].GetType().InvokeMember("ReadOnly", BindingFlags.Instance | BindingFlags.Public |
                                BindingFlags.GetProperty, null, tempObjects[i], null);
                        }
                        else readOnlys[this.names[i]] = false;
                    }
                    else readOnlys[this.names[i]] = false;
                }
                return readOnlys;
            }
        }

        private static int getHashKey(Type[] types, string[] names, string[] descriptions, bool possibleAssignTypes)
        {
            string str = "";
            for (int i = 0; i < types.Length; i++)
            {
                str += types[i].Name + names[i] + descriptions[i] + possibleAssignTypes.ToString();
            }
            return str.GetHashCode();
        }

        private static object createObject(Type[] types, string[] categories, string[] names, string[] descriptions, Type[] editors,
            bool possibleAssignTypes, object[] beginningValues)
        {
            string name = "RequestInformationType";
            int hashCode = getHashKey(types, names, descriptions, possibleAssignTypes);
            Type constructedType = null;
            lock (lockable)
            {
                if (constructedTypes.ContainsKey(hashCode))
                {
                    constructedType = constructedTypes[hashCode];
                }

                name = name + id.ToString();
                id++;
            }

            if (constructedType == null)
            {
                #region Create Type

                TypeBuilder ivTypeBld = builder.DefineType(name, TypeAttributes.Public);
                for (int i = 0; i < types.Length; i++)
                {
                    Type requestInfoType = typeof(RequestInformationItem<>).MakeGenericType(types[i]);
                    if (!possibleAssignTypes) requestInfoType = types[i];

                    FieldBuilder field = ivTypeBld.DefineField(names[i], requestInfoType, FieldAttributes.Public);
                    string propertyName = char.ToUpper(names[i][0]) + names[i].Substring(1);
                    PropertyBuilder prop = ivTypeBld.DefineProperty(propertyName, System.Reflection.PropertyAttributes.None,
                        requestInfoType, null);


                    if (descriptions != null)
                    {
                        if (descriptions[i] != null)
                        {
                            ConstructorInfo cInfo = typeof(System.ComponentModel.DescriptionAttribute).GetConstructor(new Type[] { typeof(string) });
                            CustomAttributeBuilder propDesc = new CustomAttributeBuilder(cInfo, new object[] { descriptions[i] });
                            prop.SetCustomAttribute(propDesc);
                        }
                    }

                    if (categories != null)
                    {
                        if (categories[i] != null)
                        {
                            ConstructorInfo catInfo = typeof(System.ComponentModel.CategoryAttribute).GetConstructor(new Type[] { typeof(string) });
                            CustomAttributeBuilder catDesc = new CustomAttributeBuilder(catInfo, new object[] { categories[i] });
                            prop.SetCustomAttribute(catDesc);
                        }
                    }


                    if (editors != null && editors[i] != null)
                    {
                        ConstructorInfo eInfo = typeof(System.ComponentModel.EditorAttribute).GetConstructor(new Type[] { typeof(Type), typeof(Type) });
                        CustomAttributeBuilder eDesc = new CustomAttributeBuilder(eInfo, new object[] { editors[i], typeof(UITypeEditor) });
                        prop.SetCustomAttribute(eDesc);
                    }
                    else
                    {
                        if (possibleAssignTypes)
                        {
                            ConstructorInfo cInfo = typeof(System.ComponentModel.TypeConverterAttribute).GetConstructor(new Type[] { typeof(Type) });
                            CustomAttributeBuilder propDesc = new CustomAttributeBuilder(cInfo, new object[] { typeof(RequestInformationConverter) });
                            prop.SetCustomAttribute(propDesc);

                            ConstructorInfo eInfo = typeof(System.ComponentModel.EditorAttribute).GetConstructor(new Type[] { typeof(Type), typeof(Type) });
                            CustomAttributeBuilder eDesc = new CustomAttributeBuilder(eInfo, new object[] { typeof(UI.RequestInformationEditor), typeof(UITypeEditor) });
                            prop.SetCustomAttribute(eDesc);
                        }
                    }


                    // Define the 'get_Greeting' method.
                    MethodBuilder getMethod = ivTypeBld.DefineMethod("get_" + propertyName,
                       MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName,
                       requestInfoType, null);
                    // Generate IL code for 'get_Greeting' method.
                    ILGenerator methodIL = getMethod.GetILGenerator();
                    methodIL.Emit(OpCodes.Ldarg_0);
                    methodIL.Emit(OpCodes.Ldfld, field);
                    methodIL.Emit(OpCodes.Ret);
                    prop.SetGetMethod(getMethod);

                    MethodBuilder setMethod = ivTypeBld.DefineMethod("set_" + propertyName,
                       MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName,
                       typeof(void), new Type[] { requestInfoType });

                    // Generate IL code for set_Greeting method.
                    methodIL = setMethod.GetILGenerator();
                    methodIL.Emit(OpCodes.Ldarg_0);
                    methodIL.Emit(OpCodes.Ldarg_1);
                    methodIL.Emit(OpCodes.Stfld, field);
                    methodIL.Emit(OpCodes.Ret);
                    prop.SetSetMethod(setMethod);
                }
                constructedType = ivTypeBld.CreateType();
                lock (lockable)
                {
                    constructedTypes.Add(hashCode, constructedType);
                }

                #endregion
            }

            object obj = constructedType.InvokeMember("", BindingFlags.CreateInstance, null, null, null);

            PropertyInfo[] props = obj.GetType().GetProperties();
            if (possibleAssignTypes)
            {
                for (int i = 0; i < props.Length; i++)
                {
                    PropertyInfo pInfo = props[i];
                    object toSet = pInfo.PropertyType.InvokeMember("", BindingFlags.CreateInstance, null, null, null);
                    pInfo.SetValue(obj, toSet, null);


                    PropertyInfo tagProp = toSet.GetType().GetProperty("Tag");
                    object tagSet = null;

                    if (beginningValues == null || beginningValues[i] == null)
                    {
                        if (tagProp.PropertyType == typeof(string))
                        {
                            tagSet = "";
                        }
                        else
                        {
                            tagSet = tagProp.PropertyType.InvokeMember("", BindingFlags.CreateInstance, null, null, null);
                        }
                    }
                    else
                    {
                        object convertedVal = General.Utilities.Convert(beginningValues[i], tagProp.PropertyType);
                        tagSet = convertedVal;
                    }

                    tagProp.SetValue(toSet, tagSet, null);
                }
            }
            else
            {
                if (beginningValues != null)
                {
                    for (int i = 0; i < beginningValues.Length; i++)
                    {
                        if (beginningValues[i] == null) continue;
                        object convertedVal = General.Utilities.Convert(beginningValues[i], props[i].PropertyType);
                        props[i].SetValue(obj, convertedVal, null);
                    }
                }
            }
            return obj;
        }

        public static Type CreateEnum(string[] names)
        {
            int[] toPass = new int[names.Length];
            for (int i = 0; i < toPass.Length; i++)
            {
                toPass[i] = i;
            }
            return CreateEnum<int>(names, toPass);
        }

        public static Type CreateEnum<T>(string[] names, T[] values)
        {
            string name = "RequestInformationType";
            //  int hashCode = getHashKey(types, names, descriptions, possibleAssignTypes);
            string hash = "";
            foreach (string nameStr in names)
            {
                hash += nameStr;
            }


            Type constructedType = null;
            lock (lockable)
            {
                if (enumTypes.ContainsKey(hash))
                {
                    constructedType = enumTypes[hash];
                }

                name = name + id.ToString();
                id++;
            }

            if (constructedType == null)
            {
                EnumBuilder ebuilder = builder.DefineEnum(name, TypeAttributes.Public, typeof(T));
                for (int i = 0; i < names.Length; i++)
                {
                    ebuilder.DefineLiteral(names[i], values[i]);
                }
                constructedType = ebuilder.CreateType();
                lock (lockable)
                {
                    enumTypes.Add(hash, constructedType);
                }
            }
            return constructedType;
        }

        static RequestInformationPropertyGrid()
        {
            AppDomain myDomain = System.Threading.Thread.GetDomain();
            AssemblyName myAsmName = new AssemblyName();
            myAsmName.Name = "RequestInformationAssembly";

            AssemblyBuilder myAsmBuilder = myDomain.DefineDynamicAssembly(
                           myAsmName,
                           AssemblyBuilderAccess.RunAndSave);

            builder = myAsmBuilder.DefineDynamicModule("RequestInformationModule", true);

            constructedTypes = new Dictionary<int, Type>();
        }



        //protected virtual Point FindPosition(int x, int y)
        //{
        //    if (this.RowHeight == -1)
        //    {
        //        return InvalidPosition;
        //    }
        //    Size ourSize = this.GetOurSize();
        //    if ((x < 0) || (x > (ourSize.Width + this.ptOurLocation.X)))
        //    {
        //        return InvalidPosition;
        //    }
        //    Point point = new Point(1, 0);
        //    if (x > (this.InternalLabelWidth + this.ptOurLocation.X))
        //    {
        //        point.X = 2;
        //    }
        //    point.Y = (y - this.ptOurLocation.Y) / (1 + this.RowHeight);
        //    return point;
        //}




        private int currentRow = 0;
        private void focusItem(int row)
        {
            this.setInternals();

            MethodInfo getItemByRow = this.view.GetType().GetMethod("GetGridEntryFromRow", BindingFlags.Instance | BindingFlags.NonPublic);
            object entry = getItemByRow.Invoke(this.view, new object[] { row });

            Type entryType = entry.GetType();
            while (entryType.Name.Contains("CategoryGridEntry"))
            {
                this.FocusNextItem();
                return;
            }
            short flag = 2;

            //Set last point
            MethodInfo getFlag = this.view.GetType().GetMethod("GetFlag", BindingFlags.NonPublic | BindingFlags.Instance);
            bool test = (bool)getFlag.Invoke(this.view, new object[] { flag });

            MethodInfo setFlag = this.view.GetType().GetMethod("SetFlag", BindingFlags.NonPublic | BindingFlags.Instance);
            setFlag.Invoke(this.view, new object[] { flag, false });



            MethodInfo select = this.view.GetType().GetMethod("SelectRow", BindingFlags.NonPublic | BindingFlags.Instance);
            select.Invoke(this.view, new object[] { row });

            PropertyInfo editProp = this.view.GetType().GetProperty("Edit", BindingFlags.NonPublic | BindingFlags.Instance);
            Control c = editProp.GetValue(this.view, null) as Control;
            c.Focus();

            if (c is TextBox)
            {
                (c as TextBox).SelectAll();

                if ((c as TextBox).Tag == null || (bool)((c as TextBox).Tag) != true)
                {
                    (c as TextBox).KeyDown += new KeyEventHandler(RequestInformationPropertyGrid_KeyDown);
                    (c as TextBox).Tag = true;
                }
            }
        }

        public event KeyEventHandler KeyHandler;

        void RequestInformationPropertyGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Tab)
            {
                this.FocusNextItem();
                e.Handled = true;
            }

            KeyEventHandler temp = this.KeyHandler;
            if (temp != null)
            {
                temp(sender, e);
            }
        }

        public void FocusFirstItem()
        {
            this.currentRow = 0;
            this.focusItem(this.currentRow);
        }

        public void FocusNextItem()
        {
            this.currentRow++;



            if (this.currentRow >= this.gridItems.Count)
                this.currentRow = 0;

            this.focusItem(this.currentRow);
        }

        public GridItem GetGridItemAtPoint(int x, int y)
        {
            bool toolBarVisible = true;
            int rowH = 0;
            int labelW = 0;
            GridItemCollection gridI = null;

            this.setInternals();
            lock (this.lockableInstance)
            {
                toolBarVisible = this.ToolbarVisible;
                rowH = this.rowHeight;
                labelW = this.labelWidth;
                gridI = this.gridItems;
            }


            int curHeight = toolBarVisible ? 25 : 0;
            GridItem toRet = null;
            for (int i = 0; i < gridI.Count; i++)
            {
                GridItem item = gridI[i];
                Point p = this.PointToScreen(new Point(0, curHeight));
                if ((p.Y <= y && p.Y + rowH >= y))
                {
                    if (item.GetType().Name != "CategoryGridEntry")
                        toRet = item;
                }
                curHeight += rowH;
            }
            return toRet;
        }

        public RequestInformationPropertyGrid()
        {
            InitializeComponent();
        }

        private void setInternals()
        {
            lock (this.lockableInstance)
            {
                if (this.gridItems == null)
                {
                    this.view = typeof(PropertyGrid).InvokeMember("gridView", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField,
                        null, this, null);
                    this.gridItems = (GridItemCollection)view.GetType().InvokeMember("allGridEntries", BindingFlags.NonPublic | BindingFlags.Instance
                    | BindingFlags.GetField, null, view, null);
                    this.rowHeight = (int)view.GetType().InvokeMember("cachedRowHeight", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField,
                        null, view, null);
                    this.labelWidth = (int)view.GetType().InvokeMember("labelWidth", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField,
                        null, view, null);
                }
            }
        }

        public void SetInformationToCollect(Type[] types, string[] categories, string[] names, string[] descriptions, Type[] editorTypes,
            object[] beginningValues)
        {
            bool possibleAssign = false;
            //if (this.Tag != null && this.Tag.GetType() == typeof(VariableProcessingSetup.ProcessingAssociation))
            //{
            //    possibleAssign = true;
            //}
            this.SelectedObject = createObject(types, categories, names, descriptions, editorTypes, possibleAssign, beginningValues);
            this.names = names;
        }
    }
}