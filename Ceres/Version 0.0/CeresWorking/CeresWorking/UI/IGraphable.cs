﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CeresBase.UI
{
    public interface IGraphable : ILabel
    {
        double[] XData { get; set; }
        double[][] YData { get; set; }
        GraphingType GraphType { get; set; }

        

        string XLabel { get; set; }
        string YLabel { get; set; }

        string XUnits { get; set; }
        string YUnits { get; set; }

        string SourceDescription { get; set; }

        string[] SeriesLabels { get; set; }

        object[] ExtraInfo { get; set; }
    }
}
