using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CeresBase.UI
{
    public partial class ToggleButton : Button
    {
        private bool clicked = false;
        public bool Toggled
        {
            get
            {
                return this.clicked;
            }
            set
            {
                this.clicked = value;
                this.Invalidate();
            }
        }

        public ToggleButton()
        {
            InitializeComponent();
        }

        protected override void OnClick(EventArgs e)
        {
            this.clicked = !this.clicked;           
            base.OnClick(e);
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            if (!this.clicked)
                ControlPaint.DrawButton(pevent.Graphics, pevent.ClipRectangle, ButtonState.Normal);
            else ControlPaint.DrawButton(pevent.Graphics, pevent.ClipRectangle, ButtonState.Pushed);
            
            Brush brush = new SolidBrush(Color.Black);
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            pevent.Graphics.DrawString(this.Text, this.Font, brush, new Rectangle(this.Padding.Left, this.Padding.Top + 3,
                this.Width - this.Padding.Right, this.Height - this.Padding.Bottom), format); 

        }

        
    }
}
