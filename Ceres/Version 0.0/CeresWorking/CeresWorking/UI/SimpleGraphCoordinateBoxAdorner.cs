﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows;
using System.Windows.Media;

namespace CeresBase.UI
{
    public class SimpleGraphCoordinateBoxAdorner : Adorner
    {
        public Point CentralPoint { get; set; }
        public string DisplayText { get; set; }
        public bool ShowDisplayText { get; set; }
        public bool UseCustomDisplayTextPosition { get; set; }
        public Point CustomDisplayTextPosition { get; set; }

        public SimpleGraphCoordinateBoxAdorner(UIElement adornedElement, Point targetPoint, string displayText) 
            : base(adornedElement)
        {
            this.IsHitTestVisible = false;
            this.CentralPoint = targetPoint;
            this.DisplayText = displayText;
        }

        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            Rect adornerRect = new Rect(this.CentralPoint.X - 10, this.CentralPoint.Y - 10, 20, 20);
            Color transparencyRed = new Color();
            transparencyRed.R = Colors.Red.R;
            transparencyRed.G = Colors.Red.G;
            transparencyRed.B = Colors.Red.B;
            transparencyRed.ScA = 0.7F;
            SolidColorBrush penBrush = new SolidColorBrush(transparencyRed);
            Pen pen = new Pen(penBrush, 4);
            drawingContext.DrawRoundedRectangle(new SolidColorBrush(Colors.Transparent), pen, adornerRect, 4, 4);
            if (!ShowDisplayText) return;
            Point textLoc = new Point(adornerRect.Left, adornerRect.Bottom + 2);
            if (UseCustomDisplayTextPosition) textLoc = CustomDisplayTextPosition;
            drawingContext.DrawText(new FormattedText(this.DisplayText, System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight, new Typeface("Arial"), 14.0, new SolidColorBrush(Colors.Black)),
                textLoc);
        }
    }
}
