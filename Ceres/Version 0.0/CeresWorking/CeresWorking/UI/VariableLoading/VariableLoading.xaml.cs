﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CeresBase.UI.Navigation;
using System.IO;
using System.Collections.ObjectModel;
using CeresBase.IO;
using CeresBase.IO.DataBase;
using System.Collections;
using System.Windows.Threading;
using System.ComponentModel;
using MesosoftCommon.Controls;
using Deepforest.WPF.Controls;
using System.Reflection;
using MesosoftCommon.Adorners;
using System.Windows.Media.Effects;
using Kennedy.ManagedHooks;
using MesosoftCommon.ExtensionMethods;
using System.Windows.Controls.Primitives;
using CeresBase.Data;
using MesosoftCommon.Utilities.Settings;
using CeresBase.IO.Serialization;

namespace CeresBase.UI.VariableLoading
{
    /// <summary>
    /// Interaction logic for VariableLoading.xaml
    /// </summary>
    public partial class VariableLoading : Window, INotifyPropertyChanged
    {
        public static VariableLoading Instance { get; private set; }

        private ObservableCollection<FileSystemItem> drives = new ObservableCollection<FileSystemItem>();
        public ObservableCollection<FileSystemItem> Drives { get { return this.drives; } }

        private ObservableCollection<DataBaseEntry> dataRoots = new ObservableCollection<DataBaseEntry>();
        public ObservableCollection<DataBaseEntry> DataRoots { get { return this.dataRoots; } }

        private ObservableCollection<FileDataBaseEntry> filesToLoad = new ObservableCollection<FileDataBaseEntry>();
        public ObservableCollection<FileDataBaseEntry> FilesToLoad { get { return this.filesToLoad; } }

        private ObservableCollection<FileDataBaseEntry> autoFiles = new ObservableCollection<FileDataBaseEntry>();
        public ObservableCollection<FileDataBaseEntry> AutoFiles { get { return this.autoFiles; } }

        //public bool FilterNonRecognizedFiles
        //{
        //    get { return (bool)GetValue(FilterNonRecognizedFilesProperty); }
        //    set { SetValue(FilterNonRecognizedFilesProperty, value); }
        //}

        //// Using a DependencyProperty as the backing store for FilterNonRecognizedFiles.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty FilterNonRecognizedFilesProperty =
        //    DependencyProperty.Register("FilterNonRecognizedFiles", typeof(bool), typeof(VariableLoading), 
        //    new UIPropertyMetadata(true, new PropertyChangedCallback(filterChanged)));

        //private static void filterChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        //{
        //    VariableLoading loader = sender as VariableLoading;
        //    //loader.naviTree.Refresh();
        //    CollectionViewSource source = (loader.Content as FrameworkElement).FindResource("cvs") as CollectionViewSource;
        //    if (source.View == null) return;
        //    source.View.Refresh();
            
        //}

        private ICeresReader[] readers;
        private List<TextBox> toChange = new List<TextBox>();

        private static List<string> storedAutoDirectories = NewSerializationCentral.RegisterCollection<List<string>>(typeof(VariableLoading), "storedAutoDirectories");

        private void monitorIfNecessary(DirectoryItem item)
        {
            if (storedAutoDirectories.Contains(item.ExternalPath))
            {
                item.AutoLoad = true;
                this.directoriesToMonitor.Add(item);
            }
        }

        static VariableLoading()
        {
            VariableLoading.Instance = new VariableLoading();
        }

        private VariableLoading()
        {
            InitializeComponent();

            this.readers = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<ICeresReader>(null);
            this.dataRoots.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(dataRoots_CollectionChanged);

          //  MesosoftCommon.Hooks.HookManager.InstallHook(this);
          //  HookManager.AddHandler(this, new MouseHook.MouseEventHandler(hook_MouseEvent));

            this.manageUser();
            NewSerializationCentral.UserChanged += new EventHandler(SerializationCentral_UserChanged);

            ScrollViewer.SetHorizontalScrollBarVisibility(this.dataTree, ScrollBarVisibility.Hidden);
            this.SelectedBox = this.fileDB;
            
            //HookManager.MouseEvent += new MouseHook.MouseEventHandler(hook_MouseEvent);
                CentralIO.VariableLoaded += new ProgressChangedEventHandler(CentralIO_VariableLoaded);
        }

        void SerializationCentral_UserChanged(object sender, EventArgs e)
        {
            manageUser();
        }

        private void manageUser()
        {
            if (NewSerializationCentral.CurrentUser != null)
            {
                GroupUserCentral.DBPermission perm =
                    GroupUserCentral.GetUserPermission(NewSerializationCentral.CurrentUser.Name, NewSerializationCentral.CurrentUser.Password, NewSerializationCentral.CurrentDatabase.DatabaseName);

                if (perm == GroupUserCentral.DBPermission.Administrator)
                {
                    if (this.loadingBorder.Child == null)
                    {
                        this.mainContent.Content = null;
                        this.loadingBorder.Child = this.loadingGrid;
                    }
                    this.mainContent.Content = this.allGrid;

                }
                else if(perm == GroupUserCentral.DBPermission.User)
                {
                    this.loadingBorder.Child = null;
                    this.mainContent.Content = this.loadingGrid;
                }
                else if (perm == GroupUserCentral.DBPermission.None)
                {
                    Grid newGrid = new Grid();
                    newGrid.Children.Add(new TextBlock() { Text = "DATABASE PERMISSION DENIED. CHANGE DATABASES OR USERS.", HorizontalAlignment=HorizontalAlignment.Center, VerticalAlignment=VerticalAlignment.Center });

                    this.mainContent.Content = newGrid;
                }
            }
        }

        void dataRoots_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.toChange.Clear();
        }

        void dataTree_Loaded(object sender, RoutedEventArgs e)
        {
            int s = 0;
        }

        private void naviTree_Loaded(object sender, RoutedEventArgs e)
        {
            this.rebuildDrives();

            RebuildTree();
        }

        public void RebuildTree()
        {
            DataBaseEntry[] entries = CentralIO.CreateDatabaseTree();
            this.dataRoots.Clear();
            foreach (DataBaseEntry entry in entries)
            {
                this.dataRoots.Add(entry);
            }
        }

        private void rebuildDrives()
        {
            this.Drives.Clear();

            string[] drives = Directory.GetLogicalDrives();
            foreach (string drive in drives)
            {
                if (drive == "A:\\") continue;
                DirectoryItem item = new DirectoryItem(drive, this.naviTree);
                item.RawChildren.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Children_CollectionChanged);

                this.Drives.Add(item);
            }

           
        }

        void Children_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (DirectoryItem item in e.NewItems)
                {
                    this.monitorIfNecessary(item);
                    item.RawChildren.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Children_CollectionChanged);
                }
            }
        }


        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {           
            //e.Accepted = true;
            if(e.Item is FileItem)
            {
                FileItem item = e.Item as FileItem;
                item.Tag = false;
               
               // if(this.FilterNonRecognizedFiles) 
                    e.Accepted = false;

                foreach (ICeresReader read in this.readers)
                {
                    if (read.Supported(item.ExternalPath))
                    {
                        e.Accepted = true;
                        item.Tag = true;
                    }
                }
            }
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DatabaseTree tree = e.OriginalSource as DatabaseTree;
            if (tree == null) return;
            ExperimentEntry newEntry = new ExperimentEntry(tree.SelectedItem as ExperimentEntry);
           // newEntry.DisplayName = newEntry.DefaultName;
            newEntry.BeginNameChange();

            TreeViewItem item = tree.TryFindNode(tree.SelectedItem);
            item.IsExpanded = true;
            
        }

        private void deleteEntry(DataBaseEntry entry)
        {
            DataBaseEntry[] copy = entry.Children.ToArray();

            for (int i = 0; i < copy.Length; i++ )
            {
                DataBaseEntry child = copy[i];
                this.deleteEntry(child);
            }

            if (entry is FileDataBaseVariableEntry)
            {
                CentralIO.RemoveVariable(entry.Parent.Path, entry.Name);
            }

            if (entry.Parent != null)
            {
                entry.Parent.Children.Remove(entry);
            }
            else
            {
                this.dataRoots.Remove(entry);
            }
            
        }

        private void CommandBinding_Executed_1(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.dataTree.SelectedItem != null)
            {
                this.deleteEntry(this.dataTree.SelectedItem);
            }
        }

        private void CommandBinding_Executed_2(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (e.Parameter == null || !(e.Parameter is ExperimentEntry))
            {
                e.CanExecute = false;
            }
            else e.CanExecute = true;
        }

        private void CommandBinding_CanExecute_1(object sender, CanExecuteRoutedEventArgs e)
        {
            if (e.Parameter == null || !(e.Parameter is DataBaseEntry))
            {
                e.CanExecute = false;
            }
            else e.CanExecute = true;           
        }

        private bool isEditingName = false;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ExperimentEntry newEntry = new ExperimentEntry();
            //newEntry.DisplayName = newEntry.DefaultName;
            newEntry.BeginNameChange();

            this.dataRoots.Add(newEntry);
            this.isEditingName = true;           
        }

        private bool setName(DataBaseEntry entry, TextBox box, bool capture)
        {
            int ok = this.checkForValidName(entry);
            if (ok == 0)
            {
                entry.EndNameChange(true);
                this.isEditingName = false;

                if (this.toChange.Contains(box))
                {
                    int index = this.toChange.IndexOf(box);
                    this.toChange.Remove(box);
                    if (index >= this.toChange.Count)
                    {
                        index = 0;
                    }
                    if (this.toChange.Count != 0)
                    {
                        this.toChange[index].Focus();
                        this.toChange[index].SelectAll();
                    }
                }

            }
            else
            {
                if (capture)
                {
                    box.Focus();
                    box.SelectAll();
                }
                System.Windows.Controls.ToolTip tooltip = new ToolTip();
                if (ok == 1) tooltip.Content = "There is another item with this name. Please change the name or cancel (press escape).";
                else tooltip.Content = "This is the default name. Please change the name.";
                box.ToolTip = tooltip;


                if (capture)
                {
                    tooltip.IsOpen = true;

                    DispatcherTimer timer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 3), IsEnabled = true };
                    timer.Tag = tooltip;
                    timer.Tick += new EventHandler(
                        delegate(object timerSender, EventArgs timerArgs)
                        {
                            DispatcherTimer time = timerSender as DispatcherTimer;
                            if (time.Tag != null)
                            {
                                (time.Tag as ToolTip).IsOpen = false;
                            }
                        }
                    );
                }
            }

            return ok == 0;
        }

        private int checkForValidName(DataBaseEntry entry)
        {
            if (entry.DisplayName == entry.DefaultName)
            {
                return 2;
            }

            if (entry.Parent == null)
            {
                foreach (DataBaseEntry c in this.dataRoots)
                {
                    if (c == entry) continue;
                    if (c.Name == entry.DisplayName) return 1;
                }
            }
            else
            {
                foreach (DataBaseEntry c in entry.Parent.Children)
                {
                    if (c == entry) continue;
                    if (c.Name == entry.DisplayName) return 1;
                }
            }


            return 0;
        }

        private void Name_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox box = sender as TextBox;
            if (box == null) return;
            if (!(box.Tag is DataBaseEntry)) return;

            DataBaseEntry entry = box.Tag as DataBaseEntry;

            if (e.Key == Key.Return || e.Key==Key.Tab)
            {
                this.setName(entry, box,true);
                e.Handled = true;
            }
            else if (e.Key == Key.Escape)
            {
                

                entry.CancelNameChange();
                this.isEditingName = false;
                e.Handled = true;
            }

            
        }

        private void textName_Loaded(object sender, RoutedEventArgs e)
        {
            TextBox box = sender as TextBox;
            if (box == null) return;

            if (box.IsEnabled && !toChange.Contains(box))
            {
                box.Focus();
                box.SelectAll();
                toChange.Add(box);
            }
        }

        private void textName_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = sender as TextBox;
            if (box == null) return;
            DataBaseEntry entry = box.Tag as DataBaseEntry;
            if (entry == null) return;
            this.setName(entry, box, false);
        }


        #region Dragging

        private Point startPoint;
        private bool isDragging;

        
        private void fileList_PreviewLeftMouseDown(object sender, MouseButtonEventArgs e)
        {
            ListBox box = sender as ListBox;

           

            if (e.OriginalSource.GetType().Name.Contains("Scroll"))
            {
                return;
            }
         

            if (box == null) return;

            if (Microsoft.Win32.Imports.IsKeyDown(System.Windows.Forms.Keys.ShiftKey) || Microsoft.Win32.Imports.IsKeyDown(System.Windows.Forms.Keys.ControlKey)) return;

           HitTestResult res = VisualTreeHelper.HitTest(box, e.GetPosition(sender as FrameworkElement));

           if (res == null || res.VisualHit == null) return;

           ListBoxItem parent = null;
            DependencyObject obj = res.VisualHit;
            while (obj != null)
            {
                DependencyObject test = VisualTreeHelper.GetParent(obj);

                if (test is ListBoxItem)
                {
                    parent = test as ListBoxItem;
                    break;
                }

                obj = test;
            }

            if (parent == null) return;

           bool startDrag = false;
           foreach (object o in box.SelectedItems)
           {
               if (parent.Content == o)
               {
                   startDrag = true;
                   break;
               }
           }

           if (startDrag)
           {
               if (!this.isDragging)
               {
                   this.startDrag(e, box);
               }
           }
        }

        private void startDrag(MouseEventArgs e, ListBox box)
        {
            isDragging = true;
            DataObject data = null;

            if (box.SelectedItem == null)
            {
                this.isDragging = false;
                return;
            }

            if(box.SelectedItem.GetType() == typeof(FileItem))
                data = new DataObject(box.SelectedItems.Cast<FileItem>().ToArray());
            else
                data = new DataObject(box.SelectedItems.Cast<FileDataBaseEntry>().ToArray());
            DragDropEffects de = DragDrop.DoDragDrop(box, data, DragDropEffects.Move);
            
            this.isDragging = false;


          //  this.CaptureMouse();
            this.MouseUp += new MouseButtonEventHandler(VariableLoading_MouseUp);

            
        }

        void VariableLoading_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ButtonState == MouseButtonState.Released && this.treeAdorner != null)
            {
                System.Windows.Point syspoint = new Point((int)e.MouseDevice.GetPosition(sender as IInputElement).X, (int)e.MouseDevice.GetPosition(sender as IInputElement).Y);

                PresentationSource s = PresentationSource.FromVisual(this.treeAdorner);
                if (s == null)
                    return;
                HitTestResult result = VisualTreeHelper.HitTest(this.treeAdorner, this.treeAdorner.PointFromScreen(syspoint));
                bool remove = true;

                if (result != null && result.VisualHit != null)
                {
                    if ((result.VisualHit as Visual).IsDescendantOf(this.treeAdorner))
                    {
                        remove = false;
                    }
                }

                if (remove)
                {
                    this.hideAdorner();
                }

            }
        }

       

        private void fileList_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            ListBox box = sender as ListBox;



            if (e.OriginalSource.GetType().Name.Contains("Thumb"))
            {
                return;
            }

            if (e.LeftButton == MouseButtonState.Pressed && !this.isDragging)
            {
                Point position = e.GetPosition(null);

                if (Math.Abs(position.X - this.startPoint.X) > 2 ||
                   Math.Abs(position.Y - this.startPoint.Y) > 2)
                {
                    this.startDrag(e, box);
                }

            }


        }
        #endregion


        #region File Drop

        private void GroupBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(FileItem[])))
            {
                e.Effects = DragDropEffects.Move;
            }
            else e.Effects = DragDropEffects.None;

            e.Handled = true;
        }

        private void GroupBox_DragLeave(object sender, DragEventArgs e)
        {
            int s = 0;
        }

        private void GroupBox_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(FileItem[])))
            {
                e.Effects = DragDropEffects.Move;
            }
            else e.Effects = DragDropEffects.None;
            e.Handled = true;

            
        }

        private Dictionary<FileDataBaseEntry, FileItem> itemsToEntries = new Dictionary<FileDataBaseEntry, FileItem>();

        private void GroupBox_Drop(object sender, DragEventArgs e)
        {
            //CollectionViewSource.GetDefaultView(box).GroupDescriptions.Add(new PropertyGroupDescription("LastName"));
            FileItem[] items = e.Data.GetData(typeof(FileItem[])) as FileItem[];
            if (items == null) return;
            
            foreach (FileItem item in items)
            {
                if (itemsToEntries.ContainsValue(item)) continue;

                FileDataBaseEntry entry = new FileDataBaseEntry(item.Name);
                if (FileDataBaseEntry.CanLoadFile(item.ExternalPath))
                {
                    entry.LoadFile(item.ExternalPath);
                }

                itemsToEntries.Add(entry, item);

                this.FilesToLoad.Add(entry);
            }
        }
        #endregion


        #region Tree Drop

        private void variableTree_DragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            e.Handled = true;
        }

        private void variableTree_DragLeave(object sender, DragEventArgs e)
        {
            int s = 0;
        }

        private void addVariables(FileDataBaseEntry entry)
        {
            
        }

        private AdornerLayer treeLayer;
        private UIElementAdorner treeAdorner;

        private void hideAdorner()
        {
            if (this.treeLayer != null)
            {
                Adorner[] adorners = this.treeLayer.GetAdorners(this.treeLayer);

                if (adorners != null)
                {
                    foreach (Adorner adorn in adorners)
                    {
                        if (adorn is UIElementAdorner)
                        {
                            this.treeLayer.Remove(adorn);
                        }
                    }
                }
            }

            this.ReleaseMouseCapture();
           // HookManager.UninstallHook(this);
        }



        private void variableTree_DragOver(object sender, DragEventArgs e)
        {
            DatabaseTree tree = sender as DatabaseTree;
            if (tree == null) return;
            HitTestResult res = VisualTreeHelper.HitTest(tree, e.GetPosition(sender as FrameworkElement));

            if (res == null || res.VisualHit == null) return;

            TreeViewItem parent = null;
            DependencyObject obj = res.VisualHit;
            while (obj != null)
            {
                DependencyObject test = VisualTreeHelper.GetParent(obj);

                if (test is TreeViewItem)
                {
                    parent = test as TreeViewItem;
                    break;
                }

                obj = test;
            }

            if (parent == null || parent.Header is FileDataBaseVariableEntry)
            {
                e.Effects = DragDropEffects.None;
            }
            else
            {
                if (e.Data.GetDataPresent(typeof(FileDataBaseEntry[])))
                {
                    e.Effects = DragDropEffects.None;
                    tree.SelectedItem = parent.Header as DataBaseEntry;


                    if (this.treeLayer == null)
                    {
                        this.treeLayer = AdornerLayer.GetAdornerLayer(tree);
                        DropAdornerControl dropControl = new DropAdornerControl();
                        dropControl.DropSuccessful += new EventHandler(dropControl_DropSuccessful);
                        this.treeAdorner = new UIElementAdorner(this.treeLayer, dropControl);
                        
                    }
                    else
                    {
                        Adorner[] adorners = this.treeLayer.GetAdorners(this.treeLayer);
                        if (adorners == null)
                        {
                            this.treeLayer.Add(this.treeAdorner);
                          //  HookManager.InstallHook(this);
                            this.CaptureMouse();
                        }
                    }



                    Point point = parent.TranslatePoint(new Point(0, 0), this);
                    this.treeAdorner.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                    this.treeAdorner.SetOffsets(point.X - this.treeAdorner.DesiredSize.Width - 10, point.Y -this.treeAdorner.DesiredSize.Height/ 2);
                }
                else e.Effects = DragDropEffects.None;
            }

            e.Handled = true;


        }

        void dropControl_DropSuccessful(object sender, EventArgs e)
        {
            DropAdornerControl control = sender as DropAdornerControl;
            if (control == null) return;
            FileDataBaseEntry[] items = control.Entries;

            string errors = "";
            List<FileDataBaseEntry> entriesToDel = new List<FileDataBaseEntry>();
            if (items == null) return;
            foreach (FileDataBaseEntry entry in items)
            {
                filesAlreadySeen.Add(entry.Filepath);

                ExperimentEntry parentExp = null;
                if (control.Selection == DropAdornerControl.DropSelection.None)
                {
                    parentExp = this.dataTree.SelectedItem as ExperimentEntry;
                }
                else if (control.Selection == DropAdornerControl.DropSelection.Tree || control.Selection == DropAdornerControl.DropSelection.Both)
                {
                    FileItem item = this.itemsToEntries[entry];
                    DirectoryItem directory = null;
                    DirectoryItem parent = item.Parent as DirectoryItem;
                    Stack<DirectoryItem> parentChain = new Stack<DirectoryItem>();
                    while (parent != null)
                    {
                        parentChain.Push(parent);

                        if (parent.AutoLoad)
                        {
                            directory = parent;
                            break;
                        }

                        parent = parent.Parent as DirectoryItem;
                    }

                    if (directory != null)
                    {
                        ExperimentEntry exp = this.dataTree.SelectedItem as ExperimentEntry;
                        while (parentChain.Count != 0)
                        {
                            DirectoryItem dir = parentChain.Pop();
                            ExperimentEntry child = exp.Children.SingleOrDefault(
                                c => c.Name == dir.Name && c.GetType() == typeof(ExperimentEntry)) as ExperimentEntry;
                            if (child == null)
                            {
                                child = new ExperimentEntry(dir.Name, exp);
                            }

                            exp = child;
                        }

                        parentExp = exp;
                    }
                    else
                    {
                        errors += "Cannot find root directory for " + entry.Filepath + "\n";
                    }
                }

                if (control.Selection == DropAdornerControl.DropSelection.File || 
                    (control.Selection == DropAdornerControl.DropSelection.Both && parentExp != null) )
                {
                    ExperimentEntry exp = parentExp;
                    if (exp == null) 
                        exp = this.dataTree.SelectedItem as ExperimentEntry;

                    string filename = entry.Filepath.Substring(entry.Filepath.LastIndexOf('\\') + 1);
                    int index = filename.LastIndexOf('.');
                    if (index > 0)
                        filename = filename.Substring(0, index);

                    ExperimentEntry child = exp.Children.SingleOrDefault(
                                c => c.Name == filename && c.GetType() == typeof(ExperimentEntry)) as ExperimentEntry;
                    if (child == null)
                    {
                        child = new ExperimentEntry(filename, exp);
                    }

                    parentExp = child;
                }

                if (parentExp != null)
                {
                    bool ok = true;

                    foreach (FileDataBaseVariableEntryParameter var in entry.VariableParameters)
                    {
                        if (CentralIO.ContainsVariable(parentExp.Path, var.VariableName.Value))
                        {
                            errors += "Already contains variable " + var.VariableName.Value + " in " + parentExp.Path + "\n";
                            ok = false;
                        }
                    }

                    if (ok)
                    {
                        foreach (FileDataBaseVariableEntryParameter var in entry.VariableParameters)
                        {
                            //if (var.Comments.Value == "") var.Comments.Value = null;
                            CentralIO.AddVariable(entry, parentExp.Path, var);
                            if (var.DisplayName.Value == null || var.DisplayName.Value == "")
                            {
                                var.DisplayName.Value = var.VariableName.Value;
                            }

                            FileDataBaseVariableEntry varEntry = new FileDataBaseVariableEntry(var.VariableName.Value, parentExp) { 
                                DisplayName=var.DisplayName.Value, Comments = var.Comments.Value };

                        }
                        entriesToDel.Add(entry);
                        this.itemsToEntries.Remove(entry);

                    }                   
                }              
            }

            this.hideAdorner();

            if (errors != "")
            {
                ToolTip errorTip = new ToolTip();
                errorTip.Content = errors;
                errorTip.Placement = PlacementMode.Center;
                errorTip.PlacementTarget = this;
                errorTip.IsOpen = true;

                DispatcherTimer timer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 4), IsEnabled = true };
                timer.Tag = errorTip;
                timer.Tick += new EventHandler(
                    delegate(object timerSender, EventArgs timerArgs)
                    {
                        DispatcherTimer time = timerSender as DispatcherTimer;
                        if (time.Tag != null)
                        {
                            (time.Tag as ToolTip).IsOpen = false;
                        }
                    }
                );
            }

            this.selectedBox.BeginInit();
            foreach (FileDataBaseEntry entry in entriesToDel)
            {
                if (selectedBox == this.fileDB)
                    this.filesToLoad.Remove(entry);
                else this.autoFiles.Remove(entry);
            }
            this.selectedBox.EndInit();


            this.dataTree.Refresh();



        }

        private void variableTree_Drop(object sender, DragEventArgs e)
        {
           


            //foreach (FileItem item in items)
            //{
            //    FileDataBaseEntry entry = new FileDataBaseEntry(item.Name);
            //    if (FileDataBaseEntry.CanLoadFile(item.ExternalPath))
            //    {
            //        entry.LoadFile(item.ExternalPath);
            //    }

            //    this.FilesToLoad.Add(entry);
            //}
        }

        #endregion

        private FileDataBaseEntry firstEntry;

        private bool forgoUpdate;
        private bool dontUpdate
        {
            get
            {
                return this.forgoUpdate;
            }
            set
            {
                this.forgoUpdate = value;
                if (!this.forgoUpdate)
                {
                    BindingExpression ex = BindingOperations.GetBindingExpression(this.propGrid, PropertyGrid.InstanceProperty);
                    ex.UpdateTarget();
                }
            }
        }

        private void fileDB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox box = sender as ListBox;
            
            foreach(FileDataBaseEntry entry in e.AddedItems)
            {
                if (entry.Status == FileDataBaseEntryStatus.NeedsVars)
                {
                    entry.LoadVariableParameters();
                }
            }

            if (this.selectedBox.SelectedItems.Count == 0)
            {
                this.firstEntry = null;
            }
            else if (this.selectedBox.SelectedItems.Count == 1)
            {
                this.firstEntry = this.selectedBox.SelectedItem as FileDataBaseEntry;
            }
            else
            {
                if (this.firstEntry != null)
                {
                    foreach (FileDataBaseEntry entry in e.AddedItems)
                    {
                        if (entry.Reader != this.firstEntry.Reader || entry.Status != this.firstEntry.Status)
                        {
                            this.selectedBox.SelectedItems.Remove(entry);
                        }
                    }
                }
            }

            if (!this.dontUpdate)
            {
                BindingExpression ex = BindingOperations.GetBindingExpression(this.propGrid, PropertyGrid.InstanceProperty);
                ex.UpdateTarget();
            }
        }

        private void header_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock block = e.OriginalSource as TextBlock;
            ListCollectionView view = this.selectedBox.ItemsSource as ListCollectionView;
            CollectionViewGroup group = view.Groups.SingleOrDefault(
                delegate(object g)
                {
                    return (g as CollectionViewGroup).Name.ToString() == block.Text;
                }
            ) as CollectionViewGroup;


            if (group == null) return;

            this.dontUpdate = true;

            this.selectedBox.SelectedItems.Clear();
            foreach (FileDataBaseEntry entry in group.Items)
            {
                this.selectedBox.SelectedItems.Add(entry);
            }

            this.dontUpdate = false;
        }

        private void fileDB_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }



        #region INotifyPropertyChanged Members

        protected void propChanged(string propertyChanged)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyChanged));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void settingsButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.selectedBox.SelectedItem == null) return;


            object gridObj = this.propGrid.Instance;
            PropertyInfo[] infos = gridObj.GetType().GetProperties();

            bool isFile = (this.selectedBox.SelectedItem as FileDataBaseEntry).Status == FileDataBaseEntryStatus.NeedsAll;

            if (!isFile)
            {

                List<FileDataBaseVariableEntryParameter> paras = new List<FileDataBaseVariableEntryParameter>();
                foreach (FileDataBaseEntry entry in this.selectedBox.SelectedItems)
                {
                    if (entry.Status == FileDataBaseEntryStatus.NeedsVars || entry.Status == FileDataBaseEntryStatus.Complete)
                    {
                        paras.AddRange(entry.VariableParameters);
                    }
                }

                foreach (PropertyInfo info in infos)
                {
                    int lastIndex = info.Name.LastIndexOf(' ');
                    string varName = info.Name.Substring(0, lastIndex).Trim();
                    string propName = info.Name.Substring(lastIndex).Trim();

                    object val = info.GetValue(gridObj,null);

                    FileDataBaseVariableEntryParameter[] foundParas = paras.Where(p => p.VariableName.Value == varName).ToArray();
                    foreach (FileDataBaseVariableEntryParameter para in foundParas)
                    {
                        PropertyInfo paraProp = para.GetType().GetProperty(propName);
                        object paraEntry = paraProp.GetValue(para, null);
                        PropertyInfo valProp = paraEntry.GetType().GetProperty("Value");

                        valProp.SetValue(paraEntry, val, null);
                        PropertyInfo success = paraEntry.GetType().GetProperty("SuccessfullySet");
                        success.SetValue(paraEntry, true, null);
                    }
                }

                foreach (FileDataBaseEntry entry in this.selectedBox.SelectedItems)
                {
                    entry.UpdateStatus();
                }

            }
            else
            {
                foreach (FileDataBaseEntry entry in this.selectedBox.SelectedItems)
                {

                    foreach (PropertyInfo info in infos)
                    {
                        FileDataBaseEntryParameter para = entry.Parameters.SingleOrDefault(p => p.Name == info.Name);
                        if (para == null) continue;
                        para.Value = info.GetValue(gridObj, null);
                        para.SuccessfullySet = true;
                    }
                    entry.UpdateStatus();

                    if (entry.Status == FileDataBaseEntryStatus.NeedsVars)
                        entry.LoadVariableParameters();

                }
            }

            BindingExpression ex = BindingOperations.GetBindingExpression(this.propGrid, PropertyGrid.InstanceProperty);
            ex.UpdateTarget();
        }

        void textFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = sender as TextBox;
            if (sender == null) return;
            box.SelectAll();
        }

        void rootFolder_Click(object sender, RoutedEventArgs e)
        {
          //  this.naviTree.TreeNodeStyle
            DirectoryItem selectedItem = (this.naviTree.SelectedItem as DirectoryItem);
            if (selectedItem == null) return;
            selectedItem.AutoLoad = !selectedItem.AutoLoad;

            if (selectedItem.AutoLoad && !this.directoriesToMonitor.Contains(selectedItem))
            {
                this.directoriesToMonitor.Add(selectedItem);
                storedAutoDirectories.Add(selectedItem.ExternalPath);
            }
            else if(!selectedItem.AutoLoad && this.directoriesToMonitor.Contains(selectedItem))
            {
                this.directoriesToMonitor.Remove(selectedItem);
                storedAutoDirectories.Remove(selectedItem.ExternalPath);
            }
        }

        void tabChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(e.OriginalSource is TabControl))
                return;

            if (e.AddedItems.Count == 1)
            {
                this.selectedBox.SelectedItems.Clear();

                TabItem item = e.AddedItems[0] as TabItem;
                if (item != null && item.Header.ToString() == "Auto")
                {
                    this.SelectedBox = this.autoDB;

                    this.populateAutoList();
                }
                else
                {
                    this.SelectedBox = this.fileDB;
                }

                this.firstEntry = null;
            }
        }

        #region Load Variables

        private BackgroundWorker loadWorker;

        void loadVariables(object o, DoWorkEventArgs e)
        {
            FileDataBaseVariableEntry[] vars = e.Argument as FileDataBaseVariableEntry[];
            CentralIO.LoadVariables(vars);            
        }

        void CentralIO_VariableLoaded(object sender, ProgressChangedEventArgs e)
        {
            this.loadWorker.ReportProgress(e.ProgressPercentage, e.UserState);
            DoWorkEventArgs cancel = sender as DoWorkEventArgs;
            if (this.loadWorker.CancellationPending) cancel.Cancel = true;
        }

        private void loadVariablesCompleted(object s, RunWorkerCompletedEventArgs args)
        {           
            ControlTemplate template = this.loadingGrid.FindResource("loadVars") as ControlTemplate;
            this.variableHolder.Template = template;
        }

        private void loadVarsProgressChanged(object s, ProgressChangedEventArgs args)
        {
            ControlTemplate template = this.loadingGrid.FindResource("cancelGrid") as ControlTemplate;

            this.VariablesLoadedCount = args.ProgressPercentage;
            
        }

        void loadVars_Cancel(object sender, RoutedEventArgs e)
        {
            if (this.loadWorker != null)
            {
                this.loadWorker.CancelAsync();
                (sender as Button).IsEnabled = false;
            }
        }



        public int VariablesToLoadCount
        {
            get { return (int)GetValue(VariablesToLoadCountProperty); }
            set { SetValue(VariablesToLoadCountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for VariablesToLoadCount.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty VariablesToLoadCountProperty =
            DependencyProperty.Register("VariablesToLoadCount", typeof(int), typeof(VariableLoading));



        public int VariablesLoadedCount
        {
            get { return (int)GetValue(VariablesLoadedCountProperty); }
            set { SetValue(VariablesLoadedCountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for VariablesLoaded.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty VariablesLoadedCountProperty =
            DependencyProperty.Register("VariablesLoadedCount", typeof(int), typeof(VariableLoading));




        void loadVars_Click(object sender, RoutedEventArgs e)
        {
            
            this.loadWorker = new BackgroundWorker();
            this.loadWorker.WorkerReportsProgress = true;
            this.loadWorker.WorkerSupportsCancellation = true;
            this.loadWorker.DoWork += new DoWorkEventHandler(this.loadVariables);
            this.loadWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.loadVariablesCompleted);
            this.loadWorker.ProgressChanged += new ProgressChangedEventHandler(this.loadVarsProgressChanged);
            
            List<FileDataBaseVariableEntry> vars = new List<FileDataBaseVariableEntry>();
            foreach (DataBaseEntry entry in this.dataRoots)
            {
                this.addToLoad(entry, vars);
            }


            ControlTemplate template = this.loadingGrid.FindResource("cancelGrid") as ControlTemplate;
            this.variableHolder.Template = template;
            this.VariablesToLoadCount = vars.Count;
            this.VariablesLoadedCount = 0;
            this.loadWorker.RunWorkerAsync(vars.ToArray());

            
        }



        void addToLoad(DataBaseEntry entry, List<FileDataBaseVariableEntry> vars)
        {
            if (entry is FileDataBaseVariableEntry)
            {
                FileDataBaseVariableEntry varEntry = entry as FileDataBaseVariableEntry;

                if (entry.IsSelected)
                {
                    if (!VariableSource.ContainsVariable(varEntry.Name, (varEntry.Parent as ExperimentEntry).Path))
                    {
                        vars.Add(entry as FileDataBaseVariableEntry);
                    }
                }
                else
                {
                    if (VariableSource.ContainsVariable(varEntry.Name, (varEntry.Parent as ExperimentEntry).Path))
                    {
                        VariableSource.RemoveVariable((varEntry.Parent as ExperimentEntry).Path, varEntry.Name);
                    }
                }
            }
            else
            {
                foreach (DataBaseEntry child in entry.Children)
                {
                    this.addToLoad(child, vars);
                }
            }
        }

        #endregion

        #region AutoMonitor
        private void populateDirectory(DirectoryItem item)
        {
            string[] files = Directory.GetFiles(item.ExternalPath);
            if (files != null)
            {
                foreach (string file in files)
                {
                    if (filesAlreadySeen.Contains(file))
                        continue;

                    FileItem fileItem = new FileItem(file.Substring(file.LastIndexOf('\\') + 1), item);
                    FileDataBaseEntry entry = new FileDataBaseEntry(fileItem.Name);
                    if (FileDataBaseEntry.CanLoadFile(fileItem.ExternalPath))
                    {
                        entry.LoadFile(fileItem.ExternalPath);
                        itemsToEntries.Add(entry, fileItem);

                        this.autoFiles.Add(entry);
                    }
                    else
                    {
                        filesAlreadySeen.Add(fileItem.ExternalPath);
                    }


                }
            }

            foreach (DirectoryItem child in item.Children)
            {
                this.populateDirectory(child);
            }
        }

        private void populateAutoList()
        {
            this.autoDB.BeginInit();

            this.autoFiles.Clear();
            foreach (DirectoryItem item in directoriesToMonitor)
            {
                this.populateDirectory(item);
            }

            this.autoDB.EndInit();
        }

        void refreshAuto_Click(object sender, RoutedEventArgs e)
        {
            this.populateAutoList();
        }

        void filterOut_Click(object sender, RoutedEventArgs e)
        {
            this.autoDB.BeginInit();

            List<FileDataBaseEntry> toRemove = new List<FileDataBaseEntry>();
            foreach (FileDataBaseEntry entry in this.autoDB.SelectedItems)
            {
                filesAlreadySeen.Add(entry.Filepath);
                toRemove.Add(entry);
            }

            foreach (FileDataBaseEntry entry in toRemove)
            {
                this.autoFiles.Remove(entry);
            }

            this.autoDB.EndInit();
        }


        private ListBox selectedBox;
        public ListBox SelectedBox
        {
            get
            {
                return this.selectedBox;
            }
            set
            {
                this.selectedBox = value;
                BindingExpression ex = this.propGrid.GetBindingExpression(PropertyGrid.InstanceProperty);
                if (ex != null) ex.UpdateTarget();
            }
        }

        private List<DirectoryItem> directoriesToMonitor = new List<DirectoryItem>();
        private static List<string> filesAlreadySeen = NewSerializationCentral.RegisterCollection<List<string>>(typeof(VariableLoading), "filesAlreadySeen");
        #endregion

        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            this.rebuildDrives();
        }
    }
}
