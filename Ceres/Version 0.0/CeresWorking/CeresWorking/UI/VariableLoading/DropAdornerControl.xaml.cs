﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CeresBase.IO.DataBase;

namespace CeresBase.UI.VariableLoading
{
    /// <summary>
    /// Interaction logic for DropAdornerControl.xaml
    /// </summary>
    public partial class DropAdornerControl : UserControl
    {
        public enum DropSelection { Tree, File, None, NA, Both };

        public DropAdornerControl()
        {
            InitializeComponent();
            this.Selection = DropSelection.NA;
            
        }

        public DropSelection Selection { get; private set; }
        public FileDataBaseEntry[] Entries { get; set; }

        public event EventHandler DropSuccessful;

        private void Image_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(FileDataBaseEntry[])))
            {
                e.Effects = DragDropEffects.Move;
            }
            else e.Effects = DragDropEffects.None;
            e.Handled = true;

            FrameworkElement ele = e.Source as FrameworkElement;
            if (ele.ToolTip is ToolTip)
            {
                (ele.ToolTip as ToolTip).IsOpen = true;
            }
        }

        private void Image_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(FileDataBaseEntry[])))
            {
                e.Effects = DragDropEffects.Move;
            }
            else e.Effects = DragDropEffects.None;
            e.Handled = true;
        }

        private void Image_Drop(object sender, DragEventArgs e)
        {
            FileDataBaseEntry[] items = e.Data.GetData(typeof(FileDataBaseEntry[])) as FileDataBaseEntry[];
            if (items == null) return;

            FrameworkElement ele = sender as FrameworkElement;
            if (ele == null) return;

            if (ele.ToolTip is ToolTip)
            {
                (ele.ToolTip as ToolTip).IsOpen = false;
            }

            if (ele.Name == "tree") this.Selection = DropSelection.Tree;
            else if (ele.Name == "name") this.Selection = DropSelection.File;
            else if (ele.Name == "both") this.Selection = DropSelection.Both;
            else this.Selection = DropSelection.None;

            this.Entries = items;

           
            if (this.DropSuccessful != null)
            {
                this.DropSuccessful(this, new EventArgs());
            }
        }

        private void none_DragLeave(object sender, DragEventArgs e)
        {

            FrameworkElement ele = e.Source as FrameworkElement;
            if (ele.ToolTip is ToolTip)
            {
                (ele.ToolTip as ToolTip).IsOpen = false;
            }

            e.Handled = true;
        }

       
    }
}
