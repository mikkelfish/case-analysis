﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hardcodet.Wpf.GenericTreeView;
using CeresBase.IO.DataBase;

namespace CeresBase.UI.VariableLoading
{
    public class DatabaseTree : TreeViewBase<DataBaseEntry>
    {

        public override ICollection<DataBaseEntry> GetChildItems(DataBaseEntry parent)
        {
            return parent.Children;
        }

        public override string GetItemKey(DataBaseEntry item)
        {
            return item.Path;
        }

        public override DataBaseEntry GetParentItem(DataBaseEntry item)
        {
            return item.Parent;
        }

        protected override System.Windows.Controls.TreeViewItem CreateTreeViewItem(DataBaseEntry item)
        {
            return base.CreateTreeViewItem(item);
        }
    }
}
