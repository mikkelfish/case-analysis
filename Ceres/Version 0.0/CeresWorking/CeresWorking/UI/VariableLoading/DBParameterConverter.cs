﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CeresBase.IO.DataBase;
using System.Windows.Shapes;
using System.Windows;

namespace CeresBase.UI.VariableLoading
{
    [ValueConversion(typeof(FileDataBaseEntry), typeof(FileDataBaseEntryParameter[]))]
    public class DBParameterConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is FileDataBaseEntry)) return new FileDataBaseEntryParameter[0];

            FileDataBaseEntry entry = value as FileDataBaseEntry;
            List<FileDataBaseEntryParameter> toRet = new List<FileDataBaseEntryParameter>();

            if (entry.Parameters != null)
            {
                foreach (FileDataBaseEntryParameter para in entry.Parameters)
                {
                    if ((!para.SuccessfullySet || para.CanChange) && para.UserEditable)
                    {
                        FileDataBaseEntryParameter newPara = para.Clone() as FileDataBaseEntryParameter;
                        toRet.Add(newPara);
                    }
                }
            }

            return toRet.ToArray();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
