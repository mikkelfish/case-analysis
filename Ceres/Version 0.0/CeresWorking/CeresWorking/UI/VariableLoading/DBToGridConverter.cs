﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CeresBase.IO.DataBase;
using System.Reflection;
using System.Collections;

namespace CeresBase.UI.VariableLoading
{
    [ValueConversion(typeof(IEnumerable), typeof(object[]))]
    public class DBToGridConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is IEnumerable)) return new object();


            List<string> names = new List<string>();
            List<Type> types = new List<Type>();
            List<string> categories = new List<string>();
            List<object> vals = new List<object>();
            List<bool> readOnly = new List<bool>();


            foreach (FileDataBaseEntry entry in (value as IEnumerable))
            {

                if (entry.Status == FileDataBaseEntryStatus.NeedsVars && entry.VariableParameters == null)
                {
                    entry.LoadVariableParameters();
                }

                if (entry.Status == FileDataBaseEntryStatus.NeedsAll)
                {
                    if (names.Count == 0)
                    {
                        foreach (FileDataBaseEntryParameter para in entry.Parameters)
                        {
                            if ((!para.SuccessfullySet || para.CanChange) && para.UserEditable)
                            {
                                names.Add(para.Name);
                                types.Add(para.Value.GetType());
                                categories.Add("File Properties");
                                vals.Add(para.Value);
                                readOnly.Add(false);
                            }
                        }
                    }
                }
                else if (entry.VariableParameters != null)
                {
                    for (int i = 0; i < entry.VariableParameters.Length; i++)
                    {
                        FileDataBaseVariableEntryParameter para = entry.VariableParameters[i];
                        if (categories.Contains(para.VariableName.Value))
                        {
                            continue;
                        }


                        PropertyInfo[] infos = para.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
                        foreach (PropertyInfo info in infos)
                        {
                            if (info.PropertyType.IsGenericType &&
                                info.PropertyType.GetGenericTypeDefinition() == typeof(VariableEntryParameterItem<>))
                            {
                                object obj = info.GetValue(para, null);

                                PropertyInfo nameProp = obj.GetType().GetProperty("Name");
                                PropertyInfo valProp = obj.GetType().GetProperty("Value");
                                PropertyInfo isFixedProp = obj.GetType().GetProperty("IsFixed");

                                string name = para.VariableName.Value + " " + (string)nameProp.GetValue(obj, null);
                                object val = valProp.GetValue(obj, null);
                                bool isFixed = (bool)isFixedProp.GetValue(obj, null);

                                if (val != null && !isFixed)
                                {
                                    names.Add(name);
                                    types.Add(val.GetType());
                                    categories.Add(para.VariableName.Value + " Properties");
                                    vals.Add(val);
                                    readOnly.Add(isFixed);
                                }
                            }

                        }

                    }
                }
            }

            if (names.Count == 0) return new object();

            return 
                MesosoftCommon.Utilities.Reflection.DynamicObjects.CreateObject(
                    types.ToArray(), categories.ToArray(), names.ToArray(), 
                    names.ToArray(), null, vals.ToArray(), readOnly.ToArray());
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
