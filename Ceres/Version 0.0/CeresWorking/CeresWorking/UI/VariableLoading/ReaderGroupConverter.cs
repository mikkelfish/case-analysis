﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CeresBase.UI.Navigation;
using CeresBase.IO.DataBase;

namespace CeresBase.UI.VariableLoading
{
    [ValueConversion(typeof(FileDataBaseEntry), typeof(string))]
    public class ReaderGroupConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is FileDataBaseEntry)
            {
                FileDataBaseEntry item = value as FileDataBaseEntry;
                if (item.Reader == null) return "Not Supported";
                return item.Reader.ToString();                
            }
            else return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
