using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.UI
{
    public class LoadingItemEventArgs : EventArgs
    {
        private bool done;
        public bool Done
        {
            get { return done; }
        }

        private string desc;
        public string Description
        {
            get { return desc; }
        }

        private int totalItems;
        public int TotalItems
        {
            get { return totalItems; }
        }

        private int itemNumber;
        public int ItemNumber
        {
            get { return itemNumber; }
        }

        public LoadingItemEventArgs(string description, int itemNumber, int totalItems, bool done)
        {
            this.desc = description;
            this.itemNumber = itemNumber;
            this.totalItems = totalItems;
            this.done = done;
        }
	
	
    }
}
