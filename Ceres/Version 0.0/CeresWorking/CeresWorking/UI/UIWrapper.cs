﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.UI
{
    public class UIWrapper<T>
    {

        public static implicit operator UIWrapper<T>(T obj)
        {
            return new UIWrapper<T>() { Object = obj };
        }

        public static implicit operator T(UIWrapper<T> obj)
        {
            return obj.Object;
        }

        public T Object { get; set; }

        public bool IsChecked { get; set; }
    }
}
