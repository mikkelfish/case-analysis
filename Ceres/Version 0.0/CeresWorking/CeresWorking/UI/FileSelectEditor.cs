using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Design;
using System.IO;
using System.Windows.Forms;

namespace CeresBase.UI
{
    public class FileSelectEditor : UITypeEditor
    {
        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            OpenFileDialog diag = new OpenFileDialog();
            if (diag.ShowDialog() == DialogResult.Cancel) return null;
            return diag.FileName;
        }

        public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }
    }
}
