﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using CeresBase.Projects;

namespace CeresBase.Projects
{
    [ValueConversion(typeof(Epoch), typeof(string))]
    public class EpochGroupConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Epoch)
            {
                Epoch epoch = value as Epoch;

                if(epoch.IsUser)
                 return epoch.Variable.Header.ExperimentName + " User";
                else return epoch.Variable.Header.ExperimentName + " File";
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
