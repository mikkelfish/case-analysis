﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;
using System.Runtime.Serialization;
using DatabaseUtilityLibrary;

namespace CeresBase.Projects
{
    class EpochDescriptorTranslator : ITranslator
    {
        [AlwaysSerialize]
        private class descriptorSerializer
        {
            public string Name { get; set; }
            public string Category { get; set; }
            public object Value { get; set; }
            public Guid Guid { get; set; }
        }

        #region ITranslator Members

        public object AttachedObject
        {
            get;
            set;
        }

        public bool SupportsType(Type t)
        {
            if (t == typeof(EpochDescriptor)) return true;
            return false;
        }

        public double Version
        {
            get { return 0.0; }
        }

        public bool Supports(object obj)
        {
            return obj is EpochDescriptor;
        }

        #endregion

        public EpochDescriptorTranslator()
        {

        }

        public EpochDescriptorTranslator(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            if (info.MemberCount == 0)
                return;

            descriptorSerializer ser = info.GetValue("desc", typeof(descriptorSerializer)) as descriptorSerializer;

            this.AttachedObject = new EpochDescriptor(ser.Name, ser.Category, ser.Value, ser.Guid);

        }

        #region ISerializable Members

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            EpochDescriptor desc = this.AttachedObject as EpochDescriptor;
            if (desc == null) return;

            descriptorSerializer ser = null;

            SerializationInfo passed = context.Context as SerializationInfo;
            if (passed != null)
            {
                ser = passed.GetValue("desc", typeof(descriptorSerializer)) as descriptorSerializer;
            }

            if (ser == null)
                ser = new descriptorSerializer();

            ser.Category = desc.Category;
            ser.Guid = desc.Guid;
            ser.Name = desc.Name;
            ser.Value = desc.Value;

            info.AddValue("desc", ser);
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return new EpochDescriptorTranslator();
        }

        #endregion
    }
}
