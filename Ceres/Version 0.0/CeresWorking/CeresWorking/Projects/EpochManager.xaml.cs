﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MesosoftCommon.DataStructures;

namespace CeresBase.Projects
{
    /// <summary>
    /// Interaction logic for EpochManager.xaml
    /// </summary>
    public partial class EpochManager : UserControl
    {
        public BeginInvokeObservableCollection<Epoch> Epochs
        {
            get
            {
                return EpochCentral.BindToManager(this.Dispatcher);
            }
        }

        public EpochManager()
        {
            InitializeComponent();

            BindingExpression ex = this.epochList.GetBindingExpression(ListBox.ItemsSourceProperty);

            CollectionViewSource source = this.epochList.TryFindResource("cvs") as CollectionViewSource;
            BindingExpression sc= BindingOperations.GetBindingExpression(source, CollectionViewSource.SourceProperty);
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {

        }
    }
}
