﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.Interfaces;
using System.ComponentModel;

namespace CeresBase.Projects
{
    public class EpochDescriptor : IConstructorProvider
    {

        public override bool Equals(object obj)
        {
            if (!(obj is EpochDescriptor))
                return false;

            EpochDescriptor ext = obj as EpochDescriptor;
            return ext.category == this.category && ext.name == this.name;
        }

        public override int GetHashCode()
        {
            return (this.category + this.name).GetHashCode();
        }

        private string name;
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public string Name
        {
            get { return this.name; }
        }

        private string category;
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public string Category
        {
            get { return this.category; }
        }

        private object val;
        public object Value
        {
            get
            {
                return this.val;
            }
            set
            {
                this.val = value;
            }
        }

        private Guid guid;
        public Guid Guid
        {
            get
            {
                return this.guid;
            }
        }

        public EpochDescriptor()
        {
            AssignUniqueGuid();
        }

        public EpochDescriptor(string name, string category, object val)
        {
            this.name = name;
            this.category = category;
            this.val = val;
            AssignUniqueGuid();
        }

        public EpochDescriptor(string name, string category, object val, Guid guid)
        {
            this.name = name;
            this.category = category;
            this.val = val;
            this.guid = guid;
        }

        private void AssignUniqueGuid()
        {
            Guid newGuid;
            do
            {
                newGuid = Guid.NewGuid();
            } while (EpochDescriptorCentral.GuidAlreadyUsed(newGuid));

            this.guid = newGuid;
        }

        #region IConstructorProvider Members

        public object[] ConstructionArgs
        {
            get { return new object[] { this.name, this.category, this.val }; }
        }

        #endregion
    }

    //public class EpochDescriptorSerializer
    //{
    //    public string Name { get; set; }
    //    public string Category { get; set; }
    //    public object Value { get; set; }
    //    public Guid Guid { get; set; }

    //    public static EpochDescriptorSerializer Serialize(EpochDescriptor toSerialize)
    //    {
    //        EpochDescriptorSerializer ret = new EpochDescriptorSerializer();
    //        ret.Name = toSerialize.Name;
    //        ret.Category = toSerialize.Category;
    //        ret.Value = toSerialize.Value;
    //        ret.Guid = toSerialize.Guid;

    //        return ret;
    //    }

    //    public static EpochDescriptor Deserialize(EpochDescriptorSerializer toDeserialize)
    //    {
    //        EpochDescriptor ret = new EpochDescriptor(toDeserialize.Name, toDeserialize.Category,
    //                                                    toDeserialize.Value, toDeserialize.Guid);
    //        return ret;
    //    }

    //    public bool ValuesMatch(EpochDescriptor match)
    //    {
    //        return (Name == match.Name && Category == match.Category && Value == match.Value);
    //    }
    //}
}
