﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
using CeresBase.Projects.Flowable;
using CeresBase.FlowControl;

namespace CeresBase.Projects
{
    /// <summary>
    /// Interaction logic for EpochDescriptorControl.xaml
    /// </summary>
    public partial class EpochDescriptorControl : UserControl, INotifyPropertyChanged
    {
        private int epochDescTypeIndex = -1;
        public int EpochDescTypeIndex
        {
            get
            {
                return epochDescTypeIndex;
            }
            set
            {
                if (epochDescTypeIndex != value)
                {
                    epochDescTypeIndex = value;
                    OnPropertyChanged("EpochDescTypeIndex");
                    if (epochDescTypeIndex == 3)
                        AdapterPropPicker.Visibility = Visibility.Visible;
                    else
                        AdapterPropPicker.Visibility = Visibility.Collapsed;
                }
            }
        }

        private ObservableCollection<string> epochDescTypeEnumList = new ObservableCollection<string>();
        public ObservableCollection<string> EpochDescTypeEnumList
        {
            get
            {
                return epochDescTypeEnumList;
            }
        }

        public EpochDescriptor ReturnValue()
        {
            string valString = EpochDescValue.Text;
            object val = new object();

            if (EpochDescType.SelectedIndex == 0)
            {
                int intVal;
                if (!int.TryParse(valString, out intVal))
                    MessageBox.Show("Couldn't parse your Value into an integer!");
                else
                    val = intVal;
            }
            else if (EpochDescType.SelectedIndex == 1)
            {
                double doubleVal;
                if (!double.TryParse(valString, out doubleVal))
                    MessageBox.Show("Couldn't parse your Value into a decimal!");
                else
                    val = doubleVal;
            }
            else if (EpochDescType.SelectedIndex == 2)
                val = valString;

            EpochDescriptor ret;
            if (val is double || val is int || val is string)
                ret = new EpochDescriptor(EpochDescName.Text, EpochDescCategory.Text, val);
            else
                ret = null;

            return ret;
        }

        public EpochDescriptorControl()
        {
            InitializeComponent();
            epochDescTypeEnumList.Add("Integer");
            epochDescTypeEnumList.Add("Decimal");
            epochDescTypeEnumList.Add("Text");

            this.IsVisibleChanged += new DependencyPropertyChangedEventHandler(EpochDescriptorControl_IsVisibleChanged);

            //epochDescTypeEnumList.Add("Pick from an Adapter");

            //IBatchFlowable[] flowables = CeresBase.General.Utilities.CreateInterfaces<IBatchFlowable>(null);

            //foreach (IBatchFlowable flow in flowables)
            //{
            //    FlowableBatchProcessorAdapter adapter = new FlowableBatchProcessorAdapter(flow);
            //    adapter.Name = flow.ToString();
            //    this.AdapterPropPicker.AvailableAdapters.Add(adapter);
            //}

            //GraphPickerAdapter graphAdapter = new GraphPickerAdapter();
            //this.AdapterPropPicker.AvailableAdapters.Add(graphAdapter);

            //EpochDescriptorSetAdapter epochSetAdapter = new EpochDescriptorSetAdapter();
            //this.AdapterPropPicker.AvailableAdapters.Add(epochSetAdapter);
        }

        void EpochDescriptorControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.EpochDescCategory.Focus();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
