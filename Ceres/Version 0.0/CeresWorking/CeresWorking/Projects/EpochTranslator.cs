﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;
using CeresBase.Data;
using CeresBase.UI.VariableLoading;
using System.Runtime.Serialization;
using MesosoftCommon.Utilities.Settings;
using DatabaseUtilityLibrary;

namespace CeresBase.Projects
{
    public class EpochTranslator : ITranslator
    {
      

        //public static string[] GetPreviousEpochNames()
        //{
        //    return (from epochSaver save in previousEpochs
        //            where save.IsUser
        //            select save.ExptName).ToArray();
        //}


        public object Clone()
        {
            return new EpochTranslator();
        }

        [AlwaysSerialize]
        private class epochSaver
        {
            public string VarName { get; set; }
            public string ExptName { get; set; }
            public string Description { get; set; }
            public float BeginTime { get; set; }
            public float EndTime { get; set; }
            public string Name { get; set; }
            public bool IsUser { get; set; }
            public bool AppliesToExpt { get; set; }
            public string Units { get; set; }
            public EpochAttachment[] Attachments { get; set; }
        }

        #region ITranslator Members

        public Epoch EpochToTranslate { get; set; }

        object ITranslator.AttachedObject
        {
            get
            {
                return this.EpochToTranslate;
            }
            set
            {
                this.EpochToTranslate = value as Epoch;
            }
        }

        public bool SupportsType(Type t)
        {
            if (t == typeof(Epoch)) return true;
            return false;
        }

        public double Version
        {
            get { return 0.0; }
        }

        #endregion

        #region ISerializable Members

        //private static void writeEpochs()
        //{
        //    foreach (Epoch e in epochs)
        //    {
        //        epochSaver saver = previousEpochs.SingleOrDefault(prev => prev.VarName == e.Variable.Header.VarName &&
        //            prev.ExptName == e.Variable.Header.ExperimentName &&
        //            prev.BeginTime == e.BeginTime && prev.EndTime == e.EndTime &&
        //            prev.Name == e.Name);

        //        if (saver == null)
        //        {
        //            saver = new epochSaver();
        //            previousEpochs.Add(saver);
        //        }

        //        saver.AppliesToExpt = e.AppliesToExperiment;
        //        saver.BeginTime = e.BeginTime;
        //        saver.Description = e.Description;
        //        saver.EndTime = e.EndTime;
        //        saver.IsUser = e.IsUser;
        //        saver.Name = e.Name;
        //        saver.ExptName = e.Variable.Header.ExperimentName;
        //        saver.VarName = e.Variable.Header.VarName;
        //        saver.Attachments = e.Attachments;


        //    }

        //    if (!Directory.Exists(storagePath))
        //    {
        //        Directory.CreateDirectory(storagePath);
        //    }

        //    using (FileStream stream = new FileStream(storagePath + "EpochCentral.xaml", FileMode.Create, FileAccess.Write))
        //    {
        //        MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(previousEpochs, stream);
        //    }
        //}

        public EpochTranslator()
        {

        }

        protected EpochTranslator(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            if (info.MemberCount == 0) return;

            epochSaver saver = info.GetValue("Saver", typeof(epochSaver)) as epochSaver;

            Variable var = VariableSource.GetVariable(saver.VarName, saver.Units, saver.ExptName);

            Epoch epoch = null;

            if (var != null)
            {
                epoch = new Epoch(var, saver.Name, (float)Math.Round(saver.BeginTime, 2), (float)Math.Round(saver.EndTime, 2)) { AppliesToExperiment = false, Description = saver.Description, IsUser = saver.IsUser };
            }
            else
            {
                epoch = new Epoch(null, saver.Name, (float)Math.Round(saver.BeginTime, 2), (float)Math.Round(saver.EndTime, 2))
                {
                    AppliesToExperiment = false,
                    IsUser = saver.IsUser,
                    Description =  saver.Description + "$EXPT$" + saver.ExptName + "$VAR$" + saver.VarName
                };
            }
            if (saver.Attachments != null)
            {
                foreach (EpochAttachment attach in saver.Attachments)
                {
                    epoch.AddAttachment(attach);
                }
            }

            this.EpochToTranslate = epoch;
        }

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            if (this.EpochToTranslate.EndTime == this.EpochToTranslate.BeginTime || this.EpochToTranslate.Variable == null) return;

            epochSaver saver = null;

            SerializationInfo passed = context.Context as SerializationInfo;
            if (passed != null)
            {
                saver = passed.GetValue("Saver", typeof(epochSaver)) as epochSaver;
            }

            if (saver == null)
                saver = new epochSaver();


            saver.AppliesToExpt = this.EpochToTranslate.AppliesToExperiment;
            saver.BeginTime = this.EpochToTranslate.BeginTime;
            saver.Description = this.EpochToTranslate.Description;
            saver.EndTime = this.EpochToTranslate.EndTime;
            saver.IsUser = this.EpochToTranslate.IsUser;
            saver.Name = this.EpochToTranslate.Name;
            saver.ExptName = this.EpochToTranslate.Variable.Header.ExperimentName;
            saver.VarName = this.EpochToTranslate.Variable.Header.VarName;
            saver.Units = this.EpochToTranslate.Variable.Header.UnitsName;
            saver.Attachments = this.EpochToTranslate.Attachments;

            info.AddValue("Saver", saver);

        }

        #endregion

        #region ITranslator Members


        public bool Supports(object obj)
        {
            if (obj is Epoch) return true;

            return false;
        }

        #endregion
    }
}
