﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows;
using MesosoftCommon.DataStructures;
using System.Windows.Threading;
using System.IO;
using CeresBase.Data;
using MesosoftCommon.Utilities.Settings;
using CeresBase.IO.Serialization;
using MesosoftCommon.Threading;
using MesosoftCommon.ExtensionMethods;

namespace CeresBase.Projects
{
    public static class EpochCentral
    {


        private static ObservableCollection<Epoch> savedepochs = null;

        private static ObservableCollection<Epoch> epochs = new ObservableCollection<Epoch>();

        //public static ObservableCollection<Epoch> Epochs { get { return epochs; } }
        private static object lockable = new object();

        public static BeginInvokeObservableCollection<Epoch> Epochs
        {
            get
            {
                return ThreadSafeBinding.BindToManager<Epoch>(Dispatcher.CurrentDispatcher, "EpochCentral");
            }
        }


        public static void AddEpochs(Epoch[] epochs)
        {
            foreach (Epoch epoch in epochs)
            {
                AddEpoch(epoch);
            }

            
        }

        public static bool EpochExists(Epoch epoch)
        {
            return EpochCentral.Epochs.Any(test => test == epoch);
        }

        public static void AddEpoch(Epoch epoch)
        {
            if (!EpochCentral.savedepochs.Any(test => test == epoch))
            {
                EpochCentral.savedepochs.Add(epoch);
            }
        }

        //public static void RemoveEpochs(Epoch[] epochs)
        //{
        //    //foreach (Epoch epoch in epochs)
        //    //{
        //    //    EpochCentral.savedepochs.Remove(epoch);
                
        //    //}

        //    //SerializationCentral.DeleteFromCollection(typeof(EpochCentral), "savedepochs", epochs);

        //}

        public static BeginInvokeObservableCollection<Epoch> BindToManager(Dispatcher dispatch)
        {
            return ThreadSafeBinding.BindToManager<Epoch>(dispatch, "EpochCentral");
        }

   
        static EpochCentral()
        {
                savedepochs = NewSerializationCentral.RegisterCollection<ObservableCollection<Epoch>>(typeof(EpochCentral), "savedepochs");

                epochs = new ObservableCollection<Epoch>();

                ThreadSafeBinding.RegisterCollection<Epoch>(epochs, "EpochCentral");

                //savedepochs.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(savedepochs_CollectionChanged);
                //epochs.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(epochs_CollectionChanged);

                NewSerializationCentral.DatabaseChanging += new DatabaseChangingEventHandler(SerializationCentral_DatabaseChanging);
            //SerializationCentral.DatabaseChanged += new EventHandler(SerializationCentral_DatabaseChanged);
         //   readEpochs();
        }

        static void SerializationCentral_DatabaseChanging(object sender, DatabaseChangingEventArgs e)
        {
            savedepochs.Clear();
            epochs.Clear();
        }

        //static void SerializationCentral_DatabaseChanged(object sender, EventArgs e)
        //{
        //    //savedepochs.Clear();
        //    savedepochs.Clear();
        //    epochs.Clear();
        //}

        //static void savedepochs_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add && e.NewItems != null)
        //    {
        //        Epoch[] es = e.NewItems.Cast<Epoch>().ToArray();
        //        epochs.AddRange(es);

        //        //foreach (Epoch epoch in e.NewItems)
        //        //{
        //        //    epochs.Add(epoch);
        //        //}
        //    }
        //    else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove && e.OldItems != null)
        //    {
        //        foreach (Epoch epoch in e.OldItems)
        //        {
        //            epochs.Remove(epoch);
        //        }
        //    }
        //}

        public static void VariableRemoved(Variable var)
        {
            Epoch[] storedEpochs = (from Epoch epoch in EpochCentral.epochs
                                    where epoch.Variable == var
                                    select epoch).ToArray();

            foreach (Epoch e in storedEpochs)
            {
                EpochCentral.epochs.Remove(e);
            }
        }

        public static void DeleteEpoch(string exptName, string varName, string epochName)
        {
            Epoch toDel = EpochCentral.savedepochs.SingleOrDefault(v => v.Name == epochName && v.Variable.Header.ExperimentName == exptName &&
                v.Variable.Header.VarName == varName);
            if (toDel == null) return;
            EpochCentral.savedepochs.Remove(toDel);
            
            Epoch toRemove = EpochCentral.epochs.SingleOrDefault(v => v.Name == epochName && v.Variable.Header.ExperimentName == exptName &&
                  v.Variable.Header.VarName == varName);
            if (toRemove != null)
            {
                EpochCentral.epochs.Remove(toRemove);
            }
        }

        public static void DeleteVariableEpochs(string exptName, string varName)
        {
            var allVarEpochs = (from Epoch epoch in EpochCentral.savedepochs
                               where epoch.Variable.Header.ExperimentName == exptName && epoch.Variable.Header.VarName == varName
                               select epoch).ToArray();
            foreach (var epoch in allVarEpochs)
            {
                DeleteEpoch(epoch.Variable.Header.ExperimentName, epoch.Variable.Header.VarName, epoch.Name);
            }
        }

        public static void VariablesLoaded()
        {
            //SerializationCentral.UpdateDelayedCollection(typeof(EpochCentral), "savedepochs");

            EpochCentral.ProcessPendingEpochs();          
        }

        public static void ProcessPendingEpochs()
        {
            List<Epoch> toAdd = new List<Epoch>();
            foreach (Epoch epoch in savedepochs)
            {
                string desc = epoch.Description;

                if (epoch.Variable == null)
                {
                    if (desc.Contains("$EXPT$"))
                    {
                        int exIndx = desc.IndexOf("$EXPT$") + "$EXPT$".Length;
                        int varIndex = desc.IndexOf("$VAR$");

                        string expt = desc.Substring(exIndx, varIndex - exIndx);

                        varIndex += "$VAR$".Length;
                        string var = desc.Substring(varIndex);

                        Variable variable = VariableSource.GetVariable(var, "", expt);
                        if (variable != null)
                        {
                            epoch.Variable = variable;
                            epoch.Description = desc.Substring(0, desc.IndexOf("$EXPT$"));
                            if (!epochs.Any(e => e.Variable.Header.ExperimentName == epoch.Variable.Header.ExperimentName &&
                                e.Variable.Header.VarName == epoch.Variable.Header.VarName &&
                                e.Name == epoch.Name))
                            {
                                toAdd.Add(epoch);
                            }
                        }
                    }
                }
                else
                {
                    if (!epochs.Any(e => e.Variable.Header.ExperimentName == epoch.Variable.Header.ExperimentName &&
                        e.Variable.Header.VarName == epoch.Variable.Header.VarName &&
                        e.Name == epoch.Name))
                    {

                        toAdd.Add(epoch);
                    }
                }
            }

            epochs.AddRange(toAdd.ToArray());

            //foreach (Epoch e in toAdd)
            //{
            //    savedepochs.Remove(e);
            //}

            //savedepochs.AddRange(toAdd.ToArray());
            //pendingEpochs.Clear();
        }
    
        #region Epoch merge

        public static void ReadOldEpochs()
        {
            string storagePath = Paths.DefaultPath.LocalPath + "Epochs\\";

            List<epochSaver> previousEpochs = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(storagePath + "EpochCentral.xaml") as List<epochSaver>;
            if (previousEpochs == null) previousEpochs = new List<epochSaver>();

            foreach (Variable var in VariableSource.Variables)
            {
                var fullVars = from epochSaver save in previousEpochs
                               where save.VarName == var.Header.VarName && save.ExptName == var.Header.ExperimentName &&
                                    save.AppliesToExpt == false
                               select save;

                foreach (epochSaver saver in fullVars)
                {
                    Epoch epoch = new Epoch(var, saver.Name, saver.BeginTime, saver.EndTime) { AppliesToExperiment = false, Description = saver.Description, IsUser = saver.IsUser };

                    if (saver.Attachments != null)
                    {
                        foreach (EpochAttachment attach in saver.Attachments)
                        {
                            epoch.AddAttachment(attach);
                        }
                    }

                    EpochCentral.AddEpoch(epoch);
                }

                var exptVars = from epochSaver save in previousEpochs
                               where save.ExptName == var.Header.ExperimentName && save.AppliesToExpt == true
                               select save;

                foreach (epochSaver saver in exptVars)
                {
                    Epoch epoch = new Epoch(var, saver.Name, saver.BeginTime, saver.EndTime) { AppliesToExperiment = true, Description = saver.Description, IsUser = saver.IsUser };

                    if (saver.Attachments != null)
                    {
                        foreach (EpochAttachment attach in saver.Attachments)
                        {
                            epoch.AddAttachment(attach);
                        }
                    }

                    EpochCentral.AddEpoch(epoch);
                }
            }

            EpochCentral.ProcessPendingEpochs();
        }

        private class epochSaver
        {
            public string VarName { get; set; }
            public string ExptName { get; set; }
            public string Description { get; set; }
            public float BeginTime { get; set; }
            public float EndTime { get; set; }
            public string Name { get; set; }
            public bool IsUser { get; set; }
            public bool AppliesToExpt { get; set; }
            public string Units { get; set; }
            public EpochAttachment[] Attachments { get; set; }
        }

        #endregion      
    }
}
