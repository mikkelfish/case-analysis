﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace CeresBase.Projects
{
    public class EpochAttachment : INotifyPropertyChanged
    {
        public bool AllowMultiple { get; set; }

        private object item;
        public object Item 
        {
            get
            {
                return this.item;
            }
            set
            {
                this.item = value;
                this.OnPropertyChanged("Item");
            }
        }
        public string Name { get; set; }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler ev = PropertyChanged;
            if (ev != null)
            {
                ev(this, new PropertyChangedEventArgs(name));
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
