﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;
using System.ComponentModel;
using System.Runtime.Serialization;
using DatabaseUtilityLibrary;

namespace CeresBase.Projects
{
    class EpochDescriptorSetTranslator : ITranslator
    {
        [AlwaysSerialize]
        private class epochDescriptorSerialization
        {
            public string Description { get; set; }
            
            private List<Guid> epochs = new List<Guid>();
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
            public List<Guid> Epochs { get { return epochs; } }
        }

        #region ITranslator Members

        public object AttachedObject
        {
            get;
            set;
        }

        public bool SupportsType(Type t)
        {
            if (t == typeof(EpochDescriptorSet)) return true;
            return false;
        }

        public double Version
        {
            get { return 0.0; }
        }

        public bool Supports(object obj)
        {
            return obj is EpochDescriptorSet;
        }

        #endregion

        public EpochDescriptorSetTranslator(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            if (info.MemberCount == 0) return;

            epochDescriptorSerialization serial = info.GetValue("serial", typeof(epochDescriptorSerialization)) as epochDescriptorSerialization;

            EpochDescriptorSet ret = new EpochDescriptorSet();

            foreach (Guid id in serial.Epochs)
            {
                EpochDescriptor ed = EpochDescriptorCentral.GetEpochDescriptor(id);
                if (ed != null)
                    ret.Epochs.Add(ed);
            }

            ret.Description = serial.Description;

            this.AttachedObject = ret;
        }

        public EpochDescriptorSetTranslator()
        {

        }

        #region ISerializable Members

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            EpochDescriptorSet set = this.AttachedObject as EpochDescriptorSet;
            if (set == null) return;

            epochDescriptorSerialization serial = null;

            SerializationInfo passed = context.Context as SerializationInfo;
            if (passed != null)
            {
                serial = passed.GetValue("serial", typeof(epochDescriptorSerialization)) as epochDescriptorSerialization;
            }

            if (serial == null)
                serial = new epochDescriptorSerialization();

            serial.Description = set.Description;
            foreach (EpochDescriptor desc in set.Epochs)
            {
                serial.Epochs.Add(desc.Guid);
            }

            info.AddValue("serial", serial);

        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return new EpochDescriptorSetTranslator();
        }

        #endregion
    }
}
