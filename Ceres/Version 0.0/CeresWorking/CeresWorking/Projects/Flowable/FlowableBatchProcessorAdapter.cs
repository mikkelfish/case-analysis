﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl;
using CeresBase.Data;
using System.Globalization;
using System.Windows.Controls;

namespace CeresBase.Projects.Flowable
{
    public class FlowableBatchProcessorAdapter : CeresBase.FlowControl.IFlowControlAdapter
    {
        public object[] Output { get; set; }

        public IBatchFlowable BatchProcess 
        { 
            get; 
            set; 
        }

        private BatchProcessableParameter[] paras;

        public FlowableBatchProcessorAdapter(IBatchFlowable process)
        {
            this.BatchProcess = process;
            this.paras = this.BatchProcess.GetParameters();
            this.Name = this.BatchProcess.ToString();
        }

        public FlowableBatchProcessorAdapter()
        {

        }


        #region IFlowControlAdapter Members

        public int ID
        {
            get;
            set;
        }

        public event EventHandler<CeresBase.FlowControl.FlowControlProcessEventArgs> RunComplete;

        public string[] SupplyPropertyNames()
        {
            string[] allNames = this.paras.Select(p => p.Name).ToArray();
            List<string> all = new List<string>(allNames);
            all.AddRange(this.BatchProcess.OutputDescription);
            return all.ToArray();
        }

        public object SupplyPropertyValue(string sourcePropertyName)
        {
            int index = -1;
            for (int i = 0; i < this.BatchProcess.OutputDescription.Length; i++)
            {
                if (sourcePropertyName == this.BatchProcess.OutputDescription[i])
                {
                    index = i;
                    break;
                }
            }

            if (index > -1)
            {
                if (this.Output == null) return null;
                return this.Output[index];
            }

            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == sourcePropertyName);
            if (para == null)
            {
                throw new ArgumentException("Property " + sourcePropertyName + " not found");
            }

            return para.Val;
        }

        public void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            int index = -1;
            for (int i = 0; i < this.BatchProcess.OutputDescription.Length; i++)
            {
                if (targetPropertyName == this.BatchProcess.OutputDescription[i])
                {
                    index = i;
                    break;
                }
            }

            if (index > -1)
            {
                if (this.Output != null)
                {
                    this.Output[index] = propertyValue;
                }
                return;
            }

            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == targetPropertyName);
            if (para == null)
            {
                throw new ArgumentException("Property " + targetPropertyName + " not found");
            }
            para.Val = propertyValue;
        }

        public bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            int index = -1;
            for (int i = 0; i < this.BatchProcess.OutputDescription.Length; i++)
            {
                if (targetPropertyName == this.BatchProcess.OutputDescription[i])
                {
                    index = i;
                    break;
                }
            }

            if (index > -1) return true;

            return this.paras.Any(p => p.Name == targetPropertyName);
        }

        public CeresBase.FlowControl.FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
        {
            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == sourcePropertyName);
            if (para == null)
            {
                throw new ArgumentException("Property " + sourcePropertyName + " not found");
            }
            FlowControlParameterDescriptor desc = new FlowControlParameterDescriptor();
            desc.Version = "0.0";
            desc.Type = para.Val.GetType();
            return new FlowControlParameterDescriptor[] { desc };
        }

        public CeresBase.FlowControl.FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
        {
            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == targetPropertyName);
            if (para == null)
            {
                throw new ArgumentException("Property " + targetPropertyName + " not found");
            }
            FlowControlParameterDescriptor desc = new FlowControlParameterDescriptor();
            desc.Version = "0.0";
            desc.Type = para.Val.GetType();
            return new FlowControlParameterDescriptor[] { desc };
        }

        public object SupplyPropertyValueAs(string sourcePropertyName, CeresBase.FlowControl.FlowControlParameterDescriptor supplyType)
        {
            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == sourcePropertyName);
            if (para == null)
            {
                throw new ArgumentException("Property " + sourcePropertyName + " not found");
            }

            return CeresBase.General.Utilities.Convert(para.Val, supplyType.Type);
        }

        public void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, CeresBase.FlowControl.FlowControlParameterDescriptor receiveType)
        {
            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == targetPropertyName);
            if (para == null)
            {
                throw new ArgumentException("Property " + targetPropertyName + " not found");
            }

            para.Val = CeresBase.General.Utilities.Convert(propertyValue, receiveType.Type);
        }

        /// <summary>
        /// Run the adapter
        /// </summary>
        public void RunAdapter()
        {
           

            object[] value = this.BatchProcess.Run(this.paras);

            if (this.RunComplete != null)
            {
                this.RunComplete(this, new FlowControlProcessEventArgs(this, value));
            }

            this.Output = value;
        }



        public Type PropertyType(string targetPropertyName)
        {
            int index = -1;
            for (int i = 0; i < this.BatchProcess.OutputDescription.Length; i++)
            {
                if (targetPropertyName == this.BatchProcess.OutputDescription[i])
                {
                    index = i;
                    break;
                }
            }

            if (index > -1)
            {
                if (this.Output != null && this.Output[index] != null) return this.Output[index].GetType();
                return typeof(object);
            }

            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == targetPropertyName);
            if (para == null)
            {
                throw new ArgumentException("Property " + targetPropertyName + " not found");
            }

            if (para.Val == null)
            {
                if(para.TargetType == null)
                    return typeof(object);
                return para.TargetType;
            }

            return para.Val.GetType();
        }




        public object[] GetValuesForSerialization()
        {
            return new object[]{this.BatchProcess.GetType().AssemblyQualifiedName};
        }

        public void LoadValuesFromSerialization(object[] values)
        {
            string type = values[0] as string;
            Type batch = Type.GetType(type);
            this.BatchProcess = Activator.CreateInstance(batch) as IBatchFlowable;
            this.paras = this.BatchProcess.GetParameters();
        }

        public string Name
        {
            get;
            set;
        }




        public bool CanReceivePropertyLinks(string targetPropertyname)
        {
            int index = -1;
            for (int i = 0; i < this.BatchProcess.OutputDescription.Length; i++)
            {
                if (targetPropertyname == this.BatchProcess.OutputDescription[i])
                {
                    index = i;
                    break;
                }
            }

            if (index > -1) return false;

            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == targetPropertyname);
            if (para == null)
            {
                throw new ArgumentException("Property " + targetPropertyname + " not found");
            }

            return para.CanLinkTo;
        }




        public string Category
        {
            get
            {
                string toRet = "Analysis Tools";
                if (this.BatchProcess is IRoutineCategoryProvider)
                {
                    toRet = (this.BatchProcess as IRoutineCategoryProvider).Category;
                }

                return toRet;
            }
            set
            {

            }
        }
        

        public bool CanEditProperty(string targetPropertyname)
        {
            int index = -1;
            for (int i = 0; i < this.BatchProcess.OutputDescription.Length; i++)
            {
                if (targetPropertyname == this.BatchProcess.OutputDescription[i])
                {
                    index = i;
                    break;
                }
            }

            if (index > -1) return false;

            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == targetPropertyname);
            if (para == null)
            {
                throw new ArgumentException("Property " + targetPropertyname + " not found");
            }
            return para.Editable;
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            FlowableBatchProcessorAdapter adapter = new FlowableBatchProcessorAdapter(this.BatchProcess);
            adapter.paras = this.BatchProcess.GetParameters();

            for (int i = 0; i < this.paras.Length; i++)
            {
                adapter.paras[i].Name = this.paras[i].Name;
                adapter.paras[i].Val = this.paras[i].Val;
            }

            adapter.Name = this.Name;

            return adapter;
        }

        #endregion






        #region IFlowControlAdapter Members


        private Process hostProcess;
        public Process HostProcess
        {
            get
            {
                return hostProcess;
            }
            set
            {
                hostProcess = value;
            }
        }

        public bool ShouldSerialize(string targetPropertyname)
        {
            return true;
        }

   
        public ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            int index = -1;
            for (int i = 0; i < this.BatchProcess.OutputDescription.Length; i++)
            {
                if (targetPropertyname == this.BatchProcess.OutputDescription[i])
                {
                    index = i;
                    break;
                }
            }

            if (index > -1) return new ValidationStatus(ValidationState.Pass);

            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == targetPropertyname);
            if (para == null)
            {
                throw new ArgumentException("Property " + targetPropertyname + " not found");
            }

            if (para.Validator == null) return new ValidationStatus(ValidationState.Pass);

            ValidationResult result = para.Validator.Validate(targetValue, CultureInfo.CurrentCulture);

            string error = "";
            if (result.ErrorContent != null)
            {
                error = result.ErrorContent.ToString();
            }

            if (result.IsValid) return new ValidationStatus(ValidationState.Pass, error);
            return new ValidationStatus(ValidationState.Fail, error);
        }

        public bool CanSendPropertyLinks(string targetPropertyname)
        {
            int index = -1;
            for (int i = 0; i < this.BatchProcess.OutputDescription.Length; i++)
            {
                if (targetPropertyname == this.BatchProcess.OutputDescription[i])
                {
                    index = i;
                    break;
                }
            }

            if (index > -1)
                return true;

            BatchProcessableParameter para = this.paras.SingleOrDefault(p => p.Name == targetPropertyname);
            if (para == null)
            {
                throw new ArgumentException("Property " + targetPropertyname + " not found");
            }

            return para.CanLinkFrom;
        }

        public bool IsOutputProperty(string targetPropertyname)
        {
            int index = -1;
            for (int i = 0; i < this.BatchProcess.OutputDescription.Length; i++)
            {
                if (targetPropertyname == this.BatchProcess.OutputDescription[i])
                {
                    index = i;
                    break;
                }
            }

            if (index > -1) return true;
            return false;
        }

        #endregion

        #region IFlowControlAdapter Members


        public bool HasUserInteraction
        {
            get 
            {
                bool toRet = false;

                if (this.BatchProcess is IUIProvider)
                {
                    toRet = (this.BatchProcess as IUIProvider).HasUserInteraction;
                }

                return toRet;

                
            }
        }

        #endregion

        public object SupplyPropertyDescription(string propertyName)
        {
            return null;
        }

        public object SupplyAdapterDescription()
        {
            return null;
        }
    }
}
