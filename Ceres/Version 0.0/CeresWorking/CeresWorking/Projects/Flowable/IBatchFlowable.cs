﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Data;

namespace CeresBase.Projects.Flowable
{
    public interface IBatchFlowable
    {
        BatchProcessableParameter[] GetParameters();
        object[] Run(BatchProcessableParameter[] parameters);
        string[] OutputDescription { get; }
    }
}
