﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace CeresBase.Projects.Flowable
{
    public class BatchProcessableParameter
    {
        public string Name { get; set; }
        public object Val { get; set; }
        public bool Editable { get; set; }
        public string Description { get; set; }
        public ValidationRule Validator { get; set; }
        public bool CanLinkTo { get; set; }
        public bool CanLinkFrom { get; set; }

        public Type TargetType { get; set; }

        public BatchProcessableParameter()
        {
            this.Editable = true;
            this.CanLinkFrom = true;
            this.CanLinkTo = true;
        }
    }
}
