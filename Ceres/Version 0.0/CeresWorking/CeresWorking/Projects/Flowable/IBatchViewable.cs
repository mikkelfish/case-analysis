using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.Projects.Flowable
{
    public interface IBatchViewable
    {
        //System.Windows.Controls.Control[] View(IBatchViewable[] viewable, bool saveToManager);
        System.Windows.Controls.Control[] View(IBatchViewable[] viewable, out IBatchViewable[] notUsed);
    }
}
