﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.Projects.Flowable
{
    public interface IUIProvider
    {
        bool HasUserInteraction { get; }
    }
}
