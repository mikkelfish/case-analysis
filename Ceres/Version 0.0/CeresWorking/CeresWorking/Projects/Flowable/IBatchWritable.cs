using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.Projects.Flowable
{
    public interface IBatchWritable
    {
        IBatchWritable[] WriteToDisk(IBatchWritable[] toWrite);
    }
}
