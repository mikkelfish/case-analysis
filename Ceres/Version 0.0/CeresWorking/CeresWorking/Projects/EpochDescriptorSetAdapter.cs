﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl;
using CeresBase.Data;
using System.Reflection;
using System.ComponentModel;

namespace CeresBase.Projects
{
    public class EpochDescriptorSetAdapter : StorageAdapter
    {

        public EpochDescriptorSetAdapter()
        {
            List<string> setnames = new List<string>();
            foreach (EpochDescriptorSet set in EpochDescriptorSetCentral.EpochDescriptorSets)
            {
                setnames.Add(set.Description);
            }

            Type enumType = MesosoftCommon.Utilities.Reflection.DynamicObjects.CreateEnum(setnames.ToArray());

            this.DescriptorType = enumType.InvokeMember("", BindingFlags.CreateInstance, null, null, null);

            
        }

        public override Process HostProcess
        {
            get
            {
                return base.HostProcess;
            }
            set
            {
                if (base.HostProcess != null)
                {
                    base.HostProcess.PropertyEdited -= new EventHandler<ProcessPropertyChangedArgs>(HostProcess_PropertyEdited);
                }

                base.HostProcess = value;
                if (base.HostProcess != null)
                {
                    base.HostProcess.PropertyEdited += new EventHandler<ProcessPropertyChangedArgs>(HostProcess_PropertyEdited);
                }
            }
        }

        void HostProcess_PropertyEdited(object sender, ProcessPropertyChangedArgs e)
        {
            Process process = sender as Process;
            if (process == null) return;

            ProcessPropertyChangedInfo info = e.PropertiesChanged.SingleOrDefault(p => p.PropertyName == "Descriptor Type");

            if (info != null)
            {
                    this.DescriptorType = info.NewValue;
                    process.ReloadPropertiesFromAdapter();
            }
        }


        #region IFlowControlAdapter Members

       


        public override string Category
        {
            get
            {
                return "Variable Interaction";
            }
            set
            {
                
            }
        }

        public EpochDescriptorSet DescriptorSet { get; set; }

        private Variable var;
        public Variable Variable 
        {
            get
            {
                return this.var;
            }
            set
            {
                this.var = value;
                this.VariableName = this.var.Header.Description;
                this.ExperimentName = this.var.Header.ExperimentName;

                PropertyInfo res = this.var.GetType().GetProperty("Resolution");
                if (res != null)
                {
                    this.Resolution = (double)res.GetValue(this.var, null);
                }
            }
        }

        public override string Name
        {
            get
            {
                return "Descriptor Set";
            }
            set
            {
                base.Name = value;
            }
        }

        public float StartTime { get; set; }
        public float EndTime { get; set; }

        public string VariableName { get; private set; }
        public double Resolution { get; private set; }

        public string ExperimentName { get; private set; }



        private object descType;
        
        public object DescriptorType 
        {
            get
            {
                return this.descType;
            }
            set
            {
                this.descType = value;

                this.DescriptorSet =  EpochDescriptorSetCentral.EpochDescriptorSets.SingleOrDefault(set => set.Description == this.descType.ToString());
            }
        }

        public Epoch Epoch 
        {
            set
            {
                this.Variable = value.Variable;
                this.StartTime = value.BeginTime;
                this.EndTime = value.EndTime;
            }
        }

        private float[] data;

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;
       

        public override string[] SupplyPropertyNames()
        {
            List<string> names = new List<string>();

            names.Add("Data");
            names.Add("Variable Name");
            names.Add("Resolution");
            names.Add("Start Time");
            names.Add("End Time");
            names.Add("Experiment Name");

            if (this.DescriptorSet != null)
            {
                foreach (EpochDescriptor desc in this.DescriptorSet.Epochs)
                {
                    names.Add(desc.Name);
                }
            }
           
                names.Add("Descriptor Type");

            return names.ToArray();
        }

        public override object SupplyPropertyValue(string sourcePropertyName)
        {
            if (sourcePropertyName == "Variable Name")
                return this.VariableName;
            else if (sourcePropertyName == "Resolution")
                return this.Resolution;
            else if (sourcePropertyName == "Start Time")
                return this.StartTime;
            else if (sourcePropertyName == "End Time")
                return this.EndTime;
            else if (sourcePropertyName == "Data")
            {
                if (this.data == null && this.var != null)
                {
                    MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes loaded = new MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes();
                    Type t = loaded.GetTypesPresentInRunningAssembly(false).SingleOrDefault(ty => ty.Name == "SpikeVariable");
                    if (t != null)
                    {
                        MethodInfo getData = t.GetMethod("ReadData", BindingFlags.Static | BindingFlags.Public);
                        this.data = (float[])getData.Invoke(null, new object[] { this.var, this.StartTime, this.EndTime });
                    }
                }
                return this.data;
            }
            else if (sourcePropertyName == "Experiment Name")
                return this.ExperimentName;
            else if (sourcePropertyName == "Descriptor Type")
                return this.DescriptorType;
            

            if (this.DescriptorSet != null)
            {
                EpochDescriptor desc = this.DescriptorSet.Epochs.SingleOrDefault(e => e.Name == sourcePropertyName);
                if (desc != null) return desc.Value;
            }

            return null;

        }

        public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName == "Variable Name")
                this.VariableName = propertyValue as string;
            else if (targetPropertyName == "Resolution")
                this.Resolution = (double)propertyValue;
            else if (targetPropertyName == "Start Time")
                this.StartTime = (float)propertyValue;
            else if (targetPropertyName == "End Time")
                this.EndTime = (float)propertyValue;
            else if (targetPropertyName == "Experiment Name")
                this.ExperimentName = propertyValue as string;
            else if (targetPropertyName == "Descriptor Type")
                this.DescriptorType = propertyValue;
            else if (this.DescriptorSet != null)
            {
                EpochDescriptor desc = this.DescriptorSet.Epochs.SingleOrDefault(e => e.Name == targetPropertyName);
                if (desc == null) return;

                desc.Value = propertyValue;
                    
            }
        }

        public override bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName == "Variable Name" && propertyValue is string)
                return true;
            if (targetPropertyName == "Resolution" && propertyValue is double)
                return true;
            if (targetPropertyName == "Start Time" && propertyValue is float)
                return true;
            if (targetPropertyName == "End Time" && propertyValue is float)
                return true;
            if (targetPropertyName == "Experiment Name" && propertyValue is string)
                return true;
            if (targetPropertyName == "Descriptor Type") return true;

            if (targetPropertyName == "Data") return false;

            if (this.DescriptorSet != null)
            {
                return this.DescriptorSet.Epochs.Any(ep => ep.Name == targetPropertyName);
            }

            return false;
        }

        public override FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
        {
            if (this.DescriptorSet == null)
            {
                return null;
            }

            EpochDescriptor descriptor = this.DescriptorSet.Epochs.Single(ep => ep.Name == sourcePropertyName);

            FlowControlParameterDescriptor desc = new FlowControlParameterDescriptor();
            desc.Version = "0.0";
            desc.Type = descriptor.Value.GetType();
            return new FlowControlParameterDescriptor[] { desc };
        }

        public override FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
        {
            if (this.DescriptorSet == null)
            {
                return null;
            }

            EpochDescriptor descriptor = this.DescriptorSet.Epochs.Single(ep => ep.Name == targetPropertyName);

            FlowControlParameterDescriptor desc = new FlowControlParameterDescriptor();
            desc.Version = "0.0";
            desc.Type = descriptor.Value.GetType();
            return new FlowControlParameterDescriptor[] { desc };
        }

        public override object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
        {

            if (this.DescriptorSet == null)
            {
                return null;
            }

            EpochDescriptor descriptor = this.DescriptorSet.Epochs.Single(ep => ep.Name == sourcePropertyName);
            return CeresBase.General.Utilities.Convert(descriptor.Value, supplyType.Type);
            
        }

        public override void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
        {
            if (this.DescriptorSet == null)
            {
                return;
            }

            EpochDescriptor descriptor = this.DescriptorSet.Epochs.Single(ep => ep.Name == targetPropertyName);
            descriptor.Value = CeresBase.General.Utilities.Convert(propertyValue, receiveType.Type);
            
        }

        public override Type PropertyType(string targetPropertyName)
        {
            EpochDescriptor descriptor = this.DescriptorSet.Epochs.Single(ep => ep.Name == targetPropertyName);
            return descriptor.Value.GetType();
            
        }

        public override object RunAdapter()
        {
            throw new NotImplementedException();
        }

        public override object[] GetValuesForSerialization()
        {
            if (this.DescriptorSet != null)
            {
                return new object[] { this.DescriptorSet.Description };

            }
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            if (values != null && values.Length != 0)
            {
                if (this.DescriptorType.GetType().IsEnum)
                {
                    try
                    {
                        this.DescriptorType = Enum.Parse(this.DescriptorType.GetType(), values[0] as string);
                    }
                    catch
                    {

                    }
                }

             //   this.DescriptorSet = EpochDescriptorSetCentral.EpochDescriptorSets.SingleOrDefault(dse => dse.Description == values[0] as string);
            }
        }

        public override bool CanEditProperty(string targetPropertyname)
        {
            if (targetPropertyname == "Variable Name" || targetPropertyname == "Start Time" ||
                targetPropertyname == "End Time" || targetPropertyname == "Resolution" || targetPropertyname == "Data" || targetPropertyname=="Experiment Name")
                return false;

            return true;
        }

        public override bool CanReceivePropertyLinks(string targetPropertyname)
        {
            if (targetPropertyname == "Variable Name" || targetPropertyname == "Start Time" ||
                targetPropertyname == "End Time" || targetPropertyname == "Resolution" || targetPropertyname == "Data" || targetPropertyname=="Experiment Name") return false;
            return true;
        }

        public override bool ShouldSerialize(string targetPropertyname)
        {
            if (targetPropertyname == "Descriptor Type")
                return false;

            return base.ShouldSerialize(targetPropertyname);
        }

        #endregion

        #region ICloneable Members

        public override object Clone()
        {
            EpochDescriptorSetAdapter adapter = new EpochDescriptorSetAdapter();

            if (adapter.DescriptorSet != null)
            {
                adapter.DescriptorSet = new EpochDescriptorSet();
                foreach (EpochDescriptor descriptor in this.DescriptorSet.Epochs)
                {
                    adapter.DescriptorSet.Epochs.Add(descriptor);
                }

                adapter.DescriptorSet.Description = this.DescriptorSet.Description;
            }

            adapter.Name = this.Name;

            return adapter;

        }

        #endregion

    }
}
