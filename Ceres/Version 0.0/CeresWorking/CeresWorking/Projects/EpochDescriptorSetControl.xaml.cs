﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using CeresBase.UI;
using System.ComponentModel;

namespace CeresBase.Projects
{
    /// <summary>
    /// Interaction logic for EpochDescriptorSetControl.xaml
    /// </summary>
    public partial class EpochDescriptorSetControl : UserControl, INotifyPropertyChanged
    {
        public ObservableCollection<EpochDescriptor> EpochDescriptorList
        {
            get
            {
                return EpochDescriptorCentral.EpochDescriptors;
            }
        }

        private ObservableCollection<EpochDescriptor> epochsToGroup = new ObservableCollection<EpochDescriptor>();
        public ObservableCollection<EpochDescriptor> EpochsToGroup
        {
            get
            {
                return epochsToGroup;
            }
        }

        private EpochDescriptor selectedEpoch = new EpochDescriptor();
        public EpochDescriptor SelectedEpoch
        {
            get
            {
                return selectedEpoch;
            }
            set
            {
                selectedEpoch = value;
                BindingExpression be = this.NameBox.GetBindingExpression(TextBox.TextProperty);
                OnPropertyChanged("SelectedEpoch");

            }
        }

        public EpochDescriptorSetControl()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (object obj in AvailableEpochDescriptorsList.SelectedItems)
            {
                EpochDescriptor ed = obj as EpochDescriptor;

                if (!epochsToGroup.Contains(ed))
                    epochsToGroup.Add(ed);
            }
        }

        //Accept
        private void CreateEpochDescGroupButton_Click(object sender, RoutedEventArgs e)
        {
            if (EpochDescGroupName.Text == "")
            {
                MessageBox.Show("Please name your Epoch Descriptor Group!");
                return;
            }

            EpochDescriptorSet eds = new EpochDescriptorSet();
            eds.Description =  SetDescription.Text;
            foreach (EpochDescriptor ed in EpochsToGroup)
            {
                eds.Epochs.Add(ed);
            }

            EpochDescriptorSetCentral.EpochDescriptorSets.Add(eds);

            if (this.Parent is Window)
                (this.Parent as Window).Close();
        }

        //Invoke control for new EpochDesc
        private void CreateNewEpochDesc_Click(object sender, RoutedEventArgs e)
        {
            for (; ; )
            {
                EpochDescriptorControl edc = new EpochDescriptorControl();

                

                ContentBox cb = new ContentBox();
               
                cb.Buttons = ContentBoxButton.OKAddCancel;
                cb.ContentElement = edc;

                cb.Height = 300;
                cb.Width = 200;
                cb.Title = "Create new Epoch Descriptor";
                cb.ContentBoxClosed += new EventHandler<ContentBoxEventArgs>(cb_ContentBoxClosed);

                newBox = false;

                cb.ShowDialog();

                if (ed != null)
                    EpochDescriptorCentral.EpochDescriptors.Add(ed);

                if (!newBox)
                    break;
            }
        }

        private EpochDescriptor ed;
        bool newBox;

        void cb_ContentBoxClosed(object sender, ContentBoxEventArgs e)
        {
            ContentBox box = sender as ContentBox;
            EpochDescriptorControl edc = box.ContentElement as EpochDescriptorControl;

            if (e.Result == ContentBoxResult.OK || e.Result == ContentBoxResult.Add)
                ed = edc.ReturnValue();
            else
                ed = null;

            if (e.Result == ContentBoxResult.Add)
                newBox = true;
        }

        private void RemoveEpochDescFromSet_Click(object sender, RoutedEventArgs e)
        {
            List<EpochDescriptor> toRemove = new List<EpochDescriptor>();

            foreach (object obj in EpochsToGroupList.SelectedItems)
            {
                EpochDescriptor ed = obj as EpochDescriptor;

                if (epochsToGroup.Contains(ed))
                    toRemove.Add(ed);
            }

            foreach (EpochDescriptor ed in toRemove)
            {
                epochsToGroup.Remove(ed);
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
