﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using MesosoftCommon.Utilities.Settings;

namespace CeresBase.Projects
{
    public static class EpochDescriptorCentral
    {
        //private static string fallbackSerializationpath = Paths.DefaultPath.LocalPath;
        //private static string serializationPath = Paths.DefaultPath.LocalPath + "Projects";
        //private static string centralDescFileSerializationPath = EpochDescriptorCentral.serializationPath + "\\EpochDescriptors.XML";

        //private static List<EpochDescriptorSerializer> savedEpochDescriptors = new List<EpochDescriptorSerializer>();

        private static ObservableCollection<EpochDescriptor> epochDescriptors =
            CeresBase.IO.Serialization.NewSerializationCentral.RegisterCollection<ObservableCollection<EpochDescriptor>>(typeof(EpochDescriptorCentral), "epochDescriptors"); //new ObservableCollection<EpochDescriptor>();
        public static ObservableCollection<EpochDescriptor> EpochDescriptors { get { return epochDescriptors; } }

        public static bool GuidAlreadyUsed(Guid toCheck)
        {
            //foreach (EpochDescriptorSerializer desc in savedEpochDescriptors)
            //{
            //    if (toCheck.CompareTo(desc.Guid) == 0)
            //        return true;
            //}

            foreach (EpochDescriptor desc in epochDescriptors)
            {
                if (toCheck.CompareTo(desc.Guid) == 0)
                    return true;
            }

            return false;
        }

        //public static EpochDescriptorSerializer GetSavedEpochDescriptor(Guid guid)
        //{
        //    foreach (EpochDescriptorSerializer desc in savedEpochDescriptors)
        //    {
        //        if (guid.CompareTo(desc.Guid) == 0)
        //            return desc;
        //    }

        //    return null;
        //}

        public static EpochDescriptor GetEpochDescriptor(Guid guid)
        {
            foreach (EpochDescriptor desc in EpochDescriptors)
            {
                if (guid.CompareTo(desc.Guid) == 0)
                    return desc;
            }

            return null;
        }

        //static EpochDescriptorCentral()
        //{
        //    if (!Directory.Exists(serializationPath))
        //        Directory.CreateDirectory(serializationPath);

        //    if (File.Exists(centralDescFileSerializationPath))
        //    {
        //        savedEpochDescriptors = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(centralDescFileSerializationPath) as List<EpochDescriptorSerializer>;
        //        foreach (EpochDescriptorSerializer desc in savedEpochDescriptors)
        //        {
        //            EpochDescriptors.Add(EpochDescriptorSerializer.Deserialize(desc));
        //        }
        //    }

        //    if (Application.Current == null)
        //        System.Windows.Forms.Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
        //    else
        //        Application.Current.Exit += new ExitEventHandler(Current_Exit);
        //}

        //static void Current_Exit(object sender, ExitEventArgs e)
        //{
        //    saveSettings();
        //}

        //static void Application_ApplicationExit(object sender, EventArgs e)
        //{
        //    saveSettings();
        //}

        //static private void saveSettings()
        //{
        //    using (FileStream saveFileStream = new FileStream(centralDescFileSerializationPath, FileMode.Create))
        //    {
        //        //Save either updates or new entries into the saved repository
        //        foreach (EpochDescriptor desc in EpochDescriptorCentral.EpochDescriptors)
        //        {
        //            EpochDescriptorSerializer savedDesc = GetSavedEpochDescriptor(desc.Guid);
        //            if (savedDesc == null)
        //            {
        //                //Not saved, so make a new entry.
        //                savedEpochDescriptors.Add(EpochDescriptorSerializer.Serialize(desc));
        //            }
        //            else
        //            {
        //                //Saved, so update values if they need updating.
        //                if (!savedDesc.ValuesMatch(desc))
        //                {
        //                    savedDesc.Name = desc.Name;
        //                    savedDesc.Category = desc.Category;
        //                    savedDesc.Value = desc.Value;
        //                }
        //            }
        //        }

        //        MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(savedEpochDescriptors, saveFileStream);
        //    }
        //}
    }
}
