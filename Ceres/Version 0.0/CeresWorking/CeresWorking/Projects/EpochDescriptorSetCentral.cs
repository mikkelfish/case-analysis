﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using MesosoftCommon.Utilities.Settings;
using System.IO;
using System.Windows;

namespace CeresBase.Projects
{
    public static class EpochDescriptorSetCentral
    {
        //private static string fallbackSerializationpath = Paths.DefaultPath.LocalPath;
        //private static string serializationPath = Paths.DefaultPath.LocalPath + "Projects";
        //private static string centralDescSetSerializationPath = EpochDescriptorSetCentral.serializationPath + "\\EpochDescriptorSets.XML";

        //private static List<EpochDescriptorSetSerializer> savedEpochDescriptorSets = new List<EpochDescriptorSetSerializer>();

        private static ObservableCollection<EpochDescriptorSet> epochDescriptorSets = new ObservableCollection<EpochDescriptorSet>();
            //CeresBase.IO.Serialization.SerializationCentral.RegisterCollection<ObservableCollection<EpochDescriptorSet>>(typeof(EpochDescriptorSetCentral), "epochDescriptorSets"); //new ObservableCollection<EpochDescriptorSet>();
        public static ObservableCollection<EpochDescriptorSet> EpochDescriptorSets { get { return epochDescriptorSets; } }

        //static EpochDescriptorSetCentral()
        //{
        //    if (!Directory.Exists(serializationPath))
        //        Directory.CreateDirectory(serializationPath);

        //    if (File.Exists(centralDescSetSerializationPath))
        //    {
        //        savedEpochDescriptorSets = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(centralDescSetSerializationPath) as List<EpochDescriptorSetSerializer>;
        //        foreach (EpochDescriptorSetSerializer desc in savedEpochDescriptorSets)
        //        {
        //            EpochDescriptorSets.Add(EpochDescriptorSetSerializer.Deserialize(desc));
        //        }
        //    }

        //    if (Application.Current == null)
        //        System.Windows.Forms.Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
        //    else
        //        Application.Current.Exit += new ExitEventHandler(Current_Exit);
        //}

        //static void Current_Exit(object sender, ExitEventArgs e)
        //{
        //    saveSettings();
        //}

        //static void Application_ApplicationExit(object sender, EventArgs e)
        //{
        //    saveSettings();
        //}

        //static private bool AreEpochsIdentical(EpochDescriptorSet descSet, EpochDescriptorSetSerializer descSetSer)
        //{
        //    bool ret = true;

        //    if (descSet.Epochs.Count != descSetSer.Epochs.Count) return false;

        //    foreach (EpochDescriptor desc in descSet.Epochs)
        //    {
        //        if (!descSetSer.Epochs.Contains(desc.Guid))
        //        {
        //            ret = false;
        //            break;
        //        }
        //    }

        //    return ret;
        //}

        //static private void saveSettings()
        //{
        //    using (FileStream saveFileStream = new FileStream(centralDescSetSerializationPath, FileMode.Create))
        //    {
        //        //Save either updates or new entries into the saved repository
        //        foreach (EpochDescriptorSet desc in EpochDescriptorSets)
        //        {
        //            bool matchFound = false;

        //            foreach (EpochDescriptorSetSerializer descSet in savedEpochDescriptorSets)
        //            {
        //                //match found
        //                if (descSet.Description == desc.Description)
        //                {
        //                    matchFound = true;

        //                    //Update the contents if they've changed
        //                    if (AreEpochsIdentical(desc, descSet)) continue;

        //                    descSet.Epochs.Clear();
        //                    foreach (EpochDescriptor thisDesc in desc.Epochs)
        //                    {
        //                        descSet.Epochs.Add(thisDesc.Guid);
        //                    }

        //                    break;
        //                }
        //            }

        //            if (matchFound) continue;

        //            //Match not found, so add it.
        //            savedEpochDescriptorSets.Add(EpochDescriptorSetSerializer.Serialize(desc));
        //        }

        //        MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(savedEpochDescriptorSets, saveFileStream);
        //    }
        //}
    }
}
