﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CeresBase.FlowControl;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace CeresBase.Projects
{
    /// <summary>
    /// Interaction logic for AdapterPropertyPicker.xaml
    /// </summary>
    public partial class AdapterPropertyPicker : UserControl, INotifyPropertyChanged
    {
        private ObservableCollection<IFlowControlAdapter> availableAdapters = new ObservableCollection<IFlowControlAdapter>();
        public ObservableCollection<IFlowControlAdapter> AvailableAdapters
        {
            get
            {
                return availableAdapters;
            }
            set
            {
                availableAdapters = value;
                OnPropertyChanged("AvailableAdapters");
            }
        }

        private ObservableCollection<PropertyListing> availableProperties = new ObservableCollection<PropertyListing>();
        public ObservableCollection<PropertyListing> AvailableProperties
        {
            get
            {
                return availableProperties;
            }
        }

        private object selectedAdapter;
        public object SelectedAdapter 
        {
            get
            {
                return selectedAdapter;
            }
            set 
            {
                if (selectedAdapter != value)
                {
                    selectedAdapter = value;
                    loadPropertiesFromAdapter();
                    OnPropertyChanged("AvailableProperties");
                }
            }
        }

        private void loadPropertiesFromAdapter()
        {
            IFlowControlAdapter adapter = (selectedAdapter as IFlowControlAdapter);
            string[] propertyNames = adapter.SupplyPropertyNames();

            availableProperties.Clear();
            foreach (string propName in propertyNames)
            {
                PropertyListing pl = new PropertyListing(propName, adapter.SupplyPropertyValue(propName));
                availableProperties.Add(pl);
            }
        }

        public AdapterPropertyPicker()
        {
            InitializeComponent();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
