﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using MesosoftCommon.Utilities.Settings;
using System.ComponentModel;
using CeresBase.IO.Serialization;

namespace CeresBase.Projects
{
    public class RoutineCentralSerializer
    {
        private Dictionary<string, List<RoutinePackage>> routines = new Dictionary<string, List<RoutinePackage>>();
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Dictionary<string, List<RoutinePackage>> Routines { get { return this.routines; } }

        public static RoutineCentralSerializer Serialize(Dictionary<string, ObservableCollection<RoutinePackage>> RoutinesList)
        {
            RoutineCentralSerializer ret = new RoutineCentralSerializer();
            foreach (string key in RoutinesList.Keys)
            {
                ret.routines.Add(key, new List<RoutinePackage>());
                ObservableCollection<RoutinePackage> thisList = RoutinesList[key];
                foreach (RoutinePackage pack in thisList)
                {
                    ret.routines[key].Add(pack);
                }
            }

            return ret;
        }

        public static Dictionary<string, ObservableCollection<RoutinePackage>> Deserialize(RoutineCentralSerializer toDeserialize)
        {
            Dictionary<string, ObservableCollection<RoutinePackage>> ret = new Dictionary<string, ObservableCollection<RoutinePackage>>();
            foreach (string key in toDeserialize.Routines.Keys)
            {
                ret.Add(key, new ObservableCollection<RoutinePackage>());
                List<RoutinePackage> thisList = toDeserialize.Routines[key];
                foreach (RoutinePackage pack in thisList)
                {
                    ret[key].Add(pack);
                }
            }

            return ret;
        }
    }

    public class OldRoutinePackage
    {
        public string RoutineName { get; set; }
        public string RoutinePath { get; set; }

        public override string ToString()
        {
            return RoutineName;
        }
    }

    public class RoutinePackage : INotifyPropertyChanged
    {
        private Guid id;
        public Guid ID 
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
                this.OnPropertyChanged("ID");
            }
        }
        
        public string RoutineName { get; set; }

        private RoutineSerializer sr;
        public RoutineSerializer SerializedRoutine 
        {
            get
            {
                return this.sr;
            }
            set
            {
                this.sr = value;
                this.OnPropertyChanged("SerializedRoutine");
            }
        }

        private RoutineManagerSerializer serializedRM;
        public RoutineManagerSerializer SerializedRoutineManager 
        {
            get
            {
                return this.serializedRM;
            }
            set
            {
                this.serializedRM = value;
                this.OnPropertyChanged("SerializedRoutineManager");
            }
        }

        private int iteration = 1;
        public int Iteration { get { return this.iteration; } set { this.iteration = value; } }

        public override string ToString()
        {
            return this.RoutineName;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler ev = this.PropertyChanged;
            if (ev != null)
            {
                ev(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }

    public static class RoutineCentral
    {
        public static void OldMerge()
        {
            string serializationPath = Paths.DefaultPath.LocalPath + "Projects";
            string fileLocationSerializationPath = serializationPath + "\\Routines.XML";
              if (File.Exists(fileLocationSerializationPath))
            {
                RoutineCentralSerializer rcs = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(fileLocationSerializationPath) as RoutineCentralSerializer;
                Dictionary<string, ObservableCollection<RoutinePackage>> oldRoutines = RoutineCentralSerializer.Deserialize(rcs);

                foreach (string key in oldRoutines.Keys)
                {
                    if (!RoutineCentral.routines.ContainsKey(key))
                    {
                        RoutineCentral.routines.Add(key, new ObservableCollection<RoutinePackage>());
                    }

                    foreach (RoutinePackage pack in oldRoutines[key])
                    {
                        if (!RoutineCentral.routines[key].Any(p => p.RoutineName == pack.RoutineName))
                        {
                            RoutineCentral.routines[key].Add(pack);
                        }
                    }
                }
            }
           
        }


        //private static string fallbackSerializationpath = Paths.DefaultPath.LocalPath;
        //private static string serializationPath = Paths.DefaultPath.LocalPath + "Projects";
        ////private static string oldfileLocationSerializationPath = RoutineCentral.serializationPath + "\\RoutineLocations.XML";
        //private static string fileLocationSerializationPath = RoutineCentral.serializationPath + "\\Routines.XML";

        private static Dictionary<string, ObservableCollection<RoutinePackage>> routines =
            NewSerializationCentral.RegisterCollection<Dictionary<string, ObservableCollection<RoutinePackage>>>(typeof(RoutineCentral), "routines");  // = new Dictionary<string, ObservableCollection<RoutinePackage>>();
        public static Dictionary<string, ObservableCollection<RoutinePackage>> Routines { get { return RoutineCentral.routines; } }

        //private static ObservableCollection<OldRoutinePackage> oldroutines = new ObservableCollection<OldRoutinePackage>();
        //public static ObservableCollection<OldRoutinePackage> oldRoutines { get { return oldroutines; } }

        public static int GetRoutineIndex(string routine, string controllerTypeName)
        {
            if (!Routines.ContainsKey(controllerTypeName)) return -1;
            for (int i = 0; i < Routines[controllerTypeName].Count; i++)
            {
                if (routines[controllerTypeName][i].RoutineName == routine)
                    return i;
            }
            return -1;
        }

        public static Routine GetRoutine(int index, string controllerTypeName)
        {
            if (!Routines.ContainsKey(controllerTypeName) || index <0 || index >= Routines[controllerTypeName].Count) return null; ;
            RoutineSerializer rs = Routines[controllerTypeName][index].SerializedRoutine;
            Routine ret = RoutineSerializer.Deserialize(rs);
            return ret;
        }

        public static Routine GetRoutine(string routineName, string controllerType)
        {
            if (!Routines.ContainsKey(controllerType)) return null;
            RoutinePackage rs = Routines[controllerType].FirstOrDefault(r => r.RoutineName == routineName);
            if (rs == null) return null;
            return RoutineSerializer.Deserialize(rs.SerializedRoutine);
        }

        //public static Routine GetRoutine(int index)
        //{
        //    //if (File.Exists(oldRoutines[index].RoutinePath))
        //    //{
        //    //    RoutineSerializer rs = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(oldRoutines[index].RoutinePath) as RoutineSerializer;
        //    //    Routine ret = RoutineSerializer.Deserialize(rs);
        //    //    return ret;
        //    //}

        //    return null;
        //}

        public static RoutineManager GetRoutineManager(int indexOfRoutineToAttach, Routine routineToAttach, string controllerTypeName)
        {
            RoutineManagerSerializer rms = Routines[controllerTypeName][indexOfRoutineToAttach].SerializedRoutineManager;
            RoutineManager ret = RoutineManagerSerializer.Deserialize(rms, routineToAttach);
            return ret;
        }

        public static RoutineManager GetRoutineManager(string routineName, Routine routineToAttach, string controllerTypeName)
        {
            RoutineManagerSerializer rms = Routines[controllerTypeName].Single(p => p.RoutineName == routineName).SerializedRoutineManager;

            //RoutineManagerSerializer rms = Routines[controllerTypeName][indexOfRoutineToAttach].SerializedRoutineManager;
            RoutineManager ret = RoutineManagerSerializer.Deserialize(rms, routineToAttach);
            return ret;
        }

        //public static RoutineManager GetRoutineManager(int indexOfRoutineToAttach, Routine routineToAttach)
        //{
        //    //string managerPath = oldRoutines[indexOfRoutineToAttach].RoutinePath;
        //    //managerPath = managerPath.Substring(0, managerPath.Length - 4);
        //    //managerPath += "ManagerData.xaml";

        //    //if (File.Exists(managerPath))
        //    //{
        //    //    RoutineManagerSerializer rms = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(managerPath) as RoutineManagerSerializer;
        //    //    RoutineManager ret = RoutineManagerSerializer.Deserialize(rms, routineToAttach);
        //    //    return ret;
        //    //}

        //    return null;
        //}

        static RoutineCentral()
        {
            //if (!Directory.Exists(serializationPath))
            //    Directory.CreateDirectory(serializationPath);


            //if (File.Exists(RoutineCentral.fileLocationSerializationPath))
            //{
            //    RoutineCentralSerializer rcs = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(fileLocationSerializationPath) as RoutineCentralSerializer;
            //    routines = RoutineCentralSerializer.Deserialize(rcs);
            //}

            //if (Application.Current == null)
            //    System.Windows.Forms.Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
            //else
            //    Application.Current.Exit += new ExitEventHandler(Current_Exit);
        }

        public static void UpdateCentralWithRoutine(string nameToUse, Guid uid, RoutineSerializer serializedRoutine,
            RoutineManagerSerializer serializedRoutineManager, string controllerTypeName)
        {
            if (!RoutineCentral.Routines.ContainsKey(controllerTypeName))
                RoutineCentral.Routines.Add(controllerTypeName, new ObservableCollection<RoutinePackage>());
            ObservableCollection<RoutinePackage> thisList = RoutineCentral.Routines[controllerTypeName];
            int iteration = 1;
            foreach (RoutinePackage rp in thisList)
            {
                if (rp.ID == uid)
                {
                    //UID match, update and exit
                    if (serializedRoutine != null)
                        rp.SerializedRoutine = serializedRoutine;
                    if (serializedRoutineManager != null)
                    {
                        rp.SerializedRoutineManager = serializedRoutineManager;
                    }
                    if (rp.RoutineName != nameToUse)
                        rp.RoutineName = nameToUse;

                    //if (rp.SerializedRoutineManager.Name != nameToUse)
                    //    rp.SerializedRoutineManager.Name = nameToUse;
                    return;
                }
                //While we're iterating to check for UID matches, also figure out the pack iteration in case it is a 
                //register instead of an update call
                if (rp.RoutineName == nameToUse)
                    iteration++;
            }

            RoutinePackage pack = new RoutinePackage();
            pack.ID = uid;
            pack.RoutineName = nameToUse;
            pack.SerializedRoutine = serializedRoutine;
            pack.SerializedRoutineManager = serializedRoutineManager;
            pack.Iteration = iteration;

            thisList.Add(pack);
        }

        //public static void RegisterFile(string nameToUse, string filePath)
        //{
        //    //foreach (OldRoutinePackage rp in oldRoutines)
        //    //{
        //    //    if (nameToUse == rp.RoutineName && filePath == rp.RoutinePath)
        //    //        return;
        //    //}

        //    //OldRoutinePackage rop = new OldRoutinePackage();
        //    //rop.RoutineName = nameToUse;
        //    //rop.RoutinePath = filePath;
        //    //oldRoutines.Add(rop);
        //}

        //static void Current_Exit(object sender, ExitEventArgs e)
        //{
        //    saveSettings();
        //}

        //static void Application_ApplicationExit(object sender, EventArgs e)
        //{
        //    saveSettings();
        //}

        //static private void saveSettings()
        //{
        //    //using (FileStream saveFileStream = new FileStream(oldfileLocationSerializationPath, FileMode.Create))
        //    //{
        //    //    RoutineLocationSerializer rls = RoutineLocationSerializer.Serialize(oldRoutines);
        //    //    MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(rls, saveFileStream);
        //    //}

        //    using (FileStream saveFileStream = new FileStream(fileLocationSerializationPath, FileMode.Create))
        //    {
        //        RoutineCentralSerializer rcs = RoutineCentralSerializer.Serialize(Routines);
        //        MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(rcs, saveFileStream);
        //    }
        //}
    }
}
