﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Data;
using CeresBase.Projects.Flowable;
using System.Windows.Data;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace CeresBase.Projects
{
    public class Epoch : INotifyPropertyChanged
    {
        private string name;
        public string Name
        {
            get { return name; }
        }
        private float beginTime;
        public float BeginTime
        {
            get { return beginTime; }
        }

        private float endTime;
        public float EndTime
        {
            get { return endTime; }
        }

        public string Description { get; set; }

        public bool AppliesToExperiment { get; set; }
        public bool IsUser { get; set; }

        public string FullDescription 
        {
            get
            {
                string toRet = this.name.Replace('/', ',').Replace('-', ',') + " " + Math.Round(beginTime, 1) + "-" + Math.Round(endTime, 1);

                if (!this.IsUser)
                    return toRet;

                if (this.Variable != null)
                {
                    toRet = this.Variable.Header.Description + "_" + toRet;
                }

                return toRet;
            }
        }

        private ObservableCollection<EpochAttachment> attachments = new ObservableCollection<EpochAttachment>();
        public EpochAttachment[] Attachments { get { return this.attachments.ToArray(); } }

        public void AddAttachment(EpochAttachment attachment)
        {
            this.attachments.Add(attachment);
            this.OnPropertyChanged("Attachments");
        }

        public void RemoveAttachment(EpochAttachment attachment)
        {
            this.attachments.Remove(attachment);
            this.OnPropertyChanged("Attachments");
        }


        #region Equals Override

        public override bool Equals(object obj)
        {
            if (!(obj is Epoch)) return false;
            Epoch other = obj as Epoch;

            if (this.AppliesToExperiment && other.AppliesToExperiment)
            {
               return other.Variable.Header.ExperimentName == this.Variable.Header.ExperimentName 
                   && other.beginTime == this.beginTime && other.endTime == this.endTime &&
                    other.Name == this.name && other.Description == this.Description;
            }

            return other.Variable == this.Variable && other.beginTime == this.beginTime && other.endTime == this.endTime &&
                other.Name == this.name && other.Description == this.Description;
        }

        public static bool operator ==(Epoch a, Epoch b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            return a.Equals(b);
        }

        public static bool operator !=(Epoch a, Epoch b)
        {
            return !(a == b);
        }

        #endregion

        public CeresBase.Data.Variable Variable { get; internal set; }

        public Epoch(Variable variable, string name, float beginTime, float endTime)
        {            
            this.Variable = variable;
            this.name = name;
            this.beginTime = beginTime;
            this.endTime = endTime;
        }

        #region Input/Output

        //public static bool ParseSpot(string toParse, out string name, out float begin, out float end, out bool user)
        //{
        //    string[] splitUser = toParse.Split('|');
        //    if (splitUser.Length != 2)
        //    {
        //        user = false;
        //        name = "";
        //        end = float.MaxValue;
        //        begin = float.MaxValue;
        //        return false;
        //    }

        //    user = false;
        //    if (splitUser[0] == "UserCreated") user = true;

        //    try
        //    {
        //        string[] splitName = splitUser[1].Split(new string[] { ": " }, StringSplitOptions.RemoveEmptyEntries);
        //        name = splitName[0];

        //        string[] splitTime = splitName[1].Split('-');

        //        begin = float.Parse(splitTime[0]);
        //        end = float.Parse(splitTime[1]);
        //    }
        //    catch (Exception ex)
        //    {
        //        user = false;
        //        name = "";
        //        end = float.MaxValue;
        //        begin = float.MaxValue;
        //    }

        //    return true;

        //}

        public override string ToString()
        {
            

            string toRet= this.name.Replace('/', ',').Replace('-', ',') + ": " + Math.Round(beginTime,1) + "-" + Math.Round(endTime,1);

            if (!this.IsUser)
                return toRet;

            if (this.Variable != null)
            {
                toRet = this.Variable.FullDescription + "/" + toRet;
            }

            return toRet;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }

    public class EpochComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            Epoch one = x as Epoch;
            Epoch two = y as Epoch;
            if (one.BeginTime < two.BeginTime) return -1;
            if (one.BeginTime > two.BeginTime) return 1;
            return 0;
        }

        #endregion
    }
}
