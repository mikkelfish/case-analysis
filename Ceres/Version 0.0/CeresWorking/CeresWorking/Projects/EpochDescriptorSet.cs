﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace CeresBase.Projects
{
    public class EpochDescriptorSet
    {
        private ObservableCollection<EpochDescriptor> epochs = new ObservableCollection<EpochDescriptor>();
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ObservableCollection<EpochDescriptor> Epochs
        {
            get
            {
                return this.epochs;
            }
        }

        public string Description { get; set; }
    }

    //public class EpochDescriptorSetSerializer
    //{
    //    //private List<Guid> epochs = new List<Guid>();
    //    //[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    //    //public List<Guid> Epochs { get { return epochs; } }

    //    //public string Description { get; set; }

    //    //public static EpochDescriptorSetSerializer Serialize(EpochDescriptorSet toSerialize)
    //    //{            
    //    //    EpochDescriptorSetSerializer ret = new EpochDescriptorSetSerializer();

    //    //    foreach (EpochDescriptor desc in toSerialize.Epochs)
    //    //    {
    //    //        ret.Epochs.Add(desc.Guid);
    //    //    }

    //    //    ret.Description = toSerialize.Description;

    //    //    return ret;
    //    //}

    //    public static EpochDescriptorSet Deserialize(EpochDescriptorSetSerializer toDeserialize)
    //    {
    //        EpochDescriptorSet ret = new EpochDescriptorSet();

    //        foreach (Guid id in toDeserialize.Epochs)
    //        {
    //            EpochDescriptor ed = EpochDescriptorCentral.GetEpochDescriptor(id);
    //            if (ed != null)
    //                ret.Epochs.Add(ed);
    //        }

    //        ret.Description = toDeserialize.Description;

    //        return ret;
    //    }
    //}
}
