using System;
using System.Collections.Generic;
using System.Text;
using NetCDFCSharp;
using CeresBase.IO.NCDF;

namespace CeresBase.Data.DataPools
{
    public class DataPoolNetCDF : IDataPool
    {
        #region Private variables

        private NetCDFFile file;
        private int varID;
        private DataIterator iterator;
        private int[] dimlens;
        private float min;
        private float max;
        private int maxBufferSize = 2400000;
        private int linked = 0;
        private readonly object lockable = new object();
        private bool beenSet = false;
        private bool minMaxFound = false;

        private bool isTemp = false;

        #endregion

        #region Constructors

        public DataPoolNetCDF(NetCDFFile file)
        {
            this.file = file;
            this.dimlens = file.DimLengths;
            this.iterator = new DataIterator(this.dimlens, new int[dimlens.Length], this.dimlens);
            this.isTemp = false;
            this.min = float.MaxValue;
            this.max = float.MinValue;
        }

        public DataPoolNetCDF(int[] dimlens)
        {
            this.file = new NetCDFFile(CeresBase.General.Utilities.GetTempFilePath(), true);
            this.dimlens = dimlens;
            int[] dimids = new int[dimlens.Length];
            for (int i = 0; i < dimlens.Length; i++)
            {
                int dimid = NetCDF.nc_def_dim(this.file.FileId, "TempDim" + i.ToString(), (uint)dimlens[i]);
                dimids[i] = dimid;
            }

            this.varID = NetCDF.nc_def_var(this.file.FileId, "TempVar", dimids, typeof(float));
            this.iterator = new DataIterator(dimlens, new int[dimlens.Length], dimlens);
            NetCDF.nc_enddef(this.file.FileId);

            this.min = float.MaxValue;
            this.max = float.MinValue;
            this.isTemp = true;


        }

        #endregion

        #region IDataPool Members

        public CompressionChain Chain
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public void PopCompression()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Type CurrentStorageType
        {
            get { return typeof(float); }
        }

        public float GetDataValue()
        {
            int dump = iterator.Next;
            return 0;
           // return (float)NetCDF.nc_get_var1(this.file.FileId, this.varID, this.iterator.CurrentIndices);
        }

        public float GetDataValue(int[] indices)
        {
            //return 0;
            uint[] netCdf = new uint[indices.Length];
            for (int i = 0; i < indices.Length; i++)
            {
                netCdf[i] = (uint)indices[i];
            }
            return (float)NetCDF.nc_get_var1(this.file.FileId, this.varID, netCdf);
        }

        private void getDataHelper(int[] indices, int[] amount, int[] strides, Delegate readFunction)
        {
            int[] curIndices = (int[])indices.Clone();

            for (int i = 0; i < amount.Length; i++)
            {
                if (amount[i] + indices[i] > this.dimlens[i])
                {
                    amount[i] = this.dimlens[i] - indices[i];
                }
            }


            if (this.dimlens.Length == 1)
            {
                uint spot = (uint)curIndices[0];
                uint read = 0;
                while (read < amount[0])
                {

                    uint[] start = new uint[] { (uint)spot };
                    uint[] readA = new uint[] { (uint)(amount[0] - read) };
                    if (amount[0] - spot > (uint)(this.maxBufferSize / 4))
                    {
                        readA[0] = (uint)(this.maxBufferSize / 4);
                    }

                    uint[] ustride = new uint[] { (uint)strides[0] };
                    uint[] end = new uint[start.Length];
                    for (int i = 0; i < end.Length; i++)
                    {
                        end[i] = start[i] + readA[i];
                    }

                        float[] data = (float[])NetCDF.nc_get_vars(this.file.FileId, this.varID, start, readA, ustride);


                        if (readFunction is DataUtilities.PoolReadStridedDelegate)
                            (readFunction as DataUtilities.PoolReadStridedDelegate)(data, start, end, ustride);
                        else
                        {
                            (readFunction as DataUtilities.PoolReadDelegate)(data, start, readA);
                        }

                    spot += readA[0];
                    read += readA[0];

                }
            }
        }

        public void GetData(int[] indices, int[] amount, DataUtilities.PoolReadDelegate readFunction)
        {
            if (indices == null)
            {
                indices = new int[this.dimlens.Length];
            }

            if (amount == null)
            {
                amount = new int[this.dimlens.Length];
                for (int i = 0; i < amount.Length; i++)
                {
                    amount[i] = this.dimlens[i] - indices[i];
                }
            }

            int[] strides = new int[indices.Length];
            for (int i = 0; i < strides.Length; i++)
            {
                strides[i] = 1;
            }
            this.getDataHelper(indices, amount, strides, readFunction);
        }

        public void GetData(int[] firstIndices, int[] lastIndices, int[] strides, DataUtilities.PoolReadStridedDelegate readFunction)
        {
            int[] counts = new int[firstIndices.Length];
            for (int i = 0; i < counts.Length; i++)
            {
                counts[i] = lastIndices[i] - firstIndices[i];
                counts[i] = counts[i]/strides[i];
            }
            this.getDataHelper(firstIndices, counts, strides, readFunction);
        }

        public void SetDataValue(float val)
        {
            uint[] uintIndices = new uint[this.iterator.CurrentIndices.Length];
            for (int i = 0; i < uintIndices.Length; i++)
            {
                uintIndices[i] = (uint)this.iterator.CurrentIndices[i];
            }
            NetCDF.nc_put_var1(this.file.FileId, this.varID, uintIndices, val);
            int dump = iterator.Next;
        }

        public void SetDataValue(float val, int[] indices)
        {
            uint[] uintIndices = new uint[indices.Length];
            for (int i = 0; i < uintIndices.Length; i++)
            {
                uintIndices[i] = (uint)indices[i];
            }
            NetCDF.nc_put_var1(this.file.FileId, this.varID, uintIndices, val);
        }

        private void writeToNetCDF(byte[] buffer, int count, object tag)
        {
            uint[] indices = new uint[this.dimlens.Length];
            uint[] counts = new uint[this.dimlens.Length];

            int[] pindex = (int[])(tag as object[])[0];
            int[] pcounts = (int[])(tag as object[])[1];

            if (pindex == null)
            {
                // uint[] current = new uint[this.dimlens.Length];
                for (int i = 0; i < indices.Length; i++)
                {
                    indices[i] = (uint)this.iterator.CurrentIndices[i];
                }
            }
            else
            {
                for (int i = 0; i < indices.Length; i++)
                {
                    indices[i] = (uint)pindex[i];
                }
            }

            uint realCount = (uint)(count / 4);

            if (pcounts == null)
            {
                uint currentL = indices[indices.Length - 1];
                uint dimLenL = (uint)this.dimlens[this.dimlens.Length - 1];

                if (realCount <= dimLenL - currentL)
                {
                    counts[counts.Length - 1] = (uint)realCount;
                }
                else
                {
                    counts[counts.Length - 1] = (uint)(dimLenL - currentL);
                }
            }
            else
            {
                for (int i = 0; i < counts.Length; i++)
                {
                    counts[i] = (uint)pcounts[i];
                }
            }


            
        //    uint[] counts = new uint[this.dimlens.Length];


            NetCDF.nc_put_vara(this.file.FileId, this.varID, indices, counts, buffer);
            indices[counts.Length - 1] += counts[counts.Length - 1];
            written += counts[counts.Length - 1];

            for (int i = 0; i < realCount; i++)
            {
                int ok = this.iterator.Next;
            }
        }

        uint written = 0;
        public void SetData(System.IO.Stream data)
        {
            CeresBase.General.Utilities.ReadFromStream(data, -1, this.maxBufferSize, this.writeToNetCDF,
                 new object[] { null, null });
        }

        public void SetData(System.IO.Stream data, int[] indices, int[] amounts)
        {
            CeresBase.General.Utilities.ReadFromStream(data, -1, this.maxBufferSize, this.writeToNetCDF,
                new object[] { indices, amounts });
        }

        public void SetData(System.IO.Stream data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            
        }

        public int GetDimensionLength(int index)
        {
            return this.dimlens[index];
        }

        public int[] DimensionLengths
        {
            get { return this.dimlens; }
        }

        public float Min
        {
            get { return this.min; }
        }

        public float Max
        {
            get { return this.max; }
        }

        public int NumDimensions
        {
            get { return this.dimlens.Length; }
        }

        public void SetMinMaxManually(float min, float max)
        {
            this.max = max;
            this.min = min;
            this.minMaxFound = true;
        }

        public void DoneSettingData()
        {          
            this.beenSet = true;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            this.file.Close();            
            if(this.isTemp)
                System.IO.File.Delete(this.file.FilePath);
        }

        #endregion

        #region IDataPool Members

        public void FrameAdded()
        {
            lock (this.lockable)
            {
                this.linked++;
            }
        }

        public void FrameRemoved()
        {
            lock (this.lockable)
            {
                this.linked--;
            }
        }

        #endregion

        #region IDataPool Members


        public bool BeenSet
        {
            get { return this.beenSet; }
        }

        #endregion

        #region IDataPool Members


        public void CalcMinMax()
        {
            this.min = float.MaxValue;
            this.max = float.MinValue;
            float missing = CeresBase.Data.DataConstants.GetMissingValue<float>();
            this.GetData(new int[this.dimlens.Length], this.dimlens,
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        if (data[i] == missing) continue;

                        if (data[i] < this.min)
                            this.min = data[i];
                        if (data[i] > this.max)
                            this.max = data[i];
                    }
                }
            );
            this.minMaxFound = true;
        }

        public bool MinMaxFound
        {
            get
            {
                return this.minMaxFound;
            }
        }

        #endregion
    }
}
