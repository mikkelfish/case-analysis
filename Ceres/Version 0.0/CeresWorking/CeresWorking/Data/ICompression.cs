using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using CeresBase.Development;

namespace CeresBase.Data
{
    /// <summary>
    /// Interface used to compress and decompress data. This is used by the data pool when communicating
    /// to ensure optimal memory usage.
    /// </summary>
    [CeresCreated("Mikkel", "03/13/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    public interface ICompression
    {
        /// <summary>
        /// Is compression initalized and ready to be decompressed?
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        bool IsInitialized { get;}

        /// <summary>
        /// Types supported for TOriginalType parameter
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        Type[] SupportedInputTypes { get;}

        /// <summary>
        /// The supported OutputTypes.
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        Type[] SupportedOutputTypes { get; }

        /// <summary>
        /// The current input type. (Passed to the compression in the constructor)
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        Type CurrentInputType { get;}

        /// <summary>
        /// The current output type. (Passed to the compression in the constructor)
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        Type CurrentOutputType { get;}

        /// <summary>
        /// The maximum relative (in percentage) error possible in the current compression.
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        double MaxRelativeError { get;}

        /// <summary>
        /// The maximum absolute error possible in the current compression.
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        double MaxAbsError { get;}

        /// <summary>
        /// Compress data
        /// </summary>
        /// <param name="data">Data to compress</param>
        /// <param name="lengths">Dimension lengths</param>
        [CeresCreated("Mikkel", "03/13/06")]
        IList CompressRange(IList data, int[] lengths);
        
        /// <summary>
        /// Decompress data
        /// </summary>
        /// <param name="data">Data to decompress</param>
        /// <param name="firstIndices"></param>
        /// <param name="lastIndices"></param>
        /// <param name="strides"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        IList DecompressRange(IList data, int[] firstIndices, int[] lastIndices, int[] strides);
    }
}
