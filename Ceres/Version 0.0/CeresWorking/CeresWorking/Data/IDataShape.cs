using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using CeresBase.Development;
using CeresBase.IO;

namespace CeresBase.Data
{
    /// <summary>
    /// This interface is used to translate coordinates from an specific "real world" space
    /// (like lat/lon/hgt) to array index space.
    /// </summary>
    [CeresCreated("Mikkel", "03/13/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    public interface IDataShape : ICeresSerializable
    {
        /// <summary>
        /// Convert a point in shape space to index locations
        /// </summary>
        /// <param name="args">An object that describes the location.</param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        double[] ShapeToGrid(object args);

        /// <summary>
        /// Convert an index location to a point in shape space.
        /// </summary>
        /// <param name="gridCoords">Array specifying a location in data</param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        object GridToShape(double[] gridCoords);

        /// <summary>
        /// Convert an array of locations in shape space to an array of index locations.
        /// </summary>
        /// <param name="args">An array of objects that describes the location in shape space.</param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        double[][] ShapeToGridArray(object[] args);

        /// <summary>
        /// Convert an array of index locations to an array of objects that describes locations in shape space.
        /// </summary>
        /// <param name="gridCoords"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        object[] GridToShapeArray(double[][] gridCoords);

        ///// <summary>
        ///// Tak data and the boundaries of the data as specified in shape space and rearrange it to some
        ///// internal format that the DataShape uses. For example, if the data shape specified lat/lon coords and
        ///// went north/south and west/east, the user shouldn't have to know this. If they passed in data
        ///// and boundaries that specified their data was north/south and east/west, then transpose the data
        ///// so it is in correct orientation.
        ///// </summary>
        ///// <param name="data">The raw data fed to the shape.</param>
        ///// <param name="bounds">An array specifying the bounadries of the data. Will be unique for each
        ///// data space</param>
        ///// <param name="counts">An array of the dimension lengths.</param>
        ///// <param name="rowmajor">Was the data written in row major or column major form?</param>
        ///// <returns></returns>
        //[CeresCreated("Mikkel", "03/13/06")]
        //Array ArrangeData(Array data, object[] bounds, int[] counts, bool rowmajor);

        /// <summary>
        /// Return user friendly names for the dimensions in shape space.
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        string[] DimensionNames { get;}

        //Array[] GetValuesOfDimensions(int[] lengths);
        //int GetHashCode();
        //string ToString();
    }
}