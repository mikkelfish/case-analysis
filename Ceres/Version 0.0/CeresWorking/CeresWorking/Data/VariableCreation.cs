//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Reflection;
//using CeresBase.Development;
//using Razor.Configuration;
//using CeresBase.UI.MethodEditor;

//namespace CeresBase.Data
//{
//    //[CeresBase.Settings.XMLSetting("VariableCreation", "", true, false)]
//    static class VariableCreation
//    {
//        //private static Settings.XMLSettingAttribute attribute;


//        public static ThreadSafeConstructs.ThreadSafeEvent<UI.LoadingItemEventArgs> ItemStarted
//            = new ThreadSafeConstructs.ThreadSafeEvent<CeresBase.UI.LoadingItemEventArgs>();
//        public static ThreadSafeConstructs.ThreadSafeEvent<UI.LoadingItemEventArgs> ItemEnded =
//            new ThreadSafeConstructs.ThreadSafeEvent<CeresBase.UI.LoadingItemEventArgs>();


//        static VariableCreation()
//        {
//            try
//            {
//                attribute = CeresBase.General.Utilities.GetAttribute<CeresBase.Settings.XMLSettingAttribute>(typeof(VariableCreation), true);
//                foreach (XmlConfigurationCategory cat in attribute.Category.Categories)
//                {

//                    MethodInfo info = Settings.SettingsUtilities.StringToMethod((string)cat.Options["MethodInfoKey"].Value);

//                    Type varHeaderType = Type.GetType((string)cat.Options["VarHeaderType"].Value);
//                    bool methodToVar = (bool)cat.Options["MethodToVar"].Value;
//                    XmlConfigurationCategory assoc = cat.Categories["Associations"];

//                    string consString = (string)assoc.Options["ConstructorInfo"].Value;

//                    ConstructorInfo consInfo = Settings.SettingsUtilities.StringToConstructorInfo(consString);
                    
                    
//                    Dictionary<string, string> nonCons = Settings.SettingsUtilities.StringToDictionary<string, string>((string)assoc.Options["NonConstructorParameters"].Value);
//                    List<string> constructorParas = Settings.SettingsUtilities.StringToList((string)assoc.Options["ConstructorParameters"].Value);
//                    List<string> constructorNames = Settings.SettingsUtilities.StringToList((string)assoc.Options["ConstructorNames"].Value);
//                    Dictionary<string, bool> readOnlys = Settings.SettingsUtilities.StringToDictionary<string, bool>((string)assoc.Options["ReadOnly"].Value);
//                    Dictionary<string, object> defaults = Settings.SettingsUtilities.StringToDictionary<string, object>((string) assoc.Options["Defaults"].Value);


//                    entryKey key = new entryKey(info, varHeaderType, methodToVar);
//                    entryValue value = new entryValue();
//                    value.Constructor = consInfo;
//                    value.ConstructorNames.AddRange(constructorNames);
//                    value.ConstructorParameters.AddRange(constructorParas);
//                    foreach (string nKey in nonCons.Keys)
//                    {
//                        value.NonConstructorParameters.Add(nKey, nonCons[nKey]);
//                    }

//                    foreach (string nKey in readOnlys.Keys)
//                    {
//                        value.ReadOnly.Add(nKey, readOnlys[nKey]);
//                    }

//                    foreach(string nKey in defaults.Keys)
//                    {
//                        value.Defaults.Add(nKey, defaults[nKey]);
//                    }

//                    associations.Add(key.GetHashCode(), value);
//                }
//            }
//            catch (Exception ex)
//            {
//                int x = 0;
//            }
//        }

//        #region Entry Classes
//        private class varCreateKey
//        {
//            private VariableHeader header;
//            public VariableHeader Header
//            {
//                get { return header; }
//            }

//            public varCreateKey(VariableHeader header)
//            {
//                this.header = header;
//            }

//            public override int GetHashCode()
//            {
//                return (header.VarName + header.ExperimentName).GetHashCode();
//            }
//        }

//        private class varCreateValue
//        {
//            private MethodInfo info;
//            public MethodInfo Info
//            {
//                get { return info; }
//            }

            
//            private object[] args;
//            public object[] Args
//            {
//                get { return args; }
//            }

//            private VariableHeader createdHeader;
//            public VariableHeader CreatedHeader
//            {
//                get { return createdHeader; }
//            }

//            private UI.MethodEditor.EditorInfoAttribute attr;
//            public UI.MethodEditor.EditorInfoAttribute Attr
//            {
//                get
//                {
//                    return this.attr;
//                }
//            }

//            public varCreateValue(object[] args, UI.MethodEditor.EditorInfoAttribute attr,  VariableHeader createdHeader, MethodInfo info)
//            {
//                this.args = args;
//                this.info = info;
//                this.createdHeader = createdHeader;
//                this.attr = attr;
//            }	
//        }

//        private class entryKey
//        {
//            private MethodInfo info;
//            public MethodInfo Info
//            {
//                get { return info; }
//            }
//            private Type varheadType;
//            public Type VariableHeaderType
//            {
//                get { return varheadType; }
//            }

//            private bool methodToVar;
//            public bool IsMethodToVar
//            {
//                get { return methodToVar; }
//            }	

//            public entryKey(MethodInfo info, Type varheadType, bool methodToVar)
//            {
//                this.info = info;
//                this.varheadType = varheadType;
//                this.methodToVar = methodToVar;
//            }

//            public override int GetHashCode()
//            {
//                if (methodToVar) return (info.Name + varheadType.FullName).GetHashCode();
//                else return (varheadType.FullName + info.Name).GetHashCode();
//            }
//        }
//        private class entryValue
//        {
//            private ConstructorInfo constructor;
//            public ConstructorInfo Constructor
//            {
//                get { return constructor; }
//                set
//                {
//                    this.constructor = value;
//                }
//            }

//            private Dictionary<string, string> nonConsParas;
//            public Dictionary<string, string> NonConstructorParameters
//            {
//                get { return nonConsParas; }
//            }

//            private List<string> constructorParas;
//            public List<string> ConstructorParameters
//            {
//                get { return constructorParas; }
//            }

//            private List<string> constructorNames;
//            public List<string> ConstructorNames
//            {
//                get { return constructorNames; }
//            }

//            private Dictionary<string, object> defaults ;
//            public Dictionary<string, object> Defaults
//            {
//                get { return defaults; }
//            }

//            private Dictionary<string, bool> readOnly;
//            public Dictionary<string, bool> ReadOnly
//            {
//                get { return readOnly; }
//            }	

//            public entryValue()
//            {
//                constructorParas = new List<string>();
//                nonConsParas = new Dictionary<string, string>();
//                constructorNames = new List<string>();
//                defaults = new Dictionary<string, object>();
//                this.readOnly = new Dictionary<string, bool>();
//            }
//        }

//        #endregion

//        private static Dictionary<int, entryValue>
//            associations = new Dictionary<int, entryValue>();

//        private static Dictionary<varCreateKey, List<varCreateValue>>
//            queue = new Dictionary<varCreateKey, List<varCreateValue>>();

//        private static readonly object lockable = new object();

//        public static bool ContainsAssociation(MethodInfo info, Type varHeaderType, bool methodToVar)
//        {
//            entryKey key = new entryKey(info, varHeaderType, methodToVar);
//            return associations.ContainsKey(key.GetHashCode());
//        }

//        public static void RemoveAssociation(MethodInfo info, Type varHeaderType, bool methodToVar)
//        {
//            entryKey key = new entryKey(info, varHeaderType, methodToVar);
//            associations.Remove(key.GetHashCode());
//            attribute.Category.Categories.Remove(key.GetHashCode().ToString());
//        }

//        private static void addAssociation(entryKey key, entryValue val)
//        {
//            lock (lockable)
//            {
//                if(!associations.ContainsKey(key.GetHashCode())) associations.Add(key.GetHashCode(), val);
//                XmlConfigurationCategory cat =
//                    attribute.Category.Categories[key.GetHashCode().ToString(), true];

//                XmlConfigurationOption methodOption = cat.Options["MethodInfoKey", true];
//                methodOption.Value = Settings.SettingsUtilities.MethodToString(key.Info);

//                XmlConfigurationOption typeOption = cat.Options["VarHeaderType", true];
//                typeOption.Value = key.VariableHeaderType.AssemblyQualifiedName;

//                XmlConfigurationOption methodToVarOption = cat.Options["MethodToVar", true];
//                methodToVarOption.Value = key.IsMethodToVar;

//                XmlConfigurationCategory assocCat = cat.Categories["Associations", true];
//                XmlConfigurationOption constructOption = assocCat.Options["ConstructorInfo", true];

//                string fullConsstring = Settings.SettingsUtilities.ConstructorInfoToString(val.Constructor);
//                constructOption.Value = fullConsstring;

//                XmlConfigurationOption nonCons = assocCat.Options["NonConstructorParameters", true];
//                nonCons.Value = Settings.SettingsUtilities.DictionaryToString(val.NonConstructorParameters);

//                XmlConfigurationOption constructorParas = assocCat.Options["ConstructorParameters", true];
//                constructorParas.Value = Settings.SettingsUtilities.ListToString(val.ConstructorParameters);

//                XmlConfigurationOption constructorNames = assocCat.Options["ConstructorNames", true];
//                constructorNames.Value = Settings.SettingsUtilities.ListToString(val.ConstructorNames);

//                XmlConfigurationOption readOnly = assocCat.Options["ReadOnly", true];
//                readOnly.Value = Settings.SettingsUtilities.DictionaryToString<string, bool>(val.ReadOnly);

//                XmlConfigurationOption defaults = assocCat.Options["Defaults", true];
//                defaults.Value = Settings.SettingsUtilities.DictionaryToString<string, object>(val.Defaults);

                
//            }
//        }

//        public static void SetMethodAssociation(Type varType, MethodInfo info, PropertyInfo[] varProperties, string[] methodParaNames)
//        {
//            entryKey key = new entryKey(info, varType, false);
//            entryValue val = null;
//            if (associations.ContainsKey(key.GetHashCode()))
//            {
//                val = associations[key.GetHashCode()];
//            }
//            else val = new entryValue();

//            val.Constructor = null;
//            val.NonConstructorParameters.Clear();
//            for (int i = 0; i < varProperties.Length; i++)
//            {
//                val.NonConstructorParameters.Add(methodParaNames[i], varProperties[i].Name);
//            }
//            addAssociation(key, val);
//        }

//        public static Dictionary<string, object> GetMethodValues(VariableHeader header, MethodInfo info)
//        {
//            entryKey key = new entryKey(info, header.GetType(), false);
//            if (!associations.ContainsKey(key.GetHashCode())) return null;
//            entryValue val = associations[key.GetHashCode()];
//            Dictionary<string, object> paramVals = new Dictionary<string, object>();
//            ParameterInfo[] paras = info.GetParameters();
//            foreach (string methodParam in val.NonConstructorParameters.Keys)
//            {
//                object toAdd = header.GetType().InvokeMember(val.NonConstructorParameters[methodParam],
//                    BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty, null,
//                    header, null);
                
//                ParameterInfo para = Array.Find<ParameterInfo>(paras,
//                    delegate(ParameterInfo p)
//                    {
//                        return p.Name == methodParam;
//                    }
//                );
//                paramVals.Add(methodParam, CeresBase.General.Utilities.Convert(toAdd, para.ParameterType));
//            }
//            return paramVals;
//        }

//        public static void SetAssociatedDefaults(MethodInfo info, Type varHeaderType, string[] names, bool[] readOnly, object[] values, bool areMethodDefaults)
//        {
//            entryKey key = new entryKey(info, varHeaderType, !areMethodDefaults);
//            entryValue val = null;
//            if (associations.ContainsKey(key.GetHashCode()))
//            {
//                val = associations[key.GetHashCode()];
//            }
//            else val = new entryValue();

//            val.Defaults.Clear();
//            val.ReadOnly.Clear();
//            for (int i = 0; i < names.Length; i++)
//            {
//                val.Defaults.Add(names[i], values[i]);
//                val.ReadOnly.Add(names[i], readOnly[i]);
//            }
//            addAssociation(key, val);
//        }

//        public static Dictionary<string, object> GetDefaults(MethodInfo info, Type varHeaderType, bool areMethodDefaults)
//        {
//            entryKey key = new entryKey(info, varHeaderType, !areMethodDefaults);
//            if(!associations.ContainsKey(key.GetHashCode())) return null;
//            return associations[key.GetHashCode()].Defaults;
//        }

//        public static Dictionary<string, bool> GetDefaultReadOnly(MethodInfo info, Type varHeaderType, bool areMethodDefaults)
//        {
//            entryKey key = new entryKey(info, varHeaderType, !areMethodDefaults);
//            if (!associations.ContainsKey(key.GetHashCode())) return null;
//            return associations[key.GetHashCode()].ReadOnly;
//        }

//        public static ParameterInfo[] GetNeededHeaderParams(Type headerType, MethodInfo info)
//        {
//            entryKey key = new entryKey(info, headerType, true);
//            if (!associations.ContainsKey(key.GetHashCode())) return new ParameterInfo[] { };
//            entryValue val = associations[key.GetHashCode()];
            
//            List<ParameterInfo> toRet = new List<ParameterInfo>();
//            ParameterInfo[] paras = val.Constructor.GetParameters();
//            for (int i = 0; i < paras.Length; i++)
//            {
//                if (val.Defaults.ContainsKey(paras[i].Name))
//                    toRet.Add(paras[i]);
//            }
//            return toRet.ToArray();
//        }

//        [CeresToDo("Mikkel", "03/20/06", Comments="Incorporate into auto-binding part of framework")]
//        [CeresToDo("Mikkel", "03/20/06", Comments = "Exception", Priority=DevelopmentPriority.Exception)]
//        public static void SetHeaderAssociation(MethodInfo info, Type varHeaderType, string[] paras,
//            string[] varPars)
//        {
//            entryKey key = new entryKey(info, varHeaderType, true);
//            entryValue val = null;
//            if (associations.ContainsKey(key.GetHashCode()))
//            {
//                val = associations[key.GetHashCode()];
//            }
//            else val = new entryValue();

//            val.Constructor = null;
//            val.ConstructorNames.Clear();
//            val.ConstructorParameters.Clear();
//            val.NonConstructorParameters.Clear();

//            ConstructorInfo[] constructors = varHeaderType.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
//            foreach (ConstructorInfo cInfo in constructors)
//            {
//                object[] args = cInfo.GetCustomAttributes(typeof(UI.MethodEditor.EditorConstructorAttribute), true);
//                if (args == null || args.Length == 0) continue;
//                ParameterInfo[] cParams = cInfo.GetParameters();
//                foreach (ParameterInfo cParam in cParams)
//                {
//                    int index = Array.FindIndex<string>(varPars,
//                        delegate(string varPara)
//                        {
//                            return varPara == cParam.Name;
//                        }
//                    );
//                    if (index < 0) continue;
//                    val.ConstructorNames.Add(cParam.Name);
//                    val.ConstructorParameters.Add(paras[index]);
//                }
//                val.Constructor = cInfo;
//            }

//            if (val.Constructor == null) throw new Exception("Couldn't bind");



//            for (int i = 0; i < varPars.Length; i++)
//            {
//                string toAdd = varPars[i];
//                if (val.ConstructorNames.Contains(toAdd)) continue;
//                val.NonConstructorParameters.Add(paras[i], toAdd);
//            }

//            addAssociation(key, val);        
//        }
    
//        private static VariableHeader createHeader(MethodInfo info, Type varHeaderType, string[] methodParaNames, object[] values,
//            ParameterInfo[] fromMethod, object[] fromMethodVals)
//        {
//            entryKey key = new entryKey(info, varHeaderType, true);
//            if (!associations.ContainsKey(key.GetHashCode())) return null;
//            entryValue val = associations[key.GetHashCode()];

//            ParameterInfo[] paras = val.Constructor.GetParameters();
//            object[] toPass = new object[paras.Length];
//            for (int i = 0; i < paras.Length; i++)
//            {
//                if (!val.ConstructorNames.Contains(paras[i].Name))
//                {
//                    int index = Array.FindIndex<string>(methodParaNames,
//                                            delegate(string name)
//                                            {
//                                                return name == paras[i].Name;
//                                            });
//                    if (index < 0)
//                    {
//                        toPass[i] = val.Defaults[paras[i].Name];
//                    }
//                    else
//                    {
//                        toPass[i] = values[index];
//                    }
//                }
//                else
//                {
//                    //Linked
//                    int index = val.ConstructorNames.IndexOf(paras[i].Name);

//                    if (index < 0) throw new Exception("Couldn't assign parameter to constructor!");
//                    index = Array.FindIndex<ParameterInfo>(fromMethod,
//                        delegate(ParameterInfo para)
//                        {
//                            return para.Name == val.ConstructorParameters[index];
//                        }
//                    );
//                    if (index < 0) throw new Exception("Couldn't assign parameter to constructor!");
//                    toPass[i] = fromMethodVals[index];
//                }
//            }

//            VariableHeader header = (VariableHeader)val.Constructor.Invoke(toPass);            
//            return header;
//        }

        

//        public static VariableHeader AddVariableToCreate(MethodInfo info, VariableHeader header, string[] methodParaNames, object[] values)
//        {
//            object[] attrs = info.GetCustomAttributes(typeof(UI.MethodEditor.EditorInfoAttribute), true);
//            UI.MethodEditor.EditorInfoAttribute attr = attrs[0] as UI.MethodEditor.EditorInfoAttribute;

//            Dictionary<string, object> methodVals = GetMethodValues(header, info);
//            ParameterInfo[] methodParams = info.GetParameters();
//            object[] toPass = new object[methodParams.Length];
//            for (int i = 0; i < methodParams.Length; i++)
//            {
//                ParameterInfo param = methodParams[i];
//                if (methodVals.ContainsKey(param.Name))
//                {
//                    toPass[i] = methodVals[param.Name];
//                }
//                else
//                {
//                    int index = Array.FindIndex<string>(methodParaNames,
//                        delegate(string name)
//                        {
//                            return name == param.Name;
//                        }
//                    );

//                    if (index < 0) throw new Exception("Couldn't assign parameter");

//                    toPass[i] = values[index];
//                }
//            }

            
//            VariableHeader createdHeader = null;
//            if (info.ReturnType == typeof(VariableHeader) ||
//                info.ReturnType.IsSubclassOf(typeof(VariableHeader)))
//            {
                
//            }
//            else
//            {
//                createdHeader = VariableCreation.createHeader(info, header.GetType(), methodParaNames, values, 
//                    methodParams, toPass);
//                varCreateKey key = new varCreateKey(header);
//                varCreateValue val = new varCreateValue(toPass, attr, createdHeader, info);
//                if (!queue.ContainsKey(key))
//                    queue[key] = new List<varCreateValue>();
//                queue[key].Add(val);
//            }

//            return createdHeader;
//        }

//        public static void RunItems()
//        {
//            varCreateKey[] keys = new varCreateKey[queue.Keys.Count];
//            queue.Keys.CopyTo(keys, 0);
//            int keyCount = 0;
//            foreach (varCreateKey key in keys)
//            {
//                object[] tags = new object[queue[key].Count];
//                MethodInfo[] mainBodies = new MethodInfo[queue[key].Count];
//                MethodInfo[] done = new MethodInfo[queue[key].Count];                
//                bool[] supportsStrides = new bool[queue[key].Count];
//                IDataPool[] pools = new IDataPool[queue[key].Count];
                
//                Variable var = VariableSource.GetVariable(key.Header); 
//                DataFrame frame = var[0];
                
//                VariableCreation.ItemStarted.CallEvent(null, 
//                    new CeresBase.UI.LoadingItemEventArgs("Running Functions on Variable: " + var.Header.Description,
//                    keyCount, keys.Length, false));


//                for (int i = 0; i < queue[key].Count; i++)
//                {
//                        varCreateValue val = queue[key][i];
//                        tags[i] = val.Info.Invoke(null, val.Args);

//                        if (val.Attr is EditorFunctionAttribute)
//                        {
//                            MethodInfo lenInfo = (MethodInfo)val.Info.DeclaringType.GetMember((val.Attr as EditorFunctionAttribute).GetLengthFunction, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static)[0];
//                            int[] dimLens = (int[])lenInfo.Invoke(null, new object[] { tags[i], frame.Pool, var });
//                            pools[i] = new DataPools.DataPoolNetCDF(dimLens);
//                        }

//                        if (val.Attr.Callback != "")
//                        {
//                            mainBodies[i] = (MethodInfo)val.Info.DeclaringType.GetMember(val.Attr.Callback, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static)[0];
//                        }
//                        if (val.Attr.Finalize != "")
//                        {
//                            done[i] = (MethodInfo)val.Info.DeclaringType.GetMember(val.Attr.Finalize, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static)[0];
//                        }
//                       supportsStrides[i] = val.Attr.SupportsStrides;
//                }

//                try
//                {

//                    frame.Pool.GetData(null, null,
//                        delegate(float[] data, uint[] indices, uint[] counts)
//                        {
//                            for (int i = 0; i < mainBodies.Length; i++)
//                            {
//                                if (!supportsStrides[i])
//                                {
//                                    mainBodies[i].Invoke(null, new object[]{pools[i], data, frame.Pool.DimensionLengths,
//                                      indices, counts, tags[i]});
//                                }
//                                else
//                                {
//                                    uint[] lastIndices = new uint[indices.Length];
//                                    uint[] strides = new uint[indices.Length];
//                                    for (int j = 0; j < lastIndices.Length; j++)
//                                    {
//                                        lastIndices[j] = indices[j] + counts[j];
//                                        strides[j] = 1;
//                                    }
//                                    mainBodies[i].Invoke(null, new object[]{pools[i], data, frame.Pool.DimensionLengths,
//                                    indices, lastIndices, strides, tags[i]});
//                                }
//                            }
//                        }
//                    );

//                    for (int i = 0; i < pools.Length; i++)
//                    {
//                        //TODO CHANGE
//                        Variable newVar = IO.IOFactory.CreateVariable(queue[key][i].CreatedHeader);
//                        DataFrame dataFrame = new DataFrame(newVar, pools[i], new DataShapes.NoShape(), General.Constants.Timeless);
//                        newVar.AddDataFrame(dataFrame);
//                        VariableSource.AddVariable(newVar);
//                    }

//                    VariableCreation.ItemEnded.CallEvent(null,
//                        new CeresBase.UI.LoadingItemEventArgs("Running Functions on Variable: " + var.Header.Description,
//                        keyCount, keys.Length, false));
//                    keyCount++;
//                }
//                catch (Exception ex)
//                {
//                    int asfe = 0;
//                }

//            }

//            queue.Clear();
//        }
//    }
//}
