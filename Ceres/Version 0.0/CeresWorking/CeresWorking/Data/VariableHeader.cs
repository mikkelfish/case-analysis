using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;
using System.ComponentModel;
using CeresBase.IO.DataBase;
using CeresBase.IO;
using CeresBase.Data.DataShapes;

namespace CeresBase.Data
{
    /// <summary>
    /// Stores basic information about the variable. Should be propogated when loading a file.
    /// </summary>
    [CeresCreated("Mikkel", "03/13/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    [CeresModified("Mikkel", "03/15/06", Comments = "Added experiment name", DocumentedStatus=CeresDocumentedStage.Complete)]
    public class VariableHeader : IO.ICeresSerializable
    {
        #region Private Variables
        private string varName;
        private string unitsName;
        private string description;
        private string expt;
        #endregion

        [Browsable(false)]
        public virtual Type VarType
        {
            get
            {
                return typeof(Variable);
            }
        }

        private string fileName;
        public string FileName
        {
            get
            {
                return this.fileName;
            }
            set
            {
                this.fileName = value;
            }
        }

        #region Private Functions

        #endregion

        #region Properties

        /// <summary>
        /// Get the unique variable name.
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        [System.ComponentModel.Category("Common Properties")]
        [System.ComponentModel.Description("This is the internal name for the variable.")]
        public string VarName { get { return varName; } }

        /// <summary>
        /// Get the units for the variable
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        [System.ComponentModel.Category("Common Properties")]
        [System.ComponentModel.Description("This is the type of units for the variable.")]
        public string UnitsName { get { return this.unitsName; } }

        /// <summary>
        /// Get the display text for the variable
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        [System.ComponentModel.Category("Common Properties")]
        [System.ComponentModel.Description("This is what will be displayed in all programs that use this variable.")]
        public string Description 
        { 
            get 
            { 
                return this.description; 
            }
            set
            {
                this.description = value;
            }
        }

        private string comments;
        [CeresCreated("Mikkel", "03/13/06")]
        [System.ComponentModel.Category("Common Properties")]
        [System.ComponentModel.Description("Enter any comments here.")]
        public string Comments
        {
            get { return comments; }
            set { comments = value; }
        }
	

        [CeresCreated("Mikkel", "03/15/06")]
        [System.ComponentModel.Category("Common Properties")]
        [System.ComponentModel.Description("This is the name of the experiment that the variable belongs to.")]
        public string ExperimentName 
        { 
            get 
            { 
                return this.expt; 
            } 
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create a variable header where the display name is the same as the variable name.
        /// </summary>
        /// <param name="varName"></param>
        /// <param name="unitsName"></param>
        [CeresCreated("Mikkel", "03/13/06")]
        [CeresModified("Mikkel", "03/15/06", Comments = "Added experiment name")]
        protected VariableHeader(string varName, string unitsName, string experimentName)
            :
            this(varName, unitsName, varName, experimentName) { }

        /// <summary>
        /// Create a variable header with a user supplied display name and variable name.
        /// </summary>
        /// <param name="varName"></param>
        /// <param name="unitsName"></param>
        /// <param name="description"></param>
        [CeresCreated("Mikkel", "03/13/06")]
        [CeresModified("Mikkel", "03/15/06", Comments = "Added experiment name")]
        protected VariableHeader(string varName, string unitsName, string description, string experimentName)
        {
            this.varName = varName;
            this.unitsName = unitsName;
            this.description = description;
            this.expt = experimentName;
            this.comments = "";
        }


        #endregion

        #region Public Functions

        public static bool operator ==(VariableHeader header1, VariableHeader header2)
        {
            if (!(header1 is VariableHeader))
                return false;
            return header1.Equals(header2);
        }

        public static bool operator !=(VariableHeader header1, VariableHeader header2)
        {
            if (!(header1 is VariableHeader))
                return false;
            return !(header1 == header2);
        }

        public override bool Equals(object obj)
        {
            if (obj is VariableHeader)
            {
                VariableHeader header = obj as VariableHeader;
                return this.VarName == header.VarName && this.expt == header.expt;
            }

            return false;
        }

        public override int GetHashCode()
        {
                return (this.VarName + this.expt).GetHashCode();
        }

        #endregion

        #region ICeresSerializable Members

        public virtual void AddSpecificInformation(CeresBase.IO.ICeresWriter writer, CeresBase.IO.ICeresFile file)
        {
           writer.AddAttribute(file, this.VarName, "Comments", this.comments);
            writer.AddAttribute(file, this.varName, "Description", this.description);
        }

        public virtual void ReadSpecificInformation(CeresBase.IO.ICeresReader reader, CeresBase.IO.ICeresFile file)
        {
         //   this.comments = (string)reader.ReadAttribute(file, this.varName, "Comments");
         //   this.description = (string)reader.ReadAttribute(file, this.varName, "Description");
        }

        #endregion

        public virtual FileDataBaseVariableEntryParameter GetDataBaseParameter(ICeresReader reader, ICeresFile file)
        {
            FileDataBaseVariableEntryParameter para = new FileDataBaseVariableEntryParameter();
            return para;
        }

        public virtual IDataShape CreateShape()
        {
            return new NoShape();
        }

        public override string ToString()
        {
            return this.ExperimentName + "_" + this.description;
        }

    }
}
