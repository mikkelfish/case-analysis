using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.Data.DataShapes
{
    public class NoShape : IDataShape
    {
        public double[] ShapeToGrid(object args)
        {
            return (double[])args;
        }

        public object GridToShape(double[] gridCoords)
        {
            return gridCoords;
        }

        public double[][] ShapeToGridArray(object[] args)
        {
            return (double[][])args;
        }

        public object[] GridToShapeArray(double[][] gridCoords)
        {
            return gridCoords;
        }

        //public Array ArrangeData(Array data, object[] bounds, int[] counts, bool rowmajor)
        //{
        //    return data;
        //}

        public string[] DimensionNames
        {
            get { return new string[] { "None" }; }
        }

        //public Array[] GetValuesOfDimensions(int[] lengths)
        //{
        //    return null;
        //    //List<Array> toRet = new List<Array>();
        //    //for (int i = 0; i < lengths.Length; i++)
        //    //{
        //    //    toRet.Add(new double[lengths[i]]);
        //    //    for (int j = 0; j < lengths[i]; j++)
        //    //    {
        //    //        ((double[])toRet[i])[j] = j;
        //    //    }
        //    //}
        //    //return toRet.ToArray();
        //}

        ////public int CompareTo(object obj)
        ////{
        ////    if(!(obj is NoShape)
        ////}


        public void AddSpecificInformation(CeresBase.IO.ICeresWriter writer, CeresBase.IO.ICeresFile file)
        {

        }

        public void ReadSpecificInformation(CeresBase.IO.ICeresReader reader, CeresBase.IO.ICeresFile file)
        {

        }
    }
}
