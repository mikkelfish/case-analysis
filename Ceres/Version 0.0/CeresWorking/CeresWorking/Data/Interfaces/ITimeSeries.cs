﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.Data.Interfaces
{
    public interface ITimeSeries
    {
        double Resolution { get; }
    }
}
