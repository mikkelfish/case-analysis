using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;

namespace CeresBase.Data
{
    /// <summary>
    /// The interface takes in a location in data shape space and then interpolates a value
    /// from the given data pool.
    /// </summary>
    [CeresCreated("Mikkel", "03/13/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    public interface IPointInterpolation
    {
        /// <summary>
        /// Get a single interpolated value at the given location.
        /// </summary>
        /// <param name="location"></param>
        /// <param name="pool"></param>
        /// <param name="shape"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        float GetAndInterpolate(object location, IDataPool pool, IDataShape shape);

        /// <summary>
        /// Give an array of locations and get the interpolated values at each location.
        /// </summary>
        /// <param name="locations"></param>
        /// <param name="pool"></param>
        /// <param name="shape"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        float[] GetAndInterpolateArray(object[] locations, IDataPool pool, IDataShape shape);

        float[] GetContiguousData(object start, object end, IDataPool pool, IDataShape shape);
    }
}
