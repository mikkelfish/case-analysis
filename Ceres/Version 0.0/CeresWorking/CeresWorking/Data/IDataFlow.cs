using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;

namespace CeresBase.Data
{
    /// <summary>
    /// Used for iterative data streaming. I got this idea from Google Maps and the like, where 
    /// it does multiple passes to do calculations. The interface allows for easily subsampling the 
    /// data when the user is interacting with it, and then filling in the specifics once they have decided
    /// what they want.
    /// </summary>
    [CeresCreated("Mikkel", "03/14/06")]
    public interface IDataFlow
    {
        /// <summary>
        /// Do a pass.
        /// </summary>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        float[] DoPass();
        
        /// <summary>
        /// Do a pass and get the pass number.
        /// </summary>
        /// <param name="passNumber"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        float[] DoPass(out int passNumber);

        /// <summary>
        /// Do a pass and get the pass number and indices associated with the data
        /// </summary>
        /// <param name="dataIndices">Indices associated with the data</param>
        /// <param name="passNumber">Pass number</param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        float[] DoPass(out int[][] dataIndices, out int passNumber);

        /// <summary>
        /// Get the data frame associated with the data flow
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        DataFrame Frame { get;}

        /// <summary>
        /// Get the first corner
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        int[] FirstIndices { get;}

        /// <summary>
        /// Get the second corner
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        int[] LastIndices { get;}
    }
}
