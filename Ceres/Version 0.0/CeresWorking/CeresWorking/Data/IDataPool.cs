using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;
using System.IO;

namespace CeresBase.Data
{
    /// <summary>
    /// This interface implements the actual data storage.
    /// </summary>
    [CeresCreated("Mikkel", "03/13/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    public interface IDataPool : IDisposable
    {
        void FrameAdded();

        void FrameRemoved();

        /// <summary>
        /// Sometimes we've already calculated the min/max while setting the data. This allows a user
        /// to set the min/max so the pool doesn't have to recalculate.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        [CeresCreated("Mikkel", "03/20/06")]
        void SetMinMaxManually(float min, float max);

        /// <summary>
        /// The compression chain associated with the data pool. This is used to compress and decompress
        /// all writes/reads from the pool
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        CompressionChain Chain { get;}
        
        /// <summary>
        /// It is rather common that the performance engine will want to simply pop off the last 
        /// layer of compression. This should take care of the necessary side effects
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        void PopCompression();
        
        /// <summary>
        /// Returns the current type the data is actually stored in. This will change based on the
        /// compression(s) used.
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        Type CurrentStorageType { get;}

        /// <summary>
        /// Get the value at the current location
        /// </summary>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/20/06")]
        float GetDataValue();

        /// <summary>
        /// Reads a single value and decompresses it.
        /// </summary>
        /// <param name="indices">Define the data point to read. Must be same size as the DataPool.NumDimensions</param>
        /// <returns>The value at the indices</returns>
        [CeresCreated("Mikkel", "03/13/06")]
        float GetDataValue(int[] indices);

        /// <summary>
        /// Reads and returns a block of data with dimensions of amount, starting at indices. This makes it
        /// easy to get blocks of any rectangular shape. Decompresses the data before returning.
        /// </summary>
        /// <param name="amount">An array of lengths. Must be the same size of indices. Simply pass in 0 for any dimension if you want to collapse the
        /// return array or pass null if you want to read to the end.</param>
        /// <param name="indices">An array of start indices. Must provide an indice for each appropriate dimension OR pass null to have it start at the beginning of the data.</param>
        /// <example>Passing GetData({10,20,10},{9,1,2}) to a DataPool3D would return a 3D block
        /// of data that is 10x20x10 with the range of data read {9-18,1-20,2-11}.</example>
        /// <returns>An array of the WorkingType.</returns>
        [CeresCreated("Mikkel", "03/13/06")]
        void GetData(int[] indices, int[] amount, CeresBase.Data.DataUtilities.PoolReadDelegate readFunction);

        /// <summary>
        /// This function is for reading a subsampling of the data. Of course the data is decompressed
        /// before returning.
        /// </summary>
        /// <param name="firstIndices">The location of the first corner</param>
        /// <param name="lastIndices">The location of the second corner</param>
        /// <param name="strides">The stepsize</param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        void GetData(int[] firstIndices, int[] lastIndices, int[] strides, CeresBase.Data.DataUtilities.PoolReadStridedDelegate readFunction);

        /// <summary>
        /// Set a value at the current location in the stream.
        /// </summary>
        /// <param name="val"></param>
        [CeresCreated("Mikkel", "03/20/06")]
        void SetDataValue(float val);

        /// <summary>
        /// Set a value at a particular location
        /// </summary>
        /// <param name="val"></param>
        /// <param name="indices"></param>
        [CeresCreated("Mikkel", "03/13/06")]
        void SetDataValue(float val, int[] indices);

        /// <summary>
        /// Set the ENTIRE data pool.
        /// </summary>
        /// <param name="data">The data to set</param>
        /// <param name="amounts">The lengths of each dimension.</param>
        [CeresCreated("Mikkel", "03/13/06")]
        void SetData(Stream data);

        /// <summary>
        /// Set a section of data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="indices"></param>
        /// <param name="amounts"></param>
        [CeresCreated("Mikkel", "03/13/06")]
        void SetData(Stream data, int[] indices, int[] amounts);

        /// <summary>
        /// Set a subsampling of the data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="firstIndices"></param>
        /// <param name="lastIndices"></param>
        /// <param name="strides"></param>
        [CeresCreated("Mikkel", "03/13/06")]
        void SetData(Stream data, int[] firstIndices, int[] lastIndices, int[] strides);

        /// <summary>
        /// Get the length of a particular dimension
        /// </summary>
        /// <param name="index">Dimension to return the length</param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        int GetDimensionLength(int index);

        /// <summary>
        /// Get an array of all the dimension lengths
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        int[] DimensionLengths { get;}

        /// <summary>
        /// The minimum value for the whole data pool
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        float Min { get;}

        /// <summary>
        /// The maximum value for the data pool.
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        float Max { get;}

        /// <summary>
        /// Return the number of dimensions in the pool
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        int NumDimensions { get;}

        /// <summary>
        /// ICompression's design requires that all data must be entered before the compression is completed.
        /// Call this function after all the data is set so it can do this and other startup functionality.
        /// After this function is called, the pool is ready to be integrated into the program at large.
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        void DoneSettingData();

        void CalcMinMax();

        bool BeenSet { get;}
        bool MinMaxFound { get; }
    }
}
