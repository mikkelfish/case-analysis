﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Collections;
using CeresBase.UI;

namespace CeresBase.Data
{
    public class DataSeries
    {
        public PointCollection PointList;

        public double[] XData { get; set; }
        public double[][] YData { get; set; }
        public GraphingType GraphType { get; set; }


        public DataSeries()
        {
            PointList = new PointCollection();
        }

        public DataSeries(double[] xData, double[][] yData)
        {
            PointList = new PointCollection();

            this.XData = new double[xData.Length];
            this.YData = new double[yData.Length][];

            Array.Copy(xData, XData, xData.Length);

            for (int i = 0; i < yData.Length; i++)
            {
                YData[i] = new double[yData[i].Length];
                Array.Copy(yData[i], YData[i], yData[i].Length);
            }
        }
    }
}
