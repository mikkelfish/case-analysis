using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Reflection;
using System.Threading;
using CeresBase.Development;

//MBF DUPTD 3/10/06
namespace CeresBase.Data
{
    /// <summary>
    /// This chain stores a series of compressions that data can be run through to be compressed/decompressed.
    /// It is what the datapool uses to interact with directly.
    /// </summary>
    [CeresCreated("Mikkel", "03/12/06", DocumentedStatus = CeresDocumentedStage.Complete)]
    public class CompressionChain
    {
        #region Private Variables
        private List<ICompression> compressions;
        private ManualResetEvent ev;
        #endregion

        #region Private Functions

        #endregion

        #region Properties
        /// <summary>
        /// Return the number of compressions present in this chain
        /// </summary>
        [CeresCreated("Mikkel", "03/12/06")]
        public int Count
        {
            get
            {
                ev.WaitOne();
                return this.compressions.Count;
            }
        }
        #endregion 

        #region Constructors
        /// <summary>
        /// Initalize the compression chain with a sequence of compressions
        /// </summary>
        /// <param name="compressions"></param>
        /// <param name="workingType"></param>
        [CeresCreated("Mikkel", "03/13/06")]
        public CompressionChain(ICompression[] compressions)
        {
            this.compressions = new List<ICompression>(compressions);
            this.ev = new ManualResetEvent(true);
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Decompress a value at the specified location.
        /// </summary>
        /// <param name="data">Compressed value</param>
        /// <param name="indices">Location of the value</param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public double DecompressValue(object data, int[] indices)
        {
            int[] firstIndices = new int[indices.Length];
            int[] lastIndices = new int[indices.Length];
            int[] strides = new int[indices.Length];

            IList dataArray = new object[1];
            dataArray[0] = data;
            //DataUtilities.CreateArray(1, this.workingType);
            //dataArray[0] = data;

            for (int i = 0; i < indices.Length; i++)
            {
                firstIndices[i] = indices[i];
                lastIndices[i] = indices[i];
                strides[i] = 1;
            }
            return this.DecompressRange(dataArray, firstIndices, lastIndices, strides)[0];
        }

        /// <summary>
        /// Decompress a range of data.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="lengths"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public double[] DecompressRange(IList data, int[] lengths)
        {
            int[] firstIndices = new int[lengths.Length];
            int[] lastIndices = new int[lengths.Length];
            int[] strides = new int[lengths.Length];

            for (int i = 0; i < lengths.Length; i++)
            {
                firstIndices[i] = 0;
                lastIndices[i] = lengths[i] - 1;
                strides[i] = 1;
            }
            return this.DecompressRange(data, firstIndices, lastIndices, strides);
        }

        /// <summary>
        /// Decompress a subsampled range of data NOTE: the subsampling is NOT for the data passed,
        /// the data must already be the subsampled array, the other arguments are merely so 
        /// the ICompression interfaces know exactly what data they are dealing with.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="firstIndices"></param>
        /// <param name="lastIndices"></param>
        /// <param name="strides"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public double[] DecompressRange(IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            this.ev.WaitOne();
            IList retData = data;
            for (int i = this.compressions.Count - 1; i >= 0; i--)
            {
                retData = this.compressions[i].DecompressRange(data, firstIndices, lastIndices, strides);
            }
            return (double[])retData;
        }

        /// <summary>
        /// Compress data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="lengths"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public IList CompressRange(double[] data, int[] lengths)
        {
            this.ev.WaitOne();

            IList retData = data;
            for (int i = 0; i < this.compressions.Count; i++)
            {
                retData = this.compressions[i].CompressRange(data, lengths);
            }
            return retData;
        }

        /// <summary>
        /// Pop off a compression
        /// </summary>
        /// <param name="data"></param>
        /// <param name="lengths"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public IList PopCompression(IList data, int[] lengths)
        {
            if (this.compressions.Count == 0) return data;

            int[] firstIndices = new int[lengths.Length];
            int[] lastIndices = new int[lengths.Length];
            int[] strides = new int[lengths.Length];

            for (int i = 0; i < lengths.Length; i++)
            {
                firstIndices[i] = 0;
                lastIndices[i] = lengths[i] - 1;
                strides[i] = 1;
            }

            this.ev.Reset();
            ICompression compression = null;
            try
            {
                compression = this.compressions[this.compressions.Count - 1];
                this.compressions.RemoveAt(this.compressions.Count - 1);
            }
            finally
            {
                this.ev.Set();
            }
            return compression.DecompressRange(data, firstIndices, lastIndices, strides);
        }

        /// <summary>
        /// Decompress the data using one chain and recompress it using the new chain.
        /// </summary>
        /// <param name="newChain"></param>
        /// <param name="oldChain"></param>
        /// <param name="data"></param>
        /// <param name="lengths"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static IList TransferAndRecompress(CompressionChain newChain, CompressionChain oldChain, IList data, int[] lengths)
        {
            double[] decompresseddata = oldChain.DecompressRange(data, lengths);
            return newChain.CompressRange(decompresseddata, lengths);
        }

        #endregion
    }
}
