//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Reflection;
//using CeresBase.UI.MethodEditor;

//namespace CeresBase.Data
//{
//    public static class VariableProcessingCreation
//    {


//        private static Dictionary<VariableHeader[], List<object>> methodValues =
//            new Dictionary<VariableHeader[], List<object>>();

//        private static Dictionary<VariableHeader[], VariableProcessingSetup.ProcessingAssociation> methods =
//            new Dictionary<VariableHeader[], VariableProcessingSetup.ProcessingAssociation>();
//        private static Dictionary<VariableHeader[], VariableHeader[][]> sourceHeaders = new Dictionary<VariableHeader[], VariableHeader[][]>();

//        public static VariableHeader[] AddVariablesToCreate(VariableProcessingSetup.ProcessingAssociation methodAssoc,
//            VariableProcessingSetup.ProcessingAssociation varAssoc, VariableHeader[][] headers, Dictionary<string, object> enteredValues)
//        {
//            List<object> methodVals = new List<object>();
//            Dictionary<string, object> variableVals = new Dictionary<string, object>();
//            Dictionary<string, object> tempMethodVals = new Dictionary<string, object>();

//            ParameterInfo[] methodParas = methodAssoc.Method.GetParameters();
//            for (int i = 0; i < methodParas.Length; i++)
//            {
//                ParameterInfo key = methodParas[i];
//                if (enteredValues.ContainsKey(key.Name))
//                {
//                    methodVals.Add(enteredValues[key.Name]);
//                }
//                else
//                {
//                    if (methodAssoc.Parameters[key].Value is PropertyInfo)
//                    {
//                        VariableHeader toGetFrom = null;
//                        PropertyInfo pInfo = methodAssoc.Parameters[key].Value as PropertyInfo;
//                        foreach (VariableHeader header in headers[methodAssoc.Parameters[key].FromCollection])
//                        {
//                            if (pInfo.ReflectedType ==
//                                header.GetType())
//                            {
//                                toGetFrom = header;
//                                break;
//                            }
//                        }

//                        object val = pInfo.GetValue(toGetFrom, null);
//                        val = CeresBase.General.Utilities.Convert(val, key.ParameterType);
//                        methodVals.Add(val);
//                    }
//                    else
//                    {
//                        methodVals.Add(varAssoc.Parameters[key].Value);
//                    }
//                }

//                tempMethodVals.Add(key.Name, methodVals[methodVals.Count - 1]);
//            }

//            if (varAssoc != null)
//            {
//                foreach (ParameterInfo key in varAssoc.Parameters.Keys)
//                {
//                    if (enteredValues.ContainsKey(key.Name))
//                    {
//                        variableVals.Add(key.Name, enteredValues[key.Name]);
//                    }
//                    else
//                    {
//                        if (varAssoc.Parameters[key].Value is ParameterInfo)
//                        {
//                            ParameterInfo pInfo = varAssoc.Parameters[key].Value as ParameterInfo;
//                            object val = tempMethodVals[pInfo.Name];
//                            val = CeresBase.General.Utilities.Convert(val, key.ParameterType);
//                            variableVals.Add(key.Name, val);
//                        }
//                        else
//                        {
//                            variableVals.Add(key.Name, varAssoc.Parameters[key].Value);
//                        }
//                    }
//                }
//            }

//            VariableHeader[] headersToRet = null;
//            if (varAssoc.Constructor == null)
//            {

//            }
//            else
//            {
//                ParameterInfo[] paras = varAssoc.Constructor.GetParameters();
//                string function = (methodAssoc.Attribute as EditorFunctionAttribute).GetNumberVarsCreatedFunction;
//                MethodInfo numVarsMethod = varAssoc.Method.ReflectedType.GetMethod(function, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

//                string name = "";
//                ParameterInfo nameInfo = null;
//                foreach (ParameterInfo info in paras)
//                {
//                    object[] attrs =
//                        info.GetCustomAttributes(typeof(CeresBase.UI.MethodEditor.EditorParamAttribute), true);
//                    if (attrs.Length != 0)
//                    {
//                        if ((attrs[0] as EditorParamAttribute).Description == EditorParamAttribute.VariableDescription)
//                        {
//                            name = (string)variableVals[info.Name];
//                            nameInfo = info;
//                        }

//                    }
//                }

//                string[] varNames = (string[])numVarsMethod.Invoke(null, new object[] { headers, name });

//                headersToRet = new VariableHeader[varNames.Length];

//                //TODO USE BINDING?
//                //TODO MAKE SURE NOT ALREADY CREATED/IN QUEUE
//                for (int i = 0; i < varNames.Length; i++)
//                {
//                    List<object> args = new List<object>();
//                    foreach (ParameterInfo info in paras)
//                    {
//                        if (info == nameInfo)
//                        {
//                            args.Add(varNames[i]);
//                        }
//                        else args.Add(variableVals[info.Name]);

//                    }
//                    headersToRet[i] = (VariableHeader)varAssoc.Constructor.Invoke(args.ToArray());
//                }
//            }

//            VariableProcessingCreation.methodValues.Add(headersToRet, methodVals);
//            VariableProcessingCreation.methods.Add(headersToRet, methodAssoc);
//            VariableProcessingCreation.sourceHeaders.Add(headersToRet, headers);

//            return headersToRet;
//        }

//        public static Variable[] CreateVariables()
//        {
//            Dictionary<VariableHeader[], object> objs = new Dictionary<VariableHeader[],object>();
//            Dictionary<VariableHeader[], IDataPool[]> pools = new Dictionary<VariableHeader[],IDataPool[]>();
//            Dictionary<VariableHeader[], Variable[]> sourceVars = new Dictionary<VariableHeader[],Variable[]>();
//            List<Variable> allVars = new List<Variable>();


//            foreach (VariableHeader[] createdHeaders in VariableProcessingCreation.methods.Keys)
//            {
//                MethodInfo info = VariableProcessingCreation.methods[createdHeaders].Method;
//                objs.Add(createdHeaders, info.Invoke(null, VariableProcessingCreation.methodValues[createdHeaders].ToArray()));

//                IDataPool[][] toPassPools = new IDataPool[VariableProcessingCreation.sourceHeaders[createdHeaders].Length][];
//                for (int i = 0; i < VariableProcessingCreation.sourceHeaders[createdHeaders].Length; i++)
//                {
//                    List<Variable> createdVars = new List<Variable>();
//                    VariableHeader[] tempHeaders = VariableProcessingCreation.sourceHeaders[createdHeaders][i];

//                    toPassPools[i] = new IDataPool[tempHeaders.Length];
//                    for (int j = 0; j < tempHeaders.Length; j++)
//                    {
//                        Variable var = VariableSource.GetVariable(tempHeaders[j]);
//                        if (!allVars.Contains(var)) allVars.Add(var);
//                        createdVars.Add(var);
//                        toPassPools[i][j] = (var[0] as DataFrame).Pool;
                        
//                    }
//                    sourceVars[createdHeaders] = createdVars.ToArray();
//                }

//                if (VariableProcessingCreation.methods[createdHeaders].Attribute is EditorFunctionAttribute)
//                {
//                    IDataPool[] createdPools = new IDataPool[createdHeaders.Length];
//                    MethodInfo dimLensF =
//                        VariableProcessingCreation.methods[createdHeaders].HelperMethods[(VariableProcessingCreation.methods[createdHeaders].Attribute as EditorFunctionAttribute).GetLengthFunction];
//                    int[][] dimeLens = (int[][])dimLensF.Invoke(null, new object[] { objs[createdHeaders], toPassPools });

//                    for (int i = 0; i < createdPools.Length; i++)
//                    {
//                        createdPools[i] = new CeresBase.Data.DataPools.DataPoolNetCDF(dimeLens[i]);
//                    }

//                    pools.Add(createdHeaders, createdPools);
//                }


//            }

//            Dictionary<Variable, float[]> buffers = new Dictionary<Variable, float[]>();
//            Dictionary<Variable, int[]> dimLens = new Dictionary<Variable,int[]>();
//            int[] bufferSize = new int[]{256000};
//            int[] curIndex = new int[]{0};
//            bool[] done = new bool[allVars.Count];

//            while (true)
//            {
//                for(int i =0 ; i < done.Length; i++)
//                {
//                    Variable var = allVars[0];

//                    buffers[var] = null;
//                    if (done[i]) continue;

//                    List<float> allData = new List<float>();
//                    (var[0] as DataFrame).Pool.GetData(curIndex, bufferSize,
//                        delegate(float[] data, uint[] indices, uint[] lengths)
//                        {
//                            allData.AddRange(data);
//                        }
//                    );
//                    if (allData.Count != bufferSize[0])
//                    {
//                        done[i] = true;
//                    }
//                    buffers[var] = allData.ToArray();
//                }

//                foreach (VariableHeader[] createdHeaders in VariableProcessingCreation.methods.Keys)
//                {
//                    List<float[]> dataToPass = new List<float[]>();
//                    foreach (Variable var in sourceVars[createdHeaders])
//                    {
//                        dataToPass.Add(buffers[var]);
//                    }

//                    MethodInfo callback = VariableProcessingCreation.methods[createdHeaders].HelperMethods[VariableProcessingCreation.methods[createdHeaders].Attribute.Callback];
//                    callback.Invoke(null, new object[] { pools[createdHeaders], dataToPass.ToArray(), curIndex, bufferSize, objs[createdHeaders] });
//                }

//                curIndex[0] += bufferSize[0];

//                bool stop = true;
//                foreach (bool test in done)
//                {
//                    if (!test) stop = false;
//                }
//                if (stop) break;
//            }

//            List<Variable> toRet = new List<Variable>();
//            foreach (VariableHeader[] headers in pools.Keys)
//            {
//                for (int i = 0; i < headers.Length; i++)
//                {
//                    Variable var = CeresBase.IO.IOFactory.CreateVariable(headers[i]);
//                    DataFrame frame = new DataFrame(var, pools[headers][i],
//                        new CeresBase.Data.DataShapes.NoShape(), General.Constants.Timeless);
//                    var.AddDataFrame(frame);
//                    toRet.Add(var);
//                }
//            }

//            methods.Clear();
//            methodValues.Clear();
//            sourceHeaders.Clear();

//            return toRet.ToArray();
//        }
//    }
//}
