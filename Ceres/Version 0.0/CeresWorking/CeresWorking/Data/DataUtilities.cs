using System;
using System.Collections.Generic;
using System.Text;
using ThreadSafeConstructs.Memory;
using System.Reflection;
using System.Collections;
using CeresBase.Development;
using System.IO;

namespace CeresBase.Data
{
    public delegate void TransformFunctionCallback(IDataPool pool, float[] data, int[] poolDimLengths,
            uint[] indices, uint[] counts, object tag);
    /// <summary>
    /// This static class provides helpful functions when dealing with data manipulation across all aspects
    /// of the program.
    /// </summary>
    [CeresCreated("Mikkel", "03/13/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    public static class DataUtilities
    {
        public static byte[] ArrayToByteArray(IList toConvert, int amount)
        {
            if (toConvert is byte[]) return (byte[])toConvert;
            int timesAmount = 4;
            if (toConvert is short[]) timesAmount = 2;
            else if (toConvert is double[] || toConvert is long[]) timesAmount = 8;
            byte[] toRet = new byte[amount * timesAmount];
            unsafe
            {
                #region All the different conversions
                fixed (byte* bPtr = toRet)
                {
                    if (toConvert is short[])
                    {
                        fixed (short* sPtr = (short[])toConvert)
                        {
                            Memory.Copy(sPtr, bPtr, toRet.Length);
                        }
                    }
                    else if (toConvert is int[])
                    {
                        fixed (int* sPtr = (int[])toConvert)
                        {
                            Memory.Copy(sPtr, bPtr, toRet.Length);
                        }
                    }
                    else if (toConvert is float[])
                    {
                        fixed (float* sPtr = (float[])toConvert)
                        {
                            Memory.Copy(sPtr, bPtr, toRet.Length);
                        }
                    }
                    else if (toConvert is double[])
                    {
                        fixed (double* sPtr = (double[])toConvert)
                        {
                            Memory.Copy(sPtr, bPtr, toRet.Length);
                        }
                    }
                    else if (toConvert is long[])
                    {
                        fixed (long* sPtr = (long[])toConvert)
                        {
                            Memory.Copy(sPtr, bPtr, toRet.Length);
                        }
                    }
                    else throw new Exception("Doesn't support this type."); //TODO CUSTOM EXCEPTION
                }
                #endregion
            }
            return toRet;
        }

        /// <summary>
        /// Converts an array to a byte array using direct memory copy. This is the
        /// "safe" way to do the fastest conversion possible
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public static byte[] ArrayToByteArray(IList toConvert)
        {
            return ArrayToByteArray(toConvert, toConvert.Count);
        }

        public static MemoryStream ArrayToMemoryStream(IList array)
        {
            byte[] data = ArrayToByteArray(array);
            return new MemoryStream(data);
        }

        /// <summary>
        /// Converts a byte array to array of Type ttype. This is the
        /// "safe" way to do the fastest conversion possible.
        /// </summary>
        /// <param name="array">IList to convert</param>
        /// <param name="ttype">Target conversion type</param>
        /// <returns>IList of type ttype</returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static IList ByteArrayToArray(byte[] array, Type ttype)
        {
            IList toRet = null;
            if (ttype == typeof(byte)) toRet = array;

            unsafe
            {
                #region PerType
                fixed (byte* bPtr = array)
                {
                    if (ttype == typeof(short))
                    {
                        toRet = new short[array.Length / 2];
                        fixed (short* sPtr = (short[])toRet)
                        {
                            Memory.Copy(bPtr, sPtr, array.Length);
                        }
                    }
                    else if (ttype == typeof(ushort))
                    {
                        toRet = new ushort[array.Length / 2];
                        fixed (ushort* sPtr = (ushort[])toRet)
                        {
                            Memory.Copy(bPtr, sPtr, array.Length);
                        }
                    }
                    else if (ttype == typeof(int))
                    {
                        toRet = new int[array.Length / 4];
                        fixed (int* sPtr = (int[])toRet)
                        {
                            Memory.Copy(bPtr, sPtr, array.Length);
                        }
                    }
                    else if (ttype == typeof(float))
                    {
                        toRet = new float[array.Length / 4];
                        fixed (float* sPtr = (float[])toRet)
                        {
                            Memory.Copy(bPtr, sPtr, array.Length);
                        }
                    }
                    else if (ttype == typeof(double))
                    {
                        toRet = new double[array.Length / 8];
                        fixed (double* sPtr = (double[])toRet)
                        {
                            Memory.Copy(bPtr, sPtr, array.Length);
                        }
                    }
                    else if (ttype == typeof(long))
                    {
                        toRet = new long[array.Length / 8];
                        fixed (long* sPtr = (long[])toRet)
                        {
                            Memory.Copy(bPtr, sPtr, array.Length);
                        }
                    }
                    else if (ttype != typeof(byte)) throw new Exception("Type not supported"); //TODO CUSTOM EXCEPTION
                }
                #endregion
            }
            return toRet;
        }

        /// <summary>
        /// Generic way to convert a byte array to array of Type ttype. This is the
        /// "safe" way to do the fastest conversion possible.
        /// </summary>
        /// <typeparam name="T">Type to convert to</typeparam>
        /// <param name="array">Data to convert</param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static T[] ByteArrayToArray<T>(byte[] array)
        {
            Type ttype = typeof(T);
            IList toRet = DataUtilities.ByteArrayToArray(array, ttype);
            return (T[])toRet;
        }

        /// <summary>
        /// Create an array of the given type with 'amount' elements.
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static IList CreateArray(int amount, Type type)
        {
            return (IList)type.MakeArrayType().InvokeMember("", BindingFlags.CreateInstance, null, null, new object[] { amount });
        }

        public static T[] CreateArray<T>(int amount)
        {
            return (T[])typeof(T).MakeArrayType().InvokeMember("", BindingFlags.CreateInstance, null, null, new object[] { amount });
        }

        /// <summary>
        /// Get the amount of data in each dimension given two data "corners" and a stride for each
        /// dimension. Used to calculate the amount of data in a subsampled routine.
        /// </summary>
        /// <param name="firstIndices">The first data "corner"</param>
        /// <param name="lastIndices">The second data "corner"</param>
        /// <param name="strides">The stride for each dimension</param>
        /// <returns>Count of each subsampled dimension</returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static int[] GetLengthsFromIndices(int[] firstIndices, int[] lastIndices, int[] strides)
        {
            int[] toRet = new int[firstIndices.Length];
            for (int i = 0; i < toRet.Length; i++)
            {
                double dval = (double)(lastIndices[i] - firstIndices[i]) / (double)strides[i];
                toRet[i] = (int)(Math.Ceiling(dval) + 1);
            }
            return toRet;
        }

        /// <summary>
        /// This simply takes an array of index location and converts them to the equivalent int
        /// cast down (floored). This is useful when doing interpolations.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static int[] CastDown(double[] array)
        {
            bool throwAway;
            return CastDown(array, out throwAway);
        }
        /// <summary>
        /// This simply takes an array of index location and converts them to the equivalent int
        /// cast up (ceilinged). This is useful when doing interpolations.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="isSame">Is the input array all integers? This can be used to say that 
        /// an interpolation isn't necessary. </param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static int[] CastDown(double[] array, out bool isSame)
        {
            int[] castDown = new int[array.Length];
            isSame = true;
            for (int i = 0; i < array.Length; i++)
            {
                castDown[i] = (int)array[i];
                if (castDown[i] != array[i]) isSame = false;
            }
            return castDown;
        }

        /// <summary>
        /// This simply takes an array of index location and converts them to the equivalent int
        /// cast up (ceilinged). This is useful when doing interpolations.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static int[] CastUp(double[] array)
        {
            bool throwAway;
            return CastUp(array, out throwAway);
        }

        /// <summary>
        /// This simply takes an array of index location and converts them to the equivalent int
        /// cast up (ceilinged). This is useful when doing interpolations.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="isSame">Is the input array all integers? This can be used to say that 
        /// an interpolation isn't necessary. </param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static int[] CastUp(double[] array, out bool isSame)
        {
            int[] castUp = new int[array.Length];
            isSame = true;
            for (int i = 0; i < array.Length; i++)
            {
                castUp[i] = (int)Math.Ceiling(array[i]);
                if (castUp[i] != array[i]) isSame = false;
            }
            return castUp;
        }

        [CeresCreated("Mikkel", "03/20/06")]
        public delegate void PoolReadStridedDelegate(float[] data, uint[] firstIndices, uint[] lastIndices, uint[] strides);

        public delegate void PoolReadDelegate(float[] data, uint[] indices, uint[] counts);
    }
}
