//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Reflection;
//using CeresBase.UI.MethodEditor;
//using CeresBase.Settings;
//using Razor.Configuration;

//namespace CeresBase.Data
//{

//    [CeresBase.Settings.XMLSetting("VariableCreation", "", true, false)]
//    public static class VariableProcessingSetup
//    {
//        private static List<ProcessingAssociation> associations = new List<ProcessingAssociation>();
//        private static XMLSettingAttribute attr;

//        static VariableProcessingSetup()
//        {
//            VariableProcessingSetup.attr = CeresBase.General.Utilities.GetAttribute<XMLSettingAttribute>(typeof(VariableProcessingSetup), false);
//            foreach (XmlConfigurationCategory assoc in VariableProcessingSetup.attr.Category.Categories)
//            {
//                //string toRet = constructor.DeclaringType.FullName + "," + constructor.ToString() + "|"
//                //    + method.DeclaringType.FullName + "." + method.Name + "|" + 

//                string[] parse = assoc.DisplayName.Split('|');

//                #region Figure out Constructor

//                int index = parse[0].IndexOf(',');
//                string className = parse[0].Substring(0, index);
//                parse[0] = parse[0].Substring(index + 1);
//                Type classType = CeresBase.IO.IOFactory.CreateType(className);
//                ConstructorInfo[] constructors = classType.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
//                ConstructorInfo theConstructor;
//                List<string> paramNames = new List<string>();
//                while ((index = parse[0].IndexOf(',')) > 0)
//                {
//                    paramNames.Add(parse[0].Substring(0, index));
//                    parse[0] = parse[0].Substring(index + 1);
//                }

//                foreach (ConstructorInfo cons in constructors)
//                {
//                    bool found = true;
//                    ParameterInfo[] paras = cons.GetParameters();
//                    if (paras.Length != paramNames.Count)
//                        continue;
                    
//                    foreach (ParameterInfo para in paras)
//                    {
//                        if (!paramNames.Contains(para.ParameterType.FullName))
//                        {
//                            found = false;
//                            break;
//                        }
//                    }

//                    if (found)
//                    {
//                        theConstructor = cons;
//                        break;
//                    }
//                }

//                #endregion

//                int methodIndex = parse[1].LastIndexOf('.');
//                string methodTypeStr = parse[1].Substring(0, methodIndex);
//                Type methodType = CeresBase.IO.IOFactory.CreateType(methodTypeStr);
//                MethodInfo method = methodType.GetMethod(parse[1].Substring(methodIndex + 1));

//                bool methodToVar = bool.Parse(parse[2]);

//                string[] typesStrings = parse[3].Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);



//                //ProcessingAssociation assoc = new ProcessingAssociation(methodToVar, theConstructor, method, null);



//            }
//        }

//        public class ProcessingAssociation
//        {
//            private Type[][] types;
//            public Type[][] VarTypes
//            {
//                get { return types; }
//            }

//            private MethodInfo method;
//            public MethodInfo Method
//            {
//                get { return method; }
//            }

//            private Dictionary<string, MethodInfo> helperMethods =
//                new Dictionary<string,MethodInfo>();
//            public Dictionary<string, MethodInfo> HelperMethods
//            {
//                get { return helperMethods; }
//            }


//            private bool methodToVar;
//            public bool MethodToVar
//            {
//                get { return methodToVar; }
//            }

//            private EditorInfoAttribute attr;
//            public EditorInfoAttribute Attribute
//            {
//                get { return attr; }
//            }

//            private ConstructorInfo constructor;
//            public ConstructorInfo Constructor
//            {
//                get { return constructor; }
//            }


//            public override int GetHashCode()
//            {
//                return (constructor.GetHashCode().ToString() + methodToVar.ToString() + 
//                    method.GetHashCode().ToString() + types.GetHashCode().ToString()).GetHashCode();
//            }

//            public override string ToString()
//            {
//                string toRet = constructor.DeclaringType.FullName + ",";
//                foreach (ParameterInfo para in constructor.GetParameters())
//                {
//                    toRet += para.ParameterType.FullName + ",";
//                }
                
//                toRet += "|" + method.DeclaringType.FullName + "." + method.Name + "|" + 
//                    methodToVar.ToString() + "|";
//                foreach (Type[] typeA in this.types)
//                {
//                    toRet += "&&";
//                    foreach (Type type in typeA)
//                    {
//                        toRet += type.FullName + "&";
//                    }
//                }
//                return toRet;
//            }


//            private Dictionary<ParameterInfo, ProcessingAssocationParameter> parameters = 
//                new Dictionary<ParameterInfo,ProcessingAssocationParameter>();
//            public Dictionary<ParameterInfo, ProcessingAssocationParameter> Parameters
//            {
//                get
//                {
//                    return this.parameters;
//                }
//            }

//            public ProcessingAssocationParameter this[string name]
//            {
//                get
//                {
//                    foreach (ParameterInfo info in this.parameters.Keys)
//                    {
//                        if (info.Name == name) return this.parameters[info];
//                    }
//                    return null;
//                }
//            }

//            public ProcessingAssociation(bool methodToVar, ConstructorInfo constructor, MethodInfo function, Type[][] types)
//            {
//                this.method = function;
//                this.methodToVar = methodToVar;
//                this.types = types;
//                this.constructor = constructor;

//                object[] attrs = function.GetCustomAttributes(typeof(EditorInfoAttribute), false);
//                if (attrs.Length != 0)
//                {
//                    this.attr = (EditorInfoAttribute)attrs[0];
//                    PropertyInfo[] props = this.attr.GetType().GetProperties();
//                    foreach (PropertyInfo prop in props)
//                    {
//                        if (prop.PropertyType == typeof(string))
//                        {
//                            string funcName = (string)prop.GetValue(this.attr, null);
//                            MethodInfo helper = function.ReflectedType.GetMethod(funcName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
//                            if (helper == null) continue;
//                            this.helperMethods.Add(helper.Name, helper);
//                        }
//                    }
//                }
//            }
	
//        }

//        public class ProcessingAssocationParameter
//        {
//            private bool readOnly;
//            public bool ReadOnly
//            {
//                get { return readOnly; }
//                set { this.readOnly = value;}
//            }

//            private object value;
//            public object Value
//            {
//                get { return value; }
//                set { this.value = value; }
//            }

//            private int fromCollection;
//            public int FromCollection
//            {
//                get { return fromCollection; }
//                set { fromCollection = value; }
//            }
	

//            public ProcessingAssocationParameter(bool readOnly, object value)
//            {
//                this.readOnly = readOnly;
//                this.value = value;
//            }
//        }

//        public static ProcessingAssociation GetAssociation(MethodInfo info, ConstructorInfo constructor, Type[][] varTypes, bool methodToVar)
//        {
//            ProcessingAssociation assocation = VariableProcessingSetup.associations.Find(
//               delegate(ProcessingAssociation a)
//               {
//                   return a.Method == info && varTypes == a.VarTypes && constructor == a.Constructor && methodToVar == a.MethodToVar;
//               }
//           );

//            return assocation;
//        }

//        public static ProcessingAssociation AddAssociation(MethodInfo info, ConstructorInfo constructor, Type[][] varTypes, bool methodToVar)
//        {
//            ProcessingAssociation association = new ProcessingAssociation(methodToVar, constructor, info, varTypes);
//            VariableProcessingSetup.associations.Add(association);

//            ParameterInfo[] pInfos = null;
//            if (!methodToVar) pInfos = info.GetParameters();
//            else pInfos = constructor.GetParameters();
//            foreach (ParameterInfo pInfo in pInfos)
//            {
//                association.Parameters.Add(pInfo, new ProcessingAssocationParameter(false,
//                    CeresBase.General.Utilities.GetDefaultObject(pInfo.ParameterType)));
//            }

//            return association;
//        }

//        public static void RemoveAssociation(MethodInfo info, ConstructorInfo constructor, Type[][] varTypes, bool methodToVar)
//        {
//            ProcessingAssociation assocation = VariableProcessingSetup.associations.Find(
//                delegate(ProcessingAssociation a)
//                {
//                    return a.Method == info && varTypes == a.VarTypes && constructor == a.Constructor && methodToVar == a.MethodToVar;                    
//                }
//            );

//            if (assocation == null) return;
//            VariableProcessingSetup.associations.Remove(assocation);
//        }

//        public static void WriteAssociations()
//        {
//            foreach (ProcessingAssociation assoc in VariableProcessingSetup.associations)
//            {
//                XmlConfigurationCategory cat = attr.Category.Categories[assoc.ToString(), true];
//                foreach (ParameterInfo paramInfo in assoc.Parameters.Keys)
//                {
//                    XmlConfigurationCategory para = cat.Categories[paramInfo.Name, true];
//                    ProcessingAssocationParameter proc = assoc.Parameters[paramInfo];
                 
//                    XmlConfigurationOption readOnly = para.Options["ReadOnly", true];
//                    readOnly.Value = proc.ReadOnly;

//                    XmlConfigurationOption value = para.Options["Value", true];
//                    value.Value = proc.Value;

//                    XmlConfigurationOption fromCollection = para.Options["FromCollection", true];
//                    fromCollection.Value = proc.FromCollection;
//                }
//            }
//        }
//    }
//}
