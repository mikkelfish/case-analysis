//using System;
//using System.Collections.Generic;
//using System.Text;
//using CeresBase.Development;

//namespace CeresBase.Data.Interpolations
//{
//    /// <summary>
//    /// This is an N-Dimensional linear interpolation implementation.
//    /// </summary>
//    [CeresCreated("Mikkel", "03/13/06")]
//    [CeresToDo("Mikkel", "03/13/06", Comments = "Decide whether this should be a corner to corner or nearest neighbor interpolation.",
//        Priority=DevelopmentPriority.Critical)]
//    public class LinearPointInterpolation : IPointInterpolation
//    {
//        [CeresCreated("Mikkel", "03/13/06")]
//        public float GetAndInterpolate(object location, IDataPool pool, IDataShape dataShape)
//        {
//            double[] grid = dataShape.ShapeToGrid(location);
//            bool noNeedToInterpolate;
//            int[] castDown = DataUtilities.CastDown(grid, out noNeedToInterpolate);
//            if (!noNeedToInterpolate)
//            {
//                int[] castUp = DataUtilities.CastUp(grid);
//                double val = pool.GetDataValue(castDown);
//                double val2 = pool.GetDataValue(castUp);

//                double perc = 0;
//                foreach (double gridVal in grid)
//                {
//                    perc += (gridVal - (int)gridVal) * (gridVal - (int)gridVal);
//                }
//                perc = Math.Sqrt(perc);

//                return (1.0 - perc) * val + perc * val2;
//            }
//            return pool.GetDataValue(castDown);
//        }


//        [CeresCreated("Mikkel", "03/13/06")]
//        [CeresToDo("Mikkel", "03/13/06", Comments = "Should probably look into situations where this could be optimized",
//            Priority=DevelopmentPriority.Low)]
//        public float[] GetAndInterpolateArray(object[] locations, IDataPool pool, IDataShape shape)
//        {
//            double[] interpData = new double[locations.Length];
//            for (int i = 0; i < interpData.Length; i++)
//            {
                
//            }
//            return interpData;
//        }
//    }
//}
