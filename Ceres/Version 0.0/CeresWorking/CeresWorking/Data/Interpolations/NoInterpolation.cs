﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.Data.Interpolations
{
    public class NoInterpolation : IPointInterpolation
    {
        #region IPointInterpolation Members

        public float GetAndInterpolate(object location, IDataPool pool, IDataShape shape)
        {
            double[] grid = shape.ShapeToGrid(location);
            int[] intcoords = new int[grid.Length];
            for (int i = 0; i < grid.Length; i++)
            {
                intcoords[i] = (int)grid[i];
            }
            return pool.GetDataValue(intcoords);
        }

        public float[] GetAndInterpolateArray(object[] locations, IDataPool pool, IDataShape shape)
        {
            List<float> toRet = new List<float>();
            foreach (object loc in locations)
            {
                toRet.Add(this.GetAndInterpolate(loc, pool, shape));
            }

            return toRet.ToArray();
        }

        #endregion

        #region IPointInterpolation Members


        public float[] GetContiguousData(object start, object end, IDataPool pool, IDataShape shape)
        {
            double[] gridStart = shape.ShapeToGrid(start);
            double[] gridEnd = shape.ShapeToGrid(end);

            List<float> toRet = new List<float>();
            int[] intcoords = gridStart.Select(d => (int)d).ToArray();
            int[] lengths = intcoords.Select((coord, index) => (int)gridEnd[index] - coord).ToArray();

            pool.GetData(intcoords, lengths,
                delegate(float[] data, uint[] starts, uint[] length)
                {
                    toRet.AddRange(data);
                }
            );

            return toRet.ToArray();
        }

        #endregion
    }
}
