using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;
using ThreadSafeConstructs;
using CeresBase.UI;
using System.Linq;
using CeresBase.IO.Serialization;
using System.Collections.ObjectModel;
using MesosoftCommon.Threading;
using MesosoftCommon.DataStructures;
using System.Windows.Threading;
using MesosoftCommon.ExtensionMethods;

namespace CeresBase.Data
{
    /// <summary>
    /// This static class is the underlying source for accessing all variables. When a context is created
    /// it must get all the variables that will be in the context from the source. 
    /// </summary>
    [CeresCreated("Mikkel", "03/14/06")]
    public static class VariableSource
    {
        static VariableSource()
        {

            NewSerializationCentral.DatabaseChanged += new EventHandler(SerializationCentral_DatabaseChanged);
            ThreadSafeBinding.RegisterCollection<Variable>(variables, "VariableSource");




        }

        static void SerializationCentral_DatabaseChanged(object sender, EventArgs e)
        {
            Variable[] allVars = variables.ToArray();
            foreach (Variable var in allVars)
            {
                RemoveVariable(var);
            }
        }

        #region Private Variables
        private static ObservableCollection<Variable> variables = new ObservableCollection<Variable>();
       
        
        private static ThreadSafeListLock<VariableHeader> queuedHeaders = new ThreadSafeListLock<VariableHeader>();
        private static ObservableCollection<VariableHeader> bindableList = new ObservableCollection<VariableHeader>();
        #endregion

        #region Private Functions

        #endregion

        #region Properties
        //public static BindableList<VariableHeader> AllVariablesKnown
        //{
        //    get
        //    {
        //        return VariableSource.bindableList;
        //    }
        //}

        /// <summary>
        /// Get all the variables currently stored in the source
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public static BeginInvokeObservableCollection<Variable> Variables
        {
            get
            {
                return ThreadSafeBinding.BindToManager<Variable>(Dispatcher.CurrentDispatcher, "VariableSource");
            }
        }

        public static BeginInvokeObservableCollection<Variable> GetThreadSafeVariables(Dispatcher dispatch)
        {
            return ThreadSafeBinding.BindToManager<Variable>(dispatch, "VariableSource");
        }



        //public static BindableList<Variable> VariableList
        //{
        //    get
        //    {
        //        BindableList<Variable> vars = new BindableList<Variable>();
        //        foreach (Variable var in VariableSource.variables)
        //        {
        //            vars.Add(var);
        //        }
        //        return vars;
        //    }
        //}

        //public static VariableHeader[] QueuedHeaders
        //{
        //    get
        //    {
        //        return VariableSource.queuedHeaders.ToArray();
        //    }
        //}
        #endregion

        #region Constructors

        #endregion

        #region Public Functions

        public static void Quitting()
        {
            foreach (Variable var in VariableSource.variables)
            {
                var.Dispose();
            }
        }

        /// <summary>
        /// Add a variable to the variable source. After it is added, then all elements of the program 
        /// can share it
        /// </summary>
        /// <param name="variable"></param>
        [CeresCreated("Mikkel", "03/14/06")]
        public static void AddVariable(Variable variable)
        {
            if (variable == null) return;
            VariableHeader header = VariableSource.queuedHeaders.Find(
                delegate(VariableHeader compare)
                {
                    return variable.Header.ExperimentName == compare.ExperimentName &&
                        variable.Header.VarName == compare.VarName;
                }
                );
            if (header != null)
            {
                VariableSource.queuedHeaders.Remove(header);
                VariableSource.bindableList.Remove(header);
            }
            VariableSource.variables.Add(variable);
            VariableSource.bindableList.Add(variable.Header);

        }

        public static void AddVariableHeaderToMake(VariableHeader header)
        {
            VariableSource.queuedHeaders.Add(header);
            VariableSource.bindableList.Add(header);
        }

        /// <summary>
        /// Does the variable source contain a variable with the given string name?
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public static bool ContainsVariable(string name, string exptName)
        {
            return VariableSource.variables.Any(
                delegate(Variable var)
                {
                    return var.Header.VarName == name  && var.Header.ExperimentName == exptName;
                });
        }

        /// <summary>
        /// Does the variable source contain the given variable
        /// </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public static bool ContainsVariable(Variable variable)
        {
            return VariableSource.variables.Contains(variable);
        }

        /// <summary>
        /// Does it contain a variable with the given header?
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/20/06")]
        public static bool ContainsVariable(VariableHeader header)
        {
            return ContainsVariable(header.VarName, header.ExperimentName);
        }

        /// <summary>
        /// Retrive a variable with the given name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public static Variable GetVariable(string name, string units, string exptName)
        {
            return VariableSource.variables.FirstOrDefault(
                delegate(Variable var)
                {
                    return var.Header.VarName == name && var.Header.ExperimentName == exptName;
                });
        }

        /// <summary>
        /// Retrieve the variable with the given header
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/20/06")]
        public static Variable GetVariable(VariableHeader header)
        {
            return GetVariable(header.VarName, header.UnitsName, header.ExperimentName);
        }

        public static void RemoveVariable(VariableHeader header)
        {
            Variable var = VariableSource.variables.FirstOrDefault(v => v.Header == header);

            if (var == null) return;
            VariableSource.variables.Remove(var);
            VariableSource.bindableList.Remove(header);
            CeresBase.Projects.EpochCentral.VariableRemoved(var);

        }

        public static void RemoveVariable(Variable var)
        {
            VariableSource.variables.Remove(var);
            VariableSource.bindableList.Remove(var.Header);
            CeresBase.Projects.EpochCentral.VariableRemoved(var);

        }

        public static void RemoveVariable(string experiment, string name)
        {
            Variable var = variables.FirstOrDefault(d => d.Header.ExperimentName == experiment && d.Header.VarName == name);
            if (var != null)
            {
                VariableSource.variables.Remove(var);
                VariableSource.bindableList.Remove(var.Header);

                CeresBase.Projects.EpochCentral.VariableRemoved(var);
            }
        }
        
        
        #endregion

        public static Variable[] GetAllVariablesInExperiment(string expt)
        {
            return Variables.Where(v => v.Header.ExperimentName == expt).ToArray();
        }
       
        
        public static void AddVariables(Variable[] vars)
        {
            foreach (Variable var in vars)
            {
                VariableHeader header = VariableSource.queuedHeaders.Find(
              delegate(VariableHeader compare)
              {
                  return var.Header.ExperimentName == compare.ExperimentName &&
                      var.Header.VarName == compare.VarName;
              }
              );
                if (header != null)
                {
                    VariableSource.queuedHeaders.Remove(header);
                    VariableSource.bindableList.Remove(header);
                }
                VariableSource.bindableList.Add(var.Header);
            }

            VariableSource.variables.AddRange(vars);

        }

        public static void AddVariableHeadersToMake(VariableHeader[] varHeaders)
        {
            foreach (VariableHeader header in varHeaders)
            {
                AddVariableHeaderToMake(header);
            }
        }

        public static void RemoveQueuedHeader(VariableHeader header)
        {
            VariableSource.queuedHeaders.Remove(header);
            VariableSource.bindableList.Remove(header);
        }
    }
}
