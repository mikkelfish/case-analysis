using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace CeresBase.Data.Compressions
{
    //This is just a bunch of ugly unsafe code for each combination to make sure that the 
    //compression/decompressions are running as fast as possible.
    public partial class SlopeCompression : ICompression
    {
        #region Decompression Info
        private struct decompressionInfo
        {
            public int[] iLengths;
            public int[] ifIndices;
            public int[] ilIndices;
            public int[] iStrides;
            public int iAmount;

            public int numStorageLevs;
            public int curLevel;
            public int lastLevel;
            public int levStride;

            public CeresBase.Data.DataIterator iterator;
        }

        private decompressionInfo getDecompressionInfo(int[] firstIndices, int[] lastIndices, int[] strides)
        {
            int numDims = lastIndices.Length;
            decompressionInfo info = new decompressionInfo();

            if (numDims <= 2)
            {
                info.iLengths = this.lengths;
                info.ifIndices = firstIndices;
                info.ilIndices = lastIndices;
                info.iStrides = strides;
                info.numStorageLevs = 1;
                info.curLevel = 0;
                info.levStride = 1;
                info.lastLevel = 1;
                info.iAmount = 0;
            }
            else
            {
                info.iLengths = new int[numDims - 1];
                info.ifIndices = new int[numDims - 1];
                info.ilIndices = new int[numDims - 1];
                info.iStrides = new int[numDims - 1];

                info.iAmount = 1;
                for (int i = 0; i < numDims - 1; i++)
                {
                    info.iLengths[i] = this.lengths[i];
                    info.ifIndices[i] = firstIndices[i];
                    info.ilIndices[i] = lastIndices[i];
                    info.iStrides[i] = strides[i];
                    info.iAmount *= this.lengths[i];
                }

                int[] numStor = CeresBase.Data.DataUtilities.GetLengthsFromIndices(
                    new int[] { firstIndices[numDims - 1] }, new int[] { lastIndices[numDims - 1] }, new int[] { strides[numDims - 1] });
                info.numStorageLevs = numStor[0];
                info.curLevel = firstIndices[numDims - 1];
                info.levStride = strides[numDims - 1];
                info.lastLevel = lastIndices[numDims - 1];
            }

            info.iterator = new DataIterator(info.iLengths, info.ifIndices, info.ilIndices, info.iStrides);

            return info;
        }

        #endregion

        #region Min/Max Helpers
        private void getMinMaxDouble(IList data, int level, int count, out double min, out double max)
        {
            min = double.MaxValue;
            max = double.MinValue;
            double missing = DataConstants.GetMissingValue<double>();
            unsafe
            {
                fixed (double* ptr = (double[])data)
                {
                    double* cur = ptr + level * count;
                    for (int i = 0; i < count; i++, cur++)
                    {
                        if (*cur == missing) continue;
                        if (*cur > max) max = *cur;
                        if (*cur < min) min = *cur;
                    }
                }
            }
        }
        private void getMinMaxLong(IList data, int level, int count, out double min, out double max)
        {
            min = double.MaxValue;
            max = double.MinValue;
            long missing = DataConstants.GetMissingValue<long>();
            unsafe
            {
                fixed (long* ptr = (long[])data)
                {
                    long* cur = ptr + level * count;
                    for (int i = 0; i < count; i++, cur++)
                    {
                        if (*cur == missing) continue;                    
                        if (*cur > max) max = *cur;
                        if (*cur < min) min = *cur;
                    }
                }
            }
        }
        private void getMinMaxInt(IList data, int level, int count, out double min, out double max)
        {
            min = double.MaxValue;
            max = double.MinValue;
            int missing = DataConstants.GetMissingValue<int>();
            unsafe
            {
                fixed (int* ptr = (int[])data)
                {
                    int* cur = ptr + level * count;
                    for (int i = 0; i < count; i++, cur++)
                    {
                        if (*cur == missing) continue;
                        if (*cur > max) max = *cur;
                        if (*cur < min) min = *cur;
                    }
                }
            }
        }
        private void getMinMaxFloat(IList data, int level, int count, out double min, out double max)
        {
            min = double.MaxValue;
            max = double.MinValue;
            float missing = DataConstants.GetMissingValue<float>();
            unsafe
            {
                fixed (float* ptr = (float[])data)
                {
                    float* cur = ptr + level * count;
                    for (int i = 0; i < count; i++, cur++)
                    {
                        if (*cur == missing) continue;
                        if (*cur > max) max = *cur;
                        if (*cur < min) min = *cur;
                    }
                }
            }
        }
        private void getMinMaxShort(IList data, int level, int count, out double min, out double max)
        {
            min = double.MaxValue;
            max = double.MinValue;
            short missing = DataConstants.GetMissingValue<short>();
            unsafe
            {
                fixed (short* ptr = (short[])data)
                {
                    short* cur = ptr + level * count;
                    for (int i = 0; i < count; i++, cur++)
                    {
                        if (*cur == missing) continue;
                        if (*cur > max) max = *cur;
                        if (*cur < min) min = *cur;
                    }
                }
            }
        }
        #endregion

        #region Byte CalcIndices
        private void calcIndicesByteDouble(IList retData, IList data, int level, int count)
        {
            double dMissingVal = DataConstants.GetMissingValue<double>();
            byte bMissingVal = DataConstants.GetMissingValue<byte>();
            double yint = this.bs[level];
            double slope = this.ms[level];
            unsafe
            {
                fixed (double* ptr = (double[])data)
                {
                    fixed (byte* ret = (byte[])retData)
                    {
                        double* cur = ptr + level * count;
                        byte* curRet = ret + level * count;
                        for (int i = 0; i < count; i++, cur++, curRet++)
                        {
                            if (*cur == dMissingVal)
                            {
                                *curRet = bMissingVal;
                                continue;
                            }

                            *curRet = (byte)(Math.Round((*cur - yint) / slope, 0));
                        }
                    }
                }
            }
        }
        private void calcIndicesByteLong(IList retData, IList data, int level, int count)
        {
            long dMissingVal = DataConstants.GetMissingValue<long>();
            byte bMissingVal = DataConstants.GetMissingValue<byte>();
            double yint = this.bs[level];
            double slope = this.ms[level];
            unsafe
            {
                fixed (long* ptr = (long[])data)
                {
                    fixed (byte* ret = (byte[])retData)
                    {
                        long* cur = ptr + level * count;
                        byte* curRet = ret + level * count;
                        for (int i = 0; i < count; i++, cur++, curRet++)
                        {
                            if (*cur == dMissingVal)
                            {
                                *curRet = bMissingVal;
                                continue;
                            }

                            *curRet = (byte)(Math.Round((*cur - yint) / slope, 0));
                        }
                    }
                }
            }
        }
        private void calcIndicesByteInt(IList retData, IList data, int level, int count)
        {
            int dMissingVal = DataConstants.GetMissingValue<int>();
            byte bMissingVal = DataConstants.GetMissingValue<byte>();
            double yint = this.bs[level];
            double slope = this.ms[level];
            unsafe
            {
                fixed (int* ptr = (int[])data)
                {
                    fixed (byte* ret = (byte[])retData)
                    {
                        int* cur = ptr + level * count;
                        byte* curRet = ret + level * count;
                        for (int i = 0; i < count; i++, cur++, curRet++)
                        {
                            if (*cur == dMissingVal)
                            {
                                *curRet = bMissingVal;
                                continue;
                            }

                            *curRet = (byte)(Math.Round((*cur - yint) / slope, 0));
                        }
                    }
                }
            }
        }
        private void calcIndicesByteFloat(IList retData, IList data, int level, int count)
        {
            float dMissingVal = DataConstants.GetMissingValue<float>();
            byte bMissingVal = DataConstants.GetMissingValue<byte>();
            double yint = this.bs[level];
            double slope = this.ms[level];
            unsafe
            {
                fixed (float* ptr = (float[])data)
                {
                    fixed (byte* ret = (byte[])retData)
                    {
                        float* cur = ptr + level * count;
                        byte* curRet = ret + level * count;
                        for (int i = 0; i < count; i++, cur++, curRet++)
                        {
                            if (*cur == dMissingVal)
                            {
                                *curRet = bMissingVal;
                                continue;
                            }

                            *curRet = (byte)(Math.Round((*cur - yint) / slope, 0));
                        }
                    }
                }
            }
        }
        private void calcIndicesByteShort(IList retData, IList data, int level, int count)
        {
            short dMissingVal = DataConstants.GetMissingValue<short>();
            byte bMissingVal = DataConstants.GetMissingValue<byte>();
            double yint = this.bs[level];
            double slope = this.ms[level];
            unsafe
            {
                fixed (short* ptr = (short[])data)
                {
                    fixed (byte* ret = (byte[])retData)
                    {
                        short* cur = ptr + level * count;
                        byte* curRet = ret + level * count;
                        for (int i = 0; i < count; i++, cur++, curRet++)
                        {
                            if (*cur == dMissingVal)
                            {
                                *curRet = bMissingVal;
                                continue;
                            }

                            *curRet = (byte)(Math.Round((*cur - yint) / slope, 0));
                        }
                    }
                }
            }
        }
        #endregion

        #region Short CalcIndices
        private void calcIndicesShortDouble(IList retData, IList data, int level, int count)
        {
            double dMissingVal = DataConstants.GetMissingValue<double>();
            short bMissingVal = DataConstants.GetMissingValue<short>();
            double yint = this.bs[level];
            double slope = this.ms[level];
            unsafe
            {
                fixed (double* ptr = (double[])data)
                {
                    fixed (short* ret = (short[])retData)
                    {
                        double* cur = ptr + level * count;
                        short* curRet = ret + level * count;
                        for (int i = 0; i < count; i++, cur++, curRet++)
                        {
                            if (*cur == dMissingVal)
                            {
                                *curRet = bMissingVal;
                                continue;
                            }

                            *curRet = (short)(Math.Round((*cur - yint) / slope - short.MinValue, 0));
                        }
                    }
                }
            }
        }
        private void calcIndicesShortLong(IList retData, IList data, int level, int count)
        {
            long dMissingVal = DataConstants.GetMissingValue<long>();
            short bMissingVal = DataConstants.GetMissingValue<short>();
            double yint = this.bs[level];
            double slope = this.ms[level];
            unsafe
            {
                fixed (long* ptr = (long[])data)
                {
                    fixed (short* ret = (short[])retData)
                    {
                        long* cur = ptr + level * count;
                        short* curRet = ret + level * count;
                        for (int i = 0; i < count; i++, cur++, curRet++)
                        {
                            if (*cur == dMissingVal)
                            {
                                *curRet = bMissingVal;
                                continue;
                            }

                            *curRet = (short)(Math.Round((*cur - yint) / slope - short.MinValue, 0));
                        }
                    }
                }
            }
        }
        private void calcIndicesShortInt(IList retData, IList data, int level, int count)
        {
            int dMissingVal = DataConstants.GetMissingValue<int>();
            short bMissingVal = DataConstants.GetMissingValue<short>();
            double yint = this.bs[level];
            double slope = this.ms[level];
            unsafe
            {
                fixed (int* ptr = (int[])data)
                {
                    fixed (short* ret = (short[])retData)
                    {
                        int* cur = ptr + level * count;
                        short* curRet = ret + level * count;
                        for (int i = 0; i < count; i++, cur++, curRet++)
                        {
                            if (*cur == dMissingVal)
                            {
                                *curRet = bMissingVal;
                                continue;
                            }

                            *curRet = (short)(Math.Round((*cur - yint) / slope - short.MinValue, 0));
                        }
                    }
                }
            }
        }
        private void calcIndicesShortFloat(IList retData, IList data, int level, int count)
        {
            float dMissingVal = DataConstants.GetMissingValue<float>();
            short bMissingVal = DataConstants.GetMissingValue<short>();
            double yint = this.bs[level];
            double slope = this.ms[level];
            unsafe
            {
                fixed (float* ptr = (float[])data)
                {
                    fixed (short* ret = (short[])retData)
                    {
                        float* cur = ptr + level * count;
                        short* curRet = ret + level * count;
                        for (int i = 0; i < count; i++, cur++, curRet++)
                        {
                            if (*cur == dMissingVal)
                            {
                                *curRet = bMissingVal;
                                continue;
                            }

                            *curRet = (short)(Math.Round((*cur - yint) / slope - short.MinValue, 0));
                        }
                    }
                }
            }
        }
        #endregion

        #region Byte Decompress
        private void decompressByteDouble(IList toRet, IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            decompressionInfo info = this.getDecompressionInfo(firstIndices, lastIndices, strides);

            byte bMissingVal = DataConstants.GetMissingValue<byte>();
            double dMissingVal = DataConstants.GetMissingValue<double>();

            unsafe
            {
                fixed (double* rPtr = (double[])toRet)
                {
                    fixed (byte* ptr = (byte[])data)
                    {
                        double* curR = rPtr;
                        for (int i = 0; i < info.numStorageLevs; i++, info.curLevel += info.levStride)
                        {
                            if (info.curLevel > info.lastLevel) info.curLevel = info.lastLevel;

                            double m = this.ms[info.curLevel];
                            double b = this.bs[info.curLevel];

                            int levIndex = info.curLevel * info.iAmount;

                            info.iterator.Reset();
                            foreach (int offset in info.iterator.Enumerate)
                            {
                                byte* val = ptr + offset + levIndex;
                                if (*val == bMissingVal)
                                {
                                    *curR = dMissingVal;
                                    curR++;
                                    continue;
                                }
                                *curR = (double)(b + *val * m);
                                curR++;
                            }
                        }
                    }
                }
            }

        }
        private void decompressByteLong(IList toRet, IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            decompressionInfo info = this.getDecompressionInfo(firstIndices, lastIndices, strides);

            byte bMissingVal = DataConstants.GetMissingValue<byte>();
            long dMissingVal = DataConstants.GetMissingValue<long>();

            unsafe
            {
                fixed (long* rPtr = (long[])toRet)
                {
                    fixed (byte* ptr = (byte[])data)
                    {
                        long* curR = rPtr;
                        for (int i = 0; i < info.numStorageLevs; i++, info.curLevel += info.levStride)
                        {
                            if (info.curLevel > info.lastLevel) info.curLevel = info.lastLevel;

                            double m = this.ms[info.curLevel];
                            double b = this.bs[info.curLevel];

                            int levIndex = info.curLevel * info.iAmount;

                            info.iterator.Reset();
                            foreach (int offset in info.iterator.Enumerate)
                            {
                                byte* val = ptr + offset + levIndex;
                                if (*val == bMissingVal)
                                {
                                    *curR = dMissingVal;
                                    curR++;
                                    continue;
                                }
                                *curR = (long)(b + *val * m);
                                curR++;
                            }
                        }
                    }
                }
            }

        }
        private void decompressByteInt(IList toRet, IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            decompressionInfo info = this.getDecompressionInfo(firstIndices, lastIndices, strides);

            byte bMissingVal = DataConstants.GetMissingValue<byte>();
            int dMissingVal = DataConstants.GetMissingValue<int>();

            unsafe
            {
                fixed (int* rPtr = (int[])toRet)
                {
                    fixed (byte* ptr = (byte[])data)
                    {
                        int* curR = rPtr;
                        for (int i = 0; i < info.numStorageLevs; i++, info.curLevel += info.levStride)
                        {
                            if (info.curLevel > info.lastLevel) info.curLevel = info.lastLevel;

                            double m = this.ms[info.curLevel];
                            double b = this.bs[info.curLevel];

                            int levIndex = info.curLevel * info.iAmount;

                            info.iterator.Reset();
                            foreach (int offset in info.iterator.Enumerate)
                            {
                                byte* val = ptr + offset + levIndex;
                                if (*val == bMissingVal)
                                {
                                    *curR = dMissingVal;
                                    curR++;
                                    continue;
                                }
                                *curR = (int)(b + *val * m);
                                curR++;
                            }
                        }
                    }
                }
            }

        }
        private void decompressByteFloat(IList toRet, IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            decompressionInfo info = this.getDecompressionInfo(firstIndices, lastIndices, strides);

            byte bMissingVal = DataConstants.GetMissingValue<byte>();
            float dMissingVal = DataConstants.GetMissingValue<float>();

            unsafe
            {
                fixed (float* rPtr = (float[])toRet)
                {
                    fixed (byte* ptr = (byte[])data)
                    {
                        float* curR = rPtr;
                        for (int i = 0; i < info.numStorageLevs; i++, info.curLevel += info.levStride)
                        {
                            if (info.curLevel > info.lastLevel) info.curLevel = info.lastLevel;

                            double m = this.ms[info.curLevel];
                            double b = this.bs[info.curLevel];

                            int levIndex = info.curLevel * info.iAmount;

                            info.iterator.Reset();
                            int lastOffset;
                            foreach (int offset in info.iterator.Enumerate)
                            {
                                lastOffset = offset;
                                byte* val = ptr + offset + levIndex;
                                if (*val == bMissingVal)
                                {
                                    *curR = dMissingVal;
                                    curR++;
                                    continue;
                                }
                                *curR = (float)(b + *val * m);
                                curR++;
                            }
                        }
                    }
                }
            }
        }
        private void decompressByteShort(IList toRet, IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            decompressionInfo info = this.getDecompressionInfo(firstIndices, lastIndices, strides);

            byte bMissingVal = DataConstants.GetMissingValue<byte>();
            short dMissingVal = DataConstants.GetMissingValue<short>();

            unsafe
            {
                fixed (short* rPtr = (short[])toRet)
                {
                    fixed (byte* ptr = (byte[])data)
                    {
                        short* curR = rPtr;
                        for (int i = 0; i < info.numStorageLevs; i++, info.curLevel += info.levStride)
                        {
                            if (info.curLevel > info.lastLevel) info.curLevel = info.lastLevel;

                            double m = this.ms[info.curLevel];
                            double b = this.bs[info.curLevel];

                            int levIndex = info.curLevel * info.iAmount;

                            info.iterator.Reset();
                            foreach (int offset in info.iterator.Enumerate)
                            {
                                byte* val = ptr + offset + levIndex;
                                if (*val == bMissingVal)
                                {
                                    *curR = dMissingVal;
                                    curR++;
                                    continue;
                                }
                                *curR = (short)(b + *val * m);
                                curR++;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Short Decompress
        private void decompressShortDouble(IList toRet, IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            decompressionInfo info = this.getDecompressionInfo(firstIndices, lastIndices, strides);

            short bMissingVal = DataConstants.GetMissingValue<short>();
            double dMissingVal = DataConstants.GetMissingValue<double>();

            unsafe
            {
                fixed (double* rPtr = (double[])toRet)
                {
                    fixed (short* ptr = (short[])data)
                    {
                        double* curR = rPtr;
                        for (int i = 0; i < info.numStorageLevs; i++, info.curLevel += info.levStride)
                        {
                            if (info.curLevel > info.lastLevel) info.curLevel = info.lastLevel;

                            double m = this.ms[info.curLevel];
                            double b = this.bs[info.curLevel];

                            int levIndex = info.curLevel * info.iAmount;

                            info.iterator.Reset();
                            foreach (int offset in info.iterator.Enumerate)
                            {
                                short* val = ptr + offset + levIndex;
                                if (*val == bMissingVal)
                                {
                                    *curR = dMissingVal;
                                    curR++;
                                    continue;
                                }
                                *curR = (double)(b + *val * m);
                                curR++;
                            }
                        }
                    }
                }
            }

        }
        private void decompressShortLong(IList toRet, IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            decompressionInfo info = this.getDecompressionInfo(firstIndices, lastIndices, strides);

            short bMissingVal = DataConstants.GetMissingValue<short>();
            long dMissingVal = DataConstants.GetMissingValue<long>();

            unsafe
            {
                fixed (long* rPtr = (long[])toRet)
                {
                    fixed (short* ptr = (short[])data)
                    {
                        long* curR = rPtr;
                        for (int i = 0; i < info.numStorageLevs; i++, info.curLevel += info.levStride)
                        {
                            if (info.curLevel > info.lastLevel) info.curLevel = info.lastLevel;

                            double m = this.ms[info.curLevel];
                            double b = this.bs[info.curLevel];

                            int levIndex = info.curLevel * info.iAmount;

                            info.iterator.Reset();
                            foreach (int offset in info.iterator.Enumerate)
                            {
                                short* val = ptr + offset + levIndex;
                                if (*val == bMissingVal)
                                {
                                    *curR = dMissingVal;
                                    curR++;
                                    continue;
                                }
                                *curR = (long)(b + *val * m);
                                curR++;
                            }
                        }
                    }
                }
            }

        }
        private void decompressShortInt(IList toRet, IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            decompressionInfo info = this.getDecompressionInfo(firstIndices, lastIndices, strides);

            short bMissingVal = DataConstants.GetMissingValue<short>();
            int dMissingVal = DataConstants.GetMissingValue<int>();

            unsafe
            {
                fixed (int* rPtr = (int[])toRet)
                {
                    fixed (short* ptr = (short[])data)
                    {
                        int* curR = rPtr;
                        for (int i = 0; i < info.numStorageLevs; i++, info.curLevel += info.levStride)
                        {
                            if (info.curLevel > info.lastLevel) info.curLevel = info.lastLevel;

                            double m = this.ms[info.curLevel];
                            double b = this.bs[info.curLevel];

                            int levIndex = info.curLevel * info.iAmount;

                            info.iterator.Reset();
                            foreach (int offset in info.iterator.Enumerate)
                            {
                                short* val = ptr + offset + levIndex;
                                if (*val == bMissingVal)
                                {
                                    *curR = dMissingVal;
                                    curR++;
                                    continue;
                                }
                                *curR = (int)(b + *val * m);
                                curR++;
                            }
                        }
                    }
                }
            }

        }
        private void decompressShortFloat(IList toRet, IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            decompressionInfo info = this.getDecompressionInfo(firstIndices, lastIndices, strides);

            short bMissingVal = DataConstants.GetMissingValue<short>();
            float dMissingVal = DataConstants.GetMissingValue<float>();

            unsafe
            {
                fixed (float* rPtr = (float[])toRet)
                {
                    fixed (short* ptr = (short[])data)
                    {
                        float* curR = rPtr;
                        for (int i = 0; i < info.numStorageLevs; i++, info.curLevel += info.levStride)
                        {
                            if (info.curLevel > info.lastLevel) info.curLevel = info.lastLevel;

                            double m = this.ms[info.curLevel];
                            double b = this.bs[info.curLevel];

                            int levIndex = info.curLevel * info.iAmount;

                            info.iterator.Reset();
                            foreach (int offset in info.iterator.Enumerate)
                            {
                                short* val = ptr + offset + levIndex;
                                if (*val == bMissingVal)
                                {
                                    *curR = dMissingVal;
                                    curR++;
                                    continue;
                                }
                                *curR = (float)(b + *val * m);
                                curR++;
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}
