using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Lambda.Generic.Arithmetic;
using CeresBase.Development;

namespace CeresBase.Data.Compressions
{
    /// <summary>
    /// Compress based on the formula mx + b. b is the minimum value, m is (range)/(binsize). There are 256 bins
    /// if compressing to bytes and 2^16 bins if compressing to shorts. This is pretty fast compression/decompression
    /// but can be very inaccurate if there is a wide data range. Stores m, b for each slowest varying dimension
    /// (since layers tend to be more similar) to provide for better precision.
    /// </summary>
    [CeresCreated("Mikkel", "03/12/06", DocumentedStatus = CeresDocumentedStage.Complete)]
    public partial class SlopeCompression : ICompression
    {
        #region Private Variables
        private double[] ms;
        private double[] bs;
        private double[] maxvals;
        private int[] lengths;
        private double maxValue;
        private bool init;
        private readonly object lockable = new object();

        private delegate void calcIndicesDelegate(IList retData, IList data, int level, int count);
        private calcIndicesDelegate calcIndices;

        private delegate void getMinMaxDelegate(IList data, int level, int count, out double min, out double max);
        private getMinMaxDelegate getMinMax;

        private delegate void decompressDelegate(IList toRet, IList data, int[] firstIndices, int[] lastIndices, int[] strides);
        private decompressDelegate decompress;
        private Type inputType;
        private Type outputType;

        #endregion

        #region Private Functions

        #endregion

        #region Public Attributes
        [CeresCreated("Mikkel", "03/13/06")]
        public bool IsInitialized
        {
            get { lock (this.lockable) { return this.init; } }
        }
        
        [CeresCreated("Mikkel", "03/13/06")]
        public double MaxRelativeError { get { return 1.0 / (this.maxValue * 2.0); } }
        
        [CeresCreated("Mikkel", "03/13/06")]
        public double MaxAbsError
        {
            get
            {
                //TODO CUSTOM EXCEPTION
                if (!this.IsInitialized)
                    throw new Exception("Cannot calculate because compression is not initialized. Please compress data before asking for this value.");

                double max = double.MinValue;
                for (int i = 0; i < ms.Length; i++)
                {
                    double range = this.maxvals[i] - this.bs[i];
                    if (range > max) max = range;
                }
                return max / (this.maxValue * 2.0);
            }
        }

        [CeresCreated("Mikkel", "03/13/06")]
        public Type CurrentInputType
        {
            get { return inputType; }
        }

        [CeresCreated("Mikkel", "03/13/06")]
        public Type CurrentOutputType
        {
            get { return outputType; }
        }

        [CeresCreated("Mikkel", "03/13/06")]
        public Type[] SupportedInputTypes
        {
            get { return new Type[] { typeof(short), typeof(float), typeof(int), typeof(long), typeof(double) }; }
        }

        [CeresCreated("Mikkel", "03/13/06")]
        public Type[] SupportedOutputTypes { get { return new Type[] { typeof(byte), typeof(short) }; } }


        #endregion

        #region Constructors

        [CeresCreated("Mikkel", "03/13/06")]
        public SlopeCompression(Type inputType, Type outputType)
        {
            this.init = false;
            this.inputType = inputType;
            this.outputType = outputType;

            if (this.inputType != typeof(double) && this.inputType != typeof(long) &&
                this.inputType != typeof(float) && this.inputType != typeof(int) &&
                this.inputType != typeof(short))
                throw new Exception("Input type not supported"); //TODO CUSTOM EXCEPTION

            if (this.outputType == typeof(byte))
            {
                this.maxValue = byte.MaxValue - 1;
                #region Set Delagates
                if (this.inputType == typeof(double))
                {
                    this.calcIndices = new calcIndicesDelegate(this.calcIndicesByteDouble);
                    this.getMinMax = new getMinMaxDelegate(this.getMinMaxDouble);
                    this.decompress = new decompressDelegate(this.decompressByteDouble);
                }
                else if (this.inputType == typeof(long))
                {
                    this.calcIndices = new calcIndicesDelegate(this.calcIndicesByteLong);
                    this.getMinMax = new getMinMaxDelegate(this.getMinMaxLong);
                    this.decompress = new decompressDelegate(this.decompressByteLong);
                }
                else if (this.inputType == typeof(int))
                {
                    this.calcIndices = new calcIndicesDelegate(this.calcIndicesByteInt);
                    this.getMinMax = new getMinMaxDelegate(this.getMinMaxInt);
                    this.decompress = new decompressDelegate(this.decompressByteInt);
                }
                else if (this.inputType == typeof(float))
                {
                    this.calcIndices = new calcIndicesDelegate(this.calcIndicesByteFloat);
                    this.getMinMax = new getMinMaxDelegate(this.getMinMaxFloat);
                    this.decompress = new decompressDelegate(this.decompressByteFloat);
                }
                else if (this.inputType == typeof(short))
                {
                    this.calcIndices = new calcIndicesDelegate(this.calcIndicesByteShort);
                    this.getMinMax = new getMinMaxDelegate(this.getMinMaxShort);
                    this.decompress = new decompressDelegate(this.decompressByteShort);
                }
                #endregion
            }
            else if (this.outputType == typeof(short))
            {
                this.maxValue = ushort.MaxValue - 1;
                #region Set Delegates
                if (this.inputType == typeof(double))
                {
                    this.calcIndices = new calcIndicesDelegate(this.calcIndicesShortDouble);
                    this.getMinMax = new getMinMaxDelegate(this.getMinMaxDouble);
                    this.decompress = new decompressDelegate(this.decompressShortDouble);
                }
                else if (this.inputType == typeof(long))
                {
                    this.calcIndices = new calcIndicesDelegate(this.calcIndicesShortLong);
                    this.getMinMax = new getMinMaxDelegate(this.getMinMaxLong);
                    this.decompress = new decompressDelegate(this.decompressShortLong);
                }
                else if (this.inputType == typeof(int))
                {
                    this.calcIndices = new calcIndicesDelegate(this.calcIndicesShortInt);
                    this.getMinMax = new getMinMaxDelegate(this.getMinMaxInt);
                    this.decompress = new decompressDelegate(this.decompressShortInt);
                }
                else if (this.inputType == typeof(float))
                {
                    this.calcIndices = new calcIndicesDelegate(this.calcIndicesShortFloat);
                    this.getMinMax = new getMinMaxDelegate(this.getMinMaxFloat);
                    this.decompress = new decompressDelegate(this.decompressShortFloat);
                }
                #endregion
            }
            else throw new Exception("Output type not supported"); //TODO CUSTOM EXCEPTION

            //this.compressionCalc = General.Utilities.GetCalculator<double>();
        }

        #endregion

        #region Public Functions
        [CeresCreated("Mikkel", "03/13/06")]
        public IList CompressRange(IList data, int[] lengths)
        {
            lock (this.lockable)
            {
                this.init = false;
            }

            int numPerStorage = 0;
            if (this.ms == null)
            {
                if (lengths.Length > 2)
                {
                    int numLastDim = lengths[lengths.Length - 1];
                    this.ms = new double[numLastDim]; //create a value for each slowest varying dimension
                    this.bs = new double[numLastDim]; //create a value for each slowest varying dimension
                    this.maxvals = new double[numLastDim];
                    numPerStorage = data.Count / numLastDim;
                }
                else
                {
                    this.ms = new double[1];
                    this.bs = new double[1];
                    this.maxvals = new double[1];
                    numPerStorage = data.Count;
                }
            }

            IList toRet = DataUtilities.CreateArray(data.Count, this.outputType);

            for (int i = 0; i < this.ms.Length; i++)
            {
                double min, max;
                this.getMinMax(data, i, numPerStorage, out min, out max);
                this.bs[i] = min;
                this.ms[i] = (max - min) / this.maxValue;
                this.maxvals[i] = max;
                this.calcIndices(toRet, data, i, numPerStorage);
            }

            lock (this.lockable)
            {
                this.init = true;
            }

            this.lengths = lengths;
            return toRet;
        }

        [CeresCreated("Mikkel", "03/13/06")]
        public IList DecompressRange(IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            if (!this.IsInitialized)
                throw new Exception("Cannot decompress all data has been compressed.");

            int amount = 1;
            int[] retLens = DataUtilities.GetLengthsFromIndices(firstIndices, lastIndices, strides);
            foreach (int lens in retLens)
            {
                amount *= lens;
            }

            IList toRet = DataUtilities.CreateArray(amount, this.inputType);
            //TOriginalType[] toRet = new TOriginalType[amount];
            this.decompress(toRet, data, firstIndices, lastIndices, strides);
            return toRet;
        }
        #endregion
    }
}
