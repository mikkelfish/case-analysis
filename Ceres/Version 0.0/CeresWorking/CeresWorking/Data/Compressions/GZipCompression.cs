using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Compression;
using System.IO;
using System.Threading;
using ThreadSafeConstructs;
using System.Collections;
using CeresBase.Development;

namespace CeresBase.Data.Compressions
{
    [CeresCreated("Mikkel", "03/12/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    class GZipCompression : ICompression
    {
        #region Private Variables
        private Type currentInputType;

        #endregion

        #region Private Functions

        #endregion

        #region Properties
        /// <summary>
        /// Zip compression needs no intializing
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public bool IsInitialized
        {
            get { return true; }
        }

        /// <summary>
        /// Zip compression can support any output type
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public Type[] SupportedOutputTypes { get { return new Type[] { typeof(byte) }; } }

        /// <summary>
        /// Zip compression can support any input type
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public Type[] SupportedInputTypes
        {
            get { return new Type[] { typeof(byte), typeof(short), typeof(float), typeof(int), typeof(long), typeof(double) }; }
        }

        [CeresCreated("Mikkel", "03/13/06")]
        public Type CurrentInputType
        {
            get { return currentInputType; }
        }

        /// <summary>
        /// Zip compression only returns byte arrays
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public Type CurrentOutputType
        {
            get
            {
                return typeof(byte);
            }
        }

        /// <summary>
        /// Zip compression is always lossless
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public double MaxRelativeError { get { return 0D; } }

        /// <summary>
        /// Zip compression is always lossless
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public double MaxAbsError { get { return 0D; } }


        #endregion

        #region Constructors
        [CeresCreated("Mikkel", "03/13/06")]
        public GZipCompression(Type inputType)
        {
            this.currentInputType = inputType;
        }

        #endregion

        #region Public Functions
        [CeresCreated("Mikkel", "03/13/06")]
        public IList CompressRange(IList data, int[] lengths)
        {
            byte[] byteData = DataUtilities.ArrayToByteArray(data);
            byte[] toRet = null;

            using (ThreadSafeMemoryStream newStream = new ThreadSafeMemoryStream())
            {
                using (DeflateStream str = new DeflateStream(newStream, CompressionMode.Compress, true))
                {
                    int blockSize = 64000;
                    int curSpot = 0;
                    while (curSpot < byteData.Length)
                    {
                        int realSize = blockSize;
                        if (curSpot + blockSize >= byteData.Length) realSize = byteData.Length - curSpot;
                        str.Write(byteData, curSpot, realSize);
                        curSpot += blockSize;
                    }
                    str.Flush();
                }
                toRet = newStream.ToArray();
            }
            return toRet;
        }

        [CeresCreated("Mikkel", "03/13/06")]
        public IList DecompressRange(IList data, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            byte[] byteData = (byte[])data;
            List<byte> toRet = new List<byte>();

            using (ThreadSafeMemoryStream newStream = new ThreadSafeMemoryStream(byteData))
            {
                using (DeflateStream str = new DeflateStream(newStream, CompressionMode.Decompress, true))
                {
                    int blockSize = 64000;
                    byte[] buffer = new byte[blockSize];
                    int curSpot = 0;
                    while (true)
                    {
                        int read = str.Read(buffer, 0, blockSize);
                        if (read == 0) break;

                        if (read == blockSize)
                        {
                            toRet.AddRange(buffer);
                        }
                        else
                        {
                            for (int i = 0; i < read; i++)
                            {
                                toRet.Add(buffer[i]);
                            }
                        }

                        curSpot += read;
                    }
                    str.Flush();
                }
            }
            return DataUtilities.ByteArrayToArray(toRet.ToArray(), this.currentInputType);
        }

        #endregion
    }
}
