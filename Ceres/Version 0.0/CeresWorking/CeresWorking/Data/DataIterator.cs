using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;

//MBF DUPTD 3/11/06
namespace CeresBase.Data
{
    /// <summary>
    /// This class is for setting up a subsampling map and then iterating through all the indices.
    /// </summary>
    [CeresCreated("Mikkel", "03/13/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    public class DataIterator : ICloneable
    {
        #region Private Variables
        private int[] curIndexes;
        private readonly int[] firstIndices;
        private int[] lastIndices;
        private int[] strides;
        private int[] dataDimensionLengths;
        private int[] iteratorAmounts;

        private int curOffset;
        private int origOffset;
        private int totalAmount;
        #endregion

        #region Private Functions
        //This is used to get the next index
        [CeresCreated("Mikkel", "03/13/06")]
        private int advanceLoop()
        {
            int curAdd = 1;
            for (int j = 0; j < curIndexes.Length; j++)
            {
                curIndexes[j] += strides[j];
                if (curIndexes[j] > lastIndices[j])
                {
                    if (curIndexes[j] < lastIndices[j] + strides[j])
                    {
                        this.curOffset += (curIndexes[j] - lastIndices[j]) * curAdd;
                        curIndexes[j] = lastIndices[j];
                        break;
                    }
                    else
                    {
                        curIndexes[j] = this.firstIndices[j];
                        this.curOffset -= (this.lastIndices[j] - this.firstIndices[j]) * curAdd;
                        curAdd *= this.dataDimensionLengths[j];
                    }
                }
                else
                {
                    this.curOffset += strides[j] * curAdd;
                    break;
                }
            }
            return this.curOffset;
        }

        /// <summary>
        /// Setup information regardless of constructor called
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        private void forAllConstructors(int[] strides, int[] dimLens)
        {
            this.strides = strides;
            this.curIndexes = new int[this.firstIndices.Length];
            this.firstIndices.CopyTo(this.curIndexes, 0);

            this.totalAmount = 1;
            this.iteratorAmounts = DataUtilities.GetLengthsFromIndices(this.firstIndices, this.lastIndices,
                this.strides);
            for (int i = 0; i < this.firstIndices.Length; i++)
            {
                totalAmount *= this.iteratorAmounts[i];
            }
            this.dataDimensionLengths = dimLens;

            this.curOffset = DataIterator.GetSpot(this.firstIndices, this.dataDimensionLengths);
            this.origOffset = this.curOffset;
        }

        [CeresCreated("Mikkel", "03/13/06")]
        private static int getSpotHelper(int[] indexes, int[] dimensionLengths, int cur, int pSpot)
        {
            int spot = indexes[cur - 1] + pSpot * dimensionLengths[cur - 1];

            if (cur == 1) return spot;
            else return getSpotHelper(indexes, dimensionLengths, --cur, spot);

        }


        #endregion

        #region Properties
        /// <summary>
        /// Implements the Enumerate function so this can be used in foreach loops which is really spiffy
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public System.Collections.Generic.IEnumerable<int> Enumerate
        {
            get
            {
                yield return this.curOffset;
                for (int i = 0; i < this.Count - 1; i++)
                {
                    yield return this.advanceLoop();
                }
                yield break;
            }
        }

        /// <summary>
        /// Get the next location
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public int Next
        {
            get
            {
                int spot = this.advanceLoop();
                if (spot > this.Count) return -1;
                return spot;
            }
        }

        /// <summary>
        /// The length of the whole iteration
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public int Count
        {
            get { return this.totalAmount; }
        }

        [CeresCreated("Mikkel", "03/20/06")]
        public int[] CurrentIndices
        {
            get
            {
                return this.curIndexes;
            }
        }

        /// <summary>
        /// Return the indices of the entire enumeration.
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public int[][] Indices
        {
            get
            {
                int[][] toRet = new int[this.firstIndices.Length][];

                for (int i = 0; i < toRet.Length; i++)
                {
                    List<int> thisList = new List<int>();
                    int curSpot = this.firstIndices[i];
                    for (int j = 0; j < this.iteratorAmounts[i]; j++, curSpot += this.strides[i])
                    {
                        if (curSpot > this.lastIndices[i]) curSpot = this.lastIndices[i];
                        thisList.Add(curSpot);
                    }
                    toRet[i] = thisList.ToArray();
                }
                return toRet;
            }
        }
        #endregion

        #region Constructors

        /// <summary>
        /// Use this constructor when dealing in Data Shape Space.
        /// </summary>
        /// <param name="dataDimLengths">The lengths of each dimension</param>
        /// <param name="firstLocation">The location of the first "corner"</param>
        /// <param name="secondLocation">The location of the second "corner"</param>
        /// <param name="strides">The subsampling array.</param>
        /// <param name="shape">The shape to use to convert the location to the indices</param>
        [CeresCreated("Mikkel", "03/13/06")]
        public DataIterator(int[] dataDimLengths, object firstLocation, object secondLocation, int[] strides, IDataShape shape)
        {
            this.firstIndices = DataUtilities.CastDown(shape.ShapeToGrid(firstLocation));
            this.lastIndices = DataUtilities.CastUp(shape.ShapeToGrid(secondLocation));
            this.forAllConstructors(strides, dataDimLengths);
        }

        [CeresCreated("Mikkel", "03/20/06")]
        public DataIterator(int[] dataDimLengths, int[] firstIndices, int[] count)
        {
            this.firstIndices = firstIndices;
            this.lastIndices = new int[count.Length];
            this.strides = new int[count.Length];
            for (int i = 0; i < count.Length; i++)
            {
                this.lastIndices[i] = this.firstIndices[i] + count[i];
                this.strides[i] = 1;
            }
            
            this.forAllConstructors(strides, dataDimLengths);
        }

        /// <summary>
        /// Use this constructor when dealing in Index Space
        /// </summary>
        /// <param name="dataDimLengths">The lengths of each dimension.</param>
        /// <param name="firstIndices">The indices of the first corner</param>
        /// <param name="lastIndices">The indices of the second corner</param>
        /// <param name="strides">The subsampling array</param>
        [CeresCreated("Mikkel", "03/13/06")]
        public DataIterator(int[] dataDimLengths, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            this.firstIndices = firstIndices;
            this.lastIndices = lastIndices;
            this.forAllConstructors(strides, dataDimLengths);
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Reset the iterator
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public void Reset()
        {
            this.firstIndices.CopyTo(this.curIndexes, 0);
            this.curOffset = this.origOffset;
        }

        /// <summary>
        /// Get the index of the given location.
        /// </summary>
        /// <param name="indexes">The N-Dimensional location</param>
        /// <param name="dimensionLengths">Length of each dimension</param>
        /// <returns>The index for the given location</returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static int GetSpot(int[] indexes, int[] dimensionLengths)
        {
            if (indexes.Length == 1) return indexes[0];
            return getSpotHelper(indexes, dimensionLengths, indexes.Length - 1, indexes[indexes.Length - 1]);
        }

        [CeresCreated("Mikkel", "03/20/06")]
        public static int[] GetIndices(int spot, int[] dimensionLengths)
        {
            int[] amountPerDim = new int[dimensionLengths.Length];
            int amount = 1;
            amountPerDim[amountPerDim.Length - 1] = amount;
            for (int i = amountPerDim.Length - 2; i >= 0; i--)
            {
                amount *= dimensionLengths[i + 1];
                amountPerDim[i] = amount;
            }

            int[] toRet = new int[dimensionLengths.Length];
            for (int i = 0; i < amountPerDim.Length; i++)
            {
                toRet[i] = spot / amountPerDim[i];
                spot = spot % amountPerDim[i];
            }
            return toRet;
        }

        public static int[] GetOffsetIndices(int spot, int[] firstIndices, int[] lastIndices, int[] strides)
        {
            int[] dimensionLengths = new int[firstIndices.Length];
            for (int i = 0; i < dimensionLengths.Length; i++)
            {
                dimensionLengths[i] = lastIndices[i] - firstIndices[i];
            }

            DataIterator iterator = new DataIterator(dimensionLengths, firstIndices, lastIndices, strides);
            for (int i = 0; i < spot; i++)
            {
                int throwaway = iterator.Next;
            }
            return iterator.curIndexes;
        }
        #endregion

        #region ICloneable Members

        public object Clone()
        {
            DataIterator newIterator = new DataIterator(this.dataDimensionLengths, this.firstIndices, this.lastIndices, this.strides);
            
            newIterator.curIndexes = this.curIndexes;
            newIterator.curOffset = this.curOffset;

            return newIterator;
        }

        #endregion
    }
}
