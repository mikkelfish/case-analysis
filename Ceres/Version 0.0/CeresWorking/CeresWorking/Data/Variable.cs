using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;
using System.Windows.Forms;
using CeresBase.Projects;

namespace CeresBase.Data
{
    /// <summary>
    /// This class is the basic abstraction for a collection of data.
    /// </summary>
    [CeresCreated("Mikkel", "03/13/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    [CeresModified("Mikkel", "03/16/06", Comments = "Added ICeresSerializable Property")]
    public class Variable : IO.ICeresSerializable, IDisposable
    {
        #region Private Variables
        private double min;
        private double max;
        private List<DataFrame> frames;
        private VariableHeader header;
        private readonly object lockable = new object();
        private bool visible;
        #endregion

        #region Private Functions
        [CeresCreated("Mikkel", "03/14/06")]
        private void sortTimes()
        {
            lock (this.lockable)
            {
                this.frames.Sort();
            }
        }

        [CeresCreated("Mikkel", "03/14/06")]
        [CeresToDo("Mikkel", "03/14/06", Comments = "Exception", Priority = DevelopmentPriority.Exception)]
        private void addDataFrame(DataFrame frame)
        {
            if (this.frames.Contains(frame))
                throw new Exception("This variable already has this data frame\n"); //TODO MAKE CUSTOM

            lock (this.lockable)
            {
                this.frames.Add(frame);
                if (this.min > frame.Min) this.min = frame.Min;
                if (this.max < frame.Max) this.max = frame.Max;
            }
        }
        #endregion

        #region Public Properties
        [CeresCreated("Mikkel", "03/20/06")]
        public bool Visible
        {
            get
            {
                return this.visible;
            }
            set
            {
                this.visible = value;
            }
        }

        public string FullDescription
        {
            get
            {
                return this.Header.ExperimentName + "_" + this.Header.Description;
            }
        }

        public string FullVarName
        {
            get
            {
                return this.header.ExperimentName + "_" + this.header.VarName;
            }
        }

        public System.Collections.Generic.IEnumerator<DataFrame> GetEnumerator()
        {
            for (int i = 0; i < this.frames.Count; i++)
            {
                yield return this.frames[i];
            }
            yield break; ;
        }

        /// <summary>
        /// Get the max value for the entire variable. In practice this means asking each data frame
        /// what its max is.
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public double Max
        {
            get { lock (this.lockable) { return max; } }
        }

        /// <summary>
        /// Get the min value for the entire variable. This just asks what each data frame's min is.
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public double Min
        {
            get { lock (this.lockable) { return min; } }
        }

        /// <summary>
        /// Returns true if the concept of time is not valid for this variable
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public bool TimeLess
        {
            get 
            {
                lock (this.lockable)
                {
                    List<DateTime> times = new List<DateTime>();
                    foreach (DataFrame frame in this.frames)
                    {
                        if (!times.Contains(frame.Time)) times.Add(frame.Time);
                    }

                    return times.Count == 1 && times[0] == CeresBase.General.Constants.Timeless;
                }
            }
        }

        /// <summary>
        /// Get an array of all the times for the variable
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public DateTime[] Times
        {
            get
            {
                if (this.TimeLess) return new DateTime[] { CeresBase.General.Constants.Timeless };
                lock (this.lockable)
                {
                    //TODO ACCOUNT FOR DUPLICATE TIMES
                    DateTime[] list = new DateTime[this.frames.Count];
                    for (int i = 0; i < this.frames.Count; i++)
                    {
                        DataFrame frame = this.frames[i];
                        list[i] = frame.Time;
                    }
                    return list;
                }
            }
        }

        /// <summary>
        /// Get a data frame based on its index
        /// </summary>
        /// <param name="index">Index of the data frame to retrieve</param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public DataFrame this[int index]
        {
            get
            {
                return this.GetFrame(index);
            }
        }

        ///// <summary>
        ///// Get the data frame(s) based on a certain time. Note that a variable can have multiple
        ///// data frames with the same time if they have different shapes and/or data.
        ///// </summary>
        ///// <param name="time"></param>
        ///// <returns></returns>
        //[CeresCreated("Mikkel", "03/14/06")]
        //public DataFrame[] this[DateTime time]
        //{
        //    get { return this.GetFrames(time); }
        //}

        /// <summary>
        /// Retrieve variable header information to access things like variable name, units
        /// and description
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public VariableHeader Header { get { return this.header; } }

        /// <summary>
        /// Return the number of frames in the variable.
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public int NumFrames { get { lock (this.lockable) { return this.frames.Count; } } }


        #endregion

        #region Constructors

       

        /// <summary>
        /// Create a variable that is not timeless and which keeps the same name as in the header
        /// </summary>
        /// <param name="header"></param>
        [CeresCreated("Mikkel", "03/20/06")]
        protected Variable(VariableHeader header)
        {
            this.max = double.MinValue;
            this.min = double.MaxValue;
            this.frames = new List<DataFrame>();
            this.header = header;

            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
        }

        void Application_ApplicationExit(object sender, EventArgs e)
        {
            this.OnExit();
        }

        /// <summary>
        /// NOT FOR USE EXCEPT FOR QUERYING
        /// </summary>
        [CeresCreated("Mikkel", "03/20/06")]
        protected Variable()
        {
        }
        #endregion

        #region Public Functions

        public virtual void OnExit()
        {

        }


        /// <summary>
        /// Add an array of data frames to the variable
        /// </summary>
        /// <param name="frames"></param>
        [CeresCreated("Mikkel", "03/14/06")]
        public virtual void AddDataFrames(DataFrame[] frames)
        {
            foreach (DataFrame frame in frames)
            {
                this.addDataFrame(frame);
            }
            this.sortTimes();
        }

        /// <summary>
        /// Add a single data frame to the variable
        /// </summary>
        /// <param name="frame"></param>
        [CeresCreated("Mikkel", "03/14/06")]
        public virtual void AddDataFrame(DataFrame frame)
        {
            this.addDataFrame(frame);
            this.sortTimes();
        }

        /// <summary>
        /// Get the indices of all the data frames that have the passed time
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public int[] GetIndicesOfTime(DateTime time)
        {
            if (this.TimeLess) return new int[] { 0 };
            List<int> indices = new List<int>();
            lock (this.lockable)
            {
                for (int i = 0; i < this.frames.Count; i++)
                {
                    if (frames[i].Time == time)
                        indices.Add(i);
                }
            }
            return indices.ToArray();
        }

        /// <summary>
        /// Get the time of the data frame at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public DateTime GetTimeOfIndex(int index)
        {
            lock (this.lockable)
            {
                if (this.TimeLess) return CeresBase.General.Constants.Timeless;
                return this.frames[index].Time;
            }
        }

        /// <summary>
        /// Get the frame at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public DataFrame GetFrame(int index)
        {
            lock (this.lockable)
            {
                return this.frames[index];
            }
        }

        /// <summary>
        /// Get the frame(s) at the specified time
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public DataFrame[] GetFrames(DateTime time)
        {
            List<DataFrame> toRet = new List<DataFrame>();
            lock (this.lockable)
            {
                foreach (DataFrame frame in this.frames)
                {
                    if (frame.Time == time)
                        toRet.Add(frame);
                }
            }
            return toRet.ToArray();
        }

        /// <summary>
        /// Does the variable have at least one data frame with the given time?
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public bool HasTime(DateTime time)
        {
            lock (this.lockable)
            {
                return this.frames.Exists(delegate(DataFrame frame)
                { return frame.Time == time; });
            }
        }
        #endregion

 
        #region Equals Override
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj.GetType() == this.GetType())
            {
                return (obj as Variable).header == this.header;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.header.VarName.GetHashCode();
        }
        #endregion

        //private List<Epoch> epochsFromFile = new List<Epoch>();
        //public List<Epoch> EpochsFromFile
        //{
        //    get
        //    {
        //        return this.epochsFromFile;
        //    }
        //}
        
        public virtual Type HeaderType
        {
            get
            {
                return typeof(VariableHeader);
            }
        }


        public override string ToString()
        {
            return this.FullDescription;
        }

        #region ICeresSerializable Members

        public virtual void AddSpecificInformation(CeresBase.IO.ICeresWriter writer, CeresBase.IO.ICeresFile file)
        {
            
        }

        public virtual void ReadSpecificInformation(CeresBase.IO.ICeresReader reader, CeresBase.IO.ICeresFile file)
        {
            
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            foreach (DataFrame frame in this.frames)
            {
                frame.Dispose();
            }
        }

        #endregion
    }
}
