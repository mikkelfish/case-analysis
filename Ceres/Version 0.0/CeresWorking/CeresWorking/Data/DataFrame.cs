using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using System.Threading;
using CeresBase.Development;
using CeresBase.Settings;
using System.Linq;

namespace CeresBase.Data
{
    /// <summary>
    /// 
    /// </summary>
    [CeresCreated("Mikkel", "03/13/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    [CeresModified("Mikkel", "03/16/06", Comments = "Added variable Property", DocumentedStatus=CeresDocumentedStage.Complete)]
    //[XMLSetting("DataFrame", "", true, false)]
    public class DataFrame: IComparable<DataFrame>, IDisposable
    {
        private class dataFrameInfo
        {
            public float Minimum { get; set; }
            public float Maximum { get; set; }
            public string VarName { get; set; }
            public string Time { get; set; }
        }

        private static List<dataFrameInfo> saved =
            CeresBase.IO.Serialization.NewSerializationCentral.RegisterCollection<List<dataFrameInfo>>(typeof(DataFrame), "saved");

        #region Private Variables
        private DateTime time;
        private IDataPool pool;

        //MBF Change 2/2/05
        private IDataShape shape;
        private IPointInterpolation interpolator;
        private ManualResetEvent ev;
        private Variable variable;

        //private static XMLSettingAttribute attr;
        #endregion

        #region Public Properties

        /// <summary>
        /// Get the data shape associated with this particular frame.
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public IDataShape Shape { get { return this.shape; } }

        /// <summary>
        /// Get the minimum value in the pool that is inside the bounds of the shape
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        [CeresToDo("Mikkel", "03/13/06", Comments="ACCOUNT FOR SHAPE")]
        public double Min { get { return this.pool.Min; } } //TODO ACCOUNT FOR SHAPE

        /// <summary>
        /// Get the maximum value in the pool that is inside the bounds of the shape
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        [CeresToDo("Mikkel", "03/13/06", Comments = "ACCOUNT FOR SHAPE")]
        public double Max { get { return this.pool.Max; } } //TODO ACCOUNT FOR SHAPE

        /// <summary>
        /// Get the time for the data frame
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public DateTime Time { get { return this.time; } }

        /// <summary>
        /// Get the underlying data pool for the frame
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public IDataPool Pool { get { return this.pool; } }

        /// <summary>
        /// Gets or sets the interpolator responsible for 
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public IPointInterpolation Interpolator
        {
            get
            {
                this.ev.WaitOne();
                return this.interpolator;
            }
            set
            {
                this.ev.Reset();
                try
                {
                    this.interpolator = value;
                }
                finally
                {
                    this.ev.Set();
                }
            }
        }

        /// <summary>
        /// Get the variable that this data frame belongs to.
        /// </summary>
        [CeresCreated("Mikkel", "03/16/06")]
        public Variable Variable
        {
            get
            {
                return this.variable;
            }
        }

        #endregion

        #region Constructors

        static DataFrame()
        {
            //DataFrame.attr = CeresBase.General.Utilities.GetAttribute<XMLSettingAttribute>(typeof(DataFrame), true);
        }

        /// <summary>
        /// Create a data frame with the default PointInterpolation
        /// </summary>
        /// <param name="pool">The underlying data pool</param>
        /// <param name="shape"></param>
        /// <param name="time"></param>
        [CeresCreated("Mikkel", "03/13/06")]
        [CeresToDo("Mikkel", "03/13/06", Comments = "Read the default interpolator from the settings")]
        [CeresModified("Mikkel", "03/16/06", Comments = "Since a Data Frame cannot belong to more than one variable, it should store which variable it belongs to")]
        public DataFrame(Variable variable, IDataPool pool, DateTime time)
            : this(variable, pool, time, new Interpolations.NoInterpolation())
        {

        }

        /// <summary>
        /// Create a data frame with a specified PointInterpolation
        /// </summary>
        /// <param name="pool">The underlying data pool</param>
        /// <param name="shape"></param>
        /// <param name="time"></param>
        /// <param name="interpolator"></param>
        [CeresCreated("Mikkel", "03/13/06")]
        [CeresModified("Mikkel", "03/16/06", Comments = "Since a Data Frame cannot belong to more than one variable, it should store which variable it belongs to")]        
        public DataFrame(Variable variable, IDataPool pool, 
            DateTime time, IPointInterpolation interpolator)
        {
            this.variable = variable;
            this.pool = pool;

            if (!this.pool.BeenSet)
            {                
                this.pool.DoneSettingData();
            }

            //XmlConfigurationCategory maxMinCat = DataFrame.attr.Category.Categories["MinMax", true];
            //XMLSettingAttribute config = CeresBase.General.Utilities.GetAttribute<XMLSettingAttribute>(typeof(DataFrame), true);

            //XmlConfigurationCategory frameCat = maxMinCat.Categories[variable.FullVarName, true];
            //XmlConfigurationOption thisOption = frameCat.Options[time.ToLongTimeString(), true]; 
            if (!this.pool.MinMaxFound)
            {
                //if (thisOption.Value != null && (string)thisOption.Value != "")
                //{
                //    string[] vals = (thisOption.Value as string).Split(' ');
                //    this.pool.SetMinMaxManually(float.Parse(vals[0]), float.Parse(vals[1]));
                //}
                dataFrameInfo info = saved.FirstOrDefault(dr => dr.VarName == variable.FullVarName && dr.Time == time.ToString());
                 
                if (info != null)
                {
                    this.pool.SetMinMaxManually(info.Minimum, info.Maximum);
                }
                else
                {
                    this.pool.CalcMinMax();

                    info = new dataFrameInfo(){VarName = variable.FullVarName, Time = time.ToString(), Minimum = this.pool.Min,
                        Maximum = this.pool.Max};
                    saved.Add(info);
                }
            }
            else if(!saved.Any(dr => dr.VarName == variable.FullVarName && dr.Time == time.ToString()))
            {
                dataFrameInfo info = new dataFrameInfo(){VarName = variable.FullVarName, Time = time.ToString(), Minimum = this.pool.Min,
                        Maximum = this.pool.Max};
                    saved.Add(info);
            }

            //if (thisOption.Value == null || (string)thisOption.Value == "") 
            //    thisOption.Value = pool.Min.ToString() + " " + pool.Max.ToString();

            this.shape = variable.Header.CreateShape();
            this.time = time;
            this.interpolator = interpolator;
            this.pool.FrameAdded();
            this.ev = new ManualResetEvent(true);
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Get the data value at a particular location in data shape space. The return value will be interpolated.
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public float GetDataValue(object location)
        {
            this.ev.WaitOne();
            return this.interpolator.GetAndInterpolate(location, this.pool, this.shape);
        }

        /// <summary>
        /// Get the values at an array of locations.
        /// </summary>
        /// <param name="locationArray"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public float[] GetData(object[] locationArray)
        {
            this.ev.WaitOne();
            return this.interpolator.GetAndInterpolateArray(locationArray, this.pool, this.shape);
        }

        public float[] GetContiguousData(object start, object end)
        {
            this.ev.WaitOne();
            return this.interpolator.GetContiguousData(start, end, this.pool, this.shape);
        }

        public float[] GetAllData()
        {
            List<float> toRet = new List<float>();
            this.pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] lengths)
                {
                    toRet.AddRange(data);
                }
            );

            return toRet.ToArray();
        }


        #region Equals Override
        // override object.Equals
        [CeresCreated("Mikkel", "03/13/06")]
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return this.Time == (obj as DataFrame).Time;
        }

        [CeresCreated("Mikkel", "03/13/06")]
        public override int GetHashCode()
        {
            return this.Time.GetHashCode();
        }

        #endregion

        #region IComparable<DataFrame> Members

        [CeresCreated("Mikkel", "03/13/06")]
        public int CompareTo(DataFrame other)
        {
            return this.Time.CompareTo(other.Time);
        }

        #endregion
        #endregion


        #region IDisposable Members

        public void Dispose()
        {
            this.pool.FrameRemoved();
        }

        #endregion
    }
}

