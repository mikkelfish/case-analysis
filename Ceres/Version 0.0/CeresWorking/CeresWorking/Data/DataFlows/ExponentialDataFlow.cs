//using System;
//using System.Collections.Generic;
//using System.Text;
//using CeresBase.Development;

//namespace CeresBase.Data.DataFlows
//{
//    /// <summary>
//    /// This particular data flow gives back an exponential amount of data for each pass. It calculates 
//    /// the spacing for each flow with 2^(maxPasses - currentPass). This means that the second pass will have 
//    /// twice as much data as the first pass, the third will have four times as much as the first pass, etc.
//    /// NOTE: The first and second pass will have the same amount of data (+/- 1) because that was the easiest way
//    /// to do it. So this means that the first and second pass will actually be 2^(maxPasses-currentPass-1) 
//    /// Keep in mind that this is for EACH dimension, so if you have a 3D data source with 3 max passes,
//    /// then the first pass will return (1/4)^3 so 1/64th the amount of data.
//    /// </summary>
//    [CeresCreated("Mikkel", "03/14/06")]
//    public class ExponentialDataFlow : IDataFlow
//    {
//        #region Private Variables
//        private DataFrame frame;
//        private int numPasses;
//        private int[] firstIndices;
//        private int[] lastIndices;
//        private int curPass;
//        private readonly object lockable = new object();
//        #endregion

//        #region Private Functions
        
//        [CeresCreated("Mikkel", "03/14/06")]
//        [CeresToDo("Mikkel", "03/14/06", Comments = "I made this a long time ago and I'm not sure whether I ever really tested it", 
//            Priority=DevelopmentPriority.Critical)]
//        private float[] doPassWrapper(out int[][] dataIndices, bool setIndices, out int passNumber)
//        {
//            int thisPass;

//            lock (this.lockable)
//            {
//                thisPass = this.curPass;
//                this.curPass++;
//            }

//            passNumber = thisPass;

//            if (thisPass >= this.numPasses)
//            {
//                dataIndices = null;
//                return null;
//            }

//            int exponent;
//            if (thisPass == 0) exponent = this.numPasses - 1;
//            else exponent = this.numPasses - thisPass;
//            int stride = (int)Math.Pow(2.0, (double)(exponent - 1));

//            int[] passStrides = new int[this.firstIndices.Length];
//            int[] passFirstIndices = new int[this.firstIndices.Length];
//            int[] passLastIndices = new int[this.lastIndices.Length];
//            for (int i = 0; i < passStrides.Length; i++)
//            {
//                passStrides[i] = stride;
//                if (thisPass != 0) passFirstIndices[i] = (int)(stride / 2.0);
//                else passFirstIndices[i] = this.firstIndices[i];

//                if (thisPass != 0)
//                {
//                    int numPoints = this.lastIndices[i] / stride;
//                    passLastIndices[i] = passFirstIndices[i] + stride * numPoints;
//                }
//                else passLastIndices[i] = this.lastIndices[i];
//            }

//            if (setIndices)
//            {
//                DataIterator iterator = new DataIterator(this.frame.Pool.DimensionLengths, passFirstIndices, passLastIndices,
//                    passStrides);
//                dataIndices = iterator.Indices;
//            }
//            else dataIndices = null;

//            return null;
//        }

//        #endregion

//        #region Properties

//        /// <summary>
//        /// Get the frame associated with the data flow
//        /// </summary>
//        [CeresCreated("Mikkel", "03/14/06")]
//        public DataFrame Frame
//        {
//            get
//            {
//                return this.frame;
//            }
//        }

//        /// <summary>
//        /// Get the first corner of the flow
//        /// </summary>
//        [CeresCreated("Mikkel", "03/14/06")]
//        public int[] FirstIndices
//        {
//            get
//            {
//                return this.firstIndices;
//            }
//        }

//        /// <summary>
//        /// Get the last corner of the flow
//        /// </summary>
//        [CeresCreated("Mikkel", "03/14/06")]
//        public int[] LastIndices
//        {
//            get
//            {
//                return this.lastIndices;
//            }
//        }

//        #endregion

//        #region Constructors
//        /// <summary>
//        /// Give the data flow the frame, two corners, and the number of passes total to completely
//        /// fill in the data.
//        /// </summary>
//        /// <param name="frame"></param>
//        /// <param name="firstIndices"></param>
//        /// <param name="lastIndices"></param>
//        /// <param name="numPasses"></param>
//        [CeresCreated("Mikkel", "03/14/06")]
//        public ExponentialDataFlow(DataFrame frame, int[] firstIndices, int[] lastIndices, int numPasses)
//        {
//            this.numPasses = numPasses;
//            this.firstIndices = firstIndices;
//            this.lastIndices = lastIndices;
//            this.curPass = 0;
//            this.frame = frame;
//        }
//        #endregion

//        #region Public Functions
//        /// <summary>
//        /// Do a pass. The amount of data returned will be about 2^pass
//        /// </summary>
//        /// <returns></returns>
//        [CeresCreated("Mikkel", "03/14/06")]
//        public float[] DoPass()
//        {
//            int[][] throwAway;
//            int throwInt;
//            return doPassWrapper(out throwAway, false, out throwInt);
//        }

//        /// <summary>
//        /// Do a pass and get the pass number
//        /// </summary>
//        /// <param name="passNumber"></param>
//        /// <returns></returns>
//        [CeresCreated("Mikkel", "03/14/06")]
//        public float[] DoPass(out int passNumber)
//        {
//            int[][] throwAway;
//            return doPassWrapper(out throwAway, false, out passNumber);
//        }

//        /// <summary>
//        /// Do a pass and get the indices for each value returned as well as the pass number
//        /// </summary>
//        /// <param name="dataIndices"></param>
//        /// <param name="passNumber"></param>
//        /// <returns></returns>
//        [CeresCreated("Mikkel", "03/14/06")]
//        public float[] DoPass(out int[][] dataIndices, out int passNumber)
//        {
//            return doPassWrapper(out dataIndices, true, out passNumber);
//        }
//        #endregion
//    }
//}
