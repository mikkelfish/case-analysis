using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;

//MBF DUPTD 3/10/06 
namespace CeresBase.Data
{
    /// <summary>
    /// Provides typebased constants
    /// </summary>
    [CeresCreated("Mikkel", "03/13/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    public static class DataConstants
    {
        /// <summary>
        /// Get the size of an integral type
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public static int MemorySize(Type T)
        {
            if (T == typeof(double)) return sizeof(double);
            if (T == typeof(int)) return sizeof(int);
            if (T == typeof(float)) return sizeof(float);
            if (T == typeof(short)) return sizeof(short);
            if (T == typeof(byte)) return sizeof(byte);
            if (T == typeof(long)) return sizeof(long);
            if (T == typeof(sbyte)) return sizeof(sbyte);
            throw new Exception("This object is not supported by MemorySize.");
        }

        /// <summary>
        /// Is the value "missing?"
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public static bool IsMissing(object val)
        {
            if (val is byte) return (byte)val == byte.MaxValue;
            if (val is float) return (float)val == float.MaxValue;
            if (val is double) return (double)val == double.MaxValue;
            if (val is short) return (short)val == short.MaxValue;
            if (val is int) return (int)val == int.MaxValue;
            if (val is long) return (long)val == long.MaxValue;
            if (val is sbyte) return (sbyte)val == sbyte.MaxValue;
            throw new Exception("This object is not supported by IsMissing."); //TODO CUSTOM EXCEPTION
        }

        /// <summary>
        /// Get the value for Missing based on type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static T GetMissingValue<T>()
        {
            return (T)DataConstants.GetMissingValue(typeof(T));
        }

        /// <summary>
        /// Get the value for Missing based on type
        /// </summary>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static object GetMissingValue(Type type)
        {
            if (type == typeof(double)) return double.MaxValue;
            if (type == typeof(int)) return int.MaxValue;
            if (type == typeof(float)) return float.MaxValue;
            if (type == typeof(short)) return short.MaxValue;
            if (type == typeof(byte)) return byte.MaxValue;
            if (type == typeof(long)) return long.MaxValue;
            if (type == typeof(sbyte)) return sbyte.MaxValue;
            throw new Exception("This object is not supported by GetMissingValue.");
        }

        /// <summary>
        /// Get the maxvalue for the given integral type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/13/06")]
        public static object GetMaxValue(Type type)
        {
            if (type == typeof(double)) return double.MaxValue;
            if (type == typeof(int)) return int.MaxValue;
            if (type == typeof(float)) return float.MaxValue;
            if (type == typeof(short)) return short.MaxValue;
            if (type == typeof(byte)) return byte.MaxValue;
            if (type == typeof(long)) return long.MaxValue;
            if (type == typeof(sbyte)) return sbyte.MaxValue;
            throw new Exception("This object is not supported by GetMaxValue.");
        }

        /// <summary>
        /// Get the maxvalue for the given integral type
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public static T GetMaxValue<T>()
        {
            return (T)DataConstants.GetMaxValue(typeof(T));
        }

        /// <summary>
        /// Get the minvalue for the given integral type
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public static object GetMinValue(Type type)
        {
            if (type == typeof(double)) return double.MinValue;
            if (type == typeof(int)) return int.MinValue;
            if (type == typeof(float)) return float.MinValue;
            if (type == typeof(short)) return short.MinValue;
            if (type == typeof(byte)) return byte.MinValue;
            if (type == typeof(long)) return long.MinValue;
            if (type == typeof(sbyte)) return sbyte.MinValue;
            throw new Exception("This object is not supported by GetMinValue.");
        }

        /// <summary>
        /// Get the minvalue for the given integral type
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public static T GetMinValue<T>()
        {
            return (T)DataConstants.GetMinValue(typeof(T));
        }

        /// <summary>
        /// Get an array of all the numeric types supported by Ceres
        /// </summary>
        [CeresCreated("Mikkel", "03/13/06")]
        public static readonly Type[] AllSupportedTypes = new Type[]{typeof(byte), typeof(short),
            typeof(int), typeof(float), typeof(double), typeof(long)};
    }
}
