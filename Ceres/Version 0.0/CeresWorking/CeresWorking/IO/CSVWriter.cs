using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CeresBase.IO
{
    public static class CSVWriter
    {
        public static void WriteCSV(string filename, object[][] toWrite)
        {
            using (FileStream stream = new FileStream(filename, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    foreach (object[] objArray in toWrite)
                    {
                        if (objArray != null)
                        {
                            string line = String.Empty;
                            foreach (object obj in objArray)
                            {
                                if (obj == null) line += ",";
                                else line += obj.ToString().Replace(',', '.') + ",";
                            }
                            
                            if(line.Length > 0)
                                line = line.Substring(0, line.Length - 1);
                            writer.WriteLine(line);
                        }
                        else writer.WriteLine("");
                    }
                }
            }
        }
    }
}
