﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Reflection;
//using System.Collections;
//using System.Windows;
//using System.IO;
//using System.Collections.ObjectModel;
//using CeresBase.IO.Serialization.PostgreSQL;
//using System.Runtime.Serialization;
//using CeresBase.IO.Serialization.Translation;
//using System.ComponentModel;
//using System.Collections.Specialized;
//using System.Configuration;
//using DBManager;
//using MesosoftCommon.Threading;
//using System.Windows.Threading;
//using Npgsql;
////using NHibernatePoweredDB;

//namespace CeresBase.IO.Serialization
//{
//    public static class SerializationCentral
//    {
//        //"Server={0};Port={1};" +
//        //            "User Id={2};Password={3};Database={4};",
//        //            hostName, "5432", "postgres",
//        //            "postgres", "ceresdb"

//        public class DbDescription
//        {
//            public string Name { get; set; }
//            public string Description { get; set; }

//            public override string ToString()
//            {
//                return Description;
//            }
//        }


//        private static ObservableCollection<DbDescription> availableDatabases = new ObservableCollection<DbDescription>();
//        public static ObservableCollection<DbDescription> AvailableDatabases
//        {
//            get
//            {
//                return ThreadSafeBinding.BindToManager<DbDescription>(Dispatcher.CurrentDispatcher, "SerializationCentral");
//            }
//        }

//        public static string[] BuiltInDBSs { get { return new string[] { "postgres", "ceresdb" }; } }

//        public static DbDescription CurrentDatabase
//        {
//            get
//            {
//                if (currentDatabase == null) return null;
//                return new DbDescription() { Name = currentDatabase.DatabaseName, Description = currentDatabase.DisplayName };
//            }
//        }

//        private static User currentUser;
//        public static User  CurrentUser
//        {
//            get
//            {
//                return currentUser;
//            }
//            set
//            {
//                currentUser = value;
//                EventHandler handle = UserChanged;
//                if (handle != null)
//                {
//                    handle(null, new EventArgs());
//                }
//            }
//        }


//        public static void MergeOldDBIntoList(string target, string source)
//        {
//            currentDatabase.MergeTableIntoCollection(target, source);

//        }

//        public static event EventHandler UserChanged;

//        static SerializationCentral()
//        {
//         //   Test.Testing();

//            ThreadSafeBinding.RegisterCollection<DbDescription>(availableDatabases, "SerializationCentral");

//                DBProxy.DBProxySingleton.Instance.UpdateEvent += new EventHandler<DBProxy.DBEventArgs>(Instance_UpdateEvent);

//            currentDatabase = new SerializationDatabase();

//            if (Application.Current == null)
//                System.Windows.Forms.Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
//            else
//                Application.Current.Exit += new ExitEventHandler(Current_Exit);

//            string[] dbs = PostgreSQLDBEngine.ListDBNames(PostgreSQLDBEngine.UtilityConnection);
//            foreach (string db in dbs)
//            {
//                string desc = PostgreSQLDBEngine.GetDatabaseComment(db, PostgreSQLDBEngine.UtilityConnection);
//                if (desc == "ERROR DB NOT FOUND")
//                    desc = db;
//                availableDatabases.Add(new DbDescription() { Name = db, Description = desc });
//            }
//        }

//        static void Instance_UpdateEvent(object sender, DBProxy.DBEventArgs e)
//        {
//            if (e.ClientSource == DBProxy.DBProxySingleton.Instance.ID)
//                return;

//            switch (e.Action)
//            {
//                case DBProxy.DBAction.DBAdded:
//                    string[] split = e.Args.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
//                    string name = split[0];
                    
//                    string desc = "";
//                    if (split.Length > 1)
//                    {
//                        desc = split[1];
//                    }
//                    if (!availableDatabases.Any(db => db.Name == name.ToLower()))
//                    {
//                        AddNewDatabase(name, desc);
//                    }
//                    break;
//            }
//        }

//        private static void newDatabaseGuts(string name, string description)
//        {
//            if (availableDatabases.Any(db => db.Name == name))
//            {
//                throw new InvalidOperationException("This database already exists.");
//            }

//            PostgreSQLDBEngine.CreateDatabase(name, PostgreSQLDBEngine.UtilityConnection);
//            PostgreSQLDBEngine.SetDatabaseComment(name, description, PostgreSQLDBEngine.UtilityConnection);


//            availableDatabases.Add(new DbDescription() { Name = name, Description = description });
//        }

//        public static void AddNewDatabase(string name, string description)
//        {
//            newDatabaseGuts(name, description);

//            string[] knownDbs;
//            string[] knownDesc;
//            DBProxy.DBProxySingleton.Instance.GetDBs(out knownDbs, out knownDesc);

//            if (knownDbs != null)
//            {
//                if (knownDbs.Contains(name))
//                {
//                    string hostaddress = DBProxy.DBProxySingleton.Instance.GetHostConnection(name);

//                    using (NpgsqlConnection conn = PostgreSQLDBEngine.ConnectToDB(hostaddress))
//                    {
//                        string dbaddress = String.Format("Server={0};Port={1};" +
//                            "User Id={2};Password={3};Database={4};",
//                            "localhost", PostgreSQLDBEngine.DefaultPort, "postgres",
//                            "postgres", name);
//                        using (NpgsqlConnection newConn = PostgreSQLDBEngine.ConnectToDB(dbaddress))
//                        {


//                           // PostgreSQLDBEngine.SynchronizeFullDatabases(conn, newConn);
//                            //PostgreSQLDBEngine.MassMerge(conn, newConn);
//                        }
//                    }
//                }
//                else
//                {
//                    DBProxy.DBProxySingleton.Instance.AddDatabase(name, description);
//                }
//            }
//        }

//        public static void CommitDatabase()
//        {
//            if (currentDatabase == null) return;

//            foreach (MemberInfo info in registeredCollections)
//            {
//                ICollection collection = null;

//                if (info.MemberType == MemberTypes.Field)
//                {
//                    collection = (info as FieldInfo).GetValue(null) as ICollection;
//                }
//                else if (info.MemberType == MemberTypes.Property)
//                {
//                    collection = (info as PropertyInfo).GetValue(null, null) as ICollection;
//                }

//                if(collection.Count > 0)
//                    currentDatabase.WriteObject(collection, info.DeclaringType + "." + info.Name, false);
//            }

//            foreach (MemberInfo info in delayedCollections)
//            {
//                ICollection collection = null;

//                if (info.MemberType == MemberTypes.Field)
//                {
//                    collection = (info as FieldInfo).GetValue(null) as ICollection;
//                }
//                else if (info.MemberType == MemberTypes.Property)
//                {
//                    collection = (info as PropertyInfo).GetValue(null, null) as ICollection;
//                }

//                currentDatabase.WriteObject(collection, info.DeclaringType + "." + info.Name, true);       
//            }

//            mergeDatabase();

            
//        }

//        private static void mergeDatabase()
//        {
//            string hostaddress = DBProxy.DBProxySingleton.Instance.GetHostConnection(currentDatabase.DatabaseName);

//            if (hostaddress != null)
//            {
//                try
//                {
//                    using (NpgsqlConnection conn = PostgreSQLDBEngine.ConnectToDB(hostaddress))
//                    {
//                        DateTime time = DateTime.Now;
//                        //currentDatabase.MergeWithDB(conn);
//                        TimeSpan span = DateTime.Now.Subtract(time);
//                    }
//                    DBProxy.DBProxySingleton.Instance.DatabaseHasChanged(currentDatabase.DatabaseName);
//                }
//                catch (Exception ex)
//                {
//                    string full = ex.Message;
//                    if (ex.InnerException == null)
//                    {
//                        full += "\n" + ex.StackTrace;
//                    }
//                    else
//                    {
//                        full += "\nInner " + ex.InnerException.Message;
//                        full += "\n" + ex.InnerException.StackTrace;
//                    }
//                    MessageBox.Show("There was an error in the merge \n" + full);
//                }
//                finally
//                {

//                    DBProxy.DBProxySingleton.Instance.Disconnect();
//                }
//            }
//        }

//        static void Application_ApplicationExit(object sender, EventArgs e)
//        {
//            Exit();

//        }

//        static void Current_Exit(object sender, ExitEventArgs e)
//        {
//            Exit();

//        }

//        public static void Exit()
//        {
//            try
//            {
//                CommitDatabase();



//                if (currentDatabase != null)
//                {
//                    currentDatabase.Dispose();
//                }

//                NpgsqlConnection.ClearAllPools();

//            }
//            catch (Exception ex)
//            {
//                string full = ex.Message;
//                if (ex.InnerException == null)
//                {
//                    full += "\n" + ex.StackTrace;
//                }
//                else
//                {
//                    full += "\nInner " + ex.InnerException.Message;
//                    full += "\n" + ex.InnerException.StackTrace;
//                }
//                MessageBox.Show("There was an error shutting down \n" + full);
//            }
//            finally
//            {

//                DBProxy.DBProxySingleton.Instance.Disconnect();
//            }
//        }


//        private static List<MemberInfo> registeredCollections = new List<MemberInfo>();
//        public static T RegisterCollection<T>(Type type, string member) 
//            where T:class, ICollection
//        {
//            MemberInfo[] infos = type.GetMember(member, MemberTypes.Property | MemberTypes.Field, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
//            if (infos == null || infos.Length == 0)
//                throw new InvalidOperationException("Member not found. Either the member name is wrong or it's not static. Only static collections can be registered.");
             
             
//            if (!registeredCollections.Any(m => m.DeclaringType == type && m.Name == member))
//            {
//               registeredCollections.Add(infos[0]);
//            }

//            T toRet = currentDatabase.GetObject<T>(type + "." + infos[0].Name, false);
//            if (toRet == null) toRet = Activator.CreateInstance<T>();

//            SetUpCollection(toRet);

//            return toRet;
//        }

//        private static Dictionary<object, int> counts = new Dictionary<object, int>();
//        private static Dictionary<Type, PropertyInfo[]> props = new Dictionary<Type, PropertyInfo[]>();

//        #region Do Tracking

//        public static void SetUpCollection(ICollection collection)
//        {
//            if (collection is INotifyCollectionChanged)
//            {
//                (collection as INotifyCollectionChanged).CollectionChanged += new NotifyCollectionChangedEventHandler(SerializationCentral_CollectionChanged);
//            }

//            if (collection is IDictionary)
//            {
//                IDictionary dict = collection as IDictionary;
//                foreach (object key in dict.Keys)
//                {
//                    if (key is INotifyCollectionChanged)
//                    {
//                        (key as INotifyCollectionChanged).CollectionChanged += new NotifyCollectionChangedEventHandler(SerializationCentral_CollectionChanged);
//                    }
//                    else if (key is INotifyPropertyChanged)
//                    {
//                        setPropertyChanged(key as INotifyPropertyChanged);
//                    }

//                    if (key is ICollection)
//                    {
//                        foreach (object obj in (key as ICollection))
//                        {
//                            if (obj is INotifyPropertyChanged)
//                            {
//                                setPropertyChanged(obj as INotifyPropertyChanged);
//                            }
//                        }
//                    }

//                    if (dict[key] is INotifyCollectionChanged)
//                    {
//                        (dict[key] as INotifyCollectionChanged).CollectionChanged += new NotifyCollectionChangedEventHandler(SerializationCentral_CollectionChanged);
//                    }
//                    else if (dict[key] is INotifyPropertyChanged)
//                    {
//                        setPropertyChanged(dict[key] as INotifyPropertyChanged);
//                    }

//                    if (dict[key] is ICollection)
//                    {
//                        foreach (object obj in (dict[key] as ICollection))
//                        {
//                            if (obj is INotifyPropertyChanged)
//                            {
//                                setPropertyChanged(obj as INotifyPropertyChanged);
//                            }
//                        }
//                    }
//                }
//            }
//        }

//        static void setPropertyChanged(INotifyPropertyChanged obj)
//        {

//                if (!counts.ContainsKey(obj))
//                {
//                    counts.Add(obj, 0);
//                    obj.PropertyChanged += new PropertyChangedEventHandler(SerializationCentral_PropertyChanged);
//                }

//                counts[obj]++;

//                PropertyInfo[] theseProps = null;
//                if (props.ContainsKey(obj.GetType()))
//                {
//                    theseProps = props[obj.GetType()];
//                }
//                else
//                {
//                    theseProps = obj.GetType().GetProperties();
//                    props.Add(obj.GetType(), theseProps);
//                }

//                if (theseProps != null)
//                {
//                    foreach (PropertyInfo info in theseProps)
//                    {

//                        ParameterInfo[] paras = info.GetIndexParameters();
//                        if (paras.Length != 0)
//                            continue;
//                        object val = info.GetValue(obj, null);

//                        if (val is IList)
//                        {
//                            foreach (object item in (val as IList))
//                            {
//                                if (item is INotifyPropertyChanged)
//                                    setPropertyChanged(item as INotifyPropertyChanged);
//                            }
//                        }
//                        else if (val is IDictionary)
//                        {
//                            foreach (object key in (val as IDictionary).Keys)
//                            {
//                                if (key is INotifyPropertyChanged)
//                                    setPropertyChanged(key as INotifyPropertyChanged);
//                            }

//                            foreach (object value in (val as IDictionary).Keys)
//                            {
//                                if (value is INotifyPropertyChanged)
//                                    setPropertyChanged(value as INotifyPropertyChanged);
//                            }
//                        }
//                        else if (val is INotifyPropertyChanged)
//                        {
//                            setPropertyChanged(val as INotifyPropertyChanged);
//                        }
//                    }
//                }
//        }

//        static void removePropertyChanged(INotifyPropertyChanged obj)
//        {
//            if (!counts.ContainsKey(obj)) return;

//            counts[obj]--;

//            if (counts[obj] == 0)
//            {
//                obj.PropertyChanged -= new PropertyChangedEventHandler(SerializationCentral_PropertyChanged);
//            }

//            PropertyInfo[] theseProps = null;
//            if (props.ContainsKey(obj.GetType()))
//            {
//                theseProps = props[obj.GetType()];
//            }
//            else
//            {
//                theseProps = obj.GetType().GetProperties();
//                props.Add(obj.GetType(), theseProps);
//            }

//            foreach (PropertyInfo info in theseProps)
//            {
//                object val = info.GetValue(obj, null);
//                if (val is INotifyPropertyChanged)
//                {
//                    removePropertyChanged(val as INotifyPropertyChanged);
//                }
//            }

//        }

//        static void SerializationCentral_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
//        {
//            if (e.Action == NotifyCollectionChangedAction.Add)
//            {
//                if (e.NewItems != null)
//                {
//                    foreach (object o in e.NewItems)
//                    {
//                        if (o is INotifyPropertyChanged)
//                        {
//                            setPropertyChanged(o as INotifyPropertyChanged);
//                        }
//                    }
//                }
//            }
//            else if (e.Action == NotifyCollectionChangedAction.Remove)
//            {
//                if (e.OldItems != null)
//                {
//                    foreach (object o in e.OldItems)
//                    {
//                        if (o is INotifyPropertyChanged)
//                        {
//                            removePropertyChanged(o as INotifyPropertyChanged);
//                        }
//                    }
//                }
//            }
//            else if (e.Action == NotifyCollectionChangedAction.Reset)
//            {
//                if (e.NewItems != null)
//                {
//                    foreach (object o in e.NewItems)
//                    {
//                        if (o is INotifyPropertyChanged)
//                        {
//                            setPropertyChanged(o as INotifyPropertyChanged);
//                        }
//                    }
//                }

//                if (e.OldItems != null)
//                {
//                    foreach (object o in e.OldItems)
//                    {
//                        if (o is INotifyPropertyChanged)
//                        {
//                            removePropertyChanged(o as INotifyPropertyChanged);
//                        }
//                    }
//                }
//            }
//        }

//        static void SerializationCentral_PropertyChanged(object sender, PropertyChangedEventArgs e)
//        {
//            SerializedObjectManager.FlagUpdate(sender, true);
//        }

//        #endregion

//        private class delayedStruct
//        {
//            public MemberInfo Info { get; set; }
//           // public MethodInfo Method { get; set; }            
//        }

//        public static TimeSpan tRegisterDelayedCollection = new TimeSpan();

//        private static List<MemberInfo> delayedCollections = new List<MemberInfo>();
//        //public static T RegisterDelayedCollection<T>(Type type, string member)
//        //    where T : class, ICollection
//        //{
//        //    DateTime now = DateTime.Now;
//        //    MemberInfo[] infos = type.GetMember(member, MemberTypes.Property | MemberTypes.Field, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
//        //    if (infos == null || infos.Length == 0)
//        //        throw new InvalidOperationException("Member not found. Either the member name is wrong or it's not static. Only static collections can be registered.");


//        //    if (!delayedCollections.Any(m => m.DeclaringType == type && m.Name == member))
//        //    {
//        //        //delayedStruct delay = new delayedStruct();
//        //        //delay.Info = infos[0];
//        //        //if(filterFunction != null)
//        //        //{
//        //        //    delay.Method = filterFunction.Method;
//        //        //}
//        //        delayedCollections.Add(infos[0]);
//        //    }

//        //    T collection = Activator.CreateInstance<T>();
//        //        T toRet = currentDatabase.GetObject<T>(type + "." + infos[0].Name, false);

//        //        if (toRet != null)
//        //        {
//        //            if (collection is IList)
//        //            {
//        //                foreach (object o in toRet)
//        //                {
//        //                    (collection as IList).Add(o);
//        //                }
//        //            }
//        //            else if (collection is IDictionary)
//        //            {
//        //                foreach (object key in (toRet as IDictionary).Keys)
//        //                {
//        //                    (collection as IDictionary).Add(key, (toRet as IDictionary)[key]);
//        //                }
//        //            }
//        //        }


//        //        //currentDatabase.SwitchEntry(type + "." + infos[0].Name, collection);


//        //    tRegisterDelayedCollection = tRegisterDelayedCollection.Add(DateTime.Now.Subtract(now));



//        //    SetUpCollection(collection);

//        //    return collection;
//        //}

//        public static void DeleteFromCollection(Type type, string member, IList toDel)
//        {
//            MemberInfo info = delayedCollections.SingleOrDefault(m => m.DeclaringType == type && m.Name == member);
//            if (info == null) return;

//            ICollection collection = null;

//            if (info.MemberType == MemberTypes.Field)
//            {
//                collection = (info as FieldInfo).GetValue(null) as ICollection;
//            }
//            else if (info.MemberType == MemberTypes.Property)
//            {
//                collection = (info as PropertyInfo).GetValue(null, null) as ICollection;
//            }

//            if (collection == null) return;

//            currentDatabase.DeleteStuff(type + "." + info.Name, toDel);
//        }

//        public static void UpdateDelayedCollection(Type type, string member)
//        {
//            MemberInfo info = delayedCollections.SingleOrDefault(m => m.DeclaringType == type && m.Name == member);
//            if (info == null) return;

//            updateDelayedGuts(info);
//        }

//        private static void updateDelayedGuts(MemberInfo info)
//        {
//            ICollection collection = null;

//            if (info.MemberType == MemberTypes.Field)
//            {
//                collection = (info as FieldInfo).GetValue(null) as ICollection;
//            }
//            else if (info.MemberType == MemberTypes.Property)
//            {
//                collection = (info as PropertyInfo).GetValue(null, null) as ICollection;
//            }

//            if (collection == null) return;

//            MethodInfo getObject = currentDatabase.GetType().GetMethod("GetObject", BindingFlags.Public | BindingFlags.Instance);
//            ICollection updatedCollection = MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod(getObject, new Type[] { collection.GetType() }, currentDatabase, new object[] { info.DeclaringType + "." + info.Name, true }) as ICollection;

//            if (updatedCollection == null) return;


//            //currentDatabase.SwitchEntry(info.DeclaringType + "." + info.Name, collection);


//            if (updatedCollection is IList)
//            {
//                foreach (object obj in updatedCollection)
//                {
//                    if (!(collection as IList).Contains(obj))
//                    {
//                        (collection as IList).Add(obj);
//                    }
//                }

//                ////Remove any objects not in the collection
//                //foreach (object obj in collection)
//                //{
//                //    if (!(updatedCollection as IList).Contains(obj))
//                //    {
//                //        (collection as IList).Remove(obj);
//                //    }
//                //}
//            }
//            else if (updatedCollection is IDictionary)
//            {
//                foreach (object obj in (updatedCollection as IDictionary).Keys)
//                {
//                    if (!(collection as IDictionary).Contains(obj))
//                    {
//                        (collection as IDictionary).Add(obj, (updatedCollection as IDictionary)[obj]);
//                    }
//                }
//            }
//        }

//        //public static void UpdateDelayedCollection<T>(Type type, string member, Func<T, bool> filterFunction)
//        //{

//        //}

//        public static event DatabaseChangingEventHandler DatabaseChanging;
//        public static event EventHandler DatabaseChanged;

//        private static SerializationDatabase currentDatabase;

//        public static void ReloadDatabase()
//        {
//            ChangeDatabase(currentDatabase.DatabaseName);
//        }

//        public static void ChangeDatabase(string name)
//        {
//            ChangeDatabase("localhost", PostgreSQLDBEngine.DefaultPort, "postgres", "postgres", name);
//        }

//        private static object getObject(object oldObj, object newObj)
//        {
//            if (oldObj.GetType() != newObj.GetType()) return newObj;

//            if (oldObj is IList && !(oldObj is Array))
//            {
//                IList oldList = oldObj as IList;
//                oldList.Clear();

//                IList newList = newObj as IList;
//                foreach (object o in newList)
//                {
//                    oldList.Add(o);
//                }

//                return oldList;

//            }
//            else if (oldObj is IDictionary)
//            {
//                IDictionary oldDict = oldObj as IDictionary;
//                IDictionary newDict = newObj as IDictionary;

//                object[] allKeys = new object[oldDict.Keys.Count];
//                oldDict.Keys.CopyTo(allKeys, 0);

//                foreach (object key in allKeys)
//                {
                    
//                    if (!newDict.Contains(key))
//                    {
//                        oldDict.Remove(key);
//                    }
//                }

//                foreach (object key in newDict.Keys)
//                {
//                    if (newDict.Contains(key))
//                    {
//                        newDict[key] = getObject(newDict[key], newDict[key]);
//                    }
//                    else
//                    {
//                        oldDict.Add(key, newDict[key]);
//                    }
//                }
//            }

//            return newObj;
//        }

//        public static bool ChangeDatabase(string hostName, string port, string userId, string password, string database)
//        {
//            //if (!GroupUserCentral.HasAccess(userId, password, database))
//            //{
//            //    throw new InvalidOperationException("The user does not have access to the database");
//            //}

//            if (currentDatabase != null)
//            {
//                if (currentDatabase.DatabaseName != "ceresdb") CommitDatabase();
//                currentDatabase.Dispose();
//            }

//            DatabaseChangingEventArgs args = new DatabaseChangingEventArgs(currentDatabase.DisplayName);
//            DatabaseChangingEventHandler handler = DatabaseChanging;
//            if (handler != null)
//            {
//                handler(null, args);
//            }

//            if (args.Cancel) return false;


//            string desc = availableDatabases.Single(odb => odb.Name == database).Description;

//            PostgreSQLDBEngine.ClearTables();
//            SerializedObjectManager.ClearSerializedObjectManager();
//            RelationalFormatter.ClearRelationalFormatter();


//            SerializationDatabase db = new SerializationDatabase(hostName, port, userId, password, database, desc);
//            currentDatabase = db;

//            mergeDatabase();

//            MemberInfo[] priorRegistered = registeredCollections.ToArray();

//            foreach (MemberInfo info in priorRegistered)
//            {
//                #region Registered

//                ICollection collection = null;

//                if (info.MemberType == MemberTypes.Field)
//                {
//                    collection = (info as FieldInfo).GetValue(null) as ICollection;
//                }
//                else if (info.MemberType == MemberTypes.Property)
//                {
//                    collection = (info as PropertyInfo).GetValue(null, null) as ICollection;
//                }

//                if (collection == null)
//                    throw new InvalidOperationException("There is an error, the collection " + info.Name + " in " + info.DeclaringType + " was not found");

//                MethodInfo method = db.GetType().GetMethod("GetObject", BindingFlags.Instance | BindingFlags.Public);

//                if (collection is IDictionary)
//                {
//         //           (collection as IDictionary).Clear();
//                    IDictionary dict = MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod(method, new Type[] { collection.GetType() }, db, new object[] { info.DeclaringType + "." + info.Name, true })
//                        as IDictionary;
//                    if (dict != null)
//                    {
//                        object[] allKeys = new object[(collection as IDictionary).Keys.Count];
//                        (collection as IDictionary).Keys.CopyTo(allKeys, 0);
//                        foreach (object key in allKeys)
//                        {
//                            if (!dict.Contains(key))
//                            {
//                                (collection as IDictionary).Remove(key);
//                            }
//                        }

//                        foreach (object key in dict.Keys)
//                        {
//                            if ((collection as IDictionary).Contains(key))
//                            {
//                                (collection as IDictionary)[key] = getObject((collection as IDictionary)[key], dict[key]);
//                            }
//                            else
//                            {
//                                (collection as IDictionary).Add(key, dict[key]);
//                            }
//                        }

//                        db.SwitchEntry(info.DeclaringType + "." + info.Name, dict);
//                    }
//                }
//                else if (collection is IList)
//                {
//                    (collection as IList).Clear();

//                    IList list = MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod(method, new Type[] { collection.GetType() }, db, new object[] { info.DeclaringType + "." + info.Name, true }) 
//                        as IList;

//                    //IList list = db.GetObject(info.DeclaringType + "." + info.Name) as IList;
//                    if (list != null)
//                    {
//                        foreach (object obj in list)
//                        {
//                            (collection as IList).Add(obj);
//                        }

//                        db.SwitchEntry(info.DeclaringType + "." + info.Name, collection);

//                    }
//                }

//                #endregion
//            }


//            foreach (MemberInfo info in delayedCollections)
//            {
//                updateDelayedGuts(info);
//            }
            
//            EventHandler changed = DatabaseChanged;

//            if (changed != null)
//            {
//                changed(db, new EventArgs());
//            }

//            return true;
//        }



//    }
//}
