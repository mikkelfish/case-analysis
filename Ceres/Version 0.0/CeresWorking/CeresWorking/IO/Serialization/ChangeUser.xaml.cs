﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DBManager;

namespace CeresBase.IO.Serialization
{
    /// <summary>
    /// Interaction logic for ChangeUser.xaml
    /// </summary>
    public partial class ChangeUser : Window
    {
        public ChangeUser()
        {
            InitializeComponent();

            this.Title = "Change User From " + (NewSerializationCentral.CurrentUser != null ? NewSerializationCentral.CurrentUser.Name : "None");
            this.Loaded += new RoutedEventHandler(ChangeUser_Loaded);
        }

        void ChangeUser_Loaded(object sender, RoutedEventArgs e)
        {
            this.userName.Focus();
        }

        private void changeUser(object sender, RoutedEventArgs e)
        {
            doChange();
        }

        private void doChange()
        {
            User user = GroupUserCentral.Users.SingleOrDefault(u => u.Name.ToLower() == this.userName.Text.Trim().ToLower());
            if (user == null)
            {
                MessageBox.Show("The user does not exist.");
                this.userName.Text = "";
                this.password.Password = "";
                this.userName.Focus();
                return;
            }

            if (user.Password != this.password.Password)
            {
                MessageBox.Show("The password does not match what's in the system");
                this.password.Password = "";
                this.password.Focus();
                return;
            }

            NewSerializationCentral.CurrentUser = user;
            this.Close();
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return || e.Key == Key.Enter)
            {
                doChange();
            }
        }

        private void create(object sender, RoutedEventArgs e)
        {
             User user = GroupUserCentral.Users.SingleOrDefault(u => u.Name.ToLower() == this.userName.Text.Trim().ToLower());
             if (user != null)
             {
                 MessageBox.Show("This user already exists in the system.");
                 this.userName.Text = "";
                 this.password.Password = "";
                 this.userName.Focus();
                 return;
             }

             User newUser = new User() { Name = this.userName.Text, Password = this.password.Password };
             GroupUserCentral.Users.Add(newUser);
             GroupUserCentral.WriteAndSynch();
             NewSerializationCentral.CurrentUser = newUser;
             this.Close();
        }
    }
}
