﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CeresBase.IO.Serialization
{
    /// <summary>
    /// Interaction logic for DatabaseSwitch.xaml
    /// </summary>
    public partial class DatabaseSwitch : Window
    {
        public DatabaseSwitch()
        {
            InitializeComponent();

            NewSerializationCentral.UserChanged += new EventHandler(SerializationCentral_UserChanged);
        }

        void SerializationCentral_UserChanged(object sender, EventArgs e)
        {
            ListCollectionView source = this.comboSwitch.ItemsSource as ListCollectionView;
            source.Refresh();
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            string item = (e.Item as string);
            //if (NewSerializationCentral.BuiltInDBSs.Contains(item))
            //{
            //    e.Accepted = false;
            //}
            //else 
            if (!NewSerializationCentral.CurrentUser.SuperUser)
            {
                GroupUserCentral.DBPermission perm = GroupUserCentral.GetUserPermission(NewSerializationCentral.CurrentUser.Name, NewSerializationCentral.CurrentUser.Password,
                    item);
               if (perm == GroupUserCentral.DBPermission.None)
               {
                   e.Accepted = false;
               }
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void switchButton_Click(object sender, RoutedEventArgs e)
        {
            string sel = (this.comboSwitch.SelectedItem  as string);
            if (sel == null || sel == NewSerializationCentral.CurrentDatabase.DatabaseName) return;

            NewSerializationCentral.ChangeDatabase(sel);
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.dbText.Text == "")
            {
                MessageBox.Show("Please enter a name for the db");
                return;
            }

            string description = this.dbText.Text.Trim();
            string name = description.ToLower().Replace("'", "").Replace(".", "").Replace("\\", "").Replace("/", "");


            if (NewSerializationCentral.AvailableDatabases.Any(db => db == name))
            {
                MessageBox.Show("This db already exists. Enter a new name");
                return;
            }

            GroupUserCentral.SetUserPermission(NewSerializationCentral.CurrentUser.Name, NewSerializationCentral.CurrentUser.Password,
                name, GroupUserCentral.DBPermission.Administrator);

           // NewSerializationCentral.AddNewDatabase(name, description);
            NewSerializationCentral.ChangeDatabase(name);
            this.Close();

        }
    }
}
