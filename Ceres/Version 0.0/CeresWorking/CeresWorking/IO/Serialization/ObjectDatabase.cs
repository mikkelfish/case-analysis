﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Db4objects.Db4o;
using Db4objects.Db4o.Config;
using System.IO;

namespace CeresBase.IO.Serialization
{
    public class ObjectDatabase : IDisposable
    {
        public class DatabaseDescription
        {
            public string DatabaseName { get; private set; }
            public string DisplayName { get; private set; }
            public DatabaseDescription(string databaseName, string displayName)
            {
                this.DatabaseName = databaseName;
                this.DisplayName = displayName;
            }

            public override string ToString()
            {
                return this.DisplayName;
            }
        }

        private class databaseEntry
        {
            public string Name { get; private set; }
            public object Object { get; set; }
            public databaseEntry(string name, object obj)
            {
                this.Name = name;
                this.Object = obj;
            }
        }

        public DatabaseDescription Description { get; private set; }
        private IObjectContainer DatabaseContainer { get; set; }

        private static readonly string DBDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\analysis dbs\\";

        public ObjectDatabase(string databaseName)
        {
            IEmbeddedConfiguration config = Db4oEmbedded.NewConfiguration();
            config.Common.UpdateDepth = 20;
            config.Common.ActivationDepth = 20;
            
            

            if (!Directory.Exists(DBDirectory))
                Directory.CreateDirectory(DBDirectory);

            string dbPath = DBDirectory + databaseName + ".objd";

            this.DatabaseContainer = Db4oEmbedded.OpenFile(config, dbPath);
           IObjectSet set = this.DatabaseContainer.QueryByExample(typeof(DatabaseDescription));

           if (set.Count == 0)
           {
               this.Description = new DatabaseDescription(databaseName, databaseName);
               this.DatabaseContainer.Store(this.Description);
           }
           else this.Description = set.Next() as DatabaseDescription;
        }

        //private ObjectDatabase()
        //{

        //}

        //public static ObjectDatabase CreateNewDatabase(string databaseName, string displayName)
        //{
        //    ObjectDatabase toRet = new ObjectDatabase();
        //    IEmbeddedConfiguration config = Db4oEmbedded.NewConfiguration();
        //    config.Common.UpdateDepth = 20;
        //    config.Common.ActivationDepth = 20;


        //    if (!Directory.Exists(DBDirectory))
        //        Directory.CreateDirectory(DBDirectory);

        //    string dbPath = DBDirectory + databaseName + ".objd";
        //    toRet.DatabaseContainer = Db4oEmbedded.OpenFile(config, dbPath);
        //    toRet.Description = new DatabaseDescription(databaseName, displayName);
        //    toRet.DatabaseContainer.Store(toRet.Description);
        //    return toRet;
        //}

        public object GetObject(string name)
        {
            databaseEntry entry = this.DatabaseContainer.Query<databaseEntry>(o => o.Name == name).SingleOrDefault();
            return entry == null ? null : entry.Object;
        }

        public void UpdateCollection(string name, object obj)
        {
            databaseEntry entry = this.DatabaseContainer.Query<databaseEntry>(o => o.Name == name).SingleOrDefault();
            if (entry == null)
            {
                entry = new databaseEntry(name, obj);
            }

            if (entry.Object != null && !ReferenceEquals(entry.Object, obj))
            {
                this.DatabaseContainer.Delete(entry.Object);
            }

            entry.Object = obj;

            this.DatabaseContainer.Store(entry);
        }

        public void UpdateObject(object obj)
        {
            this.DatabaseContainer.Store(obj);
        }

        public void DeleteObject(object obj)
        {
           // databaseEntry entry = this.DatabaseContainer.Query<databaseEntry>(o => o.Name == name).Single();
            this.DatabaseContainer.Delete(obj);
           // this.DatabaseContainer.Store(entry);
        }


        public void Dispose()
        {
            if (this.DatabaseContainer != null)
            {
                this.DatabaseContainer.Dispose();
                this.DatabaseContainer = null;
            }
        }

        public static string[] GetDatabases()
        {
            if (!Directory.Exists(DBDirectory))
                return new string[0];
            return Directory.GetFiles(DBDirectory, "*.objd").Select(s => s.Substring(s.LastIndexOf('\\') + 1).Replace(".objd","")).ToArray();
        }
    }
}
