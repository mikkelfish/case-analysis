﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.PostgreSQL;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;

namespace CeresBase.IO.Serialization
{
    public class SerializationDatabase : IDisposable
    {
        private interface ISerializationWrapper
        {
            string Name { get; set; }
            object Object { get; set; }
        }

        private class serializationWrapper<T> : ISerializationWrapper
        {
            public string Name { get; set; }
            public T Object { get; set; }

            object ISerializationWrapper.Object 
            {
                get
                {
                    return this.Object;
                }

                set
                {
                    this.Object = (T)value;
                }
            }
        }

        private List<ISerializationWrapper> wrapperObjects = new List<ISerializationWrapper>();


        public string DatabaseName { get; private set; }
        public string DisplayName { get; set; }

        private PostgreSQLStream stream;

        public void MergeWithDB(Npgsql.NpgsqlConnection otherDB)
        {
            PostgreSQLDBEngine.SynchronizeFullDatabases(otherDB, this.stream.Connection);
            //PostgreSQLDBEngine.MassMerge(this.stream.Connection, otherDB);
            //PostgreSQLDBEngine.MassMerge(otherDB, this.stream.Connection);
        }

        public void MergeTableIntoCollection(string target, string source)
        {
            PostgreSQLDBEngine.EpochTranslatorTest(target, source, this.stream.Connection);
        }

        public SerializationDatabase()
        {
            this.stream = new PostgreSQLStream();            
            this.DatabaseName = this.stream.Connection.Database;
            this.DisplayName = "Utility Database";
        }


        public SerializationDatabase(string hostName, string port, string userId, string password, string database, string description)
        {
            this.stream = new PostgreSQLStream(PostgreSQL.PostgreSQLDBEngine.ConnectToDB(hostName, port, userId, password, database));
            this.DatabaseName = database;
            this.DisplayName = description;

            string id = PostgreSQLDBEngine.GetComputerName(this.stream.Connection);
            if (id == null)
            {
                PostgreSQLDBEngine.SetComputerName(PostgreSQLDBEngine.GetComputerName(PostgreSQLDBEngine.UtilityConnection), this.stream.Connection);
            }
        }

        public T GetObject<T>(string name, bool forceRefresh) where T : class
        {
            T[] found = this.GetObjects<T>(name, forceRefresh);
            if (found == null) return null;
            return found[0];
        }

        public T[] GetObjects<T>(string name, bool forceRefresh) where T : class
        {
            RelationalFormatter formatter = new RelationalFormatter();


            this.stream.Filter = new RelationalFilter(typeof(serializationWrapper<T>), new Condition("Name", Comparison.Equal, name)) { ForceRefresh = forceRefresh  };
            object[] objs = formatter.Deserialize(this.stream);
            if (objs == null || objs.Length == 0) return null;
            foreach (ISerializationWrapper wrapper in objs)
            {
                ISerializationWrapper foundWrapper = wrapperObjects.SingleOrDefault(wr => wr.Name == wrapper.Name);
                if (foundWrapper == null)
                {
                    wrapperObjects.Add(wrapper);
                }
                else if (forceRefresh)
                {
                    wrapperObjects.Remove(foundWrapper);
                    wrapperObjects.Add(wrapper);
                }
            }
            
            return objs.Cast<serializationWrapper<T>>().Select(obj => obj.Object).ToArray();
        }

        public bool HasEntry(string name)
        {
            return wrapperObjects.Any(wr => wr.Name == name);
        }

        public void SwitchEntry(string name, object val)
        {
            ISerializationWrapper wrapper = wrapperObjects.SingleOrDefault(wr => wr.Name == name);
            if (wrapper != null)
            {
                SerializedObjectManager.UpdateRegistration(wrapper.Object, val);
            }
        }


        public void UpdateEntry(string name, bool addTo)
        {
            ISerializationWrapper wrapper = wrapperObjects.SingleOrDefault(wr => wr.Name == name);
            if (wrapper != null)
            {
                this.WriteObject(wrapper.Object, name, addTo);
            }
        }

        public void DeleteStuff(string name, IList itemsToDelete)
        {
            ISerializationWrapper wrapper = wrapperObjects.SingleOrDefault(wr => wr.Name == name);
            if (wrapper == null) return;
            if (wrapper.Object == null) return;

            MessageBox.Show("Items are being deleted");

            RelationalFormatter formatter = new RelationalFormatter();
            this.stream.Filter = new RelationalFilter() { DeleteObjects = true };

            if (wrapper.Object is IList)
            {
                IList source = wrapper.Object as IList;
                List<object> temp = new List<object>();
                foreach (object obj in source)
                {
                    if (!itemsToDelete.Contains(obj))
                    {
                        temp.Add(obj);
                    }
                }

                source.Clear();
                foreach (object obj in itemsToDelete)
                {
                    source.Add(obj);
                }

                formatter.Serialize(this.stream, wrapper);

                source.Clear();
                foreach (object obj in temp)
                {
                    source.Add(obj);
                }
            }
            else if (wrapper.Object is IDictionary)
            {
                
            }
        }

        public void WriteObject(object obj, string name, bool addTo)
        {
            RelationalFormatter formatter = new RelationalFormatter();

            ISerializationWrapper wrapper = wrapperObjects.SingleOrDefault(wr => wr.Name == name);
            if (wrapper == null)
            {
                Type t = typeof(serializationWrapper<>).MakeGenericType(obj.GetType());
                wrapper = t.InvokeMember("", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.CreateInstance,
                    null, null, null) as ISerializationWrapper;
            }

            this.stream.Filter = new RelationalFilter() { CompareAndUpdateCollections = addTo };

            wrapper.Name = name;

            if (!addTo || wrapper.Object == null)
            {
                wrapper.Object = obj;
            }
            else
            {
                if (wrapper.Object is IList)
                {
                    IList source = obj as IList;
                    IList target = wrapper.Object as IList;
                    foreach (object item in source)
                    {
                        if (!target.Contains(item))
                            target.Add(item);
                    }
                }
                else if (wrapper.Object is IDictionary)
                {
                    IDictionary source = obj as IDictionary;
                    IDictionary target = wrapper.Object as IDictionary;
                    foreach (object key in source.Keys)
                    {
                        if (!target.Contains(key))
                            target.Add(key, source[key]);
                        else
                        {
                            target[key] = source[key];
                        }
                    }
                }

            }
            formatter.Serialize(this.stream, wrapper);
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (this.stream != null)
            {
                this.stream.Dispose();
            }
        }

        #endregion
    }
}
