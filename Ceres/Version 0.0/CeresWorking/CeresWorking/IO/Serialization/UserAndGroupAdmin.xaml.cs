﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DBManager;
using System.Collections.ObjectModel;

namespace CeresBase.IO.Serialization
{
    /// <summary>
    /// Interaction logic for UserAndGroupAdmin.xaml
    /// </summary>
    public partial class UserAndGroupAdmin : Window
    {
        public ObservableCollection<User> Users
        {
            get
            {
                return GroupUserCentral.Users;
            }
        }

        public ObservableCollection<Group> Groups
        {
            get
            {
                return GroupUserCentral.Groups;
            }
        }

        public ObservableCollection<string> Databases
        {
            get
            {
                return NewSerializationCentral.AvailableDatabases;
            }
        }

        public UserAndGroupAdmin()
        {
            InitializeComponent();
            this.layoutForUser();
            NewSerializationCentral.UserChanged += new EventHandler(SerializationCentral_UserChanged);
        }



        public bool CanEditUser
        {
            get { return (bool)GetValue(CanEditUserProperty); }
            set { SetValue(CanEditUserProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanEditUser.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanEditUserProperty =
            DependencyProperty.Register("CanEditUser", typeof(bool), typeof(UserAndGroupAdmin));



        public User CurrentUserInstance
        {
            get { return (User)GetValue(CurrentUserInstanceProperty); }
            set { SetValue(CurrentUserInstanceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurrentUserInstance.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentUserInstanceProperty =
            DependencyProperty.Register("CurrentUserInstance", typeof(User), typeof(UserAndGroupAdmin));



        void SerializationCentral_UserChanged(object sender, EventArgs e)
        {
            layoutForUser();
        }

        private void layoutForUser()
        {
            #region Database Admin

            bool remove = true;
            var sel = this.Databases.Select(d => d);
            foreach (string db in sel)
            {

                GroupUserCentral.DBPermission perm = GroupUserCentral.GetUserPermission(NewSerializationCentral.CurrentUser.Name, NewSerializationCentral.CurrentUser.Password,
                    db);
                if (perm == GroupUserCentral.DBPermission.Administrator)
                {
                    remove = false;
                    break;
                }
            }

            if (remove)
            {
                if(this.tabControl.Items.Contains(this.dbManagementTab))
                    this.tabControl.Items.Remove(this.dbManagementTab);
            }
            else
            {
                if (!this.tabControl.Items.Contains(this.dbManagementTab))
                    this.tabControl.Items.Insert(0, this.dbManagementTab);

                this.tabControl.SelectedItem = this.dbManagementTab;   
            }

            ListCollectionView source = this.dbBox.ItemsSource as ListCollectionView;
            source.Refresh();

            #endregion

            #region Group admin
            ListCollectionView dropdownSource = this.groupDropdown.ItemsSource as ListCollectionView;
            dropdownSource.Refresh();

            remove = !NewSerializationCentral.CurrentUser.SuperUser;
            foreach (Group group in this.Groups)
            {
                if (group.Administrators.Any(gr => gr.Name == NewSerializationCentral.CurrentUser.Name))
                {
                    remove = false;
                    break;
                }
            }

            if (remove)
            {
                if (this.tabControl.Items.Contains(this.groupManagementTab))
                    this.tabControl.Items.Remove(this.groupManagementTab);
            }
            else
            {
                if (!this.tabControl.Items.Contains(this.groupManagementTab))
                {
                    if (!this.tabControl.Items.Contains(this.dbManagementTab))
                    {
                        this.tabControl.Items.Insert(0, this.groupManagementTab);
                        this.tabControl.SelectedItem = this.groupManagementTab;
                    }
                    else
                    {
                        this.tabControl.Items.Insert(1, this.groupManagementTab);
                    }
                }

                
            }

            #endregion

            #region Group add
            if (NewSerializationCentral.CurrentUser.SuperUser)
            {
                this.groupGrid.RowDefinitions[2].Height = new GridLength(30, GridUnitType.Pixel);
            }
            else
            {
                this.groupGrid.RowDefinitions[2].Height = new GridLength(0, GridUnitType.Pixel);
            }

            #endregion

            #region User control
            setCanEdit();


            #endregion
        }

       

        #region Database management stuff

        private void reloadUserGroup()
        {
            object o = this.dbBox.SelectedItem;
            this.dbBox.SelectedItem = null;
            this.dbBox.SelectedItem = o;
        }

        private void giveAdminAccess(object sender, RoutedEventArgs e)
        {
            if (this.dbBox.SelectedItem == null) return;

            foreach (User user in this.userBox.SelectedItems)
            {
                GroupUserCentral.SetUserPermission(user.Name, user.Password, (this.dbBox.SelectedItem as string),
                    GroupUserCentral.DBPermission.Administrator);
            }

            foreach (Group group in this.groupBox.SelectedItems)
            {
                GroupUserCentral.SetGroupPermission(group.Name, group.Password, (this.dbBox.SelectedItem as string),
                    GroupUserCentral.DBPermission.Administrator);
            }

            this.userBox.SelectedItems.Clear();
            this.groupBox.SelectedItems.Clear();

            reloadUserGroup();
        }

        private void giveBasicAccess(object sender, RoutedEventArgs e)
        {
            if (this.dbBox.SelectedItem == null) return;

            foreach (User user in this.userBox.SelectedItems)
            {
                GroupUserCentral.SetUserPermission(user.Name, user.Password, (this.dbBox.SelectedItem as string),
                    GroupUserCentral.DBPermission.User);
            }

            foreach (Group group in this.groupBox.SelectedItems)
            {
                GroupUserCentral.SetGroupPermission(group.Name, group.Password, (this.dbBox.SelectedItem as string),
                    GroupUserCentral.DBPermission.User);
            }

            this.userBox.SelectedItems.Clear();
            this.groupBox.SelectedItems.Clear();


            reloadUserGroup();
        }

        private void revokeAccess(object sender, RoutedEventArgs e)
        {
            if (this.dbBox.SelectedItem == null) return;

            foreach (User user in this.userBox.SelectedItems)
            {
                GroupUserCentral.SetUserPermission(user.Name, user.Password, (this.dbBox.SelectedItem as string),
                    GroupUserCentral.DBPermission.None);
            }

            foreach (Group group in this.groupBox.SelectedItems)
            {
                GroupUserCentral.SetGroupPermission(group.Name, group.Password, (this.dbBox.SelectedItem as string),
                    GroupUserCentral.DBPermission.None);
            }

            this.userBox.SelectedItems.Clear();
            this.groupBox.SelectedItems.Clear();


            reloadUserGroup();
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            string db = e.Item as string;

            //if (NewSerializationCentral.BuiltInDBSs.Contains(db.Name))
            //{
            //    e.Accepted = false;
            //}
            //else 
            if (!NewSerializationCentral.CurrentUser.SuperUser)
            {

                GroupUserCentral.DBPermission perm = GroupUserCentral.GetUserPermission(NewSerializationCentral.CurrentUser.Name, NewSerializationCentral.CurrentUser.Password,
                   db);
               if (perm != GroupUserCentral.DBPermission.Administrator)
               {
                   e.Accepted = false;
               }
            }
        }

        #endregion

        #region Group admin stuff

        private void userBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.groupBox.SelectedItems.Clear();
        }

        private void groupBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.userBox.SelectedItems.Clear();
        }

        private void CollectionViewSource_Filter_1(object sender, FilterEventArgs e)
        {
            if (NewSerializationCentral.CurrentUser.SuperUser) return;

            Group group = e.Item as Group;
            if (!group.Administrators.Any(a => a.Name == NewSerializationCentral.CurrentUser.Name))
            {
                e.Accepted = false;
            }
        }

        private void addMember(object sender, RoutedEventArgs e)
        {
            Group group = this.groupDropdown.SelectedItem as Group;
            if (group == null) return;

            foreach (User user in this.allUsers.SelectedItems)
            {
                if (!group.Users.Contains(user))
                {
                    group.Users.Add(user);
                }
            }

            GroupUserCentral.WriteAndSynch();

        }

        private void addAdmin(object sender, RoutedEventArgs e)
        {
            Group group = this.groupDropdown.SelectedItem as Group;
            if (group == null) return;

            foreach (User user in this.allUsers.SelectedItems)
            {
                if (!group.Administrators.Contains(user))
                {
                    group.Administrators.Add(user);
                }
            }

            GroupUserCentral.WriteAndSynch();
        }

        private void removeFromMemberOrAdmin(object sender, RoutedEventArgs e)
        {
            Group group = this.groupDropdown.SelectedItem as Group;
            if (group == null) return;

            var admins = this.adminBox.SelectedItems.Cast<User>().ToArray();
            foreach (var admin in admins)
            {
                group.Administrators.Remove(admin);
            }

            var users = this.memberBox.SelectedItems.Cast<User>().ToArray();
            foreach (var user in users)
            {
                group.Users.Remove(user);
            }

            GroupUserCentral.WriteAndSynch();

        }

        private void memberBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.adminBox.SelectedItems.Clear();
        }

        private void adminBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.memberBox.SelectedItems.Clear();
        }

        #endregion

        #region Add Group
        private void addGroup(object sender, RoutedEventArgs e)
        {
            Group g = new Group() { Name = this.addGroupTextBox.Text };
            if (this.Groups.Any(group => group.Name == g.Name))
            {
                MessageBox.Show("This group already exists.");
                return;
            }

            this.Groups.Add(g);
        }

        #endregion

        #region User functions

        private void selectedUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            makeInstance();
            this.setCanEdit();
        }

        private void makeInstance()
        {

            User user = this.selectedUser.SelectedItem as User;
            if (user == null) return;


            User instance = new User();
            instance.Email = user.Email;
            instance.Institution = user.Institution;
            instance.Lab = user.Lab;
            instance.Name = user.Name;
            instance.Password = user.Password;
            instance.SuperUser = user.SuperUser;
            if (this.passwordBox != null)  this.passwordBox.Password = instance.Password;

            this.CurrentUserInstance = instance;
        }

        private void revertChanges(object sender, RoutedEventArgs e)
        {
            makeInstance();
        }

        private void saveChanges(object sender, RoutedEventArgs e)
        {
            User user = this.selectedUser.SelectedItem as User;
            if (user == null) return;

            user.Email = this.CurrentUserInstance.Email;
            user.Institution = this.CurrentUserInstance.Institution;
            user.Lab = this.CurrentUserInstance.Lab;
            user.Password = this.passwordBox.Password;
            user.SuperUser = this.CurrentUserInstance.SuperUser;

            GroupUserCentral.WriteAndSynch();
        }

        private void setCanEdit()
        {
            if (NewSerializationCentral.CurrentUser.SuperUser)
            {
                this.CanEditUser = true;
            }
            else
            {
                User selUser = this.selectedUser.SelectedItem as User;
                if (selUser == NewSerializationCentral.CurrentUser)
                {
                    this.CanEditUser = true;
                }
                else this.CanEditUser = false;
            }
        }

        private void CollectionViewSource_Filter_2(object sender, FilterEventArgs e)
        {
            User user = e.Item as User ;
            if (user == null) return;
            if (user.SuperUser) e.Accepted = false;
        }

        #endregion


    }
}
