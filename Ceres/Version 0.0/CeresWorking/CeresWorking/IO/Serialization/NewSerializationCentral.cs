﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using MesosoftCommon.Threading;
using System.Windows.Threading;
using Db4objects.Db4o;
using Sharpen.IO;
using System.IO;
using System.Windows;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;

namespace CeresBase.IO.Serialization
{
    public static class NewSerializationCentral
    {
        public static bool DBIsChanging { get; private set; }

        private static ObjectDatabase objectDatabase;
        public static ObjectDatabase.DatabaseDescription CurrentDatabase
        {
            get
            {
                if (objectDatabase == null) return null;
                return objectDatabase.Description;
            }
        }

        private static ObservableCollection<string> availableDatabases = new ObservableCollection<string>();
        public static ObservableCollection<string> AvailableDatabases
        {
            get
            {
                return ThreadSafeBinding.BindToManager<string>(Dispatcher.CurrentDispatcher, "NewSerializationCentral");
            }
        }

       // public static string[] BuiltInDBSs { get { return new string[] { "Default" }; } }

        public static event EventHandler UserChanged;

        private static DBManager.User currentUser;
        public static DBManager.User CurrentUser
        {
            get
            {
                return currentUser;
            }
            set
            {
                currentUser = value;
                EventHandler handle = UserChanged;
                if (handle != null)
                {
                    handle(null, new EventArgs());
                }
            }
        }

        static NewSerializationCentral()
        {
            ThreadSafeBinding.RegisterCollection<string>(availableDatabases, "NewSerializationCentral");
            string[] dbs = ObjectDatabase.GetDatabases();
            foreach (string db in dbs)
            {
                availableDatabases.Add(db);
            }

            NewSerializationCentral.objectDatabase = new ObjectDatabase("default");

            if (Application.Current == null)
                System.Windows.Forms.Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
            else
                Application.Current.Exit += new ExitEventHandler(Current_Exit);


        }

        public static void Exit()
        {
            CommitDatabase();
            NewSerializationCentral.objectDatabase.Dispose();
        }

        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            Exit();
        }

        static void Current_Exit(object sender, ExitEventArgs e)
        {
            Exit();
        }


        public static event DatabaseChangingEventHandler DatabaseChanging;
        public static event EventHandler DatabaseChanged;

        //public static void AddNewDatabase(string name, string description)
        //{
        //    if (availableDatabases.Contains(name)) throw new InvalidOperationException("This database " + name + " already exists");

        //   // NewSerializationCentral.objectDatabase.Dispose();
        //   // NewSerializationCentral.objectDatabase = 
        //        ObjectDatabase.CreateNewDatabase(name, description);
        //}

        public static void ReloadDatabase()
        {
            ChangeDatabase(CurrentDatabase.DatabaseName);
        }

        private static object getObject(object oldObj, object newObj)
        {
            if (oldObj.GetType() != newObj.GetType()) return newObj;

            if (oldObj is IList && !(oldObj is Array))
            {
                IList oldList = oldObj as IList;
                oldList.Clear();

                IList newList = newObj as IList;
                foreach (object o in newList)
                {
                    oldList.Add(o);
                }

                return oldList;

            }
            else if (oldObj is IDictionary)
            {
                IDictionary oldDict = oldObj as IDictionary;
                IDictionary newDict = newObj as IDictionary;

                object[] allKeys = new object[oldDict.Keys.Count];
                oldDict.Keys.CopyTo(allKeys, 0);

                foreach (object key in allKeys)
                {

                    if (!newDict.Contains(key))
                    {
                        oldDict.Remove(key);
                    }
                }

                foreach (object key in newDict.Keys)
                {
                    if (newDict.Contains(key))
                    {
                        newDict[key] = getObject(newDict[key], newDict[key]);
                    }
                    else
                    {
                        oldDict.Add(key, newDict[key]);
                    }
                }
            }

            return newObj;
        }


        public static void CommitDatabase()
        {
            if (objectDatabase == null) return;

            foreach (MemberInfo info in registeredCollections)
            {
                ICollection collection = null;

                if (info.MemberType == MemberTypes.Field)
                {
                    collection = (info as FieldInfo).GetValue(null) as ICollection;
                }
                else if (info.MemberType == MemberTypes.Property)
                {
                    collection = (info as PropertyInfo).GetValue(null, null) as ICollection;
                }

               // if (collection.Count > 0)
                    objectDatabase.UpdateCollection(info.DeclaringType + "." + info.Name, collection);
            }
        }

        public static void ChangeDatabase(string name)
        {
            DBIsChanging = true;
            if (objectDatabase != null)
            {
               CommitDatabase();
                objectDatabase.Dispose();
            }

            DatabaseChangingEventArgs args = new DatabaseChangingEventArgs(objectDatabase.Description.DisplayName);
            DatabaseChangingEventHandler handler = DatabaseChanging;
            if (handler != null)
            {
                handler(null, args);
            }

            if (args.Cancel)
            {
                DBIsChanging = false;
                return;
            }


            objectDatabase = new ObjectDatabase(name);

            MemberInfo[] priorRegistered = registeredCollections.ToArray();
            foreach (MemberInfo info in priorRegistered)
            {
                #region Registered

                ICollection collection = null;

                if (info.MemberType == MemberTypes.Field)
                {
                    collection = (info as FieldInfo).GetValue(null) as ICollection;
                }
                else if (info.MemberType == MemberTypes.Property)
                {
                    collection = (info as PropertyInfo).GetValue(null, null) as ICollection;
                }

                if (collection == null)
                    throw new InvalidOperationException("There is an error, the collection " + info.Name + " in " + info.DeclaringType + " was not found");

                if (collection is IDictionary)
                {
                    IDictionary dict = objectDatabase.GetObject(info.DeclaringType + "." + info.Name) as IDictionary;
                    if (dict != null)
                    {
                        object[] allKeys = new object[(collection as IDictionary).Keys.Count];
                        (collection as IDictionary).Keys.CopyTo(allKeys, 0);
                        foreach (object key in allKeys)
                        {
                            if (!dict.Contains(key))
                            {
                                (collection as IDictionary).Remove(key);
                            }
                        }

                        foreach (object key in dict.Keys)
                        {
                            if ((collection as IDictionary).Contains(key))
                            {
                                (collection as IDictionary)[key] = getObject((collection as IDictionary)[key], dict[key]);
                            }
                            else
                            {
                                (collection as IDictionary).Add(key, dict[key]);
                            }
                        }
                    }
                }
                else if (collection is IList)
                {
                    (collection as IList).Clear();

                    IList list = objectDatabase.GetObject(info.DeclaringType + "." + info.Name) as IList;

                    if (list != null)
                    {
                        foreach (object obj in list)
                        {
                            (collection as IList).Add(obj);
                        }
                    }
                }

                #endregion
            }

            
            EventHandler changed = DatabaseChanged;

            if (changed != null)
            {
                changed(objectDatabase, new EventArgs());
            }

            DBIsChanging = false;

            return;
        
        }


        private static List<MemberInfo> registeredCollections = new List<MemberInfo>();
        public static T RegisterCollection<T>(Type type, string member)
            where T : class, ICollection
        {
            MemberInfo[] infos = type.GetMember(member, MemberTypes.Property | MemberTypes.Field, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            if (infos == null || infos.Length == 0)
                throw new InvalidOperationException("Member not found. Either the member name is wrong or it's not static. Only static collections can be registered.");


            if (!registeredCollections.Any(m => m.DeclaringType == type && m.Name == member))
            {
                registeredCollections.Add(infos[0]);
            }

            T toRet = objectDatabase.GetObject(type + "." + infos[0].Name) as T;
            if (toRet == null) toRet = Activator.CreateInstance<T>();

            SetUpCollection(toRet);

            return toRet;
        }

        private static Dictionary<object, int> counts = new Dictionary<object, int>();
        private static Dictionary<Type, PropertyInfo[]> props = new Dictionary<Type, PropertyInfo[]>();

        #region Do Tracking

        public static void SetUpCollection(ICollection collection)
        {
            if (collection is INotifyCollectionChanged)
            {
                (collection as INotifyCollectionChanged).CollectionChanged += new NotifyCollectionChangedEventHandler(SerializationCentral_CollectionChanged);
            }

            if (collection is IDictionary)
            {
                IDictionary dict = collection as IDictionary;
                foreach (object key in dict.Keys)
                {
                    if (key is INotifyCollectionChanged)
                    {
                        (key as INotifyCollectionChanged).CollectionChanged += new NotifyCollectionChangedEventHandler(SerializationCentral_CollectionChanged);
                    }
                    else if (key is INotifyPropertyChanged)
                    {
                        setPropertyChanged(key as INotifyPropertyChanged);
                    }

                    if (key is ICollection)
                    {
                        foreach (object obj in (key as ICollection))
                        {
                            if (obj is INotifyPropertyChanged)
                            {
                                setPropertyChanged(obj as INotifyPropertyChanged);
                            }
                        }
                    }

                    if (dict[key] is INotifyCollectionChanged)
                    {
                        (dict[key] as INotifyCollectionChanged).CollectionChanged += new NotifyCollectionChangedEventHandler(SerializationCentral_CollectionChanged);
                    }
                    else if (dict[key] is INotifyPropertyChanged)
                    {
                        setPropertyChanged(dict[key] as INotifyPropertyChanged);
                    }

                    if (dict[key] is ICollection)
                    {
                        foreach (object obj in (dict[key] as ICollection))
                        {
                            if (obj is INotifyPropertyChanged)
                            {
                                setPropertyChanged(obj as INotifyPropertyChanged);
                            }
                        }
                    }
                }
            }
        }

        static void setPropertyChanged(INotifyPropertyChanged obj)
        {

            if (!counts.ContainsKey(obj))
            {
                counts.Add(obj, 0);
                obj.PropertyChanged += new PropertyChangedEventHandler(SerializationCentral_PropertyChanged);
            }

            counts[obj]++;

            PropertyInfo[] theseProps = null;
            if (props.ContainsKey(obj.GetType()))
            {
                theseProps = props[obj.GetType()];
            }
            else
            {
                theseProps = obj.GetType().GetProperties();
                props.Add(obj.GetType(), theseProps);
            }

            if (theseProps != null)
            {
                foreach (PropertyInfo info in theseProps)
                {

                    ParameterInfo[] paras = info.GetIndexParameters();
                    if (paras.Length != 0)
                        continue;
                    object val = info.GetValue(obj, null);

                    if (val is IList)
                    {
                        foreach (object item in (val as IList))
                        {
                            if (item is INotifyPropertyChanged)
                                setPropertyChanged(item as INotifyPropertyChanged);
                        }
                    }
                    else if (val is IDictionary)
                    {
                        foreach (object key in (val as IDictionary).Keys)
                        {
                            if (key is INotifyPropertyChanged)
                                setPropertyChanged(key as INotifyPropertyChanged);
                        }

                        foreach (object value in (val as IDictionary).Keys)
                        {
                            if (value is INotifyPropertyChanged)
                                setPropertyChanged(value as INotifyPropertyChanged);
                        }
                    }
                    else if (val is INotifyPropertyChanged)
                    {
                        setPropertyChanged(val as INotifyPropertyChanged);
                    }
                }
            }
        }

        static void removePropertyChanged(INotifyPropertyChanged obj)
        {
            if (!counts.ContainsKey(obj)) return;

            counts[obj]--;

            if (counts[obj] == 0)
            {
                obj.PropertyChanged -= new PropertyChangedEventHandler(SerializationCentral_PropertyChanged);
            }

            PropertyInfo[] theseProps = null;
            if (props.ContainsKey(obj.GetType()))
            {
                theseProps = props[obj.GetType()];
            }
            else
            {
                theseProps = obj.GetType().GetProperties();
                props.Add(obj.GetType(), theseProps);
            }

            foreach (PropertyInfo info in theseProps)
            {
                object val = info.GetValue(obj, null);
                if (val is INotifyPropertyChanged)
                {
                    removePropertyChanged(val as INotifyPropertyChanged);
                }
            }

        }

        static void SerializationCentral_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (DBIsChanging) return;

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (e.NewItems != null)
                {
                    foreach (object o in e.NewItems)
                    {
                        if (o is INotifyPropertyChanged)
                        {
                            setPropertyChanged(o as INotifyPropertyChanged);
                        }

                        objectDatabase.UpdateObject(o);
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (e.OldItems != null)
                {
                    foreach (object o in e.OldItems)
                    {
                        if (o is INotifyPropertyChanged)
                        {
                            removePropertyChanged(o as INotifyPropertyChanged);
                        }

                        objectDatabase.DeleteObject(o);
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                if (e.NewItems != null)
                {
                    foreach (object o in e.NewItems)
                    {
                        if (o is INotifyPropertyChanged)
                        {
                            setPropertyChanged(o as INotifyPropertyChanged);
                        }
                    }
                }

                if (e.OldItems != null)
                {
                    foreach (object o in e.OldItems)
                    {
                        if (o is INotifyPropertyChanged)
                        {
                            removePropertyChanged(o as INotifyPropertyChanged);
                        }

                      //  objectDatabase.DeleteObject(o);

                    }
                }
            }
        }

        static void SerializationCentral_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            objectDatabase.UpdateObject(sender);
        }

        #endregion
    }
}
