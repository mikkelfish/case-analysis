﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;
using System.Runtime.Serialization;

namespace CeresBase.IO.Serialization.Translators
{
    public class StringTranslator : ITranslator
    {
        public StringTranslator()
        {

        }

        public object Clone()
        {
            return new StringTranslator();
        }

        #region ITranslator Members

        public new object AttachedObject
        {
            get;
            set;
        }

        public bool SupportsType(Type t)
        {
            if (t == typeof(string)) return true;
            return false;
        }

        public double Version
        {
            get { return 1.0; }
        }

        #endregion

        private StringTranslator(SerializationInfo info, StreamingContext context)
        {
            string val = info.GetValue("stringval", typeof(string)) as string;

            if (val != null)
            {
                val = val.Replace("$BACKSLASH$", "\\").Replace("$COLON$", ":").Replace("$SLASH$", "/").Replace("$QUOTE$", "'");
            }

            this.AttachedObject = val;
        }

        #region ISerializable Members

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            string toConvert = this.AttachedObject as string;
            if (toConvert == null) return;

            toConvert = toConvert.Replace("\\", "$BACKSLASH$").Replace(":", "$COLON$").Replace("/", "$SLASH$").Replace("'", "$QUOTE$");

            info.AddValue("stringval", toConvert);
        }

        public bool Supports(object obj)
        {
            if (obj is string)
            {
                string toConvert = obj as string;
                if (toConvert.Contains("\\") || toConvert.Contains(":") || toConvert.Contains("/") || toConvert.Contains("'")) return true;
            }

            return false;
        }

        #endregion
    }
}
