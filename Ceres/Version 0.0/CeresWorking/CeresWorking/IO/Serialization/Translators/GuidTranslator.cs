﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;

namespace CeresBase.IO.Serialization.Translators
{
    class GuidTranslator : ITranslator
    {
        #region ITranslator Members

        public object AttachedObject
        {
            get;
            set;
        }

        public bool SupportsType(Type t)
        {
            if (t == typeof(Guid)) return true;
            return false;
        }

        public double Version
        {
            get { return 0.0; }
        }

        public bool Supports(object obj)
        {
            return obj is Guid;
        }

        #endregion

        #region ISerializable Members

        public GuidTranslator()
        {

        }

        protected GuidTranslator(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            string guid = info.GetValue("thevalue", typeof(string)) as string;
            guid = guid.Substring(0, guid.Length - "$DAGUID$".Length);
            this.AttachedObject = new Guid(guid);
        }

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            if (!(AttachedObject is Guid))
                return;

            string guid = AttachedObject.ToString() + "$DAGUID$";
            info.AddValue("thevalue", guid);
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return new GuidTranslator();
        }

        #endregion
    }
}
