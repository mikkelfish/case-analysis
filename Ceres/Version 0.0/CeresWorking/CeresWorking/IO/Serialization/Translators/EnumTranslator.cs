﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;
using System.Runtime.Serialization;

namespace CeresBase.IO.Serialization.Translators
{
    class EnumTranslator : ITranslator
    {
        class enumInfo
        {
            public string Val { get; set; }
            public string Type { get; set; }
        }



        #region ITranslator Members

        public object AttachedObject
        {
            get;
            set;
        }

        public bool SupportsType(Type t)
        {
            if (t.IsEnum) 
                return true;
            return false;
        }

        public double Version
        {
            get { return 0.0; }
        }

        public bool Supports(object obj)
        {
            if (obj is Enum)
            {
                return true;
            }

            return false;
        }

        #endregion

        public EnumTranslator()
        {

        }

        public EnumTranslator(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            if (info.MemberCount == 0) return;

            enumInfo eInfo = info.GetValue("val", typeof(enumInfo)) as enumInfo;

            Type enumType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(eInfo.Type);

            object en = Enum.Parse(enumType, eInfo.Val);
           // object en = enumType.InvokeMember("", System.Reflection.BindingFlags.CreateInstance, null, null, new object[] { eInfo.Val });

            this.AttachedObject = en;
        }

        #region ISerializable Members

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            Enum enumeration = this.AttachedObject as Enum;
            if (enumeration == null) return;

            enumInfo enumInfo = null;

            SerializationInfo passed = context.Context as SerializationInfo;
            if (passed != null)
            {
                enumInfo = passed.GetValue("val", typeof(enumInfo)) as enumInfo;
            }

            if (enumInfo == null)
            {
                enumInfo = new enumInfo();
            }

            enumInfo.Type = enumeration.GetType().FullName;
            enumInfo.Val = enumeration.ToString();

            info.AddValue("val", enumInfo);           
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            EnumTranslator trans = new EnumTranslator();
            return trans;
        }

        #endregion
    }
}
