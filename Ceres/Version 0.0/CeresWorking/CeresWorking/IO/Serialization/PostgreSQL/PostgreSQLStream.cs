﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Npgsql;
using System.Reflection;
using System.Threading;

namespace CeresBase.IO.Serialization.PostgreSQL
{
    public class PostgreSQLStream : Stream, IRelationalStream
    {
        public override bool CanRead
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanSeek
        {
            get { throw new NotImplementedException(); }
        }  

        public override bool CanWrite
        {
            get { throw new NotImplementedException(); }
        }

        public override void Flush()
        {
          
        }

        public override long Length
        {
            get { throw new NotImplementedException(); }
        }

        public override long Position
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public RelationalFilter Filter { get; set; }

        protected override void Dispose(bool disposing)
        {

            base.Dispose(disposing);

            if (conn != null)
            {
                conn.ClearPool();
                conn.Close();
                conn.Dispose();
            }
                
        }

        #region IRelationalStream Members

        private NpgsqlConnection conn = null;
        public NpgsqlConnection Connection
        {
            get
            {
                return this.conn;
            }
            set
            {
                this.conn = value;
            }
        }

        public PostgreSQLStream()
        {
            if (PostgreSQLDBEngine.LastConnectionString == "")
                conn = PostgreSQLDBEngine.ConnectToDefaultDB();
            else
                conn = PostgreSQLDBEngine.ConnectToDB(PostgreSQLDBEngine.LastConnectionString);
        }

        public PostgreSQLStream(NpgsqlConnection connection)
        {
            if (connection == null)
            {
                if (PostgreSQLDBEngine.LastConnectionString == "")
                    conn = PostgreSQLDBEngine.ConnectToDefaultDB();
                else
                    conn = PostgreSQLDBEngine.ConnectToDB(PostgreSQLDBEngine.LastConnectionString);
            }
            else
                this.conn = connection;
        }

        public static TimeSpan tWriteTable_InsertOrUpdate = new TimeSpan();
        public static TimeSpan tTableExists = new TimeSpan();
        public static TimeSpan tHelperTableExists = new TimeSpan();
        public static TimeSpan tCreateTable = new TimeSpan();
        public static TimeSpan tCreateHelperTable = new TimeSpan();

        public static TimeSpan tReadTableOther = new TimeSpan();
        public static TimeSpan tReadTableMain = new TimeSpan();


        public static int tTablesCreated = 0;
        public static int tHTablesCreated = 0;

        public void WriteTables(RelationalTable[] tables)
        {
            //Set the transaction if necessary and check for a connection.
            DateTime subs;
            if (conn == null)
                throw new Exception("Null connection.");

            List<RelationalTable> tablesToCreate = new List<RelationalTable>();
            Dictionary<string, RelationalRow> helperTableSetsToCreate = new Dictionary<string, RelationalRow>();
            List<RelationalRow> helperTablesToCreate = new List<RelationalRow>();

            object[] tableExists = new object[tables.Length];

            //Handle all table existence checks at once... 
            for (int i = 0; i < tables.Length; i++)
            {
                RelationalTable table = tables[i];

                if (table.TableType == typeof(RelationalFormatterHelper))
                {
                    bool[] helperTableArray = new bool[table.Rows.Count];
                    foreach(RelationalRow row in table.Rows.Values)
                    {
                        //RelationalRow row = table.Rows[j];
                        subs = DateTime.Now;
                        string tableName = PostgreSQLDBEngine.GetTableNameFromHelperRow(row, conn);
                        if (!helperTableSetsToCreate.Keys.Contains(tableName) && !PostgreSQLDBEngine.TableExists(tableName, conn))
                            helperTableSetsToCreate.Add(tableName, row);

                        tHelperTableExists = tHelperTableExists.Add(DateTime.Now.Subtract(subs));
                    }
                    tableExists[i] = helperTableArray;
                }
                else
                {
                    subs = DateTime.Now;
                    if (!tablesToCreate.Contains(table) && !PostgreSQLDBEngine.TableExists(table, conn))
                        tablesToCreate.Add(table);
                    tTableExists = tTableExists.Add(DateTime.Now.Subtract(subs));
                }
            }

            Dictionary<string, List<string>> createdTables = new Dictionary<string, List<string>>();

            //...and then all creation requests at once.
            foreach (RelationalTable table in tablesToCreate)
            {
                subs = DateTime.Now;
                string[] strings = PostgreSQLDBEngine.CreateTable(table, conn);
                tCreateTable = tCreateTable.Add(DateTime.Now.Subtract(subs));
                //Convert the basic string-array report into the dictionary format for ease of use
                if (strings.Length > 0)
                {
                    List<string> stringsList = new List<string>();
                    for (int i = 1; i < strings.Length; i++)
                    {
                        stringsList.Add(strings[i]);
                    }
                    createdTables.Add(strings[0], stringsList);
                }
            }

            foreach (RelationalRow row in helperTableSetsToCreate.Values)
            {
                subs = DateTime.Now;
                string[] strings = PostgreSQLDBEngine.CreateHelperTable(row, conn);
                tCreateHelperTable = tCreateHelperTable.Add(DateTime.Now.Subtract(subs));
                //Convert the basic string-array report into the dictionary format for ease of use
                if (strings.Length > 0)
                {
                    List<string> stringsList = new List<string>();
                    for (int i = 1; i < strings.Length; i++)
                    {
                        stringsList.Add(strings[i]);
                    }
                    createdTables.Add(strings[0], stringsList);
                }
            }
            
            ////RelationalFormatterHelper needs special Table-level handling (existence checks and creates)
            //if (table.TableType == typeof(RelationalFormatterHelper))
            //{
            //    foreach (RelationalRow row in table.Rows)
            //    {
            //        subs = DateTime.Now;
            //        bool HTableExists = (!PostgreSQLDBEngine.HelperTableExists(row, conn));
            //        tHelperTableExists = tHelperTableExists.Add(DateTime.Now.Subtract(subs));

            //        if (HTableExists)
            //        {
            //            subs = DateTime.Now;
            //            PostgreSQLDBEngine.CreateHelperTable(row, conn);
            //            tCreateHelperTable = tCreateHelperTable.Add(DateTime.Now.Subtract(subs));
            //            tHTablesCreated++;
            //        }
            //    }
            //}
            //else
            //{
            //    subs = DateTime.Now;
            //    bool TableExists = (!PostgreSQLDBEngine.TableExists(table, conn));
            //    tTableExists = tTableExists.Add(DateTime.Now.Subtract(subs));

            //    if (TableExists)
            //    {
            //        subs = DateTime.Now;
            //        PostgreSQLDBEngine.CreateTable(table, conn);
            //        tCreateTable = tCreateTable.Add(DateTime.Now.Subtract(subs));
            //        tTablesCreated++;
            //    }

            //}

            foreach (RelationalTable table in tables)
            {
                DateTime now = DateTime.Now;
                //Insert/update rows is able to be used universally by both types
                PostgreSQLDBEngine.InsertOrUpdateRows(table, conn, currentTransaction, Filter.CompareAndUpdateCollections, Filter.DeleteObjects, createdTables);
                tWriteTable_InsertOrUpdate = tWriteTable_InsertOrUpdate.Add(DateTime.Now.Subtract(now));                
            }

        }

        public RelationalTable[] ReadTables()
        {
            DateTime now = DateTime.Now;
            List<RelationalTable> ret = new List<RelationalTable>();
            List<RelationalTable> dummyRet = new List<RelationalTable>();

            if (conn == null)
                throw new Exception("Null connection.");

            //Empty filter is a no-no.
            if (this.Filter == null || this.Filter.TargetClass == null ||
                !PostgreSQLDBEngine.TableExists(new RelationalTable() { TableType = this.Filter.TargetClass }, conn))
            {
                return ret.ToArray();
            }

            ////Target class needs to be defined.
            //if (this.Filter.TargetClass == null)
            //{
            //    return ret.ToArray();
            //}

            ////If the table doesn't exist, obviously there's no data to find
            //if (!PostgreSQLDBEngine.TableExists(new RelationalTable() { TableType = this.Filter.TargetClass }, conn))
            //{
            //    return ret.ToArray();
            //}


            now = DateTime.Now;
            //PostgreSQLDBEngine.ReadDataIntoTables(ref ret, this.Filter, conn, currentTransaction, null);

            PostgreSQLDBEngine.ReadDataIntoTablesQuicker(ref ret, this.Filter, conn, currentTransaction, null);
            //PostgreSQLDBEngine.ReadDataIntoTablesQuicker(ref dummyRet, this.Filter, conn, currentTransaction, null);

  
            tReadTableMain = tReadTableMain.Add(DateTime.Now.Subtract(now));

            return ret.ToArray();
        }

        public Type[] SupportedTypes
        {
            get
            {
                List<Type> types = new List<Type>();
                types.Add(typeof(int));
                types.Add(typeof(double));
                types.Add(typeof(float));
                types.Add(typeof(string));
                types.Add(typeof(char));
                types.Add(typeof(bool));
                types.Add(typeof(DateTime));
                types.Add(typeof(Guid));
                types.Add(typeof(IDictionary));
                types.Add(typeof(IList));
                types.Add(typeof(Enum));

                return types.ToArray();
            }
        }

        public bool NativelyStorable(Type type)
        {
            if (type == typeof(int))
            {
                return true;
            }
            else if (type == typeof(double))
            {
                return true;
            }
            else if (type == typeof(float))
            {
                return true;
            }
            else if (type == typeof(string))
            {
                return true;
            }
            else if (type == typeof(char))
            {
                return true;
            }
            else if (type == typeof(bool))
            {
                return true;
            }
            else if (type == typeof(DateTime))
            {
                return true;
            }
            else if (type == typeof(Guid))
            {
                return true;
            }
            else if (type.GetInterfaces().Contains(typeof(IDictionary)))
            {
                return false;
            }
            else if (type.GetInterfaces().Contains(typeof(IList)))
            {
                return false;
            }
            else if (type.IsEnum)
            {
                return false;
            }

            return false;
        }


        private bool TypeIsSupported(Type propertyType, Type[] supportedTypes)
        {
            if (supportedTypes.Contains(propertyType))
                return true;

            Type[] interfaces = propertyType.GetInterfaces();
            foreach (Type thisType in supportedTypes)
            {
                if (interfaces.Contains(thisType))
                    return true;
            }

            return false;
        }

        private static NpgsqlTransaction currentTransaction = null;
        private static AutoResetEvent beginReset = new AutoResetEvent(true);

        static PostgreSQLStream()
        {
            NewSerializationCentral.DatabaseChanging += new DatabaseChangingEventHandler(SerializationCentral_DatabaseChanging);
        }

        private static object lockable = new object();

        static void SerializationCentral_DatabaseChanging(object sender, DatabaseChangingEventArgs e)
        {
            beginReset.WaitOne();
            beginReset.Set();
        }

        public void BeginTransaction()
        {
            beginReset.WaitOne();
            //beginReset.Reset();
            if (currentTransaction != null)
                throw new Exception("Only one transaction in RelationalStream at a time is allowed.");

            currentTransaction = conn.BeginTransaction();
        }

        public void EndTransaction()
        {
            if (currentTransaction == null)
                throw new Exception("Attempt to end non-existant transaction.");

            currentTransaction.Commit();
            currentTransaction = null;
            beginReset.Set();
        }
 
        #endregion
    }
}
