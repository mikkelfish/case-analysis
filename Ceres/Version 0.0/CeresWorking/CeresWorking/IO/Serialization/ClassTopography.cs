﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.Serialization
{
    public class ClassTopography
    {
        private List<JoinChain> Chains = new List<JoinChain>();
        private Dictionary<Type, JoinChain> ChainIndex = new Dictionary<Type, JoinChain>();

        public void AddChain(JoinChain newChain)
        {
            Chains.Add(newChain);

            foreach (Type newType in newChain.Links.Keys)
            {
                ChainIndex.Add(newType, newChain);
            }
        }

        public JoinChain GetChainFromType(Type requestedType)
        {
            JoinChain ret = null;

            if (ChainIndex.Keys.Contains(requestedType))
                ret = ChainIndex[requestedType];

            return ret;
        }

        public ClassTopography(object rootObject)
        {

        }
    }
}
