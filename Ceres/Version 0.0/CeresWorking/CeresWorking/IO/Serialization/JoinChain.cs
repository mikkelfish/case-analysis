﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.Serialization
{
    public class JoinChain
    {
        private Dictionary<Type, List<Type>> links = new Dictionary<Type, List<Type>>();
        /// <summary>
        /// Key is the owning Type, Value is the owned Types.
        /// </summary>
        public Dictionary<Type, List<Type>> Links
        {
            get
            {
                return this.links;
            }
            set
            {
                this.links = value;
            }
        }       
    }
}
