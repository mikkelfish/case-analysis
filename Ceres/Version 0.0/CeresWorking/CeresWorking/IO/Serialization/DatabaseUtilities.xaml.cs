﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace CeresBase.IO.Serialization
{

    /// <summary>
    /// Interaction logic for DatabaseUtilities.xaml
    /// </summary>
    public partial class DatabaseUtilities : Window
    {
        public ObservableCollection<string> Databases
        {
            get
            {
                return NewSerializationCentral.AvailableDatabases;
            }
        }


        public DatabaseUtilities()
        {
            InitializeComponent();
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            string desc = e.Item as string;

            if (GroupUserCentral.GetUserPermission(NewSerializationCentral.CurrentUser.Name, NewSerializationCentral.CurrentUser.Password, desc) !=
                GroupUserCentral.DBPermission.Administrator)
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_1(object sender, FilterEventArgs e)
        {
            string desc = e.Item as string;

            if (desc == (this.sourceCombo.SelectedItem as string))
                e.Accepted = false;

            if (GroupUserCentral.GetUserPermission(NewSerializationCentral.CurrentUser.Name, NewSerializationCentral.CurrentUser.Password, desc) !=
                GroupUserCentral.DBPermission.Administrator)
            {
                e.Accepted = false;
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListCollectionView view = this.targetBox.ItemsSource as ListCollectionView;
            view.Refresh();
        }
    }
}
