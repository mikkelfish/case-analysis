﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using DBManager;

namespace CeresBase.IO.Serialization
{
    [ValueConversion(typeof(User), typeof(string))]
    [ValueConversion(typeof(Group), typeof(string))]
    public class AccessConverter : IMultiValueConverter
    {

        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string db = values[0] as string;
            object value = values[1];
            if (db != null)
            {

                if (value is User)
                {
                    return GroupUserCentral.GetUserPermission((value as User).Name, (value as User).Password, db).ToString();
                }
                else if (value is Group)
                {
                    return GroupUserCentral.GetGroupPermission((value as Group).Name, (value as Group).Password, db).ToString();
                 
                }
            }

            return "None";
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
