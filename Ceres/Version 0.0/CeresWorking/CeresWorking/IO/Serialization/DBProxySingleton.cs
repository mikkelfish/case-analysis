﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DBManager;
using System.IO;
using CeresBase.IO.Serialization.PostgreSQL;
using CeresBase.IO.Serialization;
using Npgsql;
using System.Windows;

namespace DBProxy
{
    public enum DBAction { DBNone, DBAdded, DBRemoved, UserAdded, DBChanged };

    public class DBEventArgs : EventArgs
    {
        public string ClientSource { get; set; }
        public DBAction Action { get; set; }
        public string Args { get; set; }
    }

    [CallbackBehavior(UseSynchronizationContext=false, ConcurrencyMode=ConcurrencyMode.Multiple)]
    public sealed class DBProxySingleton : IDBManagerCallback
    {
        private DBManagerClient client;
        private static object lockable = new object();
        private static DBProxySingleton singleton;
        private User user;
        private string connectionID;

        private string hostAddress;


        public string GetHostConnection(string db)
        {
            if (this.client == null) return null;

            // PostgeSQL-style connection string
            string connstring = String.Format("Server={0};Port={1};" +
                "User Id={2};Password={3};Database={4};",
                hostAddress, PostgreSQLDBEngine.DefaultPort, "postgres",
                "postgres", db);
            return connstring;
        }

        public string ID
        {
            get
            {
                return this.connectionID;
            }
        }
      
        public static DBProxySingleton Instance
        {
            get
            {
                lock (lockable)
                {
                    if (singleton == null)
                    {
                        singleton = new DBProxySingleton();
                    }
                }

                return singleton;
            }
        }

        public void DatabaseHasChanged(string database)
        {
            if (this.client != null)
                this.client.NeedToUpdate(this.connectionID, new Database() { Name = database });
        }

        public void Connect(User user)
        {
            if (this.client == null)
            {
                InstanceContext site = new InstanceContext(this);
                this.client = new DBManagerClient(site);

                this.hostAddress = this.client.Endpoint.Address.Uri.Host;

                try
                {
                    string feedback = this.client.RegisterClient(this.connectionID, user);
                    this.user = user;
                    doConnectionStuff();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("There has been an error connecting to the server\n" + ex.Message);
                }
            }          
        }

        private void doConnectionStuff()
        {
            Database[] databases = this.client.GetDatabases(this.user);
            foreach (Database db in databases)
            {
                if (!NewSerializationCentral.AvailableDatabases.Any(ob => ob == db.Name))
                {
                    NewSerializationCentral.ChangeDatabase(db.Name);
                }
            }

            foreach (string db in NewSerializationCentral.AvailableDatabases)
            {
                if (!databases.Any(d => d.Name == db))
                {
                    this.client.AddDatabase(new Database() { Description = db, Name = db });
                }
            }
        }

        public void Disconnect()
        {
            if (this.client != null)
            {
                try
                {
                    this.client.DeregisterClient(this.connectionID);
                    this.connectionID = null;
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void AddUser(User user)
        {
            if (this.client != null)
                this.client.AddUser(user);
        }

        public void AddDatabase(string name, string description)
        {
            if(this.client != null)
                this.client.AddDatabase(new Database() { Name = name, Description = description });
        }

        public void GetDBs(out string[] dbs, out string[] descs)
        {
            if (this.client != null)
            {
                Database[] rdbs = this.client.GetDatabases(null);
                dbs = rdbs.Select(d => d.Name).ToArray();
                descs = rdbs.Select(d => d.Description).ToArray();
            }
            else
            {
                dbs = null;
                descs = null;
            }
        }

        private DBProxySingleton()
        {
            string id = PostgreSQLDBEngine.GetComputerName(PostgreSQLDBEngine.UtilityConnection);

            if (id == null)
            {
                id = Guid.NewGuid().ToString();
                PostgreSQLDBEngine.SetComputerName(id, PostgreSQLDBEngine.UtilityConnection);
            }

            this.connectionID = id;

            //string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            //path += "Case Western Reserve University\\Apollo";
            //if (!Directory.Exists(path))
            //{
            //    Directory.CreateDirectory(path);
            //}

            //path += "\\ComputerID.txt";

            //if (File.Exists(path))
            //{
            //    using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            //    {
            //        using (StreamReader reader = new StreamReader(stream))
            //        {
            //            connectionID = reader.ReadLine();
            //        }
            //    }
            //}
            //else
            //{
                

            //    connectionID = Guid.NewGuid().ToString();
            //    using (FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            //    {
            //        using (StreamWriter writer = new StreamWriter(stream))
            //        {
            //            writer.WriteLine(connectionID);
            //        }
            //    }
            //}
        }

        #region IDBManagerCallback Members

        public event EventHandler<DBEventArgs> UpdateEvent;

        private void onUpdateEvent(string client, string message, string args)
        {
            EventHandler<DBEventArgs> upev = this.UpdateEvent;
            if (upev != null)
            {
                DBAction action = DBAction.DBNone;
                switch (message)
                {
                    case "DBAdded":
                        action = DBAction.DBAdded;
                        break;

                    case "DBRemoved":
                        action = DBAction.DBRemoved;
                        break;

                    case "UserInfoChanged":
                        string[] split = args.Split(':');
                        if (split[0] == "Added")
                        {
                            action = DBAction.UserAdded;
                            args = split[1];
                        }
                        break;

                    case "DBChanged":
                        action = DBAction.DBChanged;
                        break;
                }

                try
                {

                    upev(this, new DBEventArgs() { ClientSource = client, Action = action, Args = args });
                }
                catch(Exception ex)
                {
                    int s = 0;
                }
            }
        }

        private void onUpdateEventAsync(string client, AsyncCallback callback, object asyncState)
        {

        }


        public void Updated(string client, string message, string args)
        {
            this.onUpdateEvent(client, message, args);
        }

        #endregion
    }
}
