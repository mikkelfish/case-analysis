﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.Utilities.Settings;
using CeresBase.IO.DataBase;
using System.Reflection;
using CeresBase.Projects;

namespace CeresBase.IO.Serialization
{
    public class LegacyMerge
    {
        public static void MergeOldIntoDatabase()
        {
            #region central pool io

            CentralIO.OldMerge();
            EpochCentral.ReadOldEpochs();
            RoutineCentral.OldMerge();


            #endregion 
        }
    }
}
