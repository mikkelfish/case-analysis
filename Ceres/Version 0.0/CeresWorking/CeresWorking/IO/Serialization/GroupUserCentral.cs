﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using DBManager;
using System.ComponentModel;
using CeresBase.IO.Serialization.PostgreSQL;
using Npgsql;
using System.Windows;
using System.Collections;

namespace CeresBase.IO.Serialization
{
    public static class GroupUserCentral
    {
        static GroupUserCentral()
        {

            NewSerializationCentral.DatabaseChanged += new EventHandler(SerializationCentral_DatabaseChanged);
            NewSerializationCentral.SetUpCollection(Users);
            NewSerializationCentral.SetUpCollection(Entries);
            NewSerializationCentral.SetUpCollection(Groups);

            LoadAndSynch();

            if (!users.Any(u => u.Name == "postgres"))
            {
                Users.Add(new User() { Name = "postgres", Password = "postgres", SuperUser = true });
            }

            Users.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Users_CollectionChanged);
            Groups.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Groups_CollectionChanged);

 
        }

        private static object lockable = new object();
        private static bool isLoading = false;

        static void Groups_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (isLoading) return;

            WriteAndSynch();
        }


        static void Users_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (isLoading) return;

            WriteAndSynch();
        }

        public enum DBPermission { Administrator = 0x11, User = 0x01, None = 0x0 };

        public class DBPermissionEntry : INotifyPropertyChanged
        {
            private string dbName;
            public string DBName 
            {
                get
                {
                    return this.dbName;
                }
                set
                {
                    this.dbName = value;
                    this.OnPropertyChanged("DBName");
                }
            }

            private DBPermission perm;
            public DBPermission Permission 
            {
                get
                {
                    return this.perm;
                }
                set
                {
                    this.perm = value;
                    this.OnPropertyChanged("Permission");
                }
            }

            private User user;
            public User User
            {
                get
                {
                    return this.user;
                }
                set
                {
                    this.user = value;
                    this.OnPropertyChanged("User");
                }
            }

            private Group group;
            public Group Group
            {
                get
                {
                    return this.group;
                }
                set
                {
                    this.group = value;
                    this.OnPropertyChanged("Group");
                }
            }


            protected void OnPropertyChanged(string name)
            {
                PropertyChangedEventHandler ev = PropertyChanged;
                if (ev != null)
                {
                    ev(this, new PropertyChangedEventArgs(name));
                }
            }

            #region INotifyPropertyChanged Members

            public event PropertyChangedEventHandler PropertyChanged;

            #endregion
        }


        private static ObservableCollection<DBPermissionEntry> entries = new ObservableCollection<DBPermissionEntry>();
        public static ObservableCollection<DBPermissionEntry> Entries
        {
            get
            {
                return entries;
            }
        }

        private static ObservableCollection<User> users = new ObservableCollection<User>();
        public static ObservableCollection<User> Users
        {
            get
            {
                return users;
            }
        }

        private static ObservableCollection<Group> groups = new ObservableCollection<Group>();
        public static ObservableCollection<Group> Groups
        {
            get
            {
                return groups;
            }
        }

        

        public static bool HasAccess(string user, string password, string db)
        {
            User use = users.SingleOrDefault(u => u.Name == user);
            if (use == null) return false;

            if (use.SuperUser) return true;

            bool toRet = entries.Any(e => e.DBName == db && 
                (e.User != null && e.User.Name == user && e.Permission != DBPermission.None));
            if (toRet) return true;

            Group[] groupsToCheck = groups.Where(g => g.Users.Any(u => u.Name == user)).ToArray();
            if (groupsToCheck.Length == 0) return false;

            toRet = entries.Any(e => e.DBName == db && e.Group != null && 
                (groupsToCheck.Any(g => g.Name == e.Group.Name)));

            return toRet;

            
        }

        public static void SetUserPermission(string user, string password, string db, DBPermission perm)
        {
            User use = users.SingleOrDefault(u => u.Name == user);
            if (use == null) throw new InvalidOperationException("User " + user + " not in system.");

            if (use.Password != password) throw new InvalidOperationException("User password not valid.");

            DBPermissionEntry entry = entries.FirstOrDefault(e => e.DBName == db && (e.User != null && e.User.Name == user));
            if (entry == null)
            {
                entry = new DBPermissionEntry() { DBName = db, User = use, Permission = perm };
                entries.Add(entry);
            }
            else entry.Permission = perm;

            WriteAndSynch();

        }

        public static void SetGroupPermission(string group, string password, string db, DBPermission perm)
        {
            Group use = groups.SingleOrDefault(u => u.Name == group);
            if (use == null) throw new InvalidOperationException("Group " + group + " not in system.");

            if (use.Password != password) throw new InvalidOperationException("Group password not valid.");

            DBPermissionEntry entry = entries.FirstOrDefault(e => e.DBName == db && (e.Group != null && e.Group.Name == group));
            if (entry == null)
            {
                entry = new DBPermissionEntry() { DBName = db, Group = use, Permission = perm };
                entries.Add(entry);
            }
            else entry.Permission = perm;

            WriteAndSynch();
        }

        public static DBPermission GetUserPermission(string user, string password, string db)
        {
            if (db == "localdb") return DBPermission.Administrator;

            User fuser = Users.SingleOrDefault(u => u.Name == user);
            if (fuser == null) throw new Exception("The user " + user + " does not exist");
            if (fuser.SuperUser) return DBPermission.Administrator;

            GroupUserCentral.DBPermissionEntry use = GroupUserCentral.Entries.FirstOrDefault(to => to.DBName == db && 
                to.User != null && to.User.Name == user && to.User.Password == password);
            if (use == null || use.Permission == DBPermission.None)
            {
                var groupsWhereMem = groups.Where(g => g.Users.Any(u => u.Name == user) || g.Administrators.Any(u => u.Name == user));
                DBPermission permission = DBPermission.None;
                foreach (Group group in groupsWhereMem)
                {
                    DBPermission perm = GetGroupPermission(group.Name, group.Password, db);
                    permission |= perm;
                }

                return permission;
            }


            return use.Permission;
        }

        public static DBPermission GetGroupPermission(string group, string password, string db)
        {
            Group gro = Groups.SingleOrDefault(g => g.Name == group);
            if (gro == null) throw new Exception("The group " + group + " does not exist");
            if (gro.SuperUser) return DBPermission.Administrator;


            GroupUserCentral.DBPermissionEntry use = GroupUserCentral.Entries.FirstOrDefault(to => to.DBName == db && 
                to.Group != null && to.Group.Name == group && to.Group.Password == password);
            if (use == null) return DBPermission.None;


            return use.Permission;
        }

        #region Serialization

        private static void synch(NpgsqlConnection thisConnection)
        {
            //string hostaddress = DBProxy.DBProxySingleton.Instance.GetHostConnection("ceresdb");

            //if (hostaddress != null)
            //{
            //    try
            //    {
            //        using (NpgsqlConnection conn = PostgreSQLDBEngine.ConnectToDB(hostaddress))
            //        {
            //            PostgreSQLDBEngine.SynchronizeFullDatabases(conn, thisConnection);                        
            //        }
            //        DBProxy.DBProxySingleton.Instance.DatabaseHasChanged("ceresdb");
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show("User data could not be synched with main database.");
            //    }
            //}
        }

        private static serializationWrapper<ObservableCollection<User>> userSerial;
        private static serializationWrapper<ObservableCollection<Group>> groupSerial;
        private static serializationWrapper<ObservableCollection<DBPermissionEntry>> entriesSerial;

        static void SerializationCentral_DatabaseChanged(object sender, EventArgs e)
        {
            userSerial = null;
            groupSerial = null;
            entriesSerial = null;

            LoadAndSynch();
        }

        public static void LoadAndSynch()
        {
            //lock (lockable)
            //{
            //    isLoading = true;
            //}

            //using (PostgreSQLStream stream = new PostgreSQLStream(PostgreSQL.PostgreSQLDBEngine.ConnectToDB("localhost", PostgreSQLDBEngine.DefaultPort, "postgres", "postgres", "ceresdb")))
            //{
            //    synch(stream.Connection);

            //    RelationalFormatter formatter = new RelationalFormatter();
            //    stream.Filter = new RelationalFilter(typeof(serializationWrapper<ObservableCollection<User>>), new Condition("Name", Comparison.Equal, "Users")) { ForceRefresh = true };

            //    object[] deserialized = formatter.Deserialize(stream);
            //    if (deserialized != null && deserialized.Length > 0)
            //    {
            //        userSerial = deserialized[0] as serializationWrapper<ObservableCollection<User>>;
            //        if (userSerial != null)
            //        {
            //            reLink(userSerial, users);
            //        }
            //    }


            //    stream.Filter = new RelationalFilter(typeof(serializationWrapper<ObservableCollection<Group>>), new Condition("Name", Comparison.Equal, "Groups")) { ForceRefresh = true };                
            //    deserialized = formatter.Deserialize(stream);
            //    if (deserialized != null && deserialized.Length > 0)
            //    {
            //        groupSerial = deserialized[0] as serializationWrapper<ObservableCollection<Group>>;

            //        if (groupSerial != null)
            //        {
            //            reLink(groupSerial, groups);
            //        }
            //    }

            //    stream.Filter = new RelationalFilter(typeof(serializationWrapper<ObservableCollection<DBPermissionEntry>>), new Condition("Name", Comparison.Equal, "Entries")) { ForceRefresh = true };               
            //    deserialized = formatter.Deserialize(stream);
            //    if (deserialized != null && deserialized.Length > 0)
            //    {
            //        entriesSerial = deserialized[0] as serializationWrapper<ObservableCollection<DBPermissionEntry>>;
            //        if (entriesSerial != null)
            //        {
            //            reLink(entriesSerial, entries);
            //        }
            //    }
            //}

            //lock (lockable)
            //{
            //    isLoading = false;
            //}
        }

        private static void reLink(ISerializationWrapper source, IList target)
        {
            if (source.Object == null) return;

            target.Clear();
            foreach (Object o in source.Object as IList)
            {
                target.Add(o);
            }

            SerializedObjectManager.UpdateRegistration(source.Object, target);
            source.Object = target;
        }

        public static void WriteAndSynch()
        {
            //using (PostgreSQLStream stream = new PostgreSQLStream(PostgreSQL.PostgreSQLDBEngine.ConnectToDB("localhost", PostgreSQLDBEngine.DefaultPort, "postgres", "postgres", "ceresdb")))
            //{
            //    stream.Filter = new RelationalFilter() { CompareAndUpdateCollections = false};
            //    RelationalFormatter formatter = new RelationalFormatter();

            //    if(userSerial == null)
            //        userSerial = new serializationWrapper<ObservableCollection<User>>() { Name = "Users", Object = users };
            //    if (userSerial.Object == null)
            //        userSerial.Object = users;
            //    formatter.Serialize(stream, userSerial);

            //    if(groupSerial == null)
            //        groupSerial = new serializationWrapper<ObservableCollection<Group>>() { Name = "Groups", Object = groups };
            //    if (groupSerial.Object == null)
            //        groupSerial.Object = groups;
            //    formatter.Serialize(stream, groupSerial);

            //    if(entriesSerial == null)
            //        entriesSerial = new serializationWrapper<ObservableCollection<DBPermissionEntry>>() { Name = "Entries", Object = entries };
            //    if (entriesSerial.Object == null)
            //        entriesSerial.Object = entries;
            //    formatter.Serialize(stream, entriesSerial);

            //    synch(stream.Connection);
            //}            
        }

        private interface ISerializationWrapper
        {
            string Name { get; set; }
            object Object { get; set; }
        }

        private class serializationWrapper<T> : ISerializationWrapper
        {
            public string Name { get; set; }
            public T Object { get; set; }

            object ISerializationWrapper.Object
            {
                get
                {
                    return this.Object;
                }

                set
                {
                    this.Object = (T)value;
                }
            }
        }

        #endregion
    }
}
