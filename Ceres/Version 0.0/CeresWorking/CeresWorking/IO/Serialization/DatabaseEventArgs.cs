﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.Serialization
{
    public delegate void DatabaseChangingEventHandler(object sender, DatabaseChangingEventArgs e);


    public class DatabaseChangingEventArgs : EventArgs
    {
        public bool Cancel { get; set; }
        public string Database { get; private set; }

        public DatabaseChangingEventArgs(string database)
        {
            this.Database = database;
        }
    }
}
