﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Reflection;
using System.Collections;
using CeresBase.IO.Serialization.Translation;

namespace CeresBase.IO.Serialization
{

        //return new RelationalFormatterHelper() { PropertyName = "DictionaryItems", Value = new object[] { keyObjs.ToArray(), itemsObjs.ToArray() } };
        //}

        //public static RelationalFormatterHelper WriteList(IList list)
        //{
        //    List<object> items = new List<object>();
        //    foreach (object item in list)
        //    {
        //        items.Add(item);
        //    }

        //return new RelationalFormatterHelper() { PropertyName = "ListItems", Value = items.ToArray() };

    public class RelationalFormatterHelper
    {
        private string propertyName;
        public string PropertyName
        {
            get { return propertyName; }
            set { propertyName = value; }
        }

        private object originalReference;
        public object OriginalReference
        {
            get { return originalReference; }
            set { originalReference = value; }
        }

        private object val;
        public object Value
        {
            get { return val; }
            set 
            { 
                val = value;

                //Yank the types out
                List<string> ret = new List<string>();
                if (this.PropertyName == "DictionaryItems")
                {
                    if ((value as Array).Length == 2)
                    {
                        for (int i = 0; i < ((value as Array).GetValue(0) as Array).Length; i++)
                        {
                            string toAdd = ((value as Array).GetValue(0) as Array).GetValue(i).GetType().FullName;
                            toAdd += ":";
                            toAdd += ((value as Array).GetValue(1) as Array).GetValue(i).GetType().FullName;
                            ret.Add(toAdd);
                        }
                    }
                    //ret.Add(((value as Array).GetValue(0) as Array).GetValue(0).GetType().FullName);
                    //ret.Add(((value as Array).GetValue(1) as Array).GetValue(0).GetType().FullName);
                }
                else if (this.PropertyName == "ListItems")
                {
                    foreach (object sub in (value as Array))
                    {
                        ret.Add(sub.GetType().FullName);
                    }
                    //ret.Add((value as Array).GetValue(0).GetType().FullName);
                }

                this.OriginatingTypeNames = ret.ToArray();
            }
        }

        /// <summary>
        /// Used to store the "Originating Type" of a GUID-style item link in the database.
        /// Lists will have one originating type, Dictionaries will have two.
        /// </summary>
        public string[] OriginatingTypeNames { get; set; }

        public static string GetDBTableName()
        {
            return "";
        }

        public static string[] GetDBColumnNames()
        {
            return null;
        }

        public static Type[] GetDBColumnTypes()
        {
            return null;
        }
    }

    public class RelationalFormatter : IFormatter
    {
        private interface IFormatterWrapper
        {
            string Name { get; set; }
            object Object { get; set; }
        }

        private class formatterWrapper<T> : IFormatterWrapper
        {
            public string Name { get; set; }
            public T Object { get; set; }

            object IFormatterWrapper.Object
            {
                get
                {
                    return this.Object;
                }

                set
                {
                    this.Object = (T)value;
                }
            }
        }

        #region IFormatter Members

        object IFormatter.Deserialize(System.IO.Stream serializationStream)
        {
            return this.Deserialize(serializationStream);
        }

        public object[] Deserialize(System.IO.Stream serializationStream)
        {
            if (!(serializationStream is IRelationalStream))
                throw new Exception("Stream passed into the Deserialize method of a RelationalFormatter was not an IRelationalStream.");

            IRelationalStream relationalStream = serializationStream as IRelationalStream;

            Type[] supportedTypes = relationalStream.SupportedTypes;
            if (!(supportedTypes.Contains(typeof(System.Collections.IList)) && supportedTypes.Contains(typeof(System.Collections.IDictionary))))
                throw new Exception("Support for streams that do not support IList and IDictionary types is not implemented.");

            RelationalTable[] tables = relationalStream.ReadTables();

            object[] ret = null;

            //We want to acquire objects that are of the type requested in the stream filter.
            //Other tables are simply sub-tables of the requested type and will be parsed into the object by recursive calls.
            foreach (RelationalTable relTable in tables)
            {
                if (relTable.TableType == relationalStream.Filter.TargetClass)
                {
                    ret = this.readTable(relTable, tables);
                    break;
                }
            }

            return ret;
        }

        private object[] readTable(RelationalTable table, RelationalTable[] tableList)
        {
            List<object> ret = new List<object>();

            foreach (RelationalRow row in table.Rows)
            {
                ret.Add(this.readRow(row, table, tableList, table.TableType));
            }

            return ret.ToArray();
        }

        private object findAndRecurse(RelationalTable[] tableList, Type returnType, Guid newValue)
        {
            object ret = null;

            //Find the GUID in the rows of the other tables.
            foreach (RelationalTable subTable in tableList)
            {
                if (returnType == null || subTable.TableType == returnType || subTable.TableType == typeof(RelationalFormatterHelper))
                {
                    foreach (RelationalRow subRow in subTable.Rows)
                    {
                        if (subRow.RowIdentifier == (Guid)newValue)
                        {
                            ret = this.readRow(subRow, subTable, tableList, returnType);
                            break;
                        }
                    }
                    break;
                }
            }

            return ret;
        }

        private object readRow(RelationalRow row, RelationalTable table, RelationalTable[] tableList, Type returnType)
        {
            if (SerializedObjectManager.IsObjectRegistered(row.RowIdentifier))
            {
                //If the object's already registered, it can be pulled out immediately, without recursing or anything else
                return SerializedObjectManager.RetrieveObject(row.RowIdentifier);
            }
            
            Type targetType = table.TableType;
            object newObject = null;

            if (targetType == typeof(RelationalFormatterHelper))
            {
                newObject = Activator.CreateInstance(returnType);

                if ((string)row["PropertyName"] == "DictionaryItems")
                {
                    IDictionary dict = newObject as IDictionary;
                    object[] keysAndItems = row["Value"] as object[];
                    string[] originalTypeNames = row["OriginatingTypeNames"] as string[];

                    Array keys = keysAndItems[0] as Array;
                    Array items = keysAndItems[1] as Array;

                    for (int i = 0; i < keys.Length; i++)
                    {
                        object newKey = keys.GetValue(i);
                        object newItem = items.GetValue(i);
                        string typeNameString = originalTypeNames[i];
                        Type keyType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeNameString.Split(':')[0]);
                        Type itemType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeNameString.Split(':')[1]);

                        if (newKey.GetType() == typeof(Guid))
                            newKey = this.findAndRecurse(tableList, keyType, (Guid)newKey);
                        if (newItem.GetType() == typeof(Guid))
                            newItem = this.findAndRecurse(tableList, itemType, (Guid)newKey);

                        dict.Add(newKey, newItem);
                    }

                }
                else if ((string)row["PropertyName"] == "ListItems")
                {
                    IList list = newObject as IList;

                    Array items = row["Value"] as Array;
                    
                    string[] originalTypeNames = row["OriginatingTypeNames"] as string[];
                    //string originalTypeNames = row["OriginatingTypeNames"] as string;

                    if (originalTypeNames != null)
                    {
                        for (int i = 0; i < items.Length; i++)
                        {
                            Type originalType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(originalTypeNames[i]);
                            object value = items.GetValue(i);

                            if (value.GetType() == typeof(Guid))
                                value = this.findAndRecurse(tableList, originalType, (Guid)value);

                            list.Add(value);
                        }
                    }
                }
                else
                    throw new Exception("Unexpected helper type in deserialization.");
            }
            else if (targetType.GetInterfaces().Any(t=> t.Name.Contains("ITranslator"))) //TranslatorCentral.ContainsTranslatorForType(graph.GetType())
            {

                ConstructorInfo cons = targetType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public,
                    null,
                    new Type[] { typeof(SerializationInfo), typeof(StreamingContext) },
                    null);

                if (cons == null) throw new Exception("Translator constructor not found in deserialization");

                SerializationInfo info = new SerializationInfo(targetType, new FormatterConverter());
                string[] keyNames = row.GetKeyNames;
                foreach (string keyName in keyNames)
                {
                    object newValue = row[keyName];
                    if (newValue.GetType() == typeof(Guid))
                        newValue = this.findAndRecurse(tableList, null, (Guid)newValue);

                    info.AddValue(keyName, newValue, newValue.GetType());
                }

                ITranslator translator = cons.Invoke(new object[] { info, new StreamingContext() }) as ITranslator;
                newObject = translator.AttachedObject;

            }
            else if (targetType.GetInterfaces().Contains(typeof(ISerializable)))
            {
                SerializationInfo info = new SerializationInfo(targetType, new FormatterConverter());
                string[] keyNames = row.GetKeyNames;
                foreach (string keyName in keyNames)
                {
                    object newValue = row[keyName];
                    if (newValue.GetType() == typeof(Guid))
                        newValue = this.findAndRecurse(tableList, null, (Guid)newValue);

                    info.AddValue(keyName, newValue, newValue.GetType());
                }

                newObject = targetType.InvokeMember("", BindingFlags.CreateInstance | BindingFlags.Public | BindingFlags.Instance, null, null,
                                                    new object[] { info, new StreamingContext() });
            }
            else
            {
                newObject = targetType.InvokeMember("", BindingFlags.CreateInstance | BindingFlags.Public | BindingFlags.Instance, null, null, null);
                string[] keyNames = row.GetKeyNames;
                foreach (string keyName in keyNames)
                {
                    if (!table.ColumnNames.Contains(keyName))
                        continue;
                    Type keyType = table.GetTypeFromColumnName(keyName);

                    PropertyInfo pInfo = targetType.GetProperty(keyName);

                    if (pInfo == null)
                        continue;

                    object newValue = row[keyName];

                    //Recurse to fill GUID properties with the proper member
                    if (newValue.GetType() == typeof(Guid))
                        newValue = this.findAndRecurse(tableList, pInfo.PropertyType, (Guid)newValue);


                    //if (newValue.GetType() != keyType || newValue.GetType() != pInfo.PropertyType)
                    //    throw new Exception("Type mismatch during read.");

                    pInfo.SetValue(newObject, newValue, null);
                }
            }

            //Register top-level object.
            SerializedObjectManager.RegisterObject(row.RowIdentifier, newObject);
            return newObject;
        }

        //MBouges: http://www.codeproject.com/KB/cs/objserial.aspx
        //MBouges: see how it has those two functions
        //MBouges: the constructor
        //MBouges: and the getobjectdata
        //MBouges: will that work?
        //MBouges: ok so
        //MBouges: all you have to do
        //MBouges: is make sure that the object doesn't implement ISerializable irst
        //MBouges: if it does then call get object info and skip your recursive thing
        //MBouges: and instead read from the serializationinfo
        //MBouges: to insert things into the db
        //CruxDesideratum: gotcha
        //MBouges: if it doesn't, then ask the TranslatorCentral
        //MBouges: for a translator
        //MBouges: and if you get one
        //MBouges: call that GetObjectData
        //MBouges: do the same thing
        //MBouges: if not then use this
        //MBouges: what you have
        //CruxDesideratum: so getting the data from the properties
        //CruxDesideratum: is only for
        //CruxDesideratum: Doesn't have a translator
        //CruxDesideratum: doesn't implement ISerializable
        //MBouges: right
        //CruxDesideratum: the SerializationInfo from ISerializable is just a list of name and propertyname pairs of stuff you want to serialize
        //CruxDesideratum: and the translator from translatorcentral gives me a SerializationInfo when I call GetObjectData?
        //CruxDesideratum: or rather it fills the serializationInfo
        //MBouges: the translator implements ISerializable
        //MBouges: like id' make an abstract base class
        //MBouges: SerializationTranslator or whatever
        //CruxDesideratum: ahh I see
        //MBouges: then have that implement it abstractly
        //MBouges: and have a constructor that wraps around an object
        //CruxDesideratum: so the Translator *is* pretty much the uh, Serializer things we were using, just centralized and significantly less ghetto
        //MBouges: and a property that returns the types it works on
        //MBouges: yeah pretty much
        //CruxDesideratum: okay
        //MBouges: except they don't use properties exclusively
        //CruxDesideratum: that's why I got confused when I mentioned those and you were like "no way"
        //CruxDesideratum: gotcha
        //MBouges: well they are different
        //MBouges: because it's structured
        //MBouges: and it's not jsut reading properties
        //CruxDesideratum: alright
        //MBouges: because it allows for side effects and stuff



        //        MBouges: i think the right thing to do is
        //MBouges: a) see if the object itself is ISerializable
        //MBouges: b) if not see if there is a translator available
        //MBouges: and the translators implmeent ISerializable
        //MBouges: and will have a standard thing to attach the object to it
        //MBouges: c) if not a or b, use the properties
        //CruxDesideratum: makes sense
        //CruxDesideratum: if the object is ISerializable what do you do?
        //MBouges: hrm wait i just realized you're doing things differently
        //CruxDesideratum: ?
        //MBouges: well normally it takes a parameter
        //MBouges:    void ISerializable.GetObjectData(SerializationInfo info, StreamingContext ctx) {
        //        // Fill up the SerializationInfo object with the formatted data.
        //        info.AddValue("First_Item", dataItemOne.ToUpper());
        //        info.AddValue("dataItemTwo", dataItemTwo.ToUpper());
        //    }
        //MBouges: the Serializationinfo
        //MBouges: you don't use that right?
        //CruxDesideratum: no
        //CruxDesideratum: I probably could
        //MBouges: although you could always create it
        //CruxDesideratum: especially if I'm supposed to, heh
        //MBouges: just for things that implement iserializable
        //MBouges: yeah
        //CruxDesideratum: well
        //MBouges: and so see it has a name and value
        //CruxDesideratum: the reason I've been relatively "meh whatever" about translation
        //CruxDesideratum: is that 95% of what is complex about my program
        //CruxDesideratum: takes place well after the translation
        //CruxDesideratum: the formatter code is pretty simple
        //MBouges: so you'll have to have an internal class that has a name/value
        //MBouges: pair
        //CruxDesideratum: so I don't think it's a huge leap to implement whatever is the best moving forward
        //MBouges: probably generics
        //MBouges: like my wrapper is
        //CruxDesideratum: *nods*
        //CruxDesideratum: the name won't always be a string, eh?
        //MBouges: i think it will
        //CruxDesideratum: or the generic is for the value
        //MBouges: for the value!
        //CruxDesideratum: okay I wasn't sure if it was for both haha
        //MBouges: so you can have an internal wrapper class
        //MBouges: exactly like i use in the db
        //MBouges: as a hack
        //MBouges: so the rest of your stuff's typing works
        //CruxDesideratum: alrighty
        //MBouges: call GetObjectData 
        //MBouges: then read the SerializationInfo
        //MBouges: and make a wrapper for each one
        //MBouges: each thing in it
        //MBouges: and then those are written instead of the properties
        //CruxDesideratum: gotcha
        //MBouges: that should work!
        //CruxDesideratum: now does the translator use the internal type too, or is that only for ISerializable?
        //MBouges: well
        //MBouges: the translator itself is ISerializable
        //MBouges: i think that's the best way to make it
        //CruxDesideratum: good point
        //MBouges: so the formatter just makes a call to the translator central
        //MBouges: and gets one if available
        //MBouges: actually that might be the first thing to do
        //MBouges: because if there is a translator
        //CruxDesideratum: translator > ISerializable?
        //MBouges: then that becomes the object
        //MBouges: to do everything on
        //CruxDesideratum: *nods*

        private IFormatterWrapper[] SerializationInfoToWrapper(SerializationInfo info)
        {
            List<IFormatterWrapper> wrappers = new List<IFormatterWrapper>();

            foreach (SerializationEntry entry in info)
            {
                IFormatterWrapper wrap = CeresBase.General.Utilities.CreateGenericObject(typeof(formatterWrapper<>),
                                                                                         new Type[] { entry.ObjectType }
                                                                                         ) as IFormatterWrapper;
                wrap.Name = entry.Name;
                wrap.Object = entry.Value;

                wrappers.Add(wrap);
            }

            return wrappers.ToArray();
        }


        //**************************************************************
        //**************************************************************
        //**************************************************************
        //TODO
        //Singular object handling - must maintain "object" status and handle multi-type, use string conversion
        //**************************************************************
        //**************************************************************
        //**************************************************************


        public void Serialize(System.IO.Stream serializationStream, object graph)
        {
            if (!(serializationStream is IRelationalStream))
                throw new Exception("Stream passed into the Serialize method of a RelationalFormatter was not an IRelationalStream.");

            IRelationalStream relationalStream = serializationStream as IRelationalStream;

            Type[] supportedTypes = relationalStream.SupportedTypes;
            if (!(supportedTypes.Contains(typeof(System.Collections.IList)) && supportedTypes.Contains(typeof(System.Collections.IDictionary))))
                throw new Exception("Support for streams that do not support IList and IDictionary types is not implemented.");

            List<RelationalTable> tables = new List<RelationalTable>();

            Guid registeredID = Guid.Empty;
            if (SerializedObjectManager.IsObjectRegistered(graph))
            {
                registeredID = SerializedObjectManager.GetRegisteredID(graph);
            }

            this.addRowsRecursively(ref tables, graph, relationalStream, registeredID);

            foreach (RelationalTable table in tables)
            {
                relationalStream.WriteTable(table);
            }
        }

        private Guid addRowsRecursively(ref List<RelationalTable> tables, object graph, IRelationalStream stream, Guid id)
        {
            PropertyInfo[] infos = null;
            IFormatterWrapper[] wrappers = null;
            if (SerializationCentral.HasTranslator(graph)) 
            {
                ITranslator translator = SerializationCentral.GetTranslator(graph.GetType());
                translator.AttachedObject = graph;
                SerializationInfo info = new SerializationInfo(graph.GetType(), new FormatterConverter());
                translator.GetObjectData(info, new StreamingContext());
                wrappers = SerializationInfoToWrapper(info);
            }
            else if (graph.GetType().GetInterfaces().Contains(typeof(ISerializable)))
            {
                ISerializable ser = graph as ISerializable;
                SerializationInfo serInfo = new SerializationInfo(graph.GetType(), new FormatterConverter());
                ser.GetObjectData(serInfo, new StreamingContext());
                wrappers = SerializationInfoToWrapper(serInfo);
            }
            else
                infos = RelationalFormatter.GetPropertyInfoForSerialization(graph.GetType());

            //This should never be called on a simple type - make sure the graph is NOT on the supportedTypes list, since it won't have
            //any table to be stored in the relational structure.
            if (wrappers == null && stream.SupportedTypes.Contains(graph.GetType()))
                throw new Exception("Can't use a supported type as the root serialization object for a row/table!");

             RelationalTable targetTable;

            //Add the table type if necessary
            targetTable =  RelationalTable.GetTable(tables, graph.GetType());
            if (targetTable == null)
                targetTable = RelationalTable.AddTableToList(ref tables, graph.GetType());

            RelationalRow newRow = new RelationalRow();
            if (id == Guid.Empty)
                newRow.RowIdentifier = Guid.NewGuid();
            else
                newRow.RowIdentifier = id;

            if (infos != null)
            {
                foreach (PropertyInfo info in infos)
                {
                    string key = info.Name;
                    object obj = info.GetValue(graph, null);
                    if (!targetTable.ColumnNames.Contains(key))
                        throw new Exception("Serialization failed in RelationalFormatter : Graph-Table mismatch.");

                    if (obj != null)
                    {
                        newRow[key] = getRowData(info.PropertyType, stream, obj, ref tables);
                    }
                }
            }
            else if (wrappers != null)
            {
                foreach (IFormatterWrapper wrapper in wrappers)
                {
                    string key = wrapper.Name;
                    object obj = wrapper.Object;
                    //In an ISerializable, it's not really feasible to get the columns beforehand, so put them in here.
                    if (!targetTable.ColumnNames.Contains(key))
                    {
                        targetTable.AddColumn(key, obj.GetType());
                        //throw new Exception("Serialization failed in RelationalFormatter : Graph-Table mismatch.");
                    }

                    if (obj != null)
                    {
                        newRow[key] = getRowData(wrapper.Object.GetType(), stream, obj, ref tables);
                    }
                }
            }
            else
                throw new Exception("Unexpected empty property identifiers.");

            targetTable.Rows.Add(newRow);

            //Register top-level object.
            SerializedObjectManager.RegisterObject(newRow.RowIdentifier, graph);
            return newRow.RowIdentifier;
        }

        private object getArrayData(Type infoType, IRelationalStream stream, object obj, ref List<RelationalTable> tables)
        {
            List<object> ret = new List<object>();

            foreach (object item in (obj as IList))
            {
                ret.Add(getRowData(infoType, stream, item, ref tables));
            }

            return ret.ToArray();
        }

        private object getRowData(Type infoType, IRelationalStream stream, object obj, ref List<RelationalTable> tables)
        {
            object ret = null;
            Guid registeredID = Guid.Empty;

            if (SerializedObjectManager.IsObjectRegistered(obj))
            {
                int ky = 0;

                registeredID = SerializedObjectManager.GetRegisteredID(obj);
                //object registeredObj = SerializedObjectManager.RetrieveObject(registeredID);
            }

            if (obj.GetType().IsArray)
            {
                ret = getArrayData(infoType, stream, obj, ref tables);
            }
            else if (this.TypeIsSupported(obj.GetType(), stream.SupportedTypes))
            {
                if (SerializationCentral.HasTranslator(obj))
                {
                    ret = this.addRowsRecursively(ref tables, obj, stream, registeredID);
                }
                else if (stream.NativelyStorable(obj.GetType()))
                {
                    //serialize directly
                    ret = obj;
                }
                else
                {
                    //Dictionary<Guid, object> containedObjects = stream.HandleSpecialStorage(ref tables, obj, info, targetTable, ref newRow);
                    //There are a few types that might not be natively storable, run through them here:
                    //To make things simple, translate any non-native storage to external "special table" setups
                    //IList
                    if (infoType.GetInterfaces().Contains(typeof(IList)))
                    {
                        ret = this.addRowsRecursively(ref tables, WriteList(obj as IList), stream, registeredID);
                    }
                    //IDictionary
                    if (infoType.GetInterfaces().Contains(typeof(IDictionary)))
                    {
                        ret = this.addRowsRecursively(ref tables, WriteDictionary(obj as IDictionary),
                                                            stream, registeredID);
                    }
                    //Enum
                    if (infoType.IsEnum)
                    {

                    }
                }
            }
            else
            {
               //recurse and store the Guid... but only if it's not null, obviously
                if (obj != null)
                    ret = this.addRowsRecursively(ref tables, obj, stream, registeredID);
            }

            return ret;
        }


        public static RelationalFormatterHelper WriteDictionary(IDictionary dict)
        {
            List<object> keyObjs = new List<object>();
            foreach (object key in dict.Keys)
            {
                keyObjs.Add(key);
            }

            List<object> itemsObjs = new List<object>();
            foreach (object item in dict.Values)
            {
                itemsObjs.Add(item);
            }

            return new RelationalFormatterHelper() { PropertyName = "DictionaryItems", OriginalReference = dict, Value = new object[] { keyObjs.ToArray(), itemsObjs.ToArray() } };
        }

        public static RelationalFormatterHelper WriteList(IList list)
        {
            List<object> items = new List<object>();
            foreach (object item in list)
            {
                items.Add(item);
            }

            return new RelationalFormatterHelper() { PropertyName = "ListItems", OriginalReference = list, Value = items.ToArray() };
        }

        private bool TypeIsSupported(Type propertyType, Type[] supportedTypes)
        {
            if (supportedTypes.Contains(propertyType))
                return true;

            Type[] interfaces = propertyType.GetInterfaces();
            foreach (Type thisType in supportedTypes)
            {
                if(interfaces.Contains(thisType))
                    return true;
            }

            return false;
        }

        static public PropertyInfo[] GetPropertyInfoForSerialization(Type forType)
        {
            PropertyInfo[] infos = forType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            List<PropertyInfo> ret = new List<PropertyInfo>();

            //Cut out properties based on several criteria
            foreach (PropertyInfo info in infos)
            {
                if (!info.CanRead)
                    continue;

                object[] attributes = info.GetCustomAttributes(typeof(NonSerializedAttribute), false);
                if (attributes.Count() != 0)
                    continue;

                attributes = info.GetCustomAttributes(typeof(System.ComponentModel.DesignerSerializationVisibilityAttribute), false);
                if (attributes.Count() == 1)
                {
                    System.ComponentModel.DesignerSerializationVisibilityAttribute attr = attributes[0] as System.ComponentModel.DesignerSerializationVisibilityAttribute;
                    if (attr.Visibility == System.ComponentModel.DesignerSerializationVisibility.Hidden)
                        continue;
                }

                

                if (!info.CanWrite)
                {
                    if (!((info.PropertyType.GetInterfaces().Contains(typeof(IDictionary))) ||
                            (info.PropertyType.GetInterfaces().Contains(typeof(IList)))))
                        continue;
                }

                ret.Add(info);
            }

            return ret.ToArray();
        }

        /// <summary>
        /// Allows for selection of a specific type for saving/loading, in case they are not exactly analagous.
        /// </summary>
        private SerializationBinder binder;
        public SerializationBinder Binder
        {
            get
            {
                return this.binder;
            }
            set
            {
                this.binder = value;
            }
        }

        /// <summary>
        /// Indicates source/destination of the stream being formatted to allow additional modifications based on that information.
        /// </summary>
        private StreamingContext context = new StreamingContext(StreamingContextStates.All);
        public StreamingContext Context
        {
            get
            {
                return this.context;
            }
            set
            {
                this.context = value;
            }
        }


        /// <summary>
        /// Allows one to define a selector method that can choose an ISerializer to use instead of the supplied serialization in particular cases.
        /// </summary>
        private ISurrogateSelector surrogateSelector;
        public ISurrogateSelector SurrogateSelector
        {
            get
            {
                return this.surrogateSelector;
            }
            set
            {
                this.surrogateSelector = value;
            }
        }

        #endregion
    }
}
