using System;
using System.Collections.Generic;
using System.Text;
using NetCDFCSharp;
using CeresBase.Development;
using CeresBase.Data;

namespace CeresBase.IO.NCDF
{
    [CeresCreated("Mikkel", "03/16/06")]
    public class NetCDFFile : ICeresFile, IDisposable
    {

        public const string TimesDim = "TimesDim";
        public const string TimesBaseAttr = "BaseTime";
        public const string UnitsStr = "units";
        public const string ExptNameStr = "ExptName";
        public const string VarTypeStr = "VariableType";
        public const string ShapeTypeStr = "ShapeType";



        #region Private Variables
        private int fileId;
        private DateTime time;
        private Dictionary<string, int> dimids;
        private Dictionary<string, int> varids;
        private string units;
        private string expt;
        private int[] varDims;
        private int[] dimLengths;
        private Type variableType;
        private Type shapeType;
        string filePath;
        #endregion


        #region Properties
        [CeresCreated("Mikkel", "03/20/06")]
        public Type VariableType
        {
            get
            {
                return this.variableType;
            }
        }

        [CeresCreated("Mikkel", "03/20/06")]
        public Type ShapeType
        {
            get
            {
                return this.shapeType;
            }
        }

        [CeresCreated("Mikkel", "03/20/06")]
        internal int[] DimLengths
        {
            get
            {
                return this.dimLengths;
            }
        }

        [CeresCreated("Mikkel", "03/20/06")]
        internal int[] VarDims
        {
            get
            {
                return this.varDims;
            }
        }

        [CeresCreated("Mikkel", "03/20/06")]
        internal string Units
        {
            get
            {
                return this.units;
            }
        }

        [CeresCreated("Mikkel", "03/20/06")]
        internal string ExptName
        {
            get
            {
                return this.expt;
            }
        }
        
        [CeresCreated("Mikkel", "03/16/06")]
        internal Dictionary<string, int> DimIDs
        {
            get
            {
                return this.dimids;
            }
        }

        [CeresCreated("Mikkel", "03/16/06")]
        internal Dictionary<string, int> VarIDs
        {
            get
            {
                return this.varids;
            }
        }

        [CeresCreated("Mikkel", "03/16/06")]
        internal DateTime Time
        {
            get
            {
                return this.time;
            }
            set
            {
                this.time = value;
            }
        }

        [CeresCreated("Mikkel", "03/16/06")]
        internal int FileId
        {
            get
            {
                return this.fileId;
            }
        }

        //[CeresCreated("Mikkel", "03/16/06")]
        //public DataFrame Frame
        //{
        //    get
        //    {
        //        return this.frame;
        //    }
        //}

        [CeresCreated("Mikkel", "03/16/06")]
        public string[] VariableNames
        {
            get
            {
                List<string> list = new List<string>();
                foreach (string name in this.varids.Keys)
                {
                    if (!this.dimids.ContainsKey(name))
                    {
                        list.Add(name);
                    }
                }
                return list.ToArray();
            }
        }

        [CeresCreated("Mikkel", "03/16/06")]
        public DateTime[] Times
        {
            get
            {
                return new DateTime[] { this.time };
            }
        }

        [CeresCreated("Mikkel", "03/20/06")]
        public string FilePath
        {
            get
            {
                return this.filePath;
            }
        }

        #endregion

        private void fillInformation()
        {
            int ndims = NetCDF.nc_inq_ndims(this.fileId);
            for (int i = 0; i < ndims; i++)
            {
                string name = NetCDF.nc_inq_dimname(this.fileId, i).Replace("_", " ");
                this.dimids.Add(name, i);
            }

            int nvars = NetCDF.nc_inq_nvars(this.fileId);
            for (int i = 0; i < nvars; i++)
            {
                string name = NetCDF.nc_inq_varname(this.fileId, i).Replace("_", " ");
                this.varids.Add(name, i);
            }

            this.expt = (string)NetCDF.nc_get_att(this.fileId, NetCDF.Constants.NC_GLOBAL, NetCDFFile.ExptNameStr);

            int varid = this.varids[this.VariableNames[0]];
            this.units = (string)NetCDF.nc_get_att(this.fileId, varid, NetCDFFile.UnitsStr);
            this.varDims = NetCDF.nc_inq_vardimid(this.fileId, varid);
            this.dimLengths = new int[this.varDims.Length];

            string varTypeStr = (string)NetCDF.nc_get_att(this.fileId, NetCDF.Constants.NC_GLOBAL, NetCDFFile.VarTypeStr);
            string shapeTypeStr = (string)NetCDF.nc_get_att(this.fileId, NetCDF.Constants.NC_GLOBAL, NetCDFFile.ShapeTypeStr);
            this.variableType = IOFactory.CreateType(varTypeStr);
            this.shapeType = IOFactory.CreateType(shapeTypeStr);


            for (int i = 0; i < varDims.Length; i++)
            {
                this.dimLengths[i] = NetCDF.nc_inq_dimlen(this.fileId, this.varDims[i]);
            }

            if (!this.dimids.ContainsKey(NetCDFFile.TimesDim))
            {
                this.time = CeresBase.General.Constants.Timeless;
            }
            else
            {
                int timedim = this.varids[NetCDFFile.TimesDim];
                string timeBaseStr = (string)NetCDF.nc_get_att(this.fileId, timedim, NetCDFFile.TimesBaseAttr);
                double val = ((double[])NetCDF.nc_get_var(this.fileId, timedim))[0];
                this.time = CeresBase.General.Constants.DateTimeFromBaseStr(timeBaseStr, val);
            }

        }

        #region Constructors
        //[CeresCreated("Mikkel", "03/16/06")]
        //[CeresToDo("Mikkel", "03/16/06", Comments = "Exception", Priority = DevelopmentPriority.Exception)]
        //internal NetCDFFile(string filepath, Type variableType, Type shapeType)
        //{
        //    this.fileId = NetCDF.nc_open(filepath, NetCDF.Constants.NC_SHARE);
        //    if (this.fileId < 0) throw new Exception("File not found");
        //    this.dimids = new Dictionary<string, int>();
        //    this.varids = new Dictionary<string, int>();
        //    this.variableType = variableType;
        //    this.shapeType = shapeType;
        //    this.fillInformation();

        //    this.filePath = filepath;
        //}

        [CeresCreated("Mikkel", "03/20/06")]
        internal NetCDFFile(string filepath, bool create)
        {
            this.dimids = new Dictionary<string, int>();
            this.varids = new Dictionary<string, int>();

            if (create)
            {
                this.fileId = NetCDF.nc_create(filepath, 0);
            }
            else
            {
                this.fileId = NetCDF.nc_open(filepath, NetCDF.Constants.NC_SHARE);
                if (this.fileId < 0) throw new Exception("File not found");
                this.fillInformation();
            }

            this.filePath = filepath;

        }

        //[CeresCreated("Mikkel", "03/16/06")]
        //internal NetCDFFile(string filepath, DataFrame frame, bool create)
        //{
        //    this.frame = frame;
        //    this.variableType = frame.Variable.GetType();
        //    this.shapeType = frame.Shape.GetType();
        //    this.dimids = new Dictionary<string, int>();
        //    this.varids = new Dictionary<string, int>();

        //    if (create)
        //    {
        //        this.fileId = NetCDF.nc_create(filepath, 0);
        //    }
        //    else
        //    {
        //        this.fileId = NetCDF.nc_open(filepath, NetCDF.Constants.NC_SHARE);
        //        if (this.fileId < 0) throw new Exception("File not found");
        //        this.fillInformation();
        //    }

        //    this.filePath = filepath;

        //}

        ~NetCDFFile()
        {
            if (this.fileId < 0) return;
            this.Close();
        }

        #endregion

        #region Public Functions

        [CeresCreated("Mikkel", "03/16/06")]
        public void Close()
        {
            try
            {
                NetCDF.nc_close(this.fileId);
            }
            finally
            {
                this.fileId = -1;
            }
        }

        public void Refresh()
        {
            if(this.fileId == -1)
                this.fileId = NetCDF.nc_open(this.filePath, NetCDF.Constants.NC_SHARE);

            this.fillInformation();
        }

        #region IDisposable Members
        [CeresCreated("Mikkel", "03/16/06")]
        public void Dispose()
        {
            this.Close();
        }

        #endregion

        #endregion
    }
}
