using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;
using CeresBase.Data;
using NetCDFCSharp;
using CeresBase.IO.DataBase;

namespace CeresBase.IO.NCDF
{
    [CeresCreated("Mikkel", "03/16/06")]
    public class NetCDFReader : ICeresReader
    {
        private static readonly object lockable = new object();

        //[CeresCreated("Mikkel", "03/20/06")]
        //private CeresFileDataBase database;

        [CeresCreated("Mikkel", "03/16/06")]
        public object ReadAttribute(ICeresFile file, string name, string attrName)
        {
            NetCDFFile nfile = file as NetCDFFile;
            int varid = nfile.VarIDs[name];
            try
            {
                return NetCDF.nc_get_att(nfile.FileId, varid, attrName);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        
        //[CeresCreated("Mikkel", "03/20/06")]
        //[CeresToDo("Mikkel", "03/20/06", Comments = "Take times into account", Priority=DevelopmentPriority.Critical)]
        //public Variable[] ReadVariables(string filename, VariableHeader[] headers, DateTime[][] times)
        //{
        //    List<Variable> variables = new List<Variable>();
        //    for (int i = 0; i < headers.Length; i++)
        //    {
        //        variables.Add(this.ReadVariable(filename, headers[i], null));
        //    }
        //    return variables.ToArray();
        //}

        //[CeresCreated("Mikkel", "03/16/06")]
        //[CeresToDo("Mikkel", "03/20/06", Comments = "Look at threading implications", Priority=DevelopmentPriority.Critical)]
        //public Variable ReadVariable(string filename, VariableHeader header, DateTime[] times)
        //{
        //    Variable var = null;
        //    List<DataFrame> frames = new List<DataFrame>();
        //    if (times == null) times = new DateTime[] { CeresBase.General.Constants.Timeless };
        //    foreach (DateTime time in times)
        //    {
        //        //Type shapeType;
        //        //Type varType;
        //        //this.database.GetFileProperties(filename, out
        //        //    varType, out shapeType);
               
        //        NetCDFFile nfile = new NetCDFFile(filename, false);

        //        string varName = nfile.VariableNames[0]; //Only one variable per file
        //        int vid = nfile.VarIDs[varName];

        //        lock (NetCDFReader.lockable)
        //        {
        //            if (var == null)
        //            {
        //                var = VariableSource.GetVariable(varName, nfile.Units, header.ExperimentName);
        //                if (var == null)
        //                {
        //                    var = IOFactory.CreateVariable(header);
        //                    var.ReadSpecificInformation(this, nfile);
        //                }
        //            }
        //        }

        //        //TODO CHANGE TO TAKE THE TYPE INTO ACCOUNT AS WELL AS EFFICENT READING AND COMPRESSION
        //        IDataShape shape = (IDataShape)nfile.ShapeType.InvokeMember("", System.Reflection.BindingFlags.CreateInstance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance,
        //            null, null, null);
        //        shape.ReadSpecificInformation(this, nfile);

        //        int[] vardimslen = nfile.VarDims;
        //        for (int i = 0; i < vardimslen.Length; i++)
        //        {
        //            string name = NetCDF.nc_inq_dimname(nfile.FileId, vardimslen[i]);
        //            int dimid = NetCDF.nc_inq_dimid(nfile.FileId, name);
        //            vardimslen[i] = NetCDF.nc_inq_dimlen(nfile.FileId, dimid);
        //        }

        //        IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(nfile);
        //        //IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(vardimslen);

        //        if (pool.GetType() != typeof(CeresBase.Data.DataPools.DataPoolNetCDF))
        //        {
        //            if (vardimslen.Length == 1)
        //            {
        //                uint buffer = 6000000;
        //                uint spot = 0;
        //                while (spot < vardimslen[0])
        //                {
        //                    uint[] amounts = new uint[] { buffer };
        //                    if (vardimslen[0] - spot < buffer)
        //                    {
        //                        amounts[0] = (uint)(vardimslen[0] - spot);
        //                    }

        //                    uint[] index = new uint[] { spot };
        //                    float[] data = (float[])NetCDF.nc_get_vara(nfile.FileId, vid,
        //                        index, amounts);


        //                    pool.SetData(DataUtilities.ArrayToMemoryStream(data));
        //                    spot += amounts[0];
        //                }
        //            }
        //        }


        //        float min = (float)NetCDF.nc_get_att(nfile.FileId, vid, "Min");
        //        float max = (float)NetCDF.nc_get_att(nfile.FileId, vid, "Max");
        //        pool.SetMinMaxManually(min, max);

        //        pool.DoneSettingData();


        //        DataFrame frame = new DataFrame(var, pool, shape, time);
        //        frames.Add(frame);
        //    }
        //    var.AddDataFrames(frames.ToArray());
        //    return var;
        //}

        [CeresCreated("Mikkel", "03/20/06")]
        public double[][] GetShapeValues(ICeresFile file)
        {
            NetCDFFile nfile = file as NetCDFFile;
            int[] vardims = nfile.VarDims;
            double[][] toRet = new double[vardims.Length][];
            for (int i = 0; i < vardims.Length; i++)
            {
                string name = NetCDF.nc_inq_dimname(nfile.FileId, vardims[i]);
                int varid = NetCDF.nc_inq_dimid(nfile.FileId, name);
                toRet[i] = (double[])NetCDF.nc_get_var(nfile.FileId, varid);
            }
            return toRet;
        }


        [CeresCreated("Mikkel", "03/16/06")]
        public byte[] ReadBinary(ICeresFile file, string name)
        {
            NetCDFFile nfile = file as NetCDFFile;
            int varid = nfile.VarIDs[name];
            byte[] toRet = (byte[])NetCDF.nc_get_var(nfile.FileId, varid);
            return toRet;
        }

        //[CeresCreated("Mikkel", "03/16/06")]
        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}

        public VariableHeader[] GetAllVariableHeaders(string file, string experimentName)
        {
            NetCDFFile nfile = new NetCDFFile(file, false);
            VariableHeader header = IO.IOFactory.GetVariableHeader(nfile.VariableType, experimentName,
                nfile.VariableNames[0], nfile.Units);
            header.ReadSpecificInformation(this, nfile);
            nfile.Close();
            return new VariableHeader[] { header };
        }


        public bool Supported(string file)
        {
            return NetCDF.IsNetCDF(file);
        }
   
        //public void AddFilesToDB(string[] files)
        //{
        //    foreach (string file in files)
        //    {
        //        NetCDFFile nfile = new NetCDFFile(file, false);
        //        this.database.CreateFile(nfile.ExptName, nfile);
        //        nfile.Close();
        //    }
        //}

        #region ICeresReader Members


        public bool StayNative
        {
            get { return true; }
        }

        #endregion

        #region ICeresReader Members


        public CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(FileDataBaseEntry entry)
        {
            return null;
        }

        #endregion

        #region ICeresReader Members


        public FileDataBaseVariableEntryParameter[] GetVariableParameters(FileDataBaseEntry entry)
        {
            NetCDFFile nfile = new NetCDFFile(entry.Filepath, false);
            VariableHeader header = IO.IOFactory.GetVariableHeader(nfile.VariableType, "",
                nfile.VariableNames[0], nfile.Units);
            FileDataBaseVariableEntryParameter para = header.GetDataBaseParameter(this, nfile);
            para.Units.Value = nfile.Units;
            para.Units.SuccessfullySet = true;

            para.VariableName.Value = nfile.VariableNames[0];
            para.VariableName.IsFixed = true;
            nfile.Close();
            return new FileDataBaseVariableEntryParameter[] { para };

        }

        #endregion

        #region ICeresReader Members


        public Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers)
        {
            if (headers.Length > 1) throw new NotSupportedException("NetCDF only supports one variable per file.");

            VariableHeader header = headers[0];

            Variable var = null;
            NetCDFFile nfile = new NetCDFFile(filename, false);

            string varName = nfile.VariableNames[0]; //Only one variable per file
            int vid = nfile.VarIDs[varName];

            lock (NetCDFReader.lockable)
            {
                if (var == null)
                {
                    var = VariableSource.GetVariable(varName, nfile.Units, header.ExperimentName);
                    if (var == null)
                    {
                        var = IOFactory.CreateVariable(header);
                        var.ReadSpecificInformation(this, nfile);
                    }
                }
            }
            
            int[] vardimslen = nfile.VarDims;
            for (int i = 0; i < vardimslen.Length; i++)
            {
                string name = NetCDF.nc_inq_dimname(nfile.FileId, vardimslen[i]);
                int dimid = NetCDF.nc_inq_dimid(nfile.FileId, name);
                vardimslen[i] = NetCDF.nc_inq_dimlen(nfile.FileId, dimid);
            }

            IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(nfile);
            //IDataPool pool = new CeresBase.Data.DataPools.DataPoolNetCDF(vardimslen);

            if (pool.GetType() != typeof(CeresBase.Data.DataPools.DataPoolNetCDF))
            {
                if (vardimslen.Length == 1)
                {
                    uint buffer = 6000000;
                    uint spot = 0;
                    while (spot < vardimslen[0])
                    {
                        uint[] amounts = new uint[] { buffer };
                        if (vardimslen[0] - spot < buffer)
                        {
                            amounts[0] = (uint)(vardimslen[0] - spot);
                        }

                        uint[] index = new uint[] { spot };
                        float[] data = (float[])NetCDF.nc_get_vara(nfile.FileId, vid,
                            index, amounts);


                        pool.SetData(DataUtilities.ArrayToMemoryStream(data));
                        spot += amounts[0];
                    }
                }
            }


            float min = (float)NetCDF.nc_get_att(nfile.FileId, vid, "Min");
            float max = (float)NetCDF.nc_get_att(nfile.FileId, vid, "Max");
            pool.SetMinMaxManually(min, max);

            pool.DoneSettingData();

            
            DataFrame frame = new DataFrame(var, pool, CeresBase.General.Constants.Timeless);
            var.AddDataFrame(frame);
            return new Variable[]{var};
        }

        #endregion
    }
}
