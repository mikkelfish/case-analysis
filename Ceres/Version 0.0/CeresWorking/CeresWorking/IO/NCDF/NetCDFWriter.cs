using System;
using System.Collections.Generic;
using System.Text;
using NetCDFCSharp;
using CeresBase.Data;
using CeresBase.Development;

namespace CeresBase.IO.NCDF
{
    /// <summary>
    /// 
    /// </summary>
    [CeresCreated("Mikkel", "03/16/06")]
    public class NetCDFWriter : ICeresWriter
    {
        //#region Private variables
        //private CeresFileDataBase database;
        //#endregion

        #region Private Functions

        private readonly char[] illegalChars = new char[] { ' ', '/', '\'' };

        private string makeLegal(string toWrite)
        {
            foreach (char character in this.illegalChars)
            {
                toWrite = toWrite.Replace(character, '_');
            }
            return toWrite;
        }

        [CeresCreated("Mikkel", "03/16/06")]
        private NetCDFFile writeDataFrame(DataFrame frame, string filePath)
        {
           // if (this.database.ContainsKey(filePath)) return null;

            NetCDFFile file = new NetCDFFile(filePath, true);


            Array[] dimensionValues = null;// frame.Shape.GetValuesOfDimensions(frame.Pool.DimensionLengths);
             

            List<int> dimids = new List<int>();
            List<int> varids = new List<int>();

            if (dimensionValues != null)
            {
                for (int i = 0; i < dimensionValues.Length; i++)
                {
                    int dimId = NetCDF.nc_def_dim(file.FileId, frame.Shape.DimensionNames[i], (uint)dimensionValues[i].Length);
                    int dvarId = NetCDF.nc_def_var(file.FileId, frame.Shape.DimensionNames[i], new int[] { dimId }, MesosoftCommon.Utilities.Reflection.Common.UnarrayType(dimensionValues[i].GetType()));
                    varids.Add(dvarId);
                    dimids.Add(dimId);
                }
                for (int i = 0; i < varids.Count; i++)
                {
                    NetCDF.nc_put_var(file.FileId, varids[i], dimensionValues[i]);
                }
            }
            else
            {
                for (int i = 0; i < frame.Pool.DimensionLengths.Length; i++)
                {
                    int dimId = NetCDF.nc_def_dim(file.FileId, frame.Shape.DimensionNames[i], (uint)frame.Pool.DimensionLengths[i]);
                    dimids.Add(dimId);
                }
            }
            frame.Shape.AddSpecificInformation(this, file);
            


            int timeDimId = -1;
            if (!frame.Variable.TimeLess)
            {
                timeDimId = NetCDF.nc_def_dim(file.FileId, NetCDFFile.TimesDim, (uint)1);
                int timeVarID = NetCDF.nc_def_var(file.FileId, NetCDFFile.TimesDim, NetCDF.nc_type.NC_DOUBLE, new int[] { timeDimId });
                NetCDF.nc_put_att(file.FileId, timeVarID, NetCDFFile.TimesBaseAttr, General.Constants.FileBaseTime.ToShortTimeString());

                double timeOffset = frame.Time.Subtract(General.Constants.FileBaseTime).TotalSeconds;
                NetCDF.nc_put_var(file.FileId, timeVarID, new double[] { timeOffset });
            }
            
            int varId = NetCDF.nc_def_var(file.FileId, this.makeLegal(frame.Variable.Header.VarName), dimids.ToArray(), typeof(float));

            NetCDF.nc_put_att(file.FileId, varId, NetCDFFile.UnitsStr, frame.Variable.Header.UnitsName);

            string test = (string)NetCDF.nc_get_att(file.FileId, varId, NetCDFFile.UnitsStr);

            frame.Variable.AddSpecificInformation(this, file);
            frame.Variable.Header.AddSpecificInformation(this, file);

            NetCDF.nc_put_att(file.FileId, varId, "Max", frame.Pool.Max);
            NetCDF.nc_put_att(file.FileId, varId, "Min", frame.Pool.Min);

            NetCDF.nc_put_att(file.FileId, NetCDF.Constants.NC_GLOBAL, NetCDFFile.ExptNameStr, frame.Variable.Header.ExperimentName);
            NetCDF.nc_put_att(file.FileId, NetCDF.Constants.NC_GLOBAL, NetCDFFile.ShapeTypeStr, frame.Shape.GetType().FullName);
            NetCDF.nc_put_att(file.FileId, NetCDF.Constants.NC_GLOBAL, NetCDFFile.VarTypeStr, frame.Variable.GetType().FullName);

            NetCDF.nc_enddef(file.FileId);


            frame.Pool.GetData(null, null,
                delegate(float[] data, uint[] indices, uint[] counts)
                {
                    if (indices.Length == 1)
                    {
                        NetCDF.nc_put_vara(file.FileId, varId, indices, counts, data);
                    }
                }
            );

          //  this.database.CreateFile(frame.Variable.Header.ExperimentName, file);
            file.Close();

            file.Refresh();
            return file;
        }

        #endregion

        //#region Properties
        //[CeresCreated("Mikkel", "03/16/06")]
        //public CeresFileDataBase Database
        //{
        //    get
        //    {
        //        return this.database;
        //    }
        //    set
        //    {
        //        this.database = value;
        //    }
        //}
        //#endregion

        #region Public Functions
        public override string ToString()
        {
            return "NetCDF (preferred)";
        }

        [CeresCreated("Mikkel", "03/16/06")]
        public ICeresFile[] WriteVariable(Variable variable, string writeToDirectory)
        {
            List<ICeresFile> files = new List<ICeresFile>();
            if (writeToDirectory[writeToDirectory.Length - 1] != '\\')
                writeToDirectory += '\\';
            foreach (DataFrame frame in variable)
            {
                //"Expt_" + frame.Variable.Header.ExperimentName +
                //"_Time_" + frame.Time.ToShortDateString()
                string filePath = writeToDirectory + 
                     frame.Variable.FullVarName.Replace('\\', '_');
                filePath = filePath.Replace('/', '-');
                
                int index = filePath.LastIndexOf('\\');
                string temp = filePath.Substring(0, index + 1);
                for (int i = index + 1; i < filePath.Length; i++)
                {
                    if (!char.IsLetterOrDigit(filePath[i]))
                        temp += '_';
                    else temp += filePath[i];
                }
                temp += ".nc";
                ICeresFile file = this.writeDataFrame(frame, temp);
                if (file != null) files.Add(file);
            }
            return files.ToArray();
        }


        public ICeresFile[] WriteVariables(Variable[] variables, string writeToDirectory)
        {
            List<ICeresFile> files = new List<ICeresFile>();
            foreach (Variable var in variables)
            {
                files.AddRange(this.WriteVariable(var, writeToDirectory));
            }
            return files.ToArray();
        }

        [CeresCreated("Mikkel", "03/16/06")]
        public void Close()
        {
            
        }

        #region IDisposable Members
        [CeresCreated("Mikkel", "03/16/06")]
        public void Dispose()
        {
            this.Close();
        }

        #endregion

        #region CeresWriter Members

        [CeresCreated("Mikkel", "03/16/06")]
        public void AddAttribute(ICeresFile file, string varName, string attrName, object value)
        {
            NetCDFFile nfile = file as NetCDFFile;
            varName =  this.makeLegal(varName);
            int id = NetCDF.nc_inq_varid(nfile.FileId, varName);
            NetCDF.nc_put_att(nfile.FileId, id, attrName, value);
        }

        [CeresCreated("Mikkel", "03/16/06")]
        public void WriteBinary(ICeresFile file, string name, byte[] data)
        {
            NetCDFFile nfile = file as NetCDFFile;
            int dimid = NetCDF.nc_def_dim(nfile.FileId, name, (uint)data.Length);
            int varid = NetCDF.nc_def_var(nfile.FileId, name, new int[] { dimid }, typeof(byte));
            NetCDF.nc_put_var(nfile.FileId, varid, data);
        }
        #endregion

        #endregion

        #region ICeresWriter Members



        #endregion

        #region ICeresWriter Members


        public void AddAttribute(string fileName, string name, string attrName, object value)
        {
            NetCDFFile file = new NetCDFFile(fileName, false);
            this.AddAttribute(file, name, attrName, value);
            file.Close();
        }

        #endregion
    }
}
