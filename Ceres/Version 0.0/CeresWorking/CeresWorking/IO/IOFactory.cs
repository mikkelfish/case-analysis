using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using CeresBase.Data;
using System.Reflection;
using CeresBase.Development;

namespace CeresBase.IO
{
    public static class IOFactory
    {
        private static Dictionary<Type, ConstructorInfo> variableHeaderTypes = new Dictionary<Type, ConstructorInfo>();

        [CeresToDo("Mikkel", "03/20/06", Comments = "Exceptions", Priority=DevelopmentPriority.Exception )]
        public static Variable CreateVariable(VariableHeader header)
        {
            ConstructorInfo info = header.VarType.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { header.GetType() }, null);
            return (Variable)info.Invoke(new object[]{header});
        }


        [CeresCreated("Mikkel", "03/20/06")]    
        public static VariableHeader GetVariableHeader(Type variableType, string experiment, string variable, string units)
        {
            if (!variableHeaderTypes.ContainsKey(variableType))
            {
                ConstructorInfo info = variableType.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, Type.EmptyTypes, null);
                Variable var = (Variable)info.Invoke(null);
                ConstructorInfo headInfo = var.HeaderType.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                   null, new Type[]{ typeof(string), typeof(string), typeof(string)},null );
                variableHeaderTypes.Add(variableType, headInfo);
                
                return (VariableHeader)headInfo.Invoke(new object[]{variable,units, experiment});
            }
            return (VariableHeader)variableHeaderTypes[variableType].Invoke(new object[] { variable, units, experiment });
        }
        
        public static Type CreateType(string typeStr)
        {
            return MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeStr);

            //CeresBase.Plugins.CeresPluginBase[] plugins =
            //    CeresBase.General.Utilities.CreatePlugins<CeresBase.Plugins.CeresPluginBase>();
            //foreach (CeresBase.Plugins.CeresPluginBase plugin in plugins)
            //{
            //    Type t = plugin.GetType().Assembly.GetType(typeStr);
            //    if (t != null) return t;
            //}
            //return null;
        }
    }
}
