//namespace CeresBase.IO.Controls
//{
//    partial class CeresVariableSelector
//    {
//        /// <summary> 
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary> 
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Component Designer generated code

//        /// <summary> 
//        /// Required method for Designer support - do not modify 
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.components = new System.ComponentModel.Container();
//            this.treeView1 = new System.Windows.Forms.TreeView();
//            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
//            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
//            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
//            this.deleteFromDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
//            this.splitContainer1.Panel1.SuspendLayout();
//            this.splitContainer1.Panel2.SuspendLayout();
//            this.splitContainer1.SuspendLayout();
//            this.contextMenuStrip1.SuspendLayout();
//            this.SuspendLayout();
//            // 
//            // treeView1
//            // 
//            this.treeView1.CheckBoxes = true;
//            this.treeView1.ContextMenuStrip = this.contextMenuStrip1;
//            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.treeView1.Location = new System.Drawing.Point(0, 0);
//            this.treeView1.Name = "treeView1";
//            this.treeView1.Size = new System.Drawing.Size(400, 471);
//            this.treeView1.TabIndex = 0;
//            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
//            // 
//            // propertyGrid1
//            // 
//            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.propertyGrid1.Location = new System.Drawing.Point(0, 0);
//            this.propertyGrid1.Name = "propertyGrid1";
//            this.propertyGrid1.Size = new System.Drawing.Size(269, 471);
//            this.propertyGrid1.TabIndex = 1;
//            // 
//            // splitContainer1
//            // 
//            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
//            this.splitContainer1.Name = "splitContainer1";
//            // 
//            // splitContainer1.Panel1
//            // 
//            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
//            // 
//            // splitContainer1.Panel2
//            // 
//            this.splitContainer1.Panel2.Controls.Add(this.propertyGrid1);
//            this.splitContainer1.Size = new System.Drawing.Size(673, 471);
//            this.splitContainer1.SplitterDistance = 400;
//            this.splitContainer1.TabIndex = 2;
//            // 
//            // contextMenuStrip1
//            // 
//            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
//            this.deleteFromDBToolStripMenuItem});
//            this.contextMenuStrip1.Name = "contextMenuStrip1";
//            this.contextMenuStrip1.Size = new System.Drawing.Size(158, 48);
//            // 
//            // deleteFromDBToolStripMenuItem
//            // 
//            this.deleteFromDBToolStripMenuItem.Name = "deleteFromDBToolStripMenuItem";
//            this.deleteFromDBToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
//            this.deleteFromDBToolStripMenuItem.Text = "Delete from DB";
//            this.deleteFromDBToolStripMenuItem.Click += new System.EventHandler(this.deleteFromDBToolStripMenuItem_Click);
//            // 
//            // CeresVariableSelector
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.Controls.Add(this.splitContainer1);
//            this.Name = "CeresVariableSelector";
//            this.Size = new System.Drawing.Size(673, 471);
//            this.splitContainer1.Panel1.ResumeLayout(false);
//            this.splitContainer1.Panel2.ResumeLayout(false);
//            this.splitContainer1.ResumeLayout(false);
//            this.contextMenuStrip1.ResumeLayout(false);
//            this.ResumeLayout(false);

//        }

//        #endregion

//        private System.Windows.Forms.TreeView treeView1;
//        private System.Windows.Forms.PropertyGrid propertyGrid1;
//        private System.Windows.Forms.SplitContainer splitContainer1;
//        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
//        private System.Windows.Forms.ToolStripMenuItem deleteFromDBToolStripMenuItem;
//    }
//}
