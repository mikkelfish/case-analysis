//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;
//using System.Windows.Forms;
//using CeresBase.Development;
//using CeresBase.Data;
//using CeresBase.General;
//using System.Reflection;
//using CeresBase.UI;

//namespace CeresBase.IO.Controls
//{
//    public partial class CeresDBManager : UI.BaseWindowing
//    {
//        private Variable[] selected;
//        public Variable[] Selected
//        {
//            get { return selected; }
//        }

//        private UI.LoadingWindow loadWindow;
   

//        private CeresFileDataBase dataBase;
//        public CeresFileDataBase DataBase
//        {
//            get
//            {
//                return this.dataBase;
//            }
//            set
//            {
//                this.dataBase = value;
//                this.ceresVariableSelector1.ReadDataBase(value);
//            }
//        }

//        private void itemStart(object sender, UI.LoadingItemEventArgs e)
//        {
//            this.loadWindow.LabelText = e.Description;
//            this.loadWindow.Percent = (int)((e.ItemNumber / (double)e.TotalItems)*100);
//        }

//        //private void itemEnd(object sender, UI.LoadingItemEventArgs e)
//        //{

//        //}

//        public CeresDBManager() : base(false)
//        {
//            InitializeComponent();
//            this.ceresVariableSelector1.NodeChecked += new TreeViewEventHandler(ceresVariableSelector1_NodeChecked);
//            this.listViewVariables.View = View.List;

           

//            this.backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
//            this.backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
//        }

//        void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
//        {
//            this.dataBase.ItemStart -= new ThreadSafeConstructs.ThreadSafeEventHandler<CeresBase.UI.LoadingItemEventArgs>(this.itemStart);
//            VariableCreation.ItemStarted -= new ThreadSafeConstructs.ThreadSafeEventHandler<CeresBase.UI.LoadingItemEventArgs>(this.itemStart);
//            this.loadWindow.Close();

//            this.selected = new Variable[this.listViewVariables.SelectedItems.Count];
//            for (int i = 0; i < this.selected.Length; i++)
//            {
//                if (this.listViewVariables.SelectedItems[i].Tag is CeresFileDataBase.CeresFileEntry)
//                {
//                    this.selected[i] = VariableSource.GetVariable(
//                        (this.listViewVariables.SelectedItems[i].Tag as CeresFileDataBase.CeresFileEntry).Header);
//                }
//                else
//                {
//                    this.selected[i] = VariableSource.GetVariable(this.listViewVariables.SelectedItems[i].Tag as
//                        VariableHeader);
//                }
//            }

//            this.DialogResult = DialogResult.OK;
//            this.Close();
//        }

//        void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
//        {
//            List<CeresFileDataBase.CeresFileEntry> entriesToLoad =
//                e.Argument as List<CeresFileDataBase.CeresFileEntry>;
//            this.dataBase.LoadVariables(entriesToLoad.ToArray(), null);
//           // VariableCreation.RunItems();
//            VariableSource.AddVariables(VariableProcessingCreation.CreateVariables());
//        }

//        void ceresVariableSelector1_NodeChecked(object sender, TreeViewEventArgs e)
//        {

//            CeresFileDataBase.CeresFileEntry header = e.Node.Tag as CeresFileDataBase.CeresFileEntry;
//            if (header == null) return;
            
//            if (!e.Node.Checked)
//            {
//                this.listViewVariables.Items.RemoveByKey(header.Header.ToString());
//                if (!VariableSource.ContainsVariable(header.Header))
//                    VariableSource.RemoveQueuedHeader(header.Header);
//            }
//            else
//            {
//                ListViewItem item = new ListViewItem();
//                item.Name = header.Header.ToString();
//                item.Text = header.Header.ToString();
//                item.Tag = header;
//                this.listViewVariables.Items.Add(item);

//                if(!VariableSource.ContainsVariable(header.Header))
//                    VariableSource.AddVariableHeaderToMake(header.Header);
//            }
            
//        }

//        [CeresCreated("Mikkel", "03/20/06")]
//        [CeresToDo("Mikkel", "03/20/06", Comments = "Exception", Priority=DevelopmentPriority.Exception)]
//        public void ImportFileIntoDB(string[] files)
//        {
//            Dictionary<ICeresReader, List<string>> readers = new Dictionary<ICeresReader, List<string>>();
//            foreach (string file in files)
//            {
//                ICeresReader reader = this.dataBase.GetReader(file);
//                if (reader == null)
//                {
//                    MessageBox.Show("There are no readers that support the file " + file);
//                    return;
//                }
//                if (!readers.ContainsKey(reader))
//                {
//                    readers.Add(reader, new List<string>());
//                }
//                readers[reader].Add(file);
//            }

//            foreach (ICeresReader reader in readers.Keys)
//            {
//                reader.AddFilesToDB(readers[reader].ToArray());
//            }

//            this.listViewVariables.Items.Clear();

//            this.ceresVariableSelector1.ReadDataBase(this.dataBase);
//        }

//        private void importFileIntoDBToolStripMenuItem_Click(object sender, EventArgs e)
//        {
//            OpenFileDialog dialog = new OpenFileDialog();
//            dialog.Multiselect = true;
//            if (dialog.ShowDialog() == DialogResult.Cancel) return;
//            this.ImportFileIntoDB(dialog.FileNames);
//        }

//        private void button3_Click(object sender, EventArgs e)
//        {
//            this.DialogResult = DialogResult.Cancel;
//            this.Close();
//        }


//        private void buttonSelect_Click(object sender, EventArgs e)
//        {
//            //INSERT PLEASE WAIT POP UP AND LOAD DATA
//            List<CeresFileDataBase.CeresFileEntry> entriesToLoad = new List<CeresFileDataBase.CeresFileEntry>();
//            foreach (ListViewItem item in this.listViewVariables.Items)
//            {
//                CeresFileDataBase.CeresFileEntry entry = item.Tag as CeresFileDataBase.CeresFileEntry;
//                if (entry == null) continue;

//                if (VariableSource.ContainsVariable(entry.Header.VarName,  entry.Header.ExperimentName))
//                    continue;
//                entriesToLoad.Add(entry);
//           }

//           this.dataBase.ItemStart += new ThreadSafeConstructs.ThreadSafeEventHandler<UI.LoadingItemEventArgs>(this.itemStart);
//           VariableCreation.ItemStarted += new ThreadSafeConstructs.ThreadSafeEventHandler<UI.LoadingItemEventArgs>(this.itemStart);

//           this.loadWindow = new CeresBase.UI.LoadingWindow();
           
//           this.backgroundWorker1.RunWorkerAsync(entriesToLoad);
//           this.loadWindow.ShowDialog();

//         //  this.dataBase.ConvertToNative();

          
//        }

//        private void selectStorageDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
//        {
//            FolderBrowserDialog select = new FolderBrowserDialog();
//            select.ShowDialog();
//            string path = select.SelectedPath;
//            CeresCorePlugin.Instance.CurrentApplication.Settings.FileWriteLocation = path;
//        }

//        //private void listViewMethods_ItemActivate(object sender, EventArgs e)
//        //{
//        //    ListView view = sender as ListView;
//        //    this.textBoxMethod.Tag = view.SelectedItems[view.SelectedItems.Count - 1].Tag;
//        //    this.textBoxMethod.Text = (view.SelectedItems[view.SelectedItems.Count - 1].Tag as MethodInfo).Name;
//        //}

//       // private void textBoxMethod_DragOver(object sender, DragEventArgs e)
//       // {
//       //     if(!e.Data.GetDataPresent(typeof(ListViewItem)))
//       //     {
//       //         e.Effect = DragDropEffects.None;
//       //         return;
//       //     }
//       //     e.Effect = DragDropEffects.Link;
//       // }

//       // private void listViewMethods_MouseDown(object sender, MouseEventArgs e)
//       // {
//       //     if (e.Button != MouseButtons.Right) return;
//       //     ListView view = sender as ListView;
//       //     ListViewItem item = view.GetItemAt(e.X, e.Y);
//       //     if (item == null) return;
//       //     view.DoDragDrop(item, DragDropEffects.Link);
//       // }

//       // private void textBoxMethod_DragDrop(object sender, DragEventArgs e)
//       // {
//       //     MethodInfo info = (MethodInfo)(e.Data.GetData(typeof(ListViewItem)) as ListViewItem).Tag;
//       //     (sender as TextBox).Tag = info;
//       //     (sender as TextBox).Text = info.Name;

//       //     if (this.textBoxVariable.Tag != null)
//       //     {
//       //         this.populateCreatePropertyGrid();

//       //     }
//       // }

//       // private void listViewVariables_MouseDown(object sender, MouseEventArgs e)
//       // {
//       //     if (e.Button != MouseButtons.Right) return;
//       //     ListView view = sender as ListView;
//       //     ListViewItem item = view.GetItemAt(e.X, e.Y);
//       //     if (item == null) return;
            
//       //     if(item.Tag is CeresFileDataBase.CeresFileEntry)
//       //         view.DoDragDrop((item.Tag as CeresFileDataBase.CeresFileEntry).Header, DragDropEffects.Link);
//       //     else
//       //         view.DoDragDrop(item.Tag as VariableHeader, DragDropEffects.Link);

//       // }

//       // private void textBoxVariable_DragOver(object sender, DragEventArgs e)
//       // {
//       //     string[] test = e.Data.GetFormats(true);

//       //     Type dataType = IO.IOFactory.CreateType(e.Data.GetFormats()[0]);
//       //     if (!dataType.IsSubclassOf(typeof(VariableHeader)) &&
//       //         typeof(VariableHeader) != dataType)
//       //     {
//       //         e.Effect = DragDropEffects.None;
//       //         return;
//       //     }
//       //     e.Effect = DragDropEffects.Link;
//       // }

//       // private void textBoxVariable_DragDrop(object sender, DragEventArgs e)
//       // {
//       //     VariableHeader header = (VariableHeader)e.Data.GetData(e.Data.GetFormats()[0]);
//       //     (sender as TextBox).Tag = header;
//       //     (sender as TextBox).Text = header.VarName;

//       //     if (this.textBoxMethod.Tag != null)
//       //     {
//       //         this.populateCreatePropertyGrid();
//       //     }
//       // }

//       // private void buttonCreateNewVar_Click(object sender, EventArgs e)
//       // {
//       //     if (this.textBoxMethod.Tag == null || this.textBoxVariable.Tag == null)
//       //         return;
//       //     if (!VariableCreation.ContainsAssociation(this.textBoxMethod.Tag as MethodInfo, (this.textBoxVariable.Tag as VariableHeader).GetType(),
//       //         true))
//       //     {
//       //         MessageBox.Show("This seems to be the first time you've tried to create a variable of this type with this method");
//       //         this.makeAssociation();
//       //         return;
//       //     }

//       //     List<string> names = new List<string>();
//       //     List<object> vals = new List<object>();
//       //     foreach(string key in this.propertyGridMethods.Results.Keys)
//       //     {
//       //         names.Add(key);
//       //         vals.Add(this.propertyGridMethods.Results[key]);
//       //     }

//       //     VariableHeader created = VariableCreation.AddVariableToCreate(this.textBoxMethod.Tag as MethodInfo, (this.textBoxVariable.Tag as VariableHeader),
//       //         names.ToArray(), vals.ToArray());
//       //     ListViewItem item = new ListViewItem();
//       //     item.Name = created.VarName + "_" + created.ExperimentName;
//       //     item.Text = created.VarName + "_" + created.ExperimentName;
//       //     item.Tag = created;
//       //     this.listViewVariables.Items.Add(item);               
//       // }

//       // private void makeAssociation()
//       // {
//       //     MethodAssociation association = new MethodAssociation();
//       ////     association.Build(this.textBoxVariable.Tag as VariableHeader, this.textBoxMethod.Tag as MethodInfo);
//       //     association.ShowDialog();
//       //     this.populateCreatePropertyGrid();
//       // }

//       // private void buttonEditDefaultNewVar_Click(object sender, EventArgs e)
//       // {
//       //     if (this.textBoxMethod.Tag == null || this.textBoxVariable.Tag == null)
//       //         return;
//       //     this.makeAssociation();
//       // }

//       // private void populateCreatePropertyGrid()
//       // {
//       //     Dictionary<string, object> values =
//       //         VariableCreation.GetMethodValues(this.textBoxVariable.Tag as VariableHeader, this.textBoxMethod.Tag as MethodInfo);

//       //     Dictionary<string, object>[] defaultValues = new Dictionary<string, object>[2];
//       //     defaultValues[0] = VariableCreation.GetDefaults(this.textBoxMethod.Tag as MethodInfo, this.textBoxVariable.Tag.GetType(), true);
//       //     defaultValues[1] = VariableCreation.GetDefaults(this.textBoxMethod.Tag as MethodInfo, this.textBoxVariable.Tag.GetType(), false);

//       //     Dictionary<string, bool>[] readOnlys = new Dictionary<string, bool>[2];
//       //     readOnlys[0] = VariableCreation.GetDefaultReadOnly(this.textBoxMethod.Tag as MethodInfo, this.textBoxVariable.Tag.GetType(), true);
//       //     readOnlys[1] = VariableCreation.GetDefaultReadOnly(this.textBoxMethod.Tag as MethodInfo, this.textBoxVariable.Tag.GetType(), false);

//       //     ParameterInfo[][] pass = new ParameterInfo[2][];
//       //     pass[0] = (this.textBoxMethod.Tag as MethodInfo).GetParameters();
//       //     pass[1] = VariableCreation.GetNeededHeaderParams((this.textBoxVariable.Tag as VariableHeader).GetType(), this.textBoxMethod.Tag as MethodInfo);


//       //     List<Type> types = new List<Type>();
//       //     List<string> categories = new List<string>();
//       //     List<string> descriptions = new List<string>();
//       //     List<string> names = new List<string>();
//       //     List<Type> editors = new List<Type>();
//       //     List<object> firstSettings = new List<object>();

//       //     for (int i = 0; i < 2; i++)
//       //     {
//       //         foreach (ParameterInfo para in pass[i])
//       //         {
//       //             object[] attrs = null;

//       //             if (i == 0)
//       //             {
//       //                 attrs = para.GetCustomAttributes(typeof(UI.MethodEditor.EditorParamAttribute), true);
//       //                 if (attrs == null || attrs.Length == 0) continue;
//       //             }
                    
                    
//       //             bool set = false;
//       //             if (defaultValues[i] != null)
//       //             {
//       //                 if (defaultValues[i].ContainsKey(para.Name))
//       //                 {
//       //                     if (!readOnlys[i][para.Name])
//       //                     {

//       //                         firstSettings.Add(defaultValues[i][para.Name]);
//       //                         set = true;
//       //                     }
//       //                 }
//       //             }

//       //             if (!set)
//       //             {
//       //                 continue;
//       //             }

//       //             types.Add(para.ParameterType);
//       //             names.Add(para.Name);
//       //             if (i == 0) categories.Add(para.Member.Name);
//       //             else categories.Add((this.textBoxVariable.Tag as VariableHeader).GetType().Name);

//       //             if (attrs != null && attrs.Length > 0)
//       //             {
//       //                 UI.MethodEditor.EditorParamAttribute attr = attrs[0] as UI.MethodEditor.EditorParamAttribute;
//       //                 descriptions.Add(attr.Description);
//       //                 editors.Add(attr.EditorType);
//       //             }
//       //             else
//       //             {
//       //                 descriptions.Add("");
//       //                 editors.Add(null);
//       //             }             
//       //         }                
//       //     }
            
//       //     this.propertyGridMethods.SetInformationToCollect(types.ToArray(), categories.ToArray(), names.ToArray(),
//       //         descriptions.ToArray(), editors.ToArray(), firstSettings.ToArray());

//       // }

//        private void buttonExport_Click(object sender, EventArgs e)
//        {
//            Exporter exporter = new Exporter(this.dataBase);
//            if (exporter.ShowDialog() == DialogResult.Cancel) return;

//            List<Variable> toWrite = new List<Variable>();
//            foreach (ListViewItem item in this.listViewVariables.SelectedItems)
//            {
//                Variable var = null;

//                if (item.Tag is CeresFileDataBase.CeresFileEntry)
//                {
//                    CeresFileDataBase.CeresFileEntry entry = item.Tag as CeresFileDataBase.CeresFileEntry;


//                    //TODO CHANGE
//                    if (!VariableSource.ContainsVariable(entry.Header.VarName, entry.Header.ExperimentName))
//                    {
//                        MessageBox.Show("Please load this variable before trying to export it");
//                        continue;
//                    }
//                    var = VariableSource.GetVariable(entry.Header);
//                }
//                else
//                {
//                    //TODO CHANGE
//                    if (!VariableSource.ContainsVariable(item.Tag as VariableHeader))
//                    {
//                        MessageBox.Show("Please load this variable before trying to export it");
//                        continue;
//                    }
//                    var = VariableSource.GetVariable(item.Tag as VariableHeader);
//                }
//                toWrite.Add(var);
//            }

//            exporter.Writer.WriteVariables(toWrite.ToArray(), exporter.FilePath);                

//        }

//        private void CeresDBManager_Shown(object sender, EventArgs e)
//        {
//            Variable[] vars = VariableSource.Variables;
//            List<VariableHeader> toAdd = new List<VariableHeader>();
//            foreach (Variable var in vars)
//            {
//                bool present = false;
//                for (int i = 0; i < this.listViewVariables.Items.Count; i++)
//                {
//                    CeresFileDataBase.CeresFileEntry entry = this.listViewVariables.Items[i].Tag as
//                        CeresFileDataBase.CeresFileEntry;
//                    if (entry == null) continue;
//                    if (entry.Header == var.Header)
//                    {
//                        present = true;
//                        break;
//                    }
//                }

//                if (!present) toAdd.Add(var.Header);
//            }

//            foreach (VariableHeader header in toAdd)
//            {
//                ListViewItem item = new ListViewItem();
//                item.Name = header.ToString();
//                item.Text = header.ToString();
//                item.Tag = header;
//                this.listViewVariables.Items.Add(item);  
//            }
//        }

//        private void buttonUnload_Click(object sender, EventArgs e)
//        {
//            for (int i = 0; i < this.listViewVariables.SelectedItems.Count; i++)
//            {
//                VariableHeader header = null;
//                if (this.listViewVariables.SelectedItems[i].Tag is CeresFileDataBase.CeresFileEntry)
//                {
//                    header = (this.listViewVariables.SelectedItems[i].Tag as CeresFileDataBase.CeresFileEntry).Header;
//                }
//                else
//                {
//                    header = this.listViewVariables.SelectedItems[i].Tag as VariableHeader;
//                }


//                if (!VariableSource.ContainsVariable(header))
//                {
//                    VariableSource.RemoveQueuedHeader(header);
//                }
//                VariableSource.RemoveVariable(header);
                
//            }

//            foreach (ListViewItem item in this.listViewVariables.SelectedItems)
//            {
//                this.listViewVariables.Items.Remove(item);
//            }
//        }

//        private void buttonCreateNewVar_Click(object sender, EventArgs e)
//        {
//            GeneralCreateVariable create = new GeneralCreateVariable();
//            create.ShowDialog();

//            foreach (VariableHeader header in VariableSource.AllVariablesKnown)
//            {
//                bool found = false;
//                foreach (ListViewItem look in this.listViewVariables.Items)
//                {
//                    if (look.Name == header.ToString())
//                    {
//                        found = true;
//                        break;
//                    }
//                }

//                if (found) continue;

//                ListViewItem item = new ListViewItem();
//                item.Name = header.ToString();
//                item.Text = header.ToString();
//                item.Tag = header;
//                this.listViewVariables.Items.Add(item);
//            }
//        }

//        private void CeresDBManager_FormClosing(object sender, FormClosingEventArgs e)
//        {
//            this.ceresVariableSelector1.OnExit();
//        }
//    }
//}