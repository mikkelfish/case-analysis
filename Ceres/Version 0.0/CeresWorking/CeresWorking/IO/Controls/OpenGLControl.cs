using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Sharp3D.Math.Core;
using Tao.OpenGl;
using System.Windows.Forms;
using System.ComponentModel;

namespace CeresBase.IO.Controls
{
    public class OpenGLControl : Tao.Platform.Windows.SimpleOpenGlControl
    {
        private Color backgroundColor;
        private double zoom  = 0.5;

        /*
        * A "magic" number which describes the default size of the view volume.
        * The view volume extends from -MAGIC to MAGIC along X, Y, and Z.
        * Distance from eye to center of 3-D box when in perspective mode:
        */
        protected const double MAGIC = 1.5;
        protected const double ZMAGIC = 1.8;
        protected const double EYE_DIST = 4.0;

        private double frontClip;

        public double FrontClip
        {
            get { return frontClip; }
            set { frontClip = value; }
        }

        private bool showBox;
        public bool ShowBox
        {
            get { return showBox; }
            set { showBox = value;
            this.Invalidate();
        }
        }
	
        private Sharp3D.Math.Core.Matrix4F transformMatrix;
        public Matrix4F TransformMatrix
        {
            get
            {
                return this.transformMatrix;
            }
            set
            {
                this.transformMatrix = value;
            }
        }

        public Color BackGroundColor
        {
            get
            {
                return this.backgroundColor;
            }
            set
            {
                this.backgroundColor = value;
                Gl.glClearColor((float)this.backgroundColor.R / 255F, (float)this.backgroundColor.G / 255F,
                       (float)this.backgroundColor.G / 255F, (float)this.backgroundColor.A / 255F);
            }
        }

        [DefaultValue(.5)]
        public double Zoom
        {
            get
            {
                return this.zoom;
            }
            set
            {
                this.zoom = value;
            }
        }

        private void parentResize(object sender, EventArgs e)
        {
            if (this.Parent != sender)
            {
                return;
            }
            this.Invalidate();
        }


        public OpenGLControl()
        {
            this.transformMatrix = Matrix4F.Identity;
            this.InitializeContexts();
            this.initializeView();
            this.Paint += new PaintEventHandler(this.rendererControl_Paint);
            base.Resize += new EventHandler(this.parentResize);
            this.Resize += new EventHandler(this.reshapeView);
            this.BackGroundColor = Color.White;
        }

        private List<double[,]> threeDVerts = new List<double[,]>();
        public void AddThreeDObject(double[,] obj)
        {
            this.threeDVerts.Add(obj);
            this.findMinMaxes();
            this.Invalidate();
        }

        public void RemoveThreeDobject(double[,] obj)
        {
            this.threeDVerts.Remove(obj);
            this.findMinMaxes();
            this.Invalidate();
        }

        private List<double[,]> twoDVerts = new List<double[,]>();
        public List<double[,]> TwoDVertices
        {
            get { return twoDVerts; }
        }

        private double[] mins;
        public double[] Mins
        {
            get
            {
                return this.mins;
            }
        }

        private double[] maxes;
        public double[] Maxes
        {
            get
            {
                return this.maxes;
            }
        }

        private void findMinMaxes()
        {
            this.mins = new double[3];
            this.maxes = new double[3];
            for (int i = 0; i < 3; i++)
            {
                mins[i] = double.MaxValue;
                maxes[i] = double.MinValue;
            }

            foreach (double[,] obj in this.threeDVerts)
            {
                int numVerts = obj.GetLength(0);
                for (int i = 0; i < numVerts; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (obj[i, j] > maxes[j])
                        {
                            maxes[j] = obj[i, j];
                        }
                        if (obj[i, j] < mins[j])
                        {
                            mins[j] = obj[i, j];
                        }
                    }                    
                }
            }
        }

        private void reshapeView(object sender, EventArgs e)
        {
            this.resetViewportAndProjection();
        }

        private void render3D()
        {
            Gl.glColor3f(0F, 0F, 0F);

            foreach (double[,] obj in this.threeDVerts)
            {
                int numVerts = obj.GetLength(0);

                Gl.glBegin(Gl.GL_LINE_STRIP);

                for (int i = 0; i < numVerts; i++)
                {
                    Gl.glVertex3dv(ref obj[i, 0]);
                }
                Gl.glEnd();
            }

            Gl.glColor3f(1F, 0F, 0F);
            if (this.showBox && mins != null && maxes != null)
            {
                #region Box
                Gl.glBegin(Gl.GL_LINE_LOOP);
                Gl.glVertex3d(maxes[0], maxes[1], mins[2]);			// Top Right Of The Quad (Top)
                Gl.glVertex3d(mins[0], maxes[1], mins[2]);			// Top Left Of The Quad (Top)
                Gl.glVertex3d(mins[0], maxes[1], maxes[2]);			// Bottom Left Of The Quad (Top)
                Gl.glVertex3d(maxes[0], maxes[1], maxes[2]);			// Bottom Right Of The Quad (Top)
                Gl.glEnd();

                Gl.glBegin(Gl.GL_LINE_LOOP);
                Gl.glVertex3d(maxes[0], mins[1], maxes[2]);			// Top Right Of The Quad (Bottom)
                Gl.glVertex3d(mins[0], mins[1], maxes[2]);			// Top Left Of The Quad (Bottom)
                Gl.glVertex3d(mins[0], mins[1], mins[2]);			// Bottom Left Of The Quad (Bottom)
                Gl.glVertex3d(maxes[0], mins[1], mins[2]);			// Bottom Right Of The Quad (Bottom)
                Gl.glEnd();

                Gl.glBegin(Gl.GL_LINE_LOOP);
                Gl.glVertex3d(maxes[0], maxes[1], maxes[2]);			// Top Right Of The Quad (Front)
                Gl.glVertex3d(mins[0], maxes[1], maxes[2]);			// Top Left Of The Quad (Front)
                Gl.glVertex3d(mins[0], mins[1], maxes[2]);			// Bottom Left Of The Quad (Front)
                Gl.glVertex3d(maxes[0], mins[1], maxes[2]);			// Bottom Right Of The Quad (Front)
                Gl.glEnd();

                Gl.glBegin(Gl.GL_LINE_LOOP);
                Gl.glVertex3d(maxes[0], mins[1], mins[2]);			// Bottom Left Of The Quad (Back)
                Gl.glVertex3d(mins[0], mins[1], mins[2]);			// Bottom Right Of The Quad (Back)
                Gl.glVertex3d(mins[0], maxes[1], mins[2]);			// Top Right Of The Quad (Back)
                Gl.glVertex3d(maxes[0], maxes[1], mins[2]);			// Top Left Of The Quad (Back)
                Gl.glEnd();

                Gl.glBegin(Gl.GL_LINE_LOOP);
                Gl.glVertex3d(mins[0], maxes[1], maxes[2]);			// Top Right Of The Quad (Left)
                Gl.glVertex3d(mins[0], maxes[1], mins[2]);			// Top Left Of The Quad (Left)
                Gl.glVertex3d(mins[0], mins[1], mins[2]);			// Bottom Left Of The Quad (Left)
                Gl.glVertex3d(mins[0], mins[1], maxes[2]);			// Bottom Right Of The Quad (Left)
                Gl.glEnd();

                Gl.glBegin(Gl.GL_LINE_LOOP);
                Gl.glVertex3d(maxes[0], maxes[1], mins[2]);			// Top Right Of The Quad (Right)
                Gl.glVertex3d(maxes[0], maxes[1], maxes[2]);			// Top Left Of The Quad (Right)
                Gl.glVertex3d(maxes[0], mins[1], maxes[2]);			// Bottom Left Of The Quad (Right)
                Gl.glVertex3d(maxes[0], mins[1], mins[2]);			// Bottom Right Of The Quad (Right)
                Gl.glEnd();

                #endregion
            }
            
        }

        private void rendererControl_Paint(object sender, PaintEventArgs e)
        {
            this.resetViewportAndProjection();
            this.render3D();
            // Gl.glMultMatrixd((double[])this.ctm);
           // this.controller.Render3D();
            this.set2D();
            //this.controller.Render2D();
        }

        protected virtual void initializeView()
        {
            Gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glDepthFunc(Gl.GL_LEQUAL);
            Gl.glShadeModel(Gl.GL_SMOOTH);
            Gl.glClearDepth(1.0f);							// Depth Buffer Setup
            Gl.glHint(Gl.GL_PERSPECTIVE_CORRECTION_HINT, Gl.GL_NICEST);			// Really Nice Perspective Calculations
            this.resetViewportAndProjection();
        }

        //TODO setup clipping plane and camera
        private void resetViewportAndProjection()
        {
            if (this.Height == 0)								// Prevent A Divide By Zero By
                this.Height = 1;							// Making Height Equal One

            if (this.Width == 0)
                this.Width = 1;

            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glViewport(0, 0, Width, Height);			// Reset The Current Viewport

            if (this.frontClip < 0.0F)
            {
                this.frontClip = 0.0F;
            }
            else if (this.frontClip >= 1.0F)
            {
                this.frontClip = 0.99F;
            }

            ///* orthographic */
            //if (!this.perspective)
            //{
            #region Ortho
            double x, y, near, far;

            if (this.Width > this.Height)
            {
                x = OpenGLControl.MAGIC / this.zoom;
                y = OpenGLControl.MAGIC / this.zoom * this.Height / this.Width;
            }
            else
            {
                x = OpenGLControl.MAGIC / this.zoom * this.Width / this.Height;
                y = OpenGLControl.MAGIC / this.zoom;
            }
            near = 2.0 * OpenGLControl.ZMAGIC * this.frontClip;
            far = 2.0 * OpenGLControl.ZMAGIC;

            foreach (double[,] obj in this.threeDVerts)
            {
                //double minx = double.MaxValue;
                double maxx = double.MinValue;
                //double miny = double.MaxValue;
                double maxy = double.MinValue;
                for (int i = 0; i < obj.GetLength(0); i++)
                {
                    double tempx, tempy;
                    //if (Math.Abs(obj[i, 0] < minx)) minx = obj[i, 0];
                    if (this.Width > this.Height)
                    {
                        tempx = obj[i, 0] / this.zoom;
                        tempy = obj[i, 1] / this.zoom * this.Height / this.Width;
                    }
                    else
                    {
                        tempx = obj[i, 0] / this.zoom * this.Width / this.Height;
                        tempy = obj[i, 1] / this.zoom;
                    }



                    if (tempx > maxx) maxx = tempx;
                    //if (obj[i, 1] < miny) miny = obj[i, 1];
                    if (tempy > maxy) maxy = tempy;


                    double testFar = 2.0* obj[i, 2] * OpenGLControl.ZMAGIC;
                    if (testFar > far)
                    {
                        far = testFar;
                    }
                }

                x = maxx;
                y = maxy;
            }


            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Gl.glOrtho(-x, x, -y, y, near, far);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glTranslated(0.0, 0.0, -far/2.0);

            float[] trans = new float[16];
            for (int i = 0; i < 16; i++)
            {
                trans[i] = this.transformMatrix[i];
            }

            Gl.glMultMatrixf(trans);

            //glClipPlane(GL_CLIP_PLANE0, eqnleft);
            //glClipPlane(GL_CLIP_PLANE1, eqnright);
            //glClipPlane(GL_CLIP_PLANE2, eqntop);
            //glClipPlane(GL_CLIP_PLANE3, eqnbottom);
            //glClipPlane(GL_CLIP_PLANE4, eqnback);
            //glClipPlane(GL_CLIP_PLANE5, eqnfront);

            //Gl.glFogf(Gl.GL_FOG_START, 0.0F);
            //Gl.glFogf(Gl.GL_FOG_END, (float)far);
            #endregion
            //}
            //else
            //{
            #region Perspective
            //    double x, y, near, far;

            //    near = OpenGLControl.EYE_DIST - OpenGLControl.ZMAGIC + (2.0 * OpenGLControl.MAGIC * this.frontClip);
            //    far = OpenGLControl.EYE_DIST + OpenGLControl.ZMAGIC;

            //    if (this.Width > this.Height)
            //    {
            //        x = OpenGLControl.MAGIC / OpenGLControl.EYE_DIST * near;
            //        y = OpenGLControl.MAGIC / OpenGLControl.EYE_DIST * near * this.Height / this.Width;
            //    }
            //    else
            //    {
            //        x = OpenGLControl.MAGIC / OpenGLControl.EYE_DIST * near * this.Width / this.Height;
            //        y = OpenGLControl.MAGIC / OpenGLControl.EYE_DIST * near;
            //    }

            //    Gl.glMatrixMode(Gl.GL_PROJECTION);
            //    Gl.glLoadIdentity();
            //    Gl.glFrustum(-x, x, -y, y, near, far);

            //    Gl.glMatrixMode(Gl.GL_MODELVIEW);
            //    Gl.glLoadIdentity();
            //    Gl.glTranslated(0.0, 0.0, -OpenGLControl.EYE_DIST);
            //    Gl.glScaled(zoom, zoom, 1.0);
            //    Gl.glMultMatrixd((double[])this.ctm);

            //    //Gl.glClipPlane(GL_CLIP_PLANE0, eqnleft);
            //    //Gl.glClipPlane(GL_CLIP_PLANE1, eqnright);
            //    //Gl.glClipPlane(GL_CLIP_PLANE2, eqntop);
            //    //Gl.glClipPlane(GL_CLIP_PLANE3, eqnbottom);
            //    //Gl.glClipPlane(GL_CLIP_PLANE4, eqnback);
            //    //Gl.glClipPlane(GL_CLIP_PLANE5, eqnfront);

            //    Gl.glFogf(Gl.GL_FOG_START, (float)(OpenGLControl.EYE_DIST - OpenGLControl.ZMAGIC));
            //    Gl.glFogf(Gl.GL_FOG_END, (float)far);
            #endregion
            //}
            Gl.glEnable(Gl.GL_DEPTH_TEST);
        }

        private void set2D()
        {
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Gl.glOrtho(0.0, (double)this.Width,
                0.0, (double)this.Height, -1.0, 1.0);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Gl.glDisable(Gl.GL_DEPTH_TEST);
        }

    }
}
