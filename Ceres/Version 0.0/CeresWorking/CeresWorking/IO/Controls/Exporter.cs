using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CeresBase.IO.Controls
{
    public partial class Exporter : Form
    {
        //private CeresFileDataBase database;


        public Exporter()
        {
            InitializeComponent();

        }

        //public Exporter(CeresFileDataBase database)
        //{
        //    InitializeComponent();
        //    this.database = database;
        //}

        public ICeresWriter Writer
        {
            get { return this.listBox1.SelectedItem as ICeresWriter; }
        }

        public string FilePath
        {
            get { return this.textBox1.Text; }
        }


        private void Exporter_Load(object sender, EventArgs e)
        {
            ICeresWriter[] writers = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<ICeresWriter>(null);
            //foreach (ICeresWriter writer in writers)
            //{
            //    writer.Database = this.database;
            //}
            this.listBox1.Items.AddRange(writers);
            this.listBox1.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text == string.Empty)
            {
                MessageBox.Show("Please enter a save path.");
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private static FolderBrowserDialog selectFolder = new FolderBrowserDialog();

        private void button1_Click(object sender, EventArgs e)
        {

            selectFolder.ShowDialog();
            this.textBox1.Text = selectFolder.SelectedPath;


        }
    }
}