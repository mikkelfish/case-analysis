//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;
//using System.Windows.Forms;
//using CeresBase.Data;
//using System.Reflection;

//namespace CeresBase.IO.Controls
//{
//    public partial class MethodAssociation : Form
//    {
//        public MethodAssociation()
//        {
//            InitializeComponent();
//        }

     
//        public void Build(Type[][] varTypes, MethodInfo info, ConstructorInfo constructor)
//        {
//            for (int i = 0; i < 2; i++)
//            {
//                bool whichOne = i == 1;
//                VariableProcessingSetup.ProcessingAssociation methAssocation =
//                               VariableProcessingSetup.GetAssociation(info, constructor, varTypes, whichOne);
//                if (methAssocation == null)
//                {
//                    methAssocation = VariableProcessingSetup.AddAssociation(info, constructor, varTypes, whichOne);
//                }

//                List<Type> paramTypes = new List<Type>();
//                List<Type> editors = new List<Type>();
//                List<string> names = new List<string>();
//                List<string> categories = new List<string>();
//                List<string> descriptions = new List<string>();
//                foreach (ParameterInfo para in methAssocation.Parameters.Keys)
//                {
//                    object[] args = para.GetCustomAttributes(typeof(UI.MethodEditor.EditorParamAttribute), true);
//                    if (args.Length == 0)
//                        continue;
//                    UI.MethodEditor.EditorParamAttribute attr = args[0] as UI.MethodEditor.EditorParamAttribute;
//                    paramTypes.Add(para.ParameterType);
//                    editors.Add(attr.EditorType);
//                    names.Add(para.Name);
//                    categories.Add(info.Name);
//                    descriptions.Add(attr.Description);
//                }

//                if (i == 0)
//                {
//                    this.propertyGridMethod.Tag = methAssocation;
//                    this.propertyGridMethod.SetInformationToCollect(paramTypes.ToArray(), categories.ToArray(),
//                        names.ToArray(), descriptions.ToArray(), editors.ToArray(), null);
//                }
//                else
//                {
//                    this.propertyGridOutput.Tag = methAssocation;
//                    this.propertyGridOutput.SetInformationToCollect(paramTypes.ToArray(), categories.ToArray(),
//                        names.ToArray(), descriptions.ToArray(), editors.ToArray(), null);
//                }

//                if (i == 0)
//                {
//                    if ((info.ReturnType == typeof(Variable) || info.ReturnType.IsSubclassOf(typeof(Variable))))
//                    {
//                        break;
//                    }
//                }
//            }           
//        }

//        private void button1_Click(object sender, EventArgs e)
//        {
//            foreach (string key in this.propertyGridMethod.Results.Keys)
//            {
//                (this.propertyGridMethod.Tag as VariableProcessingSetup.ProcessingAssociation)[key].ReadOnly = this.propertyGridMethod.ReadOnlyDefaults[key];
//                (this.propertyGridMethod.Tag as VariableProcessingSetup.ProcessingAssociation)[key].Value = this.propertyGridMethod.Results[key];
//            }

//            foreach (string key in this.propertyGridOutput.Results.Keys)
//            {
//                (this.propertyGridOutput.Tag as VariableProcessingSetup.ProcessingAssociation)[key].ReadOnly = this.propertyGridOutput.ReadOnlyDefaults[key];
//                (this.propertyGridOutput.Tag as VariableProcessingSetup.ProcessingAssociation)[key].Value = this.propertyGridOutput.Results[key];
//            }
//        }
//    }
//}