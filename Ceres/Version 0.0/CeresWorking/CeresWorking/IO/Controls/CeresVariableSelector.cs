//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Drawing;
//using System.Data;
//using System.Text;
//using System.Windows.Forms;
//using CeresBase.Data;

//namespace CeresBase.IO.Controls
//{
//    public partial class CeresVariableSelector : UserControl
//    {
//        private CeresFileDataBase db;

//        private List<VariableHeader> selectedHeaders = new List<VariableHeader>();

//        public CeresVariableSelector()
//        {
//            InitializeComponent();
//            this.treeView1.AfterCheck += new TreeViewEventHandler(treeView1_AfterCheck);
//            this.treeView1.MouseDown += new MouseEventHandler(treeView1_MouseDown);
//        }

//        void treeView1_MouseDown(object sender, MouseEventArgs e)
//        {
//            if (e.Button != MouseButtons.Right) return;
//            TreeViewHitTestInfo info = this.treeView1.HitTest(e.Location);
//            if (info == null || info.Node == null) return;
//            this.treeView1.SelectedNode = info.Node;
//        }

        
//        void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
//        {
//            if (!e.Node.Checked)
//            {
//                e.Node.BackColor = Color.White;
//            }
//            else
//            {
//                CeresFileDataBase.CeresFileEntry entry = e.Node.Tag as CeresFileDataBase.CeresFileEntry;
//                if (entry != null)
//                {
//                    if (VariableSource.ContainsVariable(entry.Header)) e.Node.BackColor = Color.LightGreen;
//                    else e.Node.BackColor = Color.Red;
//                }
//            }

//            foreach (TreeNode node in e.Node.Nodes)
//            {
//                node.Checked = e.Node.Checked;
//            }

//        }

//        public CeresFileDataBase.CeresFileEntry GetSelectedEntry()
//        {
//            return this.treeView1.SelectedNode.Tag as CeresFileDataBase.CeresFileEntry;
//        }

//        public event TreeViewEventHandler NodeChecked
//        {
//            add
//            {
//                this.treeView1.AfterCheck += value;
//            }
//            remove
//            {
//                this.treeView1.AfterCheck -= value;
//            }
//        }

//        public void ReadDataBase(CeresFileDataBase database)
//        {
//            this.db = database;
//            Dictionary<string, List<CeresFileDataBase.CeresFileEntry>> headers = database.GetDataBaseInformation();
//            this.treeView1.Nodes.Clear();
//            this.treeView1.SuspendLayout();
//            foreach (string expt in headers.Keys)
//            {
//                string[] splits = expt.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
//                TreeNode rootNode = null;
//                for (int i = 0; i < splits.Length; i++)
//                {
//                    if (rootNode == null)
//                    {
//                        if (!this.treeView1.Nodes.ContainsKey(splits[i]))
//                            rootNode = this.treeView1.Nodes.Add(splits[i], splits[i]);
//                        else rootNode = this.treeView1.Nodes.Find(splits[i], false)[0];
//                    }
//                    else
//                    {
//                        if (!rootNode.Nodes.ContainsKey(splits[i]))
//                            rootNode = rootNode.Nodes.Add(splits[i], splits[i]);
//                        else rootNode = rootNode.Nodes.Find(splits[i], false)[0];
//                    }
//                }

//                foreach (CeresFileDataBase.CeresFileEntry header in headers[expt])
//                {
//                    TreeNode newNode = new TreeNode(header.Header.Description);
//                    newNode.Tag = header;
//                    rootNode.Nodes.Add(newNode);
//                    if (VariableSource.ContainsVariable(header.Header.VarName, 
//                        header.Header.ExperimentName))
//                    {
//                        newNode.Checked = true;
//                        newNode.BackColor = Color.LightGreen;
//                    }
//                }
//            }
//            this.treeView1.ResumeLayout();
//        }

//        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
//        {
//            CeresFileDataBase.CeresFileEntry entry = (e.Node.Tag as CeresFileDataBase.CeresFileEntry);
//            if(entry == null) return;
//            this.propertyGrid1.SelectedObject = entry.Header;

//            if (!this.selectedHeaders.Contains(entry.Header))
//            {
//                this.selectedHeaders.Add(entry.Header);
//            }
//        }

//        public void OnExit()
//        {
//            if (this.db != null)
//            {
//                foreach (VariableHeader header in this.selectedHeaders)
//                {
//                    this.db.ChangeDescription(header, header.Description);
//                }
//            }
//        }

//        private void deleteFromDBToolStripMenuItem_Click(object sender, EventArgs e)
//        {
//            if (this.treeView1.SelectedNode == null) return;
//            this.deleteNode(this.treeView1.SelectedNode);
            
//        }

//        private void deleteNode(TreeNode node)
//        {
//            CeresFileDataBase.CeresFileEntry entry =
//                node.Tag as CeresFileDataBase.CeresFileEntry;
//            if (entry != null)
//            {

//                TreeNode nodeParent = node.Parent;

//                node.Remove();
//                if (nodeParent.Nodes.Count == 0)
//                {
//                    nodeParent.Remove();
//                }
//                db.DeleteFile(entry.FileName);
//                return;
//            }

//            TreeNode[] nodesToRemove = new TreeNode[node.Nodes.Count];
//            node.Nodes.CopyTo(nodesToRemove, 0);
//            foreach (TreeNode child in nodesToRemove)
//            {
//                this.deleteNode(child);
//            }
//        }
//    }
//}
