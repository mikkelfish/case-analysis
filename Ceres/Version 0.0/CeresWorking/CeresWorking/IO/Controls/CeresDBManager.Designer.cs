//namespace CeresBase.IO.Controls
//{
//    partial class CeresDBManager
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
//            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
//            this.importFileIntoDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
//            this.autoImportDirectoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
//            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
//            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
//            this.selectStorageDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
//            this.ceresVariableSelector1 = new CeresBase.IO.Controls.CeresVariableSelector();
//            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
//            this.buttonCreateNewVar = new System.Windows.Forms.Button();
//            this.buttonExport = new System.Windows.Forms.Button();
//            this.buttonCancel = new System.Windows.Forms.Button();
//            this.buttonUnload = new System.Windows.Forms.Button();
//            this.buttonSelect = new System.Windows.Forms.Button();
//            this.groupBox1 = new System.Windows.Forms.GroupBox();
//            this.listViewVariables = new System.Windows.Forms.ListView();
//            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
//            this.menuStrip1.SuspendLayout();
//            this.splitContainer1.Panel1.SuspendLayout();
//            this.splitContainer1.Panel2.SuspendLayout();
//            this.splitContainer1.SuspendLayout();
//            this.groupBox1.SuspendLayout();
//            this.SuspendLayout();
//            // 
//            // menuStrip1
//            // 
//            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
//            this.fileToolStripMenuItem,
//            this.editToolStripMenuItem});
//            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
//            this.menuStrip1.Name = "menuStrip1";
//            this.menuStrip1.Size = new System.Drawing.Size(812, 24);
//            this.menuStrip1.TabIndex = 0;
//            this.menuStrip1.Text = "menuStrip1";
//            // 
//            // fileToolStripMenuItem
//            // 
//            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
//            this.importFileIntoDBToolStripMenuItem,
//            this.autoImportDirectoriesToolStripMenuItem,
//            this.exitToolStripMenuItem});
//            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
//            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
//            this.fileToolStripMenuItem.Text = "File";
//            // 
//            // importFileIntoDBToolStripMenuItem
//            // 
//            this.importFileIntoDBToolStripMenuItem.Name = "importFileIntoDBToolStripMenuItem";
//            this.importFileIntoDBToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
//            this.importFileIntoDBToolStripMenuItem.Text = "Import File Into DB";
//            this.importFileIntoDBToolStripMenuItem.Click += new System.EventHandler(this.importFileIntoDBToolStripMenuItem_Click);
//            // 
//            // autoImportDirectoriesToolStripMenuItem
//            // 
//            this.autoImportDirectoriesToolStripMenuItem.Name = "autoImportDirectoriesToolStripMenuItem";
//            this.autoImportDirectoriesToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
//            this.autoImportDirectoriesToolStripMenuItem.Text = "Auto-Import Directories";
//            // 
//            // exitToolStripMenuItem
//            // 
//            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
//            this.exitToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
//            this.exitToolStripMenuItem.Text = "Exit";
//            // 
//            // editToolStripMenuItem
//            // 
//            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
//            this.selectStorageDirectoryToolStripMenuItem});
//            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
//            this.editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
//            this.editToolStripMenuItem.Text = "Edit";
//            // 
//            // selectStorageDirectoryToolStripMenuItem
//            // 
//            this.selectStorageDirectoryToolStripMenuItem.Name = "selectStorageDirectoryToolStripMenuItem";
//            this.selectStorageDirectoryToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
//            this.selectStorageDirectoryToolStripMenuItem.Text = "Select Storage Directory";
//            this.selectStorageDirectoryToolStripMenuItem.Click += new System.EventHandler(this.selectStorageDirectoryToolStripMenuItem_Click);
//            // 
//            // ceresVariableSelector1
//            // 
//            this.ceresVariableSelector1.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.ceresVariableSelector1.Location = new System.Drawing.Point(0, 0);
//            this.ceresVariableSelector1.Name = "ceresVariableSelector1";
//            this.ceresVariableSelector1.Size = new System.Drawing.Size(812, 261);
//            this.ceresVariableSelector1.TabIndex = 1;
//            // 
//            // splitContainer1
//            // 
//            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
//            this.splitContainer1.Name = "splitContainer1";
//            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
//            // 
//            // splitContainer1.Panel1
//            // 
//            this.splitContainer1.Panel1.Controls.Add(this.ceresVariableSelector1);
//            // 
//            // splitContainer1.Panel2
//            // 
//            this.splitContainer1.Panel2.Controls.Add(this.buttonCreateNewVar);
//            this.splitContainer1.Panel2.Controls.Add(this.buttonExport);
//            this.splitContainer1.Panel2.Controls.Add(this.buttonCancel);
//            this.splitContainer1.Panel2.Controls.Add(this.buttonUnload);
//            this.splitContainer1.Panel2.Controls.Add(this.buttonSelect);
//            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
//            this.splitContainer1.Size = new System.Drawing.Size(812, 563);
//            this.splitContainer1.SplitterDistance = 261;
//            this.splitContainer1.TabIndex = 3;
//            // 
//            // buttonCreateNewVar
//            // 
//            this.buttonCreateNewVar.Location = new System.Drawing.Point(685, 158);
//            this.buttonCreateNewVar.Name = "buttonCreateNewVar";
//            this.buttonCreateNewVar.Size = new System.Drawing.Size(115, 23);
//            this.buttonCreateNewVar.TabIndex = 11;
//            this.buttonCreateNewVar.Text = "Create New Var";
//            this.buttonCreateNewVar.UseVisualStyleBackColor = true;
//            this.buttonCreateNewVar.Click += new System.EventHandler(this.buttonCreateNewVar_Click);
//            // 
//            // buttonExport
//            // 
//            this.buttonExport.Location = new System.Drawing.Point(685, 106);
//            this.buttonExport.Name = "buttonExport";
//            this.buttonExport.Size = new System.Drawing.Size(115, 23);
//            this.buttonExport.TabIndex = 10;
//            this.buttonExport.Text = "Export Variable";
//            this.buttonExport.UseVisualStyleBackColor = true;
//            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
//            // 
//            // buttonCancel
//            // 
//            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//            this.buttonCancel.Location = new System.Drawing.Point(685, 77);
//            this.buttonCancel.Name = "buttonCancel";
//            this.buttonCancel.Size = new System.Drawing.Size(115, 23);
//            this.buttonCancel.TabIndex = 6;
//            this.buttonCancel.Text = "Cancel";
//            this.buttonCancel.UseVisualStyleBackColor = true;
//            this.buttonCancel.Click += new System.EventHandler(this.button3_Click);
//            // 
//            // buttonUnload
//            // 
//            this.buttonUnload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//            this.buttonUnload.Location = new System.Drawing.Point(685, 48);
//            this.buttonUnload.Name = "buttonUnload";
//            this.buttonUnload.Size = new System.Drawing.Size(115, 23);
//            this.buttonUnload.TabIndex = 5;
//            this.buttonUnload.Text = "Unload Variables";
//            this.buttonUnload.UseVisualStyleBackColor = true;
//            this.buttonUnload.Click += new System.EventHandler(this.buttonUnload_Click);
//            // 
//            // buttonSelect
//            // 
//            this.buttonSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//            this.buttonSelect.Location = new System.Drawing.Point(685, 19);
//            this.buttonSelect.Name = "buttonSelect";
//            this.buttonSelect.Size = new System.Drawing.Size(115, 23);
//            this.buttonSelect.TabIndex = 4;
//            this.buttonSelect.Text = "Select Variables";
//            this.buttonSelect.UseVisualStyleBackColor = true;
//            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
//            // 
//            // groupBox1
//            // 
//            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
//                        | System.Windows.Forms.AnchorStyles.Left)
//                        | System.Windows.Forms.AnchorStyles.Right)));
//            this.groupBox1.Controls.Add(this.listViewVariables);
//            this.groupBox1.Location = new System.Drawing.Point(3, 3);
//            this.groupBox1.Name = "groupBox1";
//            this.groupBox1.Size = new System.Drawing.Size(676, 178);
//            this.groupBox1.TabIndex = 3;
//            this.groupBox1.TabStop = false;
//            this.groupBox1.Text = "Loaded Variables";
//            // 
//            // listViewVariables
//            // 
//            this.listViewVariables.Activation = System.Windows.Forms.ItemActivation.OneClick;
//            this.listViewVariables.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.listViewVariables.Location = new System.Drawing.Point(3, 16);
//            this.listViewVariables.Name = "listViewVariables";
//            this.listViewVariables.Size = new System.Drawing.Size(670, 159);
//            this.listViewVariables.TabIndex = 0;
//            this.listViewVariables.UseCompatibleStateImageBehavior = false;
//            // 
//            // CeresDBManager
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.ClientSize = new System.Drawing.Size(812, 482);
//            this.Controls.Add(this.splitContainer1);
//            this.Controls.Add(this.menuStrip1);
//            this.MainMenuStrip = this.menuStrip1;
//            this.Name = "CeresDBManager";
//            this.ShowInTaskbar = false;
//            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
//            this.Text = "CeresDBManager";
//            this.Shown += new System.EventHandler(this.CeresDBManager_Shown);
//            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CeresDBManager_FormClosing);
//            this.menuStrip1.ResumeLayout(false);
//            this.menuStrip1.PerformLayout();
//            this.splitContainer1.Panel1.ResumeLayout(false);
//            this.splitContainer1.Panel2.ResumeLayout(false);
//            this.splitContainer1.ResumeLayout(false);
//            this.groupBox1.ResumeLayout(false);
//            this.ResumeLayout(false);
//            this.PerformLayout();

//        }

//        #endregion

//        private System.Windows.Forms.MenuStrip menuStrip1;
//        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
//        private System.Windows.Forms.ToolStripMenuItem importFileIntoDBToolStripMenuItem;
//        private System.Windows.Forms.ToolStripMenuItem autoImportDirectoriesToolStripMenuItem;
//        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
//        private CeresVariableSelector ceresVariableSelector1;
//        private System.Windows.Forms.SplitContainer splitContainer1;
//        private System.Windows.Forms.Button buttonUnload;
//        private System.Windows.Forms.Button buttonSelect;
//        private System.Windows.Forms.GroupBox groupBox1;
//        private System.Windows.Forms.Button buttonCancel;
//        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
//        private System.Windows.Forms.ToolStripMenuItem selectStorageDirectoryToolStripMenuItem;
//        private System.Windows.Forms.ListView listViewVariables;
//        private System.Windows.Forms.Button buttonExport;
//        private System.ComponentModel.BackgroundWorker backgroundWorker1;
//        private System.Windows.Forms.Button buttonCreateNewVar;
//    }
//}