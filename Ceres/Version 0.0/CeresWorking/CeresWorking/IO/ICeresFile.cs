using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;

namespace CeresBase.IO
{
    [CeresCreated("Mikkel", "03/20/06")]
    public interface ICeresFile
    {
        [CeresCreated("Mikkel", "03/20/06")]
        Type VariableType { get;}

        [CeresCreated("Mikkel", "03/20/06")]
        Type ShapeType { get;}

        [CeresCreated("Mikkel", "03/20/06")]
        string FilePath { get;}       
    }
}
