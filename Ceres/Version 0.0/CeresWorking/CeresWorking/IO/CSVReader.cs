using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CeresBase.IO
{
    public static class CSVReader
    {
        public static object[][] ReadCSV(string filename, int max)
        {
            List<object>[] toRet = null;
            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        string[] parts = line.Split(',');
                        int amt = parts.Length;
                        if (parts.Length > max)
                        {
                            amt = max;
                        }
                        
                        if (toRet == null)
                        {
                            toRet = new List<object>[amt];
                        }
                        for (int i = 0; i < amt; i++)
                        {
                            if (toRet[i] == null) toRet[i] = new List<object>();
                            toRet[i].Add(parts[i]);
                        }
                        line = reader.ReadLine();
                    }
                }
            }

            object[][] real = new object[toRet.Length][];
            for (int i = 0; i < real.Length; i++)
            {
                real[i] = toRet[i].ToArray();
            }
            return real;
        }

    }
}
