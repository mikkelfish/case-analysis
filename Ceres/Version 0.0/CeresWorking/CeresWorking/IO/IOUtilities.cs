using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CeresBase.IO.Controls;
using CeresBase.Data;

namespace CeresBase.IO
{
    public static class IOUtilities
    {
        public static void AddVariableToDisk(string description, Variable[] vars)
        {
            if (MessageBox.Show(description, "Save?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Exporter exporter = new Exporter();
                if (exporter.ShowDialog() == DialogResult.Cancel) return;
                ICeresFile[] files = exporter.Writer.WriteVariables(vars, exporter.FilePath);

            }
        }

        //public static void AddVariableToDisk(string description, CeresFileDataBase db, Variable[] vars)
        //{
        //    if (MessageBox.Show(description, "Save?", MessageBoxButtons.YesNo) == DialogResult.Yes)
        //    {
        //        Exporter exporter = new Exporter(db);
        //        if (exporter.ShowDialog() == DialogResult.Cancel) return;
        //        ICeresFile[] files = exporter.Writer.WriteVariables(vars, exporter.FilePath);

        //        Dictionary<ICeresReader, List<string>> readers = new Dictionary<ICeresReader, List<string>>();

        //        if (files != null)
        //        {
        //            foreach (ICeresFile file in files)
        //            {
        //                ICeresReader reader = db.GetReader(file.FilePath);
        //                if (reader == null)
        //                {
        //                    MessageBox.Show("There are no readers that support the file " + file.FilePath);
        //                    return;
        //                }
        //                if (!readers.ContainsKey(reader))
        //                {
        //                    readers.Add(reader, new List<string>());
        //                }
        //                readers[reader].Add(file.FilePath);
        //            }

        //            foreach (ICeresReader reader in readers.Keys)
        //            {
        //                reader.AddFilesToDB(readers[reader].ToArray());
        //            }
        //        }
        //    }
        //}
    }
}
