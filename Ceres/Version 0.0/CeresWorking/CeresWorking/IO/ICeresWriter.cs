using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;
using CeresBase.Data;

namespace CeresBase.IO
{
    [CeresCreated("Mikkel", "03/16/06")]
    public interface ICeresWriter : IDisposable
    {
        [CeresCreated("Mikkel", "03/16/06")]
        void AddAttribute(ICeresFile file, string name, string attrName, object value);

        void AddAttribute(string fileName, string name, string attrName, object value);


        [CeresCreated("Mikkel", "03/16/06")]
        ICeresFile[] WriteVariable(Variable variable, string writeToDirectory);

        [CeresCreated("Mikkel", "03/16/06")]
        ICeresFile[] WriteVariables(Variable[] variables, string writeToDirectory);

        [CeresCreated("Mikkel", "03/16/06")]
        void WriteBinary(ICeresFile file, string name, byte[] data);

        //[CeresCreated("Mikkel", "03/16/06")]
        //CeresFileDataBase Database { get; set;}


    }
}
