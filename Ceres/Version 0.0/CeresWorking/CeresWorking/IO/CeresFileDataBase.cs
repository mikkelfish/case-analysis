//using System;
//using System.Collections.Generic;
//using System.Text;
//using CeresBase.Data;
//using CeresBase.Development;
//using CeresBase.IO.NCDF;
//using Razor.Configuration;

//namespace CeresBase.IO
//{
//    [CeresCreated("Mikkel", "03/20/06")]
//    [Settings.XMLSetting("CeresFileDatabase", "", true, false)]
//    public abstract class CeresFileDataBase
//    {
//        private ICeresReader[] readers;
//        public class CeresFileEntry
//        {
//            private VariableHeader header;
//            private string fileName;
//            private ICeresReader reader;

//            public VariableHeader Header
//            {
//                get
//                {
//                    return this.header;
//                }
//            }

//            public string FileName
//            {
//                get
//                {
//                    return this.fileName;
//                }
//            }


//            public ICeresReader Reader
//            {
//                get
//                {
//                    return this.reader;
//                }
//            }

//            public CeresFileEntry(ICeresReader reader, VariableHeader header, string fileName)
//            {
//                this.header = header;
//                this.fileName = fileName;
//                this.reader = reader;
//            }
//        }

//       // private CeresDataBaseDataSetTableAdapters.FilesTableAdapter fileAdapter = new CeresDataBaseDataSetTableAdapters.FilesTableAdapter();


//        public ThreadSafeConstructs.ThreadSafeEvent<UI.LoadingItemEventArgs> ItemStart =
//                new ThreadSafeConstructs.ThreadSafeEvent<CeresBase.UI.LoadingItemEventArgs>();

//        public ThreadSafeConstructs.ThreadSafeEvent<UI.LoadingItemEventArgs> ItemCompleted =
//            new ThreadSafeConstructs.ThreadSafeEvent<CeresBase.UI.LoadingItemEventArgs>();
//        private Settings.XMLSettingAttribute attr;

//        public CeresFileDataBase()
//        {
//           this.readers = CeresBase.General.Utilities.CreateInterfaces<ICeresReader>(null);
//           foreach (ICeresReader read in readers)
//           {
//               read.Database = this;
//           }

//           attr = CeresBase.General.Utilities.GetAttribute<Settings.XMLSettingAttribute>(this.GetType(), true);           
//        }


//        public ICeresReader GetReader(string fileName)
//        {
//            ICeresReader reader = null;
//            foreach (ICeresReader read in readers)
//            {
//                if (read.Supported(fileName))
//                {
//                    reader = read;
//                    break;
//                }
//            }

//            return reader;
//        }


//        public Dictionary<string, List<CeresFileEntry>> GetDataBaseInformation()
//        {
//            Dictionary<string, List<CeresFileEntry>> entries = new Dictionary<string, List<CeresFileEntry>>();

//            List<string> filesToDelete = new List<string>();
//          //  CeresDataBaseDataSet.FilesDataTable table = fileAdapter.GetData();
//            XmlConfigurationCategory files = this.attr.Category.Categories["Files", true];
//            foreach (XmlConfigurationCategory file in files.Categories)
//            {
//                XmlConfigurationCategory filePaths = file.Categories["FilePaths", true];
//                //XmlConfigurationOption displayName = file.Options["DisplayName", true];
//                foreach (XmlConfigurationCategory opt in filePaths.Categories)
//                {
//                    XmlConfigurationOption path = opt.Options["FilePath"];
//                    string filePath = path.Value as string;

//                    string experimentName = file.DisplayName;

//                    if (!System.IO.File.Exists(filePath))
//                    {
//                        //this.DeleteFile(filePath);
//                        filesToDelete.Add(filePath);
//                        continue;
//                    }

//                    ICeresReader reader = this.GetReader(filePath);
//                    if (reader == null) continue; //TODO THROW EXCEPTION?
//                    VariableHeader[] headers = reader.GetAllVariableHeaders(filePath, experimentName);
//                    if (headers == null)
//                    {
//                        //this.DeleteFile(filePath);
//                        filesToDelete.Add(filePath);
//                        continue;
//                    }

//                    //XmlConfigurationCategory dispNames = this.attr.Category.Categories["DisplayNames", true];


//                    foreach (VariableHeader header in headers)
//                    {
//                        header.FileName = filePath;

//                        if (!entries.ContainsKey(header.ExperimentName))
//                        {
//                            entries.Add(header.ExperimentName, new List<CeresFileEntry>());
//                        }

//                        if (file.Options.Contains(header.VarName))
//                        {
//                            header.Description = (string)file.Options[header.VarName].Value;
//                        }
//                        else
//                        {
//                            XmlConfigurationOption temp = file.Options[header.VarName, true, header.Description];
//                        }

//                        entries[header.ExperimentName].Add(new CeresFileEntry(reader, header, filePath));
//                    }
//                }
//            }

//            foreach (string file in filesToDelete)
//            {
//                this.DeleteFile(file);
//            }

//            return entries;
//        }

//        public void ChangeDescription(VariableHeader header, string newName)
//        {
//            header.Description = newName;
//            XmlConfigurationCategory files = this.attr.Category.Categories["Files"];
//            XmlConfigurationCategory dispNames = files.Categories[header.ExperimentName];
//            XmlConfigurationOption opt = dispNames.Options[header.VarName];
//            opt.Value = header.Description;
//            //opt.HasChanges = true;
//            attr.WriteToDisk();
//        }

//        //protected CeresDataBaseDataSet.FilesRow getFileRow(string file)
//        //{        
//        //    CeresDataBaseDataSet.FilesDataTable table = this.fileAdapter.GetDataByFile(file);
//        //    if (table.Rows.Count == 0) return null;
//        //    return (CeresDataBaseDataSet.FilesRow)table.Rows[0];
//        //}

//        //protected CeresDataBaseDataSet.FilesRow createFileRow(string filename, string experimentName, Type varType, Type shapeType)
//        //{
//        //    //TODO GET FILE PATH, CURRENT USER, CURRENT GROUP
//        //    fileAdapter.Insert(experimentName, filename, "Mikkel", "Global", varType.FullName, shapeType.FullName);
//        //    return fileAdapter.GetDataByFile(filename)[0];
//        //}

//        //protected void updateFileRow(CeresDataBaseDataSet.FilesRow row)
//        //{
//        //    fileAdapter.Update(row);
//        //}

//        //public ICeresFile GetFile(DataFrame frame, string key)
//        //{
//        //    CeresDBDataSet.FilesRow row = this.getFileRow(key);
//        //    if (row == null) return null;
//        //    return new NetCDFFile(row.FilePath.Trim(), frame, false);
//        //}

//        private XmlConfigurationCategory getCategory(string file)
//        {
//            XmlConfigurationCategory throwAway;
//            return getCategory(file, out throwAway);
//        }

//        private XmlConfigurationCategory getCategory(string file, out XmlConfigurationCategory parentCat)
//        {
//            XmlConfigurationCategory files = this.attr.Category.Categories["Files", true];
//            XmlConfigurationCategory found = null;
//            parentCat = null;

//            foreach (XmlConfigurationCategory par in files.Categories)
//            {
//                foreach (XmlConfigurationCategory cat in par.Categories["FilePaths"].Categories)
//                {
//                    foreach (XmlConfigurationOption opt in cat.Options)
//                    {
//                        if ((opt.Value as string) == file)
//                        {
//                            found = cat;
//                            parentCat = par;
//                            break;
//                        }
//                    }
//                }

//            }
//            return found;
//        }

       
//        public void DeleteFile(string file)
//        {
//            //this.fileAdapter.DeleteQuery(file);
//            XmlConfigurationCategory parent;

//            XmlConfigurationCategory toRemove = this.getCategory(file, out parent);

//            if (toRemove != null)
//            {
//            //    XmlConfigurationCategory files = this.attr.Category.Categories["Files", true];
//              //  files.Categories[parent..Remove(toRemove);
//               // parent.Categories.Remove(toRemove);
//                toRemove.Parent.Remove(toRemove);
//                if (parent.Categories["FilePaths"].Categories.Count == 0)
//                {
//                    parent.Parent.Remove(parent);
//                }
//            }

//            //if (attr.Category.Categories.Contains(file.Replace('\\', '_')))
//            //{
//            //    attr.Category.Categories.Remove(file.Replace('\\', '_'));
//            //}
//            attr.WriteToDisk();
//        }

//        public void GetFileProperties(string key, out Type VariableType, out Type ShapeType)
//        {
//            //CeresDataBaseDataSet.FilesRow row = this.getFileRow(key);
//            //if (row == null) throw new Exception("key not found");
//            XmlConfigurationCategory parent;
//            XmlConfigurationCategory property = this.getCategory(key, out parent);
//            XmlConfigurationOption variableType = property.Options["VariableType"];
//            XmlConfigurationOption shapeType = property.Options["ShapeType"];

//            VariableType = IOFactory.CreateType(variableType.Value as string);
//            ShapeType = IOFactory.CreateType(shapeType.Value as string);
//        }


//        public bool ContainsKey(string key)
//        {
//            XmlConfigurationCategory files = this.attr.Category.Categories["Files", true];
//            return files.Categories.Contains(key);

//          //  CeresDataBaseDataSet.FilesRow row = this.getFileRow(key);
//          //  return row != null;
//        }

//        public void CreateFile(string experimentName, ICeresFile file)
//        {
//            //this.createFileRow(file.FilePath, experimentName, file.VariableType, file.ShapeType);

//            XmlConfigurationCategory files = this.attr.Category.Categories["Files", true];
//            XmlConfigurationCategory cat = files.Categories[experimentName, true];
//            XmlConfigurationCategory paths = cat.Categories["FilePaths", true];

//            int pathTarget = 0;
//            for (int i = 0; i < paths.Categories.Count; i++, pathTarget++)
//            {
//                if (paths.Categories.Contains("File" + i))
//                {
//                    continue;
//                }
//                break;
//            }


//            XmlConfigurationCategory filePath = paths.Categories["File" + pathTarget, true];
//            XmlConfigurationOption realPath = filePath.Options["FilePath", true, file.FilePath];
//            XmlConfigurationOption variableType = filePath.Options["VariableType", true, file.VariableType.FullName];
//            XmlConfigurationOption shapeType = filePath.Options["ShapeType", true, file.ShapeType.FullName];
//        }

//        public Dictionary<string, object> GetSettings(string file)
//        {
//            XmlConfigurationCategory setting = this.getCategory(file);
//            if (setting == null) return null;
//          //  XmlConfigurationCategory cat = attr.Category.Categories[file.Replace('\\', '_')];
//            //if (cat == null) return null;
//            Dictionary<string, object> toRet = new Dictionary<string, object>();
//            foreach (XmlConfigurationOption option in setting.Options)
//            {
//                toRet.Add(option.DisplayName, option.Value);
//            }
//            return toRet;
//        }

//        public void SetSettings(string file, string[] optionNames, object[] options)
//        {
//            XmlConfigurationCategory cat = this.getCategory(file);
//           // cat.Options.Clear();
//            for(int i = 0; i < options.Length; i++)
//            {
//                XmlConfigurationOption option = new XmlConfigurationOption(optionNames[i], options[i]);
//                cat.Options.Add(option);
//            }
//            attr.WriteToDisk();
//        }

//        [CeresToDo("Mikkel", "03/20/06", Comments = "Eventually it'd be nice to get some intelligent multi-threading in here.", Priority=DevelopmentPriority.Low)]
//        public void LoadVariables(CeresFileDataBase.CeresFileEntry[] headers, DateTime[][] times)
//        {
//            //TODO KEEP TIMES INTO ACCOUNT
//            Dictionary<string, List<VariableHeader>> readers = new Dictionary<string, List<VariableHeader>>();
//            Dictionary<string, ICeresReader> files = new Dictionary<string,ICeresReader>();
            
//            foreach (CeresFileDataBase.CeresFileEntry entry in headers)
//            {
//                if (!readers.ContainsKey(entry.FileName))
//                {
//                    readers.Add(entry.FileName, new List<VariableHeader>());
//                    files.Add(entry.FileName, entry.Reader);
//                }
//                readers[entry.FileName].Add(entry.Header);
//                entry.Header.FileName = entry.FileName;
                
//            }


//            //CeresBase.Threading.ThreadSpawner spawner = new CeresBase.Threading.ThreadSpawner();
//            int keyCount = 0;
//            foreach (string file in readers.Keys)
//            {
//                //spawner.AddFunction(
//                //    delegate(object sender, CeresBase.General.ObjectArrayArgs e)
//                //    {
//                //        string pfile = e.Args[0] as string;
//                //        ICeresReader reader = files[pfile];
//                //        Variable[] vars = files[pfile].ReadVariables(pfile, readers[pfile].ToArray(), null);
//                //        VariableSource.AddVariables(vars);

//                //    }, new object[] { file }
//                //);

//                this.ItemStart.CallEvent(this, new CeresBase.UI.LoadingItemEventArgs("Reading file: " + file,
//                    keyCount, readers.Keys.Count, false));

//                ICeresReader reader = files[file];
//                Variable[] vars = files[file].ReadVariables(file, readers[file].ToArray(), null);
//                VariableSource.AddVariables(vars);

//                this.ItemCompleted.CallEvent(this, new CeresBase.UI.LoadingItemEventArgs("Done Reading file: " + file,
//                    keyCount, readers.Keys.Count, true));
//                keyCount++;
//            }

//            //spawner.StartJobs(true);
//            GC.Collect();

//        }

//        public void ConvertToNative()
//        {
//            Dictionary<string, List<CeresFileEntry>> files = GetDataBaseInformation();

//            //if (varHeaders == null)
//            //{
//            //    Dictionary<string, List<CeresFileEntry>> dbInfo = GetDataBaseInformation();
//            //    List<CeresFileEntry> nonNative = new List<CeresFileEntry>();
//            //    foreach (string expt in dbInfo.Keys)
//            //    {
//            //        foreach (CeresFileEntry entry in dbInfo[expt])
//            //        {
//            //            if (entry.Reader.GetType() != typeof(NetCDFReader))
//            //            {
//            //                nonNative.Add(entry);
//            //            }
//            //        }
//            //    }
//            //}

//            //Dictionary<string, List<CeresFileEntry>> entries = this.GetDataBaseInformation();

//            //foreach (string expt in entries.Keys)
//            //{
//            //    foreach (CeresFileEntry entry in entries[expt])
//            //    {
//            //        if(!files.ContainsKey(entry.FileName))
//            //            files.Add(entry.FileName, new List<CeresFileEntry>());
//            //        files[entry.FileName].Add(entry);
//            //    }
//            //}

//            Dictionary<string, List<CeresFileEntry>> filePathsToLoad = new Dictionary<string, List<CeresFileEntry>>();
//            foreach (string expt in files.Keys)
//            {
//                foreach (CeresFileEntry entry in files[expt])
//                {
//                    if (entry.Reader.StayNative) continue;
//                    Variable var = VariableSource.GetVariable(entry.Header);
//                    if(var != null)
//                    {
//                        if (!filePathsToLoad.ContainsKey(entry.FileName))
//                            filePathsToLoad.Add(entry.FileName, new List<CeresFileEntry>());
//                    }
//                }
//            }

//            foreach (string expt in files.Keys)
//            {
//                foreach (CeresFileEntry entry in files[expt])
//                {
//                    if (filePathsToLoad.ContainsKey(entry.FileName))
//                        filePathsToLoad[entry.FileName].Add(entry);
//                }
//            }

//            foreach(string fileName in filePathsToLoad.Keys)
//            {
//                List<VariableHeader> toReadd = new List<VariableHeader>();
//                List<Variable> vars = new List<Variable>();

//                List<VariableHeader> toLoad = new List<VariableHeader>();
//                for (int i = 0; i < filePathsToLoad[fileName].Count; i++)
//                {
//                    CeresFileEntry entry = filePathsToLoad[fileName][i];
//                    if (entry.Reader.StayNative) continue;
//                    Variable var = VariableSource.GetVariable(entry.Header);

//                    if (var == null)
//                    {
//                        //var = entry.Reader.ReadVariable(entry.FileName, entry.Header, null);
//                        toLoad.Add(entry.Header);
//                    }
//                    else
//                    {
//                        VariableSource.RemoveVariable(var);
//                        toReadd.Add(var.Header);
//                        vars.Add(var);                 
//                    }

//                }
//                if(toLoad.Count != 0)
//                    vars.AddRange(filePathsToLoad[fileName][0].Reader.ReadVariables(fileName, toLoad.ToArray(), null));

//                if (vars.Count == 0) continue;
//                NetCDFWriter writer = new NetCDFWriter();
//                writer.Database = this;
//                ICeresFile[] wfiles = writer.WriteVariables(vars.ToArray(), CeresBase.CeresCorePlugin.Instance.CurrentApplication.Settings.FileWriteLocation);
//                for (int i = 0; i < wfiles.Length; i++)
//                {
//                    this.CreateFile(filePathsToLoad[fileName][0].Header.ExperimentName, wfiles[i]);
//                }

//                this.DeleteFile(fileName);

//                //this.DeleteFile(fileName);

//                files = GetDataBaseInformation();
//                List<CeresFileEntry> entriesToAdd = new List<CeresFileEntry>();
//                foreach (string fileK in files.Keys)
//                {
//                    foreach (CeresFileEntry entry in files[fileK])
//                    {
//                        if (toReadd.Contains(entry.Header))
//                            entriesToAdd.Add(entry);
//                    }

//                }

//                this.LoadVariables(entriesToAdd.ToArray(), null);


                

//                //for (int i = 0; i < files[file].Count; i++)
//                //{
//                //    CeresFileEntry entry = files[file][i];
//                //    if (entry.Reader is NetCDFReader) continue;
//                //    //CeresFileEntry found = Array.Find<CeresFileEntry>(varHeaders,
//                //    //    delegate(CeresFileEntry h)
//                //    //    {
//                //    //        return h.Header.VarName == entry.Header.VarName &&
//                //    //            h.Header.ExperimentName == entry.Header.ExperimentName;
//                //    //    }
//                //    //    );
//                //    //if (found == null) continue;

//                //    NetCDFWriter writer = new NetCDFWriter();
//                //    writer.Database = this;

//                //    Variable var = VariableSource.GetVariable(entry.Header);

//                //    if (var == null)
//                //    {
//                //        var = entry.Reader.ReadVariable(entry.FileName, entry.Header, null);
//                //    }

//                //    writer.WriteVariable(var, CeresBase.CeresCorePlugin.Instance.CurrentApplication.Settings.FileWriteLocation);
//                //    writtenAny = true;
//                //    written[i] = true;
//                //}

//                //if (!writtenAny) continue;
//                //List<CeresFileEntry> entriesToLoad = new List<CeresFileEntry>();
//                //for (int i = 0; i < written.Length; i++)
//                //{
//                //    if (written[i]) continue;
//                //    CeresFileEntry entry = files[file][i];
//                //    if (entry.Reader is NetCDFReader) continue;
//                //    if (VariableSource.ContainsVariable(entry.Header)) continue;
//                //    entriesToLoad.Add(entry);
//                //}

//                //this.LoadVariables(entriesToLoad.ToArray(), null);

//                //for (int i = 0; i < written.Length; i++)
//                //{
//                //    if (written[i]) continue;
//                //    CeresFileEntry entry = files[file][i];
//                //    if (entry.Reader is NetCDFReader) continue;
//                //    NetCDFWriter writer = new NetCDFWriter();
//                //    writer.Database = this;

//                //    Variable var = VariableSource.GetVariable(entry.Header);
//                //    writer.WriteVariable(var, CeresBase.CeresCorePlugin.Instance.CurrentApplication.Settings.FileWriteLocation);
//                //}
//            }

            
//            GC.Collect();
//        }
//    }
//}
