using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CeresBase.IO.ImageCreation
{
    public class RectangleElement : ImageElement
    {
        private PointF location1;
        private Color color;
        private bool fill;
        private float width;
        private float height;


        private ImageElement parent;
        public ImageElement Parent
        {
            get { return parent; }
        }



        public RectangleElement(ImageElement parent, PointF loc1, float width, float height)
        {
            this.location1 = loc1;
            this.width = width;
            this.height = height;
            this.parent = parent;
            this.Color = Color.Black;
        }

        public PointF Location
        {
            get { return this.location1; }
        }

        public RectangleF Size
        {
            get { return new System.Drawing.RectangleF(this.location1.X, this.location1.Y, this.width, this.height); }
        }

        public Color Color
        {
            get
            {
                return this.color;
            }
            set
            {
                this.color = value;
            }
        }

        public bool Fill
        {
            get
            {
                return this.fill;
            }
            set
            {
                this.fill = value;
            }
        }
    }
}
