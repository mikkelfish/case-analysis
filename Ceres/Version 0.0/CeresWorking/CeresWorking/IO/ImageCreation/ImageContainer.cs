using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CeresBase.IO.ImageCreation
{
    public class ImageContainer : ImageElement
    {
        private Rectangle rectangle;
        private ImageElement parent;

        public ImageContainer(ImageElement parent, Rectangle rect)
        {
            this.parent = parent;
            this.rectangle = rect;
        }

        public System.Drawing.PointF Location
        {
            get { return new Point(rectangle.X, rectangle.Y); }
        }

        public System.Drawing.RectangleF Size
        {
            get { return this.rectangle; }
        }

        public System.Drawing.Color Color
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public bool Fill
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public ImageElement Parent
        {
            get { return this.parent; }
        }
    }
}
