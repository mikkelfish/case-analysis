using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CeresBase.IO.ImageCreation
{
    public class Lines : ImageElement
    {
        private List<PointF> points = new List<PointF>();
        private float maxY;
        private float minY;
        private float maxX;
        private float minX;
        private Color color = Color.Black;
        private bool fill;

        private int linewidth = 1; 
        public int LineWidth
        {
            get { return linewidth; }
            set { linewidth = value; }
        }

        private int maxXRes = int.MinValue;
        public int MaxXRes
        {
            set { this.maxXRes = value; }
            get 
            { 
                if(this.maxXRes != int.MinValue) return maxXRes;
                return (int)this.Size.Width;
            }
        }	

        private ImageElement parent;
        public ImageElement Parent
        {
            get
            {
                return this.parent;
            }
        }

        public PointF[] Points
        {
            get
            {
                return this.points.ToArray();
            }
        }

        public Lines(ImageElement parent)
        {
            this.parent = parent;
        }

        public void AddPoint(PointF p)
        {
            if (p.X < this.minX)
            {
                this.minX = p.X;
            }
            if (p.X > this.maxX)
            {
                this.maxX = p.X;
            }

            if (p.Y < this.minY)
            {
                this.minY = p.Y;
            }
            if (p.Y > this.maxY)
            {
                this.maxY = p.Y;
            }

            this.points.Add(p);
        }

        public PointF Location
        {
            get { return new PointF(this.minX, this.minY); }
        }

        public RectangleF Size
        {
            get { return new RectangleF(new PointF(this.minX, this.minY), new SizeF(this.maxX - this.minX, this.maxY - this.minY)); } 
        }

        #region ImageElement Members


        public Color Color
        {
            get
            {
                return this.color;
            }
            set
            {
                this.color = value;
            }
        }

        public bool Fill
        {
            get
            {
                return this.fill;
            }
            set
            {
                this.fill = value;
            }
        }

        #endregion
    }
}
