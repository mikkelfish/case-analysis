using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using Common;

namespace CeresBase.IO.ImageCreation
{
    public class GDI : ImageCreator
    {
        private ITree<ImageElement> tree = NodeTree<ImageElement>.NewTree();


        public bool AddImageElement(ImageElement element)
        {
            if (element.Parent == null)
            {
                this.tree.AddChild(element);
            }
            else
            {
                INode<ImageElement> parent = this.tree[element.Parent];
                parent.AddChild(element);
            }

            return true;
        }

        public void RemoveImageElement(ImageElement element)
        {
           // throw new Exception("The method or operation is not implemented.");
        }

        public Type[] SupportedElementTypes
        {
            get { return new Type[] { typeof(Lines), typeof(Line), typeof(Rectangle), typeof(TextElement), typeof(ImageContainer),
            typeof(Ellipse)}; }
        }

        private void drawLine(Graphics g, Line line)
        {
            Pen pen = new Pen(line.Color, line.LineWidth);
            g.DrawLine(pen, line.Location, line.Point2);
            pen.Dispose();
        }

        private void drawLines(Graphics g, Lines lines)
        {
            Pen pen = new Pen(lines.Color, lines.LineWidth);
            g.DrawLines(pen , lines.Points);
            pen.Dispose();
        }

        private void drawEllipse(Graphics g, Ellipse ellipse)
        {
            if (ellipse.Fill)
            {
                SolidBrush brush = new SolidBrush(ellipse.Color);
                g.FillEllipse(brush, ellipse.Size);
                brush.Dispose();
            }
            else
            {
                Pen pen = new Pen(ellipse.Color, 1);
                g.DrawEllipse(pen, ellipse.Size);
                pen.Dispose();
            }
        }

        private void drawText(Graphics g, TextElement text)
        {
            Brush textBrush = new SolidBrush(text.Color);
            if (text.Format == null) g.DrawString(text.Text, text.Font, textBrush, text.Location);
            else g.DrawString(text.Text, text.Font, textBrush, text.Size, text.Format);
            textBrush.Dispose();
        }

        private void drawRectangle(Graphics g, RectangleElement rect)
        {
            if (rect.Fill)
            {
                SolidBrush brush = new SolidBrush(Color.FromArgb(rect.Color.A, rect.Color.R, rect.Color.G, rect.Color.B));
                g.FillRectangle(brush, rect.Size);
                brush.Dispose();

            }
            else
            {
                Pen pen = new Pen(rect.Color, 1);
                g.DrawRectangle(pen, rect.Location.X, rect.Location.Y, rect.Size.Width, rect.Size.Height);
                pen.Dispose();

            }
        }

        private void changeDrawMatrix(Graphics g, ImageContainer cont)
        {
            g.TranslateTransform(cont.Location.X, cont.Location.Y);
        }

        private void recurse(Graphics g, INode<ImageElement> treeNode)
        {
            if (treeNode.HasChild)
            {
                this.drawElement(g, treeNode.Data);

                INode<ImageElement> child = treeNode.Child;
                while (child != null)
                {
                    this.recurse(g, child);
                    child = child.Next;
                }

                g.ResetTransform();
            }
            else
            {
                this.drawElement(g, treeNode.Data);
            }
        }

        private void drawElement(Graphics g, ImageElement imageElement)
        {
            if (imageElement is TextElement)
            {
                this.drawText(g, imageElement as TextElement);
            }
            else if (imageElement is Line)
            {
                this.drawLine(g, imageElement as Line);
            }
            else if (imageElement is Lines)
            {
                this.drawLines(g, imageElement as Lines);
            }
            else if (imageElement is ImageContainer)
            {
               // this.changeDrawMatrix(g, imageElement as ImageContainer);
            }
            else if (imageElement is RectangleElement)
            {
                this.drawRectangle(g, imageElement as RectangleElement);
            }
            else if (imageElement is Ellipse)
            {
                this.drawEllipse(g, imageElement as Ellipse);
            }

        }

        public void Create(object[] args)
        {
            Graphics g = (args[0] as Graphics);
            this.recurse(g, this.tree.Root);

        }
    }
}
