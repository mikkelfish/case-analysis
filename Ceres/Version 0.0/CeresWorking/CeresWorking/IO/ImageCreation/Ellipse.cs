using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CeresBase.IO.ImageCreation
{
    public class Ellipse : ImageElement
    {
        private RectangleF size;
        private Color color = Color.Black;
        private bool fill = false;
        private ImageContainer parent;


        public Ellipse(ImageContainer parent, float x, float y, float width, float height)
        {
            this.parent = parent;
            this.size = new RectangleF(x, y, width, height);
        }

        public System.Drawing.PointF Location
        {
            get { return this.size.Location; }
        }

        public System.Drawing.RectangleF Size
        {
            get { return this.size; }
        }

        public System.Drawing.Color Color
        {
            get
            {
                return this.color;
            }
            set
            {
                this.color = value;
            }
        }

        public bool Fill
        {
            get
            {
                return this.fill;
            }
            set
            {
                this.fill = value;
            }
        }

        public ImageElement Parent
        {
            get { return this.parent; }
        }
    }
}
