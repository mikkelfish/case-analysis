using System;
using System.Collections.Generic;
using System.Text;

namespace CeresBase.IO.ImageCreation
{
    public interface ImageCreator
    {
        bool AddImageElement(ImageElement element);
        void RemoveImageElement(ImageElement element);
        Type[] SupportedElementTypes { get;}
        void Create(object[] args);
    }
}
