using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CeresBase.IO.ImageCreation
{
    public class Line : ImageElement
    {
        PointF point1;
        PointF point2;
        private Color color;
        private bool fill;

        private ImageElement parent;
        public ImageElement Parent
        {
            get { return parent; }
        }

        public PointF Point2
        {
            get
            {
                return this.point2;
            }
        }
	

        public Line(ImageElement parent, PointF point1, PointF point2)
        {
            this.point1 = point1;
            this.point2 = point2;
            this.lineWidth = 1;
            this.color = Color.Black;
            this.parent = parent;
        }

        public PointF Location
        {
            get { return point1; }
        }

        public RectangleF Size
        {
            get 
            {
                float height = Math.Max(point1.Y, point2.Y) - Math.Min(point1.Y, point2.Y);
                float width = Math.Max(point1.X, point2.X) - Math.Min(point1.X, point2.X);
                return new RectangleF(this.Location, new SizeF(width, height));
            }
        }

        public Color Color
        {
            get 
            {
                return this.color; 
            }
            set
            {
                this.color = value;
            }
        }

        private int lineWidth;
        public int LineWidth
        {
            get { return lineWidth; }
            set { lineWidth = value; }
        }
	

        public bool Fill
        {
            get 
            { 
                return this.fill; 
            }
            set
            {
                this.fill = value;
            }
        }
    }
}
