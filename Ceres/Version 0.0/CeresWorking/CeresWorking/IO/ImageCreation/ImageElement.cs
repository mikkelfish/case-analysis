using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CeresBase.IO.ImageCreation
{
    public interface ImageElement
    {
        PointF Location { get;}
        RectangleF Size { get;}
        Color Color { get; set;}
        bool Fill { get;set;}


        ImageElement Parent { get;}
    }
}
