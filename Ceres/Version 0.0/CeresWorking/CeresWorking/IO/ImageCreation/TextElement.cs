using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CeresBase.IO.ImageCreation
{
    public class TextElement : ImageElement
    {
        private PointF location;
        private Color color;
        private bool fill;
        private RectangleF rect;
        private StringFormat format;
        private string text;

        private ImageElement parent;
        public ImageElement Parent
        {
            get { return parent; }
        }
	

        public string Text
        {
            get
            {
                return this.text;
            }
        }

        public StringFormat Format
        {
            get
            {
                return this.format;
            }
        }

        private Font font;
        public Font Font
        {
            get { return font; }
        }
	

        public TextElement(ImageElement parent, Font font, string text, PointF location)
        {
            this.text = text;
            this.location = location;
            this.font = font;
            this.parent = parent;
            this.color = Color.Black;
        }

        public TextElement(ImageElement parent, Font font, string text, RectangleF boundingRectangle, StringFormat format)
        {
            this.format = format;
            this.rect = boundingRectangle;
            this.location = new PointF(boundingRectangle.X, boundingRectangle.Y);
            this.text = text;
            this.font = font;
            this.parent = parent;
        }


        public System.Drawing.PointF Location
        {
            get { return this.location; }
        }

        /// <summary>
        /// Can be null if there is no bounding rectangle.
        /// </summary>
        public System.Drawing.RectangleF Size
        {
            get { return this.rect; }
        }

        public System.Drawing.Color Color
        {
            get
            {
                return this.color;
            }
            set
            {
                this.color = value;
            }
        }

        public bool Fill
        {
            get
            {
                return this.fill;
            }
            set
            {
                this.fill = value;
            }
        }
    }
}
