using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CeresBase.IO.ImageCreation
{
    public interface IGenImage
    {
        void MakeImage(ImageCreator creator, Rectangle clipRectangle);
    }
}
