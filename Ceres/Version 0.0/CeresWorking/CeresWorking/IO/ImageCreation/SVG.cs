using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Xml;
using System.Drawing.Printing;
using Common;

namespace CeresBase.IO.ImageCreation
{
    public class SVG : ImageCreator
    {
        private ITree<ImageElement> tree = NodeTree<ImageElement>.NewTree();
        
        private void drawLine(XmlDocument doc, XmlNode parent, Line line)
        {
            //<line x1="0" y1="0" x2="300" y2="300" style="stroke:rgb(99,99,99);stroke-width:2"/>

            XmlElement xmlLine = doc.CreateElement("line");
            xmlLine.SetAttribute("x1", line.Location.X.ToString());
            xmlLine.SetAttribute("y1", line.Location.Y.ToString());
            xmlLine.SetAttribute("x2", line.Size.Right.ToString());
            xmlLine.SetAttribute("y2", line.Size.Bottom.ToString());
            xmlLine.SetAttribute("style", "stroke:rgb(" + line.Color.R.ToString() + "," + line.Color.G.ToString() +
                "," + line.Color.B.ToString() + ");stroke-width:" + line.LineWidth.ToString());
            parent.AppendChild(xmlLine);
        }

        private void drawEllipse(XmlDocument doc, XmlNode parent, Ellipse ellipse)
        {
            XmlElement xmlRect = doc.CreateElement("ellipse");
            float centerx = ellipse.Location.X + ellipse.Size.Width / 2.0F;
            float centery = ellipse.Location.Y + ellipse.Size.Width / 2.0F;


            xmlRect.SetAttribute("cx", centerx.ToString());
            xmlRect.SetAttribute("cy", centery.ToString());
            xmlRect.SetAttribute("rx", (ellipse.Size.Width/2.0).ToString());
            xmlRect.SetAttribute("ry", (ellipse.Size.Height/2.0).ToString());
            if (ellipse.Fill)
            {
                xmlRect.SetAttribute("style", "fill:rgb(" + ellipse.Color.R.ToString() + "," + ellipse.Color.G.ToString() +
                    "," + ellipse.Color.B.ToString() + ")");
            }
            else
            {
                string fill = "fill:";
                fill += "none";

                //xmlLines.SetAttribute("style", fill + ";stroke:rgb(" + lines.Color.R.ToString() + "," + lines.Color.G.ToString() +
                //    "," + lines.Color.B.ToString() + ");stroke-width:" + lines.LineWidth.ToString());

                xmlRect.SetAttribute("style", fill + ";stroke:rgb(" + ellipse.Color.R.ToString() + "," + ellipse.Color.G.ToString() +
                    "," + ellipse.Color.B.ToString() + ")");
            }
            parent.AppendChild(xmlRect);
        }

        private void drawRectangle(XmlDocument doc, XmlNode parent, RectangleElement rect)
        {
            XmlElement xmlRect = doc.CreateElement("rect");
            xmlRect.SetAttribute("x", rect.Location.X.ToString());
            xmlRect.SetAttribute("y", rect.Location.Y.ToString());
            xmlRect.SetAttribute("width", rect.Size.Width.ToString());
            xmlRect.SetAttribute("height", rect.Size.Height.ToString());
            if (rect.Fill)
            {
                xmlRect.SetAttribute("style", "fill:rgb(" + rect.Color.R.ToString() + "," + rect.Color.G.ToString() +
                    "," + rect.Color.B.ToString() + ")");
            }
            else
            {
                string fill = "fill:";
                fill += "none";

                //xmlLines.SetAttribute("style", fill + ";stroke:rgb(" + lines.Color.R.ToString() + "," + lines.Color.G.ToString() +
                //    "," + lines.Color.B.ToString() + ");stroke-width:" + lines.LineWidth.ToString());

                xmlRect.SetAttribute("style", fill + ";stroke:rgb(" + rect.Color.R.ToString() + "," + rect.Color.G.ToString() +
                    "," + rect.Color.B.ToString() + ")");
            }
            parent.AppendChild(xmlRect);
          
            //  return xmlRect;
            //            <rect x="20" y="20" width="250" height="250"
            //style="fill:blue;stroke:pink;stroke-width:5;
            //fill-opacity:0.1;stroke-opacity:0.9"/>
        }

        private void drawLines(XmlDocument doc, XmlNode parent, Lines lines)
        {
            //polyline points="0,0 0,20 20,20 20,40 40,40 40,60" style="fill:white;stroke:red;stroke-width:2"
            XmlElement xmlLines = doc.CreateElement("polyline");

            int bufferSize = 200000;
            char[] buffer = new char[bufferSize];
            int spot = 0;
            string total = "";


            if (lines.Points.Length > lines.Size.Width * 3)
            {
                float[] mins = new float[lines.MaxXRes + 1];
                float[] maxs = new float[lines.MaxXRes + 1];
                for (int i = 0; i < mins.Length; i++)
                {
                    mins[i] = float.MaxValue;
                    maxs[i] = float.MinValue;
                }

                float xScaling = lines.Size.Width / (float)lines.MaxXRes;

                foreach (PointF point in lines.Points)
                {
                    int xindex = (int)Math.Round((point.X - lines.Location.X) / xScaling, 0);
                    if (point.Y > maxs[xindex])
                    {
                        maxs[xindex] = point.Y;
                    }
                    if (point.Y < mins[xindex])
                    {
                        mins[xindex] = point.Y;
                    }
                }

                for (int i = 0; i < mins.Length; i++)
                {
                    if (mins[i] == float.MaxValue) continue;
                    float x = ((float)i*xScaling) + lines.Location.X;
                    float y = maxs[i];
                    
                    //total += x.ToString() + "," + y.ToString() + " ";
                    string toAdd = "";
                    if (mins[i] != maxs[i])
                    {
                        toAdd = x.ToString() + "," + y.ToString() + " " + x.ToString() + "," + mins[i].ToString() + " ";
                    }
                    else toAdd = x.ToString() + "," + y.ToString() + " ";
                    
                    if (spot + toAdd.Length > bufferSize)
                    {
                        total += new string(buffer, 0, spot);
                        spot = 0;
                    }

                    for (int j = 0; j < toAdd.Length; j++, spot++)
                    {
                        buffer[spot] = toAdd[j];
                    }
                }

            }
            else
            {
                foreach (PointF point in lines.Points)
                {
                    string toAdd = point.X.ToString() + "," + point.Y.ToString() + " ";
                    if (spot + toAdd.Length > bufferSize)
                    {
                        total += new string(buffer, 0, spot);
                        spot = 0;
                    }
                    for (int j = 0; j < toAdd.Length; j++, spot++)
                    {
                        buffer[spot] = toAdd[j];
                    }
                }
            }


            if (spot != 0)
            {
                total += new string(buffer, 0, spot);
            }
            
            total = total.Substring(0, total.Length - 1);
            string fill = "fill:";
            fill += "none";
           
            xmlLines.SetAttribute("style",fill + ";stroke:rgb(" + lines.Color.R.ToString() + "," + lines.Color.G.ToString() +
                "," + lines.Color.B.ToString() + ");stroke-width:" + lines.LineWidth.ToString());
            xmlLines.SetAttribute("points", total);
            parent.AppendChild(xmlLines);
        }

        public void drawText(XmlDocument doc, XmlNode parent, TextElement text)
        {
            //<text id="TextElement" x="0" y="0" style="font-family:Verdana;font-size:24">
            XmlElement xmlText = doc.CreateElement("text");
            xmlText.SetAttribute("x", text.Location.X.ToString());
            xmlText.SetAttribute("y", text.Location.Y.ToString());
            xmlText.SetAttribute("style", "font-family:" + text.Font.FontFamily.Name + ";" +
                "font-size:" + text.Font.SizeInPoints.ToString());
            xmlText.InnerText = text.Text;
            
            //xmlText.Value = text.Text;
            parent.AppendChild(xmlText);
        }

        private XmlElement changeCoordinateSystem(XmlDocument doc, XmlNode parent, ImageContainer transform)
        {
            XmlElement trans = doc.CreateElement("g");
            trans.SetAttribute("transform", "translate(" + transform.Location.X + "," + transform.Location.Y + ")");
            parent.AppendChild(trans);
            return trans;
        }



        private XmlNode drawElement(XmlDocument doc, XmlNode parent, ImageElement element)
        {
            if (element is ImageContainer)
            {
                return this.changeCoordinateSystem(doc, parent, element as ImageContainer);
            }
            else if (element is Line)
            {
                this.drawLine(doc, parent, element as Line);
            }
            else if (element is Lines)
            {
                this.drawLines(doc, parent, element as Lines);
            }
            else if (element is TextElement)
            {
                this.drawText(doc, parent, element as TextElement);
            }
            else if (element is RectangleElement)
            {
                this.drawRectangle(doc, parent, element as RectangleElement);
            }
            else if (element is Ellipse)
            {
                this.drawEllipse(doc, parent, element as Ellipse);
            }
            return null;
        }

        private void recurse(XmlDocument doc, XmlNode node, INode<ImageElement> treeNode)
        {
            if (treeNode.HasChild)
            {
                XmlNode created = this.drawElement(doc, node, treeNode.Data);
                XmlNode parent = created;
                if (created == null)
                {
                    parent = node;
                }

                INode<ImageElement> child = treeNode.Child;
                while (child != null)
                {
                    this.recurse(doc, parent, child);
                    child = child.Next;
                }

                //foreach (INode<ImageElement> child in treeNode.Nodes)
                //{
                //    this.recurse(doc, parent, child);
                //}
            }
            else
            {
                this.drawElement(doc, node, treeNode.Data);
            }
        }

        public void Create(object[] args)
        {
//<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" 
//"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

            string filename = args[0] as string;
            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, "no");
            doc.AppendChild(dec);
      //      doc.AppendChild(doc.CreateDocumentType("svg", "-//W3C//DTD SVG 1.1//EN", "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd", null));

            XmlElement root = doc.CreateElement(@"svg");
            root.SetAttribute("width", "100%");
            root.SetAttribute("height", "100%");
            root.SetAttribute("version", "1.0");
            root.SetAttribute("xmlns", "http://www.w3.org/2000/svg");
            doc.AppendChild(root);

            this.recurse(doc, root, this.tree.Root);

            doc.Save(filename);
        }

        public bool AddImageElement(ImageElement element)
        {
            if (element.Parent == null)
            {
                this.tree.AddChild(element);
            }
            else
            {
                INode<ImageElement> parent = this.tree[element.Parent];
                if (parent == null)
                {
                    this.AddImageElement(element.Parent);
                    parent = this.tree[element.Parent];
                }
                
                parent.AddChild(element);
            }

            return true;
        }

        public void RemoveImageElement(ImageElement element)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Type[] SupportedElementTypes
        {
            get { return new Type[]{typeof(Lines), typeof(Line), typeof(Rectangle), typeof(TextElement), typeof(ImageContainer)}; }
        }
    }
}
