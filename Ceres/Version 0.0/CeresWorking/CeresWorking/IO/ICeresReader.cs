using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;
using CeresBase.Data;
using CeresBase.IO.DataBase;

namespace CeresBase.IO
{
    public interface ICeresReader
    {
        [CeresCreated("Mikkel", "03/16/06")]
        object ReadAttribute(ICeresFile file, string name, string attrName);

        //[CeresCreated("Mikkel", "03/16/06")]
        //Variable ReadVariable(string filename, VariableHeader header, DateTime[] times);

        //[CeresCreated("Mikkel", "03/20/06")]
        //Variable[] ReadVariables(string filename, VariableHeader[] headers, DateTime[][] times);

        //[CeresCreated("Mikkel", "03/16/06")]
        //byte[] ReadBinary(ICeresFile file, string name);

        //[CeresCreated("Mikkel", "03/16/06")]
        //CeresFileDataBase Database { get; set;}

        [CeresCreated("Mikkel", "03/20/06")]
        double[][] GetShapeValues(ICeresFile file);

        //[CeresCreated("Mikkel", "03/20/06")]
        //VariableHeader[] GetAllVariableHeaders(string file, string experimentName);

        [CeresCreated("Mikkel", "03/20/06")]
        bool Supported(string file);

        //[CeresCreated("Mikkel", "03/20/06")]
        //void AddFilesToDB(string[] files);

        bool StayNative { get;}

        CeresBase.IO.DataBase.FileDataBaseEntryParameter[] GetParameters(FileDataBaseEntry entry);
        CeresBase.IO.DataBase.FileDataBaseVariableEntryParameter[] GetVariableParameters(FileDataBaseEntry entry);
        Variable[] ReadVariables(string filename, FileDataBaseEntryParameter[] fileparams, VariableHeader[] headers);
    }
}
