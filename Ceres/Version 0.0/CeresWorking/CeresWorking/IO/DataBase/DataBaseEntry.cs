﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace CeresBase.IO.DataBase
{
    public abstract class DataBaseEntry : INotifyPropertyChanged
    {
        private string oldName;


        public string Name 
        { 
            get; 
            set; 
        }
        
        
        public string DisplayName { get; set; }
        public bool IsNameSet
        {
            get
            {
                return this.Name == this.DisplayName;
            }
        }

        public abstract string DefaultName { get; }

        private ObservableCollection<DataBaseEntry> children = new ObservableCollection<DataBaseEntry>();
        public ObservableCollection<DataBaseEntry> Children { get { return this.children; } }

        private ObservableCollection<DataBaseEntry> notes = new ObservableCollection<DataBaseEntry>();
        public ObservableCollection<DataBaseEntry> Notes { get { return this.notes; } }

        public DataBaseEntry Parent
        {
            get;
            private set;
        }

        public string Path
        {
            get
            {
                if (this.Parent == null) return "\\" + this.Name;
                return this.Parent.Path + "\\" + this.Name;
            }
        }



        public void BeginNameChange()
        {
            this.CanChange = true;
            this.oldName = this.DisplayName;
            this.OnPropertyChanged("CanChange");
        }

        public void CancelNameChange()
        {
            this.CanChange = false;
            this.OnPropertyChanged("CanChange");
            this.DisplayName = oldName;
            this.OnPropertyChanged("DisplayName");
            this.OnPropertyChanged("IsNameSet");

        }

        public void EndNameChange(bool changeNameToo)
        {
            this.CanChange = false;
            this.OnPropertyChanged("CanChange");
            this.oldName = "";

            if (changeNameToo)
            {
                this.Name = this.DisplayName;
                this.OnPropertyChanged("Name");
                this.OnPropertyChanged("IsNameSet");
            }
        }

        private const string defaultTag = "USEDEFAULTTAG";

        public DataBaseEntry()
            : this (defaultTag, null)
        {

        }

        public DataBaseEntry(DataBaseEntry parent)
            : this(defaultTag, parent)
        {

        }

        public DataBaseEntry(string name)
            : this(name, null)
        {

        }

        public DataBaseEntry(string name, DataBaseEntry parent)
        {
            if (name == DataBaseEntry.defaultTag)
            {
                this.Name = Guid.NewGuid().ToString();
                this.DisplayName = this.DefaultName;
            }
            else
            {
                this.Name = name;
                this.DisplayName = name;
            }
            this.CanChange = false;

            if (parent != null)
            {
                this.Parent = parent;
                this.Parent.children.Add(this);
            }

           
        }

        public bool CanChange { get; private set; }

        private bool isSelected = false;
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");

                foreach (DataBaseEntry entry in this.children)
                {
                    entry.IsSelected = this.isSelected;
                }

            }
        }


        public string Comments { get; set; }

        #region INotifyPropertyChanged Members

        protected void OnPropertyChanged(string propName)
        {
            PropertyChangedEventHandler handle = this.PropertyChanged;
            if (handle != null)
            {
                handle(this, new PropertyChangedEventArgs(propName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
