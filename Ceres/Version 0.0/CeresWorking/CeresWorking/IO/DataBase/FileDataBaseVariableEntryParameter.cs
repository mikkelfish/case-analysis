﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using CeresBase.Data;
using System.ComponentModel;
using MesosoftCommon.Interfaces;

namespace CeresBase.IO.DataBase
{
    public class VariableEntryParameterItem<T> : IConstructorProvider, INotifyPropertyChanged
    {
        private bool isFixed;
        public bool IsFixed
        {
            get
            {
                return this.isFixed;
            }
            set
            {
                this.isFixed = value;
                this.OnPropertyChanged("IsFixed");
            }
        }

        private T val;
        public T Value
        {
            get
            {
                return this.val;
            }

            set
            {
                this.val = value;
                this.OnPropertyChanged("Value");
            }
        }

        public string Name { get; private set; }

        public VariableEntryParameterItem(string name)
        {
            this.Name = name;
        }

        private bool successful;
        public bool SuccessfullySet 
        {
            get
            {
                return this.IsFixed || this.successful;
            }
            set
            {
                this.successful = value;
            }
        }

        public bool IsRequired { get; set; }

        #region IConstructorProvider Members

        public object[] ConstructionArgs
        {
            get { return new object[] { this.Name }; }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler ev = this.PropertyChanged;
            if (ev != null)
            {
                ev(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }

    public class FileDataBaseVariableEntryParameter : INotifyPropertyChanged
    {
        private VariableEntryParameterItem<string> display;
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]                    
        public VariableEntryParameterItem<string> DisplayName
        {
            get
            {
                return this.display;
            }
            set
            {
                this.display = value;
                this.OnPropertyChanged("DisplayName");
            }
        }

        private VariableEntryParameterItem<string> varName;
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]                    
        public VariableEntryParameterItem<string> VariableName
        {
            get
            {
                return this.varName;
            }
            set
            {
                this.varName = value;
                this.OnPropertyChanged("VariableName");
            }
        }

        private VariableEntryParameterItem<string> units;
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]                    
        public VariableEntryParameterItem<string> Units
        {
            get
            {
                return this.units;
            }
            set
            {
                this.units = value;
                this.OnPropertyChanged("Units");
            }
        }

        private VariableEntryParameterItem<string> comments;
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]                    
        public VariableEntryParameterItem<string> Comments
        {
            get
            {
                return this.comments;
            }
            set
            {
                this.comments = value;
                this.OnPropertyChanged("Comments");
            }
        }

        public FileDataBaseVariableEntryParameter()
        {
            this.DisplayName = new VariableEntryParameterItem<string>("DisplayName") { Value = "" };
            this.Comments = new VariableEntryParameterItem<string>("Comments") { Value = "" };
            this.Units = new VariableEntryParameterItem<string>("Units") { Value = "" };
            this.VariableName = new VariableEntryParameterItem<string>("VariableName") { Value="N/A", IsRequired=true };
        }

        public bool IsComplete
        {
            get
            {
                PropertyInfo[] infos = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo info in infos)
                {
                    if (info.PropertyType.IsGenericType && info.PropertyType.GetGenericTypeDefinition() ==
                        typeof(VariableEntryParameterItem<>))
                    {
                        object obj = info.GetValue(this, null);

                        PropertyInfo successProp = info.PropertyType.GetProperty("SuccessfullySet");
                        PropertyInfo isRequiredProp = info.PropertyType.GetProperty("IsRequired");

                        bool succ = (bool)successProp.GetValue(obj, null);
                        bool isReq = (bool)isRequiredProp.GetValue(obj, null);

                        if (isReq && !succ) return false;
                    }
                }

                return true;
            }
        }

        public virtual VariableHeader CreateHeader(string experiment)
        {
            return null;
        }

        #region INotifyPropertyChanged Members

        protected void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler ev = this.PropertyChanged;
            if (ev != null)
            {
                ev(this, new PropertyChangedEventArgs(property));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
