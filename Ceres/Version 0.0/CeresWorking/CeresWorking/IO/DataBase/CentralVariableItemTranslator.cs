﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;
using System.Runtime.Serialization;
using System.Reflection;
using System.ComponentModel;
using MesosoftCommon.Utilities.Settings;
using CeresBase.IO.Serialization;
using DatabaseUtilityLibrary;

namespace CeresBase.IO.DataBase
{
    public class CentralVariableItemTranslator : ITranslator
    {
        private static Dictionary<string, List<centralStoragePara>> seenParas = new Dictionary<string, List<centralStoragePara>>();

        static CentralVariableItemTranslator()
        {
            NewSerializationCentral.DatabaseChanging += new DatabaseChangingEventHandler(SerializationCentral_DatabaseChanging);
        }

        static void SerializationCentral_DatabaseChanging(object sender, DatabaseChangingEventArgs e)
        {
            seenParas.Clear();
        }

        [AlwaysSerialize]
        private class centralStoragePara
        {
            public string Name { get; set; }
            public object Value { get; set; }
        }

        [AlwaysSerialize]
        private class centralStorageEntry
        {
            public CentralFileItem File { get; set; }
            public string Experiment { get; set; }
            public string HeaderType { get; set; }

            private List<centralStoragePara> paras = new List<centralStoragePara>();
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
            public List<centralStoragePara> Paras
            {
                get
                {
                    return this.paras;
                }
            }

        }

        public object Clone()
        {
            return new CentralVariableItemTranslator();
        }

        #region ITranslator Members

        public CentralVariableItem Item
        {
            get;
            set;
        }

        object ITranslator.AttachedObject
        {
            get
            {
                return this.Item;
            }
            set
            {
                this.Item = value as CentralVariableItem;
            }
        }

        public bool SupportsType(Type t)
        {
            if (t == typeof(CentralVariableItem)) return true;
            return false;
        }

        public double Version
        {
            get { return 0.0; }
        }

        public bool Supports(object obj)
        {
            if (obj is CentralVariableItem) return true;
            return false;
        }

        #endregion

        #region ISerializable Members

        public CentralVariableItemTranslator()
        {

        }

        public CentralVariableItemTranslator(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            centralStorageEntry entry = info.GetValue("Entry", typeof(centralStorageEntry)) as centralStorageEntry;

            CentralVariableItem varItem = new CentralVariableItem() { File = entry.File, Experiment = entry.Experiment };
            Type headerType = Type.GetType(entry.HeaderType);
            varItem.Header = (FileDataBaseVariableEntryParameter)Activator.CreateInstance(headerType);
            foreach (centralStoragePara para in entry.Paras)
            {
                PropertyInfo pinfo = varItem.Header.GetType().GetProperty(para.Name);
                object val = pinfo.GetValue(varItem.Header, null);
                PropertyInfo valInfo = val.GetType().GetProperty("Value");
                valInfo.SetValue(val, para.Value, null);

                if (!seenParas.ContainsKey(pinfo.Name))
                {
                    seenParas.Add(pinfo.Name, new List<centralStoragePara>());
                }

                if (!seenParas[pinfo.Name].Any(p => p.Value.Equals(para.Value)))
                {
                    seenParas[pinfo.Name].Add(para);
                }
            }

            this.Item = varItem;

        }

        public void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            centralStorageEntry entry = null;

            SerializationInfo passed = context.Context as SerializationInfo;
            if (passed != null)
            {
                entry = passed.GetValue("Entry", typeof(centralStorageEntry)) as centralStorageEntry;
            }



            if (entry == null)
            {
                entry = new centralStorageEntry() { 
                    Experiment = this.Item.Experiment, 
                    HeaderType = this.Item.Header.GetType().AssemblyQualifiedName,
                    File = this.Item.File};
            }

            PropertyInfo[] infos = this.Item.Header.GetType().GetProperties();
            foreach (PropertyInfo pinfo in infos)
            {
                if (pinfo.PropertyType.IsGenericType &&
                    pinfo.PropertyType.GetGenericTypeDefinition() == typeof(VariableEntryParameterItem<>))
                {
                    object val = pinfo.GetValue(this.Item.Header, null);
                    if (val != null)
                    {
                        PropertyInfo value = val.GetType().GetProperty("Value");
                        object propVal = value.GetValue(val, null);

                        if (!seenParas.ContainsKey(pinfo.Name))
                        {
                            seenParas.Add(pinfo.Name, new List<centralStoragePara>());
                        }

                        centralStoragePara para = seenParas[pinfo.Name].SingleOrDefault(p => p.Value.Equals(propVal));

                        if (para == null)
                        {
                            para = new centralStoragePara() { Name = pinfo.Name, Value = propVal };
                            seenParas[pinfo.Name].Add(para);
                        }


                        if (!entry.Paras.Any(p => p.Name == pinfo.Name))
                        {
                            entry.Paras.Add(para);
                        }
                        else
                        {
                            entry.Paras.Remove(entry.Paras.SingleOrDefault(p => p.Name == pinfo.Name));
                            entry.Paras.Add(para);
                        }

                    }
                }
            }

            info.AddValue("Entry", entry);
        }

        #endregion
    }
}
