﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.DataBase
{
    public class FileDataBaseVariableEntry : DataBaseEntry
    {

        public FileDataBaseVariableEntry(ExperimentEntry parent)
            : base(parent)
        {

        }

        public FileDataBaseVariableEntry(string name, ExperimentEntry parent)
            : base(name, parent)
        {

        }

        

        public override string DefaultName
        {
            get { return "New Variable"; }
        }
    }
}
