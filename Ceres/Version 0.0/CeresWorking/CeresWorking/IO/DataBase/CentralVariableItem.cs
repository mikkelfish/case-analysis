﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.DataBase
{
    public class CentralVariableItem
    {
        public CentralFileItem File { get; set; }

        public FileDataBaseVariableEntryParameter Header { get; set; }
        public string Experiment { get; set; }

        public override string ToString()
        {

            if (this.Experiment == null || this.Header == null) return "";
            return this.Experiment + '\\' + this.Header.VariableName.Value;
        }
    }
}
