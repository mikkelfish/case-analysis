﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.DataBase
{
    public class FileDataBaseEntryParameter : ICloneable
    {
        public string Name { get; set; }

        private object val;
        public object Value 
        {
            get
            {
                return this.val;
            }
            set
            {
                if (this.val != null && this.SuccessfullySet && !this.CanChange)
                {
                    throw new InvalidOperationException("The value cannot be changed because it has already been set and CanChange is set to false.");
                }

                this.val = value;
            }
        }
        public bool UserEditable { get; set; }
        public bool SuccessfullySet { get; set; }
        public Type EditorType { get; set; }

        /// <summary>
        /// If true, then the parameter cannot be changed once SuccessfullySet is true
        /// </summary>
        public bool CanChange { get; set; }

        #region ICloneable Members

        public object Clone()
        {
            FileDataBaseEntryParameter newPara = new FileDataBaseEntryParameter();
            newPara.Name = this.Name;
            newPara.Value = this.Value;
            newPara.UserEditable = this.UserEditable;
            newPara.SuccessfullySet = this.SuccessfullySet;
            newPara.EditorType = this.EditorType;
            newPara.CanChange = this.CanChange;
            return newPara;
        }

        #endregion
    }
}
