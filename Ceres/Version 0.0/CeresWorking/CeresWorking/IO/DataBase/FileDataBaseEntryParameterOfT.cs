﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.DataBase
{
    public class FileDataBaseEntryParameter<T> : FileDataBaseEntryParameter
    {
        public new T Value
        {
            get
            {
                return (T)base.Value;

            }
            set
            {
                base.Value = value;
            }
        }
    }
}
