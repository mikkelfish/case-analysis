﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.DataBase
{
    public class ExperimentEntry : DataBaseEntry
    {
        public ExperimentEntry()
            : base()
        {

        }

        public ExperimentEntry(ExperimentEntry parent)
            : base(parent)
        {

        }

        public ExperimentEntry(string name)
            : base(name)
        {

        }

        public ExperimentEntry(string name, ExperimentEntry parent)
            : base(name, parent)
        {

        }

        public override string DefaultName
        {
            get { return "New Experiment"; }
        }
    }
}
