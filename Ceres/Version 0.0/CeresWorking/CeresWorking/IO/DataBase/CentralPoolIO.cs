﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using CeresBase.Data;
using System.Windows;
using System.IO;
using MesosoftCommon.Utilities.Settings;
using System.Reflection;
using CeresBase.Projects;
using CeresBase.IO.Serialization;
using System.Collections.ObjectModel;

namespace CeresBase.IO.DataBase
{
    public static class CentralIO
    {

        #region Old Merge

        private class centralStoragePara
        {
            public string Name { get; set; }
            public object Value { get; set; }
        }

        private class centralStorageEntry
        {
            public centralFileItem File { get; set; }
            public string Experiment { get; set; }
            public string HeaderType { get; set; }

            private List<centralStoragePara> paras = new List<centralStoragePara>();
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
            public List<centralStoragePara> Paras
            {
                get
                {
                    return this.paras;
                }
            }

        }

        public class centralFileItem
        {
            private static ICeresReader[] readers = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<ICeresReader>(null);

            public FileDataBaseEntryParameter[] Parameters { get; set; }

            private string fileName;
            public string FileName
            {
                get
                {
                    return this.fileName;
                }
                set
                {
                    this.fileName = value;
                    foreach (ICeresReader reader in readers)
                    {
                        if (reader.Supported(this.FileName))
                        {
                            this.Reader = reader;
                            break;
                        }
                    }
                }
            }

            public ICeresReader Reader { get; private set; }

            public override int GetHashCode()
            {
                return this.FileName.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                if (!(obj is centralFileItem)) return false;
                return this.FileName == (obj as centralFileItem).FileName;
            }
        }

        public class centralVariableItem
        {
            public centralFileItem File { get; set; }

            public FileDataBaseVariableEntryParameter Header { get; set; }
            public string Experiment { get; set; }

            public override string ToString()
            {

                if (this.Experiment == null || this.Header == null) return "";
                return this.Experiment + '\\' + this.Header.VariableName.Value;
            }
        }

        public static void OldMerge()
        {
            string storagePath = Paths.DefaultPath.LocalPath + "VariableLoading\\";

            Dictionary<centralFileItem, List<centralStorageEntry>> dict =
                MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(storagePath + "CentralIO.xaml") as Dictionary<centralFileItem, List<centralStorageEntry>>;
            if (dict != null)
            {
                foreach (centralFileItem file in dict.Keys)
                {
                    //ICeresReader[] readers = CeresBase.General.Utilities.CreateInterfaces<ICeresReader>(null);
                    //foreach (ICeresReader reader in readers)
                    //{
                    //    if (reader.Supported(file.FileName))
                    //    {
                    //        file.Reader = reader;
                    //        break;
                    //    }
                    //}

                    foreach (centralStorageEntry item in dict[file])
                    {
                        centralVariableItem varItem = new centralVariableItem() { File = file, Experiment = item.Experiment };
                        Type headerType = Type.GetType(item.HeaderType);
                        varItem.Header = (FileDataBaseVariableEntryParameter)Activator.CreateInstance(headerType);
                        foreach (centralStoragePara para in item.Paras)
                        {
                            PropertyInfo info = varItem.Header.GetType().GetProperty(para.Name);
                            object val = info.GetValue(varItem.Header, null);
                            PropertyInfo valInfo = val.GetType().GetProperty("Value");
                            valInfo.SetValue(val, para.Value, null);
                        }

                        List<CentralVariableItem> variables = typeof(CentralIO).GetField("variables", BindingFlags.Static | BindingFlags.NonPublic).GetValue(null) as List<CentralVariableItem>;

                        if (!variables.Any(v => v.Experiment == varItem.Experiment && v.Header.VariableName.Value == varItem.Header.VariableName.Value))
                        {
                            variables.Add(new CentralVariableItem()
                            {
                                Experiment = varItem.Experiment,
                                File = new CentralFileItem() { FileName = varItem.File.FileName, Parameters = varItem.File.Parameters },
                                Header = varItem.Header
                            });
                        }
                    }
                }
            }

            CeresBase.UI.VariableLoading.VariableLoading.Instance.RebuildTree();
        }

        #endregion

        static CentralIO()
        {
            NewSerializationCentral.DatabaseChanged += new EventHandler(SerializationCentral_DatabaseChanged);
        }

        static void SerializationCentral_DatabaseChanged(object sender, EventArgs e)
        {
            CeresBase.UI.VariableLoading.VariableLoading.Instance.RebuildTree();
        }

        //static CentralIO()
        //{

        //    Dictionary<centralFileItem, List<centralStorageEntry>> dict =
        //        MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(storagePath + "CentralIO.xaml") as Dictionary<centralFileItem, List<centralStorageEntry>>;
        //    if (dict != null)
        //    {
        //        foreach (centralFileItem file in dict.Keys)
        //        {
        //            ICeresReader[] readers = CeresBase.General.Utilities.CreateInterfaces<ICeresReader>(null);
        //            foreach (ICeresReader reader in readers)
        //            {
        //                if (reader.Supported(file.FileName))
        //                {
        //                    file.Reader = reader;
        //                    break;
        //                }
        //            }

        //            foreach (centralStorageEntry item in dict[file])
        //            {
        //                centralVariableItem varItem = new centralVariableItem() { File = file, Experiment = item.Experiment };
        //                Type headerType = Type.GetType(item.HeaderType);
        //                varItem.Header = (FileDataBaseVariableEntryParameter)Activator.CreateInstance(headerType);
        //                foreach (centralStoragePara para in item.Paras)
        //                {
        //                    PropertyInfo info = varItem.Header.GetType().GetProperty(para.Name);
        //                    object val = info.GetValue(varItem.Header, null);
        //                    PropertyInfo valInfo = val.GetType().GetProperty("Value");
        //                    valInfo.SetValue(val, para.Value, null);
        //                }

                        

        //                variables.Add(varItem);
        //            }
        //        }
        //    }

        //}

        //private static string storagePath = Paths.DefaultPath.LocalPath + "VariableLoading\\";


        //private static void writeInfo()
        //{
        //    Dictionary<centralFileItem, List<centralStorageEntry>> dict = new Dictionary<centralFileItem, List<centralStorageEntry>>();
        //    foreach (centralVariableItem var in variables)
        //    {
        //        if (!dict.Keys.Any(k => k.FileName == var.File.FileName))
        //        {
        //            dict.Add(var.File, new List<centralStorageEntry>());
        //        }

        //        centralStorageEntry entry = new centralStorageEntry() { Experiment = var.Experiment, HeaderType=var.Header.GetType().AssemblyQualifiedName };

        //        PropertyInfo[] infos = var.Header.GetType().GetProperties();
        //        foreach (PropertyInfo info in infos)
        //        {
        //            if (info.PropertyType.IsGenericType &&
        //                info.PropertyType.GetGenericTypeDefinition() == typeof(VariableEntryParameterItem<>))
        //            {
        //                object val = info.GetValue(var.Header, null);
        //                if (val != null)
        //                {
        //                    PropertyInfo value = val.GetType().GetProperty("Value");
        //                    object propVal = value.GetValue(val, null);
        //                    entry.Paras.Add(new centralStoragePara() { Name = info.Name, Value = propVal });
        //                }
        //            }
        //        }

        //        dict[var.File].Add(entry);
        //    }

        //    if (!Directory.Exists(storagePath))
        //    {
        //        Directory.CreateDirectory(storagePath);
        //    }

        //    using (FileStream stream = new FileStream(storagePath + "CentralIO.xaml", FileMode.Create, FileAccess.Write))
        //    {
        //        MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(dict, stream);
        //    }            
        //}



        private static ObservableCollection<CentralVariableItem> variables =
            NewSerializationCentral.RegisterCollection<ObservableCollection<CentralVariableItem>>(typeof(CentralIO), "variables");

        public static bool ContainsVariable(string experiment, string varname)
        {
            return variables.Any(v => v.ToString() == experiment + '\\' + varname);
        }

        private static CentralVariableItem getItem(string experiment, string varname)
        {
            return variables.FirstOrDefault(v => v.ToString() == experiment + '\\' + varname);
        }

        public static event ProgressChangedEventHandler VariableLoaded;

        internal static void LoadVariables(FileDataBaseVariableEntry[] variablesToLoad)
        {
            Dictionary<CentralFileItem, List<VariableHeader>> headers = new Dictionary<CentralFileItem, List<VariableHeader>>();
            foreach (FileDataBaseVariableEntry var in variablesToLoad)
            {
                CentralVariableItem item = variables.SingleOrDefault(v => v.Experiment == (var.Parent as ExperimentEntry).Path &&  v.Header.VariableName.Value == var.Name);
                if (item == null) continue;

                CentralFileItem file = headers.Keys.SingleOrDefault(key => key.FileName == item.File.FileName);
                if (file == null)
                {
                    headers.Add(item.File, new List<VariableHeader>());
                    file = item.File;
                }

                headers[file].Add(item.Header.CreateHeader(item.Experiment));
            }

            int variablesLoaded = 0;

            DoWorkEventArgs e = new DoWorkEventArgs(variablesToLoad);

            string error = "";
            foreach (CentralFileItem file in headers.Keys)
            {
                try
                {
                    Variable[] vars = file.Reader.ReadVariables(file.FileName, file.Parameters, headers[file].ToArray());

                    if (file.Reader == null) throw new InvalidOperationException("The file " + file.FileName + " is not supported.");

                    ProgressChangedEventHandler handle = VariableLoaded;
                    if (handle != null)
                    {
                        handle(e, new ProgressChangedEventArgs(variablesLoaded, vars));
                    }

                    if (e.Cancel)
                    {
                        return;
                    }


                    if (vars != null)
                    {
                        VariableSource.AddVariables(vars);
                    }
                    variablesLoaded += headers[file].Count;
                }
                catch (Exception ex)
                {
                    error += ex.Message + "\n";
                }
            }

            if (error != "")
            {
                MessageBox.Show("There have been errors loading files.\n" + error);
            }

            EpochCentral.VariablesLoaded();
        }

        internal static void AddVariable(FileDataBaseEntry file, string experiment, FileDataBaseVariableEntryParameter header)
        {
            if (file.Status != FileDataBaseEntryStatus.Complete)
                throw new InvalidOperationException("File has not been completed");

            //centralFileItem fileItem = files.SingleOrDefault(cf => cf.FileName == file.Filepath);
            //if (fileItem == null)
            //{
            //    
            //    files.Add(fileItem);
            //}

            CentralFileItem fileItem = variables.Where(v => v.File.FileName == file.Filepath).Select(v => v.File).FirstOrDefault();
            if(fileItem == null)
                fileItem = new CentralFileItem() { FileName = file.Filepath, Parameters = file.Parameters };
            CentralVariableItem varItem = new CentralVariableItem(){Header = header, Experiment = experiment, File = fileItem};
            
            
            if (variables.Any(v => v.ToString() == varItem.ToString()))
            {
                throw new InvalidOperationException("This variable already exists.");
            }

            variables.Add(varItem);
            //TODO  FIRE EVENT

        }

        internal static DataBaseEntry[] CreateDatabaseTree()
        {
            List<ExperimentEntry> topExperiments = new List<ExperimentEntry>();

            List<CentralVariableItem> unique = new List<CentralVariableItem>();
            foreach (CentralVariableItem var in variables)
            {
                if (!unique.Any(item => var.ToString() == item.ToString()))
                {
                    unique.Add(var);
                }
            }

            variables.Clear();

            foreach (CentralVariableItem item in unique)
            {
                variables.Add(item);

            }

            foreach (CentralVariableItem item in variables)
            {
                string[] splits = item.Experiment.Split(new char[]{'\\'}, StringSplitOptions.RemoveEmptyEntries);
                ExperimentEntry curEntry = topExperiments.SingleOrDefault(ex => ex.Name == splits[0]);

                if (curEntry == null)
                {
                    curEntry = new ExperimentEntry(splits[0]);
                    topExperiments.Add(curEntry);
                }

                for (int i = 1; i < splits.Length; i++)
                {
                    ExperimentEntry nextEntry = curEntry.Children.SingleOrDefault(ex => ex.Name == splits[i] && ex.GetType() == typeof(ExperimentEntry)) as ExperimentEntry;

                    if (nextEntry == null)
                    {
                        nextEntry = new ExperimentEntry(splits[i], curEntry);
                    }
                    curEntry = nextEntry;

                }

                FileDataBaseVariableEntry var = new FileDataBaseVariableEntry(item.Header.VariableName.Value, curEntry) 
                    { DisplayName = item.Header.DisplayName.Value, Comments = item.Header.Comments.Value };
            }

            return topExperiments.ToArray();
        }

        internal static bool RemoveVariable(string experiment, string varname)
        {
            CentralVariableItem item = getItem(experiment, varname);
            if (item == null) return false;

            VariableSource.RemoveVariable(experiment, varname);
            variables.Remove(item);

            EpochCentral.DeleteVariableEpochs(experiment, varname);

            //TODO  FIRE EVENT

            //If there are no more variables that point to the file remove it
            //if (!variables.Exists(v => v.File == item.File))
            //{
            //    files.Remove(item.File);
            //}

            return true;
        }
    }
}
