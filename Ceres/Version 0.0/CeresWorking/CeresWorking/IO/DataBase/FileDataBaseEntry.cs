﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Shapes;

namespace CeresBase.IO.DataBase
{
    public enum FileDataBaseEntryStatus { Complete, NeedsVars, NeedsAll, NotLoaded };
    
    public class FileDataBaseEntry : DataBaseEntry
    {
        private static ICeresReader[] readers;

        static FileDataBaseEntry()
        {
            FileDataBaseEntry.readers = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<ICeresReader>(null);
        }

        public FileDataBaseEntry(string name) : base(name)
        {

        }

        private FileDataBaseEntryStatus status = FileDataBaseEntryStatus.NotLoaded;

        public FileDataBaseEntryStatus Status 
        {
            get
            {
                return this.status;
            }
        }

        public void UpdateStatus()
        {
            if (this.Filepath == null)
            {
                this.status = FileDataBaseEntryStatus.NotLoaded;
                this.OnPropertyChanged("Status");
                return;
            }

            if (this.Parameters != null)
            {
                foreach (FileDataBaseEntryParameter para in this.Parameters)
                {
                    if (para.UserEditable && !para.SuccessfullySet)
                    {
                        this.status = FileDataBaseEntryStatus.NeedsAll;
                        this.OnPropertyChanged("Status");
                        return;
                    }
                }
            }

            if (this.VariableParameters == null)
            {
                this.status = FileDataBaseEntryStatus.NeedsVars;
                this.OnPropertyChanged("Status");
                return;

            }

            foreach (FileDataBaseVariableEntryParameter para in this.VariableParameters)
            {
                if (!para.IsComplete)
                {
                    this.status = FileDataBaseEntryStatus.NeedsVars;
                    this.OnPropertyChanged("Status");
                    return;
                }
            }

            this.status = FileDataBaseEntryStatus.Complete;
            this.OnPropertyChanged("Status");
        }

        public FileDataBaseEntry(string name, FileDataBaseEntry parent)
            : base(name, parent)
        {
            
        }

        public FileDataBaseEntryParameter[] Parameters { get; private set; }
        public FileDataBaseVariableEntryParameter[] VariableParameters { get; private set; }

        public void LoadVariableParameters()
        {
            if (this.Reader == null)
                throw new InvalidOperationException("Reader is null");

            if (this.VariableParameters != null) return;
            this.VariableParameters = this.Reader.GetVariableParameters(this);
            this.UpdateStatus();
        }

        public ICeresReader Reader { get; private set; }
        
        public string Filepath { get; private set; }

        public void SetParameters(FileDataBaseEntryParameter[] parameters)
        {
            this.Parameters = parameters;
        }

        public static bool CanLoadFile(string filepath)
        {
            foreach (ICeresReader reader in FileDataBaseEntry.readers)
            {
                if (reader.Supported(filepath))
                {
                    return true;                    
                }
            }

            return false;
        }

        public void LoadFile(string filepath)
        {
            this.Filepath = filepath;

            foreach (ICeresReader reader in FileDataBaseEntry.readers)
            {
                if (reader.Supported(filepath))
                {
                    this.Reader = reader;
                    break;
                }
            }

            if (this.Reader == null) 
                throw new ArgumentException("There is no reader that supports this file.");

            //Get the parameters that are calculated or not
            try
            {
                this.Parameters = this.Reader.GetParameters(this);
                this.UpdateStatus();

            }
            catch
            {
            }

        }

        public override string DefaultName
        {
            get { return "New File"; }
        }


    }
}
