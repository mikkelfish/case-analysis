﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.DataBase
{
    public class CentralFileItem
    {
        private static ICeresReader lastReader;

        private static ICeresReader[] readers = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<ICeresReader>(null);

        public FileDataBaseEntryParameter[] Parameters { get; set; }

        private string fileName;
        public string FileName
        {
            get
            {
                return this.fileName;
            }
            set
            {
                this.fileName = value;
              
            }
        }

        private ICeresReader reader;
        public ICeresReader Reader 
        {
            get
            {
                if (reader == null && this.fileName != null)
                {
                    if (lastReader != null)
                    {
                        if (lastReader.Supported(this.FileName))
                        {
                            this.reader = lastReader;
                        }
                    }

                    if (this.reader == null)
                    {
                        foreach (ICeresReader read in readers)
                        {
                            if (read.Supported(this.FileName))
                            {
                                this.reader = read;
                                lastReader = read;
                                break;
                            }
                        }
                    }
                }

                return this.reader;
            }
        }
         
        public override int GetHashCode()
        {
            return this.FileName.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is CentralFileItem)) return false;
            return this.FileName == (obj as CentralFileItem).FileName;
        }
    }
}
