using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Data;
using System.Collections;
using CeresBase.Development;
using CeresBase.IO.DataBase;

namespace CeresBase.IO
{
    [CeresCreated("Mikkel", "03/15/06")]
    public interface ICeresSerializable
    {
        [CeresCreated("Mikkel", "03/15/06")]
        void AddSpecificInformation(ICeresWriter writer, ICeresFile file);
        [CeresCreated("Mikkel", "03/15/06")]
        void ReadSpecificInformation(ICeresReader reader, ICeresFile file);
    }
}
