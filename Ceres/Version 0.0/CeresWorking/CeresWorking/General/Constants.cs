using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;

namespace CeresBase.General
{
    /// <summary>
    /// This class contains all the constants used by Ceres throughout the program that aren't specifically data related.
    /// </summary>
    [CeresCreated("Mikkel", "03/14/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    public static class Constants
    {
        #region Private Variables
        private static readonly DateTime timeless = new DateTime(1000, 1, 1);
        private static readonly DateTime fileBaseTime = new DateTime(1950, 1, 1);

        #endregion

        #region Private Functions

        #endregion

        #region Properties
        /// <summary>
        /// Get the DateTime that Ceres uses to specify a "timeless" variable.
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public static DateTime Timeless
        {
            get { return Constants.timeless; }
        }

        [CeresCreated("Mikkel", "03/16/06")]
        public static DateTime FileBaseTime
        {
            get
            {
                return Constants.fileBaseTime;
            }
        }

        #endregion

        #region Constructors

        #endregion

        #region Public Functions
        public static double SecondsSinceBase(DateTime time)
        {
            return time.Subtract(Constants.FileBaseTime).TotalSeconds;
        }

        public static DateTime DateTimeFromBaseStr(string baseTime, double seconds)
        {
            DateTime time = DateTime.Parse(baseTime);
            time.Add(TimeSpan.FromSeconds(seconds));
            return time;
        }
        #endregion
    }
}
