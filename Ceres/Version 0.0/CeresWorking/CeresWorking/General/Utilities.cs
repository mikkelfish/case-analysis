using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Lambda.Generic.Arithmetic;
using System.Collections;
using CeresBase.Development;
using System.IO;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using CeresBase.IO.Serialization.Translators;

namespace CeresBase.General
{
    /// <summary>
    /// Provides utility functions for all of Ceres
    /// </summary>
    [CeresCreated("Mikkel", "03/14/06")]
    public static class Utilities
    {
        public static T[,] DoubleArrayToSquare<T>(T[][] doubleArray)
        {
            if (doubleArray == null)
            {
                throw new NullReferenceException("Passed double array is null");
            }            

            int length = doubleArray[0].Length;
            for (int i = 0; i < doubleArray.Length; i++)
            {
                if (doubleArray[i] == null)
                {
                    throw new InvalidProgramException("Passed double array isn't fully initialized");
                }

                if (doubleArray[i].Length != length)
                {
                    throw new InvalidProgramException("Passed double array doesn't have the same length.");
                }


            }

            T[,] toRet = new T[doubleArray.Length, doubleArray[0].Length];
            for (int i = 0; i < doubleArray.Length; i++)
            {
                for (int j = 0; j < doubleArray[i].Length; j++)
                {
                    toRet[i, j] = doubleArray[i][j];
                }
            }

            return toRet;
        }

        public static T[][] SquareArrayToDoubleArray<T>(T[,] squareArray)
        {
            T[][] toRet = new T[squareArray.GetLength(0)][];
            for (int i = 0; i < toRet.Length; i++)
			{
                toRet[i] = new T[squareArray.GetLength(1)];
                for (int j = 0; j < toRet[i].Length; j++)
                {
                    toRet[i][j] = squareArray[i, j];
                }
			}

            return toRet;
        }


        public static byte[] StructToBytes<T>(T s )
        {
            StructLayoutAttribute attr = MesosoftCommon.Utilities.Reflection.Common.GetAttribute<StructLayoutAttribute>(typeof(T), false);

            if (attr != null)
            {
                int size = Marshal.SizeOf(s);
                byte[] retArr = new byte[size];
                IntPtr buf = Marshal.AllocHGlobal(size); // create unmanaged memory
                Marshal.StructureToPtr(s, buf, false); // copy struct

                for (int i = 0; i < size; ++i)
                {
                    retArr[i] = Marshal.ReadByte(buf, i); // read unmanaged bytes
                }
                Marshal.FreeHGlobal(buf);
                return retArr;

            }

            SerializableAttribute serial = MesosoftCommon.Utilities.Reflection.Common.GetAttribute<SerializableAttribute>(typeof(T), false);
                if (serial == null) throw new Exception("Type not serializable");

                MemoryStream ms = new MemoryStream();
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, s);
                return ms.GetBuffer();
            
        }

        public static T BytesToStruct<T>(byte[] arr) where T : new()
        {
            StructLayoutAttribute attr = MesosoftCommon.Utilities.Reflection.Common.GetAttribute<StructLayoutAttribute>(typeof(T), false);

            if (attr != null)
            {

                T retVal = new T();
                int size = Marshal.SizeOf(retVal);
                //if ( arr.Length < size ) // Throw
                IntPtr buf = Marshal.AllocHGlobal(size);
                for (int i = 0; i < size; ++i)
                {
                    Marshal.WriteByte(buf, i, arr[i]);
                }

                retVal = (T)Marshal.PtrToStructure(buf, typeof(T));
                Marshal.FreeHGlobal(buf);
                return retVal;
            }

            SerializableAttribute serial = MesosoftCommon.Utilities.Reflection.Common.GetAttribute<SerializableAttribute>(typeof(T), false);
            if (serial == null) throw new Exception("Type not serializable");

            MemoryStream ms = new MemoryStream(arr);
            BinaryFormatter bf = new BinaryFormatter();
            return (T)bf.Deserialize(ms);
        }


       

        #region Private Variables

        private static Hashtable methodInfos = new Hashtable();
        private static Hashtable argConstructors = new Hashtable();
        private static readonly object lockable = new object();
        private static Dictionary<string, TypeConverter> convertDictionary = new Dictionary<string, TypeConverter>();


        //private struct argHashEntry
        //{
        //    public ConstructorInfo Info;
        //    //public int[] Assignments;
        //}

        #endregion

        #region Public Functions
        public delegate void EmptyHandler();

        public delegate void TextStreamAction(char[] buffer, int count, object state);
        public static void ReadTextFromFileStream(StreamReader reader, int maxRead, int bufferSize, TextStreamAction action, object state)
        {
            MethodInfo readInfo = reader.GetType().GetMethod("Read", new Type[]{typeof(char[]), typeof(int)
                ,typeof(int)});
            int offset = 0;
            int read = 0;
            int total = 0;

            char[] buffer = new char[bufferSize];

            while (true)
            {
                int readSize = bufferSize;
                if (maxRead != -1)
                {
                    if (bufferSize > (maxRead - total))
                    {
                        readSize = maxRead - total;
                    }
                }

                readSize -= offset;

                if (readSize < 0) return;

                read = (int)readInfo.Invoke(reader, new object[] { buffer, offset, readSize });

                if (read <= 0) break;
                offset += read;
                if (offset == bufferSize)
                {
                    action(buffer, bufferSize, state);
                    offset = 0;
                }

                total += read;

                if (maxRead != -1)
                {
                    if (total >= maxRead)
                    {
                        break;
                    }
                }
            }

            if (offset != 0)
            {
                action(buffer, offset, state);
            }
        }

        public delegate void StreamAction(byte[] buffer, int count, object state);
        public static void ReadFromStream(object reader, int maxRead, int bufferSize, StreamAction action, object state)
        {
            MethodInfo readInfo = reader.GetType().GetMethod("Read", new Type[]{typeof(byte[]), typeof(int)
                ,typeof(int)});
            int offset = 0;
            int read = 0;
            int total = 0;

            byte[] buffer = new byte[bufferSize];

            while (true)
            {
                int readSize = bufferSize;
                if(maxRead != -1) 
                {
                    if(bufferSize > (maxRead - total))
                    {
                        readSize = maxRead - total;
                    }
                }

                readSize -= offset;

                if (readSize < 0) break;

                read = (int)readInfo.Invoke(reader, new object[] { buffer, offset, readSize });

                if (read <= 0)
                {
                    break;
                }


                offset += read;

                if (offset == bufferSize)
                {
                    action(buffer, bufferSize, state);
                    offset = 0;
                }
                total += read;

                

                if (maxRead != -1)
                {
                    if (total >= maxRead)
                    {
                        break;
                    }
                }       
            }

            if (offset != 0)
            {
                action(buffer, offset, state);
            }
        }

        public static bool CanConvertStrictly(Type toConvertType, Type targetType)
        {
            if (toConvertType == targetType || toConvertType.IsSubclassOf(targetType))
            {
                return true;
            }

            if (toConvertType == typeof(byte) || toConvertType == typeof(sbyte) ||
                toConvertType == typeof(short) ||
                toConvertType == typeof(ushort) ||
                toConvertType == typeof(int) ||
                toConvertType == typeof(uint) ||
                toConvertType == typeof(float) ||
                toConvertType == typeof(long) ||
                toConvertType == typeof(ulong) ||
                toConvertType == typeof(double))
            {
                if (targetType == typeof(byte) || targetType == typeof(sbyte) ||
                    targetType == typeof(short) ||
                    targetType == typeof(ushort) ||
                    targetType == typeof(int) ||
                    targetType == typeof(uint) ||
                    targetType == typeof(float) ||
                    targetType == typeof(long) ||
                    targetType == typeof(ulong) ||
                    targetType == typeof(double))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool CanConvert(Type toConvertType, Type targetType)
        {
            if (toConvertType.IsSubclassOf(targetType) || toConvertType == targetType)
            {
                return true;
            }
            string key = toConvertType.FullName + targetType.FullName;
            if (!convertDictionary.ContainsKey(key))
            {

                object[] attrs = toConvertType.GetCustomAttributes(typeof(TypeConverter), true);
                if (attrs != null && attrs.Length > 0)
                {
                    if ((attrs[0] as TypeConverter).CanConvertTo(targetType))
                    {
                        convertDictionary.Add(key, attrs[0] as TypeConverter);
                        return true;
                    }
                }

                convertDictionary.Add(key, null);
                Type type = toConvertType.GetInterface(typeof(IConvertible).Name, true);
                if (type != null) return true;
            }
            else
            {
                TypeConverter converter = convertDictionary[key];
                if (converter != null)
                {
                    return converter.CanConvertTo(targetType);
                }

                Type type = toConvertType.GetInterface(typeof(IConvertible).Name, true);
                if (type != null) return true;
            }
            return false;
        }

        public static object GetDefaultObject(Type targetType)
        {
            if (targetType == typeof(string)) return String.Empty;
            return targetType.InvokeMember("", BindingFlags.CreateInstance, null, null, null);
        }

        public static object Convert(object toConvert, Type targetType)
        {
            Type objType = toConvert.GetType();
            if (objType.IsSubclassOf(targetType) || objType == targetType)
            {
                return toConvert;
            }

            string key = objType.FullName + targetType.FullName;
            if (!convertDictionary.ContainsKey(key))
            {

                object[] attrs = objType.GetCustomAttributes(typeof(TypeConverter), true);
                if (attrs != null && attrs.Length > 0)
                {
                    if ((attrs[0] as TypeConverter).CanConvertTo(targetType))
                    {
                        convertDictionary.Add(key, attrs[0] as TypeConverter);
                        return (attrs[0] as TypeConverter).ConvertTo(toConvert, targetType);
                    }
                }

                convertDictionary.Add(key, null);
                if (toConvert is IConvertible)
                {
                    return System.Convert.ChangeType(toConvert, targetType);
                }
            }
            else
            {
                TypeConverter converter = convertDictionary[key];
                if (converter != null)
                {
                    return converter.ConvertTo(toConvert, targetType);
                }

                if (toConvert is IConvertible)
                {
                    return System.Convert.ChangeType(toConvert, targetType);
                }
            }

            return null;
        }

        

        /// <summary>
        /// Create an object with the passed arguments through reflection
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public static object CreateObjectWithArgs(Type objectType, CeresArgWrapper[] args)
        {
            return Utilities.CreateObjectWithArgs(objectType, args, null);
        }
        
        /// <summary>
        /// Create an object with the passed arguments and parameter types if the type is generic.
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="args"></param>
        /// <param name="parameterTypes"></param>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        [CeresToDo("Mikkel", "03/14/06", Comments = "Test to see how long it takes to generate the methods", 
            Priority=DevelopmentPriority.Low)]
        public static object CreateObjectWithArgs(Type objectType, CeresArgWrapper[] args, Type[] parameterTypes)
        {
            //string key = objectType.ToString();
            //foreach (CeresArgWrapper arg in args)
            //{
            //    key += arg.Description;
            //}
            //foreach (Type pType in parameterTypes)
            //{
            //    key += pType.Name;
            //}

            ConstructorInfo info = null;

            //lock (Utilities.lockable)
            //{
            //    if (Utilities.argConstructors.Contains(key))
            //    {
            //        argHashEntry entry = (argHashEntry)Utilities.argConstructors[key];
            //        info = entry.Info;
            //        //assignments = entry.Assignments;
            //    }
            //}

            int[] assignments = null;


            //if (info == null)
            //{
                ConstructorInfo[] constructors = objectType.GetConstructors();
                ConstructorInfo matchedConstructor = null;
                foreach (ConstructorInfo construct in constructors)
                {
                    object[] attrs = construct.GetCustomAttributes(typeof(CeresArgsAttribute), true);
                    if (attrs.Length == 0) return null;

                    bool match = (attrs[0] as CeresArgsAttribute).ArgumentsMatch(args, out assignments);
                    if (!match) continue;

                    matchedConstructor = construct;
                    break;
                }

                if (matchedConstructor == null) return null;

                Type[] constructorTypes = new Type[args.Length];
                for (int i = 0; i < args.Length; i++)
                {
                    constructorTypes[i] = args[assignments[i]].Argument.GetType();
                }

                Type constructedType = objectType;
                if (constructedType.IsGenericType) constructedType = objectType.MakeGenericType(parameterTypes);

                info = constructedType.GetConstructor(constructorTypes);

                if (info == null) return null;

                //argHashEntry entry = new argHashEntry();
                ////entry.Assignments = assignments;
                //entry.Info = info;

                //lock (Utilities.lockable)
                //{
                //    Utilities.argConstructors.Add(key, entry);
                //}

            //}
            //else
            //{
            //    CeresArgsAttribute attr = Utilities.GetAttribute<CeresArgsAttribute>(info);
                
            //}

            object[] sortedArgs = new object[args.Length];
            for (int i = 0; i < sortedArgs.Length; i++)
            {
                sortedArgs[i] = args[assignments[i]].Argument;
            }

            return info.Invoke(sortedArgs);
        }


     


     

        /// <summary>
        /// Get a calculator that can do generic math with the supplied type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public static IMath<T> GetCalculator<T>()
        {
            object calculator;
            if (typeof(T) == typeof(double)) calculator = new Lambda.Generic.Arithmetic.DoubleMath();
            else if (typeof(T) == typeof(float)) calculator = new Lambda.Generic.Arithmetic.FloatMath();
            else if (typeof(T) == typeof(int)) calculator = new Lambda.Generic.Arithmetic.IntMath();
            else if (typeof(T) == typeof(short)) calculator = new Lambda.Generic.Arithmetic.ShortMath();
            else if (typeof(T) == typeof(long)) calculator = new Lambda.Generic.Arithmetic.LongMath();
            else if (typeof(T) == typeof(sbyte)) calculator = new Lambda.Generic.Arithmetic.SByteMath();
            else if (typeof(T) == typeof(byte)) calculator = new Lambda.Generic.Arithmetic.ByteMath();
            else throw new Exception("This object is not supported by GetCalculator.");
            return (IMath<T>)calculator;
        }

        private static int tempNumber = 0;

        /// <summary>
        /// Get a filepath to a unique temp file.
        /// </summary>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        public static string GetTempFilePath()
        {
            int number;
            lock (Utilities.lockable)
            {
                number = Utilities.tempNumber;
                Utilities.tempNumber++;
            }

            string baseStr = System.Environment.GetEnvironmentVariable("TEMP", EnvironmentVariableTarget.User) + "temp";
            if (baseStr == "temp") baseStr = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "temp";
            return baseStr + number.ToString();
        }

        #endregion
    }
}
