﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using Microsoft.Win32;
using CeresBase.IO;

namespace CeresBase.General
{
    public abstract class CombinedBatchOutput : IBatchWritable
    {
        #region IBatchWritable Members

        protected abstract object[][] GetWrittenFormat(ref CombinedBatchOutput[] write);

        public IBatchWritable[] WriteToDisk(IBatchWritable[] toWrite)
        {
            SaveFileDialog diag = new SaveFileDialog();

            //if (this.label != null)
            //{
            //    string toBuild = "";
            //    for (int i = 0; i < this.label.Length; i++)
            //    {
            //        if (!char.IsLetterOrDigit(this.label[i]) && this.label[i] != ' ')
            //        {
            //            toBuild += "-";
            //        }
            //        else toBuild += this.label[i];
            //    }
            //    diag.FileName += toBuild;
            //}
            diag.Filter = "CSV(*.csv)|*.csv|All Files(*.*)|*.*";
            diag.AddExtension = true;
            diag.DefaultExt = ".csv";
            if (diag.ShowDialog().Value == false) return toWrite.Where(view => !(view is CombinedBatchOutput)).ToArray();

            CombinedBatchOutput[] outputs = toWrite.Where(view => view is CombinedBatchOutput).Cast<CombinedBatchOutput>().ToArray();

            List<object[][]> allOuts = new List<object[][]>();
            while (outputs != null && outputs.Length > 0)
            {
                object[][] toAdd = outputs[0].GetWrittenFormat(ref outputs);

                if(toAdd != null)
                    allOuts.Add(toAdd);
            }

            if (allOuts.Count == 0) return toWrite.Where(view => !(view is CombinedBatchOutput)).ToArray();

            int maxLines = allOuts.OrderByDescending(o => o.Length).Select(o => o.Length).First();
            int[] widths = (from object[][] element in allOuts
                            select
                            (
                                 (from object[] row in element
                                  orderby row.Length descending
                                  select row.Length).First()
                            )).ToArray();


            List<object[]> writeArray = new List<object[]>();
            for (int i = 0; i < maxLines; i++)
            {
                List<object> line = new List<object>();

                for (int j = 0; j < allOuts.Count; j++)
                {
                    if (allOuts[j].Length <= i)
                    {
                        for (int k = 0; k < widths[j]; k++)
                        {
                            line.Add(null);
                        }
                    }
                    else
                    {
                        for (int k = 0; k < widths[j]; k++)
                        {
                            if (allOuts[j][i].Length <= k)
                            {
                                line.Add(null);
                            }
                            else line.Add(allOuts[j][i][k]);
                        }
                    }

                    line.Add(null);
                }


                writeArray.Add(line.ToArray());
            }

            CSVWriter.WriteCSV(diag.FileName, writeArray.ToArray());

            return toWrite.Where(view => !(view is CombinedBatchOutput)).ToArray();
            
        }

        #endregion
    }
}
