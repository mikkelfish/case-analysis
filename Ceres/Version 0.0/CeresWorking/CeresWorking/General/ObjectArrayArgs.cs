using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;

namespace CeresBase.General
{

    [CeresCreated("Mikkel", "03/14/06")]
    public class ObjectArrayArgs : EventArgs
    {
        #region Private Variables
        private object[] arg;

        #endregion

        #region Private Functions

        #endregion

        #region Properties
        [CeresCreated("Mikkel", "03/14/06")]
        public object[] Args
        {
            get { return arg; }
            set { arg = value; }
        }
        #endregion

        #region Constructors
        [CeresCreated("Mikkel", "03/14/06")]
        public ObjectArrayArgs(object[] args)
        {
            this.arg = args;
        }
        #endregion

        #region Public Functions

        #endregion
    }    
}
