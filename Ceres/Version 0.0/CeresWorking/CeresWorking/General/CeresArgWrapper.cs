using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;

namespace CeresBase.General
{
    /// <summary>
    /// This is used to help facilitate the creation of objects using reflection without the function having to know
    /// the organization of the passed parameters.
    /// </summary>
    [CeresCreated("Mikkel", "03/14/06")]
    public struct CeresArgWrapper
    {
        private string description;
        public string Description { get { return this.description; } }

        private object argument;
        public object Argument { get { return this.argument; } }

        public CeresArgWrapper(string description, object argument)
        {
            this.description = description;
            this.argument = argument;
        }
    }
}
