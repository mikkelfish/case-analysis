using System;
using System.Collections.Generic;
using System.Text;
using CeresBase.Development;

namespace CeresBase.General
{
    /// <summary>
    /// This is used to help facilitate the creation of objects using reflection without the function having to know
    /// the organization of the passed parameters.
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = false)]
    [CeresCreated("Mikkel", "03/14/06")]
    public class CeresArgsAttribute : Attribute
    {
        public bool ArgumentsMatch(CeresArgWrapper[] descriptors, out int[] associations)
        {
            associations = new int[descriptors.Length];
            for (int i = 0; i < associations.Length; i++)
            {
                int index = this.arguments.IndexOf(descriptors[i].Description);
                if (index < 0) return false;
                associations[i] = index;
            }
            return true;
        }

        private List<string> arguments;
        public string[] Arguments
        {
            get { return arguments.ToArray(); }
            set { arguments = new List<string>(value); }
        }

        public CeresArgsAttribute(params string[] arguments)
        {
            this.arguments = new List<string>(arguments);
        }
    }

}
