using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms; //Necessary for message boxes

/*********************************
 * FILE CONTENTS
 * This is intended to be a temporary class, was used at first to write files when I was working with streams.
 * Now, its primary purpose is to have many message boxes that are easy to use to display exceptions, etc.
 * This should be easily removed later if need be.
 * We should use a class like this to store the current project name once we get to writing a way to save and load projects in this program, then anything that needs to reference the name of the project can just use this class.
 *
 *   author David Allen
 * - Strings to set paths for files so you do not have to check the registry every time a different class is used
 * - Message box templates (ErrorBox, WarningBox, RetryBox, YesNoBox, YesNoCancelBox)
 *      - Makes it easier to output a message and looks the same throughout the program
 *      - For errors, can pass the exception and/or a string to give error details
 *      - RetryBox returns a DialogResult and asks if the user would like to retry or cancel.
 * 
 * 
 *********************************/


//SETUP SO YOU CAN JUST USE A using REFERENCE FOR THIS!
//SETUP SO YOU CAN JUST USE A using REFERENCE FOR THIS!
//SETUP SO YOU CAN JUST USE A using REFERENCE FOR THIS!
//SETUP SO YOU CAN JUST USE A using REFERENCE FOR THIS!
//SETUP SO YOU CAN JUST USE A using REFERENCE FOR THIS!
//SETUP SO YOU CAN JUST USE A using REFERENCE FOR THIS!
//SETUP SO YOU CAN JUST USE A using REFERENCE FOR THIS!
//SETUP SO YOU CAN JUST USE A using REFERENCE FOR THIS!
//SETUP SO YOU CAN JUST USE A using REFERENCE FOR THIS!
//SETUP SO YOU CAN JUST USE A using REFERENCE FOR THIS!

namespace CeresBase
{
    /// <summary>
    /// Contains anything that would be global to the entire project but has no real nice place to go,
    /// such as error boxes, file paths, and file / directory creation / deletion code.
    /// </summary>
    public static class Global
    {
        public static string Path;
        public static string LogPath;

        /// <summary>
        /// Sets up all of the paths; call this after the main path (path) has been set
        /// </summary>
        public static void SetPaths()
        {
            LogPath = Path + @"logs\";
        }

        ///////////////
        // Error Boxes
        ///////////////
        /// <summary>
        /// Displays a message box with error details.
        /// Can pass it a custom error message and and exception.
        /// Displays string first.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        public static void ErrorBox(string s, Exception e)
        {
            ErrorBox(s + "\n\nException: " + e.ToString() + "\n" + e.Message);
        }

        /// <summary>
        /// Displays a message box with error details.
        /// Can pass it a custom error message and and exception.
        /// Displays exception details first.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        public static void ErrorBox(Exception e, string s)
        {
            ErrorBox("Exception: " + e.ToString() + "\n" + e.Message + "\n\n" + s);
        }

        /// <summary>
        /// Displays a message box with error details.
        /// Pass it the name of the exception to work.
        /// </summary>
        /// <param name="e"></param>
        public static void ErrorBox(Exception e)
        {
            ErrorBox("An error occured.\n\nException: " + e.ToString() + "\n" + e.Message);
        }

        /// <summary>
        /// Used if a string is passed.
        /// </summary>
        public static void ErrorBox(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Used if no exception is passed.
        /// </summary>
        public static void ErrorBox()
        {
            MessageBox.Show("An error occured.\nNo details are present.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        /////////////////
        // Warning Boxes
        /////////////////
        /// <summary>
        /// Displays a message box with warning details.
        /// Can pass it a custom message and an exception.
        /// Displays string first.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        public static void WarningBox(string s, Exception e)
        {
            WarningBox(s + "\n\nException: " + e.ToString() + "\n" + e.Message);
        }

        /// <summary>
        /// Displays a message box with warning details.
        /// Can pass it a custom message and an exception.
        /// Displays exception details first.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        public static void WarningBox(Exception e, string s)
        {
            WarningBox("Exception: " + e.ToString() + "\n" + e.Message + "\n\n" + s);
        }

        /// <summary>
        /// Displays a message box with warning details.
        /// Can pass it an exception.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        public static void WarningBox(Exception e)
        {
            WarningBox("Exception: " + e.ToString() + "\n" + e.Message);
        }

        /// <summary>
        /// Displays a message box with warning details.
        /// Can pass it a custom message.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        public static void WarningBox(string s)
        {
            MessageBox.Show(s, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        ///////////////
        // Retry Boxes
        ///////////////
        /// <summary>
        /// Creates a message box that asks user to retry or cancel.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static DialogResult RetryBox(string s)
        {
            return RetryBox(s, "Try Again?");
        }

        /// <summary>
        /// Pass a string message and title of the box.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static DialogResult RetryBox(string s, string title)
        {
            return MessageBox.Show(s, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question);
        }

        ////////////////
        // Yes No Boxes
        ////////////////
        /// <summary>
        /// Pass a string message.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static DialogResult YesNoBox(string s)
        {
            return YesNoBox(s, "Choose");
        }

        /// <summary>
        /// Pass a strinng message and title title for the box
        /// </summary>
        /// <param name="s"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static DialogResult YesNoBox(string s, string title)
        {
            return MessageBox.Show(s, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        /// <summary>
        /// Pass a string message. Returns button pressed.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static DialogResult YesNoCancelBox(string s)
        {
            return YesNoCancelBox(s, "Choose");
        }

        /// <summary>
        /// Pass a strinng message and title title for the box. Returns button pressed.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static DialogResult YesNoCancelBox(string s, string title)
        {
            return MessageBox.Show(s, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
        }

        /////////////////////
        // Information Boxes
        /////////////////////
        /// <summary>
        /// Display information box
        /// </summary>
        /// <param name="s"></param>
        /// <param name="title"></param>
        public static void InfoBox(string s, string title)
        {
            MessageBox.Show(s, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Display information box
        /// </summary>
        /// <param name="s"></param>
        public static void InfoBox(string s)
        {
            InfoBox(s, "Information");
        }

    }
}
