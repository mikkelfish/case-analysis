using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CeresBase
{
    public partial class InputBox : Form
    {
        //CREATE A WAY TO RESIZE IT BY PASSING A PARAMETER! WOULD BE SWEET

        private string value;
        private string textValue;

        /// <summary>
        /// Default constructor - simple input box with only one line of input.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="content"></param>
        public InputBox(String title, String inputLabel)
        {
            InitializeComponent();
            // Window Definitions
            this.Text = title;
            this.Size = new System.Drawing.Size(140, 90);

            // Item Definitions
            this.labelInputBox.Text = inputLabel;   //location 3,1 //this should never change location
            tInput.Location = new System.Drawing.Point(4, 18);
            bCancel.Location = new System.Drawing.Point(this.Width / 2 - bCancel.Width - 4, 40);
            bOK.Location = new System.Drawing.Point(bCancel.Right + 2, bCancel.Top);
            TextArea.Visible = false;
            labelTextArea.Visible = false;
        }

        /// <summary>
        /// This constructor is used if you want to enable the text box too
        /// </summary>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <param name="style"></param>
        public InputBox(String title, String inputLabel, String textLabel)
        {
            InitializeComponent();
            this.Text = title;
            this.labelInputBox.Text = inputLabel;
            this.labelTextArea.Text = textLabel;
            this.Size = new System.Drawing.Size(142, 168);

            bCancel.Location = new System.Drawing.Point(this.Width / 2 - bCancel.Width - 4, 123);
            bOK.Location = new System.Drawing.Point(bCancel.Right + 2, bCancel.Top);

        }

        /// <summary>
        /// Return value from input box.
        /// If return value in INPUTCANCEL, then the Cancel button was clicked.
        /// </summary>
        public string Value
        { get { return value; } }

        /// <summary>
        /// Returns value from text box.
        /// </summary>
        public string TextValue
        { get { return textValue; } }

        /// <summary>
        /// Returns value stored in the input box and text box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bOK_Click(object sender, EventArgs e)
        {
            if (tInput.Text != "")
            {
                this.value = this.tInput.Text;
                this.DialogResult = DialogResult.OK;
                if (TextArea.Visible) this.textValue = TextArea.Text;
                this.Close();
            }
            else { CeresBase.Global.ErrorBox("You must first input text."); }
        }

        /// <summary>
        /// Returns INPUTCANCEL when pressed to Value.
        /// TextValue is ignored.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.value = "";
            this.Close();
        }

        private void tInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.DialogResult = DialogResult.OK;
                this.value = this.tInput.Text;
                if (TextArea.Visible) this.textValue = this.TextArea.Text;
                this.Close();
            }
        }

        private void InputBox_Load(object sender, EventArgs e)
        {

        }

        private void tInput_TextChanged(object sender, EventArgs e)
        {

        }
    }
}