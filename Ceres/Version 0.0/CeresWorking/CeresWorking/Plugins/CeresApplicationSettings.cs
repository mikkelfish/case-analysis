//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Reflection;
//using Razor.Configuration;
//using System.Windows.Forms;
//using System.IO;

//namespace CeresBase.Plugins
//{
    
//    public class CeresApplicationSettings
//    {
//        private class entry
//        {
//            private Settings.XMLSettingAttribute attr;
//            public Settings.XMLSettingAttribute Attribute
//            {
//                get { return attr; }
//            }

//            private FieldInfo field;
//            public FieldInfo Field
//            {
//                get { return field; }
//            }

//            public entry(Settings.XMLSettingAttribute attr, FieldInfo info)
//            {
//                this.attr = attr;
//                this.field = info;
//            }
//        }

//        private Dictionary<string, entry> attributes = new Dictionary<string, entry>();

//        private bool setup = false;

//        [Settings.XMLSetting("CeresApplicationSettings", "IO", true, true)] 
//        private string fileLocation;        
//        public string FileWriteLocation
//        {
//            get 
//            {
//                if (!setup) this.setupFunc();
//                if (fileLocation == null)
//                {
//                    FolderBrowserDialog selectFolder = new FolderBrowserDialog();
//                    if (selectFolder.ShowDialog() == DialogResult.Cancel) return null;
//                    this.FileWriteLocation = selectFolder.SelectedPath;

//                }
//                string toRet = fileLocation;
//                if (toRet[toRet.Length - 1] != '\\') toRet += "\\";
//                return toRet;
//            }
//            set 
//            { 
//                fileLocation = value;
//                this.setSetting("fileLocation", value);
//            }
//        }

//        [Settings.XMLSetting("CeresMarkers", "", true, true)] 
//        private string markerLocation;
//        public string MarkerLocation
//        {
//            get 
//            {
//                if (!setup) this.setupFunc(); 
//                return markerLocation; 
//            }
//            set 
//            {
//                markerLocation = value;
//                this.setSetting("markerLocation", value);
//            }
//        }

//        private void setupFunc()
//        {
//            Settings.XMLSettingAttribute[] attrsA;
//            FieldInfo[] fields = CeresBase.General.Utilities.GetFieldsWithAttribute<Settings.XMLSettingAttribute>(this, out attrsA);
//            if (fields == null) return;

//            for (int i = 0; i < fields.Length; i++)
//            {
//                FieldInfo info = fields[i];
//                Settings.XMLSettingAttribute attr = attrsA[i];
//                attributes.Add(info.Name, new entry(attr, info));

//                //if (attr.Category.Categories.Contains(info.Name))
//               // {
//                    XmlConfigurationOption option = attr.Category.Options[info.Name];
//                    if (option == null) continue;
//                    info.SetValue(this, option.Value);
//               // }
//            }
//        }
	

//        public CeresApplicationSettings()
//        {
            
//        }

//        protected void setSetting(string name, object val)
//        {
//            entry entry = attributes[name];
//            if (entry == null) return;
//            XmlConfigurationOption option = entry.Attribute.Category.Options[entry.Field.Name, true];
//            option.Value = val;
//        }
        
//    }
//}
