using System.Collections.Generic;
using CeresBase.General;
using ThreadSafeConstructs;
using System.Reflection;
using CeresBase.Development;

namespace CeresBase.Threading
{
    /// <summary>
    /// This is used internally to carry around the information that the managed thread pool actually calls
    /// </summary>
    [CeresCreated("Mikkel", "03/14/06", DocumentedStatus=CeresDocumentedStage.FlowChartOnly)]
    internal class ThreadSpawnerQueueItem
    {
        #region Private Variables
        ThreadSafeEventHandler<ObjectArrayArgs> callback;
        object[] args;
        private int priority;
        private bool marked;
        private int id;
        private ThreadSpawner spawner;
        #endregion

        #region Public Properties
        
        
        [CeresCreated("Mikkel", "03/14/06")]
        public ThreadSafeEventHandler<ObjectArrayArgs> Callback { get { return this.callback; } }

        [CeresCreated("Mikkel", "03/14/06")]
        public object[] Args { get { return this.args; } }

        [CeresCreated("Mikkel", "03/14/06")]
        public int Priority
        {
            get { return priority; }
            set { priority = value; }
        }

        [CeresCreated("Mikkel", "03/14/06")]
        public bool ThrowAway { get { return this.Priority == ThreadSpawner.ThrowAwayItem; } }

        [CeresCreated("Mikkel", "03/14/06")]
        public bool Marked
        {
            get { return marked; }
            set { marked = value; }
        }

        [CeresCreated("Mikkel", "03/14/06")]
        public int ID
        {
            get { return id; }
        }

        [CeresCreated("Mikkel", "03/14/06")]
        public ThreadSpawner Spawner
        {
            get { return spawner; }
            set { spawner = value; }
        }

        #endregion

        #region Constructors
        public ThreadSpawnerQueueItem(ThreadSpawner spawner, ThreadSafeEventHandler<ObjectArrayArgs> function, object[] args, int id)
        {
            this.callback = function;
            this.args = args;
            this.id = id;
            this.spawner = spawner;
        }
        #endregion

        //public ThreadSpawnerQueueItem(ThreadSpawner spawner, MethodInfo info, object[] args, int id)
        //{
        //    this.info = info;
        //    this.args = args;
        //    this.id = id;
        //    this.spawner = spawner;
        //}
    }
}