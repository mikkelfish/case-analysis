using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using CeresBase.General;
using Toub.Threading;
using CeresBase.Development;

namespace CeresBase.Threading
{
    /// <summary>
    /// This static class looks at all the active spawners and decides which function to enter into the queue next
    /// </summary>
    [CeresCreated("Mikkel", "03/14/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    internal static class SpawnerManager
    {
        #region Private Variables
        private static readonly object lockable = new object();
        private static List<ThreadSpawner> spawners = new List<ThreadSpawner>();
        private static Random rand = new Random();
        #endregion

        #region Private Functions
        private static int sorter(ThreadSpawner spawn1, ThreadSpawner spawn2)
        {
            if (spawn1.Priority > spawn2.Priority) return 1;
            if (spawn1.Priority < spawn2.Priority) return -1;
            return SpawnerManager.rand.Next(-1, 1);
        }

        private static void runItem(object obj)
        {
            ThreadSpawnerQueueItem item = obj as ThreadSpawnerQueueItem;
            Exception error = null;
            try
            {
                if (item.Callback != null)
                    item.Callback(item.Spawner, new ObjectArrayArgs(item.Args));
            }
            catch (Exception ex)
            {
                error = ex;
                System.Diagnostics.Trace.WriteLine(ex);
            }
            finally
            {
                item.Spawner.endedJob(item, error);
                SpawnerManager.FillQueue();
            }
        }

        #endregion

        #region Properties

        #endregion

        #region Public Functions

        /// <summary>
        /// Add a threadspawner to the active list
        /// </summary>
        /// <param name="spawner"></param>
        [CeresCreated("Mikkel", "03/14/06")]
        internal static void AddSpawner(ThreadSpawner spawner)
        {
            lock (SpawnerManager.lockable)
            {
                SpawnerManager.spawners.Add(spawner);
            }
        }

        /// <summary>
        /// Remove a threadspawner from the active list
        /// </summary>
        /// <param name="spawner"></param>
        [CeresCreated("Mikkel", "03/14/06")]
        internal static void RemoveSpawner(ThreadSpawner spawner)
        {
            lock (SpawnerManager.lockable)
            {
                SpawnerManager.spawners.Remove(spawner);
            }
        }

        /// <summary>
        /// Fill the queue.
        /// </summary>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        internal static bool FillQueue()
        {
            lock (SpawnerManager.lockable)
            {
                while (ManagedThreadPool.ActiveThreads + ManagedThreadPool.WaitingCallbacks < ManagedThreadPool.MaxThreads)
                {
                    SpawnerManager.spawners.Sort();
                    ThreadSpawnerQueueItem item = null;
                    for (int i = 0; i < SpawnerManager.spawners.Count; i++)
                    {
                        item = SpawnerManager.spawners[i].getNextItem();
                        if (item != null) break;
                    }

                    if (item == null) return false;

                    Toub.Threading.ManagedThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(SpawnerManager.runItem),
                        item);
                }
                return true;
            }
        }
        #endregion

    }
}
