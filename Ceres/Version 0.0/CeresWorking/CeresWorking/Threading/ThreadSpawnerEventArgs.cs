using System.Collections.Generic;
using CeresBase.General;
using CeresBase.Development;
using System;

namespace CeresBase.Threading
{
    /// <summary>
    /// Used for the events called by a thread spawner
    /// </summary>
    [CeresCreated("Mikkel", "03/14/06")]
    public class ThreadSpawnerEventArgs : System.EventArgs
    {
        private int id;
        private object[] args;
        private Exception error;

        /// <summary>
        /// Get the arguments passed to be used with the function
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public ObjectArrayArgs Args
        {
            get { return new ObjectArrayArgs(args); }
        }

        /// <summary>
        /// Get the id of this function
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public int ID
        {
            get { return id; }
        }

        /// <summary>
        /// Returns the exception that the function threw if applicable
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public Exception Error
        {
            get
            {
                return this.error;
            }
        }

        private int itemsLeft;
        public int ItemsLeft
        {
            get { return itemsLeft; }
        }

        private ThreadSpawner spawner;
        public ThreadSpawner Spawner
        {
            get { return spawner; }
        }
	
	

        /// <summary>
        /// Create thread spawner event args
        /// </summary>
        /// <param name="id"></param>
        /// <param name="error"></param>
        /// <param name="args"></param>
        [CeresCreated("Mikkel", "03/14/06")]
        public ThreadSpawnerEventArgs(ThreadSpawner spawner, int id, int itemsLeft,  Exception error, object[] args)
        {
            this.id = id;
            this.error = null;
            this.args = args;
            this.itemsLeft = itemsLeft;
            this.spawner = spawner;
        }
    }
}
