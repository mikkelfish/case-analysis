using System;
using System.Collections.Generic;
using System.Text;
using SThreading = System.Threading;
using System.Collections;
using CeresBase.General;
using ThreadSafeConstructs;
using System.Reflection;
using CeresBase.Development;

namespace CeresBase.Threading
{
    /// <summary>
    /// This is the pivotal class for CeresThreading. It allows you to specify a group of functions that you want run in parallel
    /// and whether to block and wait for them to complete or continue on the thread. It should be sufficient for any (non-UI) multithreading.
    /// Make sure to use it whenever you have heavy duty data processing to take advantage of threading/multiple cores.
    /// </summary>
    [CeresCreated("Mikkel", "03/14/06", DocumentedStatus=CeresDocumentedStage.Complete)]
    public class ThreadSpawner
    {
        #region Private Variables

        private List<ThreadSpawnerQueueItem> jobs;
        private SThreading.ManualResetEvent threadEvent;
        private SpawnerPriority priority;
        private readonly object lockable = new object();
        private bool blocking;
        private int itemsLeft;
        private object tag;

        #endregion

        #region Private Functions

        private void endedAll()
        {
            SpawnerManager.RemoveSpawner(this);
            this.AllCompleted.CallEvent(this, EventArgs.Empty);
            if (this.blocking) this.threadEvent.Set();
        }


        private int comparer(ThreadSpawnerQueueItem item1, ThreadSpawnerQueueItem item2)
        {
            if (!item1.Marked) item1.Priority = this.Filter.CallEvent(this, new ObjectArrayArgs(item1.Args));
            if (!item2.Marked) item2.Priority = this.Filter.CallEvent(this, new ObjectArrayArgs(item2.Args));
            item1.Marked = true;
            item2.Marked = true;
            if (item1.Priority < item2.Priority) return 1;
            else if (item1.Priority > item2.Priority) return -1;
            return 0;
        }
        #endregion

        #region Events

        /// <summary>
        /// All the filtering events
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public ThreadSafeEvent<ObjectArrayArgs, int> Filter = new ThreadSafeEvent<ObjectArrayArgs, int>();

        /// <summary>
        /// Runs when an item is canceled because it was filtered out
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public ThreadSafeEvent<ThreadSpawnerEventArgs> ItemCanceled = new ThreadSafeEvent<ThreadSpawnerEventArgs>();

        /// <summary>
        /// Runs when an item was completed successfully
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public ThreadSafeEvent<ThreadSpawnerEventArgs> ItemCompleted = new ThreadSafeEvent<ThreadSpawnerEventArgs>();

        /// <summary>
        /// Runs when all the functions passed to the threadspawner have completed
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public ThreadSafeEvent<EventArgs> AllCompleted = new ThreadSafeEvent<EventArgs>();

        #endregion

        #region Public Misc Constructs
        /// <summary>
        /// The priority for the threadspawner will determine whether its items get processed before other threadspawners
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public enum SpawnerPriority { Low, Normal, High };
        
        /// <summary>
        /// Return this from a filtering delegate to specify that the item should be canceled.
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public const int ThrowAwayItem = int.MaxValue;
        #endregion

        #region Public Attributes

        /// <summary>
        /// Get the spawner priority
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public SpawnerPriority Priority
        {
            get { return priority; }
        }

        /// <summary>
        /// Gets or sets an object that will be exposed when the various events are run.
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public object Tag
        {
            get
            {
                return this.tag;
            }
            set
            {
                this.tag = value;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create a spawner with normal priority
        /// </summary>
        [CeresCreated("Mikkel", "03/14/06")]
        public ThreadSpawner() : this(SpawnerPriority.Normal) { }
        
        /// <summary>
        /// Create a spawner with a set priority
        /// </summary>
        /// <param name="priority"></param>
        [CeresCreated("Mikkel", "03/14/06")]
        public ThreadSpawner(SpawnerPriority priority)
            : base()
        {
            this.jobs = new List<ThreadSpawnerQueueItem>();
            this.threadEvent = new System.Threading.ManualResetEvent(false);
            this.priority = priority;
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Add a function to the spawner to be completed.
        /// </summary>
        /// <param name="function"></param>
        /// <param name="args"></param>
        [CeresCreated("Mikkel", "03/14/06")]
        public void AddFunction(ThreadSafeEventHandler<ObjectArrayArgs> function, object[] args)
        {
            ThreadSpawnerQueueItem item = new ThreadSpawnerQueueItem(this, function, args, this.jobs.Count);
            this.jobs.Add(item);
        }

        /// <summary>
        /// Start the spawner
        /// </summary>
        /// <param name="block">If block is true, it will block the executing thread until all the items are completed. Otherwise
        /// thread execution continues.</param>
        [CeresCreated("Mikkel", "03/14/06")]
        public void StartJobs(bool block)
        {
            SpawnerManager.AddSpawner(this);
            this.itemsLeft = this.jobs.Count;
            SpawnerManager.FillQueue();
            this.blocking = block;
            if (this.blocking)
            {
                this.threadEvent.WaitOne();
            }
        }
        
        /// <summary>
        /// SHOULD ONLY BE CALLED BY SpawnerManager
        /// </summary>
        /// <returns></returns>
        [CeresCreated("Mikkel", "03/14/06")]
        internal ThreadSpawnerQueueItem getNextItem()
        {
            List<ThreadSpawnerQueueItem> canceledItems;
            ThreadSpawnerQueueItem retitem;

            int left = 0;
            lock (this.lockable)
            {
                if (this.jobs.Count == 0) return null;

                foreach (ThreadSpawnerQueueItem item in this.jobs)
                {
                    item.Marked = false;
                }

                if (this.Filter != null)
                {
                    this.jobs.Sort(this.comparer);
                }

                canceledItems = this.jobs.FindAll(
                    delegate(ThreadSpawnerQueueItem item) { return item.ThrowAway; }
                    );

                this.jobs.RemoveRange(0, canceledItems.Count);
                this.itemsLeft -= canceledItems.Count;
                left = this.itemsLeft;

                if (this.jobs.Count == 0) return null;
                retitem = this.jobs[0];
                this.jobs.RemoveAt(0);
            }
            foreach (ThreadSpawnerQueueItem item in canceledItems)
            {
                this.ItemCanceled.CallEvent(this, new ThreadSpawnerEventArgs(this, item.ID, left, null, item.Args));
            }
            return retitem;
        }

        /// <summary>
        /// SHOULD ONLY BE CALLED BY SPAWNERMANAGER
        /// </summary>
        /// <param name="item"></param>
        /// <param name="error"></param>
        [CeresCreated("Mikkel", "03/14/06")]
        internal void endedJob(ThreadSpawnerQueueItem item, Exception error)
        {
            bool allDone = false;
            int left = 0;
            lock (this.lockable)
            {
                this.itemsLeft--;
                allDone = this.itemsLeft == 0;
                left = this.itemsLeft;
            }

            this.ItemCompleted.CallEvent(this, new ThreadSpawnerEventArgs(this, item.ID, left, error, item.Args));
            if (allDone) this.endedAll();
        }

        
        #endregion
    }
}
