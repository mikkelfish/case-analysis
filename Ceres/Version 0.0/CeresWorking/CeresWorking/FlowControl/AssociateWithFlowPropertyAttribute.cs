﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    [global::System.AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public sealed class AssociateWithPropertyAttribute : Attribute
    {
        public bool ReadOnly { get; set; }
        public bool DisableLink { get; set; }
        public bool ShouldSerialize { get; set; }
        public bool CanLinkFrom { get; set; }
        public bool IsOutput { get; set; }
        public bool ReloadOnChange { get; set; }

        public string PropertyName { get; private set; }
        public AssociateWithPropertyAttribute(string propName)
        {
            this.PropertyName = propName;
            this.ShouldSerialize = true;
            this.CanLinkFrom = true;
        }
    }
}
