﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Collections;
using CeresBase.Data;
using System.Reflection;
using System.ComponentModel;
using MesosoftCommon.Interfaces;

namespace CeresBase.FlowControl
{
    public class Routine
    {
        public event EventHandler<FlowControlAdapterInitArgs> AdaptersInitialized;

        public object Args { get; set; }

        private int idIterator = 0;

        //Change this to a list
        private ObservableCollection<Process> processList = new ObservableCollection<Process>();
        public ObservableCollection<Process> ProcessList
        {
            get
            {
                return processList;
            }
            set
            {
                processList = value;
            }
        }

        private List<List<Process>> processingStack = new List<List<Process>>();
        public List<List<Process>> ProcessingStack
        {
            get
            {
                return processingStack;
            }
        }

        private List<Process> storageProcessList = new List<Process>();



        public void GenerateRoutineProcessingStack()
        {
            //First, build the directory of dependencies.  This helps build the stack in linear time instead of exponential time.
            Dictionary<Process, List<Process>> dependedOnBy = new Dictionary<Process,List<Process>>();

            //Next, build a dictionary of the processing level each process is on.  This is required to build the stack correctly.
            Dictionary<Process, int> processingLevelOf = new Dictionary<Process, int>();

            //We only care about executables, ignoring storage processes.
            List<Process> ExecutableProcesses = new List<Process>();
            foreach (Process proc in ProcessList)
            {
                if (proc.IsAStorageProcess)
                {
                    storageProcessList.Add(proc);
                    continue;
                }
                ExecutableProcesses.Add(proc);
            }

            //initialize the dictionary
            foreach (Process proc in ExecutableProcesses)
            {
                dependedOnBy.Add(proc, new List<Process>());
            }

            //populate dependencies
            foreach (Process proc in ExecutableProcesses)
            {
                foreach (PropertyListing property in proc.PropertiesList)
                {
                    if (property.Value is PropertyLink)
                    {
                        //Ignore storage processes
                        if ((property.Value as PropertyLink).HostProcess.IsAStorageProcess) continue;
                        dependedOnBy[(property.Value as PropertyLink).HostProcess].Add(proc);
                    }
                    else if (property.Value is PropertyMultiLink)
                    {
                        foreach (PropertyLink link in (property.Value as PropertyMultiLink).PropertyLinks)
                        {
                            //Ignore storage processes
                            if ((link.HostProcess.IsAStorageProcess)) continue;
                            dependedOnBy[link.HostProcess].Add(proc);
                        }
                    }
                }
            }
            
            //Clear the existing stack.
            processingStack.Clear();

            //Next, find the Processes with no outputs.  There implicitly has to be at least one of these unless a cyclical reference exists.
            //These go in the first level of the processingStack, so we'll go ahead and initialize that now.
            processingStack.Add(new List<Process>());

            foreach (Process proc in ExecutableProcesses)
            {
                bool hasNoOutputs = true;
                foreach (PropertyListing property in proc.PropertiesList)
                {
                    if (property.Value is PropertyLink && !(property.Value as PropertyLink).HostProcess.IsAStorageProcess)
                    {
                        hasNoOutputs = false;
                        break;
                    }
                    else if (property.Value is PropertyMultiLink)
                    {
                        foreach (PropertyLink link in (property.Value as PropertyMultiLink).PropertyLinks)
	                    {
                            if (link.HostProcess.IsAStorageProcess) continue;
                            hasNoOutputs = false;
                            break;
	                    }
                    }
                }

                if (hasNoOutputs)
                {
                    processingStack[0].Add(proc);
                    processingLevelOf[proc] = 0;
                }
            }

            //Keep track of the most recently filled stack level
            int priorStack = 0;

            while (processingStack[priorStack].Count > 0)
            {
                processingStack.Add(new List<Process>());

                foreach (Process proc in processingStack[priorStack])
                {
                    //Check all dependencies
                    foreach (Process dependantProc in dependedOnBy[proc])
                    {
                        //Don't bother to check this process if it's already been placed in the processingStack
                        if (processingLevelOf.ContainsKey(dependantProc))
                            continue;

                        //If the dependency has only outputs on the priorStack or lower, it can be placed on this stack level.
                        //Otherwise, it gets bumped.
                        bool placeHere = true;

                        foreach (PropertyListing property in dependantProc.PropertiesList)
                        {
                            if (property.Value is PropertyLink)
                            {
                                //Ignore Storage processes.
                                if ((property.Value as PropertyLink).HostProcess.IsAStorageProcess) continue;

                                if (!processingLevelOf.ContainsKey((property.Value as PropertyLink).HostProcess) ||
                                    processingLevelOf[(property.Value as PropertyLink).HostProcess] > priorStack)
                                {
                                    placeHere = false;
                                    break;
                                }
                            }
                            else if (property.Value is PropertyMultiLink)
                            {
                                foreach (PropertyLink link in (property.Value as PropertyMultiLink).PropertyLinks)
                                {
                                    //Ignore storage processes.
                                    if (link.HostProcess.IsAStorageProcess) continue;

                                    if (!processingLevelOf.ContainsKey(link.HostProcess) || processingLevelOf[link.HostProcess] > priorStack)
                                    {
                                        placeHere = false;
                                        break;
                                    }
                                }

                                if (placeHere == false) break;
                            }
                        }

                        if (placeHere)
                        {
                            processingStack[priorStack + 1].Add(dependantProc);
                            processingLevelOf[dependantProc] = priorStack + 1;
                        }
                    }
                }
                priorStack++;
            }
        }

        private int findProcessingLevel(Process target)
        {
            int level = 0;
            foreach (List<Process> procList in this.processingStack)
            {
                foreach (Process proc in procList)
                {
                    if (proc == target)
                    {
                        return level;
                    }
                }
                level++;
            }
            return -1;
        }

        public void ExpandProcess(PermutationProcess permProc, int procLevel)
        {
            permProc.MirroredSelves.Add(permProc);
            permProc.Adapter.SetIteration(0);
            permProc.IsAnExpansionProcess = false;

            //mirror by the iteration count
            for (int i = 1; i < permProc.Adapter.IterationCount; i++)
            {
                //Pad the list to make it ready for mirroring
                permProc.PermutationMemberProcesses.Add(new List<Process>());

                //Mirror the permutation itself
                PermutationProcess selfCopy = CopyOfProcess(permProc) as PermutationProcess;
                selfCopy.Name += " i" + i;
                selfCopy.IsAnExpansionProcess = true;
                selfCopy.Adapter.SetIteration(i);
                permProc.MirroredSelves.Add(selfCopy);
                processingStack[procLevel].Add(selfCopy);

                //Mirror the processes
                foreach (Process proc in permProc.PermutationMemberProcesses[0])
                {
                    Process newProc = CopyOfProcess(proc);
                    newProc.Name += " i" + i; 
                    permProc.PermutationMemberProcesses[i].Add(newProc);
                    //Copy over the copies to the processingStack as well
                    int thisProcLevel = this.findProcessingLevel(proc);
                    if (thisProcLevel < 0) continue;
                    processingStack[thisProcLevel].Add(newProc);
                }

                //Now correct links, after all members have been copied over
                foreach (Process proc in permProc.PermutationMemberProcesses[i])
                {
                    foreach (PropertyListing pl in proc.PropertiesList)
                    {
                        if (pl.Value is PropertyLink)
                        {
                            PropertyLink pLink = pl.Value as PropertyLink;
                            if (permProc.PermutationMemberProcesses[0].Contains(pLink.HostProcess))
                                pLink.HostProcess = permProc.PermutationMemberProcesses[i][permProc.PermutationMemberProcesses[0].IndexOf(pLink.HostProcess)];
                            if (pLink.HostProcess == permProc)
                                pLink.HostProcess = permProc.MirroredSelves[i];
                        }
                        else if (pl.Value is PropertyMultiLink)
                        {
                            PropertyMultiLink pmLink = pl.Value as PropertyMultiLink;
                            foreach (PropertyLink pLink in pmLink.PropertyLinks)
                            {
                                if (permProc.PermutationMemberProcesses[0].Contains(pLink.HostProcess))
                                    pLink.HostProcess = permProc.PermutationMemberProcesses[i][permProc.PermutationMemberProcesses[0].IndexOf(pLink.HostProcess)];
                                if (pLink.HostProcess == permProc)
                                    pLink.HostProcess = permProc.MirroredSelves[i];
                            }
                        }
                    }
                }

                //Now correct output links
                foreach (PermutationOutputProcess outProc in permProc.PermutationOutputProcesses)
                {
                    foreach (PropertyListing pl in outProc.PropertiesList)
                    {
                        if (pl.Value is PropertyLink)
                        {
                            PropertyLink pLink = pl.Value as PropertyLink;
                            if (permProc.PermutationMemberProcesses[0].Contains(pLink.HostProcess))
                            {
                                PropertyMultiLink pmLink = new PropertyMultiLink();
                                pmLink.PropertyLinks.Add(pLink);
                                pmLink.PropertyLinks.Add(new PropertyLink(
                                    permProc.PermutationMemberProcesses[i][permProc.PermutationMemberProcesses[0].IndexOf(pLink.HostProcess)],
                                    pLink.PropertyName));
                                pl.Value = pmLink;
                            }
                            if (pLink.HostProcess == permProc)
                            {
                                PropertyMultiLink pmLink = new PropertyMultiLink();
                                pmLink.PropertyLinks.Add(pLink);
                                pmLink.PropertyLinks.Add(new PropertyLink(
                                    permProc.MirroredSelves[i],
                                    pLink.PropertyName));
                                pl.Value = pmLink;
                            }
                        }
                        else if (pl.Value is PropertyMultiLink)
                        {
                            PropertyMultiLink pmLink = pl.Value as PropertyMultiLink;
                            for (int j = pmLink.PropertyLinks.Count - 1; j >= 0 ; j--)
                            {
                                PropertyLink pLink = pmLink.PropertyLinks[j];

                                if (permProc.PermutationMemberProcesses[0].Contains(pLink.HostProcess))
                                {
                                    pmLink.PropertyLinks.Add(new PropertyLink(
                                        permProc.PermutationMemberProcesses[i][permProc.PermutationMemberProcesses[0].IndexOf(pLink.HostProcess)],
                                        pLink.PropertyName));
                                }
                                if (pLink.HostProcess == permProc)
                                {
                                    pmLink.PropertyLinks.Add(new PropertyLink(
                                        permProc.MirroredSelves[i],
                                        pLink.PropertyName));
                                }
                            }
                        }
                    }
                }
            }
        }

        private Process CopyOfProcess(Process proc)
        {
            Type thisProcType = proc.GetType();
            ConstructorInfo ci = thisProcType.GetConstructor(Type.EmptyTypes);
            Process ret = ci.Invoke(null) as Process;

            ret.Adapter = proc.Adapter.Clone() as IFlowControlAdapter;
            ret.Adapter.LoadValuesFromSerialization(proc.Adapter.GetValuesForSerialization());
            ret.Name = proc.Name;

            ret.PermutationCopyID = proc.Adapter.ID;

            //Get rid of the current Properties since they are nulled
            //ret.PropertiesList.Clear();

            for (int i = 0; i < proc.PropertiesList.Count; i++)
            {
                PropertyListing source = proc.PropertiesList[i];
                PropertyListing pl = ret.PropertiesList.FirstOrDefault(p => p.Name == source.Name);
                if (pl == null)
                    throw new Exception("Routine does not have property " + pl.Name + " and the adapter may have been updated. Please see if you need a new version of the routine.");

                if (source.Value is PropertyLink)
                {
                    pl.Value = new PropertyLink((source.Value as PropertyLink).HostProcess, (source.Value as PropertyLink).PropertyName);
                }
                else if (source.Value is PropertyMultiLink)
                {
                    pl.Value = new PropertyMultiLink();
                    foreach (PropertyLink individualLink in (source.Value as PropertyMultiLink).PropertyLinks)
                    {
                        PropertyLink newLink = new PropertyLink(individualLink.HostProcess, individualLink.PropertyName);
                        (pl.Value as PropertyMultiLink).PropertyLinks.Add(newLink);
                    }
                }
                else
                    pl.Value = source.Value;

                pl.UsingAdapterDefault = source.UsingAdapterDefault;
                pl.CanEdit = source.CanEdit;
                pl.CanReceivePropertyLinks = source.CanReceivePropertyLinks;
                pl.IsOutput = source.IsOutput;
                pl.Description = source.Description;
                pl.CanSendPropertyLinks = source.CanSendPropertyLinks;
                pl.DisplayIOHooks = source.DisplayIOHooks;
            }

            //REDONE ABOVE

            ////Copy the PropertyListings over
            //foreach (PropertyListing pl in proc.PropertiesList)
            //{
            //    int index = ret.PropertiesList.IndexOf(pl);

            //    if (index < 0)
            //        throw new Exception("Routine does not have property " + pl.Name + " and the adapter may have been updated. Please see if you need a new version of the routine.");

            // //   if (proc.PropertiesList.Count != ret.PropertiesList.Count)
            ////        throw new Exception("The runtime routine is a different version than the saved routine. Please see if you need a new version of the routine.");

            //    PropertyListing source = proc.PropertiesList[index];

            //    if (source.Value is PropertyLink)
            //    {
            //        pl.Value = new PropertyLink((source.Value as PropertyLink).HostProcess, (source.Value as PropertyLink).PropertyName);
            //    }
            //    else if (source.Value is PropertyMultiLink)
            //    {
            //        pl.Value = new PropertyMultiLink();
            //        foreach (PropertyLink individualLink in (source.Value as PropertyMultiLink).PropertyLinks)
            //        {
            //            PropertyLink newLink = new PropertyLink(individualLink.HostProcess, individualLink.PropertyName);
            //            (pl.Value as PropertyMultiLink).PropertyLinks.Add(newLink);
            //        }
            //    }
            //    else
            //        pl.Value = source.Value;

            //    pl.UsingAdapterDefault = source.UsingAdapterDefault;
            //    pl.CanEdit = source.CanEdit;
            //    pl.CanReceivePropertyLinks = source.CanReceivePropertyLinks;
            //    pl.IsOutput = source.IsOutput;
            //    pl.Description = source.Description;
            //    pl.CanSendPropertyLinks = source.CanSendPropertyLinks;
            //    pl.DisplayIOHooks = source.DisplayIOHooks;
            //}

            //foreach (PropertyListing pl in proc.PropertiesList)
            //{
            //    ret.PropertiesList.Add(pl);
            //}

            if (ret is PermutationProcess)
            {
                PermutationProcess pRet = (ret as PermutationProcess);
                pRet.PermutationMemberProcesses = new List<List<Process>>();
                foreach (List<Process> procList in (proc as PermutationProcess).PermutationMemberProcesses)
                {
                    List<Process> thisList = new List<Process>();
                    foreach (Process thisProc in procList)
                    {
                        thisList.Add(thisProc);
                    }
                    pRet.PermutationMemberProcesses.Add(thisList);
                }

                pRet.PermutationOutputProcesses = new List<PermutationOutputProcess>();
                foreach (PermutationOutputProcess opProc in (proc as PermutationProcess).PermutationOutputProcesses)
                {
                    pRet.PermutationOutputProcesses.Add(opProc);
                }
            }

            return ret;
        }

        //The processingStack automatically excludes cyclical references, this merely checks to see which processes were not assigned
        //to the processingStack and returns the list.  This method doesn't function correctly before the processingStack has been built.
        public List<Process> ListCyclicalExecutionExclusions()
        {
            List<Process> ret = new List<Process>();
            foreach (Process proc in this.ProcessList)
            {
                ret.Add(proc);
            }

            foreach (List<Process> procList in processingStack)
            {
                foreach (Process proc in procList)
                {
                    ret.Remove(proc);
                }
            }

            return ret;
        }

        public bool IsProcessWithinAPermutation(Process proc)
        {
            foreach (Process routineProc in this.ProcessList)
            {
                if (routineProc is PermutationProcess)
                {
                    int i = 0;
                    PermutationProcess thisPermProc = routineProc as PermutationProcess;
                    if (proc == routineProc) return true;
                    if (thisPermProc.MirroredSelves.Contains(proc as PermutationProcess)) return true;
                    foreach (List<Process> procList in thisPermProc.PermutationMemberProcesses)
                    {
                        if (procList.Contains(proc)) return true;
                    }
                    if (thisPermProc.PermutationOutputProcesses.Contains(proc as PermutationOutputProcess)) return true;

                }
            }
            return false;
        }

        //private List<List<Process>> processingStack = new List<List<Process>>();
        //Run down the processing stack from count-1 to 0, executing and linking things forward.
        public List<RoutineBatchOutputPoint> RunRoutine()
        {
            List<RoutineBatchOutputPoint> ret = new List<RoutineBatchOutputPoint>();

            //First, throw the event to notify the world that adapters are initialized and we are ready to begin, so the runtime can inject any extra data.
            if (this.AdaptersInitialized != null)
            {
                FlowControlAdapterInitArgs args = new FlowControlAdapterInitArgs();
                foreach (Process proc in this.ProcessList)
                {
                    AdapterInitPair aip = new AdapterInitPair();
                    aip.HostProcessName = proc.Name;
                    aip.Adapter = proc.Adapter;

                    args.InitializedAdapters.Add(aip);
                }

                this.AdaptersInitialized(this, args);
            }

            foreach (Process proc in this.ProcessList)
            {
                proc.ReloadValuesOnlyFromAdapter();
            }

            for (int i = 0; i < processingStack.Count; i++)
            {
                //TODO : Change this to work with the "Wait" button and human input ordering
                foreach (Process proc in processingStack[i])
                {
                    runAndForwardProcess(proc, true, ret, i);
                }

                foreach (Process proc in storageProcessList)
                {
                    runAndForwardProcess(proc, false, ret, i);
                }
            }

            return ret;
        }

        private void runAndForwardProcess(Process proc, bool run, List<RoutineBatchOutputPoint> ret, int processingLevel)
        {
            if (proc.Adapter == null) return;

            UpdateLinksForProcess(proc);
            //TODO :
            //Adapter_RunComplete is used for multithreaded handling, add this in later
            //proc.Adapter.RunComplete += new EventHandler<FlowControlProcessEventArgs>(Adapter_RunComplete);

            //Links are updated, now refresh as necessary on the adapter.
            proc.ReloadValuesOnlyFromAdapter();

            //Push the properties to the adapter if we're not using defaults (in the case of user defaults, for instance)
            proc.PushPropertiesToAdapter();

            if (!run) return;

            //Now run the adapter.
            proc.RunAdapter();

            if (ret == null) return;


            //Now send back the output
            foreach (PropertyListing prop in proc.PropertiesList)
            {
                if (!prop.IsOutput) continue;

                //If it is an output, pull the value back from the adapter before updating.
                prop.Value = proc.Adapter.SupplyPropertyValue(prop.Name);

                RoutineBatchOutputPoint newOutput = new RoutineBatchOutputPoint();
                newOutput.OutputData = prop.Value;
                newOutput.SourceData = this.Args;
                newOutput.SourceProcessName = proc.Name;
                newOutput.Name = prop.Name;
                newOutput.ProcessingLevel = processingLevel;

                ret.Add(newOutput);
            }
        }

        private int findIterationOfProcess(Process proc)
        {
            foreach (Process checkProc in this.ProcessList)
            {
                if (!(checkProc is PermutationProcess)) continue;

                PermutationProcess permProc = checkProc as PermutationProcess;

                if (permProc.IsAnExpansionProcess) continue;

                if (proc is PermutationProcess)
                {
                    string[] split = proc.Name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (split.Length == 0) return -1;
                    if (split[split.Length - 1][0] != 'i') return 0;

                    string number = split[split.Length - 1].Remove(0, 1);
                    int index = 0;
                    if (int.TryParse(number, out index))
                        return index;
                    return -1;
                }
                else
                {
                    int iter = 0;
                    foreach (List<Process> procList in permProc.PermutationMemberProcesses)
                    {
                        foreach (Process thisProc in procList)
                        {
                            if (thisProc.ToString() == proc.ToString())
                                return iter;
                        }
                        iter++;
                    }
                }
            }

            return -1;
        }

        private object convertNumericValue(object val, Process targetProc, string link)
        {
            Type targetType = targetProc.Adapter.PropertyType(link);


            return MesosoftCommon.Utilities.Data.TypeTools.ConvertNumbersToType(val, targetType);          
        }

        public void UpdateLinksForProcess(Process proc)
        {
            //First, pull in values from propertyLinks as necessary.
            foreach (PropertyListing prop in proc.PropertiesList)
            {
                object property = prop.Value;

               // if (!((property is PropertyLink) || (property is PropertyMultiLink))) continue;

                if (property is PropertyLink)
                {
                    Process host = (property as PropertyLink).HostProcess;
                    string linkName = (property as PropertyLink).PropertyName;
                    object value;

                    if (PropertyListing.FindFirstByName(host.PropertiesList, linkName) == null) continue;

                    value = host.Adapter.SupplyPropertyValue(linkName);

                    value = convertNumericValue(value, proc, prop.Name);

                    if (proc.Adapter.CanReceivePropertyValue(value, prop.Name))
                    {
                        if (proc.Adapter is IPermutationOutputAdapter)
                        {
                            (proc.Adapter as IPermutationOutputAdapter).ReceivePropertyValue(value, prop.Name,
                                this.findIterationOfProcess(host));
                        }
                        else
                            proc.Adapter.ReceivePropertyValue(value, prop.Name);
                    }
                    else
                    {
                        //FlowControlParameterDescriptor desc = new FlowControlParameterDescriptor();
                        ////Todo : implement type swapping (make the descriptor do something here)

                        //proc.Adapter.ReceivePropertyValueAs(value, key, desc);


                        //Panic!
                        throw new Exception("Type mismatch exception : Property \"" + prop.Name + "\" in the process \""
                            + proc.Name + "\" could not import values because it is of the type \"" +
                            proc.Adapter.PropertyType(prop.Name) + "\" and the requested import value is of the type \"" +
                            value.GetType() + "\".  Please check the types of the many-to-one link.");
                    }
                }
                else if (property is PropertyMultiLink)
                {
                    //This is handled a bit differently for PermutationOutputAdapters.  Ooops.
                    if (proc.Adapter is IPermutationOutputAdapter)
                    {
                        foreach (PropertyLink link in (property as PropertyMultiLink).PropertyLinks)
                        {
                            Process host = link.HostProcess;
                            string linkName = link.PropertyName;
                            object value;

                            if (PropertyListing.FindFirstByName(host.PropertiesList, linkName) == null) continue;

                            value = host.Adapter.SupplyPropertyValue(linkName);

                            value = this.convertNumericValue(value, proc, prop.Name);

                            if (proc.Adapter.CanReceivePropertyValue(value, prop.Name))
                            {
                                (proc.Adapter as IPermutationOutputAdapter).ReceivePropertyValue(value, prop.Name,
                                    this.findIterationOfProcess(host));
                            }
                            else
                            {
                                //FlowControlParameterDescriptor desc = new FlowControlParameterDescriptor();
                                ////Todo : implement type swapping (make the descriptor do something here)

                                //proc.Adapter.ReceivePropertyValueAs(value, key, desc);

                                //Panic!
                                throw new Exception("Type mismatch exception : Property \"" + prop.Name + "\" in the process \""
                                    + proc.Name + "\" could not import values because it is of the type \"" +
                                    proc.Adapter.PropertyType(prop.Name) + "\" and the requested import value is of the type \"" +
                                    value.GetType() + "\".  Please check the types of the many-to-one link.");
                            }

                        }

                        continue;
                    }

                    //(else)

                    if ((property as PropertyMultiLink).PropertyLinks.Count <= 0)
                        throw new Exception("Internal error : Empty PropertyMultiLink!");

                    List<object> targetList = new List<object>();
                    Type targetType = proc.Adapter.PropertyType(prop.Name);

                    PropertyLink initialLink = (property as PropertyMultiLink).PropertyLinks[0];
                    Type enforcedType = initialLink.HostProcess.Adapter.PropertyType(initialLink.PropertyName);

                    foreach (PropertyLink link in (property as PropertyMultiLink).PropertyLinks)
                    {
                        Process host = link.HostProcess;
                        string linkName = link.PropertyName;
                        object value;

                        //check for type mismatch - we really don't want to deal with mixed - type arrays at this point.
                        //Maybe handle them later, fuck it for now.
                        if (host.Adapter.PropertyType(linkName) != enforcedType)
                            throw new Exception("Internal error : PropertyMultiLink type mismatch.");

                        value = host.Adapter.SupplyPropertyValue(linkName);

                        //Add to the targetList.
                        targetList.Add(value);
                    }

                    //The target list is built, now see if we're creating an array or a list.
                    if (targetType.IsArray)
                    {
                        Array newTarget = (Array)DataUtilities.CreateArray(targetList.Count, enforcedType);
                        Array.Copy(targetList.ToArray(), newTarget, targetList.Count);
                        if (proc.Adapter.CanReceivePropertyValue(newTarget, prop.Name))
                        {
                            proc.Adapter.ReceivePropertyValue(newTarget, prop.Name);
                        }
                        else
                        {
                            //Panic!
                            throw new Exception("Type mismatch exception : Property \"" + prop.Name + "\" in the process \""
                                + proc.Name + "\" could not import values because it is of the type \"" +
                                targetType + "\" and the requested import value is of the type \"" +
                                newTarget.GetType() + "\".  Please check the types of the many-to-one link.");
                        }
                    }
                    else
                    {
                        ConstructorInfo ci = targetType.GetConstructor(Type.EmptyTypes);
                        object newTarget = ci.Invoke(null);

                        foreach (object element in targetList)
                        {
                            (newTarget as IList).Add(element);
                        }

                        if (proc.Adapter.CanReceivePropertyValue(newTarget, prop.Name))
                        {
                            proc.Adapter.ReceivePropertyValue(newTarget, prop.Name);
                        }
                        else
                        {
                            //Panic!
                            throw new Exception("Type mismatch exception : Property \"" + prop.Name + "\" in the process \""
                                + proc.Name + "\" could not import values because it is of the type \"" +
                                targetType + "\" and the requested import value is of the type \"" +
                                newTarget.GetType() + "\".  Please check the types of the many-to-one link.");
                        }
                    }
                }
            }
        }

        public void PrepareForExecution()
        {
            //Generate the processing stack from the bare routine
            this.GenerateRoutineProcessingStack();

            //Send notification that adapters are initialized.  This gives the runtime an opportunity to pass extra data to the adapters as necessary.
            if (this.AdaptersInitialized != null)
            {
                FlowControlAdapterInitArgs args = new FlowControlAdapterInitArgs();
                foreach (Process proc in this.ProcessList)
                {
                    AdapterInitPair aip = new AdapterInitPair();
                    aip.HostProcessName = proc.Name;
                    aip.Adapter = proc.Adapter;

                    args.InitializedAdapters.Add(aip);
                }

                this.AdaptersInitialized(this, args);
            }

            foreach (Process proc in this.ProcessList)
            {
                proc.ReloadValuesOnlyFromAdapter();
            }
        }

        public List<RoutineBatchOutputPoint> RunUIExecutionLevel(int executionLevel, out int processesCompleted)
        {
            List<RoutineProcessingException.ProcessExceptionArgs> exceptions = new List<RoutineProcessingException.ProcessExceptionArgs>();


            List<RoutineBatchOutputPoint> ret = new List<RoutineBatchOutputPoint>();
            processesCompleted = 0;

            if (processingStack.Count <= executionLevel) return null;

            foreach (Process proc in processingStack[executionLevel])
            {
                if (proc.Adapter.HasUserInteraction)
                    processesCompleted++;
            }

            foreach (Process proc in processingStack[executionLevel])
            {

                if (!(proc.Adapter.HasUserInteraction)) continue;

                try
                {

                    //If Proc is not contained inside a permProc
                    if (!(this.IsProcessWithinAPermutation(proc)) || (proc is PermutationOutputProcess))
                        runAndForwardProcess(proc, true, ret, executionLevel);
                    else
                        runAndForwardProcess(proc, true, null, executionLevel);
                }
                catch (Exception ex)
                {
                    RoutineProcessingException.ProcessExceptionArgs exception = new RoutineProcessingException.ProcessExceptionArgs() { Exception = ex, Process = proc };
                    exceptions.Add(exception);
                }
            }

            foreach (Process proc in storageProcessList)
            {
                try
                {

                    if (proc.Adapter.HasUserInteraction)
                        runAndForwardProcess(proc, false, null, executionLevel);

                }
                catch (Exception ex)
                {
                    RoutineProcessingException.ProcessExceptionArgs exception = new RoutineProcessingException.ProcessExceptionArgs() { Exception = ex, Process = proc };
                    exceptions.Add(exception);
                }
            }

            if (exceptions.Count != 0)
            {
                throw new RoutineProcessingException(exceptions.ToArray());
            }

            return ret;
        }

        public List<RoutineBatchOutputPoint> RunNonUIExecutionLevel(int executionLevel, out int processesCompleted)
        {
            List<RoutineProcessingException.ProcessExceptionArgs> exceptions = new List<RoutineProcessingException.ProcessExceptionArgs>();

            List<RoutineBatchOutputPoint> ret = new List<RoutineBatchOutputPoint>();
            processesCompleted = 0;

            if (processingStack.Count <= executionLevel) return null;

            foreach (Process proc in processingStack[executionLevel])
            {
                if (!proc.Adapter.HasUserInteraction)
                    processesCompleted++;
            }

            foreach (Process proc in processingStack[executionLevel])
            {
                if (proc.Adapter.HasUserInteraction) continue;


                try
                {
                    //If Proc is not contained inside a permProc
                    if ((!(this.IsProcessWithinAPermutation(proc)) || (proc is PermutationOutputProcess)) && proc.OutputToPanel)
                        runAndForwardProcess(proc, true, ret, executionLevel);
                    else
                        runAndForwardProcess(proc, true, null, executionLevel);
                }
                catch(Exception ex)
                {
                    RoutineProcessingException.ProcessExceptionArgs exception = new RoutineProcessingException.ProcessExceptionArgs() { Exception = ex, Process = proc };
                    exceptions.Add(exception);
                }
            }

            foreach (Process proc in storageProcessList)
            {
                try
                {

                    if (!proc.Adapter.HasUserInteraction)
                        runAndForwardProcess(proc, false, null, executionLevel);
                }
                catch (Exception ex)
                {
                    RoutineProcessingException.ProcessExceptionArgs exception = new RoutineProcessingException.ProcessExceptionArgs() { Exception = ex, Process = proc };
                    exceptions.Add(exception);
                }
            }

            if (exceptions.Count != 0)
            {
                throw new RoutineProcessingException(exceptions.ToArray());
            }

            return ret;
        }

        public bool TargetAlreadyHasALink(Process targetProcess, string targetProperty)
        {
            if (!(ProcessList.Contains(targetProcess))) return false;

            if (PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty) == null) return false;

            if (PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty).Value is PropertyLink) return true;
            if (PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty).Value is PropertyMultiLink) return true;
            return false;
        }

        public bool LinkExists(Process sourceProcess, string sourceProperty, Process targetProcess, string targetProperty)
        {
            if (!(ProcessList.Contains(sourceProcess) && ProcessList.Contains(targetProcess)))
                return false;

            if (PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty) == null) return false;

            if (PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty).Value is PropertyLink)
            {
                PropertyLink thisLink = PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty).Value as PropertyLink;

                if ((thisLink.HostProcess == sourceProcess) && (thisLink.PropertyName == sourceProperty))
                    return true;
            }
            else if (PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty).Value is PropertyMultiLink)
            {
                PropertyMultiLink thisLink = PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty).Value as PropertyMultiLink;

                if (thisLink.ContainsLinkToProperty(sourceProcess, sourceProperty)) return true;
            }

            return false;
        }

        public PropertyLink CreateLink(Process sourceProcess, string sourceProperty, Process targetProcess, string targetProperty)
        {
            if (!(ProcessList.Contains(sourceProcess) && ProcessList.Contains(targetProcess)))
                return null;

            if (PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty) == null) return null;

            PropertyLink thisLink = new PropertyLink(sourceProcess, sourceProperty);

            PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty).Value = thisLink;

            return thisLink;
        }

        public PropertyLink AddToLink(Process sourceProcess, string sourceProperty, Process targetProcess, string targetProperty)
        {
            PropertyListing targetListing = PropertyListing.FindFirstByName(targetProcess.PropertiesList, targetProperty);
            PropertyListing sourceListing = PropertyListing.FindFirstByName(sourceProcess.PropertiesList, sourceProperty);

            if (!(ProcessList.Contains(sourceProcess) && ProcessList.Contains(targetProcess))) return null;

            if (targetListing == null || sourceListing == null) return null;

            if (!(targetListing.Value is PropertyMultiLink))
            {
                //if it's a link, convert it.  If it's not a link, it has to be made a link first, so do nothing.
                //Review this behavior, I think it's valid
                if (!(targetListing.Value is PropertyLink)) return null;

                PropertyLink temp = targetListing.Value as PropertyLink;
                PropertyLink existingLink = new PropertyLink(temp.HostProcess, temp.PropertyName);

                PropertyMultiLink newLink = new PropertyMultiLink();
                newLink.PropertyLinks.Add(existingLink);
                targetListing.Value = newLink;                
            }

            PropertyMultiLink thisLink = targetListing.Value as PropertyMultiLink;
            PropertyLink individualLink = new PropertyLink(sourceProcess, sourceProperty);

            thisLink.PropertyLinks.Add(individualLink);

            return individualLink;
        }

        /// <summary>
        /// Deletes the link at the given process/property pair.  It replaces it with the base (default) value from the adapter.
        /// </summary>
        /// <param name="delProcess">Process to delete the link from.</param>
        /// <param name="delProperty">Property to delete the link from.</param>
        public void DeleteLink(Process delProcess, string delProperty)
        {
            if (!(ProcessList.Contains(delProcess)))
                return;

            PropertyListing listing = PropertyListing.FindFirstByName(delProcess.PropertiesList, delProperty);

            if (listing == null) return;

            //Only "delete" links.  This method is not to function as a "restore to default" hack.
            if (!(listing.Value is PropertyLink || listing.Value is PropertyMultiLink))
                return;

            listing.Value = delProcess.Adapter.SupplyPropertyValue(delProperty);
        }

        /// <summary>
        /// Finds and deletes ALL LINKS pointing to a given process/property pair.  It replaces deleted links with the base (default) value from the adapter.
        /// </summary>
        /// <param name="delProcess">Process to delete links to.</param>
        /// <param name="delProperty">Property to delete links to.</param>
        public void DeleteLinksTo(Process delProcess, string delProperty)
        {
            if (!(ProcessList.Contains(delProcess)))
                return;

            PropertyListing listing = PropertyListing.FindFirstByName(delProcess.PropertiesList, delProperty);

            if (listing == null) return;

            List<PropertyLink> deleteList = new List<PropertyLink>();

            foreach (Process proc in ProcessList)
            {
                foreach (PropertyListing property in proc.PropertiesList)
                {
                    if (!(property.Value is PropertyLink)) continue;

                    Process host = (property.Value as PropertyLink).HostProcess;
                    string hostProperty = (property.Value as PropertyLink).PropertyName;

                    if (host == delProcess && hostProperty == delProperty)
                        deleteList.Add(new PropertyLink(proc, property.Name));                    
                }
            }

            foreach (PropertyLink delTarget in deleteList)
            {
                PropertyListing.FindFirstByName(delTarget.HostProcess.PropertiesList, delTarget.PropertyName).Value =
                    delTarget.HostProcess.Adapter.SupplyPropertyValue(delTarget.PropertyName);
            }
        }

        //Delete ALL links to ALL properties on the process.
        public void DeleteLinksTo(Process delProcess)
        {
            if (!(ProcessList.Contains(delProcess)))
                return;

            List<PropertyLink> deleteList = new List<PropertyLink>();

            foreach (Process proc in ProcessList)
            {
                foreach (PropertyListing property in proc.PropertiesList)
                {
                    if (!(property.Value is PropertyLink)) continue;

                    Process host = (property.Value as PropertyLink).HostProcess;
                    
                    if (host == delProcess)
                        deleteList.Add(new PropertyLink(proc, property.Name));
                }
            }

            foreach (PropertyLink delTarget in deleteList)
            {
                PropertyListing.FindFirstByName(delTarget.HostProcess.PropertiesList, delTarget.PropertyName).Value =
                    delTarget.HostProcess.Adapter.SupplyPropertyValue(delTarget.PropertyName);
            }
        }

        public void DeleteLinksFrom(Process delProcess)
        {
            List<PropertyLink> deleteList = new List<PropertyLink>();

            foreach (PropertyListing property in delProcess.PropertiesList)
            {
                if (!(property.Value is PropertyLink)) continue;

                deleteList.Add(new PropertyLink(delProcess, property.Name));
            }

            foreach (PropertyLink delTarget in deleteList)
            {
                PropertyListing.FindFirstByName(delTarget.HostProcess.PropertiesList, delTarget.PropertyName).Value =
                    delTarget.HostProcess.Adapter.SupplyPropertyValue(delTarget.PropertyName);
            }
        }

        public void DeleteProcess(Process delProcess)
        {
            foreach (PropertyListing property in delProcess.PropertiesList)
            {
                DeleteLinksTo(delProcess, property.Name);
            }

            this.ProcessList.Remove(delProcess);

            //Scan for the process' presence in the listings of a permutationProc
            foreach (Process proc in this.ProcessList)
            {
                if (proc is PermutationProcess)
                {
                    PermutationProcess pproc = proc as PermutationProcess;
                    if (delProcess is PermutationOutputProcess && (pproc.PermutationOutputProcesses.Contains(delProcess as PermutationOutputProcess)))
                        pproc.PermutationOutputProcesses.Remove(delProcess as PermutationOutputProcess);
                }
            }

        }

        void  Adapter_RunComplete(object sender, FlowControlProcessEventArgs e)
        {
 	        
        }

        public Routine()
        {
            processList.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(processList_CollectionChanged);
        }

        void processList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add || 
                e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Replace)
            {
                foreach (Process proc in e.NewItems)
                {
                    if(proc.Adapter != null)
                        proc.Adapter.ID = idIterator;
                    idIterator++;
                }
            }
        }
    }

    public class RoutineSerializer
    {
        public List<ProcessSerializer> ProcessList { get; set; }

        static public RoutineSerializer Serialize(Routine toSerialize)
        {
            RoutineSerializer ret = new RoutineSerializer();

            ret.ProcessList = new List<ProcessSerializer>();
            foreach (Process proc in toSerialize.ProcessList)
            {
                ret.ProcessList.Add(ProcessSerializer.Serialize(proc, toSerialize));
            }

            return ret;
        }

        static public Routine Deserialize(RoutineSerializer toDeserialize)
        {
            Routine ret = new Routine();

            ret.ProcessList = new ObservableCollection<Process>();
            //Two passes, first to set the process ordering (for property links), second to fill the processes.
            foreach (ProcessSerializer proc in toDeserialize.ProcessList)
            {
                if (proc.SavedSubtype == "CeresBase.FlowControl.PermutationProcess")
                {
                    ret.ProcessList.Add(new PermutationProcess());
                }
                else if (proc.SavedSubtype == "CeresBase.FlowControl.PermutationOutputProcess")
                {
                    ret.ProcessList.Add(new PermutationOutputProcess());
                }
                else
                {
                    ret.ProcessList.Add(new Process());
                }
            }

            for (int i = 0; i < toDeserialize.ProcessList.Count; i++)
            {
                //We have to copy the process over directly rather than just copying over, to preserve the pointers being used by the
                //propertylinks as they're deserialized.
                Process newValues = ProcessSerializer.Deserialize(toDeserialize.ProcessList[i], ret);

                ret.ProcessList[i].Adapter = newValues.Adapter;
                ret.ProcessList[i].Name = newValues.Name;
                ret.ProcessList[i].PropertiesList.Clear();
                ret.ProcessList[i].OutputToPanel = newValues.OutputToPanel;
                foreach (PropertyListing prop in newValues.PropertiesList)
                {
                    ret.ProcessList[i].PropertiesList.Add(prop);
                }

                if (newValues is PermutationProcess)
                {
                    PermutationProcess pproc = ret.ProcessList[i] as PermutationProcess;
                    PermutationProcess newproc = newValues as PermutationProcess;

                    pproc.IsAnExpansionProcess = newproc.IsAnExpansionProcess;
                    //We can assume that the PermutationMemberProcesses list will only have a single list in it initially, because the other
                    //lists come during expansion
                    foreach (Process thisProc in newproc.PermutationMemberProcesses[0])
                    {
                        pproc.PermutationMemberProcesses[0].Add(thisProc);
                    }

                    foreach (PermutationOutputProcess outputProc in newproc.PermutationOutputProcesses)
                    {
                        pproc.PermutationOutputProcesses.Add(outputProc);
                    }

                    foreach (PermutationProcess permProc in newproc.MirroredSelves)
                    {
                        pproc.MirroredSelves.Add(permProc);
                    }
                }
            }

            return ret;
        }
    }
}
