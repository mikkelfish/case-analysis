﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using CeresBase.FlowControl.Adapters;

namespace CeresBase.FlowControl
{
    [AdapterDescription("Common")]
    public class PermutationTestAdapter : IPermutationAdapter
    {
        private int baseValue = 0;
        private int stepValue = 0;
        private int iterationValue = 0;
        private int permutationOutputValue = 0;
        private int countValue = 0;

        #region IFlowControlPermutationAdapter Members

        public int IterationCount
        {
            get { return countValue; }
        }

        public void SetIteration(int iteration)
        {
            iterationValue = iteration;
        }

        #endregion

        #region IFlowControlAdapter Members

        private int id;
        public int ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        private Process hostProcess;
        public Process HostProcess
        {
            get
            {
                return hostProcess;
            }
            set
            {
                hostProcess = value;
            }
        }

        public string Name
        {
            get
            {
                return "Test Permutation Adapter";
            }
            set
            {
                
            }
        }

        public string Category
        {
            get
            {
                return "Runtime Tools";
            }
            set
            {
                
            }
        }

        public bool HasUserInteraction
        {
            get { return false; }
        }

        public event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public string[] SupplyPropertyNames()
        {            
            List<string> ret = new List<string>();
            ret.Add("BaseValue");
            ret.Add("StepValue");
            ret.Add("PermutationOutputValue");
            ret.Add("CountValue");

            return ret.ToArray();
        }

        public object SupplyPropertyValue(string sourcePropertyName)
        {
            switch (sourcePropertyName)
            {
                case "BaseValue":
                    return baseValue;
                    break;
                case "StepValue":
                    return stepValue;
                    break;
                case "PermutationOutputValue":
                    return permutationOutputValue;
                    break;
                case "CountValue":
                    return countValue;
                    break;
                default:
                    return null;
                    break;
            }

            return null;
        }

        //private double baseValue = 0.0;
        //private double stepValue = 0.0;
        //private double permutationOutputValue = 0.0;
        //private int countValue = 0;
        //private int iterationValue = 0;

        public void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            switch (targetPropertyName)
            {
                case "BaseValue":
                    baseValue = (int)propertyValue;
                    break;
                case "StepValue":
                    stepValue = (int)propertyValue;
                    break;
                case "PermutationOutputValue":
                    permutationOutputValue = (int)propertyValue;
                    break;
                case "CountValue":
                    countValue = (int)propertyValue;
                    break;
                default:
                    break;
            }
        }

        public bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            return true;
        }

        public FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
        {
            throw new NotImplementedException();
        }

        public FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
        {
            throw new NotImplementedException();
        }

        public object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
        {
            throw new NotImplementedException();
        }

        public void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
        {
            throw new NotImplementedException();
        }

        public Type PropertyType(string targetPropertyName)
        {
            switch (targetPropertyName)
            {
                case "BaseValue":
                    return typeof(int);
                    break;
                case "StepValue":
                    return typeof(int);
                    break;
                case "PermutationOutputValue":
                    return typeof(int);
                    break;
                case "CountValue":
                    return typeof(int);
                    break;
                default:
                    return typeof(object);
                    break;
            }

            return typeof(object);
        }

        public void RunAdapter()
        {
            this.permutationOutputValue = baseValue + (stepValue * iterationValue);
        }

        public object[] GetValuesForSerialization()
        {
            return null;
        }

        public void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public bool CanEditProperty(string targetPropertyName)
        {
            switch (targetPropertyName)
            {
                case "BaseValue":
                    return true;
                    break;
                case "StepValue":
                    return true;
                    break;
                case "PermutationOutputValue":
                    return false;
                    break;
                case "CountValue":
                    return true;
                    break;
                default:
                    return false;
                    break;
            }

            return false;

        }

        public bool CanReceivePropertyLinks(string targetPropertyname)
        {
            return false;
        }

        public bool CanSendPropertyLinks(string targetPropertyname)
        {
            return true;
        }

        public bool IsOutputProperty(string targetPropertyname)
        {
            if (targetPropertyname == "PermutationOutputValue")
                return true;
            return false;
        }

        public bool ShouldSerialize(string targetPropertyname)
        {
            return true;
        }

        public ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            return new ValidationStatus(ValidationState.Pass);
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return new PermutationTestAdapter();
        }

        #endregion

        public object SupplyPropertyDescription(string propertyName)
        {
            switch (propertyName)
            {
                case "BaseValue":
                    return "The initial value that the iteration will start with.";
                    break;
                case "StepValue":
                    return "The amount that each iteration will increase by.";
                    break;
                case "PermutationOutputValue":
                    return "The value for a given iteration - link this to forward the value to a given iteration of another process.";
                    break;
                case "CountValue":
                    return "The number of iterations in the permutation.";
                    break;
                default:
                    return null;
                    break;
            }

            return null;
        }

        public object SupplyAdapterDescription()
        {
            return "A basic test permutation adapter that can be used to give each iteration a different\n" + 
                   "value, starting at a base value and stepping up by the step value for each iteration.\n" + 
                   "This permutation iterates a number of times determined by the CountValue property.";
        }
    }
}
//    public class TestAdapter : IFlowControlAdapter
//    {
//        #region IFlowControlAdapter Members

//        private int id;

//        public int ID
//        {
//            get
//            {
//                return this.id;
//            }
//            set
//            {
//                this.id = value;
//            }
//        }

//        private int firstNumber = 0;
//        private int secondNumber = 1;
//        private int numberSums = 0;

//        public event EventHandler<FlowControlProcessEventArgs> RunComplete;

//        public string[] SupplyPropertyNames()
//        {
//            List<string> ret = new List<string>();
//            ret.Add("FirstNumber");
//            ret.Add("SecondNumber");
//            ret.Add("NumberSums");

//            return ret.ToArray();
//        }

//        public object SupplyPropertyValue(string sourcePropertyName)
//        {
//            switch (sourcePropertyName)
//            {
//                case "FirstNumber" :
//                    return this.firstNumber;
//                case "SecondNumber" :
//                    return this.secondNumber;
//                case "NumberSums" :
//                    return this.numberSums;
//                default:
//                    break;
//            }

//            return null;
//        }

//        public void ReceivePropertyValue(object propertyValue, string targetPropertyName)
//        {
//            switch (targetPropertyName)
//            {
//                case "FirstNumber" :
//                    this.firstNumber = (int)propertyValue;
//                    break;
//                case "SecondNumber" :
//                    this.secondNumber = (int)propertyValue;
//                    break;
//                case "NumberSums" :
//                    this.numberSums = (int)propertyValue;
//                    break;
//                default:
//                    break;
//            }
//        }

//        public bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
//        {
//            if (propertyValue is int)
//                return true;
//            return false;
//        }

//        public FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
//        {
//            throw new NotImplementedException();
//        }

//        public void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public Type PropertyType(string targetPropertyName)
//        {
//            return SupplyPropertyValue(targetPropertyName).GetType();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public object[] GetValuesForSerialization()
//        {
//            return null;
//        }

//        public void LoadValuesFromSerialization(object[] values)
//        {
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public event EventHandler<FlowControlAdapterInitArgs> AdapterInitializing;

//        public object RunAdapter()
//        {
//            this.numberSums = this.firstNumber + this.secondNumber;
//            return this.numberSums;
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public bool CanEditProperty(string targetPropertyname)
//        {
//            return true;
//        }

//        #endregion

//        #region ICloneable Members

//        public object Clone()
//        {
//            TestAdapter ta = new TestAdapter();
//            ta.name = this.name;
//            ta.firstNumber = this.firstNumber;
//            ta.secondNumber = this.secondNumber;
//            ta.numberSums = this.numberSums;
//            ta.id = this.id;

//            return ta;
//        }

//        #endregion

//        #region IFlowControlAdapter Members

//        private string name = "TestAdapter";
//        public string Name
//        {
//            get
//            {
//                return name;
//            }
//            set
//            {
//                name = value;
//            }
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public bool CanReceivePropertyLinks(string targetPropertyname)
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public string Category
//        {
//            get
//            {
//                return "Tests";
//            }
//            set
//            {
                
//            }
//        }

//        #endregion

//        private Process hostProcess;
//        public Process HostProcess
//        {
//            get
//            {
//                return hostProcess;
//            }
//            set
//            {
//                hostProcess = value;
//            }
//        }
//    }

//    public class TestMultAdapter : IFlowControlAdapter
//    {

//        #region IFlowControlAdapter Members

//        private int id;
//        public int ID
//        {
//            get
//            {
//                return id;
//            }
//            set
//            {
//                id = value;
//            }
//        }

//        public event EventHandler<FlowControlProcessEventArgs> RunComplete;

//        private int firstNumber = 0;
//        private int secondNumber = 0;
//        private int numberProduct = 0;

//        public string[] SupplyPropertyNames()
//        {
//            List<string> ret = new List<string>();
//            ret.Add("FirstNumber");
//            ret.Add("SecondNumber");
//            ret.Add("NumberProduct");

//            return ret.ToArray();
//        }

//        public object SupplyPropertyValue(string sourcePropertyName)
//        {
//            switch (sourcePropertyName)
//            {
//                case "FirstNumber":
//                    return this.firstNumber;
//                case "SecondNumber":
//                    return this.secondNumber;
//                case "NumberProduct":
//                    return this.numberProduct;
//                default:
//                    break;
//            }

//            return null;
//        }

//        public void ReceivePropertyValue(object propertyValue, string targetPropertyName)
//        {
//            switch (targetPropertyName)
//            {
//                case "FirstNumber":
//                    this.firstNumber = (int)propertyValue;
//                    break;
//                case "SecondNumber":
//                    this.secondNumber = (int)propertyValue;
//                    break;
//                case "NumberSums":
//                    this.numberProduct = (int)propertyValue;
//                    break;
//                default:
//                    break;
//            }
//        }

//        public bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
//        {
//            if (propertyValue is int)
//                return true;
//            return false;
//        }

//        public FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
//        {
//            throw new NotImplementedException();
//        }

//        public void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
//        {
//            throw new NotImplementedException();
//        }

//        public object RunAdapter(object[] args)
//        {
//            this.numberProduct = this.firstNumber * this.secondNumber;
//            return this.numberProduct;
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public Type PropertyType(string targetPropertyName)
//        {
//            switch (targetPropertyName)
//            {
//                case "FirstNumber":
//                    return this.firstNumber.GetType();
//                case "SecondNumber":
//                    return this.secondNumber.GetType();
//                case "NumberProduct":
//                    return this.numberProduct.GetType();
//                default:
//                    break;
//            }

//            return null;
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public object[] GetValuesForSerialization()
//        {
//            throw new NotImplementedException();
//        }

//        public void LoadValuesFromSerialization(object[] values)
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public event EventHandler<FlowControlAdapterInitArgs> AdapterInitializing;

//        public object RunAdapter()
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public bool CanEditProperty(string targetPropertyname)
//        {
//            return true;
//        }

//        #endregion

//        #region ICloneable Members

//        public object Clone()
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public string Name
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//            set
//            {
//                throw new NotImplementedException();
//            }
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public string Category
//        {
//            get
//            {
//                return "Tests";
//            }
//            set
//            {
                
//            }
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public bool CanReceivePropertyLinks(string targetPropertyname)
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        private Process hostProcess;
//        public Process HostProcess
//        {
//            get
//            {
//                return hostProcess;
//            }
//            set
//            {
//                hostProcess = value;
//            }
//        }
//    }

//    public class TestMeanAdapter : IFlowControlAdapter
//    {

//        #region IFlowControlAdapter Members

//        private int id;
//        public int ID
//        {
//            get
//            {
//                return id;
//            }
//            set
//            {
//                id = value;
//            }
//        }

//        public event EventHandler<FlowControlProcessEventArgs> RunComplete;


//        private int number1 = 0;
//        private int number2 = 0;
//        private int number3 = 0;
//        private int number4 = 0;
//        private int number5 = 0;
//        private int number6 = 0;
//        private int number7 = 0;
//        private int number8 = 0;
//        private int number9 = 0;
//        private int number10 = 0;
//        private int number11 = 0;
//        private double mean = 0.0;

//        public string[] SupplyPropertyNames()
//        {
//            List<string> ret = new List<string>();

//            ret.Add("Number1");
//            ret.Add("Number2");
//            ret.Add("Number3");
//            ret.Add("Number4");
//            ret.Add("Number5");
//            ret.Add("Number6");
//            ret.Add("Number7");
//            ret.Add("Number8");
//            ret.Add("Number9");
//            ret.Add("Number10");
//            ret.Add("Number11");
//            ret.Add("Mean");

//            return ret.ToArray();
//        }

//        public object SupplyPropertyValue(string sourcePropertyName)
//        {
//            switch (sourcePropertyName)
//            {
//                case "Number1":
//                    return this.number1;
//                case "Number2":
//                    return this.number2;
//                case "Number3":
//                    return this.number3;
//                case "Number4":
//                    return this.number4;
//                case "Number5":
//                    return this.number5;
//                case "Number6":
//                    return this.number6;
//                case "Number7":
//                    return this.number7;
//                case "Number8":
//                    return this.number8;
//                case "Number9":
//                    return this.number9;
//                case "Number10":
//                    return this.number10;
//                case "Number11":
//                    return this.number11;
//                case "Mean":
//                    return this.mean;
//                default:
//                    break;
//            }

//            return null;
//        }

//        public void ReceivePropertyValue(object propertyValue, string targetPropertyName)
//        {
//            switch (targetPropertyName)
//            {
//                case "Number1":
//                    this.number1 = (int)propertyValue;
//                    break;
//                case "Number2":
//                    this.number2 = (int)propertyValue;
//                    break;
//                case "Number3":
//                    this.number3 = (int)propertyValue;
//                    break;
//                case "Number4":
//                    this.number4 = (int)propertyValue;
//                    break;
//                case "Number5":
//                    this.number5 = (int)propertyValue;
//                    break;
//                case "Number6":
//                    this.number6 = (int)propertyValue;
//                    break;
//                case "Number7":
//                    this.number7 = (int)propertyValue;
//                    break;
//                case "Number8":
//                    this.number8 = (int)propertyValue;
//                    break;
//                case "Number9":
//                    this.number9 = (int)propertyValue;
//                    break;
//                case "Number10":
//                    this.number10 = (int)propertyValue;
//                    break;
//                case "Number11":
//                    this.number11 = (int)propertyValue;
//                    break;
//                case "Mean":
//                    this.mean = (int)propertyValue;
//                    break;
//                default:
//                    break;
//            }

//            return;
//        }

//        public bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
//        {
//            if (propertyValue is int)
//                return true;
//            return false;
//        }

//        public FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
//        {
//            throw new NotImplementedException();
//        }

//        public void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
//        {
//            throw new NotImplementedException();
//        }

//        public object RunAdapter(object[] args)
//        {
//            int sum = (number1 + number2 + number3 + number4 + number5 + number6 + number7 + number8 + number9 + number10 + number11);
//            int count = 0;
//            if (number1 != 0) count++;
//            if (number2 != 0) count++;
//            if (number3 != 0) count++;
//            if (number4 != 0) count++;
//            if (number5 != 0) count++;
//            if (number6 != 0) count++;
//            if (number7 != 0) count++;
//            if (number8 != 0) count++;
//            if (number9 != 0) count++;
//            if (number10 != 0) count++;
//            if (number11 != 0) count++;

//            mean = (double)sum / (double)count;

//            return mean;
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public Type PropertyType(string targetPropertyName)
//        {
//            switch (targetPropertyName)
//            {
//                case "Number1":
//                    return this.number1.GetType();
//                case "Number2":
//                    return this.number2.GetType();
//                case "Number3":
//                    return this.number3.GetType();
//                case "Number4":
//                    return this.number4.GetType();
//                case "Number5":
//                    return this.number5.GetType();
//                case "Number6":
//                    return this.number6.GetType();
//                case "Number7":
//                    return this.number7.GetType();
//                case "Number8":
//                    return this.number8.GetType();
//                case "Number9":
//                    return this.number9.GetType();
//                case "Number10":
//                    return this.number10.GetType();
//                case "Number11":
//                    return this.number11.GetType();
//                case "Mean":
//                    return this.mean.GetType();
//                default:
//                    break;
//            }

//            return null;
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public object[] GetValuesForSerialization()
//        {
//            throw new NotImplementedException();
//        }

//        public void LoadValuesFromSerialization(object[] values)
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public event EventHandler<FlowControlAdapterInitArgs> AdapterInitializing;

//        public object RunAdapter()
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public bool CanEditProperty(string targetPropertyname)
//        {
//            return true;
//        }

//        #endregion

//        #region ICloneable Members

//        public object Clone()
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public string Name
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//            set
//            {
//                throw new NotImplementedException();
//            }
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public bool CanReceivePropertyLinks(string targetPropertyname)
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public string Category
//        {
//            get
//            {
//                return "Tests";
//            }
//            set
//            {
                
//            }
//        }

//        #endregion

//        private Process hostProcess;
//        public Process HostProcess
//        {
//            get
//            {
//                return hostProcess;
//            }
//            set
//            {
//                hostProcess = value;
//            }
//        }
//    }


//    public class TestCollectionMeanAdapter : IFlowControlAdapter
//    {

//        #region IFlowControlAdapter Members

//        private int id;
//        public int ID
//        {
//            get
//            {
//                return id;
//            }
//            set
//            {
//                id = value;
//            }
//        }

//        public event EventHandler<FlowControlProcessEventArgs> RunComplete;

//        private int[] array = (int[])CeresBase.Data.DataUtilities.CreateArray(0,typeof(int));
//        private List<int> list = new List<int>();
//        private double mean = 0.0;

//        public string[] SupplyPropertyNames()
//        {
//            List<string> ret = new List<string>();

//            ret.Add("Array");
//            ret.Add("List");
//            ret.Add("Mean");

//            return ret.ToArray();
//        }

//        public object SupplyPropertyValue(string sourcePropertyName)
//        {
//            switch (sourcePropertyName)
//            {
//                case "Array":
//                    return this.array;
//                case "List":
//                    return this.list;
//                case "Mean":
//                    return this.mean;
//                default:
//                    break;
//            }

//            return null;
//        }

//        public void ReceivePropertyValue(object propertyValue, string targetPropertyName)
//        {
//            switch (targetPropertyName)
//            {
//                case "Array":
//                    this.array = (int[])propertyValue;
//                    break;
//                case "List":
//                    this.list = (List<int>)propertyValue;
//                    break;
//                case "Mean":
//                    this.mean = (double)propertyValue;
//                    break;
//                default:
//                    break;
//            }

//            return;
//        }

//        public bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
//        {
//            Type targetType = PropertyType(targetPropertyName);
//            if (targetType.IsInstanceOfType(propertyValue)) return true;

//            return false;
//        }

//        public FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
//        {
//            throw new NotImplementedException();
//        }

//        public void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
//        {
//            throw new NotImplementedException();
//        }

//        public object RunAdapter(object[] args)
//        {
//            int count = 0;
//            int sum = 0;

//            for (int i = 0; i < array.Length; i++)
//            {
//                sum += array[i];
//                count++;
//            }

//            foreach (int val in list)
//            {
//                sum += val;
//                count++;
//            }

//            mean = (double)sum / (double)count;

//            return mean;
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public Type PropertyType(string targetPropertyName)
//        {
//            switch (targetPropertyName)
//            {
//                case "Array":
//                    return array.GetType();
//                case "List":
//                    return list.GetType();
//                case "Mean":
//                    return mean.GetType();
//                default:
//                    break;
//            }

//            return null;
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public object[] GetValuesForSerialization()
//        {
//            throw new NotImplementedException();
//        }

//        public void LoadValuesFromSerialization(object[] values)
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public event EventHandler<FlowControlAdapterInitArgs> AdapterInitializing;

//        public object RunAdapter()
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public bool CanEditProperty(string targetPropertyname)
//        {
//            return true;
//        }

//        #endregion

//        #region ICloneable Members

//        public object Clone()
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public string Name
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//            set
//            {
//                throw new NotImplementedException();
//            }
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public bool CanReceivePropertyLinks(string targetPropertyname)
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region IFlowControlAdapter Members


//        public string Category
//        {
//            get
//            {
//               return "Tests";
//            }
//            set
//            {
                
//            }
//        }

//        #endregion

//        private Process hostProcess;
//        public Process HostProcess
//        {
//            get
//            {
//                return hostProcess;
//            }
//            set
//            {
//                hostProcess = value;
//            }
//        }
//    }

//    public class TestStorageAdapter : StorageAdapter
//    {

//        public override string Category
//        {
//            get
//            {
//                return "Tests";
//            }
//            set
//            {
                
//            }
//        }

//        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

//        public override string[] SupplyPropertyNames()
//        {
//            return new List<string>().ToArray();
//        }

//        public override object SupplyPropertyValue(string sourcePropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public override bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public override FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public override FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public override object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
//        {
//            throw new NotImplementedException();
//        }

//        public override void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
//        {
//            throw new NotImplementedException();
//        }

//        public override Type PropertyType(string targetPropertyName)
//        {
//            throw new NotImplementedException();
//        }

//        public override object RunAdapter()
//        {
//            throw new NotImplementedException();
//        }

//        public override object[] GetValuesForSerialization()
//        {
//            throw new NotImplementedException();
//        }

//        public override void LoadValuesFromSerialization(object[] values)
//        {
//            throw new NotImplementedException();
//        }

//        public override bool CanEditProperty(string targetPropertyname)
//        {
//            throw new NotImplementedException();
//        }

//        public override bool CanReceivePropertyLinks(string targetPropertyname)
//        {
//            throw new NotImplementedException();
//        }

//        public override object Clone()
//        {
//            throw new NotImplementedException();
//        }

//        private Process hostProcess;
//        public Process HostProcess
//        {
//            get
//            {
//                return hostProcess;
//            }
//            set
//            {
//                hostProcess = value;
//            }
//        }
//    }
//}
