﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MesosoftCommon.Layout.BasicPanels;
using MesosoftCommon.ExtensionMethods;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Interaction logic for PermutationProcessControl.xaml
    /// </summary>
    public partial class PermutationProcessControl : UserControl, IHasAProcessSource
    {
        public void CorrectDragGripTarget()
        {
            MesoGrid.SetDragGripTarget(this.ProcessPanel.TopNameplate, this.ThisControl);
        }

        public PermutationProcessControl()
        {
            InitializeComponent();
        }

        #region IHasAProcessSource Members

        public Process Source
        {
            get
            {
                return this.ProcessPanel.Source;
            }
            set
            {
                this.ProcessPanel.Source = value;
            }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            //RoutineManager rm = FrameworkElements.FindVisualAncestor(this, typeof(RoutineManager)) as RoutineManager;

            //if (rm.DeleteProcessModeActive)
            //{
            //    rm.DeletePermutationProcess(e.Source as PermutationProcessControl);
            //    e.Handled = true;
            //    return;
            //}
            base.OnMouseDown(e);
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            //RoutineManager rm = FrameworkElements.FindVisualAncestor(this, typeof(RoutineManager)) as RoutineManager;

            //if (rm.DeleteProcessModeActive)
            //{
            //    rm.DeletePermutationProcess(e.Source as PermutationProcessControl);
            //    e.Handled = true;
            //    return;
            //}
            base.OnPreviewMouseDown(e);
        }

        #endregion
    }
}
