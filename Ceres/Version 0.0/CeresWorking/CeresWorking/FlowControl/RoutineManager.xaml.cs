﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MesosoftCommon.Layout.BasicPanels;
using MesosoftCommon.ExtensionMethods;
using MesosoftCommon.Utilities.Converters.ValueConverters;
using System.Reflection;
using MesosoftCommon.Utilities.Settings;
using Microsoft.Win32;
using System.IO;
using System.Collections.ObjectModel;
using CeresBase.Projects;
using MesosoftCommon.Managers;
using System.ComponentModel;
using CeresBase.UI;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Interaction logic for RoutineManager.xaml
    /// </summary>
    public partial class RoutineManager : Window, INotifyPropertyChanged
    {
        public Guid ID { get; set; }

        private bool IDisSet = false;
        public bool IDIsSet { get { return this.IDisSet; } set { this.IDisSet = value; } }

        private List<PropertyLinkVisual> propertyLinkVisuals = new List<PropertyLinkVisual>();
        public List<PropertyLinkVisual> PropertyLinkVisuals
        {
            get
            {
                return this.propertyLinkVisuals;
            }
            set
            {
                this.propertyLinkVisuals = value;
            }
        }

        //private string fallbackSerializationpath = Paths.DefaultPath.LocalPath + "Ceres";
        //private string serializationPath = Paths.DefaultPath.LocalPath + "Ceres\\Routines";

        private Routine managedRoutine = new Routine();
        public Routine ManagedRoutine 
        { 
            get
            {
                return managedRoutine;   
            }
            set
            {
                managedRoutine = value;
            }
        }

        private bool connectModeActive = false;
        public bool ConnectModeActive
        {
            get
            {
                return this.connectModeActive;
            }
        }

        private bool deleteLinkModeActive = false;
        public bool DeleteLinkModeActive
        {
            get
            {
                return this.deleteLinkModeActive;
            }
        }

        private bool deleteProcessModeActive = false;
        public bool DeleteProcessModeActive
        {
            get
            {
                return deleteProcessModeActive;
            }
            set
            {
                deleteProcessModeActive = value;
                OnPropertyChanged("DeleteProcessModeActive");
            }
        }

        private ProcessControl activeBeginProcess = null;
        private UIElement activeBeginElement = null;
        private Line activeLine = null;
        
        private ProcessControl selectedControl = null;
        public ProcessControl SelectedControl
        {
            get
            {
                return this.selectedControl;
            }
            set
            {
                if(this.selectedControl != null)
                    this.selectedControl.IsSelected = false;

                this.selectedControl = value;
                if(this.selectedControl != null)
                    this.selectedControl.IsSelected = true;
            }
        }

        private ObservableCollection<IFlowControlAdapter> availableAdapters = new ObservableCollection<IFlowControlAdapter>();
        public ObservableCollection<IFlowControlAdapter> AvailableAdapters
        {
            get
            {
                return availableAdapters;
            }
            set
            {
                availableAdapters = value;
            }
        }

        private List<Type> adapters = new List<Type>();
        public List<Type> Adapters
        {
            get
            {
                return adapters;
            }
            set
            {
                adapters = value;
            }
        }

        private TranslateTransform displayPanelTranslation = new TranslateTransform(0.0, 0.0);
        public TranslateTransform DisplayPanelTranslation
        {
            get
            {
                return this.displayPanelTranslation;
            }
            set
            {
                this.displayPanelTranslation = value;
                this.OnPropertyChanged("DisplayPanelTranslation");
            }
        }

        private Dictionary<ProcessControl, List<AttachedLine>> visualLines = new Dictionary<ProcessControl, List<AttachedLine>>();

        private List<BindingExpression> bindingUpdateList = new List<BindingExpression>();

        public RoutineManager()
        {
            InitializeComponent();

            DisplayPanel.DraggingStarted += new DraggingRoutedEventHandler(DisplayPanel_DraggingStarted);
            DisplayPanel.DraggingActive += new DraggingRoutedEventHandler(DisplayPanel_DraggingActive);
            DisplayPanel.DraggingEnded += new DraggingRoutedEventHandler(DisplayPanel_DraggingEnded);
            DisplayPanel.PreviewMouseRightButtonDown += new MouseButtonEventHandler(DisplayPanel_PreviewMouseRightButtonDown);
            ClipPanel.MouseLeftButtonDown += new MouseButtonEventHandler(ClipPanel_MouseLeftButtonDown);
            ClipPanel.MouseMove += new MouseEventHandler(ClipPanel_MouseMove);
            ClipPanel.MouseLeftButtonUp += new MouseButtonEventHandler(ClipPanel_MouseLeftButtonUp);
            //adapters.Add(typeof(TestAdapter));
            //adapters.Add(typeof(TestMultAdapter));
            //adapters.Add(typeof(TestMeanAdapter));
            //adapters.Add(typeof(TestCollectionMeanAdapter));
            adapters.Add(typeof(GraphPickerAdapter));
            //AvailableAdapters.Add(typeof(PermutationTestAdapter) as IFlowControlAdapter);
            //adapters.Add(typeof(TestStorageAdapter));

            this.displayPanelTranslation.X = (((this.DisplayPanel.Width / 2) - (this.ThisElement.Width - 200) / 2)) * -1;
            this.displayPanelTranslation.Y = (((this.DisplayPanel.Height / 2) - (this.ThisElement.Height) / 2)) * -1;
        }

        private Point initialPoint;
        private Point initialTransformCoordinates;

        void ClipPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!this.ClipPanel.IsMouseCaptured) return;
            this.ClipPanel.ReleaseMouseCapture();
            //throw new NotImplementedException();
        }

        void ClipPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (!this.ClipPanel.IsMouseCaptured) return;

            //throw new NotImplementedException();
            Point thisPoint = e.GetPosition(this.ClipPanel);

            if (thisPoint == this.initialPoint) return;
            
            double xDifference = thisPoint.X - this.initialPoint.X;
            double yDifference = thisPoint.Y - this.initialPoint.Y;

            double xOffset = this.initialTransformCoordinates.X + xDifference;
            double yOffset = this.initialTransformCoordinates.Y + yDifference;

            if (xOffset > 0) xOffset = 0;
            if (yOffset > 0) yOffset = 0;
            if (xOffset * -1 > this.DisplayPanel.ActualWidth - this.ClipPanel.ActualWidth) 
                xOffset = (this.DisplayPanel.ActualWidth - this.ClipPanel.ActualWidth) * -1;
            if (yOffset * -1 > this.DisplayPanel.ActualHeight - this.ClipPanel.ActualHeight)
                yOffset = (this.DisplayPanel.ActualHeight - this.ClipPanel.ActualHeight) * -1;

            this.DisplayPanelTranslation = new TranslateTransform(xOffset, yOffset);
            this.ThisElement.UpdateLayout();

            //this.updateLineVisuals();
        }

        void ClipPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.initialPoint = e.GetPosition(this.ClipPanel);
            this.initialTransformCoordinates = new Point(this.DisplayPanelTranslation.X, this.DisplayPanelTranslation.Y);
            this.ClipPanel.CaptureMouse();
            //throw new NotImplementedException();
        }

        void DisplayPanel_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.connectModeActive = false;
            this.activeLineCleanup();
            e.Handled = true;
        }

        void DisplayPanel_DraggingStarted(object sender, DraggingRoutedEventArgs e)
        {
            if (!e.IsADrag) return;

            if (e.ElementBeingDragged == null)
                return;

            SelectedControl = e.ElementBeingDragged as ProcessControl;

            //bindingUpdateList.Clear();

            //Find all the bindings on the lines
            //foreach (UIElement child in DisplayPanel.Children)
            //{
            //    if (!(child is Line)) continue;

            //    BindingExpression childX1 = BindingOperations.GetBindingExpression(child, Line.X1Property);
            //    BindingExpression childX2 = BindingOperations.GetBindingExpression(child, Line.X2Property);
            //    BindingExpression childY1 = BindingOperations.GetBindingExpression(child, Line.Y1Property);
            //    BindingExpression childY2 = BindingOperations.GetBindingExpression(child, Line.Y2Property);

            //    if (childX1 == null || childX2 == null || childY1 == null || childY2 == null) continue;

            //    if (e.ElementBeingDragged.IsAncestorOf(childX1.DataItem as DependencyObject)) bindingUpdateList.Add(childX1);
            //    if (e.ElementBeingDragged.IsAncestorOf(childX2.DataItem as DependencyObject)) bindingUpdateList.Add(childX2);
            //    if (e.ElementBeingDragged.IsAncestorOf(childY1.DataItem as DependencyObject)) bindingUpdateList.Add(childY1);
            //    if (e.ElementBeingDragged.IsAncestorOf(childY2.DataItem as DependencyObject)) bindingUpdateList.Add(childY2);
            //}
        }

        void DisplayPanel_DraggingActive(object sender, DraggingRoutedEventArgs e)
        {
            //foreach (BindingExpression binding in bindingUpdateList)
            //{
            //    binding.UpdateTarget();
            //}

            this.updateLineVisuals();
        }

        void DisplayPanel_DraggingEnded(object sender, DraggingRoutedEventArgs e)
        {
            if (!e.IsADrag) return;

            //Ensure the dragged object has a source, otherwise we do not care about it
            if (!e.ElementBeingDragged.GetType().GetInterfaces().Contains(typeof(IHasAProcessSource))) return;

            //foreach (BindingExpression binding in bindingUpdateList)
            //{
            //    //int i = 0;
            //    //_worker = {MS.Internal.Data.ClrBindingWorker}

            //    FieldInfo fi = binding.GetType().GetField("_worker", BindingFlags.Instance | BindingFlags.NonPublic);
            //    object obj = fi.GetValue(binding);
            //    PropertyInfo pi = obj.GetType().GetProperty("TargetElement", BindingFlags.Instance | BindingFlags.NonPublic);
            //    object targetLine = pi.GetValue(obj, null);

            //    DisplayPanel.BringToFrontOfActivePanel(targetLine as Line);

            //    ContentPresenter cp = FrameworkElements.FindVisualAncestor(e.Source as UIElement, typeof(ContentPresenter)) as ContentPresenter;
            //}


            UIElement targetPanel = null;

            //Check to see if the dragged element was originally in a permutation panel
            
            object dragElementParent = VisualTreeHelper.GetParent(e.ElementBeingDragged);
            Panel parentManagedPanel = MesoGrid.FindParentManagedPanel(dragElementParent as DependencyObject);
            PermutationProcessControl permProc = null;

            //Not stored at the top level.
            if (parentManagedPanel != this.DisplayPanel)
            {
                permProc = ((parentManagedPanel.Parent as FrameworkElement).Parent as FrameworkElement).Parent as PermutationProcessControl;
                dragElementParent = permProc.PermutationChildrenPanel;
            }
            
            Point targetPoint = new Point(e.XCoordinate, e.YCoordinate);
            targetPoint = Mouse.GetPosition(this.DisplayPanel);

            //Check to see if the mouse coordinate ended in the space of any permutation adapters, starting at the end of the list.
            for (int i = this.DisplayPanel.Children.Count - 1; i >= 0; i--)
            {
                if (!(this.DisplayPanel.Children[i] is PermutationProcessControl)) continue;

                UIElement uielem = this.DisplayPanel.Children[i];

                if (uielem == e.ElementBeingDragged) continue;

                Rect bounds = new Rect(new Point(MesoGrid.GetXCoordinate(uielem), MesoGrid.GetYCoordinate(uielem)), uielem.RenderSize);

                if ((targetPoint.X > bounds.Left) && (targetPoint.X < bounds.Right) && (targetPoint.Y > bounds.Top) && (targetPoint.Y < bounds.Bottom))
                {
                    targetPanel = uielem;
                    break;
                }
            }



            if (targetPanel != null)
            {
                //Since we're dragging into a permutationprocess, we now make sure it's typed correctly - output objects go in the output pane
                //and all others go in the main pane.
                if (this.isOverOutputPane(targetPanel))
                {
                    int i = 0;
                }

                if (dragElementParent == (targetPanel as PermutationProcessControl).PermutationChildrenPanel)
                {
                    return;
                }

                (dragElementParent as Panel).Children.Remove(e.ElementBeingDragged);
                (targetPanel as PermutationProcessControl).PermutationChildrenPanel.Children.Add(e.ElementBeingDragged);

                //Make sure the process link is added/removed appropriately to the PermutationProcess as well
                //the target is a perm panel so add it here :
                PermutationProcess targetPermProc = (targetPanel as PermutationProcessControl).ProcessPanel.Source as PermutationProcess;


                if ((e.ElementBeingDragged as IHasAProcessSource).Source.Adapter is IPermutationOutputAdapter)
                    targetPermProc.PermutationOutputProcesses.Add((e.ElementBeingDragged as IHasAProcessSource).Source as PermutationOutputProcess);
                else
                    targetPermProc.PermutationMemberProcesses[0].Add((e.ElementBeingDragged as IHasAProcessSource).Source as Process);
                Point newCoord = Mouse.GetPosition((targetPanel as PermutationProcessControl).PermutationChildrenPanel);
                Point localCoord = MesoGrid.OriginalCursorLocationOnElementBeingDragged;
                Point finalCoord = new Point(newCoord.X - localCoord.X, newCoord.Y - localCoord.Y);
                MesoGrid.SetXCoordinate(e.ElementBeingDragged, finalCoord.X);
                MesoGrid.SetYCoordinate(e.ElementBeingDragged, finalCoord.Y);
            }
            else
            {
                if (dragElementParent == this.DisplayPanel)
                {
                    return;
                }
                (dragElementParent as Panel).Children.Remove(e.ElementBeingDragged);
                this.DisplayPanel.Children.Add(e.ElementBeingDragged);
                MesoGrid.SetXCoordinate(e.ElementBeingDragged, targetPoint.X);
                MesoGrid.SetYCoordinate(e.ElementBeingDragged, targetPoint.Y);
            }

            if (permProc != null)
            {
                //It's originally from a perm panel, so remove that
                PermutationProcess sourcePermProc = permProc.ProcessPanel.Source as PermutationProcess;
                if ((e.ElementBeingDragged as IHasAProcessSource).Source.Adapter is IPermutationOutputAdapter)
                    sourcePermProc.PermutationOutputProcesses.Remove((e.ElementBeingDragged as IHasAProcessSource).Source as PermutationOutputProcess);
                else
                    sourcePermProc.PermutationMemberProcesses[0].Remove((e.ElementBeingDragged as IHasAProcessSource).Source as Process);
            }

            //Final cleanup behavior regardless of where it's ended up at this point (as long as it wasn't left in its parent panel)
            if (e.ElementBeingDragged is PermutationProcessControl)
            {
                removeLinesFromByProcess((e.ElementBeingDragged as PermutationProcessControl).ProcessPanel);
                removeLinesToByProcess((e.ElementBeingDragged as PermutationProcessControl).ProcessPanel);
            }
            else
            {
                removeLinesFromByProcess(e.ElementBeingDragged as ProcessControl);
                removeLinesToByProcess(e.ElementBeingDragged as ProcessControl);
                //Trip the set accessor to fire the update.  This is kind of filthy hax.
                (e.ElementBeingDragged as ProcessControl).OutputToPanel = (e.ElementBeingDragged as ProcessControl).OutputToPanel;
            }

            ManagedRoutine.DeleteLinksTo((e.ElementBeingDragged as IHasAProcessSource).Source);


            ManagedRoutine.DeleteLinksFrom((e.ElementBeingDragged as IHasAProcessSource).Source);

            foreach (Process proc in this.ManagedRoutine.ProcessList)
            {
                proc.PollValidationOnProperties();
            }
        }

        private bool isOverOutputPane(UIElement targetPanel)
        {
            foreach (UIElement elem in (targetPanel as PermutationProcessControl).PermutationChildrenPanel.Children)
            {
                if ((elem is FrameworkElement) && (elem as FrameworkElement).Name == "OutputPanel")
                    return elem.IsMouseOver;
            }

            return false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //for (int i = 0; i < 7; i++)
            //{
            //    TestAdapter adapter = new TestAdapter();
            //    Process proc = new Process(adapter);
            //    proc.Name = "TestProcess" + i.ToString();
            //    ManagedRoutine.ProcessList.Add(proc);

            //    ProcessControl procCon = new ProcessControl();
            //    procCon.Source = proc;
            //    DisplayPanel.Children.Add(procCon);

            //    MesoGrid.SetXCoordinate(procCon, (i/4 * 100));
            //    MesoGrid.SetYCoordinate(procCon, (i / 4 * 100));
            //}
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            //Maintain the "active line" that displays as you're connecting.
            if (connectModeActive)
            {
                activeLine.X2 = e.GetPosition(this.DisplayPanel).X;
                activeLine.Y2 = e.GetPosition(this.DisplayPanel).Y;
                DisplayPanel.UpdateLayout();
            }

            base.OnMouseMove(e);
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            ProcessControl pc;
            if (e.Source is ProcessControl)
                pc = e.Source as ProcessControl;
            else
                pc = FrameworkElements.FindVisualAncestor(e.Source as UIElement, typeof(ProcessControl)) as ProcessControl;
            SelectedControl = pc;
            e.Handled = false;
            base.OnMouseDown(e);
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            ProcessControl pc;
            if (e.Source is ProcessControl)
                pc = e.Source as ProcessControl;
            else
                pc = FrameworkElements.FindVisualAncestor(e.Source as UIElement, typeof(ProcessControl)) as ProcessControl;
            SelectedControl = pc;
            e.Handled = false;

            base.OnPreviewMouseDown(e);
        }

        public void BeginVisualConnectFrom(UIElement beginElement, ProcessControl beginProcess)
        {
            //If a connection mode is still active, cleanse it and stop processing (so the user knows that was a no-no)
            if (this.connectModeActive)
            {
                activeLineCleanup();
                this.connectModeActive = false;
                return;
            }


            Point basePoint = beginElement.PointToScreen
                (
                new Point(
                            (beginElement.RenderSize.Width / 2),
                            (beginElement.RenderSize.Height / 2)
                         )
                );
            
            Point localPoint = DisplayPanel.PointFromScreen(basePoint);

            activeLine = new Line();
            activeLine.X1 = localPoint.X;
            activeLine.Y1 = localPoint.Y;
            activeLine.X2 = activeLine.X1;
            activeLine.Y2 = activeLine.Y1;
            activeLine.Stroke = new SolidColorBrush(Colors.Black);
            activeLine.StrokeThickness = 1;
            this.DisplayPanel.Children.Add(activeLine);
            this.connectModeActive = true;

            MesoGrid.SetIsDraggable(activeLine, false);
            activeLine.IsHitTestVisible = false;

            activeBeginProcess = beginProcess;
            activeBeginElement = beginElement;
        }

        public void EndVisualConnectAt(UIElement endElement, ProcessControl endProcess)
        {
            this.connectModeActive = false;

            ContentPresenter cp = FrameworkElements.FindVisualAncestor(endElement, typeof(ContentPresenter)) as ContentPresenter;
            string propertyName = (cp.Content as PropertyListing).Name;

            string beginPropertyName;
            if (activeBeginElement is FrameworkElement && (activeBeginElement as FrameworkElement).Name == "OutputNode")
                beginPropertyName = "Output";
            else
            {
                ContentPresenter cp2 = FrameworkElements.FindVisualAncestor(activeBeginElement, typeof(ContentPresenter)) as ContentPresenter;
                beginPropertyName = (cp2.Content as PropertyListing).Name;
            }
            
            //Don't allow links that cross a Permutation Process boundary unless it is from an output node.
            if (!this.connectionPermittedByPermutationRules(activeBeginProcess, endProcess))
            {
                activeLineCleanup();
                return;
            }

            //Don't allow self-linking
            if (activeBeginProcess.Source == endProcess.Source)
            {
                activeLineCleanup();
                return;
            }

            PropertyLink createdLink = null;
            

            //If a link already exists, check for ManyToOne and react accordingly
            if (ManagedRoutine.TargetAlreadyHasALink(endProcess.Source, propertyName))
            {
                //Return out if it's a duplicate link
                if (ManagedRoutine.LinkExists(activeBeginProcess.Source, beginPropertyName, endProcess.Source, propertyName))
                    return;

                if (endProcess.Source.PropertySupportsManyToOne(propertyName))
                {
                    createdLink = ManagedRoutine.AddToLink(activeBeginProcess.Source, beginPropertyName, endProcess.Source, propertyName);
                }
                else
                {
                    createdLink = ManagedRoutine.CreateLink(activeBeginProcess.Source, beginPropertyName, endProcess.Source, propertyName);
                    this.removeLinesByPropertyName(endProcess, propertyName);
                }
            }
            else
            {
                createdLink = ManagedRoutine.CreateLink(activeBeginProcess.Source, beginPropertyName, endProcess.Source, propertyName);
            }

            //New section
            PropertyLinkVisual plv = new PropertyLinkVisual();
            plv.DestinationProcessControl = endProcess;
            plv.OriginProcessControl = this.activeBeginProcess;
            plv.DestinationPropertyListing = PropertyListing.FindFirstByName(endProcess.Source.PropertiesList, propertyName);
            plv.OriginPropertyListing = PropertyListing.FindFirstByName(this.activeBeginProcess.Source.PropertiesList, beginPropertyName);
            plv.PropertyLink = createdLink;
            this.PropertyLinkVisuals.Add(plv);
            //End new section

            //UIElementPointConverter UIEPCon = new UIElementPointConverter();
            //UIEPCon.AncestorForPoint = DisplayPanel;

            //Binding beginPointBindX = new Binding();
            //beginPointBindX.Source = activeBeginElement;
            //beginPointBindX.Converter = UIEPCon;
            //beginPointBindX.ConverterParameter = "x";

            //Binding beginPointBindY = new Binding();
            //beginPointBindY.Source = activeBeginElement;
            //beginPointBindY.Converter = UIEPCon;
            //beginPointBindY.ConverterParameter = "y";

            //Binding endPointBindX = new Binding();
            //endPointBindX.Source = endElement;
            //endPointBindX.Converter = UIEPCon;
            //endPointBindX.ConverterParameter = "x";

            //Binding endPointBindY = new Binding();
            //endPointBindY.Source = endElement;
            //endPointBindY.Converter = UIEPCon;
            //endPointBindY.ConverterParameter = "y";

            //Line newLine = new Line();
            //newLine.SetBinding(Line.X1Property, beginPointBindX);
            //newLine.SetBinding(Line.Y1Property, beginPointBindY);
            //newLine.SetBinding(Line.X2Property, endPointBindX);
            //newLine.SetBinding(Line.Y2Property, endPointBindY);
            ////MesoGrid.SetZIndex(newLine, 100);

            //newLine.Stroke = new SolidColorBrush(Colors.Black);
            //newLine.StrokeThickness = 1;
            //MesoGrid.SetIsDraggable(newLine, false);
            //newLine.IsHitTestVisible = false;
            //this.DisplayPanel.Children.Add(newLine);

            endProcess.Source.PollValidationOnProperties();
            activeBeginProcess.Source.PollValidationOnProperties();

            activeLineCleanup();

            this.updateLineVisuals();
        }

        private bool connectionPermittedByPermutationRules(UIElement from, UIElement to)
        {
            Panel fromParent = MesoGrid.FindParentManagedPanel(from as DependencyObject);
            Panel toParent = MesoGrid.FindParentManagedPanel(to as DependencyObject);

            //Equal parents is okay.
            if (fromParent == toParent) return true;

            //Outside a permutation into a permutation is okay.
            if (fromParent == this.DisplayPanel) return true;

            //Output from an OutputProcess is okay.
            if (from is ProcessControl)
            {
                Type[] interfaces = (from as ProcessControl).Source.Adapter.GetType().GetInterfaces();
                foreach (Type thisInterface in interfaces)
                {
                    if (thisInterface == typeof(IPermutationOutputAdapter)) return true;
                }
            }

            return false;
        }

        private void removeLinesByPropertyName(ProcessControl process, string propertyName)
        {
            List<PropertyLinkVisual> toRemove = new List<PropertyLinkVisual>();

            foreach (PropertyLinkVisual plv in this.PropertyLinkVisuals)
            {
                if ((plv.OriginProcessControl == process || plv.DestinationProcessControl == process) &&
                     (plv.OriginPropertyListing.Name == propertyName || plv.DestinationPropertyListing.Name == propertyName))
                    toRemove.Add(plv);
            }

            foreach (PropertyLinkVisual rem in toRemove)
            {
                this.PropertyLinkVisuals.Remove(rem);
            }

            this.updateLineVisuals();
        }

        private void removeLinesByProcess(ProcessControl process)
        {
            List<PropertyLinkVisual> toRemove = new List<PropertyLinkVisual>();

            foreach (PropertyLinkVisual plv in this.PropertyLinkVisuals)
            {
                if (plv.OriginProcessControl == process || plv.DestinationProcessControl == process)
                    toRemove.Add(plv);
            }

            foreach (PropertyLinkVisual rem in toRemove)
            {
                this.PropertyLinkVisuals.Remove(rem);
            }

            this.updateLineVisuals();

            //List<UIElement> toRemove = new List<UIElement>();

            ////Find the line by checking line bindings to this UIElement
            //foreach (UIElement child in DisplayPanel.Children)
            //{
            //    if (!(child is Line)) continue;

            //    BindingExpression childX1 = BindingOperations.GetBindingExpression(child, Line.X1Property);
            //    BindingExpression childY1 = BindingOperations.GetBindingExpression(child, Line.Y1Property);
            //    BindingExpression childX2 = BindingOperations.GetBindingExpression(child, Line.X2Property);
            //    BindingExpression childY2 = BindingOperations.GetBindingExpression(child, Line.Y2Property);

            //    if (childX1 == null || childY1 == null || childX2 == null || childY2 == null)
            //        continue;

            //    ProcessControl pcX1 = FrameworkElements.FindVisualAncestor(childX1.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;
            //    ProcessControl pcX2 = FrameworkElements.FindVisualAncestor(childX2.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;
            //    ProcessControl pcY1 = FrameworkElements.FindVisualAncestor(childY1.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;
            //    ProcessControl pcY2 = FrameworkElements.FindVisualAncestor(childY2.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;

            //    if (pcX1 == process || pcX2 == process || pcY1 == process || pcY2 == process)
            //        toRemove.Add(child);
            //}

            //foreach (UIElement rem in toRemove)
            //{
            //    DisplayPanel.Children.Remove(rem);
            //}
        }

        private void removeLinesFromByProcess(ProcessControl process)
        {
            List<PropertyLinkVisual> toRemove = new List<PropertyLinkVisual>();

            foreach (PropertyLinkVisual plv in this.PropertyLinkVisuals)
            {
                if (plv.OriginProcessControl == process)
                    toRemove.Add(plv);
            }

            foreach (PropertyLinkVisual rem in toRemove)
            {
                this.PropertyLinkVisuals.Remove(rem);
            }

            this.updateLineVisuals();

            //List<UIElement> toRemove = new List<UIElement>();

            ////Find the line by checking line bindings to this UIElement
            //foreach (UIElement child in DisplayPanel.Children)
            //{
            //    if (!(child is Line)) continue;

            //    BindingExpression childX1 = BindingOperations.GetBindingExpression(child, Line.X1Property);
            //    BindingExpression childY1 = BindingOperations.GetBindingExpression(child, Line.Y1Property);
            //    BindingExpression childX2 = BindingOperations.GetBindingExpression(child, Line.X2Property);
            //    BindingExpression childY2 = BindingOperations.GetBindingExpression(child, Line.Y2Property);

            //    if (childX1 == null || childY1 == null || childX2 == null || childY2 == null)
            //        continue;

            //    ProcessControl pcX1 = FrameworkElements.FindVisualAncestor(childX1.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;
            //    ProcessControl pcX2 = FrameworkElements.FindVisualAncestor(childX2.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;
            //    ProcessControl pcY1 = FrameworkElements.FindVisualAncestor(childY1.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;
            //    ProcessControl pcY2 = FrameworkElements.FindVisualAncestor(childY2.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;

            //    if (pcX1 == process || pcY1 == process)
            //        toRemove.Add(child);
            //}

            //foreach (UIElement rem in toRemove)
            //{
            //    DisplayPanel.Children.Remove(rem);
            //}
        }

        private void removeLinesToByProcess(ProcessControl process)
        {
            List<PropertyLinkVisual> toRemove = new List<PropertyLinkVisual>();

            foreach (PropertyLinkVisual plv in this.PropertyLinkVisuals)
            {
                if (plv.DestinationProcessControl == process)
                    toRemove.Add(plv);
            }

            foreach (PropertyLinkVisual rem in toRemove)
            {
                this.PropertyLinkVisuals.Remove(rem);
            }

            this.updateLineVisuals();

            //List<UIElement> toRemove = new List<UIElement>();

            ////Find the line by checking line bindings to this UIElement
            //foreach (UIElement child in DisplayPanel.Children)
            //{
            //    if (!(child is Line)) continue;

            //    BindingExpression childX1 = BindingOperations.GetBindingExpression(child, Line.X1Property);
            //    BindingExpression childY1 = BindingOperations.GetBindingExpression(child, Line.Y1Property);
            //    BindingExpression childX2 = BindingOperations.GetBindingExpression(child, Line.X2Property);
            //    BindingExpression childY2 = BindingOperations.GetBindingExpression(child, Line.Y2Property);

            //    if (childX1 == null || childY1 == null || childX2 == null || childY2 == null)
            //        continue;

            //    ProcessControl pcX1 = FrameworkElements.FindVisualAncestor(childX1.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;
            //    ProcessControl pcX2 = FrameworkElements.FindVisualAncestor(childX2.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;
            //    ProcessControl pcY1 = FrameworkElements.FindVisualAncestor(childY1.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;
            //    ProcessControl pcY2 = FrameworkElements.FindVisualAncestor(childY2.DataItem as UIElement, typeof(ProcessControl)) as ProcessControl;

            //    if (pcX2 == process || pcY2 == process)
            //        toRemove.Add(child);
            //}

            //foreach (UIElement rem in toRemove)
            //{
            //    DisplayPanel.Children.Remove(rem);
            //}
        }

        private void activeLineCleanup()
        {
            this.DisplayPanel.Children.Remove(activeLine);
            activeLine = null;
            activeBeginElement = null;
            activeBeginProcess = null;
        }

        public void DeleteLinkAt(UIElement delElement, ProcessControl delProcess, bool isInputSide)
        {
            ContentPresenter cp = FrameworkElements.FindVisualAncestor(delElement, typeof(ContentPresenter)) as ContentPresenter;
            string propertyName = (cp.Content as PropertyListing).Name;

            if (isInputSide)
                ManagedRoutine.DeleteLink(delProcess.Source, propertyName);
            else
                ManagedRoutine.DeleteLinksTo(delProcess.Source, propertyName);

            this.removeLinesByPropertyName(delProcess, propertyName);
            deleteLinkModeActive = false;

            foreach (Process proc in this.ManagedRoutine.ProcessList)
            {
                proc.PollValidationOnProperties();
            }

            this.updateLineVisuals();
        }

        public void DeleteProcess(ProcessControl delProcess)
        {
            //if (delProcess == null) return;
            //removeLinesByProcess(delProcess);
            //ManagedRoutine.DeleteProcess(delProcess.Source);
            //DisplayPanel.Children.Remove(delProcess);
            //DeleteProcessModeActive = false;

            //foreach (Process proc in this.ManagedRoutine.ProcessList)
            //{
            //    proc.PollValidationOnProperties();
            //}

            //this.updateLineVisuals();
            this.DeleteProcess(delProcess, this.DisplayPanel);
        }

        public void DeleteProcess(ProcessControl delProcess, Panel toDeleteFrom)
        {
            if (delProcess == null) return;
            removeLinesByProcess(delProcess);
            ManagedRoutine.DeleteProcess(delProcess.Source);

            //This doesn't seem to work correctly for removing permoutputprocs, find out why!
            toDeleteFrom.Children.Remove(delProcess);
            DeleteProcessModeActive = false;

            foreach (Process proc in this.ManagedRoutine.ProcessList)
            {
                proc.PollValidationOnProperties();
            }

            this.updateLineVisuals();
        }

        public void DeletePermutationProcess(PermutationProcessControl delProcess)
        {
            if (delProcess == null) return;
            removeLinesByProcess(delProcess.ProcessPanel);
            ManagedRoutine.DeleteProcess(delProcess.Source);
            DisplayPanel.Children.Remove(delProcess);
            DeleteProcessModeActive = false;

            foreach (Process proc in this.ManagedRoutine.ProcessList)
            {
                proc.PollValidationOnProperties();
            }

            this.updateLineVisuals();
        }

        public void ConnectProcesses(Process outputProc, string outputProperty, Process inputProc, string inputProperty)
        {

        }

        public void FullRefresh()
        {
            //DisplayPanel.Children.Clear();

            ////First, add the processes.
            //foreach (Process proc in ManagedRoutine.ProcessList)
            //{
            //    ProcessControl procCon = new ProcessControl();
            //    procCon.Source = proc;
            //    DisplayPanel.Children.Add(procCon);

            //    //Now attach to the generator so when it creates the UI, we can generate the appropriate lines.
            //    //procCon.ContentDisplay.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
            //    //EventManager.RegisterClassHandler(typeof(Rectangle), Rectangle.LoadedEvent, new EventHandler(Rectangle_Loaded));
            //}

            this.updateLineVisuals();
        }

        void Rectangle_Loaded(object sender, EventArgs e)
        {

        }

        //Compile
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (ManagedRoutine == null) return;
            ManagedRoutine.GenerateRoutineProcessingStack();
        }

        //Run
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (ManagedRoutine == null) return;

            MessageBox.Show("Don't click me!");

            //ManagedRoutine.RunRoutine();
        }

        //Values
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            //if (ManagedRoutine == null) return;
            //string ret = "";
            //foreach (Process proc in ManagedRoutine.ProcessList)
            //{
            //    ret += "\n" + proc + "\n";
            //    foreach (string prop in proc.Properties.Keys)
            //    {
            //        ret += (prop + " : " +  proc.Adapter.SupplyPropertyValue(prop).ToString() + "\n");
            //    }
            //}

            //MessageBox.Show(ret);

            //foreach (UIElement element in DisplayPanel.Children)
            //{
            //    if (!(element is ProcessControl)) continue;
            //    ProcessControl proc = element as ProcessControl;

            //    foreach (PropertyListing listing in proc.PropertyList)
            //    {
            //        listing.Value = proc.Source.Adapter.SupplyPropertyValue(listing.Name);
            //    }
            //}
        }

        //List Cyclical
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (ManagedRoutine == null) return;
            List<Process> cyc = ManagedRoutine.ListCyclicalExecutionExclusions();
            string ret = "The following Processes were involved in a cyclical reference,\n and have been removed from execution : \n\n";
            if (cyc.Count == 0)
                ret = "No cyclical references found!";

            foreach (Process proc in cyc)
            {
                ret += proc.Name + "\n";
            }

            MessageBox.Show(ret);
        }

        //Delete link
        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            this.deleteLinkModeActive = true;
        }

        private int testIter = 0;

        private double gridXOffset = 20.0;
        private double gridYOffset = 20.0;
        private double step = 1;

        //Add Adapter
        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            AddAdapters();
        }

        private void AddAdapters()
        {
            foreach (IFlowControlAdapter sel in this.AdapterListBox.SelectedItems)
            {
                if (sel == null) return;

                IFlowControlAdapter newAdapter = sel.Clone() as IFlowControlAdapter;

                object proc;
                DependencyObject procCon;

                if (newAdapter is IPermutationAdapter)
                {
                    //Do Permutation handling
                    proc = new PermutationProcess(newAdapter as IPermutationAdapter);

                    (proc as PermutationProcess).Name = testIter.ToString() + ":" + newAdapter.Name;

                    procCon = new PermutationProcessControl();
                    (procCon as PermutationProcessControl).ProcessPanel.Source = (proc as PermutationProcess);

                    (procCon as PermutationProcessControl).MouseEnter += new MouseEventHandler(RoutineManager_MouseEnter);
                    (procCon as PermutationProcessControl).MouseLeave += new MouseEventHandler(RoutineManager_MouseLeave);

                    ManagedRoutine.ProcessList.Add(proc as PermutationProcess);
                }
                else if (newAdapter is IPermutationOutputAdapter)
                {
                    proc = new PermutationOutputProcess();
                    (proc as PermutationOutputProcess).Adapter = newAdapter as IPermutationOutputAdapter;

                    (proc as PermutationOutputProcess).Name = testIter.ToString() + ":" + newAdapter.Name;

                    procCon = new ProcessControl();
                    (procCon as ProcessControl).Source = (proc as Process);
                    ManagedRoutine.ProcessList.Add(proc as Process);
                }
                else
                {
                    //Do normal handling
                    proc = new Process(newAdapter);

                    (proc as Process).Name = testIter.ToString() + ":" + newAdapter.Name;

                    procCon = new ProcessControl();
                    (procCon as ProcessControl).Source = (proc as Process);
                    ManagedRoutine.ProcessList.Add(proc as Process);
                }

                testIter++;


                MesoGrid.SetIsConstrained(procCon, false);

                //Move the X/Y offset by the Translation so it always shows up on the screen
                double adjustedXOffset = gridXOffset + (this.DisplayPanelTranslation.X * -1);
                double adjustedYOffset = gridYOffset + (this.DisplayPanelTranslation.Y * -1);

                MesoGrid.SetXCoordinate(procCon, adjustedXOffset);
                MesoGrid.SetYCoordinate(procCon, adjustedYOffset);
                gridXOffset += 30;
                gridYOffset += 30;

                if ((gridXOffset + 100) > DisplayPanel.ActualWidth || (gridYOffset + 100) > DisplayPanel.ActualHeight)
                {
                    gridYOffset = 20;
                    gridXOffset = 20 + (step * 150);
                    step += 1;
                    if ((gridXOffset + 100) > DisplayPanel.ActualWidth)
                    {
                        gridXOffset = 20;
                        step = 1;
                    }
                }


                DisplayPanel.Children.Add(procCon as UIElement);

                if (newAdapter is IPermutationAdapter)
                    (procCon as PermutationProcessControl).ProcessPanel.OutputToPanel = false;
                else
                    (procCon as ProcessControl).OutputToPanel = false;

                (proc as Process).PollValidationOnProperties();

                //Have to do this last to correct the Drag Grip targeting for the custom drag grip on the permutation control.
                if (procCon is PermutationProcessControl)
                    (procCon as PermutationProcessControl).CorrectDragGripTarget();
            }
        }

        public void AttachPermProcMouseEvents()
        {
            foreach (object obj in this.DisplayPanel.Children)
            {
                if (!(obj is IHasAProcessSource)) continue;

                IHasAProcessSource procCon = obj as IHasAProcessSource;

                if (procCon.Source.Adapter is IPermutationAdapter)
                {
                    (procCon as PermutationProcessControl).MouseEnter += new MouseEventHandler(RoutineManager_MouseEnter);
                    (procCon as PermutationProcessControl).MouseLeave += new MouseEventHandler(RoutineManager_MouseLeave);
                }
            }
        }

        private void regenerateLinesForChildren(Panel toGenerateFor, IHasAProcessSource sourcePanel)
        {
            foreach (object obj in toGenerateFor.Children)
            {
                if (!(obj is IHasAProcessSource)) continue;

                IHasAProcessSource procCon = obj as IHasAProcessSource;
                bool isPermutation = procCon.Source.Adapter is IPermutationAdapter;

                if (sourcePanel is PermutationProcessControl)
                {
                    if ((sourcePanel as PermutationProcessControl).Source == procCon.Source)
                        isPermutation = false;
                }

                foreach (PropertyListing pl in procCon.Source.PropertiesList)
                {
                    if (pl.Value is PropertyLink)
                    {
                        PropertyLink pLink = pl.Value as PropertyLink;
                        PropertyLinkVisual newVisual = new PropertyLinkVisual();
                        newVisual.PropertyLink = pLink;
                        if (isPermutation)
                            newVisual.DestinationProcessControl = (procCon as PermutationProcessControl).ProcessPanel;
                        else
                            newVisual.DestinationProcessControl = procCon as ProcessControl;
                        newVisual.DestinationPropertyListing = pl;

                        IHasAProcessSource host = this.findProcessControl(pLink.HostProcess);
                        if (host  == null || host.Source == null) continue;
                        if (host.Source.Adapter is IPermutationAdapter)
                            newVisual.OriginProcessControl = (host as PermutationProcessControl).ProcessPanel;
                        else
                            newVisual.OriginProcessControl = host as ProcessControl;
                        newVisual.OriginPropertyListing = PropertyListing.FindFirstByName(host.Source.PropertiesList, pLink.PropertyName);
                        this.PropertyLinkVisuals.Add(newVisual);
                    }
                    else if (pl.Value is PropertyMultiLink)
                    {
                        PropertyMultiLink pmLink = pl.Value as PropertyMultiLink;
                        foreach (PropertyLink pLink in pmLink.PropertyLinks)
                        {
                            PropertyLinkVisual newVisual = new PropertyLinkVisual();
                            newVisual.PropertyLink = pLink;
                            if (isPermutation)
                                newVisual.DestinationProcessControl = (procCon as PermutationProcessControl).ProcessPanel;
                            else
                                newVisual.DestinationProcessControl = procCon as ProcessControl;
                            newVisual.DestinationPropertyListing = pl;

                            IHasAProcessSource host = this.findProcessControl(pLink.HostProcess);
                            if (host == null || host.Source == null) continue;
                            if (host.Source.Adapter is IPermutationAdapter)
                                newVisual.OriginProcessControl = (host as PermutationProcessControl).ProcessPanel;
                            else
                                newVisual.OriginProcessControl = host as ProcessControl;
                            newVisual.OriginPropertyListing = PropertyListing.FindFirstByName(procCon.Source.PropertiesList, pLink.PropertyName);
                            this.PropertyLinkVisuals.Add(newVisual);
                        }
                    }
                    else
                        continue;
                }

                if (procCon is PermutationProcessControl)
                    this.regenerateLinesForChildren((procCon as PermutationProcessControl).PermutationChildrenPanel, procCon);
            }
        }

        public void RegenerateLinesFromLinks()
        {
            this.PropertyLinkVisuals.Clear();
            this.regenerateLinesForChildren(this.DisplayPanel, null);
            this.updateLineVisuals();
        }

        private IHasAProcessSource findProcessControlInChildren(Process targetSource, Panel panel)
        {
            IHasAProcessSource ret = null;

            foreach (object obj in panel.Children)
            {
                if (!(obj is IHasAProcessSource)) continue;
                IHasAProcessSource procCon = obj as IHasAProcessSource;

                if (procCon.Source == targetSource) return procCon;

                if (procCon is PermutationProcessControl)
                    ret = this.findProcessControlInChildren(targetSource, 
                        (procCon as PermutationProcessControl).PermutationChildrenPanel);
            }

            return ret;
        }

        private IHasAProcessSource findProcessControl(Process targetSource)
        {
            IHasAProcessSource ret = this.findProcessControlInChildren(targetSource, this.DisplayPanel);

            return ret;
        }

        private List<PermutationProcessControl> permProcStack = new List<PermutationProcessControl>();

        void RoutineManager_MouseLeave(object sender, MouseEventArgs e)
        {
            this.permProcStack.Remove(sender as PermutationProcessControl);
        }

        void RoutineManager_MouseEnter(object sender, MouseEventArgs e)
        {
            this.permProcStack.Add(sender as PermutationProcessControl);
        }

        private string shortName(string input)
        {
            int index = input.LastIndexOf('.');
            return input.Substring(index + 1);
        }

        //Delete Process
        private void Button_Click_7(object sender, RoutedEventArgs e)
        {

            DeleteProcessModeActive = !DeleteProcessModeActive;
        }

        public string ControllerTypeString { get; set; }

        //Processing Stack
        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            string ret = "A list of Processing Stack levels and relevant Processes follows.\n\n";

            for (int i = 0; i < ManagedRoutine.ProcessingStack.Count; i++)
            {
                ret += "Level " + i + " :\n";
                foreach (Process proc in ManagedRoutine.ProcessingStack[i])
                {
                    ret += proc.Name + " ";
                }
                ret += "\n\n";
            }

            MessageBox.Show(ret);
        }

        //Save
        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            foreach (Process proc in this.ManagedRoutine.ProcessList)
            {
                proc.PollValidationOnProperties();
                if (!proc.ValidationPassed)
                {
                    //Oops, validation failed.  Refuse to save!
                    MessageBox.Show("Validation failed on process : " + proc.Name + ".  Cannot save until all validation checks pass!");
                    return;
                }
            }



            //if (!Directory.Exists(this.serializationPath))
            //    Directory.CreateDirectory(this.serializationPath);

            //If the ID isn't set it means it's new, so create a guid for it.
            if (!this.IDIsSet)
                this.ID = Guid.NewGuid();

            //Serialize the Routine and RoutineManager
            RoutineSerializer rs = RoutineSerializer.Serialize(this.ManagedRoutine);
            RoutineManagerSerializer rms = RoutineManagerSerializer.Serialize(this);

            if (this.IDIsSet)
            {
                //Update
                RoutineCentral.UpdateCentralWithRoutine(this.RoutineName, this.ID, rs, rms, this.ControllerTypeString);
            }
            else
            {
                //Register new
                this.registerNewRoutine(rs, rms);
                this.IDIsSet = true;
            }
        }

        public string RoutineName { get; set; }

        private void registerNewRoutine(RoutineSerializer rs, RoutineManagerSerializer rms)
        {
            RoutineRegistrationControl popup = new RoutineRegistrationControl();

            ContentBox cbox = new ContentBox();
            cbox.ContentBoxClosed += new EventHandler<ContentBoxEventArgs>(cbox_ContentBoxClosed);
            cbox.Buttons = ContentBoxButton.OKCancel;
            cbox.ContentElement = popup;
            cbox.Title = "Name This Routine";

            cbox.ShowDialog();

            RoutineCentral.UpdateCentralWithRoutine(this.RoutineName, this.ID, rs, rms, this.ControllerTypeString);
        }

        void cbox_ContentBoxClosed(object sender, ContentBoxEventArgs e)
        {
            if (e.Result == ContentBoxResult.Cancel)
                return;
            RoutineRegistrationControl popup = (sender as ContentBox).ContentElement as RoutineRegistrationControl;

            this.RoutineName = popup.RoutineNameBox.Text;
        }

        //load
        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            this.updateLineVisuals();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.updateLineVisuals();
        }

        private Geometry adornerBounds = null;

        private void updateLineVisuals()
        {
            //Clean up PropertyLinkVisuals, in case any of them have lost their destination link (this can occur when an adapter changes its properties and wipes
            //out a link unexpectedly)
            List<PropertyLinkVisual> toRemove = new List<PropertyLinkVisual>();

            foreach (PropertyLinkVisual plv in this.PropertyLinkVisuals)
            {
                if (!(plv.DestinationPropertyListing.Value is PropertyLink) && !(plv.OriginPropertyListing.Value is PropertyMultiLink))
                    toRemove.Add(plv);
            }

            foreach (PropertyLinkVisual rem in toRemove)
            {
                this.PropertyLinkVisuals.Remove(rem);
            }

            //handle the adorner action
            AdornerLayer al = AdornerLayer.GetAdornerLayer(this.DisplayPanel);
            

            if (al != null)
            {
                Adorner[] toUpdateArray = al.GetAdorners(this.DisplayPanel);
                Point holderOffset = this.HolderCanvas.TranslatePoint(new Point(0,0), this);
                if (toUpdateArray != null)
                {
                    foreach (Adorner toUpdate in toUpdateArray)
                    {
                        if (toUpdate is PropertyLinkVisualAdorner)
                        {
                            (toUpdate as PropertyLinkVisualAdorner).Source = this.PropertyLinkVisuals;
                        }
                    }
                }
                else
                {
                    PropertyLinkVisualAdorner newLayer = new PropertyLinkVisualAdorner(this.DisplayPanel, this.PropertyLinkVisuals);
                    al.Add(newLayer);
                }

                this.adornerBounds = VisualTreeHelper.GetClip(this.HolderCanvas).Clone();
                
                //Use shenannigans to find the actual offset for the clip - since GetClip doesn't work right with grids!
                Point offset = this.ThisElement.PointFromScreen(this.HolderCanvas.PointToScreen(new Point(0, 0)));
                adornerBounds.Transform = new TranslateTransform(offset.X, offset.Y);
                al.Clip = adornerBounds;

                al.Update(this.DisplayPanel);
            }
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        private void AdapterListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.AddAdapters();
        }
    }

    public enum LineEnd
    {
        Beginning,
        End
    }

    public class AttachedLine
    {
        public Line SourceLine { get; set; }
        public LineEnd AttachedEnd { get; set; }
        public UIElement AttachedUIElement { get; set; }

        public double AttachedX
        {
            get
            {
                if (this.AttachedEnd == LineEnd.Beginning)
                    return SourceLine.X1;
                return SourceLine.X2;
            }
            set
            {
                if (this.AttachedEnd == LineEnd.Beginning)
                    SourceLine.X1 = value;
                SourceLine.X2 = value;
            }
        }
        public double AttachedY
        {
            get
            {
                if (this.AttachedEnd == LineEnd.Beginning)
                    return SourceLine.Y1;
                return SourceLine.Y2;
            }
            set
            {
                if (this.AttachedEnd == LineEnd.Beginning)
                    SourceLine.Y1 = value;
                SourceLine.Y2 = value;
            }
        }

        public void SetAttachedCoordinate(Point newCoordinate)
        {
            AttachedY = newCoordinate.Y;
            AttachedX = newCoordinate.X;
        }

    }

    public class ProcConPackage
    {
        public Point Offset { get; set; }
        public int ProcessIndex { get; set; }
        public int ParentIndex { get; set; }
    }

    public class RoutineManagerSerializer
    {
        public List<ProcConPackage> ProcPacks { get; set; }
        public Guid ID { get; set; }
        public string Name { get; set; }

        static private void serializeChildren(Panel serializingPanel,ref RoutineManagerSerializer ret, int parentIndex,
            RoutineManager toSerialize)
        {
            foreach (object obj in serializingPanel.Children)
            {
                if (!(obj is IHasAProcessSource)) continue;

                //if this is the members of a permProc, make sure we ignore the pproc's controller process
                if (parentIndex != -1)
                {
                    PermutationProcess pp = toSerialize.ManagedRoutine.ProcessList[ret.ProcPacks[parentIndex].ProcessIndex] as PermutationProcess;

                    if ((obj as IHasAProcessSource).Source == pp)
                        continue;
                }

                IHasAProcessSource procCon = obj as IHasAProcessSource;

                ProcConPackage pcPack = new ProcConPackage();
                Point newOffset = new Point();
                newOffset.X = MesoGrid.GetXCoordinate(procCon as DependencyObject);
                newOffset.Y = MesoGrid.GetYCoordinate(procCon as DependencyObject);
                pcPack.Offset = newOffset;
                pcPack.ProcessIndex = toSerialize.ManagedRoutine.ProcessList.IndexOf(procCon.Source);
                pcPack.ParentIndex = parentIndex;

                ret.ProcPacks.Add(pcPack);

                if (procCon is PermutationProcessControl)
                    RoutineManagerSerializer.serializeChildren((procCon as PermutationProcessControl).PermutationChildrenPanel,
                                                                ref ret, ret.ProcPacks.IndexOf(pcPack), toSerialize);
            }
        }

        static public RoutineManagerSerializer Serialize(RoutineManager toSerialize)
        {
            RoutineManagerSerializer ret = new RoutineManagerSerializer();
            ret.ProcPacks = new List<ProcConPackage>();

            RoutineManagerSerializer.serializeChildren(toSerialize.DisplayPanel, ref ret, -1, toSerialize);

            ret.ID = toSerialize.ID;
            ret.Name = toSerialize.RoutineName;

            return ret;
        }

        static public RoutineManager Deserialize(RoutineManagerSerializer toDeserialize, Routine toAttach)
        {
            RoutineManager ret = new RoutineManager();
            ret.ManagedRoutine = toAttach;

            List<IHasAProcessSource> existingProcCon = new List<IHasAProcessSource>();

            foreach (ProcConPackage pcPack in toDeserialize.ProcPacks)
            {
                Process source = toAttach.ProcessList[pcPack.ProcessIndex];
                object thisProcControl;
                //(thisProcControl as IHasAProcessSource).Source = source;

                if (source.Adapter is IPermutationAdapter)
                {
                    thisProcControl = new PermutationProcessControl();
                }
                else
                {
                    thisProcControl = new ProcessControl();
                }

                (thisProcControl as IHasAProcessSource).Source = source;

                MesoGrid.SetIsConstrained(thisProcControl as DependencyObject, false);
                MesoGrid.SetXCoordinate(thisProcControl as DependencyObject, pcPack.Offset.X);
                MesoGrid.SetYCoordinate(thisProcControl as DependencyObject, pcPack.Offset.Y);

                existingProcCon.Add(thisProcControl as IHasAProcessSource);

                if (pcPack.ParentIndex == -1)
                    ret.DisplayPanel.Children.Add(thisProcControl as UIElement);
                else
                {
                    //At this point the parent should be added.  We're going to assume it has been.
                    PermutationProcessControl ppc = existingProcCon[pcPack.ParentIndex] as PermutationProcessControl;
                    ppc.PermutationChildrenPanel.Children.Add(thisProcControl as UIElement);
                }
         
                if (source.Adapter is IPermutationAdapter)
                {
                    (thisProcControl as PermutationProcessControl).ProcessPanel.OutputToPanel = false;
                    (thisProcControl as PermutationProcessControl).CorrectDragGripTarget();
                }
                else
                    (thisProcControl as ProcessControl).OutputToPanel = true;

                source.PollValidationOnProperties();
            }

            ret.ID = toDeserialize.ID;
            ret.IDIsSet = true;
            ret.RoutineName = toDeserialize.Name;

            return ret;
        }
    }
}
