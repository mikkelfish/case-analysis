﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace CeresBase.FlowControl
{
    public class PropToFlowProp
    {
        public static PropToFlowProp[] GetProps(Type type)
        {
            List<PropToFlowProp> toret = new List<PropToFlowProp>();

            AssociateWithPropertyAttribute[][] attrs;
            PropertyInfo[] proplist =
               MesosoftCommon.Utilities.Reflection.Common.GetMembersWithAttribute<PropertyInfo, AssociateWithPropertyAttribute>(
                    type, true, BindingFlags.Public | BindingFlags.Instance, out attrs);
            for (int i = 0; i < proplist.Length; i++)
            {
                toret.Add(new PropToFlowProp(proplist[i], attrs[i][0]));                
            }

            return toret.ToArray();

        }

        public PropertyInfo Property { get; private set; }
        public AssociateWithPropertyAttribute Attribute { get; private set; }

        public PropToFlowProp(PropertyInfo prop, AssociateWithPropertyAttribute attr)
        {
            this.Property = prop;
            this.Attribute = attr;
        }
    }
}
