﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public enum ValidationState
    {
        Unvalidated,
        Fail,
        Pass,
        Indeterminate
    }

    public class ValidationStatus
    {
        public ValidationState ReturnValue = ValidationState.Unvalidated;
        public string ReturnComment = "";

        public ValidationStatus(ValidationState state)
        {
            ReturnValue = state;
        }

        public ValidationStatus(ValidationState state, string comment)
        {
            ReturnValue = state;
            ReturnComment = comment;
        }
    }
}
