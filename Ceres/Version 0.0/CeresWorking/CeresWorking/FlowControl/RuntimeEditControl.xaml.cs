﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Interaction logic for RuntimeEditControl.xaml
    /// </summary>
    public partial class RuntimeEditControl : UserControl
    {
        public RuntimeEditControl()
        {
            InitializeComponent();
        }

        public Routine Source
        {
            get { return (Routine)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Routine), typeof(RuntimeEditControl));

    }
}
