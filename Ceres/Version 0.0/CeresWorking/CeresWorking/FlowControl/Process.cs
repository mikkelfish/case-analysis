﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections;
using System.Reflection;
using MesosoftCommon.Utilities.Reflection;

namespace CeresBase.FlowControl
{
    [Serializable]
    public class Process : INotifyPropertyChanged
    {
        public event EventHandler<ProcessPropertyChangedArgs> PropertyEdited;

        public int PermutationCopyID { get; set; }

        public bool IsAStorageProcess
        {
            get
            {
                return (adapter is StorageAdapter);
            }
        }

        private bool outputToPanel = false;
        public bool OutputToPanel
        {
            get
            {
                return this.outputToPanel;
            }
            set
            {
                this.outputToPanel = value;
            }
        }

        public override string ToString()
        {
            return Name;
        }

        public string Name { get; set; }

        private object adapterDescription;
        public object AdapterDescription
        {
            get
            {
                return this.adapterDescription;
            }
            set
            {
                this.adapterDescription = value;
            }
        }

        protected IFlowControlAdapter adapter;
        public IFlowControlAdapter Adapter
        {
            get
            {
                return this.adapter;
            }
            set
            {
                if (this.adapter != null)
                    this.adapter.HostProcess = null;

                this.adapter = value;
                this.adapter.HostProcess = this;
                onAdapterChanged();
            }
        }

        protected void onAdapterChanged()
        {
            ReloadPropertiesFromAdapter();

        }


        public void RunAdapter()
        {
            this.Adapter.RunAdapter();
        }

        public void ReloadPropertiesFromAdapter()
        {
            PropertiesList.Clear();

            foreach (string propName in this.Adapter.SupplyPropertyNames())
            {
                PropertyListing newProp = new PropertyListing(propName, Adapter.SupplyPropertyValue(propName));
                newProp.UsingAdapterDefault = true;
                newProp.CanEdit = Adapter.CanEditProperty(propName);
                newProp.CanReceivePropertyLinks = Adapter.CanReceivePropertyLinks(propName);
                newProp.CanSendPropertyLinks = Adapter.CanSendPropertyLinks(propName);
                newProp.IsOutput = Adapter.IsOutputProperty(propName);
                newProp.ValidationStatus.ReturnValue = ValidationState.Unvalidated;
                newProp.Description = Adapter.SupplyPropertyDescription(propName);
                PropertiesList.Add(newProp);
            }

            this.AdapterDescription = this.Adapter.SupplyAdapterDescription();
        }

        public void ReloadValuesOnlyFromAdapter()
        {
            foreach (PropertyListing prop in this.PropertiesList)
            {
                if (prop.UsingAdapterDefault &&
                    !(prop.Value is PropertyLink) && !(prop.Value is PropertyMultiLink))
                {
                    object val = Adapter.SupplyPropertyValue(prop.Name);

                    prop.Value = val;
                }
            }
        }

        public void PushPropertiesToAdapter()
        {
            foreach (PropertyListing property in PropertiesList)
            {
                //Fixing a bug where if value was null it was crashing
                bool isLink = false;
                if (property.Value != null)
                {
                    if (property.Value.GetType() == typeof(PropertyLink) ||
                        property.Value.GetType() == typeof(PropertyMultiLink))
                    {
                        isLink = true;
                    }
                }

                //Only push properties that aren't using the adapter default and aren't links!
                if ((!property.UsingAdapterDefault) && !isLink)
                    Adapter.ReceivePropertyValue(property.Value, property.Name);
            }
        }

        //public Dictionary<string, object> Properties { get; set; }

        private ObservableCollection<PropertyListing> propertiesList = new ObservableCollection<PropertyListing>();
        public ObservableCollection<PropertyListing> PropertiesList
        {
            get
            {
                return this.propertiesList;
            }
        }

        public int? IndexInOutputList(PropertyListing target)
        {
            int ret = 0;
            foreach (PropertyListing pl in this.PropertiesList)
            {
                if (pl == target) return ret;
                if (pl.IsOutput) ret++;
            }

            return null;
        }

        public int? IndexInNormalList(PropertyListing target)
        {
            int ret = 0;
            foreach (PropertyListing pl in this.PropertiesList)
            {
                if (pl == target) return ret;
                if (!(pl.IsOutput)) ret++;
            }

            return null;
        }

        public int OutputCount()
        {
            int ret = 0;
            foreach (PropertyListing pl in this.propertiesList)
            {
                if (pl.IsOutput) ret++;
            }

            return ret;
        }

        public int NormalCount()
        {
            return this.PropertiesList.Count - this.OutputCount();
        }

        public Process()
        {
        }

        public Process(IFlowControlAdapter adapter)
        {
            Adapter = adapter;
        }

        public bool PropertySupportsManyToOne(string propertyName)
        {
            Type targetPropertyType = this.adapter.PropertyType(propertyName);

            if (targetPropertyType.IsArray) return true;

            Type[] interfaces = targetPropertyType.GetInterfaces();
            foreach (Type thisInterface in interfaces)
            {
                if (thisInterface == typeof(IList)) return true;
            }

            return false;
        }

        private List<PropertyListing> preEditValues = new List<PropertyListing>();

        public void StorePreEditValues()
        {
            preEditValues.Clear();
            foreach (PropertyListing pl in propertiesList)
            {
                PropertyListing newListing = new PropertyListing() { Name = pl.Name, Value = pl.Value };

                preEditValues.Add(newListing);
            }
        }

        public void CheckForChangesAndNotify()
        {
            List<ProcessPropertyChangedInfo> allArgs = new List<ProcessPropertyChangedInfo>();
            for (int i = 0; i < propertiesList.Count; i++)
            {
                PropertyListing oldPL = preEditValues[i];
                PropertyListing newPL = propertiesList[i];

                if (newPL.Value == null) continue;

                if (!newPL.Value.Equals(oldPL.Value))
                {
                    if (this.PropertyEdited != null)
                    {
                        ProcessPropertyChangedInfo ppca = new ProcessPropertyChangedInfo();
                        ppca.NewValue = newPL.Value;
                        ppca.OldValue = oldPL.Value;
                        ppca.PropertyName = newPL.Name;
                        allArgs.Add(ppca);
                    }
                }
            }

            this.PushPropertiesToAdapter();

            if (this.PropertyEdited != null)
            {
                this.PropertyEdited(this, new ProcessPropertyChangedArgs() { PropertiesChanged = allArgs.ToArray() });
            }

            this.PollValidationOnProperties();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }


        #endregion

        public void PollValidationOnProperties()
        {
            foreach (PropertyListing prop in this.PropertiesList)
            {
                prop.ValidationStatus = Adapter.ValidateProperty(prop.Name, prop.Value);
            }
        }

        public bool HasValidated
        {
            get
            {
                foreach (PropertyListing prop in this.PropertiesList)
                {
                    if (prop.ValidationStatus.ReturnValue == ValidationState.Unvalidated)
                        return false;
                }
                return true;
            }
        }

        public bool ValidationPassed
        {
            get
            {
                foreach (PropertyListing prop in this.PropertiesList)
                {
                    if (prop.ValidationStatus.ReturnValue != ValidationState.Pass)
                        return false;
                }
                return true;
            }
        }
    }

    public class ProcessSerializer
    {
        public int ID { get; set; }
        public object[] AdapterValues { get; set; }
        public string AdapterTypeName { get; set; }
        public string Name { get; set; }
        public object Output { get; set; }
        public string SavedSubtype { get; set; }
        public List<PropertyListingSerializer> PropertiesList { get; set; }
        public List<int> PermutationProcessList { get; set; }
        public List<int> PermutationOutputProcessList { get; set; }
        public bool OutputToPanel { get; set; }

        static public ProcessSerializer Serialize(Process toSerialize, Routine hostRoutine)
        {
            ProcessSerializer ret = new ProcessSerializer();
            object[] adapterVals = toSerialize.Adapter.GetValuesForSerialization();

            ret.ID = hostRoutine.ProcessList.IndexOf(toSerialize);
            if (adapterVals != null)
            {
                ret.AdapterValues = new object[adapterVals.Length];
                Array.Copy(adapterVals, ret.AdapterValues, adapterVals.Length);
            }
            else
                ret.AdapterValues = null;

            ret.AdapterTypeName = toSerialize.Adapter.GetType().FullName;
            ret.Name = toSerialize.Name;
            ret.OutputToPanel = toSerialize.OutputToPanel;

            ret.PropertiesList = new List<PropertyListingSerializer>();
            foreach (PropertyListing prop in toSerialize.PropertiesList)
            {
                if (!toSerialize.Adapter.ShouldSerialize(prop.Name)) continue;
                ret.PropertiesList.Add(PropertyListingSerializer.Serialize(prop, hostRoutine));
            }

            ret.SavedSubtype = toSerialize.GetType().FullName;

            //Serialize the PermutationProcess values if it is one.
            if (toSerialize is PermutationProcess)
            {
                ret.PermutationProcessList = new List<int>();
                ret.PermutationOutputProcessList = new List<int>();
                foreach (Process proc in (toSerialize as PermutationProcess).PermutationMemberProcesses[0])
                {
                    ret.PermutationProcessList.Add(hostRoutine.ProcessList.IndexOf(proc));
                }

                foreach (Process proc in (toSerialize as PermutationProcess).PermutationOutputProcesses)
                {
                    if (hostRoutine.ProcessList.Contains(proc))
                    {
                        ret.PermutationOutputProcessList.Add(hostRoutine.ProcessList.IndexOf(proc));
                    }
                }
            }

            return ret;
        }

        static public Process Deserialize(ProcessSerializer toDeserialize, Routine hostRoutine)
        {
            Type deserializeType = RetrieveLoadedTypes.GetType(toDeserialize.SavedSubtype);
            ConstructorInfo pci = deserializeType.GetConstructor(Type.EmptyTypes);
            Process ret = pci.Invoke(null) as Process;
            object[] adapterVals;



            Type adapterType = RetrieveLoadedTypes.GetType(toDeserialize.AdapterTypeName);
            ConstructorInfo ci = adapterType.GetConstructor(System.Type.EmptyTypes);
            object adapter = ci.Invoke(null);

            if (adapter is IFlowControlAdapter)
            {
                (adapter as IFlowControlAdapter).LoadValuesFromSerialization(toDeserialize.AdapterValues);
                ret.Adapter = adapter as IFlowControlAdapter;
            }
            else
                throw new Exception("Failed deserializing non-IFlowControlAdapter type adapter.");

            //if (toDeserialize.AdapterValues != null)
            //    ret.Adapter.LoadValuesFromSerialization(toDeserialize.AdapterValues);

            ret.Name = toDeserialize.Name;
            ret.OutputToPanel = toDeserialize.OutputToPanel;
            //ret.Output = toDeserialize.Output;

            ret.PropertiesList.Clear();
            foreach (PropertyListingSerializer prop in toDeserialize.PropertiesList)
            {
                PropertyListing pl = PropertyListingSerializer.Deserialize(prop, hostRoutine);
                pl.CanEdit = ret.Adapter.CanEditProperty(pl.Name);
                pl.CanReceivePropertyLinks = ret.Adapter.CanReceivePropertyLinks(pl.Name);
                pl.CanSendPropertyLinks = ret.Adapter.CanSendPropertyLinks(pl.Name);
                pl.IsOutput = ret.Adapter.IsOutputProperty(pl.Name);
                pl.ValidationStatus.ReturnValue = ValidationState.Unvalidated;
                ret.PropertiesList.Add(pl);
                if (!(pl.Value is PropertyLink) && !(pl.Value is PropertyMultiLink))
                {
                    if(ret.Adapter.CanReceivePropertyValue(pl.Value, pl.Name))
                        ret.Adapter.ReceivePropertyValue(pl.Value, pl.Name);
                }
            }

            if (ret is PermutationProcess)
            {
                (ret as PermutationProcess).PermutationMemberProcesses = new List<List<Process>>();
                (ret as PermutationProcess).PermutationMemberProcesses.Add(new List<Process>());
                if (toDeserialize.PermutationProcessList != null)
                {
                    foreach (int procIndex in toDeserialize.PermutationProcessList)
                    {
                        if (procIndex == -1) continue;
                        (ret as PermutationProcess).PermutationMemberProcesses[0].Add(hostRoutine.ProcessList[procIndex]);
                    }
                }

                (ret as PermutationProcess).PermutationOutputProcesses = new List<PermutationOutputProcess>();
                if (toDeserialize.PermutationOutputProcessList != null)
                {
                    foreach (int procIndex in toDeserialize.PermutationOutputProcessList)
                    {
                        if (!(hostRoutine.ProcessList[procIndex] is PermutationOutputProcess))
                            throw new Exception("Type mismatch : Attempted to deserialize a normal process when an output process was expected!");
                        (ret as PermutationProcess).PermutationOutputProcesses.Add(hostRoutine.ProcessList[procIndex] as PermutationOutputProcess);
                    }
                }
            }

            return ret;
        }
    }
}
