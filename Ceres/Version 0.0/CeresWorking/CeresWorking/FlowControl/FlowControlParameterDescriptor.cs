﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public class FlowControlParameterDescriptor
    {
        public Type Type { get; set; }

        public object Tag { get; set; }
        public string Version { get; set; }
    }
}
