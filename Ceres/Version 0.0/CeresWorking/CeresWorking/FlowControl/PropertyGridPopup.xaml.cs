﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Interaction logic for PropertyGridPopup.xaml
    /// </summary>
    public partial class PropertyGridPopup : UserControl
    {
        public PropertyGridPopup()
        {
            InitializeComponent();
            
        }

        

        public Process Source
        {
            get { return (Process)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register(
                "Source",
                typeof(Process),
                typeof(PropertyGridPopup)
            );

        //Close Button
        private void Button_Click(object sender, RoutedEventArgs e)
        {           
            this.Visibility = Visibility.Hidden;
            this.Source.CheckForChangesAndNotify();
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            object gridObj = this.PropertyGrid.Instance;
            PropertyInfo[] infos = gridObj.GetType().GetProperties();

            foreach (PropertyInfo info in infos)
            {
                string propName = info.Name;
                object val = info.GetValue(gridObj, null);

                PropertyListing pl = PropertyListing.FindFirstByName(this.Source.PropertiesList, propName);
                
                
                if (pl.Value != val)
                {
                    pl.Value = val;
                    pl.UsingAdapterDefault = false;
                }
            }

            this.Visibility = Visibility.Hidden;
            this.Source.CheckForChangesAndNotify();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            this.Source.CheckForChangesAndNotify();
        }       
    }
}
