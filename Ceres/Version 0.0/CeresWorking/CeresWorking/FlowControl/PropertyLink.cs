﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.Interfaces;

namespace CeresBase.FlowControl
{
    public class PropertyLink : IConstructorProvider
    {
        public Process HostProcess { get; set; }

        public string PropertyName { get; set; }

        public PropertyLink(Process host, string property)
        {
            HostProcess = host;
            PropertyName = property;
        }

        #region IConstructorProvider Members

        public object[] ConstructionArgs
        {
            get { return new object[] { this.HostProcess, this.PropertyName }; }
        }

        #endregion
    }

    public class PropertyLinkSerializer
    {
        public int HostID { get; set; }
        public string PropertyName { get; set; }

        static public PropertyLinkSerializer Serialize(PropertyLink toSerialize, Routine hostRoutine)
        {
            PropertyLinkSerializer ret = new PropertyLinkSerializer();
            ret.PropertyName = toSerialize.PropertyName;
            ret.HostID = hostRoutine.ProcessList.IndexOf(toSerialize.HostProcess);

            return ret;
        }

        static public PropertyLink Deserialize(PropertyLinkSerializer toDeserialize, Routine hostRoutine)
        {
            Process targetProc = hostRoutine.ProcessList[toDeserialize.HostID];
            string targetProperty = toDeserialize.PropertyName;

            return new PropertyLink(targetProc, targetProperty);
        }
    }
}
