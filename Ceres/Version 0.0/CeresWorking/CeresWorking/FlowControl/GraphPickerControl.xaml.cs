﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Interaction logic for GraphPickerControl.xaml
    /// </summary>
    public partial class GraphPickerControl : UserControl
    {
        public GraphPickerControl()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(GraphPickerControl_Loaded);
        }

        void GraphPickerControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.ValueBox.SelectAll();
            this.ValueBox.Focus();
        }

        private string value = "0";
        public string Value { get { return value; } set { this.value = value;} }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            //System.Windows.Forms.DialogResult dr = System.Windows.Forms.DialogResult.
        }

        private void SimpleGraphDisplay_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
