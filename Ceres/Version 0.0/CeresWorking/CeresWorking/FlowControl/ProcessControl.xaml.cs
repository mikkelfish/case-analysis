﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using MesosoftCommon.ExtensionMethods;
using Deepforest.WPF.Controls;
using MesosoftCommon.Layout.BasicPanels;
using System.IO;
using System.Reflection;
using System.Windows.Resources;
using MesosoftCommon.Adorners;

namespace CeresBase.FlowControl
{
    public enum PropertyListStates
    {
        Full,
        Active,
        Collapsed
    }

    /// <summary>
    /// Interaction logic for RoutineControl.xaml
    /// </summary>
    public partial class ProcessControl : UserControl, INotifyPropertyChanged, IHasAProcessSource
    {
        private Process source;
        public Process Source 
        {
            get 
            {
                return source;
            }
            set
            {
                if (source != value)
                {
                    source = value;
                    OnPropertyChanged("Source");
                }
            } 
        }

        private bool outputToPanel = false;
        public bool OutputToPanel
        {
            get
            {
                return this.outputToPanel;
            }
            set
            {
                if (Source is PermutationProcess)
                    this.outputToPanel = false;
                else if (this.IsInAPermutationProcess && !(this.Source is PermutationOutputProcess))
                    this.outputToPanel = false;
                else if (this.Source.IsAStorageProcess)
                    this.outputToPanel = false;
                else
                    this.outputToPanel = value;
                RefreshOutputToPanel();
            }
        }

        public void RefreshOutputToPanel()
        {
            OnPropertyChanged("OutputToPanel");
            this.Source.OutputToPanel = this.OutputToPanel;
        }

        public bool IsInAPermutationProcess
        {
            get
            {
                object dragElementParent = VisualTreeHelper.GetParent(this);
                Panel parentManagedPanel = MesoGrid.FindParentManagedPanel(dragElementParent as DependencyObject);
                PermutationProcessControl permProc = null;

                return (((parentManagedPanel.Parent as FrameworkElement).Parent as FrameworkElement).Parent is PermutationProcessControl);

                //Not stored at the top level.
                //if (parentManagedPanel != this.DisplayPanel)
                //{
                //    permProc = ((parentManagedPanel.Parent as FrameworkElement).Parent as FrameworkElement).Parent as PermutationProcessControl;
                //    dragElementParent = permProc.PermutationChildrenPanel;
                //}
            }
        }

        private PropertyGridPopup storedPropertyGrid = new PropertyGridPopup();
        public PropertyListStates PropertyListState { get; set; }

        private bool isSelected = false;
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        private Cursor deleteCursor;
        //private Cursor deleteCursor = new Cursor(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Delete.cur"));
        public Cursor DeleteCursor
        {
            get
            {
                return deleteCursor;
            }
        }


        public ProcessControl()
        {
            InitializeComponent();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        private void test_MouseDown(object sender, MouseButtonEventArgs e)
        {
            int s = 0;
            e.Handled = true;
        }

        private void test_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            int z = 0;
            e.Handled = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int j = 0;
            e.Handled = true;
        }

        private void InputButtonClicked(object sender, RoutedEventArgs e)
        {
            ContentPresenter cp = FrameworkElements.FindVisualAncestor(e.Source as UIElement, typeof(ContentPresenter)) as ContentPresenter;
            RoutineManager rm = FrameworkElements.FindVisualAncestor(this, typeof(RoutineManager)) as RoutineManager;
            ProcessControl pc = FrameworkElements.FindVisualAncestor(e.Source as UIElement, typeof(ProcessControl)) as ProcessControl;

            if (rm.DeleteLinkModeActive)
            {
                rm.DeleteLinkAt(e.OriginalSource as UIElement, pc, true);
                return;
            }

            if(rm.ConnectModeActive)
                rm.EndVisualConnectAt(e.OriginalSource as UIElement, pc);
        }

        private void OutputButtonClicked(object sender, RoutedEventArgs e)
        {
            ContentPresenter cp = FrameworkElements.FindVisualAncestor(e.Source as UIElement, typeof(ContentPresenter)) as ContentPresenter;
            RoutineManager rm = FrameworkElements.FindVisualAncestor(this, typeof(RoutineManager)) as RoutineManager;
            ProcessControl pc = FrameworkElements.FindVisualAncestor(e.Source as UIElement, typeof(ProcessControl)) as ProcessControl;

            if (rm.DeleteLinkModeActive)
            {
                rm.DeleteLinkAt(e.OriginalSource as UIElement, pc, false);
                return;
            }

            rm.BeginVisualConnectFrom(e.OriginalSource as UIElement, pc);

        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            RoutineManager rm = FrameworkElements.FindVisualAncestor(this, typeof(RoutineManager)) as RoutineManager;

            if (rm.DeleteProcessModeActive)
            {
                //Not stored at the top level.
                if (!rm.DisplayPanel.Children.Contains(e.Source as ProcessControl))
                {
                    object dragElementParent = VisualTreeHelper.GetParent(e.Source as ProcessControl);
                    Panel parentManagedPanel = MesoGrid.FindParentManagedPanel(dragElementParent as DependencyObject);
                    PermutationProcessControl permProc = 
                        ((parentManagedPanel.Parent as FrameworkElement).Parent as FrameworkElement).Parent as PermutationProcessControl;
                    //dragElementParent = permProc.PermutationChildrenPanel;
                    if (permProc.ProcessPanel == (e.Source as ProcessControl))
                        rm.DeletePermutationProcess(permProc);
                    rm.DeleteProcess(e.Source as ProcessControl, permProc.PermutationChildrenPanel);
                }
                rm.DeleteProcess(e.Source as ProcessControl);
                e.Handled = true;
                return;
            }
            base.OnMouseDown(e);
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            //RoutineManager rm = FrameworkElements.FindVisualAncestor(this, typeof(RoutineManager)) as RoutineManager;

            //if (rm.DeleteProcessModeActive)
            //{
            //    rm.DeleteProcess(e.Source as ProcessControl);
            //    e.Handled = true;
            //    return;
            //}
            base.OnPreviewMouseDown(e);
        }

        //Edit
        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //xmlns:wpg="clr-namespace:Deepforest.WPF.Controls;assembly=WPGDemo"

            RoutineManager rm = FrameworkElements.FindVisualAncestor(this, typeof(RoutineManager)) as RoutineManager;
            ProcessToGridConverter ptgc = new ProcessToGridConverter();

            PropertyGridPopup popupToUse = null;

            foreach (UIElement element in rm.DisplayPanel.Children)
            {
                if (element is PropertyGridPopup)
                    popupToUse = element as PropertyGridPopup;                
            }

            if (popupToUse == null)
            {
                popupToUse = new PropertyGridPopup();
                //rm.DisplayPanel.Children.Add(popupToUse);
                AdornerLayer al = AdornerLayer.GetAdornerLayer(rm.DisplayPanel);
                UIElementAdorner UIEA = new UIElementAdorner(rm.DisplayPanel, popupToUse);

                Point adjustedTopLeftOffset = e.GetPosition(rm.DisplayPanel);

                //Center on the mouse by pushing the offset over and up
                adjustedTopLeftOffset.X -= (popupToUse.Width / 2.0);
                adjustedTopLeftOffset.Y -= (popupToUse.Height / 2.0);

                //Make sure the offset doesn't flow beyond the top/left edges of the displaypanel
                if (adjustedTopLeftOffset.Y < 0)
                    adjustedTopLeftOffset.Y = 0;
                if (adjustedTopLeftOffset.X < 0)
                    adjustedTopLeftOffset.X = 0;

                //Make sure the offset doesn't flow beyond the bottom/right edges of the displaypanel
                if (adjustedTopLeftOffset.Y + popupToUse.Height > rm.DisplayPanel.ActualHeight)
                    adjustedTopLeftOffset.Y = rm.DisplayPanel.ActualHeight - popupToUse.Height;
                if (adjustedTopLeftOffset.X + popupToUse.Width > rm.DisplayPanel.ActualWidth)
                    adjustedTopLeftOffset.X = rm.DisplayPanel.ActualWidth - popupToUse.Width;

                UIEA.OffsetLeft = adjustedTopLeftOffset.X;
                UIEA.OffsetTop = adjustedTopLeftOffset.Y;

                al.Add(UIEA);

            }

            popupToUse.Source = null;
            popupToUse.Source = this.Source;
            //MesoGrid.SetZIndex(popupToUse, 100);
            this.Source.StorePreEditValues();
            //popupToUse.Visibility = Visibility.Visible;
        }

        //
        private void CollectionViewSource_Filter_Output(object sender, FilterEventArgs e)
        {
            e.Accepted = false;

            PropertyListing pl = e.Item as PropertyListing;
            if (pl != null && pl.IsOutput)
                e.Accepted = true;
        }

        private void CollectionViewSource_Filter_Normal(object sender, FilterEventArgs e)
        {
            e.Accepted = false;

            PropertyListing pl = e.Item as PropertyListing;
            if (pl != null && !pl.IsOutput)
                e.Accepted = true;
        }

        private void DeleteGrabber_MouseDown(object sender, MouseButtonEventArgs e)
        {
            RoutineManager rm = FrameworkElements.FindVisualAncestor(this, typeof(RoutineManager)) as RoutineManager;

            e.Handled = false;

            if (rm.DeleteProcessModeActive)
            {
                rm.DeleteProcess(e.Source as ProcessControl);
                e.Handled = true;
                return;
            }
            //base.OnMouseDown(e);
        }

        private void DeleteGrabber_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            RoutineManager rm = FrameworkElements.FindVisualAncestor(this, typeof(RoutineManager)) as RoutineManager;

            e.Handled = false;

            if (rm.DeleteProcessModeActive)
            {
                rm.DeleteProcess(e.Source as ProcessControl);
                e.Handled = true;
                return;
            }
        }

        private void OutputToPanelToggleClicked(object sender, RoutedEventArgs e)
        {
            this.OutputToPanel = !this.OutputToPanel;
        }


    }
}
