﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Defines an interface for all adapters to make them compatible with the flow control engine.
    /// </summary>
    public interface IFlowControlAdapter : ICloneable
    {
        int ID { get; set; }

        Process HostProcess { get; set; }

        string Name { get; set; }
        string Category { get; set; }

        bool HasUserInteraction { get; }

        event EventHandler<FlowControlProcessEventArgs> RunComplete;

        string[] SupplyPropertyNames();

        object SupplyPropertyValue(string sourcePropertyName);

        void ReceivePropertyValue(object propertyValue, string targetPropertyName);

        bool CanReceivePropertyValue(object propertyValue, string targetPropertyName);

        FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName);

        FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName);

        object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType);

        void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType);

        Type PropertyType(string targetPropertyName);

        /// <summary>
        /// Run the adapter with runtime specific arguments
        /// </summary>
        /// <param name="args"></param>
        void RunAdapter();

        object[] GetValuesForSerialization();

        void LoadValuesFromSerialization(object[] values);

        bool CanEditProperty(string targetPropertyname);

        bool CanReceivePropertyLinks(string targetPropertyname);

        bool CanSendPropertyLinks(string targetPropertyname);

        bool IsOutputProperty(string targetPropertyname);

        bool ShouldSerialize(string targetPropertyname);

        ValidationStatus ValidateProperty(string targetPropertyname, object targetValue);

        object SupplyPropertyDescription(string propertyName);

        object SupplyAdapterDescription();
    }
}
