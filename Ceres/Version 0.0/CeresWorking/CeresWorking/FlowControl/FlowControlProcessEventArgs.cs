﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public class FlowControlProcessEventArgs : EventArgs
    {
        private object result;
        public object Result 
        {
            get
            {
                return result;
            }
        }

        public IFlowControlAdapter Adapter { get; private set; }

        public FlowControlProcessEventArgs(IFlowControlAdapter adapter, object result)
        {
            this.result = result;
            this.Adapter = adapter;
        }
    }
}
