﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows;

namespace CeresBase.FlowControl
{
    [ValueConversion(typeof(Point), typeof(string))]
    public class PointToDoubleStringConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return new object();
            if (!(value is Point)) return new object();

            Point pointValue = (Point)value;
            double convertValue = 0.0;

            if (parameter is string)
            {
                if ((parameter as string) == "x" || (parameter as string) == "X")
                    convertValue = pointValue.X;
                else if ((parameter as string) == "y" || (parameter as string) == "Y")
                    convertValue = pointValue.Y;
                else
                    convertValue = pointValue.X;
            }

            string ret = convertValue.ToString();
            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
