﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public interface IPermutationAdapter : IFlowControlAdapter
    {
        int IterationCount { get; }
        void SetIteration(int iteration);
    }
}
