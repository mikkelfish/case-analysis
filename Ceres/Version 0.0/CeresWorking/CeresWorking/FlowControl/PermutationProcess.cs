﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public class PermutationProcess : Process
    {
        public new IPermutationAdapter Adapter
        {
            get
            {
                return base.Adapter as IPermutationAdapter;
            }
        }

        public bool IsAnExpansionProcess { get; set; }

        public PermutationProcess()
        {
            this.permutationMemberProcesses.Add(new List<Process>());
            this.IsAnExpansionProcess = false;
        }

        public PermutationProcess(IPermutationAdapter newAdapter) : base(newAdapter)
        {
            this.permutationMemberProcesses.Add(new List<Process>());
            this.IsAnExpansionProcess = false;
        }

        private List<List<Process>> permutationMemberProcesses = new List<List<Process>>();
        public List<List<Process>> PermutationMemberProcesses
        {
            get
            {
                return permutationMemberProcesses;
            }
            set
            {
                permutationMemberProcesses = value;
            }
        }

        private List<PermutationOutputProcess> permutationOutputProcesses = new List<PermutationOutputProcess>();
        public List<PermutationOutputProcess> PermutationOutputProcesses
        {
            get
            {
                return permutationOutputProcesses;
            }
            set
            {
                permutationOutputProcesses = value;
            }
        }

        private List<PermutationProcess> mirroredSelves = new List<PermutationProcess>();
        public List<PermutationProcess> MirroredSelves
        {
            get
            {
                return mirroredSelves;
            }
            set
            {
                mirroredSelves = value;
            }
        }
    }
}
