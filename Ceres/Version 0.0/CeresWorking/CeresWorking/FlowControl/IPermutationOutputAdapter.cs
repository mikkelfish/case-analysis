﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public interface IPermutationOutputAdapter : IFlowControlAdapter
    {
        void ReceivePropertyValue(object propertyValue, string targetPropertyName, int forIteration);
    }
}
