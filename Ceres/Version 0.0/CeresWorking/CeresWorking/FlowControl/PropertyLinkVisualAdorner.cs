﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows;
using System.Windows.Media;

namespace CeresBase.FlowControl
{
    public class PropertyLinkVisualAdorner : Adorner
    {
        public List<PropertyLinkVisual> Source { get; set; }

        public PropertyLinkVisualAdorner(UIElement adornedElement, List<PropertyLinkVisual> sourceElem) 
            : base(adornedElement)
        {
            Source = sourceElem;
        }

        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            foreach (PropertyLinkVisual plv in Source)
            {
                if (plv.LineIsCollapsed) continue;

                Point beginPoint = plv.OriginProcessControl.TranslatePoint(new Point(0, 0), this.AdornedElement);
                Point endPoint = plv.DestinationProcessControl.TranslatePoint(new Point(0, 0), this.AdornedElement);
                beginPoint.Y += plv.OriginVisualOffset;
                endPoint.Y += plv.DestinationVisualoffset;
                beginPoint.X += (plv.OriginProcessControl.ActualWidth - PropertyLinkVisual.RightOffset);
                endPoint.X += PropertyLinkVisual.LeftOffset;

                Pen penLine = new Pen(new SolidColorBrush(Colors.Black), 1);
                drawingContext.DrawLine(penLine, beginPoint, endPoint);
            }
            base.OnRender(drawingContext);
        }
    }
}
