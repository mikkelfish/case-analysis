﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CeresBase.FlowControl.Adapters
{
    [AdapterDescription("Common")]
    public class StringTogetherAdapter : AssociateAdapterBase
    {
        public override string Name
        {
            get
            {
                return "Add Strings Together";
            }
            set
            {

            }
        }

        public override string Category
        {
            get
            {
                return "Runtime Tools";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("One")]
        public object One { get; set; }
        [AssociateWithProperty("Two")]
        public object Two { get; set; }
        [AssociateWithProperty("Three")]
        public object Three { get; set; }
        [AssociateWithProperty("Four")]
        public object Four { get; set; }
        [AssociateWithProperty("Five")]
        public object Five { get; set; }
        [AssociateWithProperty("Six")]
        public object Six { get; set; }
        [AssociateWithProperty("Seven")]
        public object Seven { get; set; }
        [AssociateWithProperty("Eight")]
        public object Eight { get; set; }
        [AssociateWithProperty("Nine")]
        public object Nine { get; set; }

        [AssociateWithProperty("Outputted String", DisableLink=true, ReadOnly=true, IsOutput=true)]
        public string OutputString { get; set; }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        private string getString(object o)
        {
            if (o == null) return "";

            if (o is ICollection)
            {
                string toRet = "";
                foreach (object obj in (o as ICollection))
                {
                    toRet += obj.ToString() + "/";
                }
                if (toRet.Length > 0)
                    toRet = toRet.Substring(0, toRet.Length - 1);

                return toRet;
            }

            return o.ToString();
        }

        public override void RunAdapter()
        {
            string toRet = "";
            if (this.One != null)
                toRet += this.getString(this.One) + " ";
            if (this.Two != null)
                toRet += this.getString(this.Two) + " ";
            if (this.Three != null)
                toRet += this.getString(this.Three) + " ";
            if (this.Four != null)
                toRet += this.getString(this.Four) + " ";
            if (this.Five != null)
                toRet += this.getString(this.Five) + " ";
            if (this.Six != null)
                toRet += this.getString(this.Six);
            toRet += this.getString(this.Seven);
            toRet += this.getString(this.Eight);
            toRet += this.getString(this.Nine);

            this.OutputString = toRet;
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
        }

        public override object Clone()
        {
            StringTogetherAdapter together = new StringTogetherAdapter();
            together.One = this.One;
            together.Two = this.Two;
            together.Three = this.Three;
            together.Four = this.Four;
            together.Five = this.Five;
            together.Six = this.Six;
            together.Seven = this.Seven;
            together.Eight = this.Eight;
            together.Nine = this.Nine;

            return together;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
