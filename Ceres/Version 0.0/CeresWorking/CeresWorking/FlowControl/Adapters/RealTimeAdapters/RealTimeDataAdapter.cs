﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.UI;

namespace CeresBase.FlowControl.Adapters.RealTimeAdapters
{
    [AdapterDescription("RealTime")]
    public class RealTimeDataAdapter : AssociateAdapterBase
    {
        public class RealTimeDataAdapterOutput
        {
            public float[] Data { get; set; }
            public double Resolution { get; set; }
        }

        [AssociateWithProperty("Data", ReadOnly=true, CanLinkFrom=false)]
        public float[] Data { get; set; }
        [AssociateWithProperty("Resolution", ReadOnly = true, CanLinkFrom = false)]
        public double Resolution { get; set; }


        [AssociateWithProperty("Output", IsOutput=true)]
        public RealTimeDataAdapterOutput Output { get; set; }

        public override string Category
        {
            get
            {
                return "Real Time Data Adapters";
            }
            set
            {
                
            }
        }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Data must be connected.");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }

        public override string Name
        {
            get
            {
                return "Real Time Output";
            }
            set
            {
                
            }
        }

        public override void RunAdapter()
        {
            this.Output = new RealTimeDataAdapterOutput() { Data = this.Data, Resolution = this.Resolution };
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            RealTimeDataAdapter adapt = new RealTimeDataAdapter();
            adapt.Data = this.Data;
            return adapt;
        }
    }
}
