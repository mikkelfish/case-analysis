﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl.Adapters.General
{
    [AdapterDescription("Common")]
    public class DataExtractor : AssociateAdapterBase
    {
        [AssociateWithProperty("Data", ReadOnly=true)]
        public float[] Data { get; set; }

        [AssociateWithProperty("Start Time")]
        public double StartTime { get; set; }

        [AssociateWithProperty("End Time")]
        public double EndTime { get; set; }

        [AssociateWithProperty("Resolution")]
        public double Resolution { get; set; }

        [AssociateWithProperty("Output", IsOutput=true)]
        public float[] Output { get; set; }

        public override string Name
        {
            get
            {
                return "Data Extractor";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Data Transforms";
            }
            set
            {
               
            }
        }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            int startIndex = (int)(this.StartTime * this.Resolution);
            int endIndex = (int)(this.EndTime * this.Resolution);
            if (endIndex >= this.Data.Length)
                endIndex = this.Data.Length - 1;

            float[] toRet = new float[endIndex - startIndex + 1];
            Array.Copy(this.Data, startIndex, toRet, 0, toRet.Length);
            this.Output = toRet;
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            DataExtractor extract = new DataExtractor();
            extract.StartTime = this.StartTime;
            extract.EndTime = this.EndTime;
            extract.Resolution = this.Resolution;
            return extract;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null) 
                return new ValidationStatus(ValidationState.Fail, "Connect to data adapter.");

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
