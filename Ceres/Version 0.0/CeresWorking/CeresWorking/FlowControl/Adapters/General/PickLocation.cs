﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CeresBase.FlowControl.Adapters.General
{
    [AdapterDescription("Common")]
    class PickLocation : AssociateAdapterBase
    {
        public override string Name
        {
            get
            {
                return "Pick Location";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "File Tools";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("Optional Addition")]
        public string OptionalAddition { get; set; }

        [AssociateWithProperty("Always Ask")]
        public bool AlwaysAsk { get; set; }

        [AssociateWithProperty("Output", IsOutput=true)]
        public string SelectedOutput { get; set; }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        private static string lastLoc = "";

        public override void RunAdapter()
        {
            if (PickLocation.lastLoc == "" || this.AlwaysAsk)
            {
                FolderBrowserDialog di = new FolderBrowserDialog();
                di.SelectedPath = PickLocation.lastLoc;
                if (di.ShowDialog() != DialogResult.Cancel)
                {
                    this.SelectedOutput = di.SelectedPath + '\\';



                    PickLocation.lastLoc = this.SelectedOutput;
                }
            }
            else this.SelectedOutput = PickLocation.lastLoc;

            if (this.OptionalAddition != null)
            {
                this.SelectedOutput += this.OptionalAddition.Replace("/", "_").Replace(":", "") + '\\';

            }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            return new PickLocation();
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return true; }
        }
    }
}
