﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl.Adapters.General
{
    [AdapterDescription("Common")]
    class CreateDifference : AssociateAdapterBase
    {
        public override string Name
        {
            get
            {
                return "Create Difference";
            }
            set
            {
               
            }
        }

        public override string Category
        {
            get
            {
                return "Basic Tools";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("Data")]
        public float[] Data { get; set; }

        [AssociateWithProperty("Tau")]
        public int Tau { get; set; }

        [AssociateWithProperty("Output", IsOutput=true)]
        public float[] Output { get; set; }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            if (this.Data != null)
            {
                this.Output = new float[this.Data.Length - this.Tau];
                for (int i = 0; i < this.Data.Length-this.Tau; i++)
                {
                    this.Output[i] = this.Data[i + this.Tau] - this.Data[i];
                }
            }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
           
        }

        public override object Clone()
        {
            return new CreateDifference() { Tau = this.Tau };
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Connect to data source.");
            }
            if (targetValue == "Tau" && targetValue is int && (int)targetValue <= 0)
            {
                return new ValidationStatus(ValidationState.Fail, "Tau must be larger than 0");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
