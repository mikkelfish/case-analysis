﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CeresBase.FlowControl.Adapters.General
{
    [AdapterDescription("Common")]
    class ExtractSeries : AssociateAdapterBase
    {

        [AssociateWithProperty("Data")]
        public IList Data { get; set; }

        [AssociateWithProperty("Information")]
        public IList Info { get; set; }


        [AssociateWithProperty("Number to Extract", ShouldSerialize = false, ReloadOnChange = true)]
        public int NumberOfOutputs { get; set; }



        public override string Name
        {
            get
            {
                return "Extract Series";
            }
            set
            {

            }
        }

        public override string Category
        {
            get
            {
                return "Selectors";
            }
            set
            {

            }
        }

        private List<object> selection = new List<object>();
        private List<object> outputs = new List<object>();

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            this.outputs.Clear();
            if (this.Info == null || this.Data == null) return;

          
            List<object> all = new List<object>();

            for (int i = 0; i < this.selection.Count; i++)
            {
                object found = this.stepIn(this.Info, this.Data, this.selection[i], all);

                if (found == null)
                {
                    string allStr = "";
                    foreach (object o in all)
                    {
                        allStr += o.ToString() + ";";
                    }
                    throw new Exception("Item " + this.selection[i] + " not found. Out of " + allStr);
                }

                this.outputs.Add(found);
            }
        }

        private object stepIn(IList info, IList data, object toCompare, List<object> all)
        {
            IEnumerator infoE = info.GetEnumerator();
            IEnumerator dataE = data.GetEnumerator();
            
            while (infoE.MoveNext() && dataE.MoveNext())
            {
                if (infoE.Current is IList)
                {
                    object search = stepIn(infoE.Current as IList, dataE.Current as IList, toCompare, all);
                    if (search != null) return search;
                }
                else
                {
                    if (!all.Contains(infoE.Current))
                        all.Add(infoE.Current);
                    if (infoE.Current.Equals(toCompare))
                        return dataE.Current;
                }

               

            }

            return null;
        }

        public override string[] SupplyPropertyNames()
        {
            List<string> names = new List<string>(base.SupplyPropertyNames());
            for (int i = 0; i < this.NumberOfOutputs; i++)
            {
                names.Add("Select #" + (i + 1).ToString());
                names.Add("Output #" + (i + 1).ToString());
            }

            return names.ToArray();
        }

        public override bool CanEditProperty(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Output #")) return false;
            if (targetPropertyname.Contains("Select #")) return true;
            return base.CanEditProperty(targetPropertyname);
        }

        public override bool CanReceivePropertyLinks(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Select #")) return true;
            if (targetPropertyname.Contains("Output #")) return false;
            return base.CanReceivePropertyLinks(targetPropertyname);
        }

        public override bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName.Contains("Select #")) return true;
            if (targetPropertyName.Contains("Output #")) return false;
            return base.CanReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override Type PropertyType(string targetPropertyName)
        {
            if (targetPropertyName.Contains("Select #")) return typeof(object);
            if (targetPropertyName.Contains("Output #")) return typeof(object);
            return base.PropertyType(targetPropertyName);
        }

        public override bool CanSendPropertyLinks(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Select #")) return true;
            if (targetPropertyname.Contains("Output #")) return true;
            return base.CanSendPropertyLinks(targetPropertyname);
        }

        public override bool IsOutputProperty(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Select #")) return false;
            if (targetPropertyname.Contains("Output #")) return true;
            return base.IsOutputProperty(targetPropertyname);
        }

        public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName.Contains("Select #"))
            {
                string inputNumberString = targetPropertyName.Substring("Select #".Length);
                int inputNumber = int.Parse(inputNumberString);

                if (this.selection.Count < inputNumber)
                {
                    int toAdd = inputNumber - this.selection.Count;
                    for (int i = 0; i < toAdd; i++)
                    {
                        this.selection.Add(new object());
                    }

                    this.selection[inputNumber - 1] = propertyValue;
                }
            }
            else base.ReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override object SupplyAdapterDescription()
        {
            return base.SupplyAdapterDescription();
        }


        public override object SupplyPropertyValue(string sourcePropertyName)
        {
            if (sourcePropertyName.Contains("Select #"))
            {
                string inputNumberString = sourcePropertyName.Substring("Select #".Length);
                int inputNumber = int.Parse(inputNumberString);
                if (this.selection.Count < inputNumber)
                    return null;
                return this.selection[inputNumber - 1];
            }
            else if (sourcePropertyName.Contains("Output #"))
            {
                string outputNumberString = sourcePropertyName.Substring("Output #".Length);
                int outputNumber = int.Parse(outputNumberString);
                if (this.outputs.Count < outputNumber)
                    return null;
                return this.outputs[outputNumber - 1];
            }

            return base.SupplyPropertyValue(sourcePropertyName);
        }


        public override bool ShouldSerialize(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Select #") || targetPropertyname.Contains("Output #"))
                return true;
            return base.ShouldSerialize(targetPropertyname);
            // return true;
        }


        public override object[] GetValuesForSerialization()
        {
            return new object[] { this.NumberOfOutputs };
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            this.NumberOfOutputs = (int)values[0];
        }

        public override object Clone()
        {
            ExtractSeries proc = new ExtractSeries();
            proc.NumberOfOutputs = this.NumberOfOutputs;
            proc.Info = this.Info;
            return proc;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname.Contains("Select #"))
            {
                string graphNumberString = targetPropertyname.Substring("Select #".Length);
                int graphNumber = int.Parse(graphNumberString);
                if (targetValue == null)
                    return new ValidationStatus(ValidationState.Fail, "Must be given a selection criteria.");
            }

            return new ValidationStatus(ValidationState.Pass);
        }



        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
