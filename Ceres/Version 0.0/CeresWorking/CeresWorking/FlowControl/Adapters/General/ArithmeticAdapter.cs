﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ikriv.Eval;

namespace CeresBase.FlowControl.Adapters.General
{
    [AdapterDescription("Common")]
    public class ArithmeticAdapter : AssociateAdapterBase
    {
        public override string Name
        {
            get
            {
                return "Eval Adapter";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Basic Adapters";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("Evaluation Code")]
        public string Fragment { get; set; }

        [AssociateWithProperty("Number of Inputs", ShouldSerialize = false, ReloadOnChange = true)]
        public int NumberOfInputs { get; set; }

        [AssociateWithProperty("Result", IsOutput=true)]
        public object Result { get; set; }

        private List<object> inputs = new List<object>();
        
        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            string toEval = this.Fragment;
            string result = "";
            int indexOf = 0;
            while (indexOf != -1)
            {
                int pastIndexOf = indexOf;
                indexOf = toEval.IndexOf('$', indexOf);



                if (indexOf == -1)
                {
                    result += toEval.Substring(pastIndexOf);
                    break;
                }

                result += toEval.Substring(pastIndexOf, indexOf - pastIndexOf);

                
                int secondIndex = toEval.IndexOf('$', indexOf + 1);
                if (secondIndex == -1)
                    throw new Exception("Invalid code in evaluation. Use $var#$ to specify variables, e.g. $var1$, $var2$, etc.");

                string var = toEval.Substring(indexOf + 1, secondIndex - indexOf - 1);
                var = var.Replace("var", "");

                int varNumber = -1;
                try
                {
                    varNumber = int.Parse(var);
                }
                catch
                {
                    throw new Exception("Invalid code in evaluation. Use $var#$ to specify variables, e.g. $var1$, $var2$, etc.");
                }

                result += this.inputs[varNumber - 1].ToString();

                indexOf = secondIndex + 1;
            }

            var compiler = new Compiler();
            this.Result = compiler.Eval(result);
        }

        public override string[] SupplyPropertyNames()
        {
            List<string> names = new List<string>(base.SupplyPropertyNames());
            for (int i = 0; i < this.NumberOfInputs; i++)
            {
                names.Add("Input #" + (i + 1).ToString());
            }

            return names.ToArray();
        }

        public override bool CanEditProperty(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Input #")) return true;
            return base.CanEditProperty(targetPropertyname);
        }

        public override bool CanReceivePropertyLinks(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Input #")) return true;
            return base.CanReceivePropertyLinks(targetPropertyname);
        }

        public override bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName.Contains("Input #")) return true;
            return base.CanReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override Type PropertyType(string targetPropertyName)
        {
            if (targetPropertyName.Contains("Input #")) return typeof(double);
            return base.PropertyType(targetPropertyName);
        }

        public override bool CanSendPropertyLinks(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Input #")) return true;
            return base.CanSendPropertyLinks(targetPropertyname);
        }

        public override bool IsOutputProperty(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Input #")) return false;
            return base.IsOutputProperty(targetPropertyname);
        }

        public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName.Contains("Input #"))
            {
                string inputNumberString = targetPropertyName.Substring("Input #".Length);
                int inputNumber = int.Parse(inputNumberString);

                if (this.inputs.Count < inputNumber)
                {
                    int toAdd = inputNumber - this.inputs.Count;
                    for (int i = 0; i < toAdd; i++)
                    {
                        this.inputs.Add(0);
                    }

                   
                }

                this.inputs[inputNumber - 1] = propertyValue;
            }
            else base.ReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override object SupplyAdapterDescription()
        {
            return base.SupplyAdapterDescription();
        }


        public override object SupplyPropertyValue(string sourcePropertyName)
        {
            if (sourcePropertyName.Contains("Input #"))
            {
                string inputNumberString = sourcePropertyName.Substring("Input #".Length);
                int inputNumber = int.Parse(inputNumberString);
                if (this.inputs.Count < inputNumber)
                    return null;
                return this.inputs[inputNumber - 1];
            }

            return base.SupplyPropertyValue(sourcePropertyName);
        }


        public override bool ShouldSerialize(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Input #"))
                return true;
            return base.ShouldSerialize(targetPropertyname);
            // return true;
        }


        public override object[] GetValuesForSerialization()
        {
            return new object[] { this.NumberOfInputs };
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            this.NumberOfInputs = (int)values[0];
        }

        public override object Clone()
        {
            ArithmeticAdapter proc = new ArithmeticAdapter();
            proc.NumberOfInputs = this.NumberOfInputs;
            return proc;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Evaluation Code" && targetValue is string)
            {
                string str = targetValue as string;
                if (str == null || str == string.Empty)
                    return new ValidationStatus(ValidationState.Fail, "Enter valid evaluation code");
            }
            return new ValidationStatus(ValidationState.Pass);
        }



        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
