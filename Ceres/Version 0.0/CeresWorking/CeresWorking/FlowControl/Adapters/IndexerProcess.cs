﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CeresBase.FlowControl.Adapters
{
    [AdapterDescription("Common")]
    class IndexerProcess : AssociateAdapterBase
    {
        [AssociateWithProperty("Number of Inputs", ShouldSerialize=false, ReloadOnChange=true)]
        public int NumberOfInputs { get; set; }

        [AssociateWithProperty("Index")]
        public int Index { get; set; }

        public override string Name
        {
            get
            {
                return "Indexer";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Selectors";
            }
            set
            {
                
            }
        }

        private List<List<object>> inputs = new List<List<object>>();
        private List<object> outputs = new List<object>();

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            this.outputs.Clear();
            for(int i = 0; i < this.inputs.Count; i++)
            {
                List<object> list = inputs[i];
                if (this.Index < list.Count)
                {
                    this.outputs.Add(list[this.Index]);
                }
                else this.outputs.Add(null);
            }
        }

        public override string[] SupplyPropertyNames()
        {
            List<string> names = new List<string>(base.SupplyPropertyNames());
            for (int i = 0; i < this.NumberOfInputs; i++)
            {
                names.Add("Input #" + (i + 1).ToString());
                names.Add("Output #" + (i + 1).ToString());
            }

            return names.ToArray();
        }

        public override bool CanEditProperty(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Input #") || targetPropertyname.Contains("Output #")) return false;
            return base.CanEditProperty(targetPropertyname);
        }

        public override bool CanReceivePropertyLinks(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Input #")) return true;
            if (targetPropertyname.Contains("Output #")) return false;
            return base.CanReceivePropertyLinks(targetPropertyname);
        }

        public override bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName.Contains("Input #")) return true;
            if (targetPropertyName.Contains("Output #")) return false;
            return base.CanReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override Type PropertyType(string targetPropertyName)
        {
            if (targetPropertyName.Contains("Input #")) return typeof(object);
            if (targetPropertyName.Contains("Output #")) return typeof(object);
            return base.PropertyType(targetPropertyName);
        }

        public override bool CanSendPropertyLinks(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Input #")) return true;
            if (targetPropertyname.Contains("Output #")) return true;
            return base.CanSendPropertyLinks(targetPropertyname);
        }

        public override bool IsOutputProperty(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Input #")) return false;
            if (targetPropertyname.Contains("Output #")) return true;
            return base.IsOutputProperty(targetPropertyname);
        }

        public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName.Contains("Input #"))
            {
                if (!(propertyValue is IList)) return;

                string inputNumberString = targetPropertyName.Substring("Input #".Length);
                int inputNumber = int.Parse(inputNumberString);

                if (this.inputs.Count < inputNumber)
                {
                    int toAdd = inputNumber - this.inputs.Count;
                    for (int i = 0; i < toAdd; i++)
                    {
                        this.inputs.Add(new List<object>());
                    }

                    foreach (object o in (propertyValue as IList))
                    {
                        this.inputs[inputNumber - 1].Add(o);

                    }
                }
            }
            else base.ReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override object SupplyAdapterDescription()
        {
            return base.SupplyAdapterDescription();
        }


        public override object SupplyPropertyValue(string sourcePropertyName)
        {
            if (sourcePropertyName.Contains("Input #"))
            {
                string inputNumberString = sourcePropertyName.Substring("Input #".Length);
                int inputNumber = int.Parse(inputNumberString);
                if (this.inputs.Count < inputNumber)
                    return null;
                return this.inputs[inputNumber - 1];
            }
            else if (sourcePropertyName.Contains("Output #"))
            {
                string outputNumberString = sourcePropertyName.Substring("Output #".Length);
                int outputNumber = int.Parse(outputNumberString);
                if (this.outputs.Count < outputNumber)
                    return null;
                return this.outputs[outputNumber - 1];
            }

            return base.SupplyPropertyValue(sourcePropertyName);
        }


        public override bool ShouldSerialize(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Input #") || targetPropertyname.Contains("Output #"))
                return true;
            return base.ShouldSerialize(targetPropertyname);
           // return true;
        }


        public override object[] GetValuesForSerialization()
        {
            return new object[] { this.NumberOfInputs };
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            this.NumberOfInputs = (int)values[0];
        }

        public override object Clone()
        {
            IndexerProcess proc = new IndexerProcess();
            proc.NumberOfInputs = this.NumberOfInputs;
            return proc;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname.Contains("Input #"))
            {
                string graphNumberString = targetPropertyname.Substring("Input #".Length);
                int graphNumber = int.Parse(graphNumberString);
                if (targetValue == null)
                    return new ValidationStatus(ValidationState.Fail, "Must be linked to graph input.");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
