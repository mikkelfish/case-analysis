﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using MesosoftCommon.Utilities.Settings;
using System.Collections;

namespace CeresBase.FlowControl.Adapters.Matlab
{
    public static class MatlabLoader
    {

        static MatlabLoader()
        {
            if (Application.Current == null)
                System.Windows.Forms.Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
            else
                Application.Current.Exit += new ExitEventHandler(Current_Exit);
        }

        static void Current_Exit(object sender, ExitEventArgs e)
        {
            if (matlabFunctions != null)
            {
                matlabFunctions.Dispose();
                matlabFunctions = null;
            }
        }

        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            if (matlabFunctions != null)
            {
                matlabFunctions.Dispose();
                matlabFunctions = null;
            }

            //if (evaluator != null)
            //{
            //    evaluator.Dispose();
            //    evaluator = null;
            //}
        }

        //private static AllMatlab.All matlabFunctionsNN;
        //public static AllMatlab.All MatlabFunctionsNotNative
        //{
        //    get
        //    {
        //        lock (MatlabLockable)
        //        {
        //            if (matlabFunctionsNN == null)
        //                matlabFunctionsNN = new AllMatlab.All();
        //            return matlabFunctionsNN;
        //        }
        //    }
        //}

        private static AllMatlabNative.All matlabFunctions;
        public static AllMatlabNative.All MatlabFunctions
        {
            get
            {
                lock (MatlabLockable)
                {
                    try
                    {
                        if (matlabFunctions == null)
                            matlabFunctions = new AllMatlabNative.All();
                        return matlabFunctions;
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Matlab failed to start. " + ex.Message + ". This is most likely due to an outdated version. Please go to the download server and download the new version of the Matlab runtime.");                        
                    }

                    return null;
                }
            }
        }

        private static object lockable = new object();
        public static object MatlabLockable { get { return lockable; } }


        //private static MatlabEvaluator.MatlabEvaluatorclass evaluator;
        //public static MatlabEvaluator.MatlabEvaluatorclass Evaluator 
        //{
        //    get
        //    {
        //        lock (MatlabLockable)
        //        {
        //            if (evaluator == null)
        //                evaluator = new MatlabEvaluator.MatlabEvaluatorclass();
        //            return evaluator;
        //        }
        //    }
        //}

        private static Dictionary<string, List<MatlabAdapterInfo>> knownAdapters = new Dictionary<string, List<MatlabAdapterInfo>>();
        private static Dictionary<string, List<string>> knownComponents = new Dictionary<string, List<string>>();

        public static MatlabAdapter GetAdapter(string category, string name)
        {
            lock (MatlabLockable)
            {
                MatlabAdapterInfo info = null;
                if (!knownAdapters.ContainsKey(category))
                {
                    BuildAdapterInfos(category);
                }

                info = knownAdapters[category].FirstOrDefault(s => s.InternalName == name);
                if (info == null)
                {
                    BuildAdapterInfos(category);
                    info = knownAdapters[category].FirstOrDefault(s => s.InternalName == name);

                }

                if (info == null) return null;
                return new MatlabAdapter(category, info.InternalName);
            }
        }

        public static MatlabAdapter[] CreateAdapters(string category)
        {
            lock (MatlabLockable)
            {
                if (!knownAdapters.ContainsKey(category))
                    return null;

                List<MatlabAdapter> toRet = new List<MatlabAdapter>();
                foreach (MatlabAdapterInfo info in knownAdapters[category])
                {
                    MatlabAdapter adapter = new MatlabAdapter(category, info.InternalName);
                    toRet.Add(adapter);
                }

                return toRet.ToArray();
            }
        }


        public static MatlabAdapterInfo GetAdapterInfo(string category, string name)
        {
            lock (MatlabLockable)
            {
                if (!knownAdapters.ContainsKey(category)) return null;
                return knownAdapters[category].SingleOrDefault(ad => ad.InternalName == name);
            }
        }

        public static string GetMatlabDirectory(string category)
        {
            string basedir = AppDomain.CurrentDomain.BaseDirectory;
            string matlabBase = basedir + "Matlab";
            string categoryBase = matlabBase + "\\" + category;

            if (!Directory.Exists(categoryBase))
            {
                Directory.CreateDirectory(categoryBase);
            }

            return categoryBase;
        }


        public static void BuildAdapterInfos(string category)
        {
            lock (MatlabLockable)
            {

                string categoryBase = GetMatlabDirectory(category);

                string[] files = Directory.GetFiles(categoryBase, "*.adapter");
                if (!knownAdapters.ContainsKey(category))
                {
                    knownAdapters.Add(category, new List<MatlabAdapterInfo>());
                }

                knownAdapters[category].Clear();
                foreach (string toParse in files)
                {
                    // string toParse = matlabFile.Replace(".m", ".adapter");

                    //if (!File.Exists(toParse))
                    //{
                    //    createParseFile(matlabFile, toParse, category);
                    //}
                    //else
                    //{
                    List<MatlabAdapterInfo> adapters = parseFile(toParse);
                    if (adapters != null)
                    {
                        foreach (MatlabAdapterInfo info in adapters)
                        {
                            info.RoutineCategory = category;
                        }

                        knownAdapters[category].AddRange(adapters);
                    }
                    //}
                }
            }
        }

        private static List<MatlabAdapterInfo> parseFile(string file)
        {
            if (!File.Exists(file)) return null;
            List<MatlabAdapterInfo> adapterList = new List<MatlabAdapterInfo>();

            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string toRead = reader.ReadToEnd();

                   // if (toRead.Contains("%% AUTOGENERATED")) return null;

                    string[] adapterSplits = toRead.Split(new string[] { "<MatlabAdapter" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string adapter in adapterSplits)
                    {
                        int inputsStart = adapter.IndexOf("<Inputs>");
                        int inputsEnd = adapter.IndexOf("</Inputs>");
                        if (inputsStart >= 0 && inputsEnd >= 0)
                        {
                            MatlabAdapterInfo adapterInfo = new MatlabAdapterInfo();

                            #region Get Adapter Properties
                            string adapterPropString = adapter.Substring(0, inputsStart - 1).Trim();
                            getProps(adapterInfo, adapterPropString);

                            #endregion

                            #region Get Inputs
                            string inputsString = adapter.Substring(inputsStart, inputsEnd - inputsStart).Trim();
                            string[] inputSplits = inputsString.Split(new string[] { "<Input" }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string inputStr in inputSplits)
                            {
                                int inputEnd = inputStr.IndexOf("/>");
                                if (inputEnd >= 0)
                                {
                                    MatlabInputInfo input = new MatlabInputInfo();

                                    string inputParaStr = inputStr.Substring(0, inputEnd).Trim();
                                    getProps(input, inputParaStr);

                                    adapterInfo.Inputs.Add(input);
                                }
                            }

                            #endregion

                            #region Get Outputs
                            string outputsString = adapter.Substring(inputsEnd);
                            int outPutsStart = outputsString.IndexOf("<Outputs>");
                            int outPutsEnd = outputsString.IndexOf("</Outputs>");
                            outputsString = outputsString.Substring(outPutsStart, outPutsEnd - outPutsStart).Trim();
                            string[] outputSplits = outputsString.Split(new string[] { "<Output" }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string outputStr in outputSplits)
                            {
                                int outputEnd = outputStr.IndexOf("/>");
                                if (outputEnd >= 0)
                                {
                                    MatlabOutputInfo output = new MatlabOutputInfo();
                                    string outputParaStr = outputStr.Substring(0, outputEnd).Trim();
                                    getProps(output, outputParaStr);
                                    adapterInfo.Outputs.Add(output);
                                }
                            }

                            #endregion

                            adapterInfo.MakeFinal();
                            adapterList.Add(adapterInfo);
                        }
                    }
                }
            }

            return adapterList;
        }

        private static void getProps(object toFill, string propString)
        {
            string[] paras = propString.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string para in paras)
            {
                string[] parts = para.Trim().Split(':');
                if (parts.Length == 2)
                {
                    PropertyInfo prop = toFill.GetType().GetProperty(parts[0].Trim());
                    if (prop != null)
                        prop.SetValue(toFill, parts[1].Trim(), null);
                }
            }
        }

    }

        #region Old Code Extracting

        //private static string[] keywords = new string[]{    "break",
        //        "case",
        //        "catch",
        //        "continue",
        //        "else",
        //        "elseif",
        //        "end",
        //        "for",
        //        "function",
        //        "global",
        //        "if",
        //        "otherwise",
        //        "persistent",
        //        "return",
        //        "switch",
        //        "try",
        //        "while",
        //        "nargin",
        //        "NaN",
        //        "j",
        //        "pi",
        //        "Inf"};

        //private static string convertLineToLevel(string line, int level, List<string> vars)
        //{
        //    string toRet = "";

        //    bool isVar = false;

        //    string current = "";
        //    for (int i = 0; i < line.Length; i++)
        //    {
        //        if (char.IsLetterOrDigit(line[i]) || line[i] == '_')
        //        {
        //            current += line[i];
        //            if (current.Length == 1 && char.IsLetter(current[0]))
        //                isVar = true;
        //        }
        //        else
        //        {
        //            if (current == "")
        //                toRet += line[i];
        //            else if (line[i] != '(' || vars.Contains(current))
        //            {
        //                bool toAdd = true;
        //                int startIndex = i - current.Length;
        //                int endIndex = i;
        //                if (startIndex - 1 >= 0 && endIndex < line.Length)
        //                {
        //                    if (line[i - current.Length - 1] == '\'' && line[i] == '\'')
        //                    {
        //                        toAdd = false;
        //                    }
        //                }



        //                if (isVar && !keywords.Contains(current) && toAdd)
        //                {

        //                    toRet += current + "lvl" + level;
        //                    if (!vars.Contains(current))
        //                        vars.Add(current);
        //                    toRet += line[i];
        //                    current = "";
        //                }
        //                else
        //                {
        //                    toRet += current;
        //                    toRet += line[i];
        //                    current = "";
        //                }
        //            }
        //            else
        //            {
        //                toRet += current;
        //                toRet += line[i];
        //                current = "";
        //            }

        //            isVar = false;
        //        }
        //    }

        //    if (current != "")
        //    {
        //        if (isVar && !keywords.Contains(current))
        //        {
        //            toRet += current + "lvl" + level;
        //        }
        //        else
        //        {
        //            toRet += current;
        //        }
        //    }

        //    return toRet;
        //}



        //private static string extractLine(string pline, string category, string[] possibleFunctions, int level, List<string> vars)
        //{
        //    int comIndex = pline.IndexOf('%');
        //    if (comIndex >= 0)
        //    {
        //        pline = pline.Substring(0, comIndex);
        //    }

        //    string line = convertLineToLevel(pline, level, vars);

        //    int index = line.IndexOf('(');
        //    if (index < 0) return line;

        //    int startIndex = 0;
        //    if (line.IndexOf('=') >= 0)
        //    {
        //        startIndex = line.IndexOf('=') + 1;
        //    }

        //    if (startIndex > index)
        //    {
        //        startIndex = 0;
        //    }

        //    if (pline.Contains("fcdf"))
        //    {
        //        int s = 0;
        //    }

        //    string functionName = line.Substring(startIndex, index - startIndex).Trim();

        //    int realStart = -1;
        //    for (int i = 0; i < functionName.Length; i++)
        //    {
        //        if (!char.IsLetterOrDigit(functionName[i]) && functionName[i] != '_')
        //            realStart = i;
        //    }

        //    if (realStart != -1)
        //    {
        //        functionName = functionName.Substring(realStart + 1);
        //        startIndex = realStart + 1;
        //    }

        //    if (!possibleFunctions.Contains(functionName) || startIndex == 0)
        //        return line;

        //    List<string> inputs = new List<string>();

        //    int lastIndex = line.IndexOf(')');
        //    string inputStr = line.Substring(index + 1, lastIndex - index - 1);
        //    string[] inputArgs = inputStr.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        //    string sourceFile = GetMatlabDirectory(category) + "\\" + functionName + ".m";

        //    string body = getBody(sourceFile, category, possibleFunctions, inputArgs, level + 1);

        //    string[][] inputOutput = getFunctionInputOutputs(sourceFile);
        //    //Copy over the storage variables
        //    string output = line.Substring(0, startIndex - 1);
        //    output = output.Replace('[', ' ');
        //    output = output.Replace(']', ' ');

        //    string[] outputSplits = output.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
        //    string toAdd = " ";
        //    for (int i = 0; i < inputOutput[1].Length; i++)
        //    {
        //        try
        //        {
        //            toAdd += outputSplits[i] + " = " + inputOutput[1][i] + "lvl" + (level + 1).ToString() + ";"; //"''" + outputSplits[i] + " = " + inputOutput[1][i] + ";'' " + "";
        //        }
        //        catch (Exception ex)
        //        {
        //            int s = 0;
        //        }
        //    }


        //    string toRet = body + toAdd; //"eval"  + "([ " + body + toAdd +  "]);";


        //    return toRet;

        //}

        //private static string getBody(string sourceFile, string category, string[] possibleFunctions, string[] inputs, int level)
        //{
        //    string[][] inputOutput = getFunctionInputOutputs(sourceFile);


        //    string quoteStr = "";
        //    string inputStr = "";
        //    // string bracketStr = "";
        //    //if (level > 0)
        //    //{
        //    //    quoteStr = "''";
        //    //   // inputStr = "[";

        //    //}

        //    //inputStr = bracketStr;

        //    for (int i = 0; i < inputs.Length; i++)
        //    {
        //        try
        //        {
        //            inputStr += quoteStr + inputOutput[0][i] + "lvl" + level + " = ";
        //            if (inputs[i].Contains("@"))
        //                inputStr += inputs[i].Substring(1);
        //            else inputStr += inputs[i];  //"['' num2str( " + inputs[i] + ") '']";

        //            inputStr += ";" + quoteStr + " ";
        //        }
        //        catch
        //        {
        //            int s = 0;
        //        }
        //    }


        //    string total = inputStr;
        //    string file = "";
        //    using (FileStream stream = new FileStream(sourceFile, FileMode.Open, FileAccess.Read))
        //    {
        //        using (StreamReader reader = new StreamReader(stream))
        //        {
        //            file = reader.ReadToEnd();
        //        }
        //    }

        //    List<string> vars = new List<string>();

        //    total += quoteStr;

        //    string[] lines = file.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
        //    foreach (string line in lines)
        //    {
        //        string trimmed = line.Trim();
        //        if (trimmed.Length == 0) continue;
        //        if (trimmed[0] == '%') continue;
        //        if (trimmed.IndexOf("function") == 0) continue;

        //        string exline = extractLine(trimmed, category, possibleFunctions, level, vars);
        //        if (exline[exline.Length - 1] != ';')
        //            exline += ";";

        //        total += exline;
        //    }

        //    total += quoteStr;

        //    //if (level > 0)
        //    //{
        //    //    total += "]";
        //    //}

        //    return total;


        //}

        //public static string ExtractCodeToRun(MatlabAdapterInfo info, object[] inputArgs)
        //{
        //    string[] possibleFunctionFiles = Directory.GetFiles(GetMatlabDirectory(info.RoutineCategory), "*.m");

        //    List<string> possibleFunctions = new List<string>();
        //    foreach (string funcFile in possibleFunctionFiles)
        //    {
        //        string function = funcFile.Substring(funcFile.LastIndexOf('\\') + 1);
        //        function = function.Substring(0, function.IndexOf(".m"));
        //        possibleFunctions.Add(function);

        //    }

        //    string sourceFile = GetMatlabDirectory(info.RoutineCategory) + "\\" + info.InternalName + ".m";


        //    List<string> parameters = new List<string>();
        //    #region Get Parameters
        //    for (int i = 0; i < info.Inputs.Count; i++)
        //    {
        //        //parameterString += info.Inputs[i].InternalName + " = ";

        //        string parameterString = "@";
        //        if (inputArgs[i] is ICollection)
        //        {
        //            parameterString += "[";
        //            char[] buffer = new char[5120000];
        //            int bufferSpot = 0;
        //            foreach (object o in (inputArgs[i] as ICollection))
        //            {
        //                //parameterString += o.ToString() + " ";
        //                if (o.ToString().Length + bufferSpot + 1 < buffer.Length)
        //                {
        //                    o.ToString().CopyTo(0, buffer, bufferSpot, o.ToString().Length);
        //                    bufferSpot += o.ToString().Length;
        //                    buffer[bufferSpot] = ' ';
        //                    bufferSpot++;
        //                }
        //                else
        //                {
        //                    parameterString += new string(buffer, 0, bufferSpot);
        //                    bufferSpot = 0;
        //                    o.ToString().CopyTo(0, buffer, bufferSpot, o.ToString().Length);
        //                    bufferSpot += o.ToString().Length;
        //                    buffer[bufferSpot] = ' ';
        //                    bufferSpot++;
        //                    GC.Collect();
        //                }
        //            }

        //            if (bufferSpot != 0)
        //            {
        //                parameterString += new string(buffer, 0, bufferSpot);
        //            }

        //            GC.Collect();


        //            string[] splits = parameterString.Split(' ');

        //            parameterString += "]";
        //        }
        //        else
        //        {
        //            parameterString += inputArgs[i].ToString();
        //        }

        //        parameters.Add(parameterString);
        //    }
        //    #endregion


        //    return getBody(sourceFile, info.RoutineCategory, possibleFunctions.ToArray(), parameters.ToArray(), 0);


        //}

        //private static string[][] getFunctionInputOutputs(string sourceFile)
        //{
        //    List<string> outputParams = new List<string>();
        //    List<string> inputParams = new List<string>();

        //    using (FileStream stream = new FileStream(sourceFile, FileMode.Open, FileAccess.Read))
        //    {
        //        using (StreamReader reader = new StreamReader(stream))
        //        {
        //            string firstLine = reader.ReadLine().Trim();
        //            while (firstLine != null)
        //            {
        //                if (firstLine.Length == 0 || firstLine[0] == '%')
        //                {
        //                    firstLine = reader.ReadLine().Trim();

        //                    continue;
        //                }
        //                if (!firstLine.Contains("function"))
        //                {
        //                    firstLine = reader.ReadLine().Trim();

        //                    continue;
        //                }

        //                firstLine = firstLine.Substring(firstLine.IndexOf("function") + "function".Length);

        //                if (firstLine.Contains('='))
        //                {
        //                    string[] split = firstLine.Split('=');
        //                    int index1 = split[1].IndexOf('(');
        //                    int index2 = split[1].IndexOf(')');

        //                    string input = split[1].Substring(index1 + 1, index2 - index1 - 1);
        //                    string[] inputs = input.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //                    for (int i = 0; i < inputs.Length; i++)
        //                    {
        //                        inputParams.Add(inputs[i].Trim());
        //                    }

        //                    int oindex1 = split[0].IndexOf('[');
        //                    int oindex2 = split[0].IndexOf(']');
        //                    if (oindex1 >= 0 && oindex2 >= 0)
        //                    {
        //                        string output = split[0].Substring(oindex1 + 1, oindex2 - oindex1 - 1);
        //                        string[] outputs = output.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
        //                        for (int i = 0; i < outputs.Length; i++)
        //                        {
        //                            outputParams.Add(outputs[i].Trim());
        //                        }
        //                    }
        //                    else
        //                    {
        //                        outputParams.Add(split[0].Trim());
        //                    }
        //                }
        //                else
        //                {
        //                    int index1 = firstLine.IndexOf('(');
        //                    int index2 = firstLine.IndexOf(')');

        //                    string input = firstLine.Substring(index1 + 1, index2 - index1);
        //                    string[] inputs = input.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //                    for (int i = 0; i < inputs.Length; i++)
        //                    {
        //                        inputParams.Add(inputs[i].Trim());
        //                    }
        //                }

        //                break;

        //            }
        //        }
        //    }

        //    return new string[][] { inputParams.ToArray(), outputParams.ToArray() };
        //}

        //private static void createParseFile(string sourceFile, string targetFile, string category)
        //{
        //    string functionName = targetFile.Substring(targetFile.LastIndexOf('\\') + 1);
        //    functionName = functionName.Substring(0, functionName.IndexOf('.'));

        //    string[][] inouts = getFunctionInputOutputs(sourceFile);
        //    string[] inputParams = inouts[0];
        //    string[] outputParams = inouts[1];

        //    string toWrite = "%% AUTOGENERATED, REMOVE THIS LINE WHEN FINISHED EDITING IN ORDER TO ACTIVATE %%" + '\n' +
        //            "<MatlabAdapter" + '\n' +
        //           '\t' + "InternalName: " + functionName + '\n' +
        //           '\t' + "DisplayName: " + functionName + '\n' +
        //            '\t' + "Description: " + "Enter Here" + '\n' +
        //            '\t' + "Category: " + "Matlab Adapters" + '\n' +
        //            '\t' + "<Inputs>" + '\n';


        //    foreach (string input in inputParams)
        //    {
        //        toWrite += "\t\t" + "<Input" + '\n' +
        //            '\t' + '\t' + '\t' + "InternalName: " + input + '\n' +
        //            '\t' + '\t' + '\t' + "DisplayName: " + input + '\n' +
        //            '\t' + '\t' + '\t' + "Description: " + "Enter Here" + '\n' +
        //            '\t' + '\t' + '\t' + "DefaultValue: " + "(text|integer|real|boolean), (value|array|array-array...)" + '\n' +
        //            '\t' + '\t' + '\t' + "Validation: " + "(NotNull, message)|(Comparison, (Equal, NotEqual, GreaterThan, LessThan, GreaterThanEqual, LessThanEqual), (value|Link-Name))" + '\n' +
        //            '\t' + '\t' + "/>" + '\n';
        //    }

        //    toWrite += "\t" + "</Inputs>" + '\n' +
        //        '\t' + "<Outputs>" + '\n';

        //    foreach (string output in outputParams)
        //    {
        //        toWrite +=
        //           "\t\t" + "<Output" + '\n' +
        //           '\t' + '\t' + '\t' + "Name: " + output + '\n' +
        //           '\t' + '\t' + '\t' + "TypeDescription: (Single, Array, DoubleArray)" + '\n' +
        //           '\t' + '\t' + "/>" + '\n';
        //    }

        //    toWrite += "\t" + "</Outputs>" + '\n' +
        //           "/>";


        //    using (FileStream stream = new FileStream(targetFile, FileMode.Create, FileAccess.Write))
        //    {
        //        using (StreamWriter writer = new StreamWriter(stream))
        //        {
        //            writer.Write(toWrite);
        //        }
        //    }
        //}


        #endregion 
}
