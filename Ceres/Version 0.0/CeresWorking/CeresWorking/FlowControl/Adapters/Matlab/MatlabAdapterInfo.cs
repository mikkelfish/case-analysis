﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using MesosoftCommon.Utilities.Validations;
using System.Reflection;
using System.ComponentModel;

namespace CeresBase.FlowControl.Adapters.Matlab
{
    public class MatlabAdapterInfo
    {
        
        public string InternalName { get; set; }
        public string DisplayName { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }

        public string RoutineCategory { get; set; }

        private List<MatlabInputInfo> inputs = new List<MatlabInputInfo>();
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<MatlabInputInfo> Inputs 
        {
            get
            {
                return this.inputs;
            }
        }

        private List<MatlabOutputInfo> outputs = new List<MatlabOutputInfo>();
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]        
        public List<MatlabOutputInfo> Outputs 
        {
            get
            {
                return this.outputs;
            }
        }


        public void MakeFinal()
        {
            foreach (MatlabInputInfo info in this.inputs)
            {
                info.MakeFinal();
            }

            foreach (MatlabOutputInfo info in this.outputs)
            {
                info.MakeFinal();
            }
        }
    }

    public class MatlabInputInfo
    {
        public string InternalName { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string DefaultValue { get; set; }
        public string Validation { get; set; }

        public ValidationRule ValidRule { get; private set; }
        public object Default { get; private set; }

        public void MakeFinal()
        {
            if (this.DefaultValue != null)
            {
                int[][] test = new int[0][];
                Type t = test.GetType();

                string[] split = this.DefaultValue.Split(',');
                string type = split[0].Trim();

                int numArrays = 0;
                if (split[1].Trim() == "array") numArrays = 1;
                else
                {
                    string[] arraySplit = split[1].Trim().Split('-');
                    for (int i = 0; i < arraySplit.Length; i++)
                    {
                        if (arraySplit[i].Trim() == "array")
                            numArrays++;
                    }
                }

                if (type.ToLower() == "text")
                {
                    if (numArrays == 0)
                        this.Default = split[1].Trim();
                }
                else if (type.ToLower() == "integer")
                {
                    if (numArrays == 0)
                    {
                        int val = 0;
                        bool complete = int.TryParse(split[1].Trim(), out val);
                        if (complete)
                        {
                            this.Default = val;
                        }
                    }
                }
                else if (type.ToLower() == "real")
                {
                    if (numArrays == 0)
                    {
                        double val = 0;
                        bool complete = double.TryParse(split[1].Trim(), out val);
                        if (complete)
                        {
                            this.Default = val;
                        }
                    }
                }
                else if (type.ToLower() == "boolean")
                {
                    if (numArrays == 0)
                    {
                        bool val = false;
                        bool complete = bool.TryParse(split[1].Trim(), out val);
                        if (complete)
                        {
                            this.Default = val;
                        }
                    }
                }
            }

            if (Validation != null)
            {
                if (this.Validation.Contains("NotNull"))
                {
                    NotNullValidator validator = new NotNullValidator();
                    if (this.Validation.Contains(','))
                    {
                        string split = this.Validation.Substring(this.Validation.IndexOf(',')+1);
                        validator.ExtraInfo = split.Trim();
                    }
                    this.ValidRule = validator;
                }
                else if (this.Validation.Contains("Comparison"))
                {
                    try
                    {
                        ComparisonValidator validator = new ComparisonValidator();
                        validator.TreatNonComparableAsSuccess = true;
                        string[] splits = this.Validation.Split(',');

                        validator.Operator = (ComparisonValidator.Comparison)Enum.Parse(typeof(ComparisonValidator.Comparison), splits[1], true);
                        if (splits[2].Contains('-'))
                        {
                            validator.CompareTo = "LINK&&" + splits[2].Substring(splits[2].IndexOf('-')+1).Trim();
                        }
                        else
                        {
                            if (this.Default is double)
                            {
                                double toCompare = 0;
                                if (double.TryParse(splits[2], out toCompare))
                                {
                                    validator.CompareTo = toCompare;
                                }
                            }
                            else if (this.Default is int)
                            {
                                int toCompare = 0;
                                if (int.TryParse(splits[2], out toCompare))
                                {
                                    validator.CompareTo = toCompare;
                                }
                            }
                        }

                        if (splits.Length == 4)
                        {
                            validator.Message = splits[3].Trim();
                        }

                        this.ValidRule = validator;
                    }
                    catch
                    {

                    }
                    
                }
            }
        }
    }

    public enum MatlabTypeDescription { Single, Array, DoubleArray };

    public class MatlabOutputInfo
    {
        public string TypeDescription { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public MatlabTypeDescription Typing { get; set; }

        public void MakeFinal()
        {
            try
            {
                this.TypeDescription = char.ToUpper(this.TypeDescription[0]) + this.TypeDescription.Substring(1);

                this.Typing = (MatlabTypeDescription)Enum.Parse(typeof(MatlabTypeDescription), this.TypeDescription);

                this.Name = this.Name.Replace(" ", "");
            }
            catch
            {

            }
        }

        public string Name { get; set; }
    }
}
