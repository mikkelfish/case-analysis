﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using MesosoftCommon.Utilities.Validations;
using System.Globalization;
using System.Reflection;
using System.Collections;

namespace CeresBase.FlowControl.Adapters.Matlab
{
    public class MatlabAdapter : IFlowControlAdapter
    {
        private class inputInstance
        {
            public MatlabInputInfo Info { get; private set; }
            public inputInstance(MatlabInputInfo info)
            {
                this.Info = info;
                this.CurrentValue = this.Info.Default;
            }

            public object CurrentValue { get; set; }
        }

        private class outputInstance
        {
            public MatlabOutputInfo Info { get; private set; }
            public outputInstance(MatlabOutputInfo info)
            {
                this.Info = info;
            }

            public object CurrentValue { get; set; }
        }

        private List<inputInstance> inputs = new List<inputInstance>();
        private List<outputInstance> outputs = new List<outputInstance>();

        #region IFlowControlAdapter Members

        public int ID
        {
            get;
            set;
        }

        public Process HostProcess
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Category
        {
            get;
            set;
        }

        public bool HasUserInteraction
        {
            get { return false; }
        }

        public MatlabAdapterInfo Info { get; private set; }

        private string matlabCategory { get; set; }
        private string matlabName { get; set; }

        private static Dictionary<string, object> callingObjects = new Dictionary<string, object>();

        public string[] GetInputs()
        {
            return this.inputs.Select(p => p.Info.DisplayName).ToArray();
        }

        public string[] GetOutputs()
        {
            return this.outputs.Select(p => p.Info.Name).ToArray();
        }

        public object[] Run(object[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                this.inputs[i].CurrentValue = args[i];
            }

            this.RunAdapter();

            return this.outputs.Select(p => p.CurrentValue).ToArray();
        }

        private void setInfo()
        {
            if (this.matlabName != null && this.matlabCategory != null)
            {
                this.Info = MatlabLoader.GetAdapterInfo(this.matlabCategory, this.matlabName);

                if (this.Info == null)
                {
                    MatlabLoader.BuildAdapterInfos(this.matlabCategory);
                    this.Info = MatlabLoader.GetAdapterInfo(this.matlabCategory, this.matlabName);
                }
                

                if (this.Info != null)
                {
                    foreach (MatlabInputInfo input in this.Info.Inputs)
                    {
                        if(!this.inputs.Any(i => i.Info.InternalName == input.InternalName))
                            this.inputs.Add(new inputInstance(input));
                    }

                    foreach (MatlabOutputInfo output in this.Info.Outputs)
                    {
                        if (!this.outputs.Any(i => i.Info.Name == output.Name))
                            this.outputs.Add(new outputInstance(output));
                    }

                    this.Name = this.Info.DisplayName;
                    this.Category = this.Info.Category;
                }
            }
        }

        public MatlabAdapter()
        {

        }

        public MatlabAdapter(string matlabCategory, string matlabName)
        {
            this.matlabCategory = matlabCategory;
            this.matlabName = matlabName;
            this.setInfo();
        }

        public event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public string[] SupplyPropertyNames()
        {
            if (this.Info == null) return null;

            List<string> names = new List<string>();
            names.AddRange(this.inputs.Select(p => p.Info.DisplayName).ToArray());
            names.AddRange(this.outputs.Select(p => p.Info.Name).ToArray());

            return names.ToArray();
        }

        public object SupplyPropertyValue(string sourcePropertyName)
        {
            if (this.Info == null) return null;
            inputInstance input = this.inputs.SingleOrDefault(p => p.Info.DisplayName == sourcePropertyName);
            if (input != null)
            {
                return input.CurrentValue;
            }

            outputInstance output = this.outputs.SingleOrDefault(p => p.Info.Name == sourcePropertyName);
            if (output != null)
            {
                return output.CurrentValue;
            }

            return null;
        }

        public void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (this.Info == null) return;
            inputInstance input = this.inputs.SingleOrDefault(p => p.Info.DisplayName == targetPropertyName);
            if (input != null)
            {
                input.CurrentValue = propertyValue;
            }

            outputInstance output = this.outputs.SingleOrDefault(p => p.Info.Name == targetPropertyName);
            if (output != null)
            {
                output.CurrentValue = propertyValue;
            }
        }

        public bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            return true;
        }

        public FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
        {
            throw new NotImplementedException();
        }

        public FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
        {
            throw new NotImplementedException();
        }

        public object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
        {
            throw new NotImplementedException();
        }

        public void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
        {
            throw new NotImplementedException();
        }

        public Type PropertyType(string targetPropertyName)
        {
            if (this.Info == null) return null;
            inputInstance input = this.inputs.SingleOrDefault(p => p.Info.DisplayName == targetPropertyName);
            if (input != null)
            {
                if (input.CurrentValue != null) return input.CurrentValue.GetType();
            }

            return null;
        }

        public void RunAdapter()
        {
            object[] args = this.inputs.Select(input => input.CurrentValue).ToArray();
            
            MethodInfo[] methods = MatlabLoader.MatlabFunctions.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public);
            MethodInfo theMethod = null;
            for (int i = 0; i < methods.Length; i++)
            {
                if (methods[i].Name != this.Info.InternalName) continue;

                ParameterInfo[] infos = methods[i].GetParameters();
                if (infos.Length == 0 || infos[0].ParameterType != typeof(int)) continue;
                if (infos.Length == args.Length + 1)
                    theMethod = methods[i];
            }

            if (theMethod == null) throw new Exception("Function " + this.Info.InternalName + " not found.");

            object[] fullArgs = new object[args.Length + 1];
            fullArgs[0] = this.outputs.Count;
            args.CopyTo(fullArgs, 1);

            for (int i = 0; i < fullArgs.Length; i++)
            {
                if (fullArgs[i] is float[])
                {
                    float[] ar = fullArgs[i] as float[];
                    double[] real = new double[ar.Length];
                    for (int j = 0; j < ar.Length; j++)
                    {
                        real[j] = ar[j];
                    }
                    fullArgs[i] = real;
                }
            }

            object[] outs = theMethod.Invoke(MatlabLoader.MatlabFunctions, fullArgs) as object[];

            //string parameterString = MatlabLoader.ExtractCodeToRun(this.Info, args);
            //string replacements = parameterString.Replace("'", "''");
            //replacements = "'" + replacements + "'";

            //MWArray[] ins = new MWArray[this.outputs.Count + 1];
            //MWArray[] outs = new MWArray[this.outputs.Count];

            ////try
            ////{

            //    ins[0] = new MWCharArray(parameterString);
            //    for (int i = 1; i < ins.Length; i++)
            //    {
            //        ins[i] = new MWCharArray(this.outputs[i - 1].Info.Name + "lvl0");
            //    }

            //    if (this.outputs.Count == 1)
            //    {
            //        MatlabLoader.Evaluator.MatlabEvaluator(this.outputs.Count, ref outs, ins);
            //    }
            //    else if (this.outputs.Count == 2)
            //    {
            //        MatlabLoader.Evaluator.MatlabEvaluator2(this.outputs.Count, ref outs, ins);

            //    }
            //    else if (this.outputs.Count == 3)
            //    {
            //        MatlabLoader.Evaluator.MatlabEvaluator3(this.outputs.Count, ref outs, ins);


            //    }
            //    else if (this.outputs.Count == 4)
            //    {
            //        MatlabLoader.Evaluator.MatlabEvaluator4(this.outputs.Count, ref outs, ins);

            //    }
            //    else if (this.outputs.Count == 5)
            //    {
            //        MatlabLoader.Evaluator.MatlabEvaluator5(this.outputs.Count, ref outs, ins);

            //    }
            //    else if (this.outputs.Count == 6)
            //    {
            //        MatlabLoader.Evaluator.MatlabEvaluator6(this.outputs.Count, ref outs, ins);

            //    }



            if (outs != null)
            {

                for (int i = 0; i < outs.Length; i++)
                {
                    if (outs[i].GetType() == typeof(char[,]))
                    {
                        #region To string
                        char[,] charArray = (outs[i]) as char[,];

                        if (outputs[i].Info.Typing == MatlabTypeDescription.Single)
                        {
                            char[] strArray = new char[charArray.GetLength(1)];
                            for (int j = 0; j < strArray.Length; j++)
                            {
                                strArray[j] = charArray[0, j];
                            }

                            outputs[i].CurrentValue = new string(strArray).Trim();
                        }
                        else
                        {
                            string[] toRet = new string[charArray.GetLength(0)];
                            for (int j = 0; j < charArray.GetLength(0); j++)
                            {
                                char[] strArray = new char[charArray.GetLength(1)];
                                for (int k = 0; k < charArray.GetLength(1); k++)
                                {
                                    strArray[k] = charArray[j, k];
                                }
                                toRet[j] = new string(strArray).Trim();
                            }
                        }
                        #endregion
                    }
                    else if (outs[i].GetType() == typeof(bool[,]))
                    {
                        #region To booleans
                        bool[,] array = (outs[i]) as bool[,];

                        if (outputs[i].Info.Typing == MatlabTypeDescription.Array)
                        {
                            bool[] toRet = new bool[array.GetLength(1)];
                            for (int j = 0; j < toRet.Length; j++)
                            {
                                toRet[j] = array[0, j];
                            }
                            outputs[i].CurrentValue = toRet;
                        }
                        else if (outputs[i].Info.Typing == MatlabTypeDescription.Single)
                        {
                            bool toRet = array[0, 0];
                            outputs[i].CurrentValue = toRet;
                        }
                        else
                        {
                            bool[][] toRet = new bool[array.GetLength(0)][];
                            for (int j = 0; j < toRet.Length; j++)
                            {
                                toRet[j] = new bool[array.GetLength(1)];
                                for (int k = 0; k < toRet[j].Length; k++)
                                {
                                    toRet[j][k] = array[j, k];
                                }
                            }
                            outputs[i].CurrentValue = toRet;
                        }
                        #endregion
                    }
                    else if (outs[i].GetType() == typeof(double[,]))
                    {
                        #region To doubles

                        double[,] array = (outs[i]) as double[,];

                        if (outputs[i].Info.Typing == MatlabTypeDescription.Array)
                        {
                            double[] toRet = new double[array.GetLength(1)];
                            for (int j = 0; j < toRet.Length; j++)
                            {
                                toRet[j] = array[0, j];
                            }
                            outputs[i].CurrentValue = toRet;
                        }
                        else if (outputs[i].Info.Typing == MatlabTypeDescription.Single)
                        {
                            double toRet = array[0, 0];
                            outputs[i].CurrentValue = toRet;
                        }
                        else
                        {
                            double[][] toRet = new double[array.GetLength(0)][];
                            for (int j = 0; j < toRet.Length; j++)
                            {
                                toRet[j] = new double[array.GetLength(1)];
                                for (int k = 0; k < toRet[j].Length; k++)
                                {
                                    toRet[j][k] = array[j, k];
                                }
                            }
                            outputs[i].CurrentValue = toRet;
                        }
                        #endregion
                    }


                }
            }

            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //finally
            //{
                //for (int i = 0; i < ins.Length; i++)
                //{
                //    ins[i].Dispose();
                //}

                //for (int i = 0; i < outs.Length; i++)
                //{
                //    (outs[i] as IDisposable).Dispose();
                //}

                
            //}

        }

        public object[] GetValuesForSerialization()
        {
            return new object[] { this.matlabCategory, this.matlabName };
        }

        public void LoadValuesFromSerialization(object[] values)
        {
            if (values != null && values.Length == 2)
            {
                this.matlabCategory = values[0] as string;
                this.matlabName = values[1] as string;
                this.setInfo();
            }
        }

        public bool CanEditProperty(string targetPropertyname)
        {
            if (this.Info == null) return false;
            inputInstance input = this.inputs.SingleOrDefault(p => p.Info.DisplayName == targetPropertyname);
            if (input != null)
            {
                return true;
            }

            return false;
        }

        public bool CanReceivePropertyLinks(string targetPropertyname)
        {
            if (this.Info == null) return false;
            inputInstance input = this.inputs.SingleOrDefault(p => p.Info.DisplayName == targetPropertyname);
            if (input != null)
            {
                return true;
            }

            return true;
        }

        public bool CanSendPropertyLinks(string targetPropertyname)
        {
            if (this.Info == null) return false;
            inputInstance input = this.inputs.SingleOrDefault(p => p.Info.DisplayName == targetPropertyname);
            if (input != null)
            {
                return true;
            }

            return true;
        }

        public bool IsOutputProperty(string targetPropertyname)
        {
            if (this.Info == null) return false;
            inputInstance input = this.inputs.SingleOrDefault(p => p.Info.DisplayName == targetPropertyname);
            if (input != null)
            {
                return false;
            }

            return true;
        }

        public bool ShouldSerialize(string targetPropertyname)
        {
            return true;
        }

        public ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (this.Info == null) return new ValidationStatus(ValidationState.Indeterminate); ;
            inputInstance input = this.inputs.SingleOrDefault(p => p.Info.DisplayName == targetPropertyname);
            if (input != null)
            {
                if (input.Info.ValidRule != null)
                {
                    if (input.Info.ValidRule is NotNullValidator)
                    {
                        NotNullValidator valid = input.Info.ValidRule as NotNullValidator;
                        ValidationResult res = valid.Validate(targetValue, CultureInfo.CurrentCulture);
                        if (res.IsValid)
                            return new ValidationStatus(ValidationState.Pass);
                        return new ValidationStatus(ValidationState.Fail, valid.ExtraInfo);
                    }
                    else if (input.Info.ValidRule is ComparisonValidator)
                    {
                        ComparisonValidator orig = input.Info.ValidRule as ComparisonValidator;

                        if (orig.CompareTo is string)
                        {
                            ComparisonValidator comparison = new ComparisonValidator();

                            comparison.CompareTo = orig.CompareTo;
                            comparison.Message = orig.Message;
                            comparison.Operator = orig.Operator;
                            comparison.Path = orig.Path;
                            comparison.TreatNonComparableAsSuccess = orig.TreatNonComparableAsSuccess;

                            string link = comparison.CompareTo as string;
                            link = link.Substring("LINK&&".Length);

                            inputInstance linkInput = this.inputs.SingleOrDefault(p => p.Info.InternalName == link);
                            if (linkInput != null)
                            {
                                comparison.CompareTo = linkInput.CurrentValue;
                                ValidationResult res = comparison.Validate(targetValue, CultureInfo.CurrentCulture);
                                if (res.IsValid)
                                    return new ValidationStatus(ValidationState.Pass);
                                return new ValidationStatus(ValidationState.Fail, comparison.Message);

                            }
                        }
                        else
                        {
                            ValidationResult res = orig.Validate(targetValue, CultureInfo.CurrentCulture);
                            if (res.IsValid)
                                return new ValidationStatus(ValidationState.Pass);
                            return new ValidationStatus(ValidationState.Fail, orig.Message);

                        }
                    }
                }
            }

            outputInstance output = this.outputs.SingleOrDefault(p => p.Info.Name == targetPropertyname);
            if (output != null)
            {
                return new ValidationStatus(ValidationState.Pass);
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            MatlabAdapter adapter = new MatlabAdapter(this.matlabCategory, this.matlabName);
            

            return adapter;
        }

        #endregion

        #region IFlowControlAdapter Members


        public object SupplyPropertyDescription(string propertyName)
        {
            if (this.Info == null) return null;

            inputInstance input = this.inputs.SingleOrDefault(p => p.Info.DisplayName == propertyName);
            if (input != null) return input.Info.Description;

            outputInstance output = this.outputs.SingleOrDefault(p => p.Info.Name == propertyName);
            if (output != null) return output.Info.Name;

            return null;
        }

        public object SupplyAdapterDescription()
        {
            if (this.Info == null) return null;
            return this.Info.Description;
        }

        #endregion
    }
}
