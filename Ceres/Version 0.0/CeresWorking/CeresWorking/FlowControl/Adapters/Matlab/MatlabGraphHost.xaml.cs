﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CeresBase.FlowControl.Adapters.Matlab
{
    /// <summary>
    /// Interaction logic for MatlabGraphHost.xaml
    /// </summary>
    public partial class MatlabGraphHost : UserControl
    {
        private static List<int> ids = new List<int>();
        private static object lockable = new object();

        public int ID { get; private set; }

        public MatlabGraphHost()
        {
            InitializeComponent();
        }

    //             0         0    1.0000
    //0.0313    0.0313    1.0000
    //0.0625    0.0625    1.0000
    //0.0938    0.0938    1.0000
    //0.1250    0.1250    1.0000
    //0.1563    0.1563    1.0000
    //0.1875    0.1875    1.0000
    //0.2188    0.2188    1.0000
    //0.2500    0.2500    1.0000
    //0.2813    0.2813    1.0000
    //0.3125    0.3125    1.0000
    //0.3438    0.3438    1.0000
    //0.3750    0.3750    1.0000
    //0.4063    0.4063    1.0000
    //0.4375    0.4375    1.0000
    //0.4688    0.4688    1.0000
    //0.5000    0.5000    1.0000
    //0.5313    0.5313    1.0000
    //0.5625    0.5625    1.0000
    //0.5938    0.5938    1.0000
    //0.6250    0.6250    1.0000
    //0.6563    0.6563    1.0000
    //0.6875    0.6875    1.0000
    //0.7188    0.7188    1.0000
    //0.7500    0.7500    1.0000
    //0.7813    0.7813    1.0000
    //0.8125    0.8125    1.0000
    //0.8438    0.8438    1.0000
    //0.8750    0.8750    1.0000
    //0.9063    0.9063    1.0000
    //0.9375    0.9375    1.0000
    //0.9688    0.9688    1.0000
    //1.0000    1.0000    1.0000
    //1.0000    0.9677    0.9677
    //1.0000    0.9355    0.9355
    //1.0000    0.9032    0.9032
    //1.0000    0.8710    0.8710
    //1.0000    0.8387    0.8387
    //1.0000    0.8065    0.8065
    //1.0000    0.7742    0.7742
    //1.0000    0.7419    0.7419
    //1.0000    0.7097    0.7097
    //1.0000    0.6774    0.6774
    //1.0000    0.6452    0.6452
    //1.0000    0.6129    0.6129
    //1.0000    0.5806    0.5806
    //1.0000    0.5484    0.5484
    //1.0000    0.5161    0.5161
    //1.0000    0.4839    0.4839
    //1.0000    0.4516    0.4516
    //1.0000    0.4194    0.4194
    //1.0000    0.3871    0.3871
    //1.0000    0.3548    0.3548
    //1.0000    0.3226    0.3226
    //1.0000    0.2903    0.2903
    //1.0000    0.2581    0.2581
    //1.0000    0.2258    0.2258
    //1.0000    0.1935    0.1935
    //1.0000    0.1613    0.1613
    //1.0000    0.1290    0.1290
    //1.0000    0.0968    0.0968
    //1.0000    0.0645    0.0645
    //1.0000    0.0323    0.0323
    //1.0000         0         0

        public object Colormap { get; set; }

        public double[] MajorAxis { get; set; }

        public double[] MinorAxis { get; set; }

        public double[][] Data { get; set; }


        private void generateDefaultColormap()
        {
            int count = 65;

            double[,] colormap = new double[count,3];
            
            double val = 0;
            double step = 1.0 / 33.0;

            for (int i = 0; i < count; i++)
            {
                if (i <= 31)
                {
                    colormap[i, 0] = val;
                    colormap[i, 1] = val;
                    colormap[i, 2] = 1.0;
                    val += step;
                }
                else if (i == 32)
                {
                    colormap[i, 0] = 1.0;
                    colormap[i, 1] = 1.0;
                    colormap[i, 2] = 1.0;
                }
                else
                {
                    colormap[i, 0] = 1.0;
                    colormap[i, 1] = val;
                    colormap[i, 2] = val;
                    val -= step;
                }
            }

            this.Colormap = colormap;

        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            this.ID = -1;

            this.generateDefaultColormap();

            this.Loaded += new RoutedEventHandler(MatlabGraphHost_Loaded);
            this.Unloaded += new RoutedEventHandler(MatlabGraphHost_Unloaded);
        }

        void MatlabGraphHost_Loaded(object sender, RoutedEventArgs e)
        {

            lock (lockable)
            {
                ids.Sort();
                int id = -1;
                for (int i = 0; i < ids.Count; i++)
                {
                    if (ids[i] != i)
                    {
                        id = i;
                        break;
                    }
                }

                if (id == -1) id = ids.Count;
                this.ID = id;
                ids.Add(id);
            }

            double[,] data = CeresBase.General.Utilities.DoubleArrayToSquare<double>(this.Data);



            if(this.MajorAxis != null && this.MinorAxis != null && this.Data != null)
                Matlab.MatlabLoader.MatlabFunctions.plotColor(this.MajorAxis, this.MinorAxis, data, this.ID + 1, this.Colormap);

        }


        void MatlabGraphHost_Unloaded(object sender, RoutedEventArgs e)
        {
            if (this.ID != -1)
            {
                lock (lockable)
                {
                    ids.Remove(this.ID);
                }
            }
        }


    }
}
