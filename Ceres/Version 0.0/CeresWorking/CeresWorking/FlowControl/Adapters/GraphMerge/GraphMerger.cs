﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.UI;

namespace CeresBase.FlowControl.Adapters.GraphMerge
{
    [AdapterDescription("Common")]
    class GraphMerger : AssociateAdapterBase
    {
        public override string Name
        {
            get
            {
                return "Graph Merger";
            }
            set
            {

            }
        }

        public override string Category
        {
            get
            {
                return "Graphing Operations";
            }
            set
            {

            }
        }

     


        public GraphMerger()
        {
            this.NumberOfGraphsToMerge = 2;
        }

        [AssociateWithProperty("Num Graphs", ShouldSerialize=false, ReloadOnChange=true)]
        public int NumberOfGraphsToMerge { get; set; }

        [AssociateWithProperty("Output Graph", IsOutput=true)]
        public GraphableOutput OutputGraph { get; set; }

        [AssociateWithProperty("Label")]
        public string Label { get; set; }

        private List<IGraphable> toGraph = new List<IGraphable>();


        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            GraphableOutput toRet = new GraphableOutput();

            int numXs = 0;
            int numSeries = 0;
            int maxYs = 0;

            List<string> labels = new List<string>();
            List<object> extraInfo = new List<object>();
            string label = this.Label;
            bool userLabel = !(label == null);
            if (!userLabel)
            {
                label = "";
            }

            for (int i = 0; i < toGraph.Count; i++)
            {
                if(toGraph[i].ExtraInfo != null)
                    extraInfo.AddRange(toGraph[i].ExtraInfo);

                if (!userLabel)
                {
                    label += toGraph[i].Label + " ";
                }

                if (numXs < toGraph[i].XData.Length)
                {
                    numXs = toGraph[i].XData.Length;
                }

                numSeries += toGraph[i].YData.Length;
                for (int j = 0; j < toGraph[i].YData.Length; j++)
                {
                    if(toGraph[i].SeriesLabels != null)
                      labels.Add(toGraph[i].SeriesLabels[j]);

                    if (maxYs < toGraph[i].YData[j].Length)
                    {
                        maxYs = toGraph[i].YData[j].Length;
                    }
                }
            }            

            double[] newXs = new double[numXs];
            double[][] newYs = new double[numSeries][];
            int curGraph = 0;
            for (int i = 0; i < toGraph.Count; i++)
            {
                for (int j = 0; j < toGraph[i].XData.Length; j++)
                {
                    newXs[j] = toGraph[i].XData[j];
                }

                for (int j = 0; j < toGraph[i].YData.Length; j++)
                {
                    newYs[curGraph] = new double[maxYs];
                    for (int k = 0; k < toGraph[i].YData[j].Length; k++)
                    {
                        newYs[curGraph][k] = toGraph[i].YData[j][k];
                    }
                    curGraph++;
                }
            }

            toRet.XData = newXs;
            toRet.YData = newYs;



            toRet.GraphType = toGraph[0].GraphType;
            toRet.SourceDescription = toGraph[0].SourceDescription;
            toRet.XLabel = toGraph[0].XLabel;
            toRet.XUnits = toGraph[0].XUnits;
            toRet.YLabel = toGraph[0].YLabel;
            toRet.YUnits = toGraph[0].YUnits;
            toRet.Label = label;

            this.OutputGraph = toRet;
        }

        public override string[] SupplyPropertyNames()
        {
            List<string> names = new List<string>(base.SupplyPropertyNames());
            for (int i = 0; i < this.NumberOfGraphsToMerge; i++)
            {
                names.Add("Graph #" + (i + 1).ToString());
            }

            return names.ToArray();
        }

        public override bool CanEditProperty(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Graph #")) return false;
            return base.CanEditProperty(targetPropertyname);
        }

        public override bool CanReceivePropertyLinks(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Graph #")) return true;
            return base.CanReceivePropertyLinks(targetPropertyname);
        }

        public override bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName.Contains("Graph #") && propertyValue is IGraphable) return true;          
            return base.CanReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override Type PropertyType(string targetPropertyName)
        {
            if (targetPropertyName.Contains("Graph #")) return typeof(IGraphable);
            return base.PropertyType(targetPropertyName);
        }

        public override bool CanSendPropertyLinks(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Graph #")) return true;
            return base.CanSendPropertyLinks(targetPropertyname);
        }

        public override bool IsOutputProperty(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Graph #")) return false;
            return base.IsOutputProperty(targetPropertyname);
        }

        public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName.Contains("Graph #"))
            {
                string graphNumberString = targetPropertyName.Substring("Graph #".Length);
                int graphNumber = int.Parse(graphNumberString);

                if (this.toGraph.Count < graphNumber)
                {
                    int toAdd = graphNumber - this.toGraph.Count;
                    for (int i = 0; i < toAdd; i++)
                    {
                        this.toGraph.Add(new GraphableOutput());
                    }

                    this.toGraph[graphNumber - 1] = propertyValue as IGraphable;
                }
            }
            else base.ReceivePropertyValue(propertyValue, targetPropertyName);
        }

        public override object SupplyAdapterDescription()
        {
            return base.SupplyAdapterDescription();
        }


        public override object SupplyPropertyValue(string sourcePropertyName)
        {
            if (sourcePropertyName.Contains("Graph #"))
            {
                string graphNumberString = sourcePropertyName.Substring("Graph #".Length);
                int graphNumber = int.Parse(graphNumberString);
                if (this.toGraph.Count < graphNumber)
                    return null;
                return this.toGraph[graphNumber - 1];
            }
            
            return base.SupplyPropertyValue(sourcePropertyName);
        }


        public override bool ShouldSerialize(string targetPropertyname)
        {
            if (targetPropertyname.Contains("Graph #"))
                return true;
            return base.ShouldSerialize(targetPropertyname);
        }

        public override object[] GetValuesForSerialization()
        {
            return new object[] { this.NumberOfGraphsToMerge };
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            this.NumberOfGraphsToMerge = (int)values[0];
        }

        public override object Clone()
        {
            GraphMerger merger = new GraphMerger();
            merger.NumberOfGraphsToMerge = this.NumberOfGraphsToMerge;
            return merger;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname.Contains("Graph #"))
            {
                string graphNumberString = targetPropertyname.Substring("Graph #".Length);
                int graphNumber = int.Parse(graphNumberString);
                if (targetValue == null)
                    return new ValidationStatus(ValidationState.Fail, "Must be linked to graph input.");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
