﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.UI;

namespace CeresBase.FlowControl.Adapters.GraphMerge
{
    [AdapterDescription("Common")]
    public class GraphCreator : AssociateAdapterBase
    {
        public override string Name
        {
            get
            {
                return "Create Graph";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Graphing Operations";
            }
            set
            {
                
            }
        }

        public GraphCreator()
        {
            this.Label = "Created Graph";
        }

        [AssociateWithProperty("Label")]
        public string Label { get; set; }
 
        [AssociateWithProperty("XData", ReadOnly = true)]
        public double[] XData { get; set; }

        [AssociateWithProperty("YData", ReadOnly=true)]
        public double[][] YData { get; set; }

        [AssociateWithProperty("Graph", IsOutput=true)]
        public GraphableOutput Graph { get; set; }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            if (this.XData == null)
            {
                this.XData = new double[this.YData[0].Length];
                for (int i = 0; i < this.XData.Length; i++)
                {
                    this.XData[i] = i + 1;
                }
            }

            this.Graph = new GraphableOutput() { Label = this.Label, XData = this.XData, YData = this.YData };
        }

        public override void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            if (targetPropertyName == "YData" && propertyValue is double[])
            {
                this.YData = new double[][] { propertyValue as double[] };
            }
            else
            {
                base.ReceivePropertyValue(propertyValue, targetPropertyName);
            }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            GraphCreator creator = new GraphCreator();
            creator.Label = this.Label;
            return creator;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            //if (targetPropertyname == "XData" && targetValue == null)
            //    return new ValidationStatus(ValidationState.Fail);

            if (targetPropertyname == "YData" && targetValue == null)
                return new ValidationStatus(ValidationState.Fail);

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
