﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace CeresBase.FlowControl.Adapters
{
    [AdapterDescription("Common")]
    public class RangeAdapter : AssociateAdapterBase
    {
        #region IFlowControlAdapter Members

        public override string Name
        {
            get
            {
                return "Range Creator";
            }
            set
            {

            }
        }

        public override string Category
        {
            get
            {
                return "Runtime Tools";
            }
            set
            {
                
            }
        }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;
        
        [AssociateWithPropertyAttribute("Base Number")]
        public double Base { get; set; }

        [AssociateWithPropertyAttribute("Subtract Amount")]
        public double SubtractAmount { get; set; }

        [AssociateWithPropertyAttribute("Min Number")]
        public double MinNumber { get; set; }

        [AssociateWithPropertyAttribute("Add Amount")]
        public double AddAmount { get; set; }

        [AssociateWithPropertyAttribute("Max Number")]
        public double MaxNumber { get; set; }

        [AssociateWithPropertyAttribute("Subtract Result", ReadOnly = true, DisableLink = true, IsOutput=true)]
        public double SubtractResult { get; private set; }

        [AssociateWithPropertyAttribute("Add Result", ReadOnly = true, DisableLink=true, IsOutput=true)]
        public double AddResult { get; private set; }


        
        public RangeAdapter()
        {
            this.MinNumber = double.MinValue;
            this.MaxNumber = double.MaxValue;
        }


        public override void RunAdapter()
        {
            this.SubtractResult = this.Base - this.SubtractAmount;
            if (this.SubtractResult < this.MinNumber)
            {
                this.SubtractResult = this.MinNumber;
            }

            this.AddResult = this.Base + this.AddAmount;
            if (this.AddAmount > this.MaxNumber)
            {
                this.AddResult = this.MaxNumber;
            }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }


        #endregion

        #region ICloneable Members

        public override object Clone()
        {
            return new RangeAdapter();
        }

        #endregion

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (this.Base < this.MinNumber) return new ValidationStatus(ValidationState.Fail);
            if (this.Base > this.MaxNumber) return new ValidationStatus(ValidationState.Fail);
            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
