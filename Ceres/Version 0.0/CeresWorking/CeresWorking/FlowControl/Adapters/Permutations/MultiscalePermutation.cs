﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl.Adapters.Permutations
{
    [AdapterDescription("Common")]
    public class MultiscalePermutation: AssociateAdapterBase, IPermutationAdapter
    {
        [AssociateWithProperty("Data")]
        public float[] Data { get; set; }

        [AssociateWithProperty("Number Steps")]
        public int NumSteps { get; set; }

        [AssociateWithProperty("Averaged Data", IsOutput=true)]
        public float[] AveragedData { get; set; }

        [AssociateWithProperty("Iteration", IsOutput=true)]
        public int Iteration { get; set; }

        public override string Name
        {
            get
            {
                return "Multiscale Permutation";
            }
            set
            {
               
            }
        }

        public override string Category
        {
            get
            {
                return "Common Permutations";
            }
            set
            {
               
            }
        }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            int divideBy = this.iteration + 1;
            int numPoints = this.Data.Length / divideBy;

            float[] toOut = new float[numPoints];
            for (int i = 0; i < numPoints; i++)
            {
                int index = i * divideBy;
                float avg = 0;
                for (int j = 0; j < divideBy; j++)
                {
                    avg += this.Data[index + j];
                }

                avg /= divideBy;
                toOut[i] = avg;
            }

            this.AveragedData = toOut;
            this.Iteration = this.iteration;

        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            MultiscalePermutation perm = new MultiscalePermutation();
            perm.NumSteps = this.NumSteps;
            return perm;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Connect to data.");
            }

            if (targetPropertyname == "NumSteps" && targetValue is int && (int)targetValue <= 0)
            {
                return new ValidationStatus(ValidationState.Fail, "Must be greater than 0.");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }

        #region IPermutationAdapter Members

        public int IterationCount
        {
            get { return this.NumSteps; }
        }

        private int iteration;

        public void SetIteration(int iteration)
        {
            this.iteration = iteration;
        }

        #endregion
    }
}
