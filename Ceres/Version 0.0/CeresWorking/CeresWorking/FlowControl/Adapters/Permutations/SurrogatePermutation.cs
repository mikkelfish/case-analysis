﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters.Matlab;

namespace CeresBase.FlowControl.Adapters.Permutations
{
    [AdapterDescription("Common")]
    public class SurrogatePermutation : AssociateAdapterBase, IPermutationAdapter
    {

        public SurrogatePermutation()
        {
            this.CutoffIndex = -1;
        }

        public override string Name
        {
            get
            {
                return "Surrogate Permutation";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Common Permutations";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("Data", CanLinkFrom=false, ReadOnly=true)]
        public float[] Data { get; set; }

        [AssociateWithProperty("Number of Surrogates")]
        public int NumberSurrogates { get; set; }

        [AssociateWithProperty("Surrogate Data", IsOutput=true)]
        public float[] SurrogateData { get; set; }

        [AssociateWithProperty("Surrogate Number", IsOutput=true)]
        public int SurrogateNumber { get; set; }

        [AssociateWithProperty("Use Old Way")]
        public bool OldMethod { get; set; }

        [AssociateWithProperty("Cutoff")]
        public int CutoffIndex { get; set; }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        private int iteration = 0;

        //private static object lockable = new object();

        public static float[] CreateSurrogate(float[] data)
        {
            return CreateSurrogate(data, float.NaN, false);
        }

        public static float[] CreateSurrogate(float[] data, float cutOff, bool old)
        {
            //string toFind = "myss";
            //if (cutOff != float.NaN)
            //    toFind = "runSurrogate";

            //MatlabAdapter[] surrm = MatlabLoader.CreateAdapters("Internal");
            //MatlabAdapter surra = MatlabLoader.GetAdapter("Internal", toFind);

            //if (surra == null) return null;
            AllMatlabNative.All matlab = Matlab.MatlabLoader.MatlabFunctions;


            lock (Matlab.MatlabLoader.MatlabLockable)
            {

                if (!old)
                {
                    object[] surrogates = null;

                    if (float.IsNaN(cutOff))
                    {
                        surrogates = matlab.myss(1, data, 5000);

                       // surrogates = surra.Run(new object[] { data, 5000 });
                    }
                    else
                    {
                        surrogates = matlab.runSurrogate(1, data, cutOff, 5000);
                       // surrogates = surra.Run(new object[] { data, cutOff, 5000 });
                    }

                    double[,] surr = surrogates[0] as double[,];
                    float[] realDataSur = new float[surr.GetLength(1)];
                    for (int j = 0; j < realDataSur.Length; j++)
                    {
                        realDataSur[j] = Convert.ToSingle(surr[0, j]);
                    }

                    return realDataSur;
                }
                else
                {
                    object[] ret = matlab.ItSurrDat(2, data, 5000);


                    //MWNumericArray dataArray = new MWNumericArray(this.Data);
                    //MWNumericArray numIters = new MWNumericArray(5000);

                    //MWArray[] surrogates = matlab.ItSurrDat(2, dataArray, numIters);
                    //MWNumericArray array = surrogates[0] as MWNumericArray;
                    // double[,] doubleVals = array.ToArray(MWArrayComponent.Real) as double[,];
                    double[,] doubleVals = ret[0] as double[,];

                    float[] realDataSur = new float[doubleVals.GetLength(1)];
                    for (int j = 0; j < realDataSur.Length; j++)
                    {
                        realDataSur[j] = Convert.ToSingle(doubleVals[0, j]);
                    }

                    return realDataSur;

                    //dataArray.Dispose();
                    //numIters.Dispose();
                }
            }
        }

        public override void RunAdapter()
        {
            if (this.Data == null) return;

            float cut = float.NaN;
            if (this.CutoffIndex >= 0 && this.CutoffIndex < this.Data.Length - 1)
                cut = this.Data[this.CutoffIndex];

            this.SurrogateData = CreateSurrogate(this.Data, cut, this.OldMethod);
            this.SurrogateNumber = this.iteration;
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            SurrogatePermutation perm = new SurrogatePermutation();
            perm.NumberSurrogates = this.NumberSurrogates;
            return perm;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Connect to data source.");
            }

            if (targetPropertyname == "Number of Surrogates")
            {
                int val = (int)targetValue;
                if (val <= 0)
                    return new ValidationStatus(ValidationState.Fail, "Number of surrogates must be greater than 0.");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }

        #region IPermutationAdapter Members

        public int IterationCount
        {
            get { return this.NumberSurrogates; }
        }

        public void SetIteration(int iteration)
        {
            this.iteration = iteration;
        }

        #endregion
    }
}
