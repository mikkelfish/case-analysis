﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CeresBase.FlowControl.Adapters.Permutations
{
    [AdapterDescription("Common")]    
    public class ForeachPermutationAdapter : AssociateAdapterBase, IPermutationAdapter
    {
        public override string Name
        {
            get
            {
                return "Foreach Permutation";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Common Permutations";
            }
            set
            {
                
            }
        }

        [AssociateWithProperty("Input", CanLinkFrom=false)]
        public IList Input { get; set; }

        [AssociateWithProperty("Output", IsOutput=true)]
        public object Output { get; set; }

        [AssociateWithProperty("Output Index", IsOutput = true)]
        public int OutputIndex { get; set; }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            if (this.Input != null && this.Input.Count > this.curIteration)
            {
                this.Output = this.Input[this.curIteration];
                this.OutputIndex = this.curIteration;
            }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            ForeachPermutationAdapter toRet = new ForeachPermutationAdapter();
            toRet.Input = this.Input;
            return toRet;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Input" && targetValue == null)
                return new ValidationStatus(ValidationState.Fail, "Input needs to be linked.");
            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }

        #region IPermutationAdapter Members

        public int IterationCount
        {
            get 
            {
                if (this.Input != null)
                    return this.Input.Count;
                return 0;
            }
        }

        private int curIteration = 0;
        public void SetIteration(int iteration)
        {
            this.curIteration = iteration;
        }

        #endregion
    }
}
