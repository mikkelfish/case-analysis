﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.UI;

namespace CeresBase.FlowControl.Adapters.Permutations.Output
{
    [AdapterDescription("Common")]
    public class MeanSDPermutationOutput : AssociateAdapterBase, IPermutationOutputAdapter
    {
        public override string Name
        {
            get
            {
                return "Mean & Std. Output";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Permutation Output";
            }
            set
            {
                
            }
        }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        [AssociateWithProperty("Means", IsOutput = true)]
        public object Means { get; set; }

        [AssociateWithProperty("Standard Deviations", IsOutput = true)]
        public object Sds { get; set; }

        [AssociateWithProperty("Input", CanLinkFrom = false)]
        public object Input { get; set; }

        private List<object> output = new List<object>();

        public override void RunAdapter()
        {
            if (output.Count == 0) return;

            if (output.All(o => o is GraphableOutput))
            {
                int series = (output[0] as GraphableOutput).YData.Length;
                int[] seriesCount = new int[series];

                double[][] means = new double[series][];
                double[][] sds = new double[series][];
                for (int i = 0; i < series; i++)
                {
                    seriesCount[i] = (output[0] as GraphableOutput).YData[i].Length;
                    for (int j = 0; j < output.Count; j++)
                    {
                        if ((output[j] as GraphableOutput).YData.Length != series ||
                            (output[j] as GraphableOutput).YData[i].Length != seriesCount[i])
                            return;
                    }

                    means[i] = new double[seriesCount[i]];
                    sds[i] = new double[seriesCount[i]];

                    for (int j = 0; j < seriesCount[i]; j++)
                    {
                        double[] thisSeries = new double[output.Count];
                        for (int k = 0; k < output.Count; k++)
                        {
                            thisSeries[k] = (output[k] as GraphableOutput).YData[i][j];
                        }

                        double mean = CeresBase.MathConstructs.Stats.Average(thisSeries);
                        double sd = CeresBase.MathConstructs.Stats.StandardDev(thisSeries);

                        means[i][j] = mean;
                        sds[i][j] = sd; 
                    }
                }

                GraphableOutput toOutputMeans = new GraphableOutput();
                toOutputMeans.YData = means;
                
                
                GraphableOutput first = output[0] as GraphableOutput;
                toOutputMeans.GraphType = first.GraphType;
                toOutputMeans.Label = first.Label;
                toOutputMeans.SeriesLabels = first.SeriesLabels;
                toOutputMeans.SourceDescription = first.SourceDescription + " Means";
                toOutputMeans.XData = first.XData;
                toOutputMeans.XLabel = first.XLabel;
                toOutputMeans.XUnits = first.XUnits;
                toOutputMeans.YLabel = first.YLabel;
                toOutputMeans.YUnits = first.YUnits;
                toOutputMeans.ExtraInfo = first.ExtraInfo;

                GraphableOutput toOutputSds = new GraphableOutput();
                toOutputSds.YData = sds;
                toOutputSds.GraphType = first.GraphType;
                toOutputSds.Label = first.Label;
                toOutputSds.SeriesLabels = first.SeriesLabels;
                toOutputSds.SourceDescription = first.SourceDescription + " Standard Deviation";
                toOutputSds.XData = first.XData;
                toOutputSds.XLabel = first.XLabel;
                toOutputSds.XUnits = first.XUnits;
                toOutputSds.YLabel = first.YLabel;
                toOutputSds.YUnits = first.YUnits;
                toOutputSds.ExtraInfo = first.ExtraInfo;

                this.Means = toOutputMeans;
                this.Sds = toOutputSds;
            }
            else if (output.All(o => o is float[]))
            {
                int amount = (output[0] as float[]).Length;
                if (output.Cast<float[]>().Any(o => o.Length != amount))
                    return;

                float[] averages = new float[amount];
                float[] sds = new float[amount];

                for (int i = 0; i < amount; i++)
                {
                    float[] thisdata = new float[output.Count];
                    for (int j = 0; j < thisdata.Length; j++)
                    {
                        thisdata[j] = (output[j] as float[])[i];
                    }

                    averages[i] = CeresBase.MathConstructs.Stats.Average(thisdata);
                    sds[i] = CeresBase.MathConstructs.Stats.StandardDev(thisdata);
                }

                this.Means = averages;
                this.Sds = sds;
            }
            else if (output.All(o => o is double[]))
            {
                int amount = (output[0] as double[]).Length;
                if (output.Cast<double[]>().Any(o => o.Length != amount))
                    return;

                double[] averages = new double[amount];
                double[] sds = new double[amount];

                for (int i = 0; i < amount; i++)
                {
                    double[] thisdata = new double[output.Count];
                    for (int j = 0; j < thisdata.Length; j++)
                    {
                        thisdata[j] = (output[j] as double[])[i];
                    }

                    averages[i] = CeresBase.MathConstructs.Stats.Average(thisdata);
                    sds[i] = CeresBase.MathConstructs.Stats.StandardDev(thisdata);
                }

                this.Means = averages;
                this.Sds = sds;
            }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            return new MeanSDPermutationOutput();
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Input" && targetValue == null) return new ValidationStatus(ValidationState.Fail, "Input must have connection");
            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }

        #region IPermutationOutputAdapter Members

        public void ReceivePropertyValue(object propertyValue, string targetPropertyName, int forIteration)
        {
            if (targetPropertyName == "Input")
            {
                if (this.output.Count < forIteration + 1)
                {
                    int amountToAdd = forIteration + 1 - this.output.Count;
                    for (int i = 0; i < amountToAdd; i++)
                    {
                        this.output.Add(new object());
                    }
                }
                this.output[forIteration] = propertyValue;
            }
        }

        #endregion
    }
}
