﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using CeresBase.UI;

namespace CeresBase.FlowControl.Adapters.Permutations.Output
{
    [AdapterDescription("Common")]
    public class LPlotOutput : AssociateAdapterBase, IPermutationOutputAdapter
    {
        [AssociateWithProperty("Input")]
        public object Input { get; set; }

        [AssociateWithProperty("Amount")]
        public int NumberToCombine { get; set; }

        [AssociateWithProperty("Backward?")]
        public bool BackAndForward { get; set; }

        [AssociateWithProperty("Label")]
        public string Label { get; set; }

        [AssociateWithProperty("Output", IsOutput=true)]
        public GraphableOutput Ouput { get; set; }

        private List<object> inputs = new List<object>();

        private readonly object lockable = new object();

        #region IPermutationOutputAdapter Members

        public void ReceivePropertyValue(object propertyValue, string targetPropertyName, int forIteration)
        {
            if (targetPropertyName == "Input")
            {
                lock (this.lockable)
                {
                    if (this.inputs.Count <= forIteration)
                    {
                        int toAdd = forIteration - this.inputs.Count + 1;
                        for (int i = 0; i < toAdd; i++)
                        {
                            this.inputs.Add(new object());
                        }
                    }

                    this.inputs[forIteration] = propertyValue;
                }
            }
            else base.ReceivePropertyValue(propertyValue, targetPropertyName);
        }

        #endregion

        public override string Name
        {
            get
            {
                return "L-Plot Output";
            }
            set
            {
               
            }
        }

        public override string Category
        {
            get
            {
                return "Permutation Output";
            }
            set
            {
                
            }
        }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            if (this.inputs.Count == 0) return;

            Type type = this.inputs[0].GetType();
            for (int i = 0; i < this.inputs.Count; i++)
            {
                if (this.inputs[i].GetType() != type)
                    throw new Exception("There is a mismatch in types in LPlot Output " + type.Name + " vs. " + this.inputs[i].GetType().Name);
            }

            if(this.inputs[0] is GraphableOutput)
            {
                GraphableOutput graph = this.inputs[0] as GraphableOutput;
                double[] x = graph.XData;
                if(this.Label == null) this.Label = graph.Label;
                else this.Label = graph.Label + ":" + this.Label;
                for (int i = 0; i < this.inputs.Count; i++)
			    {
    			    GraphableOutput tocomp = this.inputs[i] as GraphableOutput;
                    for (int j = 0; j < tocomp.XData.Length; j++)
			        {
                       if(tocomp.XData[j] != x[j])
                       {
                           throw new Exception("There is a mismatch in X coords in LPlot Output");
                       }
			        }
                    this.inputs[i] = tocomp.YData;
			    }
            }

            bool isSingle = type == typeof(double) || type == typeof(float);
            int dim = 1;
            int[] counts = null;

            if (this.inputs[0] is Array)
            {
                Array array = this.inputs[0] as Array;
                dim = array.Rank;
                counts = new int[dim];
                for (int i = 0; i < dim; i++)
                {
                    counts[i] = array.GetLength(dim);
                }

                for (int i = 1; i < this.inputs.Count; i++)
                {
                    Array array2 = this.inputs[i] as Array;
                    if(array2.Rank != dim)
                        throw new Exception("There is a mismatch in array ranks in LPlot Output " + dim + " vs. " + array2.Rank);
                    for (int j = 0; j < dim; j++)
                    {
                        if(array2.GetLength(j) != counts[j])
                            throw new Exception("There is a mismatch in array sizes in LPlot Ouput " + j + ":" + counts[j] + " vs. " + array2.GetLength(j));
                    }
                }

                isSingle = dim == 1 && counts[0] == 1;
            }

            List<GraphableOutput> graphsToOutput = new List<GraphableOutput>();

            //Every time point has one associated value, so we can put directly into one graph
            if(isSingle)
            {
                int numOuts = this.NumberToCombine;
                if(this.BackAndForward) numOuts *= 2;
                numOuts++;

                double[][] outData = new double[numOuts][];
                for (int i = 0; i < outData.Length; i++)
                {
                    outData[i] = new double[this.inputs.Count - numOuts + 1];
                }


                int start = this.BackAndForward ? this.NumberToCombine : 0;
                int total = 0;
                for (int i = start; i < this.inputs.Count - this.NumberToCombine; i++, total++)
			    {
                    double first = Convert.ToDouble(this.inputs[i]);

                    if(this.BackAndForward)
                    {
                        for (int j = -this.NumberToCombine; j <= this.NumberToCombine; j++)
			            {
            			    double second = Convert.ToDouble(this.inputs[i + j]);
                            outData[j + this.NumberToCombine][total] = second - first;
			            }
                    }
                    else
                    {
                        for (int j = 0; j < numOuts; j++)
			            {
                            double second = Convert.ToDouble(this.inputs[i + j]);
                            outData[j][total] = second - first;
			            }
                    }
			    }

                double[] x = new double[outData[0].Length];
                for (int i = 0; i < x.Length; i++)
                {
                    x[i] = i + 1;
                }

                double[] y = new double[outData.Length];
                for (int i = 0; i < y.Length; i++)
                {
                    y[i] = i + 1;
                }
                

                GraphableOutput graphOut = new GraphableOutput();
                graphOut.XData = x;
                graphOut.YData = outData;
                graphOut.OtherAxis = y;
                graphOut.Label = this.Label;
                graphOut.SourceDescription = "LPlot";
                graphsToOutput.Add(graphOut);
                this.Ouput = graphsToOutput[0];
            }
            else
            {
                //Every time point has multiple values so we will need to create multiple graphs
                throw new Exception("Multiple dimensions not supported yet.");
            }
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            LPlotOutput output = new LPlotOutput();
            output.BackAndForward = this.BackAndForward;
            output.Label = this.Label;
            output.NumberToCombine = this.NumberToCombine;
            return output;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Input" && targetValue == null)
            {
                return new ValidationStatus(ValidationState.Fail, "Link to input");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
