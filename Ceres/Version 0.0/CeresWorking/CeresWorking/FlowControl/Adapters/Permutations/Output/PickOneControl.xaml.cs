﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace CeresBase.FlowControl.Adapters.Permutations.Output
{
    /// <summary>
    /// Interaction logic for PickOneControl.xaml
    /// </summary>
    public partial class PickOneControl : Window
    {
        public UIElement[] ResultUIs { get; private set; }
        public object[] Result { get; private set; }


        private ObservableCollection<UIElement> control = new ObservableCollection<UIElement>();
        public ObservableCollection<UIElement> ControlsToView
        {
            get
            {
                return this.control;
            }
        }

        private ObservableCollection<object> data = new ObservableCollection<object>();
        public ObservableCollection<object> Data
        {
            get
            {
                return this.data;
            }
        }



        public bool ExactNumber
        {
            get { return (bool)GetValue(ExactNumberProperty); }
            set { SetValue(ExactNumberProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ExactNumber.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExactNumberProperty =
            DependencyProperty.Register("ExactNumber", typeof(bool), typeof(PickOneControl), new UIPropertyMetadata(numberProperty));

       

        public int NumberToSelect
        {
            get { return (int)GetValue(NumberToSelectProperty); }
            set { SetValue(NumberToSelectProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NumberToSelect.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberToSelectProperty =
            DependencyProperty.Register("NumberToSelect", typeof(int), typeof(PickOneControl), 
                new UIPropertyMetadata(1, new PropertyChangedCallback(numberProperty)));

        private static void numberProperty(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PickOneControl cont = obj as PickOneControl;
            cont.refresh();
        }


        private void refresh()
        {
            if (this.NumberToSelect == 1)
            {
                this.contSelector.SelectionMode = SelectionMode.Single;
                this.directions.Text = "Select One Control";
            }
            else
            {
                this.contSelector.SelectionMode = SelectionMode.Extended;
                if (this.ExactNumber)
                {
                    this.directions.Text = "Select Exactly " + this.NumberToSelect + " Controls";
                }
                else
                {
                    this.directions.Text = "Select At Least " + this.NumberToSelect + " Controls";
                }
            }
        }


        public PickOneControl()
        {
            InitializeComponent();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((this.NumberToSelect == 1 || this.ExactNumber))
            {
                if (this.contSelector.SelectedItems.Count != this.NumberToSelect)
                {
                    this.OKButton.IsEnabled = false;
                }
                else
                {
                    this.OKButton.IsEnabled = true;
                }
            }
            else
            {
                if (this.contSelector.SelectedItems.Count >= this.NumberToSelect)
                {
                    this.OKButton.IsEnabled = true;
                }
                else this.OKButton.IsEnabled = false;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            List<UIElement> toRetEle = new List<UIElement>();
            List<object> toRet = new List<object>();

            for (int i = 0; i < this.contSelector.Items.Count; i++)
            {
                if (this.contSelector.SelectedItems.Contains(this.contSelector.Items[i]))
                {
                    toRetEle.Add(this.contSelector.Items[i] as UIElement);
                    if (this.data != null)
                    {
                        toRet.Add(this.data[i]);
                    }
                }
            }

            this.Result = toRet.ToArray();
            this.ResultUIs = toRetEle.ToArray();

            this.Close();
        }
    }
}
