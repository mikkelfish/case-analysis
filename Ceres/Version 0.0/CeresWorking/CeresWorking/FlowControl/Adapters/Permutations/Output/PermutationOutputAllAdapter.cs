﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CeresBase.FlowControl.Adapters.Permutations.Output
{
    [AdapterDescription("Common")]
    public class PermutationOutputAllAdapter : AssociateAdapterBase, IPermutationOutputAdapter
    {
        public override string Name
        {
            get
            {
                return "Output All";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Permutation Output";
            }
            set
            {
               
            }
        }

       

        [AssociateWithProperty("Output", IsOutput=true)]
        public object Output { get; set; }

        [AssociateWithProperty("Input", CanLinkFrom=false)]
        public object Input { get; set; }

        [AssociateWithProperty("Merge Output")]
        public bool AsOneArray { get; set; }

        private List<object> output = new List<object>();

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            Type first = this.output[0].GetType();
            foreach(object o in this.output)
            {
                if (o.GetType() != first)
                {
                    first = typeof(object);
                    break;
                }
            }

            if (!this.AsOneArray)
            {
                IList toRet = first.MakeArrayType().InvokeMember("", System.Reflection.BindingFlags.CreateInstance, null, null, new object[] { this.output.Count }) as IList;
                for (int i = 0; i < this.output.Count; i++)
                {
                    toRet[i] = this.output[i];
                }

                this.Output = toRet;
            }
            else
            {
                IList toRet = first.InvokeMember("", System.Reflection.BindingFlags.CreateInstance, null, null, new object[] { this.output.Count }) as IList;
                for (int i = 0; i < this.output.Count; i++)
                {
                    if (this.output[i] is IList)
                        toRet[i] = (this.output[i] as IList)[0];
                    else toRet[i] = this.output[i];
                }

                this.Output = toRet;
            }

        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            return new PermutationOutputAllAdapter();
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Input" && targetValue == null) return new ValidationStatus(ValidationState.Fail, "Input must have connection");
            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }

        #region IPermutationOutputAdapter Members

        public void ReceivePropertyValue(object propertyValue, string targetPropertyName, int forIteration)
        {
            if (targetPropertyName == "Input")
            {
                if (this.output.Count < forIteration + 1)
                {
                    int amountToAdd = forIteration + 1 - this.output.Count;
                    for (int i = 0; i < amountToAdd; i++)
                    {
                        this.output.Add(new object());
                    }
                }
                this.output[forIteration] = propertyValue;
            }
        }

        #endregion

        public object SupplyPropertyDescription(string propertyName)
        {
            return null;
        }

        public object SupplyAdapterDescription()
        {
            return null;
        }
    }
}
