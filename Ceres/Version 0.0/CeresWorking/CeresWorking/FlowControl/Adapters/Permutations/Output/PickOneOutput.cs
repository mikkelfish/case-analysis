﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using System.Windows.Controls;

namespace CeresBase.FlowControl.Adapters.Permutations.Output
{
    [AdapterDescription("Common")]
    public class PickOneOutput : AssociateAdapterBase, IPermutationOutputAdapter
    {
        [AssociateWithProperty("Data", ReadOnly=true)]
        public object Data { get; set; }

        [AssociateWithProperty("Views", ReadOnly=true)]
        public IBatchViewable[] Views { get; set; }

        [AssociateWithProperty("Number To Select")]
        public int NumberToSelect { get; set; }

        [AssociateWithProperty("Must Be Equal")]
        public bool MustBeEqual{ get; set; }

        [AssociateWithProperty("Selected Output", IsOutput=true)]
        public object SelectedOutput { get; set; }

        private List<object> collectedData = new List<object>();
        private List<object> collectedViews = new List<object>();

        public override string Name
        {
            get
            {
                return "Pick The Output";
            }
            set
            {
                
            }
        }

        public override string Category
        {
            get
            {
                return "Permutation Output";
            }
            set
            {
                
            }
        }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public void RunAdapter(IBatchViewable[] views, object[] data)
        {
            if (views.Length == 0)
                return;


            PickOneControl pick = new PickOneControl();
            pick.NumberToSelect = this.NumberToSelect;
            pick.ExactNumber = this.MustBeEqual;

            int which = 0;

            List<Control> all = new List<Control>();

            for (int i = 0; i < views.Length; i++)
            {
                IBatchViewable[] whee;
                Control[] these = views[i].View(new IBatchViewable[] { views[i] }, out whee);
                foreach (Control cont in these)
                {
                    pick.ControlsToView.Add(cont);
                }

                if (data != null && data.Length > i)
                {
                    pick.Data.Add(data[i]);
                }
            }

            pick.ShowDialog();

            if (pick.Result != null)
            {
                if (this.NumberToSelect == 1)
                {
                    this.SelectedOutput = pick.Result[0];
                }
                else
                {
                    if (pick.Result != null)
                    {

                        Type first = pick.Result[0].GetType();
                        bool ok = true;
                        for (int i = 0; i < pick.Result.Length; i++)
                        {
                            if (pick.Result[i].GetType() != first)
                            {
                                ok = false;
                            }
                        }

                        if (ok)
                        {
                            object list = typeof(List<>).MakeGenericType(first).InvokeMember("", System.Reflection.BindingFlags.CreateInstance, null, null, null);
                            for (int i = 0; i < pick.Result.Length; i++)
                            {
                                (list as System.Collections.IList).Add(pick.Result[i]);
                            }

                            System.Reflection.MethodInfo method = list.GetType().GetMethod("ToArray");
                            this.SelectedOutput = method.Invoke(list, null);

                        }
                        else this.SelectedOutput = pick.Result;

                    }
                }
            }
        
        }

        public override void RunAdapter()
        {
            if (this.collectedViews.Count == 0)
                return;

            IBatchViewable[] output = this.collectedViews.Cast<IBatchViewable>().ToArray();
        
            object[] data = null;
            if(this.collectedData != null)
                data = this.collectedData.ToArray();

            this.RunAdapter(output, data);
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            PickOneOutput copy = new PickOneOutput();
            copy.Data = this.Data;
            copy.Views = this.Views;
            copy.NumberToSelect = NumberToSelect;
            return copy;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Data" && targetValue == null) 
                return new ValidationStatus(ValidationState.Fail, "Data must have connection");
            if (targetPropertyname == "Views" && targetValue == null)
                return new ValidationStatus(ValidationState.Fail, "Views must have connection");
            
            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return true; }
        }

        #region IPermutationOutputAdapter Members

        public void ReceivePropertyValue(object propertyValue, string targetPropertyName, int forIteration)
        {
             if (forIteration < 0) return;

            if (targetPropertyName == "Data")
            {
                if (this.collectedData.Count < forIteration + 1)
                {
                    int amountToAdd = forIteration + 1 - this.collectedData.Count;
                    for (int i = 0; i < amountToAdd; i++)
                    {
                        this.collectedData.Add(new object());
                    }
                }
                this.collectedData[forIteration] = propertyValue;
            }

            if (targetPropertyName == "Views")
            {
                if (this.collectedViews.Count < forIteration + 1)
                {
                    int amountToAdd = forIteration + 1 - this.collectedViews.Count;
                    for (int i = 0; i < amountToAdd; i++)
                    {
                        this.collectedViews.Add(new object());
                    }
                }
                this.collectedViews[forIteration] = propertyValue;
            }
        }

        #endregion
    }
}
