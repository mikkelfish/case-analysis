﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl.Adapters.Permutations
{
    [AdapterDescription("Common")]
    public class RangePermutation : AssociateAdapterBase, IPermutationAdapter
    {
        private int iteration;

        [AssociateWithProperty("Base Number")]
        public double Base { get; set; }

        [AssociateWithProperty("Step Size")]
        public double Step { get; set; }

        [AssociateWithProperty("Number Steps")]
        public int NumSteps { get; set; }

        public enum StepType { Add, Multiply }

        [AssociateWithProperty("Method")]
        public StepType StepMethod { get; set; }

        [AssociateWithProperty("Step Value", IsOutput=true)]
        public double StepValue { get; set; }

        [AssociateWithProperty("Iteration", IsOutput = true)]
        public int Iteration { get; set; }

        #region IPermutationAdapter Members

        public int IterationCount
        {
            get 
            {
                return this.NumSteps;
            }
        }

        public void SetIteration(int iteration)
        {
            this.iteration = iteration;
        }

        #endregion

        public override string Name
        {
            get
            {
                return "Range Permutation";
            }
            set
            {
               
            }
        }

        public override string Category
        {
            get
            {
                return "Common Permutations";
            }
            set
            {
                
            }
        }

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public override void RunAdapter()
        {
            if (this.StepMethod == StepType.Add)
            {
                double toAdd = this.iteration * this.Step;
                this.StepValue = this.Base + toAdd;
            }
            else if (this.StepMethod == StepType.Multiply)
            {
                this.StepValue = this.Base;
                for (int i = 0; i < iteration; i++)
                {
                    this.StepValue *= this.Step;
                }
            }

            this.Iteration = this.iteration;
        }

        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }

        public override object Clone()
        {
            RangePermutation perm = new RangePermutation();
            perm.NumSteps = this.NumSteps;
            perm.Base = this.Base;
            perm.StepMethod = this.StepMethod;
            perm.Step = this.Step;
            return perm;
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Number Steps" && targetValue is int)
            {
                int val = (int)targetValue;
                if (val <= 0)
                    return new ValidationStatus(ValidationState.Fail, "Number of steps must be greater than 0.");
            }

            return new ValidationStatus(ValidationState.Pass);
        }

        public override bool HasUserInteraction
        {
            get { return false; }
        }
    }
}
