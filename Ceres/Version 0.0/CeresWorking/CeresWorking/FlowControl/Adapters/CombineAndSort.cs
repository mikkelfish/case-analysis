﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects.Flowable;
using MesosoftCommon.Utilities.Validations;

namespace CeresBase.FlowControl.Adapters
{
    [AdapterDescription("Common")]
    public class CombineAndSort : IBatchFlowable, IRoutineCategoryProvider
    {
        public override string ToString()
        {
            return "Combine & Sort";
        }

        public enum SortMode { None, Ascending, Descending };

        #region IBatchFlowable Members

        public BatchProcessableParameter[] GetParameters()
        {
            BatchProcessableParameter data = new BatchProcessableParameter()
            {
                Name = "Data",
                Editable = false,
                Validator =  new NotNullValidator(),
                TargetType = typeof(float[][])
            };

            BatchProcessableParameter sort = new BatchProcessableParameter()
            {
                Name = "Sort Mode",
                Val = SortMode.Ascending
            };

            return new BatchProcessableParameter[] { data, sort };
        }

        public object[] Run(BatchProcessableParameter[] parameters)
        {
            float[][] data = parameters.Single(p => p.Name == "Data").Val as float[][];
            SortMode sort = (SortMode)parameters.Single(p => p.Name == "Sort Mode").Val;

            List<float> toRet = new List<float>();
            for (int i = 0; i < data.Length; i++)
            {
                toRet.AddRange(data[i]);
            }

            if (sort == SortMode.Ascending)
            {
                toRet.Sort();
            }
            else if (sort == SortMode.Descending)
            {
                toRet.Sort(
                    delegate(float one, float two)
                    {
                        if (one == two) return 0;
                        if (one > two) return -1;
                        return 1;
                    }
                );
            }


            return new object[] { toRet.ToArray() };

        }

        public string[] OutputDescription
        {
            get { return new string[] { "Combined Data" }; }
        }

        #endregion

        #region IRoutineCategoryProvider Members

        public string Category
        {
            get { return "Runtime Tools"; }
        }

        #endregion
    }
}
