﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl.Adapters
{
    [global::System.AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public sealed class AdapterDescriptionAttribute : Attribute
    {
        public string Description { get; private set; }
        public AdapterDescriptionAttribute(string description)
        {
            this.Description = description;
        }
    }
}
