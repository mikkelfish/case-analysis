﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl.Adapters
{
    public abstract class AssociateAdapterBase : IFlowControlAdapter
    {
        protected PropToFlowProp[] props;
        public AssociateAdapterBase()
        {
            this.props = PropToFlowProp.GetProps(this.GetType());
        }

        #region IFlowControlAdapter Members

        public int ID
        {
            get;
            set;
        }

        public abstract string Name
        {
            get;
            set;
        }

        public abstract string Category
        {
            get;
            set;
        }

        public abstract event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public virtual string[] SupplyPropertyNames()
        {
            return this.props.Select(p => p.Attribute.PropertyName).ToArray();
        }

        public virtual object SupplyPropertyValue(string sourcePropertyName)
        {
            PropToFlowProp prop = this.props.Single(p => p.Attribute.PropertyName == sourcePropertyName);

            return prop.Property.GetValue(this, null);
        }

        public virtual void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            this.props.Single(p => p.Attribute.PropertyName == targetPropertyName).Property.SetValue(this, propertyValue, null);
        }

        public virtual bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            return this.props.Single(p => p.Attribute.PropertyName == targetPropertyName).Property.CanWrite;
        }

        public virtual FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
        {
            throw new NotImplementedException();
        }

        public virtual FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
        {
            throw new NotImplementedException();
        }

        public virtual object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
        {
            throw new NotImplementedException();
        }

        public virtual void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
        {
            throw new NotImplementedException();
        }

        public virtual Type PropertyType(string targetPropertyName)
        {
            return this.props.Single(p => p.Attribute.PropertyName == targetPropertyName).Property.PropertyType;
        }

        public abstract void RunAdapter();

        public abstract object[] GetValuesForSerialization();

        public abstract void LoadValuesFromSerialization(object[] values);

        public virtual bool CanEditProperty(string targetPropertyname)
        {
           
                return this.props.Single(p => p.Attribute.PropertyName == targetPropertyname).Property.CanWrite &&
                    !this.props.Single(p => p.Attribute.PropertyName == targetPropertyname).Attribute.ReadOnly;
        }

        public virtual bool CanReceivePropertyLinks(string targetPropertyname)
        {
            return !this.props.Single(p => p.Attribute.PropertyName == targetPropertyname).Attribute.DisableLink;
        }

        #endregion

        #region ICloneable Members

        public abstract object Clone();

        #endregion

        private Process hostProcess;
        public virtual Process HostProcess
        {
            get
            {
                return hostProcess;
            }
            set
            {
                if (this.hostProcess != null)
                {
                    this.hostProcess.PropertyEdited -= new EventHandler<ProcessPropertyChangedArgs>(HostProcess_PropertyEdited);
                }

                this.hostProcess = value;
                if (this.hostProcess != null)
                {
                    this.hostProcess.PropertyEdited += new EventHandler<ProcessPropertyChangedArgs>(HostProcess_PropertyEdited);
                }
            }
        }

        void HostProcess_PropertyEdited(object sender, ProcessPropertyChangedArgs e)
        {
            Process process = sender as Process;
            if (process == null) return;

           // ProcessPropertyChangedInfo info = e.PropertiesChanged.SingleOrDefault(p => p.PropertyName == "Num Graphs");

            foreach (ProcessPropertyChangedInfo info in e.PropertiesChanged)
            {
                PropToFlowProp prop = props.SingleOrDefault(p => p.Attribute.PropertyName == info.PropertyName);
                if (prop != null)
                {
                    if (prop.Attribute.ReloadOnChange)
                    {
                        process.ReloadPropertiesFromAdapter();
                        break;
                    }
                }
            }

            //if (info != null)
            //{
            //    if (this.NumberOfGraphsToMerge != (int)info.NewValue)
            //    {
            //        this.NumberOfGraphsToMerge = (int)info.NewValue;
            //        process.ReloadPropertiesFromAdapter();
            //    }
            //}
        }

        #region IFlowControlAdapter Members


        public virtual bool ShouldSerialize(string targetPropertyname)
        {
            return this.props.Single(p => p.Attribute.PropertyName == targetPropertyname).Attribute.ShouldSerialize;
        }

        public abstract ValidationStatus ValidateProperty(string targetPropertyname, object targetValue);

        public virtual bool CanSendPropertyLinks(string targetPropertyname)
        {
            return this.props.Single(p => p.Attribute.PropertyName == targetPropertyname).Attribute.CanLinkFrom;
        }

        public virtual bool IsOutputProperty(string targetPropertyname)
        {
            return this.props.Single(p => p.Attribute.PropertyName == targetPropertyname).Attribute.IsOutput;

        }

        #endregion

        #region IFlowControlAdapter Members


        public abstract bool HasUserInteraction
        {
            get;
        }

        #endregion

        public virtual object SupplyPropertyDescription(string propertyName)
        {
            return null;
        }

        public virtual object SupplyAdapterDescription()
        {
            return null;
        }
    }
}
