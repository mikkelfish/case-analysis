﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CeresBase.Projects;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using System.IO;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Interaction logic for RoutineManagementControl.xaml
    /// </summary>
    public partial class RoutineManagementControl : UserControl, INotifyPropertyChanged
    {
        private string controllerString = "";
        public string ControllerString 
        {
            get
            {
                return this.controllerString;
            }
            set
            {
                this.controllerString = value;
                RoutineCentral.Routines[ControllerString].CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(RoutineManagementControl_CollectionChanged);
                OnPropertyChanged("RoutineList");
            }
        }

        public ObservableCollection<RoutinePackage> RoutineList
        {
            get
            {
                return RoutineCentral.Routines[ControllerString];
            }
        }

        public RoutineManagementControl()
        {
            InitializeComponent();
        }

        void RoutineManagementControl_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("RoutineList");
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        private void ExportRoutineButton_Click(object sender, RoutedEventArgs e)
        {
                        List<RoutinePackage> packages = new List<RoutinePackage>();
            for (int i = 0; i < RoutineListDisplay.SelectedItems.Count; i++)
            {
                packages.Add(RoutineListDisplay.SelectedItems[i] as RoutinePackage);
            }

            foreach(RoutinePackage package in packages)
            {
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                saveDialog.FileName = package.RoutineName;
                saveDialog.DefaultExt = ".xaml";
                saveDialog.Filter = "XAML documents (.xaml)|*.xaml";

                Nullable<bool> result = saveDialog.ShowDialog();

                if (result == true)
                {
                    using (FileStream saveFileStream = new FileStream(saveDialog.FileName, FileMode.Create))
                    {
                        MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(package, saveFileStream);
                        //RoutineSerializer rs = RoutineSerializer.Serialize(this.ManagedRoutine);
                        //MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(rs, saveFileStream);
                    }
                }
            }

        }

        private void ImportRoutineButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            openDialog.FileName = "Routine";
            openDialog.DefaultExt = ".xaml";
            openDialog.Filter = "XAML documents (.xaml)|*.xaml";

            Nullable<bool> result = openDialog.ShowDialog();

            if (result == true)
            {
                if (File.Exists(openDialog.FileName))
                {
                    using (FileStream openFileStream = new FileStream(openDialog.FileName, FileMode.Open))
                    {
                        RoutinePackage pack = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(openFileStream) as RoutinePackage;
                        pack.SerializedRoutineManager.Name = pack.RoutineName;
                        //pack.ID = Guid.NewGuid();
                        RoutineCentral.UpdateCentralWithRoutine(pack.RoutineName, pack.ID, pack.SerializedRoutine,
                            pack.SerializedRoutineManager, this.ControllerString);
                    }
                }
            }
        }

        private void DeleteRoutineButton_Click(object sender, RoutedEventArgs e)
        {
            List<RoutinePackage> packages = new List<RoutinePackage>();
            for (int i = 0; i < RoutineListDisplay.SelectedItems.Count; i++)
            {
                packages.Add(RoutineListDisplay.SelectedItems[i] as RoutinePackage);
            }

            foreach(RoutinePackage package in packages)
            {
                RoutineCentral.Routines[this.ControllerString].Remove(package);
            }
        }
    }
}
