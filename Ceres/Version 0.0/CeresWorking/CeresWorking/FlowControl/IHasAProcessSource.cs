﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public interface IHasAProcessSource
    {
        Process Source { get; set; }            
    }
}
