﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public class PropertyLinkVisual
    {
        public const double TopOffset = 27.5;
        public const double ElemOffset = 15.0;
        public const double LeftOffset = 11.0;
        public const double RightOffset = 6.0;

        public PropertyLink PropertyLink { get; set; }
        public ProcessControl OriginProcessControl { get; set; }
        public ProcessControl DestinationProcessControl { get; set; }
        public Process OriginProcess
        {
            get
            {
                if (this.OriginProcessControl == null) return null;
                return OriginProcessControl.Source;
            }
        }
        public Process DestinationProcess
        {
            get
            {
                if (this.DestinationProcessControl == null) return null;
                return DestinationProcessControl.Source;
            }
        }
        /// <summary>
        /// The Origin is the PropertyListing whose data is being sent to the destination.
        /// Note that PropertyLinks are held at the Destination Property, and point to the Origin Property.
        /// </summary>
        public PropertyListing OriginPropertyListing { get; set; }
        /// <summary>
        /// The Destination is the PropertyListing that is receiving data from the origin.
        /// Note that PropertyLinks are held at the Destination Property, and point to the Origin Property.
        /// </summary>
        public PropertyListing DestinationPropertyListing { get; set; }

        public double OriginVisualOffset
        {
            get
            {
                double? returnValue = this.getOffsetForPropertyListing(this.OriginPropertyListing, this.OriginProcess);
                if (returnValue.HasValue)
                    return returnValue.Value;
                else
                    return 0.0;
            }
        }

        public double DestinationVisualoffset
        {
            get
            {
                double? returnValue = this.getOffsetForPropertyListing(this.DestinationPropertyListing, this.DestinationProcess);
                if (returnValue.HasValue)
                    return returnValue.Value;
                else
                    return 0.0;
            }
        }

        private double? getOffsetForPropertyListing(PropertyListing target, Process hostProcess)
        {
            if (!(hostProcess.PropertiesList.Contains(target))) return null;

            double ret = 0;

            //Move down by the top offset
            ret += PropertyLinkVisual.TopOffset;
            if (target.IsOutput)
            {
                //Offset to handle the margin between the input/output panes
                ret += 2.0;
                ret += (hostProcess.NormalCount() * PropertyLinkVisual.ElemOffset);
                double? retVal = hostProcess.IndexInOutputList(target);
                if (retVal.HasValue)
                    ret += (retVal.Value * PropertyLinkVisual.ElemOffset);
            }
            else
            {
                double? retVal = hostProcess.IndexInNormalList(target);
                if (retVal.HasValue)
                    ret += (retVal.Value * PropertyLinkVisual.ElemOffset);
            }

            return ret;
        }

        public bool LineIsCollapsed { get; set; }
        public object CollapsedLineIdentifier { get; set; }

        public PropertyLinkVisual()
        {
            LineIsCollapsed = false;
        }

    }
}
