﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace CeresBase.FlowControl
{
    [ValueConversion(typeof(Process), typeof(object[]))]
    public class ProcessToGridConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return new object();
            if (!(value is Process)) return new object();


            List<string> names = new List<string>();
            List<Type> types = new List<Type>();
            List<string> categories = new List<string>();
            List<object> vals = new List<object>();
            List<bool> readOnly = new List<bool>();

            string category = (value as Process).Name + " Properties";

            if (parameter is string)
            {
                category = parameter as string;
            }

            foreach (PropertyListing prop in (value as Process).PropertiesList)
            {
                Type thisValType;

                //Add this check for output since it shouldn't be editable
                if (prop.Name == "Output") continue;

                //Don't put non-editable properties in the editor.
                if (!prop.CanEdit) continue;

                if (prop != null &&
                    (prop.Value is PropertyLink || prop.Value is PropertyMultiLink))
                {
                    continue;
                }

                names.Add(prop.Name);
                if (prop.Value == null)
                    thisValType = typeof(object);
                else
                    thisValType = prop.Value.GetType();
                types.Add(thisValType);
                categories.Add(category);
                vals.Add(prop.Value);
                readOnly.Add(false);
            }

            return MesosoftCommon.Utilities.Reflection.DynamicObjects.CreateObject(
                    types.ToArray(), categories.ToArray(), names.ToArray(), names.ToArray(), null, vals.ToArray(), readOnly.ToArray());
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
