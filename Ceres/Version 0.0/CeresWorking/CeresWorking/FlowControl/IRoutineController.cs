﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace CeresBase.FlowControl
{
    public interface IRoutineController
    {
        FrameworkElement GetContentOverride();
        void AdaptersInitialized(Routine routine, AdapterInitPair[] pairs);
        void AddAvailableAdapters(RoutineManager rm);
        void AddButtonClicked();

        RoutineBatchControl BatchControl { get; }
    }
}
