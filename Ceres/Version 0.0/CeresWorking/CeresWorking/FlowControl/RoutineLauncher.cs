﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.Projects;
using Toub.Threading;
using System.Threading;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Manages 1 to n routines for use in Routine Batching.  Works with the routine Run method to execute routines, particularly breadth-first.
    /// </summary>
    public class RoutineLauncher
    {
        private List<Routine> routines = new List<Routine>();
        public List<Routine> Routines { get { return routines; } set { routines = value; } }

        private List<List<RoutineBatchOutputPoint>> routineOutput = new List<List<RoutineBatchOutputPoint>>();
        public List<List<RoutineBatchOutputPoint>> RoutineOutput { get { return routineOutput; } }

        public int UserProcsCompleted { get; private set; }
        public int TotalProcsCompleted { get; private set; }
        public int TotalProcCount { get; set; }
        public int UserProcCount { get; set; }
        public int MaxLevel { get; private set; }

        public RoutineLauncher()
        {

        }

        public RoutineLauncher(List<Routine> paramRoutines)
        {
            this.routines = paramRoutines;
        }

        public event EventHandler<RoutineExecutionProgressEventArgs> ExecutionProgressChanged;


        private object lockable = new object();


        public void SetupExecution()
        {

            //First, determine what the "maximum" processingstack level is, so we know when to halt.
            foreach (Routine routine in Routines)
            {
                routine.PrepareForExecution();
                this.MaxLevel = (this.MaxLevel > routine.ProcessingStack.Count ? this.MaxLevel : routine.ProcessingStack.Count);
                //While we're at it, initialize stuff!
                this.TotalProcCount += routine.ProcessList.Count;

                foreach (Process process in routine.ProcessList)
                {
                    if (process.Adapter.HasUserInteraction)
                    {
                        this.UserProcCount++;
                    }
                }

            }            
        }

        class ThreadedRoutineExecutionPackage
        {
            public Routine RoutineToExecute;
            public int ProcessingLevelToExecute;
            public int IndexOfRoutineToExecute;
            public AutoResetEvent NotifyFinishedEvent;
            public int ProcCount { get; set; }
            public List<RoutineBatchOutputPoint> Output { get; set; }


            public RoutineProcessingException ThrownException { get; set; }
        }

        static void ThreadProc(object stateInfo)
        {
            ThreadedRoutineExecutionPackage trep = stateInfo as ThreadedRoutineExecutionPackage;

            if (stateInfo == null) return;
            int procCount;
            try
            {
                trep.Output = trep.RoutineToExecute.RunNonUIExecutionLevel(trep.ProcessingLevelToExecute, out procCount);
                trep.ProcCount = procCount;
                
            }
            catch(RoutineProcessingException ex)
            {
                trep.ThrownException = ex;
            }
            finally
            {
                trep.NotifyFinishedEvent.Set();

            }
        }

        private class backgroundHelper
        {
            public int ProcessingLevel { get; set; }
            public RoutineBatchControl.RoutineHelper Helper { get; set; }
        }

        private Thread backThread;

        public void CancelEverything()
        {
            ManagedThreadPool.CancelAll();
            if (backThread != null)
            {
                if(backThread.ThreadState != ThreadState.Aborted &&
                    backThread.ThreadState != ThreadState.Stopped)
                    backThread.Abort();
            }
        }

        public void SpinOffBackGround(RoutineBatchControl.RoutineHelper control, int level)
        {
            this.backThread = new Thread(new ParameterizedThreadStart(this.backgroundThreadStart));
            this.backThread.IsBackground = true;
            backgroundHelper helper = new backgroundHelper() { ProcessingLevel = level, Helper=control };
            this.backThread.Start(helper);            
        }

        private void backgroundThreadStart(object obj)
        {
            backgroundHelper helper = obj as backgroundHelper;
            RoutineProcessingException ex =runBackgroundLevel(helper.ProcessingLevel);            
            helper.Helper.RunLevel(ex);
        }


        private RoutineProcessingException runBackgroundLevel(int level)
        {
            List<AutoResetEvent> backgroundEvents = new List<AutoResetEvent>();

            List<ThreadedRoutineExecutionPackage> packages = new List<ThreadedRoutineExecutionPackage>();

            for (int i = 0; i < this.routines.Count; i++)
            {
                Routine routine = this.routines[i];
                ThreadedRoutineExecutionPackage trep = new ThreadedRoutineExecutionPackage();
                trep.ProcessingLevelToExecute = level;
                trep.RoutineToExecute = routine;
                trep.IndexOfRoutineToExecute = i;
                packages.Add(trep);

                AutoResetEvent arEvent = new AutoResetEvent(false);
                trep.NotifyFinishedEvent = arEvent;
                backgroundEvents.Add(arEvent);

                ManagedThreadPool.QueueUserWorkItem(new WaitCallback(ThreadProc), trep);
            }

            foreach (AutoResetEvent arEvent in backgroundEvents)
            {
                arEvent.WaitOne();
            }

            foreach (ThreadedRoutineExecutionPackage pack in packages)
            {
                if (pack.ThrownException != null)
                {
                    return pack.ThrownException;
                }
            }

              
            
            lock (this.lockable)
            {
                List<List<RoutineBatchOutputPoint>> toRet = new List<List<RoutineBatchOutputPoint>>();
                foreach (ThreadedRoutineExecutionPackage pack in packages)
                {
                    if (pack.Output != null && pack.Output.Count > 0)
                        toRet.Add(pack.Output);
                    this.TotalProcsCompleted += pack.ProcCount;

                }



                for (int i = 0; i < toRet.Count; i++)
                {
                    if (routineOutput.Count < toRet.Count)
                    {
                        routineOutput.Add(new List<RoutineBatchOutputPoint>());
                    }

                    foreach (RoutineBatchOutputPoint point in toRet[i])
                    {
                        routineOutput[i].Add(point);
                    }
                }
            }

            return null;
        }

        public RoutineProcessingException RunUILevel(object lev)
        {
            int level = (int)lev;
            List<ThreadedRoutineExecutionPackage> packages = new List<ThreadedRoutineExecutionPackage>();
            for (int i = 0; i < this.routines.Count; i++)
            {
                Routine routine = this.routines[i];
                ThreadedRoutineExecutionPackage trep = new ThreadedRoutineExecutionPackage();
                trep.ProcessingLevelToExecute = level;
                trep.RoutineToExecute = routine;
                trep.IndexOfRoutineToExecute = i;
                packages.Add(trep);
            }

            List<List<RoutineBatchOutputPoint>> toRet = new List<List<RoutineBatchOutputPoint>>();

            int totprocCount = 0;
            foreach (ThreadedRoutineExecutionPackage trep in packages)
            {
                int procCount;
                try
                {
                    toRet.Add(trep.RoutineToExecute.RunUIExecutionLevel(trep.ProcessingLevelToExecute, out procCount));
                    totprocCount += procCount;
                }
                catch (RoutineProcessingException ex)
                {
                    return ex;
                }
                
            }

            lock (this.lockable)
            {
                this.TotalProcsCompleted += totprocCount;
                this.UserProcsCompleted += totprocCount;
               

                for (int i = 0; i < toRet.Count; i++)
                {
                    if (routineOutput.Count < toRet.Count)
                    {
                        routineOutput.Add(new List<RoutineBatchOutputPoint>());
                    }

                    foreach (RoutineBatchOutputPoint point in toRet[i])
                    {
                        routineOutput[i].Add(point);
                    }
                }
            }

            return null;
        }
       
    }

    
}
