﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections;

namespace CeresBase.FlowControl
{
    public class PropertyListing : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public bool DisplayIOHooks { get; set; }
        public bool CanSendPropertyLinks { get; set; }

        private ValidationStatus validationStatus = new ValidationStatus(ValidationState.Unvalidated);
        public ValidationStatus ValidationStatus
        {
            get
            {
                return validationStatus;
            }
            set
            {
                validationStatus = value;
                OnPropertyChanged("ValidationStatus");
                OnPropertyChanged("ValidationState");
                OnPropertyChanged("ValidationComment");
            }
        }

        public ValidationState ValidationState
        {
            get
            {
                return validationStatus.ReturnValue;
            }
        }

        public string ValidationComment
        {
            get
            {
                if (validationStatus.ReturnComment == "")
                    return null;
                return validationStatus.ReturnComment;
            }
        }

        private object description;
        public object Description
        {
            get
            {
                return this.description;
            }
            set
            {
                this.description = value;
            }
        }

        public bool IsOutput { get; set; }

        private bool canReceivePropertyLinks = true;
        public bool CanReceivePropertyLinks
        {
            get
            {
                if (this.IsOutput) return false;
                return canReceivePropertyLinks;
            }
            set
            {
                canReceivePropertyLinks = value;
            }
        }

        private bool canEdit = true;
        public bool CanEdit
        {
            get
            {
                if (this.IsOutput) return false;
                return canEdit;
            }
            set
            {
                canEdit = value;
            }
        }


        public Type ValueType
        {
            get
            {
                if (value == null)
                    return typeof(object);
                return value.GetType();
            }
        }

        private object value;
        public object Value 
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
                OnPropertyChanged("Value");
            }
        }

        public bool UsingAdapterDefault { get; set; }

        public PropertyListing()
        {
            this.DisplayIOHooks = false;
        }

        public PropertyListing(string name)
        {
            this.Name = name;
            this.DisplayIOHooks = false;
        }

        public PropertyListing(string name, object value)
        {
            this.Name = name;
            this.DisplayIOHooks = false;
            this.value = value;
        }

        static public PropertyListing FindFirstByName(IEnumerable<PropertyListing> collection, string name)
        {
            if (collection == null || name == "") return null;

            object ret = collection.GetType().GetConstructor(System.Type.EmptyTypes).Invoke(null);

            name = char.ToUpper(name[0]) + name.Substring(1);

            foreach (PropertyListing property in collection)
            {
                string toTest = char.ToUpper(property.Name[0]) + property.Name.Substring(1);

                if (toTest == name)
                {
                    return property;
                }
            }

            return null;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }

    public class PropertyListingSerializer
    {
        public string Name { get; set; }
        public bool DisplayIOHooks { get; set; }
        public object Value { get; set; }
        public bool UsingAdapterDefault { get; set; }

        static public PropertyListingSerializer Serialize(PropertyListing toSerialize, Routine hostRoutine)
        {
            //if (property.Value is PropertyLink || property.Value is PropertyMultiLink)
            PropertyListingSerializer ret = new PropertyListingSerializer();

            ret.Name = toSerialize.Name;
            ret.DisplayIOHooks = toSerialize.DisplayIOHooks;
            ret.UsingAdapterDefault = toSerialize.UsingAdapterDefault;

            

            if (toSerialize.Value is PropertyLink)
                ret.Value = PropertyLinkSerializer.Serialize(toSerialize.Value as PropertyLink, hostRoutine);
            else if (toSerialize.Value is PropertyMultiLink)
                ret.Value = PropertyMultiLinkSerializer.Serialize(toSerialize.Value as PropertyMultiLink, hostRoutine);
            else
                ret.Value = toSerialize.Value;

            return ret;
        }

        static public PropertyListing Deserialize(PropertyListingSerializer toDeserialize, Routine hostRoutine)
        {
            PropertyListing ret = new PropertyListing();

            ret.Name = toDeserialize.Name;
            ret.DisplayIOHooks = toDeserialize.DisplayIOHooks;
            ret.UsingAdapterDefault = toDeserialize.UsingAdapterDefault;

            if (toDeserialize.Value is PropertyLinkSerializer)
                ret.Value = PropertyLinkSerializer.Deserialize(toDeserialize.Value as PropertyLinkSerializer, hostRoutine);
            else if (toDeserialize.Value is PropertyMultiLinkSerializer)
                ret.Value = PropertyMultiLinkSerializer.Deserialize(toDeserialize.Value as PropertyMultiLinkSerializer, hostRoutine);
            else
                ret.Value = toDeserialize.Value;

            return ret;
        }
    }
}
