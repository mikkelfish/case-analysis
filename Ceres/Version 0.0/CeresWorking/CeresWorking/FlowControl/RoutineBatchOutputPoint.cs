﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public class RoutineBatchOutputPoint
    {
        public string Name { get; set; }
        public object OutputData { get; set; }

        public string SourceProcessName { get; set; }
        public object SourceData { get; set; }

        public int ProcessingLevel { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
