﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public interface IFlowControlPermutationAdapter : IFlowControlAdapter
    {
        int IterationCount { get; }
        void SetIteration(int iteration);
    }
}
