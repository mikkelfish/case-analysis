﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public class ProcessPropertyChangedArgs : EventArgs
    {
        public ProcessPropertyChangedInfo[] PropertiesChanged { get; set; }
    }

    public class ProcessPropertyChangedInfo
    {
        public object NewValue { get; set; }
        public object OldValue { get; set; }
        public string PropertyName { get; set; }
    }
}
