﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    [global::System.Serializable]
    public class RoutineProcessingException : InvalidOperationException
    {
        public class ProcessExceptionArgs
        {
            public Process Process { get; set; }
            public Exception Exception { get; set; }
        }

        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public ProcessExceptionArgs[] ProcessesWithExceptions { get; private set; }

        public RoutineProcessingException() { }
        public RoutineProcessingException(ProcessExceptionArgs[] exceptions) 
        {
            this.ProcessesWithExceptions = exceptions;
        }

        protected RoutineProcessingException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }

        public override string ToString()
        {
            string toRet = "";
            if (this.ProcessesWithExceptions != null)
            {
                foreach (ProcessExceptionArgs args in this.ProcessesWithExceptions)
                {
                    toRet += "Process " + args.Process.ToString() + "\n";

                    Exception toWrite = args.Exception;
                    while (toWrite != null)
                    {
                        toRet += toWrite.Message + "\n";
                        toRet += toWrite.StackTrace + "\n";
                        toWrite = toWrite.InnerException;
                    }

                    toRet += "\n\n\n";
                }
            }

            return toRet;
        }
    }
}
