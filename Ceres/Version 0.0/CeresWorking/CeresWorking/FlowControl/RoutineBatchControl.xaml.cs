﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using CeresBase.Projects;
using CeresBase.Data;
using CeresBase.Projects.Flowable;
using MesosoftCommon.DataStructures;
using CeresBase.FlowControl.Adapters;
using System.ComponentModel;
using System.Threading;
using CeresBase.FlowControl.Adapters.Matlab;
using CeresBase.UI;
using System.Reflection;
using MesosoftCommon.Utilities.Settings;
using MesosoftCommon.Utilities.Exceptions;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Interaction logic for RoutineBatchControl.xaml
    /// </summary>
    public partial class RoutineBatchControl : UserControl, INotifyPropertyChanged
    {
        private IRoutineController controller;
        public IRoutineController Controller 
        {
            get
            {
                return this.controller;
            }
            set
            {
                this.controller = value;
                this.templateToOverride.Content = this.controller.GetContentOverride();

                    if (!RoutineCentral.Routines.ContainsKey(this.Controller.GetType().ToString()))
                        RoutineCentral.Routines.Add(this.Controller.GetType().ToString(), new ObservableCollection<RoutinePackage>());
                    RoutineCentral.Routines[this.Controller.GetType().ToString()].CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(RoutineBatchControl_CollectionChanged);
                    OnPropertyChanged("RoutineList");
   
            }
        }

        void RoutineBatchControl_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("RoutineList");
        }

       

        public RoutineBatchControl()
        {
            InitializeComponent();

            MatlabLoader.BuildAdapterInfos("Common");
        }

     

        

       

        public ObservableCollection<RoutinePackage> RoutineList 
        {
            get
            {
                if (this.Controller == null) return null;

                return RoutineCentral.Routines[this.Controller.GetType().ToString()];
            }            
        }

       

        private ObservableCollection<RoutineBatchOutputPoint> outputList = new ObservableCollection<RoutineBatchOutputPoint>();
        public ObservableCollection<RoutineBatchOutputPoint> OutputList
        {
            get
            {
                return outputList;
            }
        }

        private ObservableCollection<object> toRunList = new ObservableCollection<object>();
        public ObservableCollection<object> ToRunList { get { return toRunList; } }

        private List<Routine> expandPermutationProcesses(List<Routine> toExpand)
        {
            List<Routine> ret = new List<Routine>();

            for (int i = 0; i < toExpand.Count; i++)
            {
                List<Routine> expander = new List<Routine>();
                expander.Add(toExpand[i]);

                foreach (Process proc in toExpand[i].ProcessList)
                {
                    if (proc.Adapter is IPermutationAdapter)
                    {
                        expander = expandRoutinesByProcess(expander, proc, expander[0].ProcessList.IndexOf(proc));
                    }
                }   

                foreach (Routine routine in expander)
                {
                    ret.Add(routine);
                }
            }

            return ret;
        }

        private List<Routine> expandRoutinesByProcess(List<Routine> toExpand, Process sourceProc, int processIndex)
        {
            List<Routine> ret = new List<Routine>();

            foreach (Routine expandRoutine in toExpand)
            {
                IPermutationAdapter permAdapter = (sourceProc.Adapter as IPermutationAdapter);
                for (int i = 0; i < permAdapter.IterationCount; i++)
                {
                    Routine r = RoutineCentral.GetRoutine(RoutineListDisplay.SelectedIndex, this.Controller.GetType().ToString());
                    r.Args = expandRoutine.Args;
                    r.AdaptersInitialized += new EventHandler<FlowControlAdapterInitArgs>(r_AdaptersInitialized);

                    IPermutationAdapter ifcpa = r.ProcessList[processIndex].Adapter as IPermutationAdapter;

                    (r.ProcessList[processIndex].Adapter as IPermutationAdapter).SetIteration(i);

                    ret.Add(r);
                }
            }

            return ret;
        }

        public class RoutineHelper
        {
            public RoutineLauncher Launcher { get; set; }
            public int CurrentLevel { get; set; }


            public RoutineHelper(RoutineBatchControl control)
            {
                this.Control = control;
                if (this.Control.loaderControl != null)
                {
                    this.Control.loaderControl.Canceled += new EventHandler(loaderControl_Canceled);
                }
            }

            void loaderControl_Canceled(object sender, EventArgs e)
            {
                this.Launcher.CancelEverything();
                this.Control.runIsDone(this, true);
            }

          
            public RoutineBatchControl Control 
            { 
                get; 
                private set; 
            }



            private ManualResetEvent uiholder = new ManualResetEvent(true);
            private ManualResetEvent backholder = new ManualResetEvent(true);

            private static string storageErrorPath = Paths.DefaultPath.LocalPath + "Program Error Data\\Routines";

            public void RunLevel(object ex)
            {
                bool isCallingThreadUI = this.Control.Dispatcher.Thread == Thread.CurrentThread;

               
                uiholder.WaitOne();

              
                if (!isCallingThreadUI)
                {
                    this.Control.Dispatcher.BeginInvoke(new WaitCallback(this.RunLevel), ex);
                }
                else
                {
                    uiholder.Reset();

                    if (ex != null)
                    {
                        MessageBox.Show("There was an error in running the routine. Please export the routine and attach it to an error report. Error log at " + ExceptionLogger.WriteDirectory);
                        ExceptionLogger.WriteCustomExceptionMessage(ex.ToString());
                        this.Control.runIsDone(this, true);
                        return;
                    }

                    if (this.CurrentLevel < this.Launcher.MaxLevel)
                    {
                        try
                        {
                            //Launch expansion here!  For ALL routines.
                            foreach (Routine routine in this.Launcher.Routines)
                            {
                                for (int i = routine.ProcessingStack[this.CurrentLevel].Count - 1; i >= 0; i--)
                                {
                                    Process thisProc = routine.ProcessingStack[this.CurrentLevel][i];
                                    if (thisProc is PermutationProcess)
                                    {

                                        //Forward values before expanding, so the iteration count can be dynamic, etc
                                        routine.UpdateLinksForProcess(thisProc);
                                        routine.ExpandProcess(thisProc as PermutationProcess, this.CurrentLevel);
                                    }
                                }
                            }
                        }
                        catch (Exception ex2)
                        {
                            MessageBox.Show("There was an error in running the routine. Please export the routine and attach it to an error report. Error log at " + ExceptionLogger.WriteDirectory);
                            ExceptionLogger.WriteCustomExceptionMessage(ex2.ToString());
                            this.Control.runIsDone(this, true);
                            return;
                        }

                        this.Launcher.SpinOffBackGround(this, this.CurrentLevel);
                        if(this.Control != null && this.Control.loaderControl != null)
                            this.Control.loaderControl.AwaitingUserInput = true;
                        this.Launcher.RunUILevel(this.CurrentLevel);
                        if (this.Control != null && this.Control.loaderControl != null)                       
                            this.Control.loaderControl.AwaitingUserInput = false;
                    }
                    else
                    {
                        this.Control.runIsDone(this, false);
                    }

                    if (this.Control != null && this.Control.loaderControl != null)
                    {
                        this.Control.loaderControl.StepsCompleted = this.Launcher.TotalProcsCompleted;
                        this.Control.loaderControl.UserStepsCompleted = this.Launcher.UserProcsCompleted;
                        BindingExpression e = this.Control.loaderControl.bar.GetBindingExpression(ProgressBar.ValueProperty);
                        e.UpdateTarget();
                    }

                    this.CurrentLevel++;
                    uiholder.Set();
                }
            }
        }

        public event EventHandler RunCompleted;

        private void runIsDone(object help, bool cancelled)
        {
            if (!cancelled)
            {
                RoutineHelper helper = help as RoutineHelper;
                foreach (List<RoutineBatchOutputPoint> listBOP in helper.Launcher.RoutineOutput)
                {
                    foreach (RoutineBatchOutputPoint BOP in listBOP)
                    {
                        if (!(BOP.OutputData is System.Collections.ICollection))
                        {
                            if (BOP.OutputData is ILabel)
                            {
                                BOP.Name = BOP.OutputData.ToString();
                            }

                            OutputList.Add(BOP);
                        }
                        else
                        {
                            

                            foreach (object o in (BOP.OutputData as System.Collections.ICollection))
                            {
                                if (!(o is IBatchViewable) && !(o is IBatchWritable))
                                    continue;
                                RoutineBatchOutputPoint newPoint = new RoutineBatchOutputPoint();
                                newPoint.Name = BOP.Name;
                                newPoint.ProcessingLevel = BOP.ProcessingLevel;
                                newPoint.SourceData = BOP.SourceData;
                                newPoint.SourceProcessName = BOP.SourceProcessName;
                                newPoint.OutputData = o;
                                if(o is ILabel)
                                    newPoint.Name = o.ToString();


                                OutputList.Add(newPoint);

                            }
                        }
                    }
                }

                EventHandler handle = this.RunCompleted;
                if (handle != null)
                {
                    handle(this, new EventArgs());
                }
            }

            ListCollectionView collection = this.outputView.ItemsSource as ListCollectionView;
            //collection.GroupDescriptions
            toRunList.Clear();

            if (this.layerAdorner != null)
            {
                AdornerLayer layer = AdornerLayer.GetAdornerLayer(this.AddButton);
                if (layer != null)
                {
                    layer.Remove(this.layerAdorner);
                    this.layerAdorner = null;
                    this.loaderControl = null;
                }
            }
        }


        private RoutineLoaderControl loaderControl;
        private Adorner layerAdorner;
        private Routine runtimeRoutine;

        private void run(int index, object[] args)
        {
            //OutputList.Clear();
            //if () return;

            if (index < 0) return;

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(this.AddButton);
            if (layer != null)
            {
                this.loaderControl = new RoutineLoaderControl();
                this.loaderControl.SetBinding(UserControl.WidthProperty, new Binding() { Source = this, Path = new PropertyPath("ActualWidth") });
                this.loaderControl.SetBinding(UserControl.HeightProperty, new Binding() { Source = this, Path = new PropertyPath("ActualHeight") });

                this.layerAdorner = new MesosoftCommon.Adorners.UIElementAdorner(layer, this.loaderControl);
                layer.Add(this.layerAdorner);
            }


            RoutineLauncher rl = new RoutineLauncher();

           // List<Routine> routineList = new List<Routine>();

            foreach (object o in args)
            {
                Routine r = null;
                if (this.runtimeRoutine != null)
                    r = this.runtimeRoutine;
                else r = RoutineCentral.GetRoutine(index, this.Controller.GetType().ToString());
                
                r.Args = o;
                r.AdaptersInitialized += new EventHandler<FlowControlAdapterInitArgs>(r_AdaptersInitialized);
                r.PrepareForExecution();
                rl.Routines.Add(r);
                //routineList.Add(r);
            }

            //List<Routine> expandedRoutines = expandPermutationProcesses(routineList);

            //foreach (Routine routine in expandedRoutines)
            //{
            //    rl.Routines.Add(routine);   
            //}

            RoutineHelper helper = new RoutineHelper(this);
            helper.CurrentLevel = 0;
            helper.Launcher = rl;
            rl.SetupExecution();

            if (this.loaderControl != null)
            {
                this.loaderControl.MaxSteps = rl.TotalProcCount;
                this.loaderControl.MaxUserSteps = rl.UserProcCount;
            }


            helper.RunLevel(null);


        }

        public void Run(string routineName, object[] toRunArgs)
        {
            int index = RoutineCentral.GetRoutineIndex(routineName, this.Controller.GetType().ToString());
            if (index < 0) return;
            this.run(index, toRunArgs);
        }

        public void Run(int index, object[] toRunArgs)
        {
            this.run(index, toRunArgs);
        }

     //   private BackgroundWorker worker;

        private void RunRoutine_Click(object sender, RoutedEventArgs e)
        {
            //this.worker = new BackgroundWorker();
            //this.worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            //this.worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            //this.worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            //this.worker.WorkerReportsProgress = true;
            //this.worker.WorkerSupportsCancellation = true;

            try
            {
                this.Run(RoutineListDisplay.SelectedIndex, this.selectedData.Items.Cast<object>().ToArray());
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error starting to run the routine: " + ex.Message + ". Check the log.");
                ExceptionLogger.WriteException(ex, "Starting to run the rounte");
                runIsDone(null, true);
            }

//            this.worker.RunWorkerAsync(rl);


        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //if (!e.Cancelled)
            //{
            //    RoutineLauncher rl = e.Result as RoutineLauncher;

            //    foreach (List<RoutineBatchOutputPoint> listBOP in rl.RoutineOutput)
            //    {
            //        foreach (RoutineBatchOutputPoint BOP in listBOP)
            //        {
            //            OutputList.Add(BOP);
            //        }
            //    }

            //    this.toRunList.Clear();
            //}


        }

        void r1_ExecutionProgressChanged(object sender, RoutineExecutionProgressEventArgs e)
        {
            //this.worker.ReportProgress(0, e);
            //e.Cancel = this.worker.CancellationPending;
        }


        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //RoutineExecutionProgressEventArgs args = e.UserState as RoutineExecutionProgressEventArgs;

        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //RoutineLauncher rl = e.Argument as RoutineLauncher;
            //rl.ExecutionProgressChanged += new EventHandler<RoutineExecutionProgressEventArgs>(r1_ExecutionProgressChanged);
            //rl.ExecuteInBatch(null);
            //e.Result = rl;
        }

        
        

        void r_AdaptersInitialized(object sender, FlowControlAdapterInitArgs e)
        {
            Routine routine = sender as Routine;

            if (this.controller != null)
            {
                this.controller.AdaptersInitialized(routine, e.InitializedAdapters.ToArray());
            }
                     
        }

        private void addAvailableAdapters(RoutineManager rm)
        {
            IFlowControlAdapter[] adapters = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IFlowControlAdapter>(null);
            foreach (IFlowControlAdapter adapter in adapters)
            {
                AdapterDescriptionAttribute[] attr = MesosoftCommon.Utilities.Reflection.Common.GetAttributes<AdapterDescriptionAttribute>(adapter);
                if (attr == null) continue;
                if (attr.Any(at => at.Description == "Common") && !rm.AvailableAdapters.Any(ad => ad.Name == adapter.Name && ad.Category == adapter.Category))
                {
                    rm.AvailableAdapters.Add(adapter);
                }
            }

            IBatchFlowable[] flows = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<IBatchFlowable>(null);
            foreach (IBatchFlowable adapter in flows)
            {
                AdapterDescriptionAttribute[] attr = MesosoftCommon.Utilities.Reflection.Common.GetAttributes<AdapterDescriptionAttribute>(adapter);
                if (attr == null) continue;
                if (attr.Any(at => at.Description == "Common" ) && !rm.AvailableAdapters.Any(ad => ad.Name == adapter.ToString() && ad.Category == adapter.ToString()))
                {
                    rm.AvailableAdapters.Add(new FlowableBatchProcessorAdapter(adapter));
                }
            }

            MatlabLoader.BuildAdapterInfos("Common");

            MatlabAdapter[] matlabs = MatlabLoader.CreateAdapters("Common");
            if (matlabs != null)
            {
                foreach (MatlabAdapter mlab in matlabs)
                {
                    rm.AvailableAdapters.Add(mlab);
                }
            }

            if (this.controller != null)
                this.controller.AddAvailableAdapters(rm);
        }
      

        private void EditRoutine_Click(object sender, RoutedEventArgs e)
        {
            if (RoutineListDisplay.SelectedIndex == -1) return;
            try
            {
                Routine r = RoutineCentral.GetRoutine(RoutineListDisplay.SelectedIndex, this.Controller.GetType().ToString());
                RoutineManager rm = RoutineCentral.GetRoutineManager(RoutineListDisplay.SelectedIndex, r,
                    this.Controller.GetType().ToString());
                RoutineEdit(r, rm);
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error editing the routine: " + ex.Message);
            }
        }

        public void RoutineEdit(Routine r, RoutineManager rm)
        {
            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(rm);

            rm.AttachPermProcMouseEvents();

            this.addAvailableAdapters(rm);

            rm.ControllerTypeString = this.Controller.GetType().ToString();

            rm.FullRefresh();
            rm.Show();
            rm.RegenerateLinesFromLinks();
        }

        private void NewRoutine_Click(object sender, RoutedEventArgs e)
        {
            MakeNewRoutine();
        }

        public void MakeNewRoutine()
        {
            RoutineManager rm = new RoutineManager();
            Routine r = new Routine();
            rm.ManagedRoutine = r;
            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(rm);

            this.addAvailableAdapters(rm);
            rm.ControllerTypeString = this.Controller.GetType().ToString();
            rm.Show();
        }



        private void EditParameters_Click(object sender, RoutedEventArgs e)
        {
            if (this.RoutineListDisplay.SelectedIndex < 0) return;

            if (this.runtimeRoutine == null)
                this.runtimeRoutine = RoutineCentral.GetRoutine(RoutineListDisplay.SelectedIndex, this.Controller.GetType().ToString());

            RuntimeEditControl rtec = new RuntimeEditControl();
            rtec.Source = this.runtimeRoutine;

            foreach (Process proc in this.runtimeRoutine.ProcessList)
            {
                proc.StorePreEditValues();
            }

            Window win = new Window();
            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(win);
            win.Title = "Runtime Parameter Edits";
            win.Content = rtec;
            win.Height = rtec.Height + 32;
            win.Width = rtec.Width + 8;
            win.Show();
            win.Closing += new CancelEventHandler(win_Closing);
        }

        void win_Closing(object sender, CancelEventArgs e)
        {
            RuntimeEditControl rtec = (sender as Window).Content as RuntimeEditControl;
            
            //Have to move the focus to get the currently selected field to update
            TextBox focusedTextBox = Keyboard.FocusedElement as TextBox;
            if (focusedTextBox != null)
            {
                focusedTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            }

            object gridObj = rtec.PropertyGrid.Instance;
            PropertyInfo[] infos = gridObj.GetType().GetProperties();

            foreach (PropertyInfo info in infos)
            {
                string propName = info.Name.Substring(3,info.Name.Length - 3);
                object val = info.GetValue(gridObj, null);
                
                //Locate the correct process based on the numerical prefix in the name
                Process targetProc = null;
                string propID = info.Name.Substring(0, 2);
                foreach (Process proc in rtec.Source.ProcessList)
                {
                    if (propID == proc.Name.Substring(0, 2))
                    {
                        targetProc = proc;
                        break;
                    }
                }

                if (targetProc == null)
                    continue;

                PropertyListing pl = PropertyListing.FindFirstByName(targetProc.PropertiesList, propName);


                if (pl.Value != val)
                {
                    pl.Value = val;
                    pl.UsingAdapterDefault = false;
                }
            }

            foreach (Process proc in this.runtimeRoutine.ProcessList)
            {


                proc.CheckForChangesAndNotify();
            }
        }


        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.Controller != null)
                this.Controller.AddButtonClicked();
        }

        private void buttonView_Click(object sender, RoutedEventArgs e)
        {
            List<IBatchViewable> toView = new List<IBatchViewable>();
            foreach (RoutineBatchOutputPoint o in this.outputView.SelectedItems)
            {
                if (o.OutputData is IBatchViewable)
                {
                    toView.Add(o.OutputData as IBatchViewable);
                }
            }

            while (toView.Count > 0)
            {
                IBatchViewable[] viewing;
                Control[] toShow = toView[0].View(toView.ToArray(), out viewing);

                if (toShow != null)
                {

                    foreach (Control cont in toShow)
                    {
                        if (cont is Window)
                        {
                            (cont as Window).ShowDialog();
                        }
                        else
                        {

                            ContentBox cb = new ContentBox();
                            cb.ContentElement = cont;
                            cb.Buttons = ContentBoxButton.OK;
                            cb.Title = "Routine Output";
                            cb.ShowDialog();
                        }
                    }
                }
                
                if (viewing == null) break;
                toView.Clear();
                toView.AddRange(viewing);
            }
        }

        private void buttonWrite_Click(object sender, RoutedEventArgs e)
        {
            List<IBatchWritable> toWrite = new List<IBatchWritable>();
            foreach (RoutineBatchOutputPoint o in this.outputView.SelectedItems)
            {
                if (o.OutputData is IBatchWritable)
                {
                    toWrite.Add(o.OutputData as IBatchWritable);
                }
            }

           

            while (toWrite.Count > 0)
            {
                IBatchWritable[] writeable = toWrite[0].WriteToDisk(toWrite.ToArray());
                if (writeable == null) break;
                toWrite.Clear();
                toWrite.AddRange(writeable);
            }
        }

        private void CollectionViewSource_Filter_1(object sender, FilterEventArgs e)
        {
            e.Accepted = false;

            RoutineBatchOutputPoint point = e.Item as RoutineBatchOutputPoint;
            if (point != null)
            {
                if (point.OutputData is IBatchViewable || point.OutputData is IBatchWritable)
                    e.Accepted = true;
            }
        }

        private void CollectionViewSource_Filter_2(object sender, FilterEventArgs e)
        {

        }

        private void EditEpochDescriptorSet_Click(object sender, RoutedEventArgs e)
        {
            EpochDescSetWindow edsw = new EpochDescSetWindow();
            edsw.ShowDialog();
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            object[] toRemove = this.selectedData.SelectedItems.OfType<object>().ToArray();
            foreach (object o in toRemove)
            {
                this.toRunList.Remove(o);
            }
        }

        private void header_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock block = e.OriginalSource as TextBlock;
            CollectionViewGroup group = block.DataContext as CollectionViewGroup;


            if (group == null) return;

            bool deselect = true;
            foreach (object obj in group.Items)
            {
                if (!this.outputView.SelectedItems.Contains(obj))
                {
                    deselect = false;
                }
            }

            foreach (object obj in group.Items)
            {
                if (deselect)
                    this.outputView.SelectedItems.Remove(obj);
                else this.outputView.SelectedItems.Add(obj);
            }

        }

        private void buttonDelete_Click(object sender, RoutedEventArgs e)
        {
            List<object> toRemove = new List<object>();
            foreach (object obj in this.outputView.SelectedItems)
            {
                toRemove.Add(obj);
            }

            foreach (object obj in toRemove)
            {
                this.outputList.Remove(obj as RoutineBatchOutputPoint);
            }
        }

        private void buttonClear_Click(object sender, RoutedEventArgs e)
        {
            this.outputList.Clear();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        private void ManageRoutines_Click(object sender, RoutedEventArgs e)
        {
            RoutineManagementControl rmc = new RoutineManagementControl();
            rmc.ControllerString = this.Controller.GetType().ToString();

            Window win = new Window();
            win.Title = "Routine Management";
            win.Content = rmc;
            win.Height = rmc.Height + 32;
            win.Width = rmc.Width + 8;
            win.Show();
        }

        private void RoutineListDisplay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Null the runtimeRoutine because the selection has changed
            this.runtimeRoutine = null;
        }
    }
}
