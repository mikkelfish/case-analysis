﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Collections;

namespace CeresBase.FlowControl
{
    class PropertyTextConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts the obnoxiously long text for collections into something much more compact.  (Array) and (Collection) are displayed
        /// instead of the typical array and IList type displays, respectively.
        /// Extended to convert the appropriate types as well!
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return "(Null)";

            if (value.GetType().IsArray) return "(Array)";

            if (value.GetType().GetInterfaces().Contains(typeof(IList))) return "(Collection)";

            if (value.GetType() == typeof(PropertyLink)) return "(Link)";

            if (value.GetType() == typeof(PropertyMultiLink)) return "(N:1 Link)";

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}


