﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public class PropertyMultiLink
    {
        public List<PropertyLink> PropertyLinks;

        public PropertyMultiLink()
        {
            PropertyLinks = new List<PropertyLink>();
        }

        public bool ContainsLinkToProperty(Process host, string property)
        {
            foreach (PropertyLink link in PropertyLinks)
            {
                if (link.HostProcess == host && link.PropertyName == property) return true;
            }
            return false;
        }
    }

    public class PropertyMultiLinkSerializer
    {
        public List<PropertyLinkSerializer> PropertyLinks { get; set; }

        static public PropertyMultiLinkSerializer Serialize(PropertyMultiLink toSerialize, Routine hostRoutine)
        {
            PropertyMultiLinkSerializer ret = new PropertyMultiLinkSerializer();

            ret.PropertyLinks = new List<PropertyLinkSerializer>();
            foreach (PropertyLink prop in toSerialize.PropertyLinks)
            {
                ret.PropertyLinks.Add(PropertyLinkSerializer.Serialize(prop, hostRoutine));
            }

            return ret;
        }

        static public PropertyMultiLink Deserialize(PropertyMultiLinkSerializer toDeserialize, Routine hostRoutine)
        {
            PropertyMultiLink ret = new PropertyMultiLink();

            ret.PropertyLinks = new List<PropertyLink>();
            foreach (PropertyLinkSerializer prop in toDeserialize.PropertyLinks)
            {
                ret.PropertyLinks.Add(PropertyLinkSerializer.Deserialize(prop, hostRoutine));
            }

            return ret;
        }
    }
}
