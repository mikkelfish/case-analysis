﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public class RoutineExecutionProgressEventArgs : EventArgs
    {
        public int NumberExecuted { get; set; }
        public int TotalToExecute { get; set; }

        public int TotalUserExecuted { get; set; }
        public int TotalUserToExecute { get; set; }

        public object UserState { get; set; }

        public bool Cancel { get; set; }
    }
}
