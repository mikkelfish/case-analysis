﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Interaction logic for StorageProcessControl.xaml
    /// </summary>
    public partial class StorageProcessControl : UserControl
    {
        public StorageProcessControl()
        {
            InitializeComponent();
        }

        private void InputButtonClicked(object sender, RoutedEventArgs e)
        {
        }

        private void OutputButtonClicked(object sender, RoutedEventArgs e)
        {

        }
    }
}
