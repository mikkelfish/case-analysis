﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Interaction logic for RoutineLoaderControl.xaml
    /// </summary>
    public partial class RoutineLoaderControl : UserControl
    {


        public int StepsCompleted
        {
            get { return (int)GetValue(PercentageCompletedProperty); }
            set { SetValue(PercentageCompletedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PercentageCompleted.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PercentageCompletedProperty =
            DependencyProperty.Register("PercentageCompleted", typeof(int), typeof(RoutineLoaderControl));



        public bool AwaitingUserInput
        {
            get { return (bool)GetValue(AwaitingUserInputProperty); }
            set { SetValue(AwaitingUserInputProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AwaitingUserInput.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AwaitingUserInputProperty =
            DependencyProperty.Register("AwaitingUserInput", typeof(bool), typeof(RoutineLoaderControl));




        public int MaxSteps
        {
            get { return (int)GetValue(MaxStepsProperty); }
            set { SetValue(MaxStepsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaxSteps.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxStepsProperty =
            DependencyProperty.Register("MaxSteps", typeof(int), typeof(RoutineLoaderControl));




        public int MaxUserSteps
        {
            get { return (int)GetValue(MaxUserStepsProperty); }
            set { SetValue(MaxUserStepsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaxUserSteps.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxUserStepsProperty =
            DependencyProperty.Register("MaxUserSteps", typeof(int), typeof(RoutineLoaderControl));



        public int UserStepsCompleted
        {
            get { return (int)GetValue(UserStepsCompletedProperty); }
            set { SetValue(UserStepsCompletedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UserStepsCompleted.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UserStepsCompletedProperty =
            DependencyProperty.Register("UserStepsCompleted", typeof(int), typeof(RoutineLoaderControl));



        public event EventHandler Canceled;


        public RoutineLoaderControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.Canceled != null)
            {
                this.Canceled(this, new EventArgs());
            }
        }
    }
}
