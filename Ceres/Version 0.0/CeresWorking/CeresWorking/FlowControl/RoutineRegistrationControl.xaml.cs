﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CeresBase.FlowControl
{
    /// <summary>
    /// Interaction logic for RoutineRegistrationControl.xaml
    /// </summary>
    public partial class RoutineRegistrationControl : UserControl
    {
        public RoutineRegistrationControl()
        {
            InitializeComponent();
        }
    }
}
