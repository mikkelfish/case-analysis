﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.FlowControl.Adapters;

namespace CeresBase.FlowControl
{
    class PermutationAdapter : IPermutationAdapter
    {
        #region IPermutationAdapter Members

        public int IterationCount
        {
            get { throw new NotImplementedException(); }
        }

        public void SetIteration(int iteration)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IFlowControlAdapter Members

        public int ID
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Process HostProcess
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Category
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool HasUserInteraction
        {
            get { throw new NotImplementedException(); }
        }

        public event EventHandler<FlowControlProcessEventArgs> RunComplete;

        public string[] SupplyPropertyNames()
        {
            throw new NotImplementedException();
        }

        public object SupplyPropertyValue(string sourcePropertyName)
        {
            throw new NotImplementedException();
        }

        public void ReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            throw new NotImplementedException();
        }

        public bool CanReceivePropertyValue(object propertyValue, string targetPropertyName)
        {
            throw new NotImplementedException();
        }

        public FlowControlParameterDescriptor[] TypesSuppliable(string sourcePropertyName)
        {
            throw new NotImplementedException();
        }

        public FlowControlParameterDescriptor[] TypesReceivable(string targetPropertyName)
        {
            throw new NotImplementedException();
        }

        public object SupplyPropertyValueAs(string sourcePropertyName, FlowControlParameterDescriptor supplyType)
        {
            throw new NotImplementedException();
        }

        public void ReceivePropertyValueAs(object propertyValue, string targetPropertyName, FlowControlParameterDescriptor receiveType)
        {
            throw new NotImplementedException();
        }

        public Type PropertyType(string targetPropertyName)
        {
            throw new NotImplementedException();
        }

        public void RunAdapter()
        {
            throw new NotImplementedException();
        }

        public object[] GetValuesForSerialization()
        {
            throw new NotImplementedException();
        }

        public void LoadValuesFromSerialization(object[] values)
        {
            throw new NotImplementedException();
        }

        public bool CanEditProperty(string targetPropertyname)
        {
            throw new NotImplementedException();
        }

        public bool CanReceivePropertyLinks(string targetPropertyname)
        {
            throw new NotImplementedException();
        }

        public bool CanSendPropertyLinks(string targetPropertyname)
        {
            throw new NotImplementedException();
        }

        public bool IsOutputProperty(string targetPropertyname)
        {
            throw new NotImplementedException();
        }

        public bool ShouldSerialize(string targetPropertyname)
        {
            throw new NotImplementedException();
        }

        public ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IFlowControlAdapter Members


        public object SupplyPropertyDescription(string propertyName)
        {
            return null;
        }

        public object SupplyAdapterDescription()
        {
            return null;
        }

        #endregion
    }
}
