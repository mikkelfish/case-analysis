﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.UI;
using CeresBase.Data;
using CeresBase.FlowControl.Adapters;

namespace CeresBase.FlowControl
{
    [AdapterDescription("Common")]
    public class GraphPickerAdapter : AssociateAdapterBase
    {
        #region IFlowControlAdapter Members

        public override event EventHandler<FlowControlProcessEventArgs> RunComplete;

        [AssociateWithProperty("Input Graph", ReadOnly=true)]
        public IGraphable InputData { get; set; }

        [AssociateWithProperty("Selected Output", ReadOnly = true, DisableLink=true, IsOutput=true)]
        public int SelectedOutput { get; set; }

        

        public override void RunAdapter()
        {
            GraphPickerControl gpc = new GraphPickerControl();

            DataSeries ds = new DataSeries(this.InputData.XData, this.InputData.YData);

            gpc.SimpleGraphDisplay.IsCoordinateBoxLocked = false;
            gpc.SimpleGraphDisplay.DisplayCoordinateBox = true;

            gpc.SimpleGraphDisplay.SeriesList.Add(ds);
            gpc.SimpleGraphDisplay.Title = this.InputData.Label;
            //gpc.SimpleGraphDisplay.AddDraggableText(this.InputData.Label, new System.Windows.Point(60, 20));

            ContentBox cb = new ContentBox();
            cb.ContentElement = gpc;
            cb.Buttons = ContentBoxButton.OK;
            if (this.InputData.Label != null)
                cb.Title = this.InputData.Label;
            cb.ContentBoxClosed += new EventHandler<ContentBoxEventArgs>(ContentBox_ContentBoxClosed);
            cb.Width = 800;
           //cb.WindowStartupLocation = System.Windows.WindowStartupLocation.;
            cb.ShowDialog();
        }

        void ContentBox_ContentBoxClosed(object sender, ContentBoxEventArgs e)
        {
            ContentBox box = sender as ContentBox;
            GraphPickerControl gpc = box.ContentElement as GraphPickerControl;

            int res;
            int.TryParse(gpc.ValueBox.Text, out res);
            this.SelectedOutput = res;
        }


        public override object[] GetValuesForSerialization()
        {
            return null;
        }

        public override void LoadValuesFromSerialization(object[] values)
        {
            
        }



        public override string Name
        {
            get
            {
                return "Graph Picker";
            }
            set
            {
               
            }
        }

        public override string Category
        {
            get
            {
                return "Runtime Tools";
            }
            set
            {

            }
        }

        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            if (targetPropertyname == "Input Graph")
            {
                if (targetValue == null) return new ValidationStatus(ValidationState.Fail, "Need to connect an input graph");
            }
            return new ValidationStatus(ValidationState.Pass);
        }


        #endregion



        #region ICloneable Members

        public override object Clone()
        {
            return new GraphPickerAdapter();
        }

        #endregion


        public override bool HasUserInteraction
        {
            get { return true; }
        }
      
    }

    public abstract class StorageAdapter : AssociateAdapterBase
    {
          
        #region IFlowControlAdapter Members


        public override bool ShouldSerialize(string targetPropertyname)
        {
            return true;
        }

        #endregion

        #region IFlowControlAdapter Members


        public override ValidationStatus ValidateProperty(string targetPropertyname, object targetValue)
        {
            return new ValidationStatus(ValidationState.Pass);
        }

        #endregion

        #region IFlowControlAdapter Members


        public override bool CanSendPropertyLinks(string targetPropertyname)
        {
            return true;
        }

        public override bool IsOutputProperty(string targetPropertyname)
        {
            return false;
        }

        #endregion

        #region IFlowControlAdapter Members


        public override bool HasUserInteraction
        {
            get { return false; }
        }

        #endregion

        public override object SupplyPropertyDescription(string propertyName)
        {
            return null;
        }

        public override object SupplyAdapterDescription()
        {
            return null;
        }
    }

}
