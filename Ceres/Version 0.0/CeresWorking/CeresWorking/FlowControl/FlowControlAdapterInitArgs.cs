﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.FlowControl
{
    public class AdapterInitPair
    {
        public string HostProcessName { get; set; }
        public IFlowControlAdapter Adapter { get; set; }
    }

    public class FlowControlAdapterInitArgs : EventArgs
    {
        private List<AdapterInitPair> initializedAdapters = new List<AdapterInitPair>();
        public List<AdapterInitPair> InitializedAdapters
        {
            get
            {
                return initializedAdapters;
            }
            set
            {
                initializedAdapters = value;
            }
        }
    }
}
