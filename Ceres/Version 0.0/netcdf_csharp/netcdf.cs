using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security;

namespace NetCDFCSharp
{
    public static class NetCDF
    {
        public static nc_type FromType(Type type)
        {
            if (type == typeof(sbyte)) return nc_type.NC_BYTE;
            if (type == typeof(byte)) return nc_type.NC_CHAR;
            if (type == typeof(short)) return nc_type.NC_SHORT;
            if (type == typeof(int)) return nc_type.NC_INT;
            if (type == typeof(float)) return nc_type.NC_FLOAT;
            if (type == typeof(double)) return nc_type.NC_DOUBLE;
            throw new Exception(); //TODO CHANGE
        }

        public enum nc_type
        {
            NC_NAT = 0,
            NC_BYTE = 1,
            NC_CHAR = 2,
            NC_SHORT = 3,
            NC_INT = 4,
            NC_FLOAT = 5,
            NC_DOUBLE = 6
        }
        
        public class Constants
        {
            public const sbyte NC_FILL_BYTE = -127;
            public const byte NC_FILL_CHAR = 0;
            public const short NC_FILL_SHORT = -32767;
            public const int NC_FILL_INT = -2147483647;
            public const float NC_FILL_FLOAT = 9.9692099683868690e+36f;
            public const double NC_FILL_DOUBLE = 9.9692099683868690e+36;
            public const string _FillValue = "_FillValue";
            public const int NC_FILL = 0;
            public const int NC_NOFILL = 0x100;
            public const int NC_NOWRITE = 0;
            public const int NC_WRITE = 0x1;
            public const int NC_CLOBBER = 0;
            public const int NC_NOCLOBBER = 0x4;
            public const int NC_64BIT_OFFSET = 0x0200;
            public const int NC_SHARE = 0x0800;
            public const int NC_STRICT_NC3 = 0x8;
            public const int NC_LOCK = 0x0400;
            public const int NC_FORMAT_CLASSIC = 1;
            public const int NC_FORMAT_64BIT = 2;
            public const int NC_FORMAT_NETCDF4 = 3;
            public const int NC_FORMAT_NETCDF4_CLASSIC = 4;
            public const int NC_SIZEHINT_DEFAULT = 0;
            public const long NC_ALIGN_CHUNK = -1;
            public const long NC_UNLIMITED = 0;
            public const int NC_GLOBAL = -1;
            public const int NC_NOAXISTYPE = 0;
            public const int NC_LATITUDE = 1;
            public const int NC_LONGITUDE = 2;
            public const int NC_GEOX = 3;
            public const int NC_GEOY = 4;
            public const int NC_GEOZ = 5;
            public const int NC_HEIGHT_UP = 6;
            public const int NC_HEIGHT_DOWN = 7;
            public const int NC_PRESSURE = 8;
            public const int NC_TIME = 9;
            public const int NC_RADAZ = 10;
            public const int NC_RADEL = 11;
            public const int NC_RADDIST = 12;
            public const int NC_MAX_DIMS = 1024;
            public const int NC_MAX_ATTRS = 8192;
            public const int NC_MAX_VARS = 8192;
            public const int NC_MAX_NAME = 256;
            public const int NC_MAX_VAR_DIMS = NC_MAX_DIMS;
            public const int NC_NOERR = 0;
            public const int NC2_ERR = -1;
            public const int NC_EBADID = -33;
            public const int NC_ENFILE = -34;
            public const int NC_EEXIST = -35;
            public const int NC_EINVAL = -36;
            public const int NC_EPERM = -37;
            public const int NC_ENOTINDEFINE = -38;
            public const int NC_EINDEFINE = -39;
            public const int NC_EINVALCOORDS = -40;
            public const int NC_EMAXDIMS = -41;
            public const int NC_ENAMEINUSE = -42;
            public const int NC_ENOTATT = -43;
            public const int NC_EMAXATTS = -44;
            public const int NC_EBADTYPE = -45;
            public const int NC_EBADDIM = -46;
            public const int NC_EUNLIMPOS = -47;
            public const int NC_EMAXVARS = -48;
            public const int NC_ENOTVAR = -49;
            public const int NC_EGLOBAL = -50;
            public const int NC_ENOTNCE = -51;
            public const int NC_ESTS = -52;
            public const int NC_EMAXNAME = -53;
            public const int NC_EUNLIMIT = -54;
            public const int NC_ENORECVARS = -55;
            public const int NC_ECHAR = -56;
            public const int NC_EEDGE = -57;
            public const int NC_ESTRIDE = -58;
            public const int NC_EBADNAME = -59;
            public const int NC_ERANGE = -60;
            public const int NC_ENOMEM = -61;
            public const int NC_EVARSIZE = -62;
            public const int NC_EDIMSIZE = -63;
            public const int NC_ETRUNC = -64;
        }

        private static void checkError(int error)
        {
            if(error != Constants.NC_NOERR)
            {
                string err = _nc_strerror(error);
                throw new Exception(err); //TODO CHANGE
            }
        }
        
#region Basic File Functions

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_libvers"), SuppressUnmanagedCodeSecurity]
        public static extern string nc_inq_libvers(); //TODO POSSIBLE ERROR

        [DllImport("netcdf.dll", EntryPoint = "nc_strerror"), SuppressUnmanagedCodeSecurity]
        private static extern string _nc_strerror(int ncerr); //TODO POSSIBLE ERROR

        [DllImport("netcdf.dll", EntryPoint = "nc_create"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_create(string path, int cmode, ref int ncidp);
        public static int nc_create(string path, int cmode)
        {
            int id = 0;
            checkError(_nc_create(path, cmode, ref id));
            return id;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_open"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_open(string path, int mode, ref int ncidp);
        public static int nc_open(string path, int mode)
        {
            int id = 0;
            int error = _nc_open(path, mode, ref id);
            if (error == Constants.NC_ENFILE) return -1;
            checkError(error);
            return id;
        }

        public static bool IsNetCDF(string path)
        {
            int id = 0;
            int error = _nc_open(path, NetCDF.Constants.NC_SHARE, ref id);
            if (error != Constants.NC_NOERR) return false;
            NetCDF.nc_close(id);
            return true;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_set_fill"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_set_fill(int ncid, int fillmode, ref int old_modep);
        public static int nc_set_fill(int ncid, int fillmode)
        {
            int old = 0;
            checkError(_nc_set_fill(ncid, fillmode, ref old));
            return old;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_redef"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_redef(int ncid);
        public static void nc_redef(int ncid)
        {
           checkError(_nc_redef(ncid));
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_enddef"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_enddef(int ncid);
        public static void nc_enddef(int ncid)
        {
            checkError(_nc_enddef(ncid));
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_sync"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_sync(int ncid);
        public static void nc_sync(int ncid)
        {
            checkError(_nc_sync(ncid));
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_abort"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_abort(int ncid);
        public static void nc_abort(int ncid)
        {
            checkError(_nc_abort(ncid));
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_close"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_close(int ncid);
        public static void nc_close(int ncid)
        {
            checkError(_nc_close(ncid));
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq(int ncid, ref int ndimsp, ref int nvarsp, ref int nattsp, ref int unlimdimidp);
        public static void nc_inq(int ncid, ref int num_dims, ref int num_vars, ref int num_atts, ref int unlimited_dim)
        {
            checkError(_nc_inq(ncid, ref num_dims, ref num_vars, ref num_atts, ref unlimited_dim));
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_ndims"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_ndims(int ncid, ref int ndimsp);
        public static int nc_inq_ndims(int ncid)
        {
            int numdims =0;
            checkError(_nc_inq_ndims(ncid,ref numdims));
            return numdims;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_nvars"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_nvars(int ncid, ref int nvarsp);
        public static int nc_inq_nvars(int ncid)
        {
            int numvars = 0;
            checkError(_nc_inq_nvars(ncid, ref numvars));
            return numvars;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_natts"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_natts(int ncid, ref int nattsp);
        public static int nc_inq_natts(int ncid)
        {
            int numatts = 0;
            checkError(_nc_inq_natts(ncid, ref numatts));
            return numatts;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_unlimdim"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_unlimdim(int ncid, ref int unlimdimidp);
        public static int nc_inq_unlimdim(int ncid)
        {
            int unlimdim = 0;
            checkError(_nc_inq_unlimdim(ncid, ref unlimdim));
            return unlimdim;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_set_default_format"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_set_default_format(int format, ref int old_formatp);
        public static int nc_set_default_format(int format)
        {
            int old = 0;
            checkError(_nc_set_default_format(format, ref old));
            return old;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_format"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_format(int ncid, ref int formatp);
        public static int nc_inq_format(int ncid)
        {
            int format = 0;
            checkError(_nc_inq_format(ncid, ref format));
            return format;
        }

#endregion

#region Dimension Functions

        [DllImport("netcdf.dll", EntryPoint = "nc_def_dim"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_def_dim(int ncid, string name, uint len, ref int idp);
        public static int nc_def_dim(int ncid, string name, uint len)
        {
            int id = 0;
            checkError(_nc_def_dim(ncid, name, len, ref id));
            return id;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_dimid"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_dimid(int ncid, string name, ref int idp);
        /// <summary>
        /// Get the dimension id for the passed name
        /// </summary>
        /// <returns>Return -1 for not found</returns>
        public static int nc_inq_dimid(int ncid, string name)
        {
            int dimid = 0;
            int err = _nc_inq_dimid(ncid, name, ref dimid);
            if (err == Constants.NC_EBADNAME) return -1;
            checkError(err);
            return dimid;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_dim"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_dim(int ncid, int dimid, 
            [MarshalAs(UnmanagedType.LPStr)] StringBuilder name, ref int lenp);
        public static void nc_inq_dim(int ncid, int dimid, out string name, out int length)
        {
            StringBuilder builder = new StringBuilder(Constants.NC_MAX_NAME);
            length = 0;
            checkError(_nc_inq_dim(ncid, dimid, builder, ref length));
            name = builder.ToString();
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_dimname"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_dimname(int ncid, int dimid, 
            [MarshalAs(UnmanagedType.LPStr)] StringBuilder name);
        public static string nc_inq_dimname(int ncid, int dimid)
        {
            StringBuilder builder = new StringBuilder(Constants.NC_MAX_NAME);
            checkError(_nc_inq_dimname(ncid, dimid, builder));
            return builder.ToString();
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_dimlen"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_dimlen(int ncid, int dimid, ref int lenp);
        public static int nc_inq_dimlen(int ncid, int dimid)
        {
            int dimlen = 0;
            checkError(_nc_inq_dimlen(ncid, dimid, ref dimlen));
            return dimlen;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_rename_dim"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_rename_dim(int ncid, int dimid, string name);
        public static void nc_rename_dim(int ncid, int dimid, string name)
        {
            checkError(_nc_rename_dim(ncid, dimid, name));
        }

#endregion

#region Attribute Functions

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_att"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_att(int ncid, int varid, string name, ref nc_type xtypep, ref uint lenp);
        public static void nc_inq_att(int ncid, int varid, string name, out nc_type type, out uint length)
        {
            type = nc_type.NC_CHAR;
            length = 0;
            checkError(_nc_inq_att(ncid, varid, name, ref type, ref length));
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_attid"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_attid(int ncid, int varid, string name, ref int idp);
        public static int nc_inq_attid(int ncid, int varid, string name)
        {
            int id =0 ;
            checkError(_nc_inq_attid(ncid, varid, name, ref id));
            return id;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_atttype"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_atttype(int ncid, int varid, string name, ref nc_type xtypep);
        public static nc_type nc_inq_atttype(int ncid, int varid, string name)
        {
            nc_type type = nc_type.NC_CHAR;
            checkError(_nc_inq_atttype(ncid, varid, name, ref type));
            return type;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_attlen"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_attlen(int ncid, int varid, string name, ref uint lenp);
        public static uint nc_inq_attlen(int ncid, int varid, string name)
        {
            uint length = 0;
            checkError(_nc_inq_attlen(ncid, varid, name, ref length));
            return length;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_attname"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_attname(int ncid, int varid, int attnum, 
            [MarshalAs(UnmanagedType.LPStr)] StringBuilder name);
        public static string nc_inq_attname(int ncid, int varid, int attnum)
        {
            StringBuilder builder = new StringBuilder(Constants.NC_MAX_NAME);
            checkError(_nc_inq_attname(ncid, varid, attnum, builder));
            return builder.ToString();
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_copy_att"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_copy_att(int ncid_in, int varid_in, string name, int ncid_out, int varid_out);
        public static void nc_copy_att(int ncid_in, int varid_in, string name, int ncid_out, int varid_out)
        {
            checkError (_nc_copy_att(ncid_in, varid_in, name, ncid_out, varid_out));
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_rename_att"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_rename_att(int ncid, int varid, string name, string newname);
        public static void nc_rename_att(int ncid, int varid, string name, string newname)
        {
            checkError(_nc_rename_att(ncid, varid, name, newname));
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_del_att"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_del_att(int ncid, int varid, string name);
        public static void nc_del_att(int ncid, int varid, string name)
        {
            checkError(_nc_del_att(ncid, varid, name));
        }

#region Internal Put defs
        [DllImport("netcdf.dll", EntryPoint = "nc_put_att_text"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_att_text(int ncid, int varid, string name,
                uint len, string op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_att_uchar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_att_uchar(int ncid, int varid, string name, nc_type xtype,
                uint len, [MarshalAs(UnmanagedType.LPArray)]byte[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_att_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_att_schar(int ncid, int varid, string name, nc_type xtype,
             uint len, [MarshalAs(UnmanagedType.LPArray)]sbyte[]op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_att_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_att_short(int ncid, int varid, string name, nc_type xtype,
            uint len, [MarshalAs(UnmanagedType.LPArray)]short[] op);
                [DllImport("netcdf.dll", EntryPoint = "nc_put_att_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_att_int(int ncid, int varid, string name, nc_type xtype,
            uint len, [MarshalAs(UnmanagedType.LPArray)]int[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_att_long"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_att_long(int ncid, int varid, string name, nc_type xtype,
            uint len, [MarshalAs(UnmanagedType.LPArray)]long[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_att_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_att_float(int ncid, int varid, string name, nc_type xtype,
            uint len, [MarshalAs(UnmanagedType.LPArray)]float[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_att_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_att_double(int ncid, int varid, string name, nc_type xtype,
            uint len, [MarshalAs(UnmanagedType.LPArray)]double[] op);
#endregion        
        public static void nc_put_att(int ncid, int varid, string name, object data)
        {
            Type type = data.GetType();
            if(type == typeof(string))
            {
                checkError(_nc_put_att_text(ncid, varid, name, (uint)(data as String).Length,
                    (data as String)));
                return;
            }
            //else if(type == typeof(byte) || type == typeof(byte[]))
            //{
            //    if(type == typeof(byte)) data = new byte[]{(byte)data};
            //    checkError(_nc_put_att_uchar(ncid, varid, name, nc_type.NC_CHAR, (data as byte[]).Length,
            //        (data as byte[])));
            //}
            else if(type == typeof(sbyte) || type == typeof(sbyte[]))
            {
                if(type == typeof(sbyte)) data = new sbyte[]{(sbyte)data};
                checkError(_nc_put_att_schar(ncid, varid, name, nc_type.NC_BYTE, (uint)(data as sbyte[]).Length,
                    (data as sbyte[])));
            }
            else if(type == typeof(short) || type == typeof(short[]))
            {
                if(type == typeof(short)) data = new short[]{(short)data};
                checkError(_nc_put_att_short(ncid, varid, name, nc_type.NC_SHORT, (uint)(data as short[]).Length,
                    (data as short[])));
            }
           else if(type == typeof(int) || type == typeof(int[]))
            {
                if(type == typeof(int)) data = new int[]{(int)data};
                checkError(_nc_put_att_int(ncid, varid, name, nc_type.NC_INT, (uint)(data as int[]).Length,
                    (data as int[])));
            }
            else if(type == typeof(float) || type == typeof(float[]))
            {
                if(type == typeof(float)) data = new float[]{(float)data};
                checkError(_nc_put_att_float(ncid, varid, name, nc_type.NC_FLOAT, (uint)(data as float[]).Length,
                    (data as float[])));
            }
            //else if(type == typeof(long) || type == typeof(long[]))
            //{
            //    if(type == typeof(long)) data = new long[]{(long)data};
            //    checkError(_nc_put_att_long(ncid, varid, name, nc_type.NC_DOUBLE, (data as long[]).Length,
            //        (data as long[])));
            //}
            else if(type == typeof(double) || type == typeof(double[]))
            {
                if(type == typeof(double)) data = new double[]{(double)data};
                checkError(_nc_put_att_double(ncid, varid, name, nc_type.NC_DOUBLE, (uint)(data as double[]).Length,
                    (data as double[])));
            }
            else throw new Exception("Type not found"); //TODO CHANGE
 
        }

#region Interal Get Defs
        [DllImport("netcdf.dll", EntryPoint = "nc_get_att_text"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_att_text(int ncid, int varid, string name, 
            [MarshalAs(UnmanagedType.LPStr)]StringBuilder builder);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_att_uchar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_att_uchar(int ncid, int varid, string name, 
            [MarshalAs(UnmanagedType.LPArray)]byte[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_att_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_att_schar(int ncid, int varid, string name, 
            [MarshalAs(UnmanagedType.LPArray)]sbyte[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_att_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_att_short(int ncid, int varid, string name, 
            [MarshalAs(UnmanagedType.LPArray)]short[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_att_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_att_int(int ncid, int varid, string name, 
            [MarshalAs(UnmanagedType.LPArray)]int[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_att_long"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_att_long(int ncid, int varid, string name, 
            [MarshalAs(UnmanagedType.LPArray)]long[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_att_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_att_float(int ncid, int varid, string name, 
            [MarshalAs(UnmanagedType.LPArray)]float[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_att_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_att_double(int ncid, int varid, string name, 
            [MarshalAs(UnmanagedType.LPArray)]double[] ip);
#endregion
        public static object nc_get_att(int ncid, int varid, string name)
        {
            nc_type type;
            uint len;
            nc_inq_att(ncid, varid, name, out type, out len);
            if(type == nc_type.NC_CHAR)
            {
                 StringBuilder builder = new StringBuilder((int)len);
                checkError(_nc_get_att_text(ncid, varid, name, builder));
                return builder.ToString().Substring(0, (int)len);
            }

            IList toRet = null;
            if(type == nc_type.NC_BYTE)
            {
                toRet = new sbyte[len];
                checkError(_nc_get_att_schar(ncid, varid, name, toRet as sbyte[]));
            }
            else if(type == nc_type.NC_SHORT)
            {
                toRet = new short[len];
                checkError(_nc_get_att_short(ncid, varid, name, toRet as short[]));
            }
            else if(type == nc_type.NC_INT)
            {
                toRet = new int[len];
                checkError(_nc_get_att_int(ncid, varid, name, toRet as int[]));
            }
            else if(type == nc_type.NC_FLOAT)
            {
                toRet = new float[len];
                checkError(_nc_get_att_float(ncid, varid, name, toRet as float[]));
            }
            else if(type == nc_type.NC_DOUBLE)
            {
                toRet = new double[len];
                checkError(_nc_get_att_double(ncid, varid, name, toRet as double[]));
            }

            if (toRet.Count == 1) return toRet[0];
            return toRet;
        }
#endregion

#region Var Functions

        [DllImport("netcdf.dll", EntryPoint = "nc_def_var"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_def_var(int ncid, string name, nc_type xtype, int ndims, 
            [MarshalAs(UnmanagedType.LPArray)]int[] dimidsp, ref int varidp);
        public static int nc_def_var(int ncid, string name, nc_type type, int[] dimids)
        {
            int varid = 0;
            checkError(_nc_def_var(ncid, name, type, dimids.Length, dimids, ref varid));
            return varid;
        }

        public static int nc_def_var(int ncid, string name, int[] dimids, Type type)
        {
            return nc_def_var(ncid, name, FromType(type), dimids);
        }

#region Inq Var

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_varid"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_varid(int ncid, string name, ref int varidp);
        /// <summary>
        /// Get the variable id from its name
        /// </summary>
        /// <returns>Returns -1 if not found, otherwise the id number</returns>
        public static int nc_inq_varid(int ncid, string name)
        {
            int id = 0;
            int errorCode = _nc_inq_varid(ncid, name, ref id);
            if (errorCode == Constants.NC_EBADNAME) return -1;
            checkError(errorCode);
            return id;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_varname"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_varname(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPStr)]StringBuilder name);
        public static string nc_inq_varname(int ncid, int varid)
        {
            StringBuilder builder = new StringBuilder(Constants.NC_MAX_NAME);
            checkError(_nc_inq_varname(ncid, varid, builder));
            return builder.ToString();
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_vartype"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_vartype(int ncid, int varid, ref nc_type xtypep);
        public static nc_type nc_inq_vartype(int ncid, int varid)
        {
            nc_type type = nc_type.NC_BYTE;
            checkError(_nc_inq_vartype(ncid, varid, ref type));
            return type;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_varndims"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_varndims(int ncid, int varid, ref int ndimsp);
        public static int nc_inq_varndims(int ncid, int varid)
        {
            int numdims = 0;
            checkError(_nc_inq_varndims(ncid, varid, ref numdims));
            return numdims;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_vardimid"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_vardimid(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]int[] dimidsp);
        public static int[] nc_inq_vardimid(int ncid, int varid)
        {
            int numdims = nc_inq_varndims(ncid, varid);
            int[] dims = new int[numdims];
            checkError(_nc_inq_vardimid(ncid, varid, dims));
            return dims;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_varnatts"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_varnatts(int ncid, int varid, ref int nattsp);
        public static int nc_inq_varnatts(int ncid, int varid)
        {
            int numatts =0;
            checkError(_nc_inq_varnatts(ncid, varid, ref numatts));
            return numatts;
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_inq_var"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_inq_var(int ncid, int varid, string name, ref nc_type xtypep, 
            ref int ndimsp, [MarshalAs(UnmanagedType.LPArray)]int[] dimidsp, ref int nattsp);
        public static void nc_inq_var(int ncid, int varid, string name, out nc_type type,
            out int[] dimids, out int num_atts)
        {
            type = nc_inq_vartype(ncid, varid);
            dimids = nc_inq_vardimid(ncid, varid);
            num_atts = nc_inq_varnatts(ncid, varid);
        }

#endregion

        [DllImport("netcdf.dll", EntryPoint = "nc_rename_var"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_rename_var(int ncid, int varid, string name);
        public static void nc_rename_var(int ncid, int varid, string name)
        {
            checkError(_nc_rename_var(ncid, varid, name));
        }

        [DllImport("netcdf.dll", EntryPoint = "nc_copy_var"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_copy_var(int ncid_in, int varid, int ncid_out);
        public static void nc_copy_var(int ncid_in, int varid, int ncid_out)
        {
            checkError(_nc_copy_var(ncid_in, varid, ncid_out));
        }

#region Put Var1 internals
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var1_text"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var1_uchar(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref byte op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var1_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var1_schar(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref sbyte op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var1_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var1_short(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref short op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var1_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var1_int(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref int op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var1_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var1_float(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref float op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var1_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var1_double(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref double op);
#endregion
        public static void nc_put_var1(int ncid, int varid, uint[] indices, object val)
        {
            Type type = val.GetType();
            if(type == typeof(byte))
            {
                byte bval = (byte)val;
               checkError(_nc_put_var1_uchar(ncid, varid, indices, ref bval));
            }
            else if(type == typeof(sbyte))
            {
                sbyte bval = (sbyte)val;
                checkError(_nc_put_var1_schar(ncid, varid, indices, ref bval));
            }
            else if(type == typeof(short))
            {
                short bval = (short)val;
                checkError(_nc_put_var1_short(ncid, varid, indices, ref bval));
            }
            else if(type == typeof(int))
            {
                int bval = (int)val;
                checkError(_nc_put_var1_int(ncid, varid, indices, ref bval));
            }
            else if(type == typeof(float))
            {
                float bval = (float)val;
                checkError(_nc_put_var1_float(ncid, varid, indices, ref bval));
            }
            else if(type == typeof(double))
            {
                double bval = (double)val;
                checkError(_nc_put_var1_double(ncid, varid, indices, ref bval));
            }
        }

#region Get Var1 internals
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var1_uchar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var1_uchar(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref byte ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var1_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var1_schar(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref sbyte ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var1_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var1_short(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref short ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var1_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var1_int(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref int ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var1_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var1_float(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref float ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var1_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var1_double(int ncid, int varid, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] indexp, ref double ip);
#endregion
        public static object nc_get_var1(int ncid, int varid, uint[] indices)
        {
            object toRet = null;

            nc_type type = nc_inq_vartype(ncid, varid);
            if(type == nc_type.NC_BYTE)
            {
                sbyte bval = new sbyte();
                checkError(_nc_get_var1_schar(ncid, varid, indices, ref bval));
                toRet = bval;
            }
            else if(type == nc_type.NC_CHAR)
            {
                byte bval = new byte();
                checkError(_nc_get_var1_uchar(ncid, varid, indices, ref bval));
                toRet = bval;
            }
            else if(type == nc_type.NC_SHORT)
            {
                short bval = new short();
                checkError(_nc_get_var1_short(ncid, varid, indices, ref bval));
                toRet = bval;
            }
            else if(type == nc_type.NC_INT)
            {
                int bval = new int();
                checkError(_nc_get_var1_int(ncid, varid, indices, ref bval));
                toRet = bval;
            }
            else if(type == nc_type.NC_FLOAT)
            {
               float bval = new float();
               checkError(_nc_get_var1_float(ncid, varid, indices, ref bval));
               toRet = bval;
            }
            else if(type == nc_type.NC_DOUBLE)
            {
                double bval = new double();
                checkError(_nc_get_var1_double(ncid, varid, indices, ref bval));
                toRet = bval;
            }
            else throw new Exception(); //TODO CHANGE

            return toRet;
        }

#region Put Vara internals
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vara_uchar"), SuppressUnmanagedCodeSecurity]
        private unsafe static extern int _nc_put_vara_uchar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            void* op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vara_schar"), SuppressUnmanagedCodeSecurity]
        private unsafe static extern int _nc_put_vara_schar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            void* op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vara_short"), SuppressUnmanagedCodeSecurity]
        private unsafe static extern int _nc_put_vara_short(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            void* op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vara_int"), SuppressUnmanagedCodeSecurity]
        private unsafe static extern int _nc_put_vara_int(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            void* op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vara_float"), SuppressUnmanagedCodeSecurity]
        private unsafe static extern int _nc_put_vara_float(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            void* op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vara_double"), SuppressUnmanagedCodeSecurity]
        private unsafe static extern int _nc_put_vara_double(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            void* op);
#endregion
        private unsafe static void nc_put_varahelper(int ncid, int varid, uint[] startIndices, uint[] counts, void* ptr)
        {
            nc_type type = nc_inq_vartype(ncid, varid);
            if (type == nc_type.NC_CHAR)
            {
                checkError(_nc_put_vara_uchar(ncid, varid, startIndices, counts, ptr));
            }
            else if (type == nc_type.NC_BYTE)
            {
                checkError(_nc_put_vara_schar(ncid, varid, startIndices, counts, ptr));
            }
            else if (type == nc_type.NC_SHORT)
            {
                checkError(_nc_put_vara_short(ncid, varid, startIndices, counts, ptr));
            }
            else if (type == nc_type.NC_INT)
            {
                checkError(_nc_put_vara_int(ncid, varid, startIndices, counts, ptr));
            }
            else if (type == nc_type.NC_FLOAT)
            {
                checkError(_nc_put_vara_float(ncid, varid, startIndices, counts, ptr));
            }
            else if (type == nc_type.NC_DOUBLE)
            {
                checkError(_nc_put_vara_double(ncid, varid, startIndices, counts, ptr));
            }
            else throw new Exception(); //TODO CHANGE

        }

        public static void nc_put_vara(int ncid, int varid, uint[] startIndices, uint[] counts, Array data)
        {
            unsafe
            {
                if (data is byte[])
                {
                    fixed (byte* b = (byte[])data)
                    {
                        nc_put_varahelper(ncid, varid, startIndices, counts, b);
                    }
                }
                else if (data is short[])
                {
                    fixed (short* b = (short[])data)
                    {
                        nc_put_varahelper(ncid, varid, startIndices, counts, b);
                    }
                }

                else if (data is float[])
                {
                    fixed (float* b = (float[])data)
                    {
                        nc_put_varahelper(ncid, varid, startIndices, counts, b);
                    }
                }

                else if (data is int[])
                {
                    fixed (int* b = (int[])data)
                    {
                        nc_put_varahelper(ncid, varid, startIndices, counts, b);
                    }
                }
                else if (data is double[])
                {
                    fixed (double* b = (double[])data)
                    {
                        nc_put_varahelper(ncid, varid, startIndices, counts, b);
                    }
                }
            }
            
        }

#region Get Vara internals
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vara_uchar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vara_uchar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[]startp, [MarshalAs(UnmanagedType.LPArray)]uint[]countp, 
            [MarshalAs(UnmanagedType.LPArray)]byte[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vara_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vara_schar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[]startp, [MarshalAs(UnmanagedType.LPArray)]uint[]countp, 
            [MarshalAs(UnmanagedType.LPArray)]sbyte[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vara_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vara_short(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[]startp, [MarshalAs(UnmanagedType.LPArray)]uint[]countp, 
            [MarshalAs(UnmanagedType.LPArray)]short[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vara_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vara_int(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[]startp, [MarshalAs(UnmanagedType.LPArray)]uint[]countp, 
            [MarshalAs(UnmanagedType.LPArray)]int[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vara_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vara_float(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[]startp, [MarshalAs(UnmanagedType.LPArray)]uint[]countp, 
            [MarshalAs(UnmanagedType.LPArray)]float[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vara_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vara_double(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[]startp, [MarshalAs(UnmanagedType.LPArray)]uint[]countp, 
            [MarshalAs(UnmanagedType.LPArray)]double[]ip);
#endregion
        public static Array nc_get_vara(int ncid, int varid, uint[] startIndices, uint[] counts)
        {
            nc_type type = nc_inq_vartype(ncid, varid);
            Array toRet = null;
            uint amount = 1;
            for (int i = 0; i < counts.Length; i++)
			{
			    amount *= counts[i];
			}
            
            if(type == nc_type.NC_BYTE)
            {
                toRet = new sbyte[amount];
                checkError(_nc_get_vara_schar(ncid, varid, startIndices, counts, toRet as sbyte[]));
            }
            else if(type == nc_type.NC_CHAR)
            {
                toRet = new byte[amount];
                checkError(_nc_get_vara_uchar(ncid, varid, startIndices, counts, toRet as byte[]));
            }
            else if(type == nc_type.NC_SHORT)
            {
                toRet = new short[amount];
                checkError(_nc_get_vara_short(ncid, varid, startIndices, counts, toRet as short[]));
            }
            else if(type == nc_type.NC_INT)
            {
                toRet = new int[amount];
                checkError(_nc_get_vara_int(ncid, varid, startIndices, counts, toRet as int[]));
            }
            else if(type == nc_type.NC_FLOAT)
            {
                toRet = new float[amount];
                checkError(_nc_get_vara_float(ncid, varid, startIndices, counts, toRet as float[]));
            }
            else if(type == nc_type.NC_DOUBLE)
            {
                toRet = new double[amount];
                checkError(_nc_get_vara_double(ncid, varid, startIndices, counts, toRet as double[]));
            }
            else throw new Exception(); //TODO CHANGE
            return toRet;
        }

#region Put Vars internals
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vars_uchar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_vars_uchar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep, 
            [MarshalAs(UnmanagedType.LPArray)]byte[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vars_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_vars_schar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]sbyte[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vars_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_vars_short(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]short[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vars_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_vars_int(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]int[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vars_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_vars_float(int ncid, int varid,
            uint[] startp, uint[] countp, uint[] stridep,
            float[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_vars_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_vars_double(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]double[] op);
#endregion
        public static void nc_put_vars(int ncid, int varid, uint[] startIndices,
            uint[] counts, uint[] strides, Array data)
        {
            Type type = data.GetType();
            if(type == typeof(byte[]))
            {
                checkError(_nc_put_vars_uchar(ncid, varid, startIndices, counts, strides,(data as byte[])));
            }
            else if(type == typeof(sbyte[]))
            {
                checkError(_nc_put_vars_schar(ncid, varid, startIndices, counts, strides, (data as sbyte[])));
            }
            else if(type == typeof(short[]))
            {
                checkError(_nc_put_vars_short(ncid, varid, startIndices, counts, strides,(data as short[])));
            }
            else if(type == typeof(int[]))
            {
                checkError(_nc_put_vars_int(ncid, varid, startIndices, counts, strides,(data as int[])));
            }
            else if(type == typeof(float[]))
            {
                checkError(_nc_put_vars_float(ncid, varid, startIndices, counts, strides,(data as float[])));
            }
            else if(type == typeof(double[]))
            {
                checkError(_nc_put_vars_double(ncid, varid, startIndices, counts, strides,(data as double[])));
            }
            else throw new Exception(); //TODO CHANGE
        }
        
#region Get Vars internals
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vars_uchar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vars_uchar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]byte[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vars_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vars_schar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]sbyte[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vars_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vars_short(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]short[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vars_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vars_int(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]int[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vars_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vars_float(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]float[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_vars_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_vars_double(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]double[] ip);
#endregion
        public static Array nc_get_vars(int ncid, int varid, uint[] startIndices,
            uint[] counts, uint[] strides)
        {
            nc_type type = nc_inq_vartype(ncid, varid);
            Array toRet = null;
            uint amount = 1;
            for (int i = 0; i < counts.Length; i++)
			{
			    amount *= counts[i];
			}
            
            if(type == nc_type.NC_BYTE)
            {
                toRet = new sbyte[amount];
                checkError(_nc_get_vars_schar(ncid, varid, startIndices, counts, strides, toRet as sbyte[]));
            }
            else if(type == nc_type.NC_CHAR)
            {
                toRet = new byte[amount];
                checkError(_nc_get_vars_uchar(ncid, varid, startIndices, counts, strides, toRet as byte[]));
            }
            else if(type == nc_type.NC_SHORT)
            {
                toRet = new short[amount];
                checkError(_nc_get_vars_short(ncid, varid, startIndices, counts, strides, toRet as short[]));
            }
            else if(type == nc_type.NC_INT)
            {
                toRet = new int[amount];
                checkError(_nc_get_vars_int(ncid, varid, startIndices, counts, strides, toRet as int[]));
            }
            else if(type == nc_type.NC_FLOAT)
            {
                toRet = new float[amount];
                checkError(_nc_get_vars_float(ncid, varid, startIndices, counts, strides, toRet as float[]));
            }
            else if(type == nc_type.NC_DOUBLE)
            {
                toRet = new double[amount];
                checkError(_nc_get_vars_double(ncid, varid, startIndices, counts, strides, toRet as double[]));
            }
            else throw new Exception(); //TODO CHANGE
            return toRet;
        }

#region Put varm internals
        [DllImport("netcdf.dll", EntryPoint = "nc_put_varm_uchar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_varm_uchar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]byte[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_varm_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_varm_schar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]sbyte[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_varm_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_varm_short(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]short[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_varm_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_varm_int(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]int[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_varm_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_varm_float(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]float[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_varm_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_varm_double(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp,
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]double[] op);
#endregion
        public static void nc_put_varm(int ncid, int varid, uint[] startIndices,
            uint[] counts, uint[] strides, uint[] mapping, Array data)
        {
            Type type = data.GetType();
            if(type == typeof(byte[]))
            {
                checkError(_nc_put_varm_uchar(ncid, varid, startIndices, counts, strides, mapping, (data as byte[])));
            }
            else if(type == typeof(sbyte[]))
            {
                checkError(_nc_put_varm_schar(ncid, varid, startIndices, counts, strides, mapping,(data as sbyte[])));
            }
            else if(type == typeof(short[]))
            {
                checkError(_nc_put_varm_short(ncid, varid, startIndices, counts, strides,mapping,(data as short[])));
            }
            else if(type == typeof(int[]))
            {
                checkError(_nc_put_varm_int(ncid, varid, startIndices, counts, strides,mapping,(data as int[])));
            }
            else if(type == typeof(float[]))
            {
                checkError(_nc_put_varm_float(ncid, varid, startIndices, counts, strides,mapping,(data as float[])));
            }
            else if(type == typeof(double[]))
            {
                checkError(_nc_put_varm_double(ncid, varid, startIndices, counts, strides,mapping,(data as double[])));
            }
            else throw new Exception(); //TODO CHANGE
        }

#region Get Varm internals
        [DllImport("netcdf.dll", EntryPoint = "nc_get_varm_uchar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_varm_uchar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]byte[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_varm_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_varm_schar(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]sbyte[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_varm_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_varm_short(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]short[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_varm_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_varm_int(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]int[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_varm_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_varm_float(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[] imapp, 
            [MarshalAs(UnmanagedType.LPArray)]float[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_varm_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_varm_double(int ncid, int varid,
            [MarshalAs(UnmanagedType.LPArray)]uint[] startp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] countp, 
            [MarshalAs(UnmanagedType.LPArray)]uint[] stridep,
            [MarshalAs(UnmanagedType.LPArray)]uint[]  imap, 
            [MarshalAs(UnmanagedType.LPArray)]double[] ip);
#endregion
        public static Array nc_get_varm(int ncid, int varid, uint[] startIndices,
            uint[] counts, uint[] strides, uint[] mapping)
        {
            nc_type type = nc_inq_vartype(ncid, varid);
            Array toRet = null;
            uint amount = 1;
            for (int i = 0; i < counts.Length; i++)
			{
			    amount *= counts[i];
			}
            
            if(type == nc_type.NC_BYTE)
            {
                toRet = new sbyte[amount];
                checkError(_nc_get_varm_schar(ncid, varid, startIndices, counts, strides, mapping, toRet as sbyte[]));
            } 
            else if(type == nc_type.NC_CHAR)
            {
                toRet = new byte[amount];
                checkError(_nc_get_varm_uchar(ncid, varid, startIndices, counts, strides, mapping, toRet as byte[]));
            }
            else if(type == nc_type.NC_SHORT)
            {
                toRet = new short[amount];
                checkError(_nc_get_varm_short(ncid, varid, startIndices, counts, strides, mapping, toRet as short[]));
            }
            else if(type == nc_type.NC_INT)
            {
                toRet = new int[amount];
                checkError(_nc_get_varm_int(ncid, varid, startIndices, counts, strides, mapping, toRet as int[]));
            }
            else if(type == nc_type.NC_FLOAT)
            {
                toRet = new float[amount];
                checkError(_nc_get_varm_float(ncid, varid, startIndices, counts, strides, mapping, toRet as float[]));
            }
            else if(type == nc_type.NC_DOUBLE)
            {
                toRet = new double[amount];
                checkError(_nc_get_varm_double(ncid, varid, startIndices, counts, strides, mapping, toRet as double[]));
            }
            else throw new Exception(); //TODO CHANGE
            return toRet;
        }

#region Put var internals

        [DllImport("netcdf.dll", EntryPoint = "nc_put_var_uchar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var_uchar(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]byte[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var_schar(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]sbyte[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var_short(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]short[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var_int(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]int[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var_float(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]float[] op);
        [DllImport("netcdf.dll", EntryPoint = "nc_put_var_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_put_var_double(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]double[] op);

#endregion
        public static void nc_put_var(int ncid, int varid, Array data)
        {
            Type type = data.GetType();
            if(type == typeof(byte[]))
            {
                checkError(_nc_put_var_uchar(ncid, varid, (data as byte[])));
            }
            else if(type == typeof(sbyte[]))
            {
                checkError(_nc_put_var_schar(ncid, varid, (data as sbyte[])));
            }
            else if(type == typeof(short[]))
            {
                checkError(_nc_put_var_short(ncid, varid, (data as short[])));
            }
            else if(type == typeof(int[]))
            {
                checkError(_nc_put_var_int(ncid, varid, (data as int[])));
            }
            else if(type == typeof(float[]))
            {
                checkError(_nc_put_var_float(ncid, varid, (data as float[])));
            }
            else if(type == typeof(double[]))
            {
                checkError(_nc_put_var_double(ncid, varid, (data as double[])));
            }
            else throw new Exception(); //TODO CHANGE
        }

        #region Get Var internals
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var_uchar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var_uchar(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]byte[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var_schar"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var_schar(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]sbyte[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var_short"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var_short(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]short[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var_int"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var_int(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]int[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var_float"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var_float(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]float[] ip);
        [DllImport("netcdf.dll", EntryPoint = "nc_get_var_double"), SuppressUnmanagedCodeSecurity]
        private static extern int _nc_get_var_double(int ncid, int varid, [MarshalAs(UnmanagedType.LPArray)]double[] ip);
        #endregion
        public static Array nc_get_var(int ncid, int varid)
        {
            nc_type type = nc_inq_vartype(ncid, varid);
            Array toRet = null;
            int amount = 1;
            int[] dims = nc_inq_vardimid(ncid, varid);
            foreach (int dim in dims)
            {
                int dimsize = nc_inq_dimlen(ncid, dim);
                amount *= dimsize;
            }

            if (type == nc_type.NC_BYTE)
            {
                toRet = new sbyte[amount];
                checkError(_nc_get_var_schar(ncid, varid, toRet as sbyte[]));
            }
            else if (type == nc_type.NC_CHAR)
            {
                toRet = new byte[amount];
                checkError(_nc_get_var_uchar(ncid, varid, toRet as byte[]));
            }
            else if (type == nc_type.NC_SHORT)
            {
                toRet = new short[amount];
                checkError(_nc_get_var_short(ncid, varid, toRet as short[]));
            }
            else if (type == nc_type.NC_INT)
            {
                toRet = new int[amount];
                checkError(_nc_get_var_int(ncid, varid, toRet as int[]));
            }
            else if (type == nc_type.NC_FLOAT)
            {
                toRet = new float[amount];
                checkError(_nc_get_var_float(ncid, varid, toRet as float[]));
            }
            else if (type == nc_type.NC_DOUBLE)
            {
                toRet = new double[amount];
                checkError(_nc_get_var_double(ncid, varid, toRet as double[]));
            }
            else throw new Exception(); //TODO CHANGE
            return toRet;
        }
#endregion

    }
}
