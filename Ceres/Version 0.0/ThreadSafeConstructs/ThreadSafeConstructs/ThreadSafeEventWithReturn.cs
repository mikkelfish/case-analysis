using System;
using System.Collections.Generic;
using System.Text;

namespace ThreadSafeConstructs
{
    public delegate UReturnType ThreadSafeEventHandler<TEventArgs, UReturnType>(Object sender, TEventArgs e) where TEventArgs : EventArgs;
    public class ThreadSafeEvent<TEventArgs, UReturnType> where TEventArgs : EventArgs
    {
        private event ThreadSafeEventHandler<TEventArgs, UReturnType> Event;
        public UReturnType CallEvent(object sender, TEventArgs e)
        {
            ThreadSafeEventHandler<TEventArgs, UReturnType> handle;
            lock (this)
            {
                handle = this.Event;
            }

            try
            {
                if (handle != null)
                {
                    return handle(sender, e);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex);
            }

            return default(UReturnType);
        }

        public static implicit operator Delegate(ThreadSafeEvent<TEventArgs, UReturnType> ev)
        {
            return ev.Event;
        }

        public static ThreadSafeEvent<TEventArgs, UReturnType> operator +(ThreadSafeEvent<TEventArgs, UReturnType> ev, ThreadSafeEventHandler<TEventArgs, UReturnType> toadd)
        {
            lock (ev)
            {
                ev.Event += toadd;
            }
            return ev;
        }

        public static ThreadSafeEvent<TEventArgs, UReturnType> operator -(ThreadSafeEvent<TEventArgs, UReturnType> ev, ThreadSafeEventHandler<TEventArgs, UReturnType> tosubtract)
        {
            lock (ev)
            {
                ev.Event -= tosubtract;
            }
            return ev;
        }
    }
}
