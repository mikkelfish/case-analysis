using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Collections;

namespace ThreadSafeConstructs
{
    //TODO ADD DESCRIPTIVE EXCEPTIONS
    public class ThreadSafeMemoryStream : Stream
    {
        private List<byte> data;        
        private ReaderWriterLock rwl;
        private readonly object lockable = new object();
        private long pos;

        public override bool CanRead { get { return true; } }
        public override bool CanSeek { get { return true; } }
        public override bool CanWrite { get { return true; } }

        public ThreadSafeMemoryStream() : this(null) { }
        public ThreadSafeMemoryStream(byte[] bdata)
        {
            this.pos = 0;
            if (bdata == null) this.data = new List<byte>();
            else this.data = new List<byte>(bdata);
            this.rwl = new ReaderWriterLock();
        }

        public override void Flush() { return; }

        public override long Length { get { lock (this.lockable) { return this.data.Count; } } }

        public override long Position
        {
            get { lock (this.lockable) { return this.pos; } }
            set { lock (this.lockable) { this.pos = value; } }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            int realCount = count;
            if (this.pos + count > this.data.Count) realCount = (int)(this.data.Count - this.pos);
            this.data.CopyTo((int)this.pos, buffer, offset, realCount);
            this.pos += realCount;
            rwl.ReleaseWriterLock();
            return realCount;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            lock (this.lockable)
            {
                if (SeekOrigin.Begin == origin) this.pos = offset;
                else if (SeekOrigin.Current == origin) this.pos += offset;
                else if (SeekOrigin.End == origin) this.pos = this.data.Count - offset - 1;
                return this.pos;
            }
        }

        public override void SetLength(long value)
        {
            lock (this.lockable)
            {
                this.data.Capacity = (int)value;
            }
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            byte[] toInsert = new byte[count];
            unsafe
            {
                fixed (byte* src = buffer, dst = toInsert)
                {
                    Memory.Memory.Copy(src, dst, count * sizeof(byte));
                }
            }

            rwl.AcquireWriterLock(Timeout.Infinite);
            try
            {
                if ((int)this.pos == this.data.Count || this.data.Capacity == 0) this.data.AddRange(toInsert);
                else this.data.InsertRange((int)this.pos, toInsert);
                this.pos += count;
            }
            finally
            {
                rwl.ReleaseWriterLock();
            }    
        }

        public override void Close()
        {
            
        }

        public byte[] ToArray()
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try
            {
                return this.data.ToArray();
            }
            finally
            {
                rwl.ReleaseReaderLock();
            }
        }
    }
}
