using System;
using System.Collections.Generic;
using System.Text;

namespace ThreadSafeConstructs
{
    public delegate void ThreadSafeEventHandler<TEventArgs>(Object sender, TEventArgs e) where TEventArgs : EventArgs;
    public class ThreadSafeEvent<TEventArgs> where TEventArgs : EventArgs
    {
        private event ThreadSafeEventHandler<TEventArgs> Event;
        public void CallEvent(object sender, TEventArgs e)
        {
            ThreadSafeEventHandler<TEventArgs> handle;
            lock (this)
            {
                handle = this.Event;
            }
            try
            {
                if (handle != null)
                {
                    handle(sender, e);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex) ;
            }
        }

        public static implicit operator Delegate(ThreadSafeEvent<TEventArgs> ev)
        {
            return ev.Event;
        }

        public static ThreadSafeEvent<TEventArgs> operator +(ThreadSafeEvent<TEventArgs> ev, ThreadSafeEventHandler<TEventArgs> toadd)
        {
            lock (ev)
            {
                ev.Event += toadd;
            }
            return ev;
        }

        public static ThreadSafeEvent<TEventArgs> operator -(ThreadSafeEvent<TEventArgs> ev, ThreadSafeEventHandler<TEventArgs> tosubtract)
        {
            lock (ev)
            {
                ev.Event -= tosubtract;
            }
            return ev;
        }
    }
}
