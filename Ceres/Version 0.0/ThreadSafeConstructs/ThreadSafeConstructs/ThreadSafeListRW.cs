using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace ThreadSafeConstructs
{
    public class ThreadSafeListRW<T> : List<T>
    {
        private ReaderWriterLock rwl = new ReaderWriterLock();

        #region New Properties
        public new Int32 Capacity
        {
            get
            {
                rwl.AcquireReaderLock(Timeout.Infinite);
                try { return base.Capacity; }
                finally { rwl.ReleaseReaderLock(); }
            }
            set
            {
                rwl.AcquireWriterLock(Timeout.Infinite);
                try { base.Capacity = value; }
                finally { rwl.ReleaseWriterLock(); }
            }
        }

        public new Int32 Count
        {
            get
            {
                rwl.AcquireReaderLock(Timeout.Infinite);
                try { return base.Count; }
                finally { rwl.ReleaseReaderLock(); }
            }
        }

        #endregion

        #region New Methods

        public new List<U> ConvertAll<U>(Converter<T, U> converter)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.ConvertAll(converter); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new void Add(T item)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.Add(item); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void AddRange(IEnumerable<T> collection)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.AddRange(collection); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new ReadOnlyCollection<T> AsReadOnly()
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.AsReadOnly(); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 BinarySearch(Int32 index, Int32 count, T item, IComparer<T> comparer)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.BinarySearch(index, count, item, comparer); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 BinarySearch(T item)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.BinarySearch(item); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 BinarySearch(T item, IComparer<T> comparer)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.BinarySearch(item, comparer); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new void Clear()
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.Clear(); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new Boolean Contains(T item)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.Contains(item); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new void CopyTo(T[] array)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.CopyTo(array); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void CopyTo(Int32 index, T[] array, Int32 arrayIndex, Int32 count)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.CopyTo(index, array, arrayIndex, count); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void CopyTo(T[] array, Int32 arrayIndex)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.CopyTo(array, arrayIndex); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new Boolean Exists(Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.Exists(match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new T Find(Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.Find(match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new List<T> FindAll(Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.FindAll(match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 FindIndex(Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.FindIndex(match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 FindIndex(Int32 startIndex, Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.FindIndex(startIndex, match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 FindIndex(Int32 startIndex, Int32 count, Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.FindIndex(startIndex, count, match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new T FindLast(Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.FindLast(match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 FindLastIndex(Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.FindLastIndex(match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 FindLastIndex(Int32 startIndex, Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.FindLastIndex(startIndex, match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 FindLastIndex(Int32 startIndex, Int32 count, Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.FindLastIndex(startIndex, count, match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new void ForEach(Action<T> action)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.ForEach(action); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new Enumerator GetEnumerator()
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.GetEnumerator(); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new List<T> GetRange(Int32 index, Int32 count)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.GetRange(index, count); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 IndexOf(T item)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.IndexOf(item); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 IndexOf(T item, Int32 index)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.IndexOf(item, index); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 IndexOf(T item, Int32 index, Int32 count)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.IndexOf(item, index, count); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new void Insert(Int32 index, T item)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.Insert(index, item); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void InsertRange(Int32 index, IEnumerable<T> collection)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.InsertRange(index, collection); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new Int32 LastIndexOf(T item)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.LastIndexOf(item); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 LastIndexOf(T item, Int32 index)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.LastIndexOf(item, index); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 LastIndexOf(T item, Int32 index, Int32 count)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.LastIndexOf(item, index, count); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Boolean Remove(T item)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.Remove(item); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 RemoveAll(Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.RemoveAll(match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new void RemoveAt(Int32 index)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.RemoveAt(index); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void RemoveRange(Int32 index, Int32 count)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.RemoveRange(index, count); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void Reverse()
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.Reverse(); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void Reverse(Int32 index, Int32 count)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.Reverse(index, count); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void Sort()
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.Sort(); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void Sort(IComparer<T> comparer)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.Sort(comparer); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void Sort(Int32 index, Int32 count, IComparer<T> comparer)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.Sort(index, count, comparer); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new void Sort(Comparison<T> comparison)
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.Sort(comparison); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new T[] ToArray()
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.ToArray(); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new void TrimExcess()
        {
            rwl.AcquireWriterLock(Timeout.Infinite);
            try { base.TrimExcess(); }
            finally { rwl.ReleaseWriterLock(); }
        }

        public new Boolean TrueForAll(Predicate<T> match)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.TrueForAll(match); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Type GetType()
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.GetType(); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new String ToString()
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.ToString(); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Boolean Equals(Object obj)
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.Equals(obj); }
            finally { rwl.ReleaseReaderLock(); }
        }

        public new Int32 GetHashCode()
        {
            rwl.AcquireReaderLock(Timeout.Infinite);
            try { return base.GetHashCode(); }
            finally { rwl.ReleaseReaderLock(); }
        }

        #endregion
    }
}