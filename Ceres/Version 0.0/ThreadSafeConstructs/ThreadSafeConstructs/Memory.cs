//THIS IS FROM THE .NET HELP FILES
using System;
using System.Runtime.InteropServices;

namespace ThreadSafeConstructs.Memory
{
    public unsafe class Memory
    {
        // Handle for the process heap. This handle is used in all calls to the
        // HeapXXX APIs in the methods below.
        static IntPtr ph = GetProcessHeap();
        // Private instance constructor to prevent instantiation.
        private Memory() { }
        // Allocates a memory block of the given size. The allocated memory is
        // automatically initialized to zero.
        public static void* Alloc(long size)
        {
            void* result = HeapAlloc(ph, HEAP_ZERO_MEMORY, size);
            if (result == null) throw new OutOfMemoryException();
            return result;
        }
        // Copies count bytes from src to dst. The source and destination
        // blocks are permitted to overlap.
        public static void Copy(void* src, void* dst, long count)
        {
            byte* ps = (byte*)src;
            byte* pd = (byte*)dst;
            if (ps > pd)
            {
                for (; count != 0; count--) *pd++ = *ps++;
            }
            else if (ps < pd)
            {
                for (ps += count, pd += count; count != 0; count--) *--pd = *--ps;
            }
        }
        // Frees a memory block.
        public static void Free(void* block)
        {
            if (!HeapFree(ph, 0, block)) throw new InvalidOperationException();
        }
        // Re-allocates a memory block. If the reallocation request is for a
        // larger size, the additional region of memory is automatically
        // initialized to zero.
        public static void* ReAlloc(void* block, long size)
        {
            void* result = HeapReAlloc(ph, HEAP_ZERO_MEMORY, block, size);
            if (result == null) throw new OutOfMemoryException();
            return result;
        }
        // Returns the size of a memory block.
        public static int SizeOf(void* block)
        {
            IntPtr result = HeapSize(ph, 0, block);
            if (result == (new IntPtr(-1))) throw new InvalidOperationException();
            return (int)result;
        }
        // Heap API flags
        const int HEAP_ZERO_MEMORY = 0x00000008;
        // Heap API functions
        [DllImport("kernel32")]
        static extern IntPtr GetProcessHeap();
        [DllImport("kernel32")]
        static extern void* HeapAlloc(IntPtr hHeap, int flags, long size);
        [DllImport("kernel32")]
        static extern bool HeapFree(IntPtr hHeap, int flags, void* block);
        [DllImport("kernel32")]
        static extern void* HeapReAlloc(IntPtr hHeap, int flags,
            void* block, long size);
        [DllImport("kernel32")]
        static extern IntPtr HeapSize(IntPtr hHeap, int flags, void* block);
    }

}
