using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace ThreadSafeConstructs
{
    public class ThreadSafeListLock<T> : List<T>
    {
        private readonly object lockable = new object();

        #region New Properties
        public new Int32 Capacity
        {
            get
            {
                lock (this.lockable) { return base.Capacity; }
            }
            set
            {
                lock (this.lockable) { base.Capacity = value; }
            }
        }

        public new Int32 Count
        {
            get
            {
                lock (this.lockable) { return base.Count; }
            }
        }

        #endregion

        #region New Methods

        public new List<U> ConvertAll<U>(Converter<T, U> converter)
        {
            lock (this.lockable) { return base.ConvertAll(converter); };
        }

        public new void Add(T item)
        {
            lock (this.lockable) { base.Add(item); };
        }

        public new void AddRange(IEnumerable<T> collection)
        {
            lock (this.lockable) { base.AddRange(collection); };
        }

        public new ReadOnlyCollection<T> AsReadOnly()
        {
            lock (this.lockable) { return base.AsReadOnly(); };
        }

        public new Int32 BinarySearch(Int32 index, Int32 count, T item, IComparer<T> comparer)
        {
            lock (this.lockable) { return base.BinarySearch(index, count, item, comparer); };
        }

        public new Int32 BinarySearch(T item)
        {
            lock (this.lockable) { return base.BinarySearch(item); };
        }

        public new Int32 BinarySearch(T item, IComparer<T> comparer)
        {
            lock (this.lockable) { return base.BinarySearch(item, comparer); };
        }

        public new void Clear()
        {
            lock (this.lockable) { base.Clear(); };
        }

        public new Boolean Contains(T item)
        {
            lock (this.lockable) { return base.Contains(item); };
        }

        public new void CopyTo(T[] array)
        {
            lock (this.lockable) { base.CopyTo(array); };
        }

        public new void CopyTo(Int32 index, T[] array, Int32 arrayIndex, Int32 count)
        {
            lock (this.lockable) { base.CopyTo(index, array, arrayIndex, count); };
        }

        public new void CopyTo(T[] array, Int32 arrayIndex)
        {
            lock (this.lockable) { base.CopyTo(array, arrayIndex); };
        }

        public new Boolean Exists(Predicate<T> match)
        {
            lock (this.lockable) { return base.Exists(match); };
        }

        public new T Find(Predicate<T> match)
        {
            lock (this.lockable) { return base.Find(match); };
        }

        public new List<T> FindAll(Predicate<T> match)
        {
            lock (this.lockable) { return base.FindAll(match); };
        }

        public new Int32 FindIndex(Predicate<T> match)
        {
            lock (this.lockable) { return base.FindIndex(match); };
        }

        public new Int32 FindIndex(Int32 startIndex, Predicate<T> match)
        {
            lock (this.lockable) { return base.FindIndex(startIndex, match); };
        }

        public new Int32 FindIndex(Int32 startIndex, Int32 count, Predicate<T> match)
        {
            lock (this.lockable) { return base.FindIndex(startIndex, count, match); };
        }

        public new T FindLast(Predicate<T> match)
        {
            lock (this.lockable) { return base.FindLast(match); };
        }

        public new Int32 FindLastIndex(Predicate<T> match)
        {
            lock (this.lockable) { return base.FindLastIndex(match); };
        }

        public new Int32 FindLastIndex(Int32 startIndex, Predicate<T> match)
        {
            lock (this.lockable) { return base.FindLastIndex(startIndex, match); };
        }

        public new Int32 FindLastIndex(Int32 startIndex, Int32 count, Predicate<T> match)
        {
            lock (this.lockable) { return base.FindLastIndex(startIndex, count, match); };
        }

        public new void ForEach(Action<T> action)
        {
            lock (this.lockable) { base.ForEach(action); };
        }

        public new Enumerator GetEnumerator()
        {
            lock (this.lockable) { return base.GetEnumerator(); };
        }

        public new List<T> GetRange(Int32 index, Int32 count)
        {
            lock (this.lockable) { return base.GetRange(index, count); };
        }

        public new Int32 IndexOf(T item)
        {
            lock (this.lockable) { return base.IndexOf(item); };
        }

        public new Int32 IndexOf(T item, Int32 index)
        {
            lock (this.lockable) { return base.IndexOf(item, index); };
        }

        public new Int32 IndexOf(T item, Int32 index, Int32 count)
        {
            lock (this.lockable) { return base.IndexOf(item, index, count); };
        }

        public new void Insert(Int32 index, T item)
        {
            lock (this.lockable) { base.Insert(index, item); };
        }

        public new void InsertRange(Int32 index, IEnumerable<T> collection)
        {
            lock (this.lockable) { base.InsertRange(index, collection); };
        }

        public new Int32 LastIndexOf(T item)
        {
            lock (this.lockable) { return base.LastIndexOf(item); };
        }

        public new Int32 LastIndexOf(T item, Int32 index)
        {
            lock (this.lockable) { return base.LastIndexOf(item, index); };
        }

        public new Int32 LastIndexOf(T item, Int32 index, Int32 count)
        {
            lock (this.lockable) { return base.LastIndexOf(item, index, count); };
        }

        public new Boolean Remove(T item)
        {
            lock (this.lockable) { return base.Remove(item); };
        }

        public new Int32 RemoveAll(Predicate<T> match)
        {
            lock (this.lockable) { return base.RemoveAll(match); };
        }

        public new void RemoveAt(Int32 index)
        {
            lock (this.lockable) { base.RemoveAt(index); };
        }

        public new void RemoveRange(Int32 index, Int32 count)
        {
            lock (this.lockable) { base.RemoveRange(index, count); };
        }

        public new void Reverse()
        {
            lock (this.lockable) { base.Reverse(); };
        }

        public new void Reverse(Int32 index, Int32 count)
        {
            lock (this.lockable) { base.Reverse(index, count); };
        }

        public new void Sort()
        {
            lock (this.lockable) { base.Sort(); };
        }

        public new void Sort(IComparer<T> comparer)
        {
            lock (this.lockable) { base.Sort(comparer); };
        }

        public new void Sort(Int32 index, Int32 count, IComparer<T> comparer)
        {
            lock (this.lockable) { base.Sort(index, count, comparer); };
        }

        public new void Sort(Comparison<T> comparison)
        {
            lock (this.lockable) { base.Sort(comparison); };
        }

        public new T[] ToArray()
        {
            lock (this.lockable) { return base.ToArray(); };
        }

        public new void TrimExcess()
        {
            lock (this.lockable) { base.TrimExcess(); };
        }

        public new Boolean TrueForAll(Predicate<T> match)
        {
            lock (this.lockable) { return base.TrueForAll(match); };
        }

        public new Type GetType()
        {
            lock (this.lockable) { return base.GetType(); };
        }

        public new String ToString()
        {
            lock (this.lockable) { return base.ToString(); };
        }

        public new Boolean Equals(Object obj)
        {
            lock (this.lockable) { return base.Equals(obj); };
        }

        public new Int32 GetHashCode()
        {
            lock (this.lockable) { return base.GetHashCode(); };
        }

        #endregion


    }
}