﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Runtime.Serialization;
using System.Configuration;
using DatabaseUtilityLibrary;
using MesosoftCommon.Utilities.Text;
using CeresBase.IO.Serialization.Translation;

namespace CeresBase.IO.Serialization.PostgreSQL
{
    public class DBCache
    {
        public List<string> tableExistsCache;
        public List<string> mappingExistsCache;
        public Dictionary<string, string> nameToDBTable;
        public Dictionary<string, string> nameFromIDCache;

        public DBCache()
        {
            tableExistsCache = new List<string>();
            mappingExistsCache = new List<string>();
            nameToDBTable = new Dictionary<string, string>();
            nameFromIDCache = new Dictionary<string, string>();
        }
    }

    public class TestClassA
    {
        public int TestInt { get; set; }
        public string TestString { get; set; }
        public Dictionary<string, TestClassB> TestClassBDictionary { get; set; }
    }

    public class TestClassB
    {
        public string TestString { get; set; }
    }

    public class PostgreSQLDBEngine
    {

        public const string DefaultPort = "5432";
        //static PostgreSQLDBEngine()
        //{
        //    SerializationCentral.DatabaseChanging += new DatabaseChangingEventHandler(SerializationCentral_DatabaseChanging);
        //}

        //static void SerializationCentral_DatabaseChanging(object sender, DatabaseChangingEventArgs e)
        //{
        //    tableExistsCache.Clear();
        //    nameToDBTable.Clear();
        //    nameFromIDCache.Clear();
        //}

        public static void ClearTables()
        {
            foreach (string connString in connectionCache.Keys)
            {
                connectionCache[connString].mappingExistsCache.Clear();
                connectionCache[connString].tableExistsCache.Clear();
                connectionCache[connString].nameFromIDCache.Clear();
                connectionCache[connString].nameToDBTable.Clear();
            }
        }

        private static Dictionary<string, DBCache> connectionCache = new Dictionary<string, DBCache>();
        //private static List<string> tableExistsCache = new List<string>();
        //private static List<string> mappingExistsCache = new List<string>();
        //private static Dictionary<string, string> nameToDBTable = new Dictionary<string, string>();
        //private static Dictionary<string, string> nameFromIDCache = new Dictionary<string, string>();


        const int MaxTableNameLength = 63;

        public static string LastConnectionString = "";

        public static void Test()
        {
            NpgsqlConnection sourceConn = ConnectToDB("localhost", PostgreSQLDBEngine.DefaultPort, "postgres", "postgres", "localdb");
            //NpgsqlConnection targetConn = ConnectToDB("localhost", "5432", "postgres", "postgres", "test");

            //int x = PostgreSQLDBEngine.MassMerge(sourceConn, targetConn);
            //int xj = 0;
            //PostgreSQLDBEngine.MergeOnClassTable("IList:CeresBase.IO.DataBase.CentralVariableItemTranslator+centralStoragePara", sourceConn, targetConn);

            //ClassMapNode.GenerateClassMapNode("ClassFullName:Subname{ColumnName:TypeFullName:Param1=Val1:Param2=Val2|ColumnName2:TypeFullName:Param1=Val1|ColumnName3:TypeFullName:Param1=Val1:Param2=Val2|ColumnName4:TypeFullName:Param1=Val1}");
            //ClassMapNode.GenerateClassMapNode(typeof(TestClassA));
            //PostgreSQLDBEngine.EpochTranslatorTest(sourceConn);
        }

        public static void EpochTranslatorTest(string ilistLongName, string membersLongName, NpgsqlConnection conn)
        {
            //string listTableName = PostgreSQLDBEngine.LongNameToDBTableName("IList:CeresBase.Projects.EpochTranslator", conn);
            //string epochsTableName = PostgreSQLDBEngine.LongNameToDBTableName("CeresBase.Projects.EpochTranslator", conn);

            string listTableName = PostgreSQLDBEngine.LongNameToDBTableName(ilistLongName, conn);
            string epochsTableName = PostgreSQLDBEngine.LongNameToDBTableName(membersLongName, conn);

            if (listTableName == null || epochsTableName == null) return;

            List<string> uids = new List<string>();

            Dictionary<string, List<string>> currentListings = new Dictionary<string,List<string>>();

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;

            string sql = "SELECT guid FROM " + epochsTableName + ";";

            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            //int counter = 0;
            foreach (DataRow row in dt.Rows)
            {
                string guid = row.ItemArray[0].ToString();
                if (!uids.Contains(guid))
                    uids.Add(guid + ":" + membersLongName);
            }

            //string insertArray = PostgreSQLDBEngine.GenerateDBArrayString(uids.ToArray());

            sql = "SELECT guid, \"Value\" FROM " + listTableName + ";";

            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                string guid = row.ItemArray[0].ToString();

                if (!currentListings.Keys.Contains(guid))
                    currentListings.Add(guid, new List<string>());

                string value = row.ItemArray[1].ToString();

                if (!currentListings[guid].Contains(value))
                    currentListings[guid].Add(value);
            }

            foreach (DataRow row in dt.Rows)
            {
                string guid = row.ItemArray[0].ToString();

                StringBuilder sqlComm = new StringBuilder("PREPARE updateProc (uuid, text) AS INSERT INTO " + listTableName +
                    " (guid, \"Value\") VALUES ($1, $2);");

                foreach (string guidStr in uids)
                {
                    if (!currentListings[guid].Contains(guidStr))
                        sqlComm.Append(" EXECUTE updateProc('" + guid + "', '" + guidStr + "');");                 
                }

                sqlComm.Append(" DEALLOCATE updateProc;");


                NpgsqlCommand Comm = new NpgsqlCommand(sqlComm.ToString(), conn);
                Comm.ExecuteNonQuery();
            }

            int px = 23;
        }

        public static NpgsqlConnection ConnectToDefaultDB()
        {
            return PostgreSQLDBEngine.ConnectToDefaultDB("localhost");
        }

        public static NpgsqlConnection ConnectToDefaultDB(string hostName)
        {
            // PostgeSQL-style connection string
            string connstring = String.Format("Server={0};Port={1};" +
                "User Id={2};Password={3};Database={4};",
                hostName, PostgreSQLDBEngine.DefaultPort, "postgres",
                "postgres", "ceresdb");
            // Making connection with Npgsql provider
            NpgsqlConnection conn = new NpgsqlConnection(connstring);
            conn.Open();
            //automatically check the DB to make sure it already has the necessary support structures, add them if not
            CheckAndConfigureDB(conn);
            PostgreSQLDBEngine.LastConnectionString = connstring;
            //register the connection with the connectionCache.
            if (!PostgreSQLDBEngine.connectionCache.Keys.Contains(conn.ConnectionString))
                PostgreSQLDBEngine.connectionCache.Add(conn.ConnectionString, new DBCache());
            return conn;
        }

        public static NpgsqlConnection ConnectToDB(string connectionString)
        {
            // Making connection with Npgsql provider
            NpgsqlConnection conn = new NpgsqlConnection(connectionString);
            conn.Open();
            //automatically check the DB to make sure it already has the necessary support structures, add them if not
            CheckAndConfigureDB(conn);
            PostgreSQLDBEngine.LastConnectionString = connectionString;
            //register the connection with the connectionCache.
            if (!PostgreSQLDBEngine.connectionCache.Keys.Contains(conn.ConnectionString))
                PostgreSQLDBEngine.connectionCache.Add(conn.ConnectionString, new DBCache());
            return conn;
        }

        public static NpgsqlConnection ConnectToDB(string hostName, string port, string userId, string password, string database)
        {
            // PostgeSQL-style connection string
            string connstring = String.Format("Server={0};Port={1};" +
                "User Id={2};Password={3};Database={4};",
                hostName, port, userId,
                password, database);
            // Making connection with Npgsql provider
            NpgsqlConnection conn = new NpgsqlConnection(connstring);
            conn.Open();
            //automatically check the DB to make sure it already has the necessary support structures, add them if not
            CheckAndConfigureDB(conn);
            PostgreSQLDBEngine.LastConnectionString = connstring;
            //register the connection with the connectionCache.
            if (!PostgreSQLDBEngine.connectionCache.Keys.Contains(conn.ConnectionString))
                PostgreSQLDBEngine.connectionCache.Add(conn.ConnectionString, new DBCache());

            return conn;
        }


        private static NpgsqlConnection utilityConnection = null;

        /// <summary>
        /// Maintains a connection to the default (local) database, for utility behavior.  Connection is not invoked until a create
        /// or delete call is made.
        /// </summary>
        public static NpgsqlConnection UtilityConnection
        {
            get
            {
                if (PostgreSQLDBEngine.utilityConnection == null)
                {
                    // PostgeSQL-style connection string
                    string connstring = String.Format("Server={0};Port={1};" +
                        "User Id={2};Password={3};Database={4};",
                        "localhost", PostgreSQLDBEngine.DefaultPort, "postgres",
                        "postgres", "ceresdb");
                    // Making connection with Npgsql provider
                    NpgsqlConnection conn = new NpgsqlConnection(connstring);
                    conn.Open();
                    //automatically check the DB to make sure it already has the necessary support structures, add them if not
                    CheckAndConfigureDB(conn);
                    PostgreSQLDBEngine.utilityConnection = conn;
                    //register the connection with the connectionCache.
                    if (!PostgreSQLDBEngine.connectionCache.Keys.Contains(conn.ConnectionString))
                        PostgreSQLDBEngine.connectionCache.Add(conn.ConnectionString, new DBCache());
                }

                return PostgreSQLDBEngine.utilityConnection;
            }
        }

        public static string GetDatabaseComment(string dbName, NpgsqlConnection conn)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;

            string sql = "SELECT pg_shdescription.description FROM pg_class INNER JOIN pg_shdescription INNER JOIN (SELECT oid FROM pg_database WHERE datname='"
            + dbName + "') as pgdb ON (pgdb.oid = pg_shdescription.objoid) ON (pg_class.oid = pg_shdescription.classoid);";

            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
                return "ERROR DB NOT FOUND";

            string comment = dt.Rows[0].ItemArray[0].ToString();

            comment = comment.Replace("$QUOTE$", "'").Replace("$SLASH$", "/").Replace("$BACKSLASH$", "\\").Replace("$COLON$", ":");


            return comment;
        }

        public static void SetDatabaseComment(string dbName, string newComment, NpgsqlConnection conn)
        {
            newComment = newComment.Replace("'", "$QUOTE$").Replace("/", "$SLASH$").Replace("\\", "$BACKSLASH$").Replace(":", "$COLON$");

            string rowSQL = "COMMENT ON DATABASE " + dbName + " is '" + newComment + "';";

            NpgsqlCommand command = new NpgsqlCommand(rowSQL, conn);
            int rowsAffected = command.ExecuteNonQuery();
        }

        public static Dictionary<string, string> ListDBNamesAndComments(NpgsqlConnection conn)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;

            string sql = "SELECT pgdb.datname, pg_shdescription.description FROM pg_class RIGHT OUTER JOIN pg_shdescription RIGHT OUTER JOIN"
            + " (SELECT oid,* FROM pg_database WHERE datistemplate = false) as pgdb ON (pg_shdescription.objoid = pgdb.oid) ON"
            + " (pg_class.oid = pg_shdescription.classoid);";

            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                ret.Add(row.ItemArray[0].ToString(), row.ItemArray[1].ToString());
            }

            return ret;
        }

        public static string[] ListDBNames(NpgsqlConnection conn)
        {
            List<string> ret = new List<string>();

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;

            string sql = "SELECT * FROM pg_catalog.pg_database WHERE datistemplate = false;";

            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                ret.Add(row.ItemArray[0].ToString());
            }

            return ret.ToArray();
        }

        public static bool DatabaseExists(string dbName, NpgsqlConnection conn)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;

            string sql = "SELECT 1 FROM pg_catalog.pg_database WHERE datname = '" + dbName + "';";

            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
                return false;

            return true;
        }

        public static void CreateDatabase(string dbName, NpgsqlConnection conn)
        {

            string rowSQL = "CREATE DATABASE \"" + dbName + "\";";

            NpgsqlCommand command = new NpgsqlCommand(rowSQL, conn);
            int rowsAffected = command.ExecuteNonQuery();

        }

        public static void DeleteDatabase(string dbName, NpgsqlConnection conn)
        {

            if (dbName == "ceresdb")
            {
                return;
            }

            string rowSQL = "DROP DATABASE \"" + dbName + "\";";

            NpgsqlCommand command = new NpgsqlCommand(rowSQL, conn);
            int rowsAffected = command.ExecuteNonQuery();
        }

        private static Dictionary<string, string> computerNameCache = new Dictionary<string, string>();

        public static void SetComputerName(string newName, NpgsqlConnection conn)
        {
            PostgreSQLDBEngine.SetDBParameterValue("ComputerName", newName, conn);

            if (!computerNameCache.Keys.Contains(conn.ConnectionString))
                computerNameCache.Add(conn.ConnectionString, newName);
            else
                computerNameCache[conn.ConnectionString] = newName;
        }

        public static string GetComputerName(NpgsqlConnection conn)
        {
            if (computerNameCache.Keys.Contains(conn.ConnectionString))
                return computerNameCache[conn.ConnectionString];

            string ret = PostgreSQLDBEngine.GetDBParameterValue("ComputerName", conn);
            computerNameCache.Add(conn.ConnectionString, ret);
            return ret;
        }

        public static void CheckAndConfigureDB(NpgsqlConnection conn)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql;

            //Check for existence of table
            sql = "SELECT 1 FROM pg_catalog.pg_class WHERE relkind = 'r' AND relname = 'extended_tables'"
                + " AND pg_catalog.pg_table_is_visible(oid) LIMIT 1";
            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            //If the table doesn't exist, it needs to be created to operate the db correctly.
            if (dt.Rows.Count == 0)
            {
                sql = "CREATE TABLE extended_tables(" +
                      "table_id serial UNIQUE," +
                      "name text UNIQUE," +
                      "baseclass text);";
                NpgsqlCommand comm = new NpgsqlCommand(sql, conn);
                comm.ExecuteNonQuery();
            }

            //Check for existence of table
            sql = "SELECT 1 FROM pg_catalog.pg_class WHERE relkind = 'r' AND relname = 'class_maps'"
                + " AND pg_catalog.pg_table_is_visible(oid) LIMIT 1";
            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            //If the table doesn't exist, it needs to be created to operate the db correctly.
            if (dt.Rows.Count == 0)
            {
                sql = "CREATE TABLE class_maps(" +
                      "table_id serial UNIQUE," +
                      "map_string text);";
                NpgsqlCommand comm = new NpgsqlCommand(sql, conn);
                comm.ExecuteNonQuery();
            }

            //Check for the db_parameters table
            sql = "SELECT 1 FROM pg_catalog.pg_class WHERE relkind = 'r' AND relname = 'db_parameters'"
                + " AND pg_catalog.pg_table_is_visible(oid) LIMIT 1";

            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            //If the table doesn't exist, it needs to be created to operate the db correctly.
            if (dt.Rows.Count == 0)
            {
                sql = "CREATE TABLE db_parameters(" +
                      "name text PRIMARY KEY," +
                      "value text);";
                NpgsqlCommand comm = new NpgsqlCommand(sql, conn);
                comm.ExecuteNonQuery();
            }

            //It shouldn't ever happen, but it's important to *make sure* that the version is updated even if the db_parameters table has been created.
            string paramValue = PostgreSQLDBEngine.GetDBParameterValue("DBVersion", conn);
            if (paramValue == null || int.Parse(paramValue) < 2)
            {
                //In addition, if db_parameters hasn't been set, it means we have a version 1 db.
                //Set the DBVersion parameter and perform the v1 -> v2 update to data formatting (add row versions).
                PostgreSQLDBEngine.ConvertDBV1toV2(conn);

                //Only set the version AFTER performing the row version additions.
                PostgreSQLDBEngine.SetDBParameterValue("DBVersion", "2", conn);
            }

            string escapedSQL = "SET standard_conforming_strings=on;";

            NpgsqlCommand command = new NpgsqlCommand(escapedSQL, conn);
            int rowsAffected = command.ExecuteNonQuery();

            //Version 3 update (changing format of collections)
            paramValue = PostgreSQLDBEngine.GetDBParameterValue("DBVersion", conn);
            if (int.Parse(paramValue) < 3)
            {
                PostgreSQLDBEngine.ConvertDBV2toV3(conn);

                PostgreSQLDBEngine.SetDBParameterValue("DBVersion", "3", conn);
            }

            //Load the Class Maps for the given connection.
            ClassMap.LoadClassMaps(conn);
        }

        public static void SetDBParameterValue(string parameterName, string value, NpgsqlConnection conn)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql;

            sql = "SELECT * FROM db_parameters WHERE name='" + parameterName + "';";

            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
            {
                //Already doesn't exist, so setting it to null is nonsensical.
                if (value == null)
                {
                    return;
                }
                else
                    sql = "INSERT INTO db_parameters (name, value) VALUES ('" + parameterName + "', '" + value + "');";
            }
            else
            {
                //DELETE the row to achieve equivalency with null.
                if (value == null)
                    sql = "DELETE FROM db_parameters WHERE name='" + parameterName + "';";
                else
                    sql = "UPDATE db_parameters SET value = '" + value + "' WHERE name='" + parameterName + "';";
            }

            NpgsqlCommand command = new NpgsqlCommand(sql, conn);

            int rowsAffected = command.ExecuteNonQuery();
        }

        public static string GetDBParameterValue(string parameterName, NpgsqlConnection conn)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql;

            sql = "SELECT * FROM db_parameters WHERE name='" + parameterName + "';";

            //NpgsqlTransaction trans = conn.BeginTransaction();
            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
                return null;

            object[] thisData = dt.Rows[0].ItemArray;
            return thisData[1] as string;
        }

        public static string[] ListAllTablesInDB(NpgsqlConnection conn)
        {
            List<string> ret = new List<string>();

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql;

            //Fetch the names of all tables in the database
            sql = "SELECT pg_class.relname FROM pg_catalog.pg_class INNER JOIN pg_catalog.pg_namespace ON "
                + "(pg_class.relnamespace = pg_namespace.oid) WHERE relkind = 'r' AND pg_catalog.pg_table_is_visible(pg_class.oid) AND "
                + "pg_namespace.nspname = 'public'";
            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                string tableName = row.ItemArray[0] as string;
                ret.Add(tableName);
            }

            return ret.ToArray();
        }

        public static void ConvertDBV1toV2(NpgsqlConnection conn)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql;

            //Fetch the names of all tables in the database
            sql = "SELECT pg_class.relname FROM pg_catalog.pg_class INNER JOIN pg_catalog.pg_namespace ON "
                + "(pg_class.relnamespace = pg_namespace.oid) WHERE relkind = 'r' AND pg_catalog.pg_table_is_visible(pg_class.oid) AND "
                + "pg_namespace.nspname = 'public'";
            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            string[] tableList = PostgreSQLDBEngine.ListAllTablesInDB(conn);

            foreach (string tableName in tableList)
            {
                //Do not version extended_tables or the configuration table.
                if (tableName == "extended_tables" || tableName == "db_parameters")
                    continue;

                string[] columns = PostgreSQLDBEngine.GetTableColumnNamesFromInternalName(tableName, conn, true);

                string alterString = "";
                //v1->v2 : add "row_version" and "version_updated_by" 
                if (!columns.Contains("row_version"))
                {
                    alterString += "ADD row_version integer DEFAULT '0'";
                }

                if (!columns.Contains("version_updated_by"))
                {
                    if (alterString != "")
                        alterString += ", ";
                    alterString += "ADD version_updated_by text DEFAULT 'system_maintenance'";
                }

                if (alterString != "")
                {
                    string alterSQL = "ALTER TABLE " + tableName + " " + alterString + ";";

                    NpgsqlCommand command = new NpgsqlCommand(alterSQL, conn);
                    int rowsAffected = command.ExecuteNonQuery();
                }
            }

            //In addition to converting all tables, we must add the basic "increment" function to handle version incrementing.
            string incrementSQL = "CREATE OR REPLACE FUNCTION increment(i integer) RETURNS integer AS $$ BEGIN RETURN i + 1; END; $$ LANGUAGE plpgsql;";
            NpgsqlCommand newcommand = new NpgsqlCommand(incrementSQL, conn);
            int rowsAffected2 = newcommand.ExecuteNonQuery();
        }

        public static void ConvertDBV2toV3(NpgsqlConnection conn)
        {
            string[] tableList = PostgreSQLDBEngine.ListAllTablesInDB(conn);

            foreach (string tableName in tableList)
            {
                //ignore system tables.
                if (tableName == "extended_tables" || tableName == "db_parameters" || tableName == "class_maps")
                    continue;

                string fullName = PostgreSQLDBEngine.TableNameFromInternalName(tableName, conn);
                string split = fullName.Split(':')[0];
                if (split == "IList" || split == "IDictionary")
                {
                    //All IList and IDictionaries must be converted.
                    //First step, dump the contents of the table to a data table
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    NpgsqlDataAdapter da;
                    string sql;

                    sql = "SELECT * FROM " + tableName + ";";
                    da = new NpgsqlDataAdapter(sql, conn);

                    ds.Reset();
                    da.Fill(ds);
                    dt = ds.Tables[0];

                    //Second step, rename the table.
                    string oldTableName = tableName + "_OLD";

                    sql = "ALTER TABLE " + tableName + " RENAME TO " + oldTableName + ";";
                    NpgsqlCommand comm = new NpgsqlCommand(sql, conn);

                    comm.ExecuteNonQuery();

                    //Next, create the new table.
                    sql = "CREATE TABLE \"" + tableName + "\" (" +
                        "\"guid\"    uuid," +
                        "\"row_version\"     integer DEFAULT '0'," +
                        "\"version_updated_by\"     text DEFAULT 'system_maintenance'";

                    if (split == "IDictionary")
                    {
                        sql += ", \"Key\"    ";
                        sql += "text";
                        sql += ", \"Value\"    ";
                        sql += "text";
                    }
                    else if (split == "IList")
                    {
                        sql += ", \"Value\"    ";
                        sql += "text";
                    }

                    sql += ");";

                    NpgsqlCommand command = new NpgsqlCommand(sql, conn);

                    command.ExecuteNonQuery();


                    //Fetch indexes
                    int guidIndex = dt.Columns.IndexOf("guid");
                    int valueIndex = dt.Columns.IndexOf("Values");

                    //Now handle filtering the data into its new location.
                    if (split == "IList")
                    {
                        StringBuilder inserts = new StringBuilder("");
                        inserts.Append("PREPARE collectionInsert (uuid, text) AS INSERT INTO " + tableName +
                            " (guid, \"Value\") VALUES ($1, $2);");
                        foreach (DataRow row in dt.Rows)
                        {
                            Guid guid = (Guid)row.ItemArray[guidIndex];
                            string[] values = row.ItemArray[valueIndex] as string[];

                            foreach (string valStr in values)
                            {
                                inserts.Append(" EXECUTE collectionInsert('" + guid + "', '" +  valStr + "');");
                            }

                            //We have to maintain the integrity of an empty collection.
                            if (values.Length == 0)
                            {
                                //inserts.Append(" EXECUTE collectionInsert('" + guid + "', null);");
                            }
                        }

                        inserts.Append(" DEALLOCATE collectionInsert;");
                        sql = inserts.ToString();
                    }
                    else
                    {
                        int keyIndex = dt.Columns.IndexOf("Keys");

                        StringBuilder inserts = new StringBuilder("");
                        inserts.Append("PREPARE collectionInsert (uuid, text, text) AS INSERT INTO " + tableName +
                            " (guid, \"Key\", \"Value\") VALUES ($1, $2, $3);");

                        foreach (DataRow row in dt.Rows)
                        {
                            Guid guid = (Guid)row.ItemArray[guidIndex];
                            string[] values = row.ItemArray[valueIndex] as string[];
                            string[] keys = row.ItemArray[keyIndex] as string[];

                            for (int i = 0; i < keys.Length; i++)
                            {
                                string valStr = values[i];
                                string keyStr = keys[i];

                                inserts.Append(" EXECUTE collectionInsert('" + guid + "', '" + keyStr + "', '" + valStr + "');");
                            }

                            //We have to maintain the integrity of an empty collection.
                            if (keys.Length == 0)
                            {
                                //inserts.Append(" EXECUTE collectionInsert('" + guid + "', null, null);");
                            }
                        }

                        inserts.Append(" DEALLOCATE collectionInsert;");
                        sql = inserts.ToString();
                    }

                    //Do the inserts.
                    comm = new NpgsqlCommand(sql, conn);
                    comm.ExecuteNonQuery();
                    

                    //Remove the old table.
                    sql = "DROP TABLE " + oldTableName + ";";

                    comm = new NpgsqlCommand(sql, conn);

                    comm.ExecuteNonQuery();
                    int jk = 0;
                }
            }
        }

        public static bool MapExists(string tableName, NpgsqlConnection conn)
        {
            if (connectionCache[conn.ConnectionString].mappingExistsCache.Contains(tableName))
                return true;
            //if (mappingExistsCache.Contains(tableName))
            //    return true;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql = "SELECT extended_tables.table_id FROM extended_tables INNER JOIN class_maps WHERE extended_tables.name = '" + tableName +
                         "' ON extended_tables.table_id = class_maps.table_id";
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            if (dt.Rows.Count == 0) return false;

            connectionCache[conn.ConnectionString].mappingExistsCache.Add(tableName);
            //mappingExistsCache.Add(tableName);
            return true;
        }

        public static bool TableIsRegistered(string tableName, NpgsqlConnection conn)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql;

            //Check for the tablename in the special "extended tables" list
            sql = "SELECT * FROM extended_tables WHERE name = '" + tableName + "'";
            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
                return false;
            else
                return true;
        }

        public static bool TableIsRegistered(RelationalTable table, NpgsqlConnection conn)
        {
            return PostgreSQLDBEngine.TableIsRegistered(table.TableType.FullName, conn);
        }

        public static TimeSpan tTableExists = new TimeSpan();
        public static TimeSpan tTableExists1 = new TimeSpan();
        public static TimeSpan tTableExists2 = new TimeSpan();


        public static bool TableExists(string tableName, NpgsqlConnection conn)
        {
            if (connectionCache[conn.ConnectionString].tableExistsCache.Contains(tableName))
                return true;
            //if (tableExistsCache.Contains(tableName))
            //    return true;

            DateTime now = DateTime.Now;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql;

            tTableExists1 = tTableExists1.Add(DateTime.Now.Subtract(now));

            //Check for the tablename in the special "extended tables" list
            sql = "SELECT * FROM extended_tables WHERE name = '" + tableName + "'";
            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            tTableExists2 = tTableExists2.Add(DateTime.Now.Subtract(now));

            if (dt.Rows.Count == 0)
                return false;

            string internalName = "table_" + dt.Rows[0].ItemArray[0].ToString();

            //Check for existence of table
            sql = "SELECT 1 FROM pg_catalog.pg_class WHERE relkind = 'r' AND relname='" + internalName
                + "' AND pg_catalog.pg_table_is_visible(oid) LIMIT 1";
            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            tTableExists = tTableExists.Add(DateTime.Now.Subtract(now));

            if (dt.Rows.Count == 0)
                return false;

            connectionCache[conn.ConnectionString].tableExistsCache.Add(tableName);
            return true;
        }

        public static bool TableExists(RelationalTable table, NpgsqlConnection conn)
        {
            return PostgreSQLDBEngine.TableExists(table.TableType.FullName, conn);
        }

        public static Dictionary<string, string> GetTableColumnNamesAndTypesFromInternalName(string internalName, NpgsqlConnection conn, bool getSystemColumns)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;

            //SUPER QUERY OF DOOM
            string sql = "SELECT a.attname, pg_catalog.format_type(a.atttypid, a.atttypmod) FROM "
            + "pg_catalog.pg_attribute a WHERE a.attnum > 0 AND NOT a.attisdropped AND a.attrelid = "
            + "(SELECT c.oid FROM pg_catalog.pg_class c LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace "
            + "WHERE c.relname ~ '^(" + internalName + ")$' AND pg_catalog.pg_table_is_visible(c.oid))";

            //, pg_catalog.format_type(a.atttypid, a.atttypmod) <-- Column Type Fragment
            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                object[] data = row.ItemArray;

                string colName = data[0] as string;
                string colType = data[1] as string;

                if (!getSystemColumns)
                {
                    if (colName != "row_version" && colName != "version_updated_by")
                        ret.Add(colName, colType);
                }
                else
                {
                    ret.Add(colName, colType);
                }
            }

            return ret;
        }

        public static string[] GetTableColumnNamesFromInternalName(string internalName, NpgsqlConnection conn, bool getSystemColumns)
        {
            List<string> ret = new List<string>();

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;

            //SUPER QUERY OF DOOM
            string sql = "SELECT a.attname FROM "
            + "pg_catalog.pg_attribute a WHERE a.attnum > 0 AND NOT a.attisdropped AND a.attrelid = "
            + "(SELECT c.oid FROM pg_catalog.pg_class c LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace "
            + "WHERE c.relname ~ '^(" + internalName + ")$' AND pg_catalog.pg_table_is_visible(c.oid))";

            //, pg_catalog.format_type(a.atttypid, a.atttypmod) <-- Column Type Fragment
            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                object[] data = row.ItemArray;

                string colName = data[0] as string;

                if (!getSystemColumns)
                {
                    if (colName != "row_version" && colName != "version_updated_by")
                        ret.Add(colName);
                }
                else
                {
                    ret.Add(colName);
                }
            }

            return ret.ToArray();
        }

        public static string[] GetTableColumnNames(string tableName, NpgsqlConnection conn)
        {
            string internalName = PostgreSQLDBEngine.LongNameToDBTableName(tableName, conn);
            return PostgreSQLDBEngine.GetTableColumnNamesFromInternalName(internalName, conn, false);
        }

        public static string[] GetTableColumnNames(RelationalTable table, NpgsqlConnection conn)
        {
            return PostgreSQLDBEngine.GetTableColumnNames(table.TableType.FullName, conn);
        }

        public static string GetTableNameFromHelperRow(RelationalRow row, NpgsqlConnection conn)
        {
            string tableName = "";
            string helperType = row["PropertyName"] as string;

            if (helperType == "DictionaryItems")
            {
                Type[] genArgs = row["OriginalReference"].GetType().GetGenericArguments();

                tableName = "IDictionary:" + genArgs[0].FullName + ":" + genArgs[1].FullName;
            }
            else if (helperType == "ListItems")
            {
                Type listType = row["OriginalReference"].GetType();
                //Type listType = row["OriginalReference"].GetType().GetGenericArguments()[0];

                Type forName;
                if (listType.IsGenericType)
                {
                    forName = listType.GetGenericArguments()[0];
                }
                else
                {
                    forName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(listType.FullName.Split('[')[0]);
                }

                CeresBase.IO.Serialization.Translation.ITranslator trans = DatabaseUtilityLibrary.TranslatorStore.GetTranslator(forName);

                if (trans != null)
                    forName = trans.GetType();

                tableName = "IList:" + forName.FullName;
            }
            else
                throw new Exception("Unexpected Helper classification in stream.");

            return tableName;
        }

        public static bool HelperTableExists(RelationalRow row, NpgsqlConnection conn)
        {
            string tableName = PostgreSQLDBEngine.GetTableNameFromHelperRow(row, conn);
            return PostgreSQLDBEngine.TableExists(tableName, conn);
        }

        public static string GetDBTypeName(Type type)
        {
            if (type == typeof(int))
            {
                return "integer";
            }
            else if (type == typeof(double))
            {
                return "double precision";
            }
            else if (type == typeof(float))
            {
                return "real";
            }
            else if (type == typeof(string))
            {
                return "text";
            }
            else if (type == typeof(char))
            {
                return "char(1)";
            }
            else if (type == typeof(bool))
            {
                return "boolean";
            }
            else if (type == typeof(DateTime))
            {
                return "timestamp";
            }
            else if (type == typeof(Guid))
            {
                return "uuid";
            }
            else if (type.GetInterfaces().Contains(typeof(IDictionary)))
            {
                return "uuid";
            }
            else if (type.GetInterfaces().Contains(typeof(IList)))
            {
                return "uuid";
            }
            else if (type.IsEnum)
            {
                return "uuid";
            }

            return "uuid";
        }

        //public static string TypeNameToDBName(string typeName)
        //{
        //    return typeName;
        //}

        //public static string DBNameToTypeName(string DBName)
        //{
        //    return DBName;
        //}

        public static string RegisterNewLongTableName(string longName, NpgsqlConnection conn)
        {
            string rowSQL = "INSERT INTO extended_tables (table_id, name) VALUES (DEFAULT, '" + longName + "');";

            NpgsqlCommand command = new NpgsqlCommand(rowSQL, conn);
            int rowsAffected = command.ExecuteNonQuery();
            if (rowsAffected == 0)
                throw new Exception("Failed to insert row.");

            //Now find out what ID was given by the auto-increment
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql = "SELECT * FROM extended_tables WHERE name = '" + longName + "'";
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            return "table_" + dt.Rows[0].ItemArray[0].ToString();
        }


        public static string LongNameToDBTableName(string longName, NpgsqlConnection conn)
        {
            if (longName == "System.Collections.ObjectModel.ObservableCollection`1[[CeresBase.Projects.Epoch, CeresBase, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]")
            {
                int s = 0;
            }

            if (connectionCache[conn.ConnectionString].nameToDBTable.ContainsKey(longName))
            {
                return connectionCache[conn.ConnectionString].nameToDBTable[longName];
            }

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql = "SELECT * FROM extended_tables WHERE name = '" + longName + "'";
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            if (dt.Rows.Count == 0) return null;

            string toRet = "table_" + dt.Rows[0].ItemArray[0].ToString();

            connectionCache[conn.ConnectionString].nameToDBTable.Add(longName, toRet);
            return toRet;
        }

        public static int LongNameToDBTableID(string longName, NpgsqlConnection conn)
        {
            string DBTableName = PostgreSQLDBEngine.LongNameToDBTableName(longName, conn);
            if (DBTableName == null) return -1;
            return int.Parse(DBTableName.Split('_')[1]);
        }

        public static string TableNameFromInternalName(string internalName, NpgsqlConnection conn)
        {
            DataSet recurseDs = new DataSet();
            DataTable recurseDt = new DataTable();
            NpgsqlDataAdapter recurseDa;
            string sql = "SELECT * FROM extended_tables WHERE table_id = " + internalName.Split('_')[1];
            recurseDa = new NpgsqlDataAdapter(sql, conn);
            recurseDs.Reset();
            recurseDa.Fill(recurseDs);
            recurseDt = recurseDs.Tables[0];

            string ret = recurseDt.Rows[0].ItemArray[1] as string;

            return ret;
        }

        public static string TableNameFromID(string id, NpgsqlConnection conn)
        {
            if (connectionCache[conn.ConnectionString].nameFromIDCache.Keys.Contains(id))
                return connectionCache[conn.ConnectionString].nameFromIDCache[id];

            DataSet recurseDs = new DataSet();
            DataTable recurseDt = new DataTable();
            NpgsqlDataAdapter recurseDa;
            string sql = "SELECT * FROM extended_tables WHERE table_id = " + id;
            recurseDa = new NpgsqlDataAdapter(sql, conn);
            recurseDs.Reset();
            recurseDa.Fill(recurseDs);
            recurseDt = recurseDs.Tables[0];

            string ret = recurseDt.Rows[0].ItemArray[1] as string;
            connectionCache[conn.ConnectionString].nameFromIDCache.Add(id, ret);

            return ret;
        }

        public static TimeSpan tCreateTable_SQLCall = new TimeSpan();
        public static TimeSpan tCreateTable_NonSQL = new TimeSpan();

        public static string[] CreateTable(RelationalTable table, NpgsqlConnection conn)
        {
            List<string> ret = new List<string>();
            DateTime now = DateTime.Now;

            string tableName = "";
            if (!PostgreSQLDBEngine.TableIsRegistered(table, conn))
                tableName = PostgreSQLDBEngine.RegisterNewLongTableName(table.TableType.FullName, conn);
            else
                tableName = PostgreSQLDBEngine.LongNameToDBTableName(table.TableType.FullName, conn);

            //The table's internal name is the first entry in the return list.
            ret.Add(tableName);

            string sql = "CREATE TABLE \"" + tableName + "\" (" +
                "\"guid\"    uuid PRIMARY KEY," +
                "\"row_version\"     integer DEFAULT '0'," +
                "\"version_updated_by\"     text DEFAULT 'system_maintenance'";
            for (int i = 0; i < table.ColumnNames.Count; i++)
            {
                string colName = table.ColumnNames[i];
                Type colType = table.ColumnTypes[i];
                string dbColType = PostgreSQLDBEngine.GetDBTypeName(colType);

                //Append the type identifier to the colname if it is a "link" (uid) type
                if (colType == typeof(object))
                {
                    dbColType = "text";
                }
                else if (dbColType == "uuid")
                {
                    //For a type with a translator, we want to NOT strictly define the column type.
                    if (!DatabaseUtilityLibrary.TranslatorStore.HasTranslator(colType))
                    {
                        string nameToCheck = PostgreSQLDBEngine.GetTableName(colType);

                        if (PostgreSQLDBEngine.TableExists(nameToCheck, conn))
                            colName += ":" + PostgreSQLDBEngine.LongNameToDBTableID(nameToCheck, conn);
                        else
                        {
                            string newTableName = "";
                            if (!PostgreSQLDBEngine.TableIsRegistered(nameToCheck, conn))
                                newTableName = PostgreSQLDBEngine.RegisterNewLongTableName(nameToCheck, conn);
                            else
                                newTableName = PostgreSQLDBEngine.LongNameToDBTableName(nameToCheck, conn);
                            colName += ":" + newTableName.Split('_')[1];
                        }
                    }
                    else
                    {
                        //For columns with a translator, we want to type it to text.
                        dbColType = "text";
                    }
                }

                if (colName.Length > 63)
                    throw new Exception("Cannot store a column name longer than 63 characters.  Please shorten property name : " +
                                        table.ColumnNames[i] + " before attempting to store it in this stream.");

                //Store all column names in the return report.
                ret.Add(colName);

                sql += ", \"" + colName + "\"    ";
                sql += dbColType;
            }
            sql += ");";

            NpgsqlCommand command = new NpgsqlCommand(sql, conn);
            tCreateTable_NonSQL = tCreateTable_NonSQL.Add(DateTime.Now.Subtract(now));

            now = DateTime.Now;

            int created = command.ExecuteNonQuery();
            tCreateTable_SQLCall = tCreateTable_SQLCall.Add(DateTime.Now.Subtract(now));

            if (created > 0)
                connectionCache[conn.ConnectionString].tableExistsCache.Add(table.TableType.FullName);

            PostgreSQLDBEngine.UpdateTableSubclassReferences(conn);

            return ret.ToArray();
        }

        public static string GetTableName(Type fromType)
        {
            string nameToCheck = fromType.FullName;
            if (fromType.GetInterfaces().Contains(typeof(IDictionary)))
            {
                Type[] args = fromType.GetGenericArguments();
                string arg0 = args[0].FullName;
                //if (PostgreSQLDBEngine.GetDBTypeName(args[0]) == "uuid")
                //    arg0 = typeof(Guid).FullName;
                string arg1 = args[1].FullName;
                //if (PostgreSQLDBEngine.GetDBTypeName(args[1]) == "uuid")
                //    arg1 = typeof(Guid).FullName;
                nameToCheck = "IDictionary:" + arg0 + ":" + arg1;
            }
            else if (fromType.GetInterfaces().Contains(typeof(IList)))
            {
                Type forName;
                if (fromType.IsGenericType)
                {
                    Type[] args = fromType.GetGenericArguments();
                    string arg0 = args[0].FullName;
                    //if (PostgreSQLDBEngine.GetDBTypeName(args[0]) == "uuid")
                    //    arg0 = typeof(Guid).FullName;
                    forName = args[0];
                    //nameToCheck = "IList:" + arg0;
                }
                else if (fromType.IsArray)
                {
                    forName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(fromType.FullName.Split('[')[0]);
                    //nameToCheck = "IList:" + fromType.FullName.Split('[')[0];
                }
                else
                    throw new Exception("Unknown IList type.");

                CeresBase.IO.Serialization.Translation.ITranslator trans = DatabaseUtilityLibrary.TranslatorStore.GetTranslator(forName);

                if (trans != null)
                    forName = trans.GetType();

                nameToCheck = "IList:" + forName.FullName;
            }
            return nameToCheck;
        }

        public static string[] CreateHelperTable(RelationalRow row, NpgsqlConnection conn)
        {
            List<string> ret = new List<string>();
            string tableName = PostgreSQLDBEngine.GetTableNameFromHelperRow(row, conn);

            if (!PostgreSQLDBEngine.TableIsRegistered(tableName, conn))
                tableName = PostgreSQLDBEngine.RegisterNewLongTableName(tableName, conn);
            else
                tableName = PostgreSQLDBEngine.LongNameToDBTableName(tableName, conn);

            //TableName goes first onto the return list
            ret.Add(tableName);

            string sql = "CREATE TABLE \"" + tableName + "\" (" +
                "\"guid\"    uuid," +
                "\"row_version\"     integer DEFAULT '0'," +
                "\"version_updated_by\"     text DEFAULT 'system_maintenance'";

            if (row["PropertyName"].ToString() == "DictionaryItems")
            {
                //DictionaryItems have two string[] columns consisting of special value:type strings.
                sql += ", \"Key\"    ";
                sql += "text";
                sql += ", \"Value\"    ";
                sql += "text";
                ret.Add("Key");
                ret.Add("Value");
            }
            else if (row["PropertyName"].ToString() == "ListItems")
            {
                //ListItems have one string[] column consisting of special value:type strings.
                sql += ", \"Value\"    ";
                sql += "text";
                ret.Add("Value");
            }

            sql += ");";

            NpgsqlCommand command = new NpgsqlCommand(sql, conn);

            command.ExecuteNonQuery();

            PostgreSQLDBEngine.UpdateTableSubclassReferences(conn);
            return ret.ToArray();
        }

        public static void UpdateTableSubclassReferences(NpgsqlConnection conn)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql = "SELECT * FROM extended_tables;";
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            List<object> rowData = new List<object>();

            //Pull out the row data
            foreach (DataRow row in dt.Rows)
            {
                rowData.Add(row.ItemArray);
            }

            //Pull out the type names
            object[] currentTypes = new object[rowData.Count];
            for (int i = 0; i < rowData.Count; i++)
            {
                object[] rowDat = rowData[i] as object[];

                currentTypes[i] = rowDat[1];
            }

            //Replace the type names with types
            for (int i = 0; i < currentTypes.Length; i++)
            {
                string typeName = currentTypes[i] as string;

                if (typeName.Contains(':'))
                {
                    currentTypes[i] = typeof(object);
                }
                else
                {
                    currentTypes[i] = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeName);
                }
            }

            Dictionary<int, Type> alterationsToBaseType = new Dictionary<int, Type>();

            for (int i = 0; i < currentTypes.Length; i++)
            {
                Type thisType = currentTypes[i] as Type;
                Type baseType = thisType.BaseType;

                if ((rowData[i] as object[])[2].GetType() != typeof(DBNull) && (rowData[i] as object[])[2] != null)
                    continue;

                for (int j = 0; j < currentTypes.Length; j++)
                {
                    Type innerType = currentTypes[j] as Type;

                    if (innerType == typeof(object))
                        continue;

                    if (baseType == innerType)
                    {
                        alterationsToBaseType.Add(i, innerType);
                        break;
                    }
                }
            }

            if (alterationsToBaseType.Keys.Count > 0)
            {

                //Begin to prepare the PreparedInsert
                sql = "PREPARE preparedUpdate (text, int) AS " +
                      "UPDATE extended_tables SET (baseclass) = ($1) WHERE table_id = $2;";

                foreach (int index in alterationsToBaseType.Keys)
                {
                    int tableID = (int)((rowData[index] as object[])[0]);
                    string newBaseType = alterationsToBaseType[index].FullName;
                    sql += "EXECUTE preparedUpdate ('" + newBaseType + "', " + tableID.ToString() + ");";
                }

                sql += "DEALLOCATE preparedUpdate;";

                NpgsqlCommand command = new NpgsqlCommand(sql, conn);

                int rowsAffected = command.ExecuteNonQuery();
                if (rowsAffected == 0)
                    throw new Exception("Failed to insert/update row.");
            }
        }

        public static TimeSpan tRowExists = new TimeSpan();

        public static bool RowExists(string tableName, Guid rowID, NpgsqlConnection conn)
        {
            DateTime now = DateTime.Now;

            //DataSet ds = new DataSet();
            //DataTable dt = new DataTable();
            //NpgsqlDataAdapter da;
            //string sql = "SELECT * FROM " + tableName + " WHERE guid = '" + rowID.ToString() + "'";
            //da = new NpgsqlDataAdapter(sql, conn);
            //ds.Reset();
            //da.Fill(ds);
            //dt = ds.Tables[0];

            //

            //if (dt.Rows.Count == 0)
            //    return false;

            //return true;

            bool ret = SerializedObjectManager.IsUIDSerialized(rowID);

            tRowExists = tRowExists.Add(DateTime.Now.Subtract(now));

            return ret;
        }

        public static string[] GetStringArray(object arrayOrString)
        {
            string[] arr = arrayOrString as string[];
            string str = arrayOrString as string;

            if (arr != null)
                return arr;
            else if (str != null)
            {
                List<string> wrap = new List<string>();
                wrap.Add(str);
                return wrap.ToArray();
            }
            else
                return null;
        }

        public static TimeSpan tInsertOrUpdateRows = new TimeSpan();
        public static TimeSpan tInsertOrUpdateRows1 = new TimeSpan();
        public static int tEntries = 0;

        public static void InsertOrUpdateRows(RelationalTable table, NpgsqlConnection conn, NpgsqlTransaction transaction, bool compareAndUpdateCollections, bool deleteObjects, Dictionary<string, List<string>> createdTables)
        {

            bool tableIsHelper = false;

            //RelationalFormatterHelper is used to encapsulate special types, such as IDictionary.  Pull it apart and make it compatible!
            if (table.TableType == typeof(RelationalFormatterHelper))
                tableIsHelper = true;

            string tableName = "";
            string[] colNames = null;

            if (!tableIsHelper)
            {
                tableName = PostgreSQLDBEngine.LongNameToDBTableName(table.TableType.FullName, conn);
                colNames = PostgreSQLDBEngine.GetTableColumnNames(table, conn);
            }

            foreach (RelationalRow row in table.Rows.Values)
            {
                bool update = false;

                //Helper tables have a seperate DBTable per row.
                if (tableIsHelper)
                {
                    tableName = PostgreSQLDBEngine.GetTableNameFromHelperRow(row, conn);
                    tableName = PostgreSQLDBEngine.LongNameToDBTableName(tableName, conn);


                }



                if (PostgreSQLDBEngine.RowExists(tableName, row.RowIdentifier, conn))
                    update = true;

                //Do NOT try to call delete on rows that don't exist.  It won't be appreciated by the DB.
                if (update == false && deleteObjects)
                    continue;

                DateTime now = DateTime.Now;

                #region Build query

                //string colNameSQL = "guid";
                MesosoftCommon.Utilities.Text.TextUtilities.AddStringToCreate("colNameSQL");
                MesosoftCommon.Utilities.Text.TextUtilities.AddToString("colNameSQL", "guid");

                //                string valueSQL = "'" + row.RowIdentifier + "'";
                MesosoftCommon.Utilities.Text.TextUtilities.AddStringToCreate("valueSQL");
                MesosoftCommon.Utilities.Text.TextUtilities.AddToString("valueSQL", "'" + row.RowIdentifier + "'");

                StringBuilder collectionInsertSQL = new StringBuilder("");
                int collectionInserts = 0;
                StringBuilder collectionUpdateSQL = new StringBuilder("");
                int collectionUpdates = 0;

                if (tableIsHelper)
                {
                    if ((row["PropertyName"] as string) == "DictionaryItems")
                    {
                        collectionInsertSQL.Append("PREPARE collectionInsert (uuid, text, text) AS INSERT INTO " + tableName +
                            " (guid, \"Key\", \"Value\", \"version_updated_by\") VALUES ($1, $2, $3, '" + PostgreSQLDBEngine.GetComputerName(conn) + "');");

                        collectionUpdateSQL.Append("PREPARE collectionUpdate (uuid, text, text) AS UPDATE " + tableName +
                            " SET (\"Value\", \"row_version\", \"version_updated_by\") = ($3, increment(\"row_version\"), '" + PostgreSQLDBEngine.GetComputerName(conn) + 
                            "') WHERE guid = $1 AND \"Key\" = $2;");

                        object[] rowValues = (row["Value"] as object[]);
                        object[] rowTypes = (row["OriginatingTypeNames"] as object[]);
                        object[] keyValues = rowValues[0] as object[];
                        object[] valValues = rowValues[1] as object[];

                        //colNameSQL += ", \"Keys\", \"Values\"";
                        MesosoftCommon.Utilities.Text.TextUtilities.AddToString("colNameSQL", ", \"Keys\", \"Values\"");

                        List<string> keyStrings = new List<string>();
                        List<string> valueStrings = new List<string>();

                        for (int i = 0; i < keyValues.Length; i++)
                        {
                            string keyString = keyValues[i].ToString();
                            string valString = valValues[i].ToString();
                            string typesString = rowTypes[i].ToString();

                            keyStrings.Add(keyString + ":" + typesString.Split(':')[0]);
                            valueStrings.Add(valString + ":" + typesString.Split(':')[1]);
                        }

                        //Under the new collection format, we must ALWAYS compare, in order to know what values are already there.
                        //Overwriting the "collection" is no longer really viable, all methods must be an add or delete.
                        DataSet ds = new DataSet();
                        DataTable dt = new DataTable();
                        NpgsqlDataAdapter da;
                        string readRowSQL = "SELECT guid, \"Key\", \"Value\" FROM " + tableName + " WHERE guid = '" + row.RowIdentifier + "'";
                        da = new NpgsqlDataAdapter(readRowSQL, conn);
                        ds.Reset();
                        da.Fill(ds);
                        dt = ds.Tables[0];

                        int valueIndex = dt.Columns.IndexOf("Value");
                        int keyIndex = dt.Columns.IndexOf("Key");

                        List<string> keyUpdateStrings = new List<string>();
                        List<string> valueUpdateStrings = new List<string>();

                        //If there is a matching key string, then both it and its value string shall be removed.
                        //Following that, if the value differs from the stored value string, add the key string and value string to the updateStrings lists.
                        foreach (DataRow dataRow in dt.Rows)
                        {
                            string key = dataRow.ItemArray[keyIndex].ToString();
                            string value = dataRow.ItemArray[valueIndex].ToString();

                            if (keyStrings.Contains(key))
                            {
                                string valueForNewKey = valueStrings[keyStrings.IndexOf(key)];
                                if (valueForNewKey != value)
                                {
                                    keyUpdateStrings.Add(key);
                                    valueUpdateStrings.Add(valueForNewKey);
                                }

                                keyStrings.Remove(key);
                                valueStrings.Remove(valueForNewKey);
                            }
                        }

                        for (int i = 0; i < keyStrings.Count; i++)
                        {
                            collectionInsertSQL.Append(" EXECUTE collectionInsert('" + row.RowIdentifier.ToString() + "', '" + keyStrings[i] +
                                "', '" + valueStrings[i] + "');");
                        }

                        for (int i = 0; i < keyUpdateStrings.Count; i++)
                        {
                            collectionUpdateSQL.Append(" EXECUTE collectionUpdate('" + row.RowIdentifier.ToString() + "', '" + keyUpdateStrings[i] +
                                "', '" + valueUpdateStrings[i] + "');");
                        }

                        ////If there are no records with the key in it and no strings being added, we must add the "empty key"
                        //if (dt.Rows.Count == 0 && keyStrings.Count == 0)
                        //{
                        //    collectionInsertSQL.Append(" EXECUTE collectionInsert('" + row.RowIdentifier.ToString() + "', null, null);");
                        //    collectionInserts = 1;
                        //}
                        //else
                        //{
                            collectionInserts = keyStrings.Count;
                        //}

                        collectionInsertSQL.Append(" DEALLOCATE collectionInsert;");
                        collectionUpdateSQL.Append(" DEALLOCATE collectionUpdate;");

                        collectionUpdates = keyUpdateStrings.Count;
                    }
                    else if ((row["PropertyName"] as string) == "ListItems")
                    {
                        collectionInsertSQL.Append("PREPARE collectionInsert (uuid, text) AS INSERT INTO " + tableName +
                            " (guid, \"Value\", \"version_updated_by\") VALUES ($1, $2, '" + PostgreSQLDBEngine.GetComputerName(conn) + "');");

                        object[] rowValues = (row["Value"] as object[]);
                        object[] rowTypes = (row["OriginatingTypeNames"] as object[]);
                        List<string> valueStrings = new List<string>();

                        for (int i = 0; i < rowValues.Length; i++)
                        {
                            string valString = rowValues[i].ToString();
                            string typeString = rowTypes[i].ToString();

                            string thisVal = valString + ":" + typeString;
                            valueStrings.Add(thisVal);
                        }

                        //Under the new collection format, we must ALWAYS compare, in order to know what values are already there.
                        //Overwriting the "collection" is no longer really viable, all methods must be an add or delete.

                        DataSet ds = new DataSet();
                        DataTable dt = new DataTable();
                        NpgsqlDataAdapter da;
                        string readRowSQL = "SELECT guid, \"Value\" FROM " + tableName + " WHERE guid = '" + row.RowIdentifier + "'";
                        da = new NpgsqlDataAdapter(readRowSQL, conn);
                        ds.Reset();
                        da.Fill(ds);
                        dt = ds.Tables[0];

                        int valueIndex = dt.Columns.IndexOf("Value");

                        //If we find a value already present in the db that is listed in valueStrings, remove it, as there is no concept of "updating"
                        //a list member.
                        foreach (DataRow dataRow in dt.Rows)
                        {
                            string value = dataRow.ItemArray[valueIndex].ToString();
                            if (valueStrings.Contains(value))
                            {
                                valueStrings.Remove(value);
                            }
                        }


                        for (int i = 0; i < valueStrings.Count; i++)
                        {
                            string valString = valueStrings[i];

                            collectionInsertSQL.Append(" EXECUTE collectionInsert('" + row.RowIdentifier.ToString() + "', '" + valString + "');");
                        }

                        ////Empty row!
                        //if (dt.Rows.Count == 0 && valueStrings.Count == 0)
                        //{
                        //    collectionInsertSQL.Append(" EXECUTE collectionInsert('" + row.RowIdentifier.ToString() + "', null);");
                        //    collectionInserts = 1;
                        //}
                        //else
                        //{
                            collectionInserts = valueStrings.Count;
                        //}

                        collectionInsertSQL.Append(" DEALLOCATE collectionInsert;");
                    }
                    else
                        throw new Exception("Unrecognized type in helper class.");
                }
                else
                {

                    for (int i = 0; i < table.ColumnNames.Count; i++)
                    {
                        string colName = table.ColumnNames[i];
                        Type colType = table.ColumnTypes[i];

                        if (!row.GetKeyNames.Contains(colName))
                            continue;

                        if (colNames.Length == 0)
                        {
                            List<string> colList = createdTables[tableName];
                            foreach (string column in colList)
                            {
                                string colString = column.Split(':')[0];
                                if (colString == colName)
                                {
                                    MesosoftCommon.Utilities.Text.TextUtilities.AddToString("colNameSQL", @", """ + column);
                                    break;
                                }
                            }
                        }

                        for (int j = 0; j < colNames.Length; j++)
                        {
                            string colString = colNames[j].Split(':')[0];
                            if (colString == colName)
                            {
                                MesosoftCommon.Utilities.Text.TextUtilities.AddToString("colNameSQL", @", """ + colNames[j]);
                                break;
                            }
                        }

                        //colNameSQL += @", """ + colName;
                        //   CeresBase.General.Utilities.AddToString("colNameSQL", @", """ + colName);


                        //   //We have to append the type identifier to uuid column names for them to be picked up properly.
                        //   //Object types are stored with the special string format.
                        //   //if (PostgreSQLDBEngine.GetDBTypeName(colType) == "uuid" && colType != typeof(object))
                        //   //{
                        //       if (colNames.Length == 0 || (colNames != null && colNames.Length > i + 1 && colNames[i + 1].Contains(':')))
                        //       {
                        //           //colNameSQL += ":";
                        //           CeresBase.General.Utilities.AddToString("colNameSQL", ":");

                        //           // colNameSQL += PostgreSQLDBEngine.LongNameToDBTableID(PostgreSQLDBEngine.GetTableName(colType), conn);
                        //           CeresBase.General.Utilities.AddToString("colNameSQL", PostgreSQLDBEngine.LongNameToDBTableID(PostgreSQLDBEngine.GetTableName(colType), conn).ToString());
                        //       }
                        ////   }
                        // colNameSQL += @"""";
                        MesosoftCommon.Utilities.Text.TextUtilities.AddToString("colNameSQL", @"""");


                        //valueSQL += ", ";
                        MesosoftCommon.Utilities.Text.TextUtilities.AddToString("valueSQL", ", ");


                        if (colType == typeof(object))
                        {
                            //valueSQL += "'";
                            MesosoftCommon.Utilities.Text.TextUtilities.AddToString("valueSQL", "'");


                            if ((row[colName].ToString()).Contains(':'))
                            {
                                //valueSQL += row[colName];
                                MesosoftCommon.Utilities.Text.TextUtilities.AddToString("valueSQL", row[colName].ToString());

                            }
                            else
                            {
                                //valueSQL += row[colName] + ":" + row[colName].GetType().FullName;
                                MesosoftCommon.Utilities.Text.TextUtilities.AddToString("valueSQL", row[colName] + ":" + row[colName].GetType().FullName);

                            }
                            //valueSQL += "'";
                            MesosoftCommon.Utilities.Text.TextUtilities.AddToString("valueSQL", "'");

                        }
                        else
                        {
                            if (colType == typeof(string) || colType == typeof(char) || PostgreSQLDBEngine.GetDBTypeName(colType) == "uuid")
                            {
                                //valueSQL += "'";
                                MesosoftCommon.Utilities.Text.TextUtilities.AddToString("valueSQL", "'");

                            }

                            //                            valueSQL += row[colName];
                            MesosoftCommon.Utilities.Text.TextUtilities.AddToString("valueSQL", row[colName].ToString());


                            if (colType == typeof(string) || colType == typeof(char) || PostgreSQLDBEngine.GetDBTypeName(colType) == "uuid")
                            {
                                //valueSQL += "'";
                                MesosoftCommon.Utilities.Text.TextUtilities.AddToString("valueSQL", "'");

                            }
                        }
                    }
                }
                string rowSQL;

                string colNameSQL = MesosoftCommon.Utilities.Text.TextUtilities.GetCreatedString("colNameSQL");
                string valueSQL = MesosoftCommon.Utilities.Text.TextUtilities.GetCreatedString("valueSQL");

                string computerName = PostgreSQLDBEngine.GetComputerName(conn);

                if (computerName == null)
                    computerName = "unidentified computer";

                if (deleteObjects)
                {
                    throw new Exception("Can't delete");
                    rowSQL = @"DELETE FROM """ + tableName + @""" WHERE guid = '" + row.RowIdentifier + "';";
                }
                else if (update)
                {
                    if (tableIsHelper)
                    {
                        rowSQL = "";
                        if (collectionInserts > 0)
                            rowSQL += collectionInsertSQL.ToString();
                        if (collectionUpdates > 0)
                            rowSQL += collectionUpdateSQL.ToString();
                    }
                    else
                        rowSQL = @"UPDATE """ + tableName + @""" SET (" + colNameSQL + ", \"row_version\", \"version_updated_by\"" + ") = (" + valueSQL + ", increment(\"row_version\"), '" + computerName + "') WHERE guid = '" + row.RowIdentifier + "';";
                }
                else
                {
                    if (tableIsHelper)
                    {
                        rowSQL = "";
                        if (collectionInserts > 0)
                            rowSQL += collectionInsertSQL.ToString();
                        if (collectionUpdates > 0)
                            rowSQL += collectionUpdateSQL.ToString();
                    }
                    else
                        rowSQL = @"INSERT INTO """ + tableName + @""" (" + colNameSQL + ", \"version_updated_by\"" + ") VALUES (" + valueSQL + ", '" + computerName + "');";
                }

                #endregion

                tInsertOrUpdateRows = tInsertOrUpdateRows.Add(DateTime.Now.Subtract(now));



                NpgsqlCommand command = new NpgsqlCommand(rowSQL, conn);

                DateTime now2 = DateTime.Now;



                int rowsAffected = 0;
                if (rowSQL != "")
                {

                    rowsAffected = command.ExecuteNonQuery();
                    if (rowsAffected == 0)
                        throw new Exception("Failed to insert/update row.");

                    //Obviously on delete it is no longer serialized.
                    if (deleteObjects)
                        SerializedObjectManager.MarkUIDNonSerialized(row.RowIdentifier);
                    else
                        SerializedObjectManager.MarkUIDSerialized(row.RowIdentifier);
                }

                tInsertOrUpdateRows1 = tInsertOrUpdateRows1.Add(DateTime.Now.Subtract(now2));
                tEntries++;
            }


        }

        public static Type[] GetDBSubClasses(string nameToCheck, NpgsqlConnection conn)
        {
            int jk = 11;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql = "SELECT * FROM extended_tables WHERE baseclass = '" + nameToCheck + "';";
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();

            da.Fill(ds);

            dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
                return null;

            List<Type> typesList = new List<Type>();

            foreach (DataRow row in dt.Rows)
            {
                typesList.Add(MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(row.ItemArray[1] as string));
            }


            return typesList.ToArray();
        }

        public static TimeSpan trReadDataIntoTables = new TimeSpan();
        public static TimeSpan trReadDataIntoTables_SQL = new TimeSpan();

        public static int ReadDataIntoTables(ref List<RelationalTable> tables, RelationalFilter filter, NpgsqlConnection conn, NpgsqlTransaction transaction, Type realTableType)
        {
            DateTime time = DateTime.Now;

            //Count of how many tables were read
            int ret = 0;
            string targetClassName;

            if (filter.TargetHelperClassName != null)
            {
                string tableFilter = filter.TargetHelperClassName;
                if (!PostgreSQLDBEngine.TableExists(tableFilter, conn))
                {
                    //bool found = false;
                    //Type realType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(tableFilter);
                    //if (realType != null)
                    //{
                    //    string realTable = GetTableName(realType);
                    //    found = PostgreSQLDBEngine.TableExists(realTable, conn);
                    //    if (found)
                    //    {
                    //        tableFilter  = realTable;
                    //    }
                    //}

                    //if (!found)
                    //{
                    //If we're looking for a specific UID, but the table isn't found, it means the UID is in a subclass.
                    if (filter.UUID.Count > 0)
                    {
                        Type[] types = GetDBSubClasses(filter.TargetHelperClassName, conn);
                        if (types != null)
                        {
                            foreach (Type subType in types)
                            {
                                RelationalFilter newFilter = new RelationalFilter(subType, filter.UUID[0]);
                                PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, null);
                            }
                        }
                    }
                    return ret;
                    //}
                }


                targetClassName = PostgreSQLDBEngine.LongNameToDBTableName(tableFilter, conn);
            }
            else
            {
                string tableFilter = filter.TargetClass.FullName;
                if (!PostgreSQLDBEngine.TableExists(tableFilter, conn))
                {
                    //bool found = false;
                    //Type realType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(tableFilter);
                    //if (realType != null)
                    //{
                    //    string realTable = GetTableName(realType);
                    //    found = PostgreSQLDBEngine.TableExists(realTable, conn);
                    //    if (found)
                    //    {
                    //        tableFilter = realTable;
                    //        filter = new RelationalFilter(realTable, filter.UUID);
                    //    }
                    //}

                    //if (!found)
                    //{
                    //If we're looking for a specific UID, but the table isn't found, it means the UID is in a subclass.
                    if (filter.UUID.Count > 0)
                    {
                        Type[] types = GetDBSubClasses(tableFilter, conn);

                        if (types != null)
                        {
                            foreach (Type subType in types)
                            {
                                RelationalFilter newFilter = new RelationalFilter(subType, filter.UUID[0]);
                                PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, null);
                            }
                        }
                    }
                    return ret;
                    //}
                }
                targetClassName = PostgreSQLDBEngine.LongNameToDBTableName(tableFilter, conn);
            }
            string whereClause = PostgreSQLDBEngine.translateFilterToSQL(filter);

            DateTime SQLtime = DateTime.Now;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql = "SELECT * FROM " + targetClassName + " " + whereClause;
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();

            da.Fill(ds);

            dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
            {
                string checkString;
                if (filter.TargetHelperClassName != null)
                    checkString = filter.TargetHelperClassName;
                else
                    checkString = filter.TargetClass.FullName;
                //If the row isn't found, it's possible that it's hidden within a subclass.
                Type[] types = GetDBSubClasses(checkString, conn);

                if (types != null)
                {
                    foreach (Type subType in types)
                    {
                        RelationalFilter newFilter = new RelationalFilter(subType, filter.UUID[0]);
                        PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, null);
                    }
                }

                return ret;
            }
            trReadDataIntoTables_SQL = trReadDataIntoTables_SQL.Add(DateTime.Now.Subtract(SQLtime));

            RelationalTable retTable = null;

            //See if a relational table with the class already exists in the table stack.
            if (filter.TargetHelperClassName != null)
            {
                foreach (RelationalTable checkTable in tables)
                {
                    if (checkTable.TableType.FullName == (typeof(RelationalFormatterHelper)).FullName)
                    {
                        retTable = checkTable;
                        break;
                    }
                    //if (checkTable.TableType.FullName == filter.TargetHelperClassName)
                    //    retTable = checkTable;
                }

                if (retTable == null)
                {
                    retTable = new RelationalTable();
                    retTable.TableType = typeof(RelationalFormatterHelper);
                    tables.Add(retTable);
                }
            }
            else
            {
                foreach (RelationalTable checkTable in tables)
                {
                    if (checkTable.TableType == filter.TargetClass)
                    {
                        retTable = checkTable;
                        break;
                    }

                }

                if (retTable == null)
                {
                    retTable = new RelationalTable();
                    retTable.TableType = filter.TargetClass;
                    tables.Add(retTable);
                }
            }



            ret = dt.Rows.Count;
            foreach (DataRow row in dt.Rows)
            {
                object[] data = row.ItemArray;
                RelationalRow newRow = new RelationalRow();

                for (int i = 0; i < data.Length; i++)
                {
                    object datum = data[i];

                    string colName = dt.Columns[i].Caption;

                    Type tableTypeToPass = null;

                    //Hackiest hack that ever hacked
                    if (!colName.Contains(':') && datum.ToString().Contains(':'))
                    {
                        string datStr = datum.ToString();
                        string datType = datStr.Substring(datStr.IndexOf(':') + 1);
                        Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(datType);

                        if (tableType != null)
                        {
                            string realTableName = GetTableName(tableType);
                            if (realTableName != null)
                            {
                                if (realTableName.Contains("IList") || realTableName.Contains("IDictionary"))
                                {
                                    int id = LongNameToDBTableID(realTableName, conn);
                                    if (id != -1)
                                    {
                                        colName = colName + ":" + id;
                                        datum = new Guid(datStr.Substring(0, datStr.IndexOf(':')));
                                        tableTypeToPass = tableType;

                                    }

                                }
                            }
                        }
                    }

                    //The guid column denotes the row identifier.
                    if (colName == "guid")
                    {
                        newRow.RowIdentifier = (Guid)datum;
                        //Check for cyclical references
                        //foreach (RelationalRow thisRow in retTable.Rows)
                        //{
                        //    if (thisRow.RowIdentifier == newRow.RowIdentifier)
                        //    {
                        //        newRow = null;
                        //        break;
                        //    }
                        //}
                        //if (newRow == null)
                        //    break;

                        if (retTable.Rows.ContainsKey(newRow.RowIdentifier))
                        {
                            newRow = null;
                            break;
                        }

                        //retTable.Rows.Add(newRow);
                        continue;
                    }

                    //In the case of a column with a numerical identifier, tableTypeString will hold the "original" type name.
                    string tableTypeString = "";

                    //If it has a type identifier appended, recursion is necessary to pull the relevant tables out
                    if (colName.Contains(':'))
                    {
                        string idNumber = colName.Split(':')[1];

                        SQLtime = DateTime.Now;

                        tableTypeString = PostgreSQLDBEngine.TableNameFromID(idNumber, conn);

                        trReadDataIntoTables_SQL = trReadDataIntoTables_SQL.Add(DateTime.Now.Subtract(SQLtime));

                        List<Guid> idSet = new List<Guid>();
                        if (datum.GetType().IsArray)
                        {
                            Guid[] idArray = (Guid[])datum;
                            foreach (Guid id in idArray)
                            {
                                idSet.Add(id);
                            }
                        }
                        else
                        {
                            if (datum.GetType() != typeof(DBNull))
                                idSet.Add((Guid)datum);
                        }

                        foreach (Guid thisId in idSet)
                        {
                            //We've gotten the type string, now segregate the special types (dictionary and list)
                            RelationalFilter newFilter;
                            if (tableTypeString.Contains(':'))
                            {
                                //These types are implied, don't attempt to get the type directly from them.
                                newFilter = new RelationalFilter(tableTypeString, thisId);
                            }
                            else
                            {
                                Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(tableTypeString);
                                newFilter = new RelationalFilter(tableType, thisId);
                            }
                            PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, tableTypeToPass);
                        }
                    }


                    if (retTable.TableType == typeof(RelationalFormatterHelper))
                    {
                        if (realTableType != null)
                        {
                            newRow["ObjectRealType"] = realTableType;
                        }

                        if (filter.TargetHelperClassName.Split(':')[0] == "IDictionary")
                        {
                            //Handle object dictionaries seperately
                            string keyTypeName = filter.TargetHelperClassName.Split(':')[1];
                            Type keyTypeFromName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(keyTypeName);

                            string valTypeName = filter.TargetHelperClassName.Split(':')[2];
                            Type valTypeFromName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(valTypeName);

                            if (tableTypeString == "" || filter.TargetHelperClassName.Split(':')[1] == "System.Object" || filter.TargetHelperClassName.Split(':')[2] == "System.Object"
                                || PostgreSQLDBEngine.GetDBTypeName(keyTypeFromName) == "uuid" || PostgreSQLDBEngine.GetDBTypeName(valTypeFromName) == "uuid")
                            {
                                //Only do setup on the first pass
                                if (i == 1)
                                {
                                    newRow["PropertyName"] = "DictionaryItems";
                                    newRow["Value"] = new object[2];
                                    newRow["OriginatingTypeNames"] = new string[(data[i] as string[]).Length];
                                    //Init the strings to empty so we can add to them
                                    for (int k = 0; k < (data[i] as string[]).Length; k++)
                                    {
                                        (newRow["OriginatingTypeNames"] as string[])[k] = "";
                                    }
                                }

                                string[] strList = data[i] as string[];
                                object[] valList = new object[strList.Length];
                                string[] typeList = new string[strList.Length];

                                for (int j = 0; j < strList.Length; j++)
                                {
                                    string str = strList[j];
                                    string[] split = str.Split(':');

                                    typeList[j] = split[1];

                                    Type toParse = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeList[j]);
                                    //it's already in string form, no need to parse.
                                    if (toParse == typeof(string))
                                        valList[j] = split[0];
                                    else if (toParse == typeof(Guid) || PostgreSQLDBEngine.GetDBTypeName(toParse) == "uuid")
                                    {
                                        valList[j] = new Guid(split[0]);


                                        //Since object columns do not have a type numerical suffix, the relevant table must be rooted out here.
                                        RelationalFilter newFilter;

                                        string realTable = GetTableName(toParse);

                                        //segregate dictionary/list since they have implied types.
                                        if (realTable.Contains(':'))
                                            newFilter = new RelationalFilter(realTable, new Guid(split[0]));
                                        else
                                        {
                                            Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);
                                            newFilter = new RelationalFilter(tableType, new Guid(split[0]));
                                        }
                                        PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, null);
                                    }
                                    else
                                    {
                                        MethodInfo mi = toParse.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                                        valList[j] = mi.Invoke(null, new object[] { split[0] });
                                    }
                                }

                                (newRow["Value"] as object[])[i - 1] = valList;

                                for (int k = 0; k < (data[i] as string[]).Length; k++)
                                {
                                    if ((newRow["OriginatingTypeNames"] as string[])[k] != "")
                                        (newRow["OriginatingTypeNames"] as string[])[k] += ":";
                                    (newRow["OriginatingTypeNames"] as string[])[k] += typeList[k];
                                }
                            }
                            else
                            {
                                //Dictionaries have 3 data entries, first being guid.  This will get called twice so
                                //it's important to only assign values to keys once, and to keep track of the typenames correctly.
                                if (i == 1)
                                {
                                    newRow["PropertyName"] = "DictionaryItems";
                                    newRow["Value"] = new object[2];
                                    newRow["OriginatingTypeNames"] = new string[2];
                                }

                                (newRow["Value"] as object[])[i - 1] = data[i];
                                if (tableTypeString != "")
                                    (newRow["OriginatingTypeNames"] as string[])[i - 1] = tableTypeString;
                                else
                                    (newRow["OriginatingTypeNames"] as string[])[i - 1] = (data[i] as Array).GetValue(0).GetType().FullName;
                            }
                        }
                        else if (filter.TargetHelperClassName.Split(':')[0] == "IList")
                        {
                            //Lists only have two data entries, and one is the guid.  So this will only get called once.
                            newRow["PropertyName"] = "ListItems";


                            //Handle object lists seperately

                            string typeName = filter.TargetHelperClassName.Split(':')[1];
                            Type typeFromName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeName);

                            if (tableTypeString == "" || filter.TargetHelperClassName.Split(':')[1] == "System.Object" || PostgreSQLDBEngine.GetDBTypeName(typeFromName) == "uuid")
                            {
                                string[] strList = data[1] as string[];
                                object[] valList = new object[strList.Length];
                                string[] typeList = new string[strList.Length];

                                for (int j = 0; j < strList.Length; j++)
                                {
                                    string str = strList[j];
                                    string[] split = str.Split(':');

                                    typeList[j] = split[1];

                                    Type toParse = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeList[j]);
                                    //it's already in string form, no need to parse.
                                    if (toParse == typeof(string))
                                        valList[j] = split[0];
                                    else if (toParse == typeof(Guid) || PostgreSQLDBEngine.GetDBTypeName(toParse) == "uuid")
                                    {
                                        valList[j] = new Guid(split[0]);

                                        //Since object columns do not have a type numerical suffix, the relevant table must be rooted out here.
                                        RelationalFilter newFilter = null;

                                        string realTable = GetTableName(toParse);

                                        //segregate dictionary/list since they have implied types.
                                        if (realTable.Contains(':'))
                                        {
                                            newFilter = new RelationalFilter(realTable, new Guid(split[0]));
                                        }
                                        else
                                        {
                                            Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);
                                            newFilter = new RelationalFilter(tableType, new Guid(split[0]));
                                        }
                                        PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, null);
                                    }
                                    else
                                    {
                                        MethodInfo mi = toParse.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                                        valList[j] = mi.Invoke(null, new object[] { split[0] });
                                    }
                                }

                                newRow["Value"] = valList;
                                newRow["OriginatingTypeNames"] = typeList;
                            }
                            else
                            {
                                newRow["Value"] = data[1];
                                string[] newOrigTypeNames = new string[1];
                                newOrigTypeNames[0] = tableTypeString;
                                if (tableTypeString != "")
                                    newRow["OriginatingTypeNames"] = newOrigTypeNames;
                                else
                                    newRow["OriginatingTypeNames"] = (data[i] as Array).GetValue(0).GetType().FullName;
                            }
                        }
                        else
                        {
                            throw new Exception("Unrecognized helper type during deserialization.");
                        }
                    }
                    else
                    {
                        //Handle object typing here.
                        if (retTable.GetTypeFromColumnName(colName) == typeof(object))
                        {
                            if (datum is string)
                            {
                                string[] split = (datum as string).Split(':');

                                Type toParse = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);

                                if (toParse == typeof(string))
                                    newRow[colName] = split[0];
                                else if (toParse == typeof(Guid) || PostgreSQLDBEngine.GetDBTypeName(toParse) == "uuid")
                                {
                                    newRow[colName] = new Guid(split[0]);
                                    //Since object columns do not have a type numerical suffix, the relevant table must be rooted out here.
                                    RelationalFilter newFilter;
                                    //segregate dictionary/list since they have implied types.
                                    if (split[1].Contains(':'))
                                        newFilter = new RelationalFilter(split[1], new Guid(split[0]));
                                    else
                                    {
                                        Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);
                                        newFilter = new RelationalFilter(tableType, new Guid(split[0]));
                                    }
                                    PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, null);
                                }
                                else
                                {
                                    MethodInfo mi = toParse.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                                    newRow[colName] = mi.Invoke(null, new object[] { split[0] });
                                }
                            }
                        }
                        else
                        {
                            //Strip type identifiers from the column names before indexing them in the row

                            //For some reason character types are being returned as strings.  So to prevent type issues further on,
                            //single-character strings are converted to char types here.
                            if (datum.GetType() == typeof(string) && (datum as string).Length == 1)
                                newRow[colName.Split(':')[0]] = (datum as string)[0];
                            else
                                newRow[colName.Split(':')[0]] = datum;
                        }
                    }
                }

                if (newRow != null)
                    retTable.Rows.Add(newRow.RowIdentifier, newRow);
            }

            trReadDataIntoTables = trReadDataIntoTables.Add(DateTime.Now.Subtract(time));

            return ret;
        }

        private static string translateFilterToSQL(RelationalFilter filter)
        {
            string ret = "WHERE ";
            if (filter.UUID.Count > 0)
            {
                if (filter.UUID.Count == 1)
                    ret += "guid = '" + filter.UUID[0] + "'";
                else
                {
                    bool needsAnd = false;
                    foreach (Guid uid in filter.UUID)
                    {
                        if (needsAnd)
                            ret += " OR ";
                        ret += "guid = '" + uid + "'";
                        needsAnd = true;
                    }
                }
            }
            else
            {
                bool needsAnd = false;
                foreach (Condition cond in filter.Conditions)
                {
                    if (needsAnd)
                        ret += " AND ";
                    //PropertyName
                    ret += "\"" + cond.PropertyName + "\" ";
                    //Operator
                    switch (cond.Operator)
                    {
                        case Comparison.Equal:
                            ret += "= ";
                            break;
                        case Comparison.LessThan:
                            ret += "< ";
                            break;
                        case Comparison.GreaterThan:
                            ret += "> ";
                            break;
                        case Comparison.LessThanEqual:
                            ret += "<= ";
                            break;
                        case Comparison.GreaterThanEqual:
                            ret += ">= ";
                            break;
                        case Comparison.NotEqual:
                            ret += "!= ";
                            break;
                        default:
                            break;
                    }
                    //Value
                    Type condType = cond.Value.GetType();

                    if (condType == typeof(string) || condType == typeof(char) || condType == typeof(Guid))
                        ret += "'";
                    ret += cond.Value.ToString();
                    if (condType == typeof(string) || condType == typeof(char) || condType == typeof(Guid))
                        ret += "'";

                    needsAnd = true;
                }
            }
            return ret;
        }

        public static TimeSpan trFASTReadDataIntoTables = new TimeSpan();
        public static TimeSpan trFASTReadDataIntoTables_SQL = new TimeSpan();

        public static int ReadDataIntoTablesQuickly(ref List<RelationalTable> tables, RelationalFilter filter, NpgsqlConnection conn, NpgsqlTransaction transaction, Type realTableType)
        {
            DateTime time = DateTime.Now;

            //Count of how many tables were read
            int ret = 0;
            string targetClassName;

            if (filter.TargetHelperClassName != null)
            {
                string tableFilter = filter.TargetHelperClassName;
                if (!PostgreSQLDBEngine.TableExists(tableFilter, conn))
                {
                    //If we're looking for a specific UID, but the table isn't found, it means the UID is in a subclass.
                    if (filter.UUID.Count > 0)
                    {
                        Type[] types = GetDBSubClasses(filter.TargetHelperClassName, conn);
                        if (types != null)
                        {
                            foreach (Type subType in types)
                            {
                                RelationalFilter newFilter = new RelationalFilter(subType, filter.UUID[0]);
                                PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, null);
                            }
                        }
                    }
                    return ret;
                }


                targetClassName = PostgreSQLDBEngine.LongNameToDBTableName(tableFilter, conn);
            }
            else
            {
                string tableFilter = filter.TargetClass.FullName;
                if (!PostgreSQLDBEngine.TableExists(tableFilter, conn))
                {
                    //If we're looking for a specific UID, but the table isn't found, it means the UID is in a subclass.
                    if (filter.UUID.Count > 0)
                    {
                        Type[] types = GetDBSubClasses(tableFilter, conn);

                        if (types != null)
                        {
                            foreach (Type subType in types)
                            {
                                RelationalFilter newFilter = new RelationalFilter(subType, filter.UUID[0]);
                                PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, null);
                            }
                        }
                    }
                    return ret;
                }
                targetClassName = PostgreSQLDBEngine.LongNameToDBTableName(tableFilter, conn);
            }
            string whereClause = PostgreSQLDBEngine.translateFilterToSQL(filter);

            DateTime SQLtime = DateTime.Now;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql = "SELECT * FROM " + targetClassName + " " + whereClause;
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();

            da.Fill(ds);

            dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
                return ret;
            trFASTReadDataIntoTables_SQL = trFASTReadDataIntoTables_SQL.Add(DateTime.Now.Subtract(SQLtime));

            RelationalTable retTable = null;

            //See if a relational table with the class already exists in the table stack.
            if (filter.TargetHelperClassName != null)
            {
                foreach (RelationalTable checkTable in tables)
                {
                    if (checkTable.TableType.FullName == (typeof(RelationalFormatterHelper)).FullName)
                    {
                        retTable = checkTable;
                        break;
                    }
                    //if (checkTable.TableType.FullName == filter.TargetHelperClassName)
                    //    retTable = checkTable;
                }

                if (retTable == null)
                {
                    retTable = new RelationalTable();
                    retTable.TableType = typeof(RelationalFormatterHelper);
                    tables.Add(retTable);
                }
            }
            else
            {
                foreach (RelationalTable checkTable in tables)
                {
                    if (checkTable.TableType == filter.TargetClass)
                    {
                        retTable = checkTable;
                        break;
                    }

                }

                if (retTable == null)
                {
                    retTable = new RelationalTable();
                    retTable.TableType = filter.TargetClass;
                    tables.Add(retTable);
                }
            }



            ret = dt.Rows.Count;
            foreach (DataRow row in dt.Rows)
            {
                object[] data = row.ItemArray;
                RelationalRow newRow = new RelationalRow();

                for (int i = 0; i < data.Length; i++)
                {
                    object datum = data[i];

                    string colName = dt.Columns[i].Caption;

                    Type tableTypeToPass = null;

                    //Hackiest hack that ever hacked
                    if (!colName.Contains(':') && datum.ToString().Contains(':'))
                    {
                        string datStr = datum.ToString();
                        string datType = datStr.Substring(datStr.IndexOf(':') + 1);
                        Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(datType);

                        if (tableType != null)
                        {
                            string realTableName = GetTableName(tableType);
                            if (realTableName != null)
                            {
                                if (realTableName.Contains("IList") || realTableName.Contains("IDictionary"))
                                {
                                    int id = LongNameToDBTableID(realTableName, conn);
                                    if (id != -1)
                                    {
                                        colName = colName + ":" + id;
                                        datum = new Guid(datStr.Substring(0, datStr.IndexOf(':')));
                                        tableTypeToPass = tableType;

                                    }

                                }
                            }
                        }
                    }

                    //The guid column denotes the row identifier.
                    if (colName == "guid")
                    {
                        newRow.RowIdentifier = (Guid)datum;
                        //Check for cyclical references
                        //foreach (RelationalRow thisRow in retTable.Rows)
                        //{
                        //    if (thisRow.RowIdentifier == newRow.RowIdentifier)
                        //    {
                        //        newRow = null;
                        //        break;
                        //    }
                        //}
                        //if (newRow == null)
                        //    break;

                        if (retTable.Rows.ContainsKey(newRow.RowIdentifier))
                        {
                            newRow = null;
                            break;
                        }

                        //retTable.Rows.Add(newRow);
                        continue;
                    }

                    //In the case of a column with a numerical identifier, tableTypeString will hold the "original" type name.
                    string tableTypeString = "";

                    //If it has a type identifier appended, recursion is necessary to pull the relevant tables out
                    if (colName.Contains(':'))
                    {
                        string idNumber = colName.Split(':')[1];

                        SQLtime = DateTime.Now;

                        tableTypeString = PostgreSQLDBEngine.TableNameFromID(idNumber, conn);

                        trFASTReadDataIntoTables_SQL = trFASTReadDataIntoTables_SQL.Add(DateTime.Now.Subtract(SQLtime));

                        List<Guid> idSet = new List<Guid>();
                        if (datum.GetType().IsArray)
                        {
                            Guid[] idArray = (Guid[])datum;
                            foreach (Guid id in idArray)
                            {
                                idSet.Add(id);
                            }
                        }
                        else
                        {
                            if (datum.GetType() != typeof(DBNull))
                                idSet.Add((Guid)datum);
                        }

                        foreach (Guid thisId in idSet)
                        {
                            //We've gotten the type string, now segregate the special types (dictionary and list)
                            RelationalFilter newFilter;
                            if (tableTypeString.Contains(':'))
                            {
                                //These types are implied, don't attempt to get the type directly from them.
                                newFilter = new RelationalFilter(tableTypeString, thisId);
                            }
                            else
                            {
                                Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(tableTypeString);
                                newFilter = new RelationalFilter(tableType, thisId);
                            }
                            PostgreSQLDBEngine.ReadDataIntoTablesQuickly(ref tables, newFilter, conn, transaction, tableTypeToPass);
                        }
                    }


                    if (retTable.TableType == typeof(RelationalFormatterHelper))
                    {
                        if (realTableType != null)
                        {
                            newRow["ObjectRealType"] = realTableType;
                        }

                        if (filter.TargetHelperClassName.Split(':')[0] == "IDictionary")
                        {
                            //Handle object dictionaries seperately
                            string keyTypeName = filter.TargetHelperClassName.Split(':')[1];
                            Type keyTypeFromName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(keyTypeName);

                            string valTypeName = filter.TargetHelperClassName.Split(':')[2];
                            Type valTypeFromName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(valTypeName);

                            if (tableTypeString == "" || filter.TargetHelperClassName.Split(':')[1] == "System.Object" || filter.TargetHelperClassName.Split(':')[2] == "System.Object"
                                || PostgreSQLDBEngine.GetDBTypeName(keyTypeFromName) == "uuid" || PostgreSQLDBEngine.GetDBTypeName(valTypeFromName) == "uuid")
                            {
                                //Only do setup on the first pass
                                if (i == 1)
                                {
                                    newRow["PropertyName"] = "DictionaryItems";
                                    newRow["Value"] = new object[2];
                                    newRow["OriginatingTypeNames"] = new string[(data[i] as string[]).Length];
                                    //Init the strings to empty so we can add to them
                                    for (int k = 0; k < (data[i] as string[]).Length; k++)
                                    {
                                        (newRow["OriginatingTypeNames"] as string[])[k] = "";
                                    }
                                }

                                string[] strList = data[i] as string[];
                                object[] valList = new object[strList.Length];
                                string[] typeList = new string[strList.Length];

                                for (int j = 0; j < strList.Length; j++)
                                {
                                    string str = strList[j];
                                    string[] split = str.Split(':');

                                    typeList[j] = split[1];

                                    Type toParse = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeList[j]);
                                    //it's already in string form, no need to parse.
                                    if (toParse == typeof(string))
                                        valList[j] = split[0];
                                    else if (toParse == typeof(Guid) || PostgreSQLDBEngine.GetDBTypeName(toParse) == "uuid")
                                    {
                                        valList[j] = new Guid(split[0]);

                                        //Since object columns do not have a type numerical suffix, the relevant table must be rooted out here.
                                        RelationalFilter newFilter;

                                        string realTable = GetTableName(toParse);

                                        //segregate dictionary/list since they have implied types.
                                        if (realTable.Contains(':'))
                                            newFilter = new RelationalFilter(realTable, new Guid(split[0]));
                                        else
                                        {
                                            Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);
                                            newFilter = new RelationalFilter(tableType, new Guid(split[0]));
                                        }
                                        PostgreSQLDBEngine.ReadDataIntoTablesQuickly(ref tables, newFilter, conn, transaction, null);
                                    }
                                    else
                                    {
                                        MethodInfo mi = toParse.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                                        valList[j] = mi.Invoke(null, new object[] { split[0] });
                                    }
                                }

                                (newRow["Value"] as object[])[i - 1] = valList;

                                for (int k = 0; k < (data[i] as string[]).Length; k++)
                                {
                                    if ((newRow["OriginatingTypeNames"] as string[])[k] != "")
                                        (newRow["OriginatingTypeNames"] as string[])[k] += ":";
                                    (newRow["OriginatingTypeNames"] as string[])[k] += typeList[k];
                                }
                            }
                            else
                            {
                                //Dictionaries have 3 data entries, first being guid.  This will get called twice so
                                //it's important to only assign values to keys once, and to keep track of the typenames correctly.
                                if (i == 1)
                                {
                                    newRow["PropertyName"] = "DictionaryItems";
                                    newRow["Value"] = new object[2];
                                    newRow["OriginatingTypeNames"] = new string[2];
                                }

                                (newRow["Value"] as object[])[i - 1] = data[i];
                                if (tableTypeString != "")
                                    (newRow["OriginatingTypeNames"] as string[])[i - 1] = tableTypeString;
                                else
                                    (newRow["OriginatingTypeNames"] as string[])[i - 1] = (data[i] as Array).GetValue(0).GetType().FullName;
                            }
                        }
                        else if (filter.TargetHelperClassName.Split(':')[0] == "IList")
                        {
                            //Lists only have two data entries, and one is the guid.  So this will only get called once.
                            newRow["PropertyName"] = "ListItems";


                            //Handle object lists seperately

                            string typeName = filter.TargetHelperClassName.Split(':')[1];
                            Type typeFromName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeName);

                            if (tableTypeString == "" || filter.TargetHelperClassName.Split(':')[1] == "System.Object" || PostgreSQLDBEngine.GetDBTypeName(typeFromName) == "uuid")
                            {
                                Dictionary<Type, List<Guid>> typedList = new Dictionary<Type, List<Guid>>();
                                Dictionary<String, List<Guid>> helperList = new Dictionary<string, List<Guid>>();

                                string[] strList = data[1] as string[];
                                object[] valList = new object[strList.Length];
                                string[] typeList = new string[strList.Length];

                                for (int j = 0; j < strList.Length; j++)
                                {
                                    string str = strList[j];
                                    string[] split = str.Split(':');

                                    typeList[j] = split[1];

                                    Type toParse = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeList[j]);
                                    //it's already in string form, no need to parse.
                                    if (toParse == typeof(string))
                                        valList[j] = split[0];
                                    else if (toParse == typeof(Guid) || PostgreSQLDBEngine.GetDBTypeName(toParse) == "uuid")
                                    {
                                        valList[j] = new Guid(split[0]);

                                        string realTable = GetTableName(toParse);

                                        //Since object columns do not have a type numerical suffix, the relevant table must be rooted out here.
                                        //RelationalFilter newFilter = null;
                                        //segregate dictionary/list since they have implied types.
                                        if (realTable.Contains(':'))
                                        {
                                            //newFilter = new RelationalFilter(split[1], new Guid(split[0]));
                                            if (!helperList.Keys.Contains(realTable))
                                                helperList.Add(realTable, new List<Guid>());
                                            helperList[realTable].Add(new Guid(split[0]));
                                        }
                                        else
                                        {
                                            Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);
                                            //newFilter = new RelationalFilter(tableType, new Guid(split[0]));
                                            if (!typedList.Keys.Contains(tableType))
                                                typedList.Add(tableType, new List<Guid>());
                                            typedList[tableType].Add(new Guid(split[0]));
                                        }
                                        //PostgreSQLDBEngine.ReadDataIntoTablesQuickly(ref tables, newFilter, conn, transaction, null);
                                    }
                                    else
                                    {
                                        MethodInfo mi = toParse.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                                        valList[j] = mi.Invoke(null, new object[] { split[0] });
                                    }
                                }

                                //Do late table adds in-batch here.
                                foreach (Type tabType in typedList.Keys)
                                {
                                    List<Guid> idList = typedList[tabType];

                                    RelationalFilter newFilter = new RelationalFilter(tabType, idList);
                                    PostgreSQLDBEngine.ReadDataIntoTablesQuickly(ref tables, newFilter, conn, transaction, null);
                                }

                                foreach (string tabString in helperList.Keys)
                                {
                                    List<Guid> idList = helperList[tabString];

                                    RelationalFilter newFilter = new RelationalFilter(tabString, idList);
                                    PostgreSQLDBEngine.ReadDataIntoTablesQuickly(ref tables, newFilter, conn, transaction, null);
                                }

                                newRow["Value"] = valList;
                                newRow["OriginatingTypeNames"] = typeList;
                            }
                            else
                            {
                                newRow["Value"] = data[1];
                                string[] newOrigTypeNames = new string[1];
                                newOrigTypeNames[0] = tableTypeString;
                                if (tableTypeString != "")
                                    newRow["OriginatingTypeNames"] = newOrigTypeNames;
                                else
                                    newRow["OriginatingTypeNames"] = (data[i] as Array).GetValue(0).GetType().FullName;
                            }
                        }
                        else
                        {
                            throw new Exception("Unrecognized helper type during deserialization.");
                        }
                    }
                    else
                    {
                        //Handle object typing here.
                        if (retTable.GetTypeFromColumnName(colName) == typeof(object))
                        {
                            if (datum is string)
                            {
                                string[] split = (datum as string).Split(':');

                                Type toParse = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);

                                if (toParse == typeof(string))
                                    newRow[colName] = split[0];
                                else if (toParse == typeof(Guid) || PostgreSQLDBEngine.GetDBTypeName(toParse) == "uuid")
                                {
                                    newRow[colName] = new Guid(split[0]);
                                    //Since object columns do not have a type numerical suffix, the relevant table must be rooted out here.
                                    RelationalFilter newFilter;
                                    //segregate dictionary/list since they have implied types.
                                    if (split[1].Contains(':'))
                                        newFilter = new RelationalFilter(split[1], new Guid(split[0]));
                                    else
                                    {
                                        Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);
                                        newFilter = new RelationalFilter(tableType, new Guid(split[0]));
                                    }
                                    PostgreSQLDBEngine.ReadDataIntoTablesQuickly(ref tables, newFilter, conn, transaction, null);
                                }
                                else
                                {
                                    MethodInfo mi = toParse.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                                    newRow[colName] = mi.Invoke(null, new object[] { split[0] });
                                }
                            }
                        }
                        else
                        {
                            //Strip type identifiers from the column names before indexing them in the row

                            //For some reason character types are being returned as strings.  So to prevent type issues further on,
                            //single-character strings are converted to char types here.
                            if (datum.GetType() == typeof(string) && (datum as string).Length == 1)
                                newRow[colName.Split(':')[0]] = (datum as string)[0];
                            else
                                newRow[colName.Split(':')[0]] = datum;
                        }
                    }
                }

                if (newRow != null)
                    retTable.Rows.Add(newRow.RowIdentifier, newRow);
            }

            trFASTReadDataIntoTables = trFASTReadDataIntoTables.Add(DateTime.Now.Subtract(time));

            return ret;
        }

        public static TimeSpan trFASTERReadDataIntoTables = new TimeSpan();
        public static TimeSpan trFASTERReadDataIntoTables_SQL = new TimeSpan();

        public static int ReadDataIntoTablesQuicker(ref List<RelationalTable> tables, RelationalFilter filter, NpgsqlConnection conn, NpgsqlTransaction transaction, Type realTableType)
        {
            DateTime time = DateTime.Now;

            //Count of how many tables were read
            int ret = 0;
            string targetClassName;

            if (filter.TargetHelperClassName != null)
            {
                string tableFilter = filter.TargetHelperClassName;
                if (!PostgreSQLDBEngine.TableExists(tableFilter, conn))
                {
                    //If we're looking for a specific UID, but the table isn't found, it means the UID is in a subclass.
                    if (filter.UUID.Count > 0)
                    {
                        Type[] types = GetDBSubClasses(filter.TargetHelperClassName, conn);
                        if (types != null)
                        {
                            foreach (Type subType in types)
                            {
                                RelationalFilter newFilter = new RelationalFilter(subType, filter.UUID[0]);
                                PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, null);
                            }
                        }
                    }
                    return ret;
                }


                targetClassName = PostgreSQLDBEngine.LongNameToDBTableName(tableFilter, conn);
            }
            else
            {
                string tableFilter = filter.TargetClass.FullName;
                if (!PostgreSQLDBEngine.TableExists(tableFilter, conn))
                {
                    //If we're looking for a specific UID, but the table isn't found, it means the UID is in a subclass.
                    if (filter.UUID.Count > 0)
                    {
                        Type[] types = GetDBSubClasses(tableFilter, conn);

                        if (types != null)
                        {
                            foreach (Type subType in types)
                            {
                                RelationalFilter newFilter = new RelationalFilter(subType, filter.UUID[0]);
                                PostgreSQLDBEngine.ReadDataIntoTables(ref tables, newFilter, conn, transaction, null);
                            }
                        }
                    }
                    return ret;
                }
                targetClassName = PostgreSQLDBEngine.LongNameToDBTableName(tableFilter, conn);
            }
            string whereClause = PostgreSQLDBEngine.translateFilterToSQL(filter);

            DateTime SQLtime = DateTime.Now;

            string[] colNames = PostgreSQLDBEngine.GetTableColumnNamesFromInternalName(targetClassName, conn, false);

            StringBuilder stringBuild = new StringBuilder("SELECT ", 50);
            bool empty = true;
            foreach (string colName in colNames)
            {
                if (!empty)
                    stringBuild.Append(", ");

                stringBuild.Append("\"");
                stringBuild.Append(colName);
                stringBuild.Append("\"");

                empty = false;
            }



            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql = stringBuild.ToString() + " FROM " + targetClassName + " " + whereClause;
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();

            da.Fill(ds);


            dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
                return ret;
            trFASTERReadDataIntoTables_SQL = trFASTERReadDataIntoTables_SQL.Add(DateTime.Now.Subtract(SQLtime));

            RelationalTable retTable = null;

            //See if a relational table with the class already exists in the table stack.
            if (filter.TargetHelperClassName != null)
            {
                foreach (RelationalTable checkTable in tables)
                {
                    if (checkTable.TableType.FullName == (typeof(RelationalFormatterHelper)).FullName)
                    {
                        retTable = checkTable;
                        break;
                    }
                    //if (checkTable.TableType.FullName == filter.TargetHelperClassName)
                    //    retTable = checkTable;
                }

                if (retTable == null)
                {
                    retTable = new RelationalTable();
                    retTable.TableType = typeof(RelationalFormatterHelper);
                    tables.Add(retTable);
                }
            }
            else
            {
                foreach (RelationalTable checkTable in tables)
                {
                    if (checkTable.TableType == filter.TargetClass)
                    {
                        retTable = checkTable;
                        break;
                    }

                }

                if (retTable == null)
                {
                    retTable = new RelationalTable();
                    retTable.TableType = filter.TargetClass;
                    tables.Add(retTable);
                }
            }



            ret = dt.Rows.Count;

            //Using the new collection methodology, multiples rows will compress to a single returned object.
            //Therefore, we must keep track of existing rows.
            List<RelationalRow> createdRows = new List<RelationalRow>();

            foreach (DataRow row in dt.Rows)
            {
                //List<object> dataList = new List<object>();

                ////Filter special system columns row_version and version_updated_by out of the read.
                //for (int j = 0; j < row.ItemArray.Length; j++)
                //{
                //    object thisItem = row.ItemArray[j];

                //    string itemColName = dt.Columns[j].ColumnName;

                //    if (itemColName != "row_version" && itemColName != "version_updated_by")
                //    {
                //        dataList.Add(thisItem);
                //    }
                //}

                object[] data = row.ItemArray;

                RelationalRow newRow = null;

                bool firstCycle = true;

                //We must first fetch the guid.
                int guidIndex = dt.Columns.IndexOf("guid");
                Guid rowGuid = new Guid(data[guidIndex].ToString());

                if (retTable.TableType == typeof(RelationalFormatterHelper))
                {
                    foreach (RelationalRow existingRow in createdRows)
                    {
                        if (existingRow.RowIdentifier == rowGuid)
                        {
                            newRow = existingRow;
                            firstCycle = false;
                            break;
                        }
                    }
                }

                //If a row wasn't found it'll still be null here and we know it's time to create one.
                if (newRow == null)
                {
                    newRow = new RelationalRow();
                    newRow.RowIdentifier = rowGuid;
                    createdRows.Add(newRow);
                }

                for (int i = 0; i < data.Length; i++)
                {
                    object datum = data[i];

                    string colName = dt.Columns[i].Caption;

                    Type tableTypeToPass = null;

                    //Hackiest hack that ever hacked
                    if (!colName.Contains(':') && datum.ToString().Contains(':'))
                    {
                        string datStr = datum.ToString();
                        string datType = datStr.Substring(datStr.IndexOf(':') + 1);
                        Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(datType);

                        if (tableType != null)
                        {
                            string realTableName = GetTableName(tableType);
                            if (realTableName != null)
                            {
                                if (realTableName.Contains("IList") || realTableName.Contains("IDictionary"))
                                {
                                    int id = LongNameToDBTableID(realTableName, conn);
                                    if (id != -1)
                                    {
                                        colName = colName + ":" + id;
                                        datum = new Guid(datStr.Substring(0, datStr.IndexOf(':')));
                                        tableTypeToPass = tableType;

                                    }

                                }
                            }
                        }
                    }

                    //The guid column denotes the row identifier.
                    if (colName == "guid")
                    {
                        continue;
                    }

                    //if (colName == "row_version" || colName == "version_updated_by")
                    //{
                    //    continue;
                    //}

                    //In the case of a column with a numerical identifier, tableTypeString will hold the "original" type name.
                    string tableTypeString = "";

                    //If it has a type identifier appended, recursion is necessary to pull the relevant tables out
                    if (colName.Contains(':'))
                    {
                        string idNumber = colName.Split(':')[1];

                        SQLtime = DateTime.Now;

                        tableTypeString = PostgreSQLDBEngine.TableNameFromID(idNumber, conn);

                        trFASTERReadDataIntoTables_SQL = trFASTERReadDataIntoTables_SQL.Add(DateTime.Now.Subtract(SQLtime));

                        List<Guid> idSet = new List<Guid>();
                        if (datum.GetType().IsArray)
                        {
                            Guid[] idArray = (Guid[])datum;
                            foreach (Guid id in idArray)
                            {
                                idSet.Add(id);
                            }
                        }
                        else
                        {
                            if (datum.GetType() != typeof(DBNull))
                                idSet.Add((Guid)datum);
                        }

                        foreach (Guid thisId in idSet)
                        {
                            //We've gotten the type string, now segregate the special types (dictionary and list)
                            RelationalFilter newFilter;
                            if (tableTypeString.Contains(':'))
                            {
                                //These types are implied, don't attempt to get the type directly from them.
                                newFilter = new RelationalFilter(tableTypeString, thisId);
                            }
                            else
                            {
                                Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(tableTypeString);
                                newFilter = new RelationalFilter(tableType, thisId);
                            }
                            PostgreSQLDBEngine.ReadDataIntoTablesQuicker(ref tables, newFilter, conn, transaction, tableTypeToPass);
                        }
                    }


                    if (retTable.TableType == typeof(RelationalFormatterHelper))
                    {
                        if (realTableType != null)
                        {
                            newRow["ObjectRealType"] = realTableType;
                        }

                        if (filter.TargetHelperClassName.Split(':')[0] == "IDictionary")
                        {
                            //Handle object dictionaries seperately
                            string keyTypeName = filter.TargetHelperClassName.Split(':')[1];
                            Type keyTypeFromName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(keyTypeName);

                            string valTypeName = filter.TargetHelperClassName.Split(':')[2];
                            Type valTypeFromName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(valTypeName);

                            //if (tableTypeString == "" || filter.TargetHelperClassName.Split(':')[1] == "System.Object" || filter.TargetHelperClassName.Split(':')[2] == "System.Object"
                            //    || PostgreSQLDBEngine.GetDBTypeName(keyTypeFromName) == "uuid" || PostgreSQLDBEngine.GetDBTypeName(valTypeFromName) == "uuid")

                            if (tableTypeString == "" || filter.TargetHelperClassName.Split(':')[1] == "System.Object" || filter.TargetHelperClassName.Split(':')[2] == "System.Object"
                                || PostgreSQLDBEngine.GetDBTypeName(keyTypeFromName) == "uuid" || PostgreSQLDBEngine.GetDBTypeName(valTypeFromName) == "uuid")
                            {
                                //Only do setup on the first pass
                                if (firstCycle)
                                {
                                    newRow["PropertyName"] = "DictionaryItems";
                                    //newRow["Value"] = new object[2];
                                    //newRow["OriginatingTypeNames"] = new string[(data[i] as string[]).Length];
                                    //Init the strings to empty so we can add to them
                                    //for (int k = 0; k < (data[i] as string[]).Length; k++)
                                    //{
                                    //    (newRow["OriginatingTypeNames"] as string[])[k] = "";
                                    //}

                                    firstCycle = false;
                                }

                                Dictionary<Type, List<Guid>> typedList = new Dictionary<Type, List<Guid>>();
                                Dictionary<String, List<Guid>> helperList = new Dictionary<string, List<Guid>>();

                                //string[] strList = null;//data[i] as string[];
                                //object[] valList = null;// new object[strList.Length];
                                //string[] typeList = null;// new string[strList.Length];

                                string stringVal = data[i] as string;
                                string[] split = stringVal.Split(':');
                                string stringType = split[1];
                                object value = null;

                                Type toParse = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(stringType);

                                //it's already in string form, no need to parse.
                                if (toParse == typeof(string))
                                    value = split[0];
                                else if (toParse == typeof(Guid) || PostgreSQLDBEngine.GetDBTypeName(toParse) == "uuid")
                                {
                                    value = new Guid(split[0]);

                                    //Since object columns do not have a type numerical suffix, the relevant table must be rooted out here.


                                    string realTable = GetTableName(toParse);

                                    //segregate dictionary/list since they have implied types.
                                    if (realTable.Contains(':'))
                                    {
                                        //newFilter = new RelationalFilter(realTable, new Guid(split[0]));
                                        if (!helperList.Keys.Contains(realTable))
                                            helperList.Add(realTable, new List<Guid>());
                                        helperList[realTable].Add(new Guid(split[0]));
                                    }
                                    else
                                    {
                                        Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);
                                        //newFilter = new RelationalFilter(tableType, new Guid(split[0]));
                                        if (!typedList.Keys.Contains(tableType))
                                            typedList.Add(tableType, new List<Guid>());
                                        typedList[tableType].Add(new Guid(split[0]));
                                    }
                                }
                                else
                                {
                                    MethodInfo mi = toParse.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                                    value = mi.Invoke(null, new object[] { split[0] });
                                }

                                //Do late table adds in-batch here.
                                foreach (Type tabType in typedList.Keys)
                                {
                                    List<Guid> idList = typedList[tabType];

                                    RelationalFilter newFilter = new RelationalFilter(tabType, idList);
                                    PostgreSQLDBEngine.ReadDataIntoTablesQuicker(ref tables, newFilter, conn, transaction, null);
                                }

                                foreach (string tabString in helperList.Keys)
                                {
                                    List<Guid> idList = helperList[tabString];

                                    RelationalFilter newFilter = new RelationalFilter(tabString, idList);
                                    PostgreSQLDBEngine.ReadDataIntoTablesQuicker(ref tables, newFilter, conn, transaction, null);
                                }

                                //We need to add to the current values (or create new ones)
                                if (!newRow.GetKeyNames.Contains("Value"))
                                {
                                    //object[] valList = new object[] { value };
                                    //newRow["Value"] = valList;
                                    object[] valArray = new object[2];
                                    newRow["Value"] = valArray;
                                    string thisColName = dt.Columns[i].Caption;
                                    if (thisColName == "Key")
                                    {
                                        if (valArray[0] == null)
                                        {
                                            valArray[0] = new object[] { value };
                                        }
                                    }
                                    else if (thisColName == "Value")
                                    {
                                        if (valArray[1] == null)
                                        {
                                            valArray[1] = new object[] { value };
                                        }
                                    }
                                }
                                else
                                {
                                    object[] valArray = newRow["Value"] as object[];
                                    
                                    string thisColName = dt.Columns[i].Caption;
                                    if (thisColName == "Key")
                                    {
                                        if (valArray[0] == null)
                                        {
                                            valArray[0] = new object[] { value };
                                        }
                                        else
                                        {
                                            object[] valList = valArray[0] as object[];
                                            object[] newValList = new object[valList.Length + 1];
                                            valList.CopyTo(newValList, 0);
                                            newValList[valList.Length] = value;
                                            valArray[0] = newValList;
                                        }
                                    }
                                    else if (thisColName == "Value")
                                    {
                                        if (valArray[1] == null)
                                        {
                                            valArray[1] = new object[] { value };
                                        }
                                        else
                                        {
                                            object[] valList = valArray[1] as object[];
                                            object[] newValList = new object[valList.Length + 1];
                                            valList.CopyTo(newValList, 0);
                                            newValList[valList.Length] = value;
                                            valArray[1] = newValList;
                                        }
                                    }
                                }

                                if (!newRow.GetKeyNames.Contains("OriginatingTypeNames"))
                                {
                                    newRow["OriginatingTypeNames"] = new string[] { stringType };
                                }
                                else
                                {
                                    string[] typeList = newRow["OriginatingTypeNames"] as string[];
                                    //This says "if the typelist is the same length as the list of keys..."
                                    if (typeList.Length == ((newRow["Value"] as object[])[0] as object[]).Length)
                                    {
                                        //in which case we want to append the second type onto the existing string, not create a new one.
                                        typeList[typeList.Length - 1] += ":" + stringType;
                                    }
                                    else
                                    {
                                        string[] newTypeList = new string[typeList.Length + 1];
                                        typeList.CopyTo(newTypeList, 0);
                                        newTypeList[typeList.Length] = stringType;
                                        newRow["OriginatingTypeNames"] = newTypeList;
                                    }
                                }

                                //(newRow["Value"] as object[])[i - 1] = valList;

                                //for (int k = 0; k < (data[i] as string[]).Length; k++)
                                //{
                                //    if ((newRow["OriginatingTypeNames"] as string[])[k] != "")
                                //        (newRow["OriginatingTypeNames"] as string[])[k] += ":";
                                //    (newRow["OriginatingTypeNames"] as string[])[k] += typeList[k];
                                //}
                            }
                        }
                        else if (filter.TargetHelperClassName.Split(':')[0] == "IList")
                        {
                            //Lists only have two data entries, and one is the guid.  So this will only get called once.
                            newRow["PropertyName"] = "ListItems";

                            //Handle object lists seperately
                            string typeName = filter.TargetHelperClassName.Split(':')[1];
                            Type typeFromName = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeName);

                            //Overhauling to omit arrays here...
                            //The single datum being passed through this loop will be the value.
                            string valString = datum.ToString();
                            object value = null;
                            string[] split = valString.Split(':');
                            if (split.Length < 2)
                            {
                                continue;
                            }
                            string typeString = split[1];
                            Type toParse = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeString);


                            Dictionary<Type, List<Guid>> typedList = new Dictionary<Type, List<Guid>>();
                            Dictionary<String, List<Guid>> helperList = new Dictionary<string, List<Guid>>();

                            if (toParse == typeof(string))
                                value = split[0];
                            else if (toParse == typeof(Guid) || PostgreSQLDBEngine.GetDBTypeName(toParse) == "uuid")
                            {
                                RelationalFilter newFilter;

                                value = new Guid(split[0]);

                                string realTable = GetTableName(toParse);

                                //Since object columns do not have a type numerical suffix, the relevant table must be rooted out here.
                                //RelationalFilter newFilter = null;
                                //segregate dictionary/list since they have implied types.
                                if (realTable.Contains(':'))
                                {
                                    newFilter = new RelationalFilter(split[1], new Guid(split[0]));
                                    if (!helperList.Keys.Contains(realTable))
                                        helperList.Add(realTable, new List<Guid>());
                                    helperList[realTable].Add(new Guid(split[0]));
                                }
                                else
                                {
                                    Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);
                                    newFilter = new RelationalFilter(tableType, new Guid(split[0]));
                                    if (!typedList.Keys.Contains(tableType))
                                        typedList.Add(tableType, new List<Guid>());
                                    typedList[tableType].Add(new Guid(split[0]));
                                }

                            }
                            else
                            {
                                MethodInfo mi = toParse.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                                value = mi.Invoke(null, new object[] { split[0] });
                                //valList[j] = mi.Invoke(null, new object[] { split[0] });
                            }


                            //Do late table adds in-batch here.
                            foreach (Type tabType in typedList.Keys)
                            {
                                List<Guid> idList = typedList[tabType];

                                RelationalFilter newFilter = new RelationalFilter(tabType, idList);
                                PostgreSQLDBEngine.ReadDataIntoTablesQuicker(ref tables, newFilter, conn, transaction, null);
                            }

                            foreach (string tabString in helperList.Keys)
                            {
                                List<Guid> idList = helperList[tabString];

                                RelationalFilter newFilter = new RelationalFilter(tabString, idList);
                                PostgreSQLDBEngine.ReadDataIntoTablesQuicker(ref tables, newFilter, conn, transaction, null);
                            }

                            //We need to add to the current values (or create new ones)
                            if (!newRow.GetKeyNames.Contains("Value"))
                            {
                                object[] valList = new object[] { value };
                                newRow["Value"] = valList;
                            }
                            else
                            {
                                object[] valList = newRow["Value"] as object[];
                                object[] newValList = new object[valList.Length + 1];
                                valList.CopyTo(newValList, 0);
                                newValList[valList.Length] = value;
                                newRow["Value"] = newValList;
                            }

                            if (!newRow.GetKeyNames.Contains("OriginatingTypeNames"))
                            {
                                newRow["OriginatingTypeNames"] = new string[] { typeString };
                            }
                            else
                            {
                                string[] typeList = newRow["OriginatingTypeNames"] as string[];
                                string[] newTypeList = new string[typeList.Length + 1];
                                typeList.CopyTo(newTypeList, 0);
                                newTypeList[typeList.Length] = typeString;
                                newRow["OriginatingTypeNames"] = newTypeList;
                            }

                            //newRow["Value"] = value;
                            //newRow["OriginatingTypeNames"] = new string[] { typeString };

                            //    newRow["Value"] = valList;
                            //    newRow["OriginatingTypeNames"] = typeList;
                            //}
                            //else
                            //{
                            //    newRow["Value"] = data[1];
                            //    string[] newOrigTypeNames = new string[1];
                            //    newOrigTypeNames[0] = tableTypeString;
                            //    if (tableTypeString != "")
                            //        newRow["OriginatingTypeNames"] = newOrigTypeNames;
                            //    else
                            //        newRow["OriginatingTypeNames"] = (data[i] as Array).GetValue(0).GetType().FullName;
                            //}
                        }
                        else
                        {
                            throw new Exception("Unrecognized helper type during deserialization.");
                        }
                    }
                    else
                    {
                        //Handle object typing here.
                        if (retTable.GetTypeFromColumnName(colName) == typeof(object))
                        {
                            if (datum is string)
                            {
                                string[] split = (datum as string).Split(':');

                                Type toParse = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);

                                if (toParse == typeof(string))
                                    newRow[colName] = split[0];
                                else if (toParse == typeof(Guid) || PostgreSQLDBEngine.GetDBTypeName(toParse) == "uuid")
                                {
                                    newRow[colName] = new Guid(split[0]);
                                    //Since object columns do not have a type numerical suffix, the relevant table must be rooted out here.
                                    RelationalFilter newFilter;
                                    //segregate dictionary/list since they have implied types.
                                    if (split[1].Contains(':'))
                                        newFilter = new RelationalFilter(split[1], new Guid(split[0]));
                                    else
                                    {
                                        Type tableType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(split[1]);
                                        newFilter = new RelationalFilter(tableType, new Guid(split[0]));
                                    }
                                    PostgreSQLDBEngine.ReadDataIntoTablesQuicker(ref tables, newFilter, conn, transaction, null);
                                }
                                else
                                {
                                    MethodInfo mi = toParse.GetMethod("Parse", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                                    newRow[colName] = mi.Invoke(null, new object[] { split[0] });
                                }
                            }
                        }
                        else
                        {
                            //Strip type identifiers from the column names before indexing them in the row

                            //For some reason character types are being returned as strings.  So to prevent type issues further on,
                            //single-character strings are converted to char types here.
                            if (datum.GetType() == typeof(string) && (datum as string).Length == 1)
                                newRow[colName.Split(':')[0]] = (datum as string)[0];
                            else
                                newRow[colName.Split(':')[0]] = datum;
                        }
                    }
                }

                if (newRow != null && !retTable.Rows.Keys.Contains(newRow.RowIdentifier))
                    retTable.Rows.Add(newRow.RowIdentifier, newRow);
            }

            trFASTERReadDataIntoTables = trFASTERReadDataIntoTables.Add(DateTime.Now.Subtract(time));

            return ret;
        }

        //How to handle a situation where a table for a type already exists but has new/different columns?
        //append version names and match on columns?  Keep table of "current versions"?

        //Solution: Throw an exception if the column check fails, add an option for table rename

        // TODO : CHECK BEHAVIOR WHEN A GUID (not related to the DB) IS STORED! Probably a problem!

        public struct VersionPair
        {
            public string updatedBy;
            public string versionNumber;

            public VersionPair(string versionNumber, string updatedBy)
            {
                this.versionNumber = versionNumber;
                this.updatedBy = updatedBy;
            }
        }

        //Hidden indexes used to keep track of the table index linkage between central and a client.
        private static Dictionary<string, string> indexCentralToClient = null;
        private static Dictionary<string, string> indexClientToCentral = null;

        public static int SynchronizeFullDatabases(NpgsqlConnection centralConn, NpgsqlConnection clientConn)
        {
            ////////////////////////
            //SYNCHRONIZE DISABLED//
            ////////////////////////
            //return 0; 

            //Clean the registry of new tables.
            newTablesColNamesTypes.Clear();

            //Maintain an index conversion table so that the proper index conversion can be kept track of during table creation and merging.
            indexCentralToClient = new Dictionary<string, string>();
            indexClientToCentral = new Dictionary<string, string>();

            //Get a list of all registered tables in central, and of all registered tables in the client
            Dictionary<string, string> centralRegisteredTables = PostgreSQLDBEngine.GetRegisteredTables(centralConn);
            Dictionary<string, string> clientRegisteredTables = PostgreSQLDBEngine.GetRegisteredTables(clientConn);

            //Synchronize the table registration.  This is the first step in ensuring data structure consistency across client and central.
            #region Table Registration Synchronization

            //First, synchronize from central to the client.
            foreach (string longName in centralRegisteredTables.Keys)
            {               
                //Get the internal (DB) table name from the client connection.
                //TODO : add method to "pump" internal name lookup cache, to pre-load all names at once.  Slight speedup here.
                string internalClientTableName = PostgreSQLDBEngine.LongNameToDBTableName(longName, clientConn);

                //Register any table not already registered.
                //TODO : register all new tables en masse (can use a prepared statement, slight speedup).
                if (internalClientTableName == null)
                    internalClientTableName = PostgreSQLDBEngine.RegisterNewLongTableName(longName, clientConn);

                //Now that we have the internal client table's name, we can extract the index
                string clientIndex = PostgreSQLDBEngine.GetIndexFromInternalTableName(internalClientTableName);

                //Fill index dictionaries in both directions.  Central's index is already stored in the Registered Tables driving the loop.
                indexCentralToClient.Add(centralRegisteredTables[longName], clientIndex);
                indexClientToCentral.Add(clientIndex, centralRegisteredTables[longName]);
            }
            
            //Second, we must synchronize from the client to central.  However, we can skip quite a lot of names to speed things up.
            foreach (string longName in clientRegisteredTables.Keys)
            {
                //Since the central registered tables have already been iterated over, any of them already present in the client registration may be skipped
                //(the indexes have already been built for them and they obviously don't need to be registered)
                if (centralRegisteredTables.Keys.Contains(longName))
                    continue;

                //Aside from omitting tables that are already registered, this synch continues in exactly the same fashion :

                //Get the internal (DB) table name from the central connection.
                //TODO : add method to "pump" internal name lookup cache, to pre-load all names at once.  Slight speedup here.
                string internalCentralTableName = PostgreSQLDBEngine.LongNameToDBTableName(longName, centralConn);

                //Register any table not already registered.
                //TODO : register all new tables en masse (can use a prepared statement, slight speedup).
                if (internalCentralTableName == null)
                    internalCentralTableName = PostgreSQLDBEngine.RegisterNewLongTableName(longName, centralConn);

                //Now that we have the internal client table's name, we can extract the index
                string centralIndex = PostgreSQLDBEngine.GetIndexFromInternalTableName(internalCentralTableName);

                //Fill index dictionaries in both directions.  Central's index is already stored in the Registered Tables driving the loop.
                indexCentralToClient.Add(clientRegisteredTables[longName], centralIndex);
                indexClientToCentral.Add(centralIndex, clientRegisteredTables[longName]);
            }

            #endregion

            //Get a list of all tables in central...
            List<string> centralTableList = new List<string>();
            centralTableList.AddRange(PostgreSQLDBEngine.ListAllTablesInDB(centralConn));
            
            //...and a list of all tables in client.
            List<string> clientTableList = new List<string>();
            clientTableList.AddRange(PostgreSQLDBEngine.ListAllTablesInDB(clientConn));

            //It's advantageous (critical might be the better word, given certain anomalous NPGSQL behavior) for the final row synchronization to know 
            //which tables didn't exist in the central and client databases when sychronization begins - this obviously signals a full insert across the board
            //for the side previously lacking that table.  More importantly, NPGSQL doesn't seem to gaurantee that the table actually exists in the db
            //until inserts are made (oops).  It is therefore important to retrieve the schema from the correct source.
            List<string> newCentralTables = new List<string>();
            List<string> newClientTables = new List<string>();

            //Now that table registration has been synched and existing tables have been listed on both ends, we can proceed to synch table existence (creation)
            #region Table Existence Synchronization

            //Iterate across central tables and create the equivalent in the client whenever it lacks that table.
            for (int i = 0; i < centralTableList.Count; i++)
			{
                string internalCentralTableName = centralTableList[i];

                //Ignore system tables, they are gauranteed to be in all databases.
                if (internalCentralTableName == "extended_tables" || internalCentralTableName == "class_maps" || internalCentralTableName == "db_parameters")
                    continue;

                //Get the long name for the current central table
                //TODO : Add caching for this method.  Should be able to cache this during the "cache pump" method mentioned earlier which is basically
                //fetching the whole extended_tables table to memory anyway.
                string longName = PostgreSQLDBEngine.TableNameFromInternalName(internalCentralTableName, centralConn);
                //Use the long name to obtain the internal name for the equivalent client table (already cached)
                string internalClientTableName = PostgreSQLDBEngine.LongNameToDBTableName(longName, clientConn);
                
                //Finally, check for the client internal table name in the list of client tables.
                if (!clientTableList.Contains(internalClientTableName))
                {
                    //If it's not found, that means we need to actually create the client table.
                    PostgreSQLDBEngine.MirrorTable(internalCentralTableName, internalClientTableName, centralConn, clientConn, false,
                        indexCentralToClient);
                    newClientTables.Add(internalClientTableName);
                }
			}

            //Now repeat the process with the client tables.  We do not re-fetch the table list because the tables present BEFORE synching creation are really
            //the only source of interest (do not care about anything except tables which client has that central does not).
            for (int i = 0; i < clientTableList.Count; i++)
            {
                //There is probably some optimization that we can do here to check fewer tables.  If lookups are cached properly it is probably
                //only fractionally faster, and not worth the extra complexity
                string internalClientTableName = clientTableList[i];

                //Ignore system tables, they are gauranteed to be in all databases.
                if (internalClientTableName == "extended_tables" || internalClientTableName == "class_maps" || internalClientTableName == "db_parameters")
                    continue;

                //Get the long name for the current client table
                //TODO : Add caching for this method.  Should be able to cache this during the "cache pump" method mentioned earlier which is basically
                //fetching the whole extended_tables table to memory anyway.
                string longName = PostgreSQLDBEngine.TableNameFromInternalName(internalClientTableName, clientConn);
                //Use the long name to obtain the internal name for the equivalent central table (already cached)
                string internalCentralTableName = PostgreSQLDBEngine.LongNameToDBTableName(longName, centralConn);
                
                //Finally, check for the central internal table name in the list of central tables.
                if (!centralTableList.Contains(internalCentralTableName))
                {
                    //If it's not found, that means we need to actually create the central table.
                    PostgreSQLDBEngine.MirrorTable(internalClientTableName, internalCentralTableName, clientConn, centralConn, false,
                        indexClientToCentral);
                    newCentralTables.Add(internalCentralTableName);
                }
            }

            //Fixup to ensure that subclasses are referenced correctly in the extended_tables.
            PostgreSQLDBEngine.UpdateTableSubclassReferences(centralConn);
            PostgreSQLDBEngine.UpdateTableSubclassReferences(clientConn);

            #endregion

            //Keep track of records inserted or updated by the synchronize and return them.
            int total = 0;

            //Synchronize row data on all tables - this includes inserts and updates.
            #region Row Data Synchronization

            //Combine the table lists for central and drive the synch from this combined table.
            List<string> allCentralTables = new List<string>();
            allCentralTables.AddRange(centralTableList);
            allCentralTables.AddRange(newCentralTables);

            //Synchronize!
            for (int j = 0; j < allCentralTables.Count; j++)
            {
                string centralTableToMergeOn = allCentralTables[j];

                //As always, ignore system tables.
                if (centralTableToMergeOn == "extended_tables" || centralTableToMergeOn == "class_maps" || centralTableToMergeOn == "db_parameters")
                    continue;

                //Find the client table to merge on by first stripping the index from the internal table name of the currently indexed string from
                //central table list, feeding this index through the Central->Client index dictionary, and appending the result to the standard
                // "table_" prefix.
                string clientTableToMergeOn = "table_" + indexCentralToClient[PostgreSQLDBEngine.GetIndexFromInternalTableName(centralTableToMergeOn)];

                //See if the client table is new.  If so, we want to call a different variation of synch.
                bool clientIsNew = newClientTables.Contains(clientTableToMergeOn);
                bool centralIsNew = newCentralTables.Contains(centralTableToMergeOn);

                //Finally, call the synchronize.
                total += PostgreSQLDBEngine.SynchronizeOnInternalTables(centralTableToMergeOn, clientTableToMergeOn, centralIsNew, clientIsNew, centralConn, clientConn);
            }

            #endregion

            return total;
        }

        public static int SynchronizeOnInternalTables(string internalCentralTableName, string internalClientTableName, bool centralIsNew, bool clientIsNew, 
            NpgsqlConnection centralConn, NpgsqlConnection clientConn)
        {
            //If either table doesn't exist, we can't do anything
            if (internalCentralTableName == null || internalClientTableName == null)
                return 0;

            //Skip the internal system tables.  These are never synchronized through the standard synch, because they lack a uuid-based primary key, which is
            //central in the synchronization scheme.  In addition, extended_tables and db_parameters are not meant to be synchronized anyways.
            if (internalCentralTableName == "extended_tables" || internalCentralTableName == "db_parameters" || internalCentralTableName == "class_maps" ||
                internalClientTableName == "extended_tables" || internalClientTableName == "db_parameters" || internalClientTableName == "class_maps")
                return 0;

            //Synchronization on two new tables is not allowed (and wouldn't do much anyways)
            if (centralIsNew && clientIsNew)
                return 0;

            //Get the version package for each source for each table concerned.
            Dictionary<string, VersionPair> centralVP = PostgreSQLDBEngine.GetVersionPackage(internalCentralTableName, centralConn);
            Dictionary<string, VersionPair> clientVP = PostgreSQLDBEngine.GetVersionPackage(internalClientTableName, clientConn);

            //Use the version sorter to compare version packages and split source GUIDs into insert and update requests
            List<string> centralInserts = new List<string>();
            List<string> centralUpdates = new List<string>();
            List<string> clientInserts = new List<string>();
            List<string> clientUpdates = new List<string>();

            PostgreSQLDBEngine.VersionCompareAndSort(centralVP, clientVP, centralInserts, centralUpdates, clientInserts, clientUpdates);

            //Produce a unified list of GUIDs to fetch for each db.
            List<string> centralFetchData = new List<string>();
            List<string> clientFetchData = new List<string>();

            //Naming convention is a bit confusing here - Inserts and Updates says what ID rows to Insert/Update TO THAT DB.
            //Conversely, Fetch Data says what IDs to get FROM THAT DB.  Therefore, they go in opposites :
            //From Central, we must fetch clientUpdates and clientInserts.
            centralFetchData.AddRange(clientUpdates);
            centralFetchData.AddRange(clientInserts);
            //From the client, we must fetch centralUpdates and centralInserts.
            clientFetchData.AddRange(centralUpdates);
            clientFetchData.AddRange(centralInserts);           

            //Skip out now if there are no requests.
            if (centralFetchData.Count == 0 && clientFetchData.Count == 0)
                return 0;

            //We must know if the table we're synchronizing is a collection type (an IList or IDictionary)
            //Be careful not to fetch the definition from the "new" side, as it might not actually be there yet.
            //TODO : once the TableNameFromInternalName method is properly cached this should no longer be an issue and either db can fetch the name.
            string tableName = "";
            if (!centralIsNew)
                tableName = PostgreSQLDBEngine.TableNameFromInternalName(internalCentralTableName, centralConn);
            else if (!clientIsNew)
                tableName = PostgreSQLDBEngine.TableNameFromInternalName(internalClientTableName, clientConn);

            //We can determine whether or not a table is a collection by checking what its name is in the db registry.
            bool tableIsCollection = (tableName.Split(':')[0] == "IList" || tableName.Split(':')[0] == "IDictionary");

            //Now, in a collection the desired behavior is to merge the collections on both sides.  Therefore if the table is a collection, we must
            //fetch the IDs on BOTH sides, regardless of which side receives the "update," and this may as well be done all at once.
            //To that end, if the table is a collection, add the central updates to the central fetch, and vice-versa.
            if (tableIsCollection)
            {
                clientFetchData.AddRange(clientUpdates);
                centralFetchData.AddRange(centralUpdates);
            }

            //Fetch central and client data.
            DataTable centralData = PostgreSQLDBEngine.GetData(centralFetchData, internalCentralTableName, centralConn);
            DataTable clientData = PostgreSQLDBEngine.GetData(clientFetchData, internalClientTableName, clientConn);

            //Finally, use the sourceData, inserts/updates lists, and the target's connection and internal table name to do all of the inserts/updates.
            //return PostgreSQLDBEngine.MergeData(sourceData, targetData, inserts, updates, intTargetTableName, targetConn, tableIsCollection);
            return PostgreSQLDBEngine.SynchronizeData(centralData, clientData, centralInserts, centralUpdates, clientInserts,
                clientUpdates, internalCentralTableName, internalClientTableName, centralConn, clientConn, centralIsNew,
                clientIsNew, tableIsCollection);
            return 0;
        }

        public static string GetIndexFromInternalTableName(string internalName)
        {
            //Internal names consist of the an index number prefixed by "table_", ie : table_1, table_2, etc.  Split on the underscore to extract the index.
            return internalName.Split('_')[1];
        }

        private static Dictionary<string, string> indexSourceToTarget = null;
        private static Dictionary<string, string> indexTargetToSource = null;

        public static int MassMerge(NpgsqlConnection sourceConn, NpgsqlConnection targetConn)
        {
            //Maintain an index conversion table so that the proper index conversion can be kept track of during table creation and merging.
            indexSourceToTarget = new Dictionary<string, string>();
            indexTargetToSource = new Dictionary<string, string>();

            //Get a list of all registered tables in the source - sometimes a table is registered but not created, and a merge requires a full link between all registrations to
            //create tables properly.
            Dictionary<string, string> sourceRegisteredTables = PostgreSQLDBEngine.GetRegisteredTables(sourceConn);

            //Set up the index conversion table and register tables as necessary.
            foreach (string longName in sourceRegisteredTables.Keys)
            {
                string sourceIndex = sourceRegisteredTables[longName];
                string intTargetTableName = PostgreSQLDBEngine.LongNameToDBTableName(longName, targetConn);

                if (intTargetTableName == null)
                    intTargetTableName = PostgreSQLDBEngine.RegisterNewLongTableName(longName, targetConn);

                string targetIndex = intTargetTableName.Split('_')[1];
                indexSourceToTarget.Add(sourceIndex, targetIndex);
                indexTargetToSource.Add(targetIndex, sourceIndex);
            }

            //Now that table registration has been synched, we can proceed to synch table existence (creation)
            
            //Get a list of all tables in the source.
            string[] sourceTableList = PostgreSQLDBEngine.ListAllTablesInDB(sourceConn);
            //and a list of all tables in the target
            List<string> targetTableList = new List<string>();
            targetTableList.AddRange(PostgreSQLDBEngine.ListAllTablesInDB(targetConn));

            //Iterate across source tables and create the equivalent in the target whenever the target lacks that table.
            for (int i = 0; i < sourceTableList.Length; i++)
            {
                string internalSourceTable = sourceTableList[i];

                //Do not attempt to inspect or create system tables, they are already gauranteed to be in the target.
                if (internalSourceTable == "extended_tables" || internalSourceTable == "class_maps" || internalSourceTable == "db_parameters")
                    continue;

                //Get the long name for the current source table
                string longName = PostgreSQLDBEngine.TableNameFromInternalName(internalSourceTable, sourceConn);
                //Use the long name to obtain the internal name for the equivalent target table
                string internalTargetTable = PostgreSQLDBEngine.LongNameToDBTableName(longName, targetConn);
                //Finally, check for the target internal name in the list of target tables.
                if (!targetTableList.Contains(internalTargetTable))
                {
                    //If it's not found, that means we need to actually create the target table.
                    PostgreSQLDBEngine.MirrorTable(internalSourceTable, internalTargetTable, sourceConn, targetConn, false, indexSourceToTarget);
                }
            }

            //Fixup to ensure that subclasses are referenced correctly.
            PostgreSQLDBEngine.UpdateTableSubclassReferences(targetConn);

            int total = 0;

            //Merge on all tables.
            for (int j = 0; j < sourceTableList.Length; j++)
            {
                //As always, ignore system tables.
                if (sourceTableList[j] == "extended_tables" || sourceTableList[j] == "class_maps" || sourceTableList[j] == "db_parameters")
                    continue;
                string targetTableToMergeOn = "table_" + indexSourceToTarget[sourceTableList[j].Split('_')[1]];
                total += PostgreSQLDBEngine.MergeOnInternalTables(sourceTableList[j], targetTableToMergeOn, sourceConn, targetConn);
            }


            return total;
        }

        public static int MergeOnClassTable(string tableName, NpgsqlConnection sourceConn, NpgsqlConnection targetConn)
        {
            string intSourceTableName = PostgreSQLDBEngine.LongNameToDBTableName(tableName, sourceConn);
            string intTargetTableName = PostgreSQLDBEngine.LongNameToDBTableName(tableName, targetConn);
            return PostgreSQLDBEngine.MergeOnInternalTables(intSourceTableName, intTargetTableName, sourceConn, targetConn);
        }

        public static int MergeOnInternalTables(string intSourceTableName, string intTargetTableName, NpgsqlConnection sourceConn, NpgsqlConnection targetConn)
        {
            if (intTargetTableName == "table_22")
            {
                string bullshit = "234";
            }

            //If the source table as requested doesn't exist, we can't do anything.
            if (intSourceTableName == null)
                return 0;

            //When the target table doesn't exist, we can't do anything.
            if (intTargetTableName == null)
                return 0;

            //Skip the protected tables, extended_tables and db_parameters
            if (intSourceTableName == "extended_tables" || intSourceTableName == "db_parameters" || intSourceTableName == "class_maps")
                return 0;

            //Get the version package for each source for each table concerned.
            Dictionary<string, VersionPair> sourceVP = PostgreSQLDBEngine.GetVersionPackage(intSourceTableName, sourceConn);
            Dictionary<string, VersionPair> targetVP = PostgreSQLDBEngine.GetVersionPackage(intTargetTableName, targetConn);

            //Use the version sorter to compare version packages and split source GUIDs into insert and update requests
            List<string> inserts = new List<string>();
            List<string> updates = new List<string>();
            PostgreSQLDBEngine.VersionCompareAndSort(sourceVP, targetVP, inserts, updates);

            //Produce a unified list of GUIDs to fetch
            List<string> fetchData = new List<string>();
            fetchData.AddRange(inserts);
            fetchData.AddRange(updates);

            //Skip out now if there are no requests.
            if (fetchData.Count == 0)
                return 0;

            //Fetch data from the source target from the list of GUIDs
            DataTable sourceData = PostgreSQLDBEngine.GetData(fetchData, intSourceTableName, sourceConn);

            //We must know if the targetTable is an IList or IDictionary -
            string tableName = PostgreSQLDBEngine.TableNameFromInternalName(intTargetTableName, targetConn);
            bool tableIsCollection = (tableName.Split(':')[0] == "IList" || tableName.Split(':')[0] == "IDictionary");

            //Fetch data from the target table from the list of update GUIDs only, and only if the table is a collection
            DataTable targetData = null;
            if (tableIsCollection)
                targetData = PostgreSQLDBEngine.GetData(updates, intTargetTableName, targetConn);

            //Finally, use the sourceData, inserts/updates lists, and the target's connection and internal table name to do all of the inserts/updates.
            return PostgreSQLDBEngine.MergeData(sourceData, targetData, inserts, updates, intTargetTableName, targetConn, tableIsCollection);
        }

        public static Dictionary<string, string> GetRegisteredTables(NpgsqlConnection conn)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql = "SELECT table_id, name FROM extended_tables;";
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();

            da.Fill(ds);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                string tableID = row.ItemArray[0].ToString();
                string name = row.ItemArray[1].ToString();

                ret.Add(name, tableID);
            }

            return ret;
        }

        public static Dictionary<string, VersionPair> GetVersionPackage(string internalTableName, NpgsqlConnection conn)
        {
            Dictionary<string, VersionPair> ret = new Dictionary<string, VersionPair>();

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            string sql = "SELECT guid, row_version, version_updated_by FROM " + internalTableName + ";";
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();

            da.Fill(ds);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                string guid = row.ItemArray[0].ToString();
                string row_version = row.ItemArray[1].ToString();
                string version_updated_by = row.ItemArray[2].ToString();

                VersionPair thisPair = new VersionPair(row_version, version_updated_by);
                ret.Add(guid, thisPair);
            }

            return ret;
        }

        public static void VersionCompareAndSort(Dictionary<string, VersionPair> sourceVP, Dictionary<string, VersionPair> targetVP, List<string> insertRequests, List<string> updateRequests)
        {
            //We are only parsing on the source's keys, since we do not care about elements that are in the target and not the source.
            foreach (string sourceID in sourceVP.Keys)
            {
                //If the key is in both sources, some extra processing is required to determine if we request an update, or do nothing
                if (targetVP.Keys.Contains(sourceID))
                {
                    VersionPair sourceVer = sourceVP[sourceID];
                    VersionPair targetVer = targetVP[sourceID];

                    //Always update if the updatedBy tags are different.
                    //With same tags, only when the target version number is less than the source version number is an update requested.
                    if (sourceVer.updatedBy == targetVer.updatedBy && (int.Parse(sourceVer.versionNumber) <= int.Parse(targetVer.versionNumber)))
                        continue;
                    
                    updateRequests.Add(sourceID);
                }
                else
                {
                    insertRequests.Add(sourceID);
                }
            }
        }
        
        //This version of the VersionCompareAndSort method is used by synchronization, whereas the other is used for merge.
        public static void VersionCompareAndSort(Dictionary<string, VersionPair> centralVP, Dictionary<string, VersionPair> clientVP, 
            List<string> centralInsertRequests, List<string> centralUpdateRequests, List<string> clientInsertRequests, List<string> clientUpdateRequests)
        {
            //First parse central's keys.
            foreach (string centralID in centralVP.Keys)
            {
                if (clientVP.Keys.Contains(centralID))
                {
                    VersionPair centralVer = centralVP[centralID];
                    VersionPair clientVer = clientVP[centralID];

                    //The primary driver is version.  Greater version means the OTHER database is updated from the database with the higher version.
                    if (int.Parse(centralVer.versionNumber) > int.Parse(clientVer.versionNumber))
                    {
                        clientUpdateRequests.Add(centralID);
                    }
                    else if (int.Parse(centralVer.versionNumber) < int.Parse(clientVer.versionNumber))
                    {
                        centralUpdateRequests.Add(centralID);
                    }
                    else
                    {
                        //When the versions are equal things get a little less straightforward.
                        //If the updatedBy values are equivalent, continue (ask to do nothing)
                        if (centralVer.updatedBy == clientVer.updatedBy)
                            continue;
                        //If the updatedBy values differ, overwrite the client.
                        //VERIFY THIS BEHAVIOR
                        clientUpdateRequests.Add(centralID);
                    }
                }
                else
                {
                    clientInsertRequests.Add(centralID);
                }
            }

            //Parse client's keys.  Since we've already processed any keys in central, we can skip any keys also found in central.
            foreach (string clientID in clientVP.Keys)
            {
                if (centralVP.Keys.Contains(clientID))
                {
                    continue;
                }
                else
                {
                    centralInsertRequests.Add(clientID);
                }
            }
        }

        public static DataTable GetData(List<string> uids, string internalTableName, NpgsqlConnection conn)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;
            
            //Build a string with the guids
            StringBuilder whereString = new StringBuilder(" WHERE ");
            
            bool first = true;
            foreach (string idStr in uids)
            {
                if (!first)
                    whereString.Append(" OR ");
                whereString.Append("guid = '");
                whereString.Append(idStr);
                whereString.Append("'");
                first = false;
            }

            if (first == true)
                return null;

            string sql = "SELECT * FROM " + internalTableName + whereString.ToString() + ";";
            da = new NpgsqlDataAdapter(sql, conn);
            ds.Reset();

            da.Fill(ds);
            dt = ds.Tables[0];

            //For standard tables, the primary key is the guid.  Go ahead and set this to make life easier later (no idea why it isn't automatically set from the schema)
            if (internalTableName.Split('_')[0] == "table")
                dt.PrimaryKey = new DataColumn[] { dt.Columns[dt.Columns.IndexOf("guid")] };

            return dt;
        }

        public static int MergeData(DataTable sourceData, DataTable targetData, List<string> inserts, List<string> updates, string internalTargetTableName, NpgsqlConnection targetConn, bool tableIsCollection)
        {
            //First we need to get the column name/type pairs.
            Dictionary<string, string> namesTypes = PostgreSQLDBEngine.GetTableColumnNamesAndTypesFromInternalName(internalTargetTableName, targetConn, true);

            int index = 1;
            StringBuilder columnNames = new StringBuilder("");
            StringBuilder indexNumbers = new StringBuilder("");
            StringBuilder columnTypes = new StringBuilder("");

            Dictionary<string, int> nameIndex = new Dictionary<string, int>();

            foreach (string colName in namesTypes.Keys)
            {
                if(index != 1)
                {
                    columnTypes.Append(", ");
                    indexNumbers.Append(", ");
                    columnNames.Append(", ");
                }

                columnTypes.Append(namesTypes[colName]);
                indexNumbers.Append("$");
                indexNumbers.Append(index.ToString());
                columnNames.Append("\"");
                columnNames.Append(colName);
                columnNames.Append("\"");

                //keep track of the name/index pairs because the column names are not gauranteed to be in "order" during the data processing.
                nameIndex.Add(colName, index);

                index++;
            }

            string insertPrepare = "PREPARE insertPlan (" + columnTypes.ToString() + ") AS " +
                                   "INSERT INTO " + internalTargetTableName + " (" + columnNames.ToString() + ") VALUES (" +
                                   indexNumbers.ToString() + ");";

            string updatePrepare = "PREPARE updatePlan (" + columnTypes.ToString() + ", uuid) AS " +
                                   "UPDATE " + internalTargetTableName + " SET (" + columnNames.ToString() + ") = (" +
                                   indexNumbers.ToString() + ") WHERE guid = $" + index + ";";

            StringBuilder insertQuery = new StringBuilder("");
            StringBuilder updateQuery = new StringBuilder("");

            insertQuery.Append(insertPrepare);
            updateQuery.Append(updatePrepare);

            insertQuery.Append(" ");
            updateQuery.Append(" ");

            //We must sort the items into an array by matching the index with the column name.
            //This is done to make sure that the executed statement matches the correct value order, since the column order of the ItemArray is
            //not gauranteed to be the same order as the columns read in the prepare statement.
            object[] sortedItems = null;

            foreach (DataRow row in sourceData.Rows)
            {
                string uidString = row.ItemArray[0].ToString();
                bool isInsert = inserts.Contains(uidString);

                if (isInsert)
                    insertQuery.Append("EXECUTE insertPlan(");
                else
                    updateQuery.Append("EXECUTE updatePlan(");

                if (sortedItems == null)
                    sortedItems = new object[row.ItemArray.Length];

                for (int j = 0; j < row.ItemArray.Length; j++)
                {
                    object value = row.ItemArray[j];
                    string colName = sourceData.Columns[j].Caption;
                    
                    //Because the column names differ between source and target when there is a destination index suffix on the column name, we must convert here.
                    string colType = "";
                    int thisIndex = 0;

                    if (colName.Contains(':'))
                    {
                        try
                        {
                            string convertedColName = colName.Split(':')[0] + ":" + indexSourceToTarget[colName.Split(':')[1]];
                            colType = namesTypes[convertedColName];
                            thisIndex = nameIndex[convertedColName];
                        }
                        catch (Exception ex)
                        {
                            int s = 0;
                        }
                    }
                    else
                    {
                        colType = namesTypes[colName];
                        thisIndex = nameIndex[colName];
                    }

                    //During an update, when the columnType is "text[]" and the table is a collection, we merge rather than overwrite data.
                    if (!isInsert && tableIsCollection && colType == "text[]")
                    {
                        //Get ahold of the correct row and data from the target data.
                        Guid targetID = new Guid(uidString);
                        DataRow thisRow = targetData.Rows.Find(targetID);
                        object targetValue = thisRow.ItemArray[targetData.Columns.IndexOf(colName)];

                        //Make a list of "merged" values and add both target and source data to it (uniquely)
                        List<string> mergedValues = new List<string>();

                        foreach (string targetString in targetValue as string[])
                        {
                            if (!mergedValues.Contains(targetString))
                                mergedValues.Add(targetString);
                        }

                        foreach (string sourceString in value as string[])
                        {
                            if (!mergedValues.Contains(sourceString))
                                mergedValues.Add(sourceString);
                        }

                        //Finally, parse the merged values into a DB-compatible array string
                        StringBuilder arrayString = new StringBuilder("{");
                        bool first = true;
                        foreach (string thisString in mergedValues)
                        {
                            if (!first)
                                arrayString.Append(", ");
                            arrayString.Append("\"");
                            arrayString.Append(thisString);
                            arrayString.Append("\"");
                            first = false;
                        }
                        arrayString.Append("}");

                        sortedItems[thisIndex - 1] = arrayString.ToString();
                    }
                    else
                    {
                        //Need special handling for the text arrays to split them out into string format correctly
                        if (colType == "text[]")
                        {
                            StringBuilder arrayString = new StringBuilder("{");
                            bool first = true;
                            foreach (string thisString in value as string[])
                            {
                                if (!first)
                                    arrayString.Append(", ");
                                arrayString.Append("\"");
                                arrayString.Append(thisString);
                                arrayString.Append("\"");
                                first = false;
                            }
                            arrayString.Append("}");

                            sortedItems[thisIndex - 1] = arrayString.ToString();
                        }
                        else
                            sortedItems[thisIndex - 1] = value;
                    }
                }

                for (int k = 0; k < row.ItemArray.Length; k++)
                {
                    if (isInsert)
                    {
                        if (k != 0)
                            insertQuery.Append(", ");

                        if (sortedItems[k].GetType() == typeof(DBNull))
                        {
                            insertQuery.Append("null");
                        }
                        else
                        {
                            insertQuery.Append("'");
                            insertQuery.Append(sortedItems[k].ToString());
                            insertQuery.Append("'");
                        }
                    }
                    else
                    {
                        if (k != 0)
                            updateQuery.Append(", ");

                        if (sortedItems[k].GetType() == typeof(DBNull))
                        {
                            updateQuery.Append("null");
                        }
                        else
                        {
                            updateQuery.Append("'");
                            updateQuery.Append(sortedItems[k].ToString());
                            updateQuery.Append("'");
                        }
                    }

                }

                if (isInsert)
                {
                    insertQuery.Append("); ");
                }
                else
                {
                    //Remember, in an update we must fulfill the WHERE clause's GUID request, so we tack on the GUID here in the final value location.
                    updateQuery.Append(", '");
                    updateQuery.Append(uidString);
                    updateQuery.Append("'); ");
                }
            }

            insertQuery.Append(" DEALLOCATE insertPlan;");
            updateQuery.Append(" DEALLOCATE updatePlan;");

            int totalChanges = 0;

            if (inserts.Count > 0)
            {
                NpgsqlCommand insertCommand = new NpgsqlCommand(insertQuery.ToString(), targetConn);
                totalChanges += insertCommand.ExecuteNonQuery();
            }

            if (updates.Count > 0)
            {
                NpgsqlCommand updateCommand = new NpgsqlCommand(updateQuery.ToString(), targetConn);
                totalChanges += updateCommand.ExecuteNonQuery();
            }

            return totalChanges;
        }

        private static Dictionary<string, Dictionary<string, Dictionary<string, string>>> newTablesColNamesTypes = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();

        public static void MirrorTable(string intSourceTableName, string intTargetTableName, NpgsqlConnection sourceConn,
            NpgsqlConnection targetConn, bool runSubclassRefUpdate, Dictionary<string,string> indexSourceToTarget)
        {
            //If the connection string isn't registered in newTablesColNamesTypes, set it up
            if (!newTablesColNamesTypes.Keys.Contains(targetConn.ConnectionString))
                newTablesColNamesTypes.Add(targetConn.ConnectionString, new Dictionary<string, Dictionary<string, string>>());

            //If the target table name isn't registered in newTablesColNamesTypes (hint: it shouldn't be), set it up.
            if (!newTablesColNamesTypes[targetConn.ConnectionString].Keys.Contains(intTargetTableName))
                newTablesColNamesTypes[targetConn.ConnectionString].Add(intTargetTableName, new Dictionary<string, string>());

            //Add the already-known colnames and types.
            newTablesColNamesTypes[targetConn.ConnectionString][intTargetTableName].Add("guid", "uuid");
            newTablesColNamesTypes[targetConn.ConnectionString][intTargetTableName].Add("row_version", "integer");
            newTablesColNamesTypes[targetConn.ConnectionString][intTargetTableName].Add("version_updated_by", "text");

            //Create the table in the target
            string sql = "CREATE TABLE " + intTargetTableName + " ( " +
                         "guid     uuid PRIMARY KEY," +
                         "row_version     integer DEFAULT '0'," +
                         "version_updated_by     text DEFAULT 'system_maintenance'";

            Dictionary<string, string> namesTypes = PostgreSQLDBEngine.GetTableColumnNamesAndTypesFromInternalName(intSourceTableName, sourceConn, false);

            StringBuilder columnNames = new StringBuilder("");

            foreach (string colName in namesTypes.Keys)
            {
                if (colName == "guid")
                    continue;

                columnNames.Append(", \"");

                if (colName.Contains(':'))
                {
                    string targetNumber = indexSourceToTarget[colName.Split(':')[1]];
                    string newColName = colName.Split(':')[0] + ":" + targetNumber;
                    columnNames.Append(newColName);
                    newTablesColNamesTypes[targetConn.ConnectionString][intTargetTableName].Add(newColName, namesTypes[colName]);
                }
                else
                {
                    columnNames.Append(colName);
                    newTablesColNamesTypes[targetConn.ConnectionString][intTargetTableName].Add(colName, namesTypes[colName]);
                }

                columnNames.Append("\"     ");
                columnNames.Append(namesTypes[colName]);
            }

            sql += columnNames.ToString() + ");";

            NpgsqlCommand command = new NpgsqlCommand(sql, targetConn);
            int created = command.ExecuteNonQuery();

            if (runSubclassRefUpdate)
                PostgreSQLDBEngine.UpdateTableSubclassReferences(targetConn);
        }

        public static int SynchronizeData(DataTable centralData, DataTable clientData, List<string> centralInsertRequests, List<string> centralUpdateRequests,
            List<string> clientInsertRequests, List<string> clientUpdateRequests, string internalCentralTableName, string internalClientTableName,
            NpgsqlConnection centralConn, NpgsqlConnection clientConn, bool centralIsNew, bool clientIsNew, bool tableIsCollection)
        {
            //First we need to get the column name/type pairs.
            #region Column Name/Type pair processing

            //This might be built twice, if both client and central already have the table in their db, since the definitions may differ.
            Dictionary<string, string> centralNamesTypes = null;
            Dictionary<string, string> clientNamesTypes = null;

            List<string> centralColumns = new List<string>();
            List<string> clientColumns = new List<string>();

            //If the db table in question isn't new, get the names/types dictionary and the columns list from normal methods.
            //If it IS new, get the names/types dictionary from the new table uber-dictionary, and build the columns list from it.
            if (!centralIsNew)
            {
                centralNamesTypes = PostgreSQLDBEngine.GetTableColumnNamesAndTypesFromInternalName(internalCentralTableName, centralConn, true);
                centralColumns.AddRange(PostgreSQLDBEngine.GetTableColumnNamesFromInternalName(internalCentralTableName, centralConn, true));
            }
            else
            {
                centralNamesTypes = newTablesColNamesTypes[centralConn.ConnectionString][internalCentralTableName];
                foreach (string colName in centralNamesTypes.Keys)
	            {
                    centralColumns.Add(colName);
	            }
            }
            if (!clientIsNew)
            {
                clientNamesTypes = PostgreSQLDBEngine.GetTableColumnNamesAndTypesFromInternalName(internalClientTableName, clientConn, true);
                clientColumns.AddRange(PostgreSQLDBEngine.GetTableColumnNamesFromInternalName(internalClientTableName, clientConn, true));
            }
            else
            {
                clientNamesTypes = newTablesColNamesTypes[clientConn.ConnectionString][internalClientTableName];
                foreach (string colName in clientNamesTypes.Keys)
                {
                    clientColumns.Add(colName);
                }
            }

            //Now do a fixup on the client/central columns and namestypes lists, in order to purge any columns marked as "NoSynchronize" from the synch.
            //First check the class maps of both local and central and add column names with NoSynch to the no-no list.
            List<string> noSynchColNames = new List<string>();
            if (ClassMap.MapLibrary[centralConn.ConnectionString].Keys.Contains(internalCentralTableName))
            {
                ClassMapNode node = ClassMap.MapLibrary[centralConn.ConnectionString][internalCentralTableName];
                foreach (string colName in node.Columns.Keys)
                {
                    foreach (string paramName in node.Columns[colName].Parameters.Keys)
                    {
                        if (paramName == "NoSynchronize" && node.Columns[colName].Parameters[paramName] == "True")
                        {
                            if (!noSynchColNames.Contains(colName))
                                noSynchColNames.Add(colName);
                        }
                    }
                }
            }
            if (ClassMap.MapLibrary[clientConn.ConnectionString].Keys.Contains(internalClientTableName))
            {
                ClassMapNode node = ClassMap.MapLibrary[clientConn.ConnectionString][internalClientTableName];
                foreach (string colName in node.Columns.Keys)
                {
                    foreach (string paramName in node.Columns[colName].Parameters.Keys)
                    {
                        if (paramName == "NoSynchronize" && node.Columns[colName].Parameters[paramName] == "True")
                        {
                            if (!noSynchColNames.Contains(colName))
                                noSynchColNames.Add(colName);
                        }
                    }
                }
            }
            
            //Now remove the offending column names from the client and server colname lists.
            List<string> clientColumnsToRemove = new List<string>();
            foreach (string colName in clientColumns)
            {
                string strippedName = colName.Split(':')[0];
                if (noSynchColNames.Contains(strippedName))
                {
                    clientColumnsToRemove.Add(colName);
                    clientNamesTypes.Remove(colName);
                }
            }

            List<string> centralColumnsToRemove = new List<string>();
            foreach (string colName in centralColumns)
            {
                string strippedName = colName.Split(':')[0];
                if (noSynchColNames.Contains(strippedName))
                {
                    centralColumnsToRemove.Add(colName);
                    centralNamesTypes.Remove(colName);
                }
            }

            foreach (string colName in clientColumnsToRemove)
            {
                clientColumns.Remove(colName);
            }

            foreach (string colName in centralColumnsToRemove)
            {
                centralColumns.Remove(colName);
            }

            // Only need to build the indexNumbers once, since they're the same on both tables.
            StringBuilder indexNumbers = new StringBuilder("");

            int count = 0;
            if (!centralIsNew)
                count = centralNamesTypes.Count;
            else
                count = clientNamesTypes.Count;

            StringBuilder centralColumnNames = new StringBuilder("");
            StringBuilder centralColumnTypes = new StringBuilder("");
            StringBuilder clientColumnNames = new StringBuilder("");
            StringBuilder clientColumnTypes = new StringBuilder("");

            for (int i = 0; i < count; i++)
            {
                if (i != 0)
                {
                    indexNumbers.Append(", ");
                    centralColumnNames.Append(", ");
                    centralColumnTypes.Append(", ");
                    clientColumnNames.Append(", ");
                    clientColumnTypes.Append(", ");
                }

                indexNumbers.Append("$");
                indexNumbers.Append((i+1).ToString());

                centralColumnTypes.Append(centralNamesTypes[centralColumns[i]]);
                clientColumnTypes.Append(clientNamesTypes[clientColumns[i]]);

                centralColumnNames.Append("\"");
                centralColumnNames.Append(centralColumns[i]);
                centralColumnNames.Append("\"");

                clientColumnNames.Append("\"");
                clientColumnNames.Append(clientColumns[i]);
                clientColumnNames.Append("\"");
            }

            #endregion

            //Set up the preparation statements
            #region Set up PREPARE statements

            string centralInsertPrepare = "PREPARE insertPlan (" + centralColumnTypes.ToString() + ") AS " +
                                   "INSERT INTO " + internalCentralTableName + " (" + centralColumnNames.ToString() + ") VALUES (" +
                                   indexNumbers.ToString() + ");";

            string centralUpdatePrepare = "PREPARE updatePlan (" + centralColumnTypes.ToString() + ", uuid) AS " +
                                   "UPDATE " + internalCentralTableName + " SET (" + centralColumnNames.ToString() + ") = (" +
                                   indexNumbers.ToString() + ") WHERE guid = $" + (count + 1) + ";";

            string clientInsertPrepare = "PREPARE insertPlan (" + clientColumnTypes.ToString() + ") AS " +
                                   "INSERT INTO " + internalClientTableName + " (" + clientColumnNames.ToString() + ") VALUES (" +
                                   indexNumbers.ToString() + ");";

            string clientUpdatePrepare = "PREPARE updatePlan (" + clientColumnTypes.ToString() + ", uuid) AS " +
                                   "UPDATE " + internalClientTableName + " SET (" + clientColumnNames.ToString() + ") = (" +
                                   indexNumbers.ToString() + ") WHERE guid = $" + (count + 1) + ";";

            StringBuilder centralInsertQuery = new StringBuilder("");
            StringBuilder centralUpdateQuery = new StringBuilder("");
            StringBuilder clientInsertQuery = new StringBuilder("");
            StringBuilder clientUpdateQuery = new StringBuilder("");

            centralInsertQuery.Append(centralInsertPrepare);
            centralUpdateQuery.Append(centralUpdatePrepare);
            clientInsertQuery.Append(clientInsertPrepare);
            clientUpdateQuery.Append(clientUpdatePrepare);

            centralInsertQuery.Append(" ");
            centralUpdateQuery.Append(" ");
            clientInsertQuery.Append(" ");
            clientUpdateQuery.Append(" ");

            #endregion

            //Set up the queries from the insert and update requests
            #region Query Setup from Insert/Updates

            //We need a boolean override for client/central updates in the situation where there is no client or central update
            //in the request lists, but the update needs to be pushed through anyways as it is meant to be bi-directional per
            //the behavior for collection tables.
            bool updateOverride = false;

            //append the insert requests to their relevant queries.
            foreach (string idString in centralInsertRequests)
            {
                //Fetch from the client to insert into central.
                Guid id = new Guid(idString);
                DataRow clientRow = clientData.Rows.Find(id);

                centralInsertQuery.Append("EXECUTE insertPlan(");

                object[] sortedItems = PostgreSQLDBEngine.GenerateSortedItems(clientRow, null, clientData, null, true, tableIsCollection,
                    centralNamesTypes, centralColumns, indexClientToCentral);

                string queryString = PostgreSQLDBEngine.GenerateQueryString(sortedItems, true, idString);

                centralInsertQuery.Append(queryString);
            }

            foreach (string idString in clientInsertRequests)
            {
                //Fetch from central to insert into the client
                Guid id = new Guid(idString);
                DataRow centralRow = centralData.Rows.Find(id);

                clientInsertQuery.Append("EXECUTE insertPlan(");

                object[] sortedItems = PostgreSQLDBEngine.GenerateSortedItems(centralRow, null, centralData, null, true, tableIsCollection,
                    clientNamesTypes, clientColumns, indexCentralToClient);

                string queryString = PostgreSQLDBEngine.GenerateQueryString(sortedItems, true, idString);

                clientInsertQuery.Append(queryString);
            }

            //In a collection table, both client and central receive the same query string, since the collection is fully merged.
            //We can achieve a small speedup here by only sorting the items once.
            if (tableIsCollection)
            {
                //Note that sometimes, not both client and central actually NEED to be updated, in fact frequently only one should be.
                //TODO : because of this, it can be checked to see if the merged list differs from either/both and skip updates if it doesn't,
                //       just add a pair of ref'ed bools out of sortedItems.  Speedup for this scales directly with the size of the db.
                
                //First, create a merged list of all update requests to drive from.
                List<string> bothUpdateRequests = new List<string>();
                bothUpdateRequests.AddRange(centralUpdateRequests);
                foreach (string idString in clientUpdateRequests)
                {
                    if (!bothUpdateRequests.Contains(idString))
                        bothUpdateRequests.Add(idString);
                }

                foreach (string idString in bothUpdateRequests)
                {
                    Guid id = new Guid(idString);
                    DataRow centralRow = centralData.Rows.Find(id);
                    DataRow clientRow = clientData.Rows.Find(id);

                    clientUpdateQuery.Append("EXECUTE updatePlan(");
                    centralUpdateQuery.Append("EXECUTE updatePlan(");

                    object[] sortedItems = null;

                    if (centralUpdateRequests.Contains(idString) && clientUpdateRequests.Contains(idString))
                    {
                        //UH OH
                        int uhoh = 1;
                    }

                    if (centralUpdateRequests.Contains(idString))
                    {
                        sortedItems = PostgreSQLDBEngine.GenerateSortedItems(clientRow, centralRow, clientData, centralData, false, tableIsCollection,
                            centralNamesTypes, centralColumns, indexClientToCentral);
                    }
                    else if (clientUpdateRequests.Contains(idString))
                    {
                        sortedItems = PostgreSQLDBEngine.GenerateSortedItems(centralRow, clientRow, centralData, clientData, false, tableIsCollection,
                            clientNamesTypes, clientColumns, indexCentralToClient);
                    }

                    string queryString = PostgreSQLDBEngine.GenerateQueryString(sortedItems, false, idString);

                    clientUpdateQuery.Append(queryString);
                    centralUpdateQuery.Append(queryString);

                    updateOverride = true;
                }
            }
            else
            {
                foreach (string idString in clientUpdateRequests)
                {
                    Guid id = new Guid(idString);
                    DataRow centralRow = null;
                    DataRow clientRow = null;
                    if (centralData != null)
                        centralRow = centralData.Rows.Find(id);
                    if (clientData != null)
                        clientRow = clientData.Rows.Find(id);

                    clientUpdateQuery.Append("EXECUTE updatePlan(");

                    object[] sortedItems = PostgreSQLDBEngine.GenerateSortedItems(centralRow, clientRow, centralData, clientData, false, tableIsCollection,
                        clientNamesTypes, clientColumns, indexCentralToClient);

                    string queryString = PostgreSQLDBEngine.GenerateQueryString(sortedItems, false, idString);

                    clientUpdateQuery.Append(queryString);
                }

                foreach (string idString in centralUpdateRequests)
                {
                    Guid id = new Guid(idString);
                    DataRow centralRow = null;
                    DataRow clientRow = null;
                    if (centralData != null)
                        centralRow = centralData.Rows.Find(id);
                    if (clientData != null)
                        clientRow = clientData.Rows.Find(id);

                    centralUpdateQuery.Append("EXECUTE updatePlan(");

                    object[] sortedItems = PostgreSQLDBEngine.GenerateSortedItems(clientRow, centralRow, clientData, centralData, false, tableIsCollection,
                        centralNamesTypes, centralColumns, indexClientToCentral);

                    string queryString = PostgreSQLDBEngine.GenerateQueryString(sortedItems, false, idString);

                    centralUpdateQuery.Append(queryString);
                }
            }

            #endregion

            centralInsertQuery.Append(" DEALLOCATE insertPlan;");
            clientInsertQuery.Append(" DEALLOCATE insertPlan;");

            centralUpdateQuery.Append(" DEALLOCATE updatePlan;");
            clientUpdateQuery.Append(" DEALLOCATE updatePlan;");            

            int totalChanges = 0;

            #region Send Commands to both Databases
            if (centralInsertRequests.Count > 0)
            {
                NpgsqlCommand insertCommand = new NpgsqlCommand(centralInsertQuery.ToString(), centralConn);
                totalChanges += insertCommand.ExecuteNonQuery();
            }
            if (clientInsertRequests.Count > 0)
            {
                NpgsqlCommand insertCommand = new NpgsqlCommand(clientInsertQuery.ToString(), clientConn);
                totalChanges += insertCommand.ExecuteNonQuery();
            }
            if (centralUpdateRequests.Count > 0 || updateOverride)
            {
                NpgsqlCommand updateCommand = new NpgsqlCommand(centralUpdateQuery.ToString(), centralConn);
                totalChanges += updateCommand.ExecuteNonQuery();
            }
            if (clientUpdateRequests.Count > 0 || updateOverride)
            {
                NpgsqlCommand updateCommand = new NpgsqlCommand(clientUpdateQuery.ToString(), clientConn);
                totalChanges += updateCommand.ExecuteNonQuery();
            }
            #endregion

            return totalChanges;
        }

        //The string format that the database accepts for arrays is very specific.  This converts an array of string values into a single DB Array format string.
        public static string GenerateDBArrayString(string[] fromStringArray)
        {
            StringBuilder arrayString = new StringBuilder("{");
            bool first = true;
            foreach (string thisString in fromStringArray)
            {
                if (!first)
                    arrayString.Append(", ");
                arrayString.Append("\"");
                arrayString.Append(thisString);
                arrayString.Append("\"");
                first = false;
            }
            arrayString.Append("}");

            return arrayString.ToString();
        }

        public static object[] GenerateSortedItems(DataRow sourceRow, DataRow targetRow, DataTable sourceTable, DataTable targetTable,
            bool isInsert, bool collectionRow, Dictionary<string, string> targetNamesTypes,
            List<string> targetColumns, Dictionary<string, string> indexSourceToTarget)
        {
            object[] sortedItems = new object[targetNamesTypes.Count];

            for (int j = 0; j < sourceRow.ItemArray.Length; j++)
            {
                object sourceValue = sourceRow.ItemArray[j];
                string sourceColName = sourceTable.Columns[j].Caption;
                string targetColType = "";
                int thisIndex = 0;
                string targetColName = sourceColName;

                //If there is a destination index suffix, use the indexes to convert the name so we can identify the target's internal name
                if (sourceColName.Contains(':'))
                {
                    string newIndex = indexSourceToTarget[sourceColName.Split(':')[1]];
                    string convertedColName = sourceColName.Split(':')[0] + ":" + newIndex;
                    //If the column name has been excised due to NoSynchronize being on, it should be skipped altogether.
                    if (!targetNamesTypes.Keys.Contains(convertedColName))
                        continue;
                    targetColType = targetNamesTypes[convertedColName];
                    thisIndex = targetColumns.IndexOf(convertedColName);
                    targetColName = convertedColName;
                }
                else
                {
                    //If the column name has been excised due to NoSynchronize being on, it should be skipped altogether.
                    if (!targetNamesTypes.Keys.Contains(sourceColName))
                        continue;
                    targetColType = targetNamesTypes[sourceColName];
                    thisIndex = targetColumns.IndexOf(sourceColName);
                }

                //During an update, when the columnType is "text[]" and the table is a collection, we merge rather than overwrite data.
                if (!isInsert && collectionRow && targetColType == "text[]")
                {
                    //Get ahold of the correct data from the target data.
                    object targetValue = targetRow.ItemArray[targetTable.Columns.IndexOf(targetColName)];

                    //Make a list of "merged" values and add both target and source data to it (uniquely)
                    List<string> mergedValues = new List<string>();

                    foreach (string targetString in targetValue as string[])
                    {
                        if (!mergedValues.Contains(targetString))
                            mergedValues.Add(targetString);
                    }

                    foreach (string sourceString in sourceValue as string[])
                    {
                        if (!mergedValues.Contains(sourceString))
                            mergedValues.Add(sourceString);
                    }

                    //Finally, parse the merged values into a DB-compatible array string
                    sortedItems[thisIndex] = PostgreSQLDBEngine.GenerateDBArrayString(mergedValues.ToArray());
                }
                else
                {
                    //Need special handling for the text arrays to split them out into string format correctly
                    if (targetColType == "text[]")
                    {
                        sortedItems[thisIndex] = PostgreSQLDBEngine.GenerateDBArrayString((sourceValue as string[]));
                    }
                    else
                        sortedItems[thisIndex] = sourceValue;
                }
            }

            return sortedItems;
        }

        public static string GenerateQueryString(object[] sortedItems, bool isInsert, string uidString)
        {
            StringBuilder ret = new StringBuilder("");

            for (int k = 0; k < sortedItems.Length; k++)
            {
                if (isInsert)
                {
                    if (k != 0)
                        ret.Append(", ");

                    if (sortedItems[k].GetType() == typeof(DBNull))
                    {
                        ret.Append("null");
                    }
                    else
                    {
                        ret.Append("'");
                        ret.Append(sortedItems[k].ToString());
                        ret.Append("'");
                    }
                }
                else
                {
                    if (k != 0)
                        ret.Append(", ");

                    if (sortedItems[k].GetType() == typeof(DBNull))
                    {
                        ret.Append("null");
                    }
                    else
                    {
                        ret.Append("'");
                        ret.Append(sortedItems[k].ToString());
                        ret.Append("'");
                    }
                }
            }

            if (isInsert)
            {
                ret.Append("); ");
            }
            else
            {
                //Remember, in an update we must fulfill the WHERE clause's GUID request, so we tack on the GUID here in the final value location.
                ret.Append(", '");
                ret.Append(uidString);
                ret.Append("'); ");
            }

            return ret.ToString();
        }
    }
}
