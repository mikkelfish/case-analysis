﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Collections;
using DatabaseUtilityLibrary;

namespace CeresBase.IO.Serialization
{
    public class RelationalTable
    {
        private Type tableType;
        public Type TableType 
        {
            get
            {
                return this.tableType;
            }
            set
            {
                this.tableType = value;
                this.PopulateColumnIdentifiers();
            }
        }

        //private Dictionary<Guid, RelationalRow> rowCache = new Dictionary<Guid, RelationalRow>();
        //private bool cacheCreated = true;
        //public void CreateRowCache()
        //{
        //    rowCache.Clear();
        //    foreach (RelationalRow row in rows)
        //    {
        //        rowCache.Add(row.RowIdentifier, row);
        //    }
        //    cacheCreated = true;
        //}

        //public RelationalRow GetRowFromCache(Guid id)
        //{
        //    if (!cacheCreated) throw new Exception("Cache hasn't been created");
        //    if (!rowCache.ContainsKey(id)) return null;
        //    return rowCache[id];
        //}

        private Dictionary<Guid, RelationalRow> rows = new Dictionary<Guid, RelationalRow>();
        public Dictionary<Guid, RelationalRow> Rows 
        {
            get
            {
                return this.rows;
            }
        }

        private List<string> columnNames;
        public List<string> ColumnNames
        {
            get
            {
                return this.columnNames;
            }
            set
            {
                this.columnNames = value;
            }
        }

        private List<Type> columnTypes;
        public List<Type> ColumnTypes
        {
            get
            {
                return this.columnTypes;
            }
            set
            {
                this.columnTypes = value;
            }
        }

        public void AddColumn(string ColName, Type ColType)
        {
            this.columnNames.Add(ColName);
            this.columnTypes.Add(ColType);
        }

        public void PopulateColumnIdentifiers()
        {
            List<string> newColumnNames = new List<string>();
            List<Type> newColumnTypes = new List<Type>();

            if (!(TranslatorStore.HasTranslator(TableType) 
                || TableType.GetInterfaces().Contains(typeof(System.Runtime.Serialization.ISerializable))))
            {
                PropertyInfo[] infos = RelationalFormatter.GetPropertyInfoForSerialization(this.TableType);

                foreach (PropertyInfo info in infos)
                {
                    newColumnNames.Add(info.Name);
                    newColumnTypes.Add(info.PropertyType);
                }
            }

            this.columnNames = newColumnNames;
            this.columnTypes = newColumnTypes;
        }

        public Type GetTypeFromColumnName(string columnName)
        {
            for (int i = 0; i < columnTypes.Count; i++)
            {
                if (columnNames[i] == columnName)
                    return columnTypes[i];
            }

            return null;
        }

        public static RelationalTable GetTable(List<RelationalTable> tables, Type candidateType)
        {
            foreach (RelationalTable table in tables)
            {
                if (table.TableType == candidateType)
                    return table;
            }

            return null;
        }

        public static RelationalTable AddTableToList(ref List<RelationalTable> tables, Type toAdd)
        {
            RelationalTable newTable = new RelationalTable();
            newTable.TableType = toAdd;
            newTable.PopulateColumnIdentifiers();
            tables.Add(newTable);
            return newTable;
        }
    }
}
