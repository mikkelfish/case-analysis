﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.Serialization
{
    public class RelationalRow
    {
        public Guid RowIdentifier { get; set; }

        private Dictionary<string, object> data = new Dictionary<string, object>();

        public string[] GetKeyNames
        {
            get
            {
                return this.data.Keys.ToArray();
            }
        }

        public object this[string keyName]
        {
            get
            {
                return data[keyName];
            }
            set
            {
                data[keyName] = value;
            }
        }
    }
}
