﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Text.RegularExpressions;

namespace DatabaseUtilityLibrary
{
    public class ClassMapNode
    {
        public string ClassFullName { get; set; }
        public bool ClassIsSubClassed { get; set; }
        public string SubClassedFromFullName { get; set; }
        public Dictionary<string, ClassMapNodeColumn> Columns { get; set; }

        public ClassMapNode()
        {
            this.Columns = new Dictionary<string, ClassMapNodeColumn>();
        }

        public void SetParameter(string columnName, string parameterName, string value)
        {
            if (this.Columns.Keys.Contains(columnName))
            {
                this.Columns[columnName].SetParameter(parameterName, value);
            }
        }

        public string GetParameter(string columnName, string parameterName)
        {
            if (this.Columns.Keys.Contains(columnName))
            {
                return this.Columns[columnName].GetParameter(parameterName);
            }
            return "";
        }

        public bool IsParameterValue(string columnName, string parameterName, string value)
        {
            if (this.Columns.Keys.Contains(columnName))
            {
                return this.Columns[columnName].IsParameterValue(parameterName, value);
            }
            return false;
        }

        public override string ToString()
        {
            StringBuilder ret = new StringBuilder("");

            ret.Append(this.ClassFullName);
            if (this.ClassIsSubClassed)
                ret.Append(":" + this.SubClassedFromFullName);
            ret.Append("{");

            bool first = true;
            foreach (string colName in this.Columns.Keys)
            {
                if (!first)
                    ret.Append("|");

                ClassMapNodeColumn colNode = this.Columns[colName];
                ret.Append(colNode.ColumnName);
                ret.Append(":" + colNode.ColumnTypeFullName);

                foreach (string paramName in colNode.Parameters.Keys)
                {
                    ret.Append(paramName + "=");
                    ret.Append(colNode.Parameters[paramName]);
                }
            }

            ret.Append("}");

            return ret.ToString();
        }

        //Class Map String Format :
        //"ClassFullName:SubClassedFrom{ColumnName:TypeFullName:Param1=Val1:Param2=Val2|ColumnName2:TypeFullName:Param1=Val1}"
        public static ClassMapNode GenerateClassMapNode(string classMapString)
        {
            ClassMapNode ret = new ClassMapNode();
            
            string classString = classMapString.Split('{')[0];
            string columnString = classMapString.Split('{')[1];

            if (classString.Contains(':'))
            {
                ret.ClassFullName = classString.Split(':')[0];
                ret.SubClassedFromFullName = classString.Split(':')[1];
                ret.ClassIsSubClassed = true;
            }
            else
            {
                ret.ClassFullName = classString;
                ret.SubClassedFromFullName = "";
                ret.ClassIsSubClassed = false;
            }

            string patternString = @"([\w\.]*):([\w\.]*)(:([\w\.]*=[\w\.]*))*";
            Match thisMatch = Regex.Match(columnString, patternString);

            while (thisMatch != Match.Empty)
            {
                ClassMapNodeColumn newCol = new ClassMapNodeColumn();

                //First group after identity will be the column name.
                newCol.ColumnName = thisMatch.Groups[1].Value;
                //Second group is the type name.
                newCol.ColumnTypeFullName = thisMatch.Groups[2].Value;
                //Fourth group carries parameters - there will be 0 to N matches here.
                for (int i = 0; i < thisMatch.Groups[4].Captures.Count; i++)
                {
                    string pair = thisMatch.Groups[4].Captures[i].Value;
                    newCol.SetParameter(pair.Split('=')[0], pair.Split('=')[1]);
                }

                ret.Columns.Add(newCol.ColumnName, newCol);

                thisMatch = thisMatch.NextMatch();
            }

            return ret;
        }

        private static Dictionary<Type, PropertyInfo[]> propCache = new Dictionary<Type, PropertyInfo[]>();

        public static ClassMapNode GenerateClassMapNode(Type typeName)
        {
            ClassMapNode ret = new ClassMapNode();

            ret.ClassFullName = typeName.FullName;
            if (typeName.BaseType.FullName == "System.Object")
            {
                ret.ClassIsSubClassed = false;
                ret.SubClassedFromFullName = "";
            }
            else
            {
                ret.ClassIsSubClassed = true;
                ret.SubClassedFromFullName = typeName.BaseType.FullName;
            }

            PropertyInfo[] infos = null;

            if (propCache.ContainsKey(typeName))
            {
                infos = propCache[typeName];
            }
            else
            {
                infos = typeName.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                propCache.Add(typeName, infos);
            }

            //Cut out properties based on several criteria
            foreach (PropertyInfo info in infos)
            {
                //Non-readable means ignore it
                if (!info.CanRead)
                    continue;

                //NonSerializedAttribute means ignore it
                object[] attributes = info.GetCustomAttributes(typeof(NonSerializedAttribute), false);
                if (attributes.Count() != 0)
                    continue;

                //DesignerSerializationVisibility of Hidden means ignore it.
                attributes = info.GetCustomAttributes(typeof(System.ComponentModel.DesignerSerializationVisibilityAttribute), false);
                if (attributes.Count() == 1)
                {
                    System.ComponentModel.DesignerSerializationVisibilityAttribute attr = attributes[0] as System.ComponentModel.DesignerSerializationVisibilityAttribute;
                    if (attr.Visibility == System.ComponentModel.DesignerSerializationVisibility.Hidden)
                        continue;
                }

                //Not writeable means ignore it, unless it's a collection - collections don't need to be writeable.
                MethodInfo ninfo = info.GetSetMethod(false);
                if (!info.CanWrite || ninfo == null)
                {
                    if (!((info.PropertyType.GetInterfaces().Contains(typeof(IDictionary))) ||
                            (info.PropertyType.GetInterfaces().Contains(typeof(IList)))))
                        continue;
                }

                //Finally, we get to the part where the property has successfully passed all of the ignoring criteria and a column can be constructed.
                ClassMapNodeColumn newCol = new ClassMapNodeColumn(info.Name, info.PropertyType.FullName);
                ret.Columns.Add(info.Name, newCol);
            }

            return ret;
        }
    }

    public class ClassMapNodeColumn
    {
        public string ColumnName { get; set; }
        public string ColumnTypeFullName { get; set; }
        public Dictionary<string,string> Parameters { get; set; }

        public ClassMapNodeColumn()
        {
            this.Parameters = new Dictionary<string, string>();
        }

        public ClassMapNodeColumn(string colName, string colTypeFullName)
        {
            this.ColumnName = colName;
            this.ColumnTypeFullName = colTypeFullName;
            this.Parameters = new Dictionary<string, string>();
        }

        public void SetParameter(string parameterName, string value)
        {
            if (Parameters.Keys.Contains(parameterName))
                Parameters[parameterName] = value;
            else
                Parameters.Add(parameterName, value);
        }

        public string GetParameter(string parameterName)
        {
            if (Parameters.Keys.Contains(parameterName))
                return Parameters[parameterName];
            return "";
        }

        public bool IsParameterValue(string parameterName, string value)
        {
            if (Parameters.Keys.Contains(parameterName))
                return Parameters[parameterName] == value;
            return false;
        }
    }
}
