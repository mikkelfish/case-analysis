﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;
using System.Collections.ObjectModel;

namespace DatabaseUtilityLibrary
{
    public static class TranslatorStore
    {
        private static ObservableCollection<ITranslator> translators = new ObservableCollection<ITranslator>();

        static TranslatorStore()
        {
            ITranslator[] trans = MesosoftCommon.Utilities.Reflection.Common.CreateInterfaces<ITranslator>(null);
            foreach (ITranslator translate in trans)
            {
                translators.Add(translate);
            }
        }

        public static bool HasTranslator(object obj)
        {
            return translators.Any(t => t.Supports(obj));
        }

        public static ITranslator GetTranslator(Type type)
        {
            ITranslator translate = translators.OrderByDescending(t => t.Version).FirstOrDefault(t => t.SupportsType(type));
            if (translate == null)
                return null;

            return translate.Clone() as ITranslator;
        }
    }
}
