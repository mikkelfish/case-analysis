﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Reflection;

namespace CeresBase.IO.Serialization.Translation
{
    public interface ITranslator : ISerializable, ICloneable
    {
        object AttachedObject { get; set; }
        //Type TypeToTranslate { get;  }

        bool SupportsType(Type t);

        double Version { get; }
        bool Supports(object obj);
    }
}
