﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using ApolloFinal.UI.Scope;
using CeresBase.IO.Serialization;

namespace NewHostingEngine
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            this.Exit += new ExitEventHandler(App_Exit);
            Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;

            base.OnStartup(e);
         
            ScopeWindow win = new ScopeWindow();
            win.Disposed += new EventHandler(win_Disposed);
            win.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            try
            {
                win.Show();
            }
            catch
            {
                NewSerializationCentral.Exit();
                this.Shutdown();

            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }

        void App_Exit(object sender, ExitEventArgs e)
        {
            int s = 0;
        }

        void win_Disposed(object sender, EventArgs e)
        {
            this.Shutdown();
        }
    }
}
