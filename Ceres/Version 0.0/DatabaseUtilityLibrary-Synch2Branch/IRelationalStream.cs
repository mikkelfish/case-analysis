﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace CeresBase.IO.Serialization
{
    public interface IRelationalStream
    {
        void WriteTables(RelationalTable[] tables);

        RelationalTable[] ReadTables();

        Type[] SupportedTypes { get; }

        bool NativelyStorable(Type type);

        RelationalFilter Filter { get; set; }

        void BeginTransaction();
        void EndTransaction();
    }
}
