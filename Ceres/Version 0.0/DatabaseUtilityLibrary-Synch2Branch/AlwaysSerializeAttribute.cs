﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DatabaseUtilityLibrary
{
    [AttributeUsage(AttributeTargets.Class)]
    public class AlwaysSerializeAttribute : Attribute
    {
    }
}
