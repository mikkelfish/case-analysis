﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Reflection;
using System.Collections;
using CeresBase.IO.Serialization.Translation;
using DatabaseUtilityLibrary;

namespace CeresBase.IO.Serialization
{

        //return new RelationalFormatterHelper() { PropertyName = "DictionaryItems", Value = new object[] { keyObjs.ToArray(), itemsObjs.ToArray() } };
        //}

        //public static RelationalFormatterHelper WriteList(IList list)
        //{
        //    List<object> items = new List<object>();
        //    foreach (object item in list)
        //    {
        //        items.Add(item);
        //    }

        //return new RelationalFormatterHelper() { PropertyName = "ListItems", Value = items.ToArray() };

    public class RelationalFormatter : IFormatter
    {
        //static RelationalFormatter()
        //{
        //    SerializationCentral.DatabaseChanging += new DatabaseChangingEventHandler(SerializationCentral_DatabaseChanging);
        //}

        //static void SerializationCentral_DatabaseChanging(object sender, DatabaseChangingEventArgs e)
        //{
        //    propCache.Clear();
        //    interfaceCache.Clear();
        //    translatorConstructors.Clear();
        //    serializedProps.Clear();
        //}

        public static void ClearRelationalFormatter()
        {
            propCache.Clear();
            interfaceCache.Clear();
            translatorConstructors.Clear();
            serializedProps.Clear();
        }

        private static Dictionary<Type, PropertyInfo[]> propCache = new Dictionary<Type, PropertyInfo[]>();
        private static Dictionary<Type, Type[]> interfaceCache = new Dictionary<Type, Type[]>();
        private static Dictionary<Type, ConstructorInfo> translatorConstructors = new Dictionary<Type, ConstructorInfo>();
        private static Dictionary<Type, PropertyInfo[]> serializedProps = new Dictionary<Type, PropertyInfo[]>();


        private interface IFormatterWrapper
        {
            string Name { get; set; }
            object Object { get; set; }
        }

        private class formatterWrapper<T> : IFormatterWrapper
        {
            public string Name { get; set; }
            public T Object { get; set; }

            object IFormatterWrapper.Object
            {
                get
                {
                    return this.Object;
                }

                set
                {
                    this.Object = (T)value;
                }
            }
        }

        #region IFormatter Members

        private bool forceRefresh = false;
        private bool compareAndUpdateCollections = false;

        object IFormatter.Deserialize(System.IO.Stream serializationStream)
        {
            return this.Deserialize(serializationStream);
        }

        public static TimeSpan trDeserializeReadTables = new TimeSpan();
        public static TimeSpan trDeserializeReconstructTables = new TimeSpan();
        public static TimeSpan trDeserializeFindAndRecurse = new TimeSpan();
        public static TimeSpan trDeserializeFindObject = new TimeSpan();
        public static TimeSpan trDeserializeReflect = new TimeSpan();



        public object[] Deserialize(System.IO.Stream serializationStream)
        {
            if (!(serializationStream is IRelationalStream))
                throw new Exception("Stream passed into the Deserialize method of a RelationalFormatter was not an IRelationalStream.");

            IRelationalStream relationalStream = serializationStream as IRelationalStream;

            Type[] supportedTypes = relationalStream.SupportedTypes;
            if (!(supportedTypes.Contains(typeof(System.Collections.IList)) && supportedTypes.Contains(typeof(System.Collections.IDictionary))))
                throw new Exception("Support for streams that do not support IList and IDictionary types is not implemented.");

            this.forceRefresh = relationalStream.Filter.ForceRefresh;



            DateTime now = DateTime.Now;
            RelationalTable[] tables = null;
           relationalStream.BeginTransaction();
                tables = relationalStream.ReadTables();
                relationalStream.EndTransaction();

            trDeserializeReadTables = trDeserializeReadTables.Add(DateTime.Now.Subtract(now));

            object[] ret = null;

            now = DateTime.Now;
            //We want to acquire objects that are of the type requested in the stream filter.
            //Other tables are simply sub-tables of the requested type and will be parsed into the object by recursive calls.

            foreach (RelationalTable relTable in tables)
            {
                if (relTable.TableType == relationalStream.Filter.TargetClass)
                {
                    ret = this.readTable(relTable, tables);
                    break;
                }
            }


            trDeserializeReconstructTables = trDeserializeReconstructTables.Add(DateTime.Now.Subtract(now));

            trDeserializeReadTables = new TimeSpan();
            trDeserializeReconstructTables = new TimeSpan();
            trDeserializeFindAndRecurse = new TimeSpan();
            trDeserializeFindObject = new TimeSpan();
            trDeserializeReflect = new TimeSpan();
            return ret;
        }


        private object[] readTable(RelationalTable table, RelationalTable[] tableList)
        {
            List<object> ret = new List<object>();

            foreach (RelationalRow row in table.Rows.Values)
            {
                ret.Add(this.readRow(row, table, tableList, table.TableType));
            }

            return ret.ToArray();
        }

        private object findAndRecurse(RelationalTable[] tableList, Type returnType, Guid newValue)
        {
            
            if (returnType == typeof(object))
            {
                returnType = null;
            }


            //Find the GUID in the rows of the other tables.
            foreach (RelationalTable subTable in tableList)
            {
                if (subTable.Rows.ContainsKey(newValue))
                {
                    RelationalRow row = subTable.Rows[newValue];
                    return this.readRow(row, subTable, tableList, returnType);
                }
            }


            return null;
        }


        private object readRow(RelationalRow row, RelationalTable table, RelationalTable[] tableList, Type returnType)
        {
            Type targetType = table.TableType;
            object existingObject = SerializedObjectManager.RetrieveObject(targetType, row.RowIdentifier);

            if (existingObject != null)
            {
                //existingObject = SerializedObjectManager.RetrieveObject(row.RowIdentifier);
                if (!this.forceRefresh 
                    || (!(existingObject is ICollection) && 
                        !(existingObject is RelationalFormatterHelper) && 
                        !existingObject.GetType().Name.Contains("serializationWrapper")))
                {
                    if (existingObject is ITranslator)
                    {
                        if ((existingObject as ITranslator).AttachedObject != null)
                            return (existingObject as ITranslator).AttachedObject;
                    }
                    else
                    {
                        //If the object's already registered, it can be pulled out immediately, without recursing or anything else
                        return existingObject;
                    }
                }

                //If forceRefresh is on, then a request is being made to create new objects across the board, and re-register
                //them in place of the existing object in SerializedObjectManager.  So do not return, and use the existingObject later.
            }
            

            object newObject = null;

            
            if(!interfaceCache.ContainsKey(targetType))
            {
                interfaceCache.Add(targetType, targetType.GetInterfaces());
            }

            Type[] interfaces = interfaceCache[targetType];

            if (targetType == typeof(RelationalFormatterHelper))
            {
                #region Relational Helper

                if (returnType == null)
                {
                    returnType = row["ObjectRealType"] as Type;
                }

                if (returnType.IsArray)
                {
                    //Helpers that wrap an array can't use the standard instantiation (arrays need a size) - so, grab the size
                    int arraySize = (row["Value"] as object[]).Length;
                    newObject = Activator.CreateInstance(returnType, new object[] { arraySize });
                }
                else
                    newObject = Activator.CreateInstance(returnType);

                if ((string)row["PropertyName"] == "DictionaryItems")
                {
                    IDictionary dict = newObject as IDictionary;
                    object[] keysAndItems = row["Value"] as object[];
                    string[] originalTypeNames = row["OriginatingTypeNames"] as string[];

                    Array keys = keysAndItems[0] as Array;
                    Array items = keysAndItems[1] as Array;

                    for (int i = 0; i < keys.Length; i++)
                    {

                        object newKey = keys.GetValue(i);
                        object newItem = items.GetValue(i);
                        string typeNameString = originalTypeNames[i];
                        Type keyType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeNameString.Split(':')[0]);
                        Type itemType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeNameString.Split(':')[1]);

                        if (newKey.GetType() == typeof(Guid))
                        {
                            newKey = this.findAndRecurse(tableList, keyType, (Guid)newKey);
                        }


                        if (newItem.GetType() == typeof(Guid))
                        {
                            newItem = this.findAndRecurse(tableList, itemType, (Guid)newItem);

                        }
                        
                        if(newItem != null)

                        

                        if (newKey != null && newItem != null)
                            dict.Add(newKey, newItem);
                    }

                }
                else if ((string)row["PropertyName"] == "ListItems")
                {
                        IList list = newObject as IList;

                        Array items = row["Value"] as Array;


                        string[] originalTypeNames = row["OriginatingTypeNames"] as string[];
                        //string originalTypeNames = row["OriginatingTypeNames"] as string;

                        if (originalTypeNames == null)
                        {
                            string realString = row["OriginatingTypeNames"] as string;
                            if (realString != null)
                            {
                                originalTypeNames = new string[] { realString };
                            }
                        }

                        if (originalTypeNames != null)
                        {
                            for (int i = 0; i < items.Length; i++)
                            {

                                Type originalType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(originalTypeNames[i]);
                                object value = items.GetValue(i);

                                if (value.GetType() == typeof(Guid))
                                    value = this.findAndRecurse(tableList, originalType, (Guid)value);



                                if (newObject.GetType().IsArray)
                                {
                                    (newObject as Array).SetValue(value, i);

                                }
                                else if (value != null)
                                    list.Add(value);
                            }
                        }
      
                }
                else
                    throw new Exception("Unexpected helper type in deserialization.");

                #endregion
            }
            else if (interfaces.Contains(typeof(ITranslator))) //TranslatorCentral.ContainsTranslatorForType(graph.GetType())
            {
                #region Translator
                if (!translatorConstructors.ContainsKey(targetType))
                {
                    translatorConstructors.Add(targetType, targetType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public,
                             null,
                             new Type[] { typeof(SerializationInfo), typeof(StreamingContext) },
                             null));
                }

                ConstructorInfo cons = translatorConstructors[targetType];


                if (cons == null) throw new Exception("Translator constructor not found in deserialization");


                SerializationInfo info = new SerializationInfo(targetType, new FormatterConverter());
                string[] keyNames = row.GetKeyNames;
                foreach (string keyName in keyNames)
                {
                    object newValue = row[keyName];
                    if (newValue.GetType() == typeof(Guid))
                    {
                        newValue = this.findAndRecurse(tableList, null, (Guid)newValue);
                    }

                    if (newValue == null)
                    {
                        int s = 0;
                    }

                    info.AddValue(keyName, newValue, newValue.GetType());
                }

                ITranslator translator = cons.Invoke(new object[] { info, new StreamingContext() }) as ITranslator;
                newObject = translator.AttachedObject;

                if (newObject == null) return null;

                SerializedObjectManager.RegisterTranslationPair(translator, translator.AttachedObject);
                SerializedObjectManager.RegisterSerializationPair(translator, info);

                //Register top-level object.
                if (existingObject == null)
                    SerializedObjectManager.RegisterObject(row.RowIdentifier, translator);
                else
                    SerializedObjectManager.UpdateRegistration(existingObject, translator);

                SerializedObjectManager.MarkUIDSerialized(row.RowIdentifier);


                return newObject;

                #endregion
            }
            else if (interfaces.Contains(typeof(ISerializable)))
            {
                #region Serializable
                SerializationInfo info = new SerializationInfo(targetType, new FormatterConverter());
                string[] keyNames = row.GetKeyNames;
                foreach (string keyName in keyNames)
                {

                    object newValue = row[keyName];
                    if (newValue.GetType() == typeof(Guid))
                        newValue = this.findAndRecurse(tableList, null, (Guid)newValue);


                    info.AddValue(keyName, newValue, newValue.GetType());

                }


                newObject = targetType.InvokeMember("", BindingFlags.CreateInstance | BindingFlags.Public | BindingFlags.Instance, null, null,
                                                    new object[] { info, new StreamingContext() });
                SerializedObjectManager.RegisterSerializationPair(newObject as ISerializable, info);
                #endregion
            }
            else
            {
                #region Other
                newObject = targetType.InvokeMember("", BindingFlags.CreateInstance | BindingFlags.Public | BindingFlags.Instance, null, null, null);
                string[] keyNames = row.GetKeyNames;
                
                foreach (string keyName in keyNames)
                {

                    if (!table.ColumnNames.Contains(keyName))
                        continue;
                    Type keyType = table.GetTypeFromColumnName(keyName);

                    PropertyInfo pInfo = targetType.GetProperty(keyName);

                    if (pInfo == null)
                        continue;

                    object newValue = row[keyName];

                    //Recurse to fill GUID properties with the proper member
                    Type checkType = pInfo.PropertyType;
                    ITranslator trans = TranslatorStore.GetTranslator(checkType);
                    if (trans != null)
                        checkType = trans.GetType();

                    if (newValue.GetType() == typeof(Guid))
                        newValue = this.findAndRecurse(tableList, checkType, (Guid)newValue);

                    //if (newValue.GetType() != keyType || newValue.GetType() != pInfo.PropertyType)
                    //    throw new Exception("Type mismatch during read.");

                    if (newValue != null)
                    {
                        if (newValue.GetType() == typeof(DBNull))
                            newValue = null;
                    }
                    else
                    {
                        if (checkType != typeof(object) && checkType != null && checkType.GetInterface("ICollection") == null)
                            throw new Exception("There is a bad link in table " + checkType + " finding: " + row[keyName]);
                    }

                    if (newValue != null)
                    {
                        if (pInfo.CanWrite)
                        {
                            if (newValue != null)
                            {
                                if (newValue is char && pInfo.PropertyType == typeof(string))
                                {
                                    newValue = new string(new char[] { (char)newValue });
                                }
                            }

                            pInfo.SetValue(newObject, newValue, null);
                        }
                        else
                        {
                            if (newValue is IList)
                            {
                                IList source = pInfo.GetValue(newObject, null) as IList;
                                foreach (object entry in newValue as IList)
                                {
                                    source.Add(entry);
                                }

                                SerializedObjectManager.UpdateRegistration(newValue, source);
                            }
                            else if (newValue is IDictionary)
                            {
                                IDictionary source = pInfo.GetValue(newObject, null) as IDictionary;
                                foreach (object key in (newValue as IDictionary).Keys)
                                {
                                    if (source.Contains(key))
                                    {
                                        source[key] = (newValue as IDictionary)[key];
                                    }
                                    else source.Add(key, (newValue as IDictionary)[key]);
                                }

                                SerializedObjectManager.UpdateRegistration(newValue, source);

                            }
                            else throw new Exception("Trying to write to a non-writeable property");

                        }
                    }


                }

                #endregion
            }

            //Register top-level object.
            if (existingObject == null)
                SerializedObjectManager.RegisterObject(row.RowIdentifier, newObject);
            else
                SerializedObjectManager.UpdateRegistration(existingObject, newObject);

            SerializedObjectManager.MarkUIDSerialized(row.RowIdentifier);


            return newObject;
        }

        //MBouges: http://www.codeproject.com/KB/cs/objserial.aspx
        //MBouges: see how it has those two functions
        //MBouges: the constructor
        //MBouges: and the getobjectdata
        //MBouges: will that work?
        //MBouges: ok so
        //MBouges: all you have to do
        //MBouges: is make sure that the object doesn't implement ISerializable irst
        //MBouges: if it does then call get object info and skip your recursive thing
        //MBouges: and instead read from the serializationinfo
        //MBouges: to insert things into the db
        //CruxDesideratum: gotcha
        //MBouges: if it doesn't, then ask the TranslatorCentral
        //MBouges: for a translator
        //MBouges: and if you get one
        //MBouges: call that GetObjectData
        //MBouges: do the same thing
        //MBouges: if not then use this
        //MBouges: what you have
        //CruxDesideratum: so getting the data from the properties
        //CruxDesideratum: is only for
        //CruxDesideratum: Doesn't have a translator
        //CruxDesideratum: doesn't implement ISerializable
        //MBouges: right
        //CruxDesideratum: the SerializationInfo from ISerializable is just a list of name and propertyname pairs of stuff you want to serialize
        //CruxDesideratum: and the translator from translatorcentral gives me a SerializationInfo when I call GetObjectData?
        //CruxDesideratum: or rather it fills the serializationInfo
        //MBouges: the translator implements ISerializable
        //MBouges: like id' make an abstract base class
        //MBouges: SerializationTranslator or whatever
        //CruxDesideratum: ahh I see
        //MBouges: then have that implement it abstractly
        //MBouges: and have a constructor that wraps around an object
        //CruxDesideratum: so the Translator *is* pretty much the uh, Serializer things we were using, just centralized and significantly less ghetto
        //MBouges: and a property that returns the types it works on
        //MBouges: yeah pretty much
        //CruxDesideratum: okay
        //MBouges: except they don't use properties exclusively
        //CruxDesideratum: that's why I got confused when I mentioned those and you were like "no way"
        //CruxDesideratum: gotcha
        //MBouges: well they are different
        //MBouges: because it's structured
        //MBouges: and it's not jsut reading properties
        //CruxDesideratum: alright
        //MBouges: because it allows for side effects and stuff



        //        MBouges: i think the right thing to do is
        //MBouges: a) see if the object itself is ISerializable
        //MBouges: b) if not see if there is a translator available
        //MBouges: and the translators implmeent ISerializable
        //MBouges: and will have a standard thing to attach the object to it
        //MBouges: c) if not a or b, use the properties
        //CruxDesideratum: makes sense
        //CruxDesideratum: if the object is ISerializable what do you do?
        //MBouges: hrm wait i just realized you're doing things differently
        //CruxDesideratum: ?
        //MBouges: well normally it takes a parameter
        //MBouges:    void ISerializable.GetObjectData(SerializationInfo info, StreamingContext ctx) {
        //        // Fill up the SerializationInfo object with the formatted data.
        //        info.AddValue("First_Item", dataItemOne.ToUpper());
        //        info.AddValue("dataItemTwo", dataItemTwo.ToUpper());
        //    }
        //MBouges: the Serializationinfo
        //MBouges: you don't use that right?
        //CruxDesideratum: no
        //CruxDesideratum: I probably could
        //MBouges: although you could always create it
        //CruxDesideratum: especially if I'm supposed to, heh
        //MBouges: just for things that implement iserializable
        //MBouges: yeah
        //CruxDesideratum: well
        //MBouges: and so see it has a name and value
        //CruxDesideratum: the reason I've been relatively "meh whatever" about translation
        //CruxDesideratum: is that 95% of what is complex about my program
        //CruxDesideratum: takes place well after the translation
        //CruxDesideratum: the formatter code is pretty simple
        //MBouges: so you'll have to have an internal class that has a name/value
        //MBouges: pair
        //CruxDesideratum: so I don't think it's a huge leap to implement whatever is the best moving forward
        //MBouges: probably generics
        //MBouges: like my wrapper is
        //CruxDesideratum: *nods*
        //CruxDesideratum: the name won't always be a string, eh?
        //MBouges: i think it will
        //CruxDesideratum: or the generic is for the value
        //MBouges: for the value!
        //CruxDesideratum: okay I wasn't sure if it was for both haha
        //MBouges: so you can have an internal wrapper class
        //MBouges: exactly like i use in the db
        //MBouges: as a hack
        //MBouges: so the rest of your stuff's typing works
        //CruxDesideratum: alrighty
        //MBouges: call GetObjectData 
        //MBouges: then read the SerializationInfo
        //MBouges: and make a wrapper for each one
        //MBouges: each thing in it
        //MBouges: and then those are written instead of the properties
        //CruxDesideratum: gotcha
        //MBouges: that should work!
        //CruxDesideratum: now does the translator use the internal type too, or is that only for ISerializable?
        //MBouges: well
        //MBouges: the translator itself is ISerializable
        //MBouges: i think that's the best way to make it
        //CruxDesideratum: good point
        //MBouges: so the formatter just makes a call to the translator central
        //MBouges: and gets one if available
        //MBouges: actually that might be the first thing to do
        //MBouges: because if there is a translator
        //CruxDesideratum: translator > ISerializable?
        //MBouges: then that becomes the object
        //MBouges: to do everything on
        //CruxDesideratum: *nods*

        private IFormatterWrapper[] SerializationInfoToWrapper(SerializationInfo info)
        {
            List<IFormatterWrapper> wrappers = new List<IFormatterWrapper>();

            foreach (SerializationEntry entry in info)
            {
                IFormatterWrapper wrap = MesosoftCommon.Utilities.Reflection.Common.CreateGenericObject(typeof(formatterWrapper<>),
                                                                                         new Type[] { entry.ObjectType }
                                                                                         ) as IFormatterWrapper;
                wrap.Name = entry.Name;
                wrap.Object = entry.Value;

                wrappers.Add(wrap);
            }

            return wrappers.ToArray();
        }

        public static TimeSpan tSerialize_Setup = new TimeSpan();
        public static TimeSpan tSerialize_Formatter = new TimeSpan();
        public static TimeSpan tSerialize_DBEngine = new TimeSpan();
        public static TimeSpan tSerialize_Transactions = new TimeSpan();
        public static TimeSpan tSerialize_WriteTables = new TimeSpan();
        public static TimeSpan tSerialize_FindGuid = new TimeSpan();

        public void Serialize(System.IO.Stream serializationStream, object graph)
        {
            DateTime start = DateTime.Now;
            if (!(serializationStream is IRelationalStream))
                throw new Exception("Stream passed into the Serialize method of a RelationalFormatter was not an IRelationalStream.");

            IRelationalStream relationalStream = serializationStream as IRelationalStream;

            Type[] supportedTypes = relationalStream.SupportedTypes;
            if (!(supportedTypes.Contains(typeof(System.Collections.IList)) && supportedTypes.Contains(typeof(System.Collections.IDictionary))))
                throw new Exception("Support for streams that do not support IList and IDictionary types is not implemented.");

            if (relationalStream.Filter.CompareAndUpdateCollections)
                this.compareAndUpdateCollections = true;

            List<RelationalTable> tables = new List<RelationalTable>();

            RelationalFormatter.classMaps = new Dictionary<string, string>();
            //Guid registeredID = SerializedObjectManager.GetRegisteredID(graph);

            tSerialize_Setup = tSerialize_Setup.Add(DateTime.Now.Subtract(start));
            start = DateTime.Now;

            this.addRowsRecursively(ref tables, graph, relationalStream);
            
            tSerialize_Formatter = tSerialize_Formatter.Add(DateTime.Now.Subtract(start));
            start = DateTime.Now;

            object[] getFlagged = SerializedObjectManager.GetFlaggedObjects();
            foreach (object obj in getFlagged)
            {
                this.addRowsRecursively(ref tables, obj, relationalStream);
            }


            DateTime subs = DateTime.Now;

            relationalStream.BeginTransaction();
            tSerialize_Transactions = tSerialize_Transactions.Add(DateTime.Now.Subtract(subs));
            subs = DateTime.Now;

            relationalStream.WriteTables(tables.ToArray());

            tSerialize_WriteTables = tSerialize_WriteTables.Add(DateTime.Now.Subtract(subs));
            subs = DateTime.Now;

            relationalStream.EndTransaction();
            tSerialize_Transactions = tSerialize_Transactions.Add(DateTime.Now.Subtract(subs));

            tSerialize_DBEngine = tSerialize_DBEngine.Add(DateTime.Now.Subtract(start));

            SerializedObjectManager.ClearUIDInPass();

            tSerialize_DBEngine = new TimeSpan();
            tSerialize_FindGuid = new TimeSpan();
            tSerialize_Formatter = new TimeSpan();
            tSerialize_Setup = new TimeSpan();
            tSerialize_Transactions = new TimeSpan();
            tSerialize_WriteTables = new TimeSpan();

        }

        //Bare ClassMap "Node" :
        //(TableType{PropertyName:Type|PropertyName2:Type2...})
        //
        //Constructed ClassMap "Node" : 
        //(Parent.Property:TableType{PropertyName:Type|PropertyName2:Type2...})
        //i.e. :
        //
        //(null:ClassA{SubClass:SCA|SubClass2:SCB})(ClassA.SubClass:SCA{})(ClassA.SubClass2:SCB{})
        //
        //Base table is stored in db, constructed class map is created when using the maps to guide class navigation (such as in join chain).

        private static Dictionary<string, string> classMaps = new Dictionary<string, string>();

        public void ConstructClassMap(ref List<RelationalTable> tables, object graph, IRelationalStream stream)
        {

        }


        private Guid addRowsRecursively(ref List<RelationalTable> tables, object graph, IRelationalStream stream)
        {

            object originalGraph = graph;

            PropertyInfo[] infos = null;
            IFormatterWrapper[] wrappers = null;
            
            if (TranslatorStore.HasTranslator(graph)) 
            {

                ITranslator translator = SerializedObjectManager.GetTranslator(graph);
                if(translator == null)
                {
                    translator = TranslatorStore.GetTranslator(graph.GetType());
                    SerializedObjectManager.RegisterTranslationPair(translator, graph);
                }
                translator.AttachedObject = graph;
                graph = translator;
                
            }

            Type graphType = graph.GetType();

            Type[] interfaces = null;
            if (interfaceCache.ContainsKey(graphType))
            {
                interfaces = interfaceCache[graphType];
            }
            else
            {
                interfaces = graphType.GetInterfaces();
                interfaceCache.Add(graphType, interfaces);
            }

            if (interfaces.Contains(typeof(ISerializable)))
            {
                ISerializable ser = graph as ISerializable;
                SerializationInfo serInfo = new SerializationInfo(graphType, new FormatterConverter());
                SerializationInfo lookedUp = SerializedObjectManager.GetSerializationInfo(graph as ISerializable);
                StreamingContext ctx = new StreamingContext(StreamingContextStates.All, lookedUp);

                ser.GetObjectData(serInfo, ctx);
                wrappers = SerializationInfoToWrapper(serInfo);


                if (wrappers.Length == 0) return Guid.Empty;
            }
            else
            {

                infos = RelationalFormatter.GetPropertyInfoForSerialization(graphType);

            }





            //This should never be called on a simple type - make sure the graph is NOT on the supportedTypes list, since it won't have
            //any table to be stored in the relational structure.

            //Commenting this out for speed
            //if (wrappers == null && stream.SupportedTypes.Contains(graph.GetType()))
            //    throw new Exception("Can't use a supported type as the root serialization object for a row/table!");
             RelationalTable targetTable;


            //Add the table type if necessary
             targetTable = RelationalTable.GetTable(tables, graphType);
            if (targetTable == null)
                targetTable = RelationalTable.AddTableToList(ref tables, graphType);

            RelationalRow newRow = new RelationalRow();
           
            Guid id = SerializedObjectManager.GetRegisteredID(graph);

            if (originalGraph.GetType().FullName.Contains("User"))
            {
                int s = 0;
            }

            bool wasNew = false;
            if (id == Guid.Empty)
            {
                newRow.RowIdentifier = Guid.NewGuid();
                wasNew = true;
            }
            else
            {
                if (!(originalGraph is RelationalFormatterHelper) &&
                    !originalGraph.GetType().Name.Contains("serializationWrapper") &&
                    !(originalGraph is ICollection) &&
                    !SerializedObjectManager.GetFlag(originalGraph))
                    return id;
                newRow.RowIdentifier = id;
                SerializedObjectManager.FlagUpdate(originalGraph, false);
            }


            if (SerializedObjectManager.ContainsUIDInPass(newRow.RowIdentifier))
            {
                if (wasNew) throw new InvalidOperationException("Um there is a problem");
                return newRow.RowIdentifier;
            }

            if (!RelationalFormatter.classMaps.Keys.Contains(targetTable.TableType.FullName))
                RelationalFormatter.classMaps.Add(targetTable.TableType.FullName, "");

            string propertyLinksString = "";

            if (infos != null)
            {
                int infoIndex = 0;
                foreach (PropertyInfo info in infos)
                {
                    if (info.GetIndexParameters().Length > 0) continue;
                    string key = info.Name;
                    object obj = info.GetValue(graph, null);

                    if (targetTable.TableType == typeof(RelationalFormatterHelper) && key == "OriginatingTypeNames")
                        continue;

                    //REMOVING DEBUG
                    //if (!targetTable.ColumnNames.Contains(key))
                    //    throw new Exception("Serialization failed in RelationalFormatter : Graph-Table mismatch.");

                    if (obj != null)
                    {
                        if ((targetTable.TableType == typeof(RelationalFormatterHelper)) && (key == "OriginalReference"))
                            newRow[key] = obj;
                        else
                        {
                            object toPass = graph;
                            if (graph is RelationalFormatterHelper)
                            {
                                toPass = (graph as RelationalFormatterHelper).OriginalReference;
                            }


                            if (obj.GetType().IsArray)
                            {
                                if (TranslatorStore.HasTranslator(obj))
                                {

                                    ITranslator translator = SerializedObjectManager.GetTranslator(obj);
                                    if (translator == null)
                                    {
                                        translator = TranslatorStore.GetTranslator(obj.GetType());
                                        SerializedObjectManager.RegisterTranslationPair(translator, obj);
                                    }
                                    translator.AttachedObject = obj;
                                    obj = translator;

                                }
                            }

                                

                            //at this point we have to wrap up arrays in a list so they can be stored correctly - but only if the current object isn't a helper,
                            //as their arrays should be deliberately exempted to avoid infinite loops.
                            if (targetTable.TableType != typeof(RelationalFormatterHelper) && obj.GetType().IsArray)
                            {
                                newRow[key] = getRowData(toPass, stream, WriteList(obj as IList), ref tables, 0);
                            }
                            else
                                newRow[key] = getRowData(toPass, stream, obj, ref tables, 0);

                            //Check to see if a translator exists for the data being processed and if so, change the type accordingly in the table definition.
                            if (targetTable.TableType == typeof(RelationalFormatterHelper) && key == "Value")
                            {

                                //Now this is necessary
                                string[] typeNames = (graph as RelationalFormatterHelper).OriginatingTypeNames;

                                if ((graph as RelationalFormatterHelper).OriginalReference is IList)
                                {
                                    object[] valList = obj as object[];

                                    for (int valIndex = 0; valIndex < (obj as object[]).Length; valIndex++)
                                    {
                                        object thisVal = valList[valIndex];
                                        if (TranslatorStore.HasTranslator(thisVal))
                                        {
                                            ITranslator translator = TranslatorStore.GetTranslator(thisVal.GetType());
                                            if (translator != null)
                                                typeNames[valIndex] = translator.GetType().FullName;
                                        }
                                    }
                                }
                                else
                                {
                                    object[] keys = (obj as object[])[0] as object[];
                                    object[] values = (obj as object[])[1] as object[];
                                    for (int valIndex = 0; valIndex < keys.Length; valIndex++)
                                    {
                                        object thisKey = keys[valIndex];
                                        object thisVal = values[valIndex];

                                        string[] types = (graph as RelationalFormatterHelper).OriginatingTypeNames[valIndex].Split(':');
                                        if (TranslatorStore.HasTranslator(thisKey))
                                        {
                                            ITranslator translator = TranslatorStore.GetTranslator(thisKey.GetType());
                                            if (translator != null)
                                                types[0] = translator.GetType().FullName;
                                        }

                                        if (TranslatorStore.HasTranslator(thisVal))
                                        {
                                            ITranslator translator = TranslatorStore.GetTranslator(thisVal.GetType());
                                            if (translator != null)
                                                types[1] = translator.GetType().FullName;
                                        }

                                        typeNames[valIndex] = types[0] + ":" + types[1];
                                    }
                                }

                                newRow["OriginatingTypeNames"] = typeNames.Cast<object>().ToArray();
                            }
                            else
                            {
                                if (TranslatorStore.HasTranslator(obj))
                                {
                                    ITranslator translator = TranslatorStore.GetTranslator(obj.GetType());
                                    if (infos.Length != targetTable.ColumnTypes.Count)
                                        throw new Exception("Fatal mismatch between number of types in table and propertytypes");
                                    if(targetTable.ColumnTypes[infoIndex] != typeof(object))
                                        targetTable.ColumnTypes[infoIndex] = translator.GetType();
                                }
                            }
                            //now double back and re-assign the key if the propertyType is object and GUID - we need to change it to a string 
                            //to keep track of what table it belongs to.
                            if (info.PropertyType == typeof(object) && newRow[key].GetType() == typeof(Guid))
                            {
                                if (TranslatorStore.HasTranslator(obj))
                                {
                                    ITranslator translator = TranslatorStore.GetTranslator(obj.GetType());
                                    newRow[key] = newRow[key].ToString() + ":" + translator.GetType().FullName;
                                }
                                else newRow[key] = newRow[key].ToString() + ":" + obj.GetType().FullName;
                            }
                        }
                    }

                    infoIndex++;
                }
            }
            else if (wrappers != null)
            {
                int infoIndex = 0;
                foreach (IFormatterWrapper wrapper in wrappers)
                {
                    string key = wrapper.Name;
                    object obj = wrapper.Object;
                    //In an ISerializable, it's not really feasible to get the columns beforehand, so put them in here.
                    if (!targetTable.ColumnNames.Contains(key))
                    {
                        targetTable.AddColumn(key, obj.GetType());
                        //throw new Exception("Serialization failed in RelationalFormatter : Graph-Table mismatch.");
                    }

                    if (obj != null)
                    {
                        if ((targetTable.TableType == typeof(RelationalFormatterHelper)) && (key == "OriginalReference"))
                            newRow[key] = obj;
                        else
                        {                            
                            //at this point we have to wrap up arrays in a list so they can be stored correctly - but only if the current object isn't a helper,
                            //as their arrays should be deliberately exempted to avoid infinite loops.
                            if (targetTable.TableType != typeof(RelationalFormatterHelper) && obj.GetType().IsArray)
                                newRow[key] = getRowData(wrapper.Object, stream, WriteList(obj as IList), ref tables, 0);
                            else
                                newRow[key] = getRowData(wrapper.Object, stream, obj, ref tables, 0);

                            //Check to see if a translator exists for the data being processed and if so, change the type accordingly in the table definition.
                            if (TranslatorStore.HasTranslator(obj))
                            {
                                ITranslator translator = TranslatorStore.GetTranslator(obj.GetType());
                                targetTable.ColumnTypes[infoIndex] = translator.GetType();
                            }


                            PropertyInfo pi = wrapper.GetType().GetProperty("Object");


                            //now double back and re-assign the key if the propertyType is object and GUID - we need to change it to a string 
                            //to keep track of what table it belongs to.
                            if (pi.PropertyType == typeof(object) && newRow[key].GetType() == typeof(Guid))
                            {
                                newRow[key] = newRow[key].ToString() + ":" + obj.GetType().FullName;
                            }
                        }
                    }
                }
                infoIndex++;
            }
            else
                throw new Exception("Unexpected empty property identifiers.");

            //
            //if (targetTable.TableType == typeof(RelationalFormatterHelper))
            //{
            //    object[] typeNames = newRow["OriginatingTypeNames"] as object[];
            //    for (int i = 0; i < typeNames.Length; i++)
            //    {
            //        Type toTransType = MesosoftCommon.Utilities.Reflection.RetrieveLoadedTypes.GetType(typeNames[i] as string);

            //        ITranslator trans = SerializationCentral.GetTranslator(toTransType);

            //        if (trans != null)
            //            (newRow["OriginatingTypeNames"] as object[])[i] = trans.GetType().FullName;
            //    }


            //}
            RelationalFormatter.classMaps[targetTable.TableType.FullName] = propertyLinksString;

            targetTable.Rows.Add(newRow.RowIdentifier, newRow);

            //Register top-level object.
            SerializedObjectManager.RegisterObject(newRow.RowIdentifier, graph);

            //If it's already marked, don't change the mark.  Otherwise mark it false (new mark)
            if (!SerializedObjectManager.UIDSerializationStateIsMarked(newRow.RowIdentifier))
                SerializedObjectManager.MarkUIDNonSerialized(newRow.RowIdentifier);

            SerializedObjectManager.MarkUIDInPass(newRow.RowIdentifier);
            return newRow.RowIdentifier;
        }

        private object getArrayData(object root, IRelationalStream stream, object obj, ref List<RelationalTable> tables, int level)
        {
            List<object> ret = new List<object>();

            foreach (object item in (obj as IList))
            {              
                object row = getRowData(root, stream, item, ref tables, level + 1);
                if (row != null)
                {
                    ret.Add(row);
                }
            }

            return ret.ToArray();
        }

        private object getRowData(object root, IRelationalStream stream, object obj, ref List<RelationalTable> tables, int level)
        {
            object ret = null;

            if (obj.GetType().IsArray)
            {
                bool callArray = true;
                if ((root is IList) && level >= 1)
                    callArray = false;
                else if ((root is IDictionary) && level >= 2)
                    callArray = false;

                if (callArray)
                {
                    ret = getArrayData(root, stream, obj, ref tables, level);

                }
                else ret = this.addRowsRecursively(ref tables, obj, stream);
            }
            else if (this.TypeIsSupported(obj.GetType(), stream.SupportedTypes))
            {
                if (TranslatorStore.HasTranslator(obj))
                {
                    ret = this.addRowsRecursively(ref tables, obj, stream);
                }
                else if (stream.NativelyStorable(obj.GetType()))
                {
                    //serialize directly
                    ret = obj;
                }
                else
                {
                    //Dictionary<Guid, object> containedObjects = stream.HandleSpecialStorage(ref tables, obj, info, targetTable, ref newRow);
                    //There are a few types that might not be natively storable, run through them here:
                    //To make things simple, translate any non-native storage to external "special table" setups
                    //IList
                    if (obj is IList)
                    {
                        ret = this.addRowsRecursively(ref tables, WriteList(obj as IList), stream);
                    }
                    //IDictionary
                    if (obj is IDictionary)
                    {
                        ret = this.addRowsRecursively(ref tables, WriteDictionary(obj as IDictionary),
                                                            stream);
                    }
                    ////Enum
                    //if (infoType.IsEnum)
                    //{

                    //}
                }
            }
            else
            {
                //recurse and store the Guid... but only if it's not null, obviously
                if (obj != null)
                    ret = this.addRowsRecursively(ref tables, obj, stream);
                if ((Guid)ret == Guid.Empty) return null;
            }

            return ret;
        }


        public static RelationalFormatterHelper WriteDictionary(IDictionary dict)
        {
            List<object> keyObjs = new List<object>();
            foreach (object key in dict.Keys)
            {
                keyObjs.Add(key);
            }

            List<object> itemsObjs = new List<object>();
            foreach (object item in dict.Values)
            {
                itemsObjs.Add(item);
            }

            return new RelationalFormatterHelper() { PropertyName = "DictionaryItems", OriginalReference = dict, Value = new object[] { keyObjs.ToArray(), itemsObjs.ToArray() } };
        }

        public static RelationalFormatterHelper WriteList(IList list)
        {
            List<object> items = new List<object>();
            foreach (object item in list)
            {
                items.Add(item);
            }

            return new RelationalFormatterHelper() { PropertyName = "ListItems", OriginalReference = list, Value = items.ToArray() };
        }

        private bool TypeIsSupported(Type propertyType, Type[] supportedTypes)
        {
            if (supportedTypes.Contains(propertyType))
                return true;

            Type[] interfaces = propertyType.GetInterfaces();


            foreach (Type thisType in supportedTypes)
            {
                if(interfaces.Contains(thisType))
                    return true;
            }

            return false;
        }


        static public PropertyInfo[] GetPropertyInfoForSerialization(Type forType)
        {
            if (serializedProps.ContainsKey(forType))
                return serializedProps[forType];


            PropertyInfo[] infos = null;
            if (propCache.ContainsKey(forType))
            {
                infos = propCache[forType];
            }
            else
            {
                infos = forType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                propCache.Add(forType, infos);
            }

            List<PropertyInfo> ret = new List<PropertyInfo>();

            //Cut out properties based on several criteria
            foreach (PropertyInfo info in infos)
            {
                if (!info.CanRead)
                    continue;

                object[] attributes = info.GetCustomAttributes(typeof(NonSerializedAttribute), false);
                if (attributes.Count() != 0)
                    continue;

                attributes = info.GetCustomAttributes(typeof(System.ComponentModel.DesignerSerializationVisibilityAttribute), false);
                if (attributes.Count() == 1)
                {
                    System.ComponentModel.DesignerSerializationVisibilityAttribute attr = attributes[0] as System.ComponentModel.DesignerSerializationVisibilityAttribute;
                    if (attr.Visibility == System.ComponentModel.DesignerSerializationVisibility.Hidden)
                        continue;
                }

                MethodInfo ninfo = info.GetSetMethod(false);
                

                if (!info.CanWrite || ninfo == null)
                {
                    if (!((info.PropertyType.GetInterfaces().Contains(typeof(IDictionary))) ||
                            (info.PropertyType.GetInterfaces().Contains(typeof(IList)))))
                        continue;
                }

                ret.Add(info);
            }

            serializedProps.Add(forType, ret.ToArray());

            return ret.ToArray();
        }

        /// <summary>
        /// Allows for selection of a specific type for saving/loading, in case they are not exactly analagous.
        /// </summary>
        private SerializationBinder binder;
        public SerializationBinder Binder
        {
            get
            {
                return this.binder;
            }
            set
            {
                this.binder = value;
            }
        }

        /// <summary>
        /// Indicates source/destination of the stream being formatted to allow additional modifications based on that information.
        /// </summary>
        private StreamingContext context = new StreamingContext(StreamingContextStates.All);
        public StreamingContext Context
        {
            get
            {
                return this.context;
            }
            set
            {
                this.context = value;
            }
        }


        /// <summary>
        /// Allows one to define a selector method that can choose an ISerializer to use instead of the supplied serialization in particular cases.
        /// </summary>
        private ISurrogateSelector surrogateSelector;
        public ISurrogateSelector SurrogateSelector
        {
            get
            {
                return this.surrogateSelector;
            }
            set
            {
                this.surrogateSelector = value;
            }
        }

        #endregion
    }
}
