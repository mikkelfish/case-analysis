﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using Npgsql;
using System.Data;

namespace DatabaseUtilityLibrary
{
    public class ClassMap
    {
        public static Dictionary<string, Dictionary<string, ClassMapNode>> MapLibrary { get; set; }

        static ClassMap()
        {
            ClassMap.MapLibrary = new Dictionary<string, Dictionary<string, ClassMapNode>>();
        }

        public static void LoadClassMaps(NpgsqlConnection conn)
        {
            if (!MapLibrary.Keys.Contains(conn.ConnectionString))
                MapLibrary.Add(conn.ConnectionString, new Dictionary<string, ClassMapNode>());


            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da;

            string sql = "SELECT table_id, map_string FROM class_maps;";

            da = new NpgsqlDataAdapter(sql, conn);

            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                string internalTableName = "table_" + row.ItemArray[0].ToString();
                string mapString = row.ItemArray[1].ToString();

                if (!ClassMap.MapLibrary[conn.ConnectionString].Keys.Contains(internalTableName))
                    ClassMap.MapLibrary[conn.ConnectionString].Add(internalTableName, ClassMapNode.GenerateClassMapNode(mapString));
                else
                    ClassMap.MapLibrary[conn.ConnectionString][internalTableName] = ClassMapNode.GenerateClassMapNode(mapString);
            }
        }
    }
}
