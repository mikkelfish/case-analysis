﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CeresBase.IO.Serialization.Translation;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Collections;
using System.Reflection;
using DatabaseUtilityLibrary;

namespace CeresBase.IO.Serialization
{

    //CruxDesideratum: I'm trying to think how I can forcerefresh without turning off object management
    //CruxDesideratum: I can turn off object management only for list types
    //CruxDesideratum: but then if you have a list of lists it is broken again
    //CruxDesideratum: hrm
    //CruxDesideratum: there could be an "already loaded" flag on a per-deserialize basis
    //CruxDesideratum: that would work, right?
    //CruxDesideratum: that way everything is loaded exactly once
    //MBouges: yeah that would work
    //MBouges: so it reloads everything
    //MBouges: but only once per deserializaiton
    //CruxDesideratum: right
    //MBouges: that'd do it

    public static class SerializedObjectManager
    {
        //static SerializedObjectManager()
        //{
        //    SerializationCentral.DatabaseChanging += new DatabaseChangingEventHandler(SerializationCentral_DatabaseChanging);
        //}

        //static void SerializationCentral_DatabaseChanging(object sender, DatabaseChangingEventArgs e)
        //{
        //    uidHasUpdated.Clear();
        //    storedObjects.Clear();
        //    storedTranslationPairs.Clear();
        //    storedSerializationPairs.Clear();
        //    uidIsSerialized.Clear();
        //    uidsInPass.Clear();
        //}

        public static void ClearSerializedObjectManager()
        {
            uidHasUpdated.Clear();
            storedObjects.Clear();
            storedTranslationPairs.Clear();
            storedSerializationPairs.Clear();
            uidIsSerialized.Clear();
            uidsInPass.Clear();
        }


        private static Dictionary<object, bool> uidHasUpdated = new Dictionary<object, bool>();
        private static Dictionary<Type, Dictionary<Guid, object>> storedObjects = new Dictionary<Type, Dictionary<Guid, object>>();
        private static Dictionary<object, ITranslator> storedTranslationPairs = new Dictionary<object, ITranslator>();
        private static Dictionary<ISerializable, SerializationInfo> storedSerializationPairs = new Dictionary<ISerializable, SerializationInfo>();
        private static Dictionary<Guid, bool> uidIsSerialized = new Dictionary<Guid, bool>();
        private static List<Guid> uidsInPass = new List<Guid>();




        //private static void calculateHash(Type t, Guid uid, bool fullCalculation)
        //{
        //    if (!storedObjects.ContainsKey(t) || !storedObjects[t].ContainsKey(uid))
        //        return;

        //    object obj = storedObjects[t][uid];

        //}

        public static void FlagUpdate(object obj, bool setting)
        {
            if (!uidHasUpdated.ContainsKey(obj))
                uidHasUpdated.Add(obj, setting);

            uidHasUpdated[obj] = setting;
            
        }

        public static object[] GetFlaggedObjects()
        {
            return uidHasUpdated.Where( vp => vp.Value == true).Select(vp => vp.Key).ToArray();
        }

        public static bool GetFlag(object obj)
        {
            AlwaysSerializeAttribute attr = MesosoftCommon.Utilities.Reflection.Common.GetAttribute<AlwaysSerializeAttribute>(obj.GetType(), true);

            if (attr != null) return true;

            if (!uidHasUpdated.ContainsKey(obj))
                return false;

            return uidHasUpdated[obj];
        }

        public static bool NeedToUpdate(Guid uid)
        {
            if (!uidHasUpdated.ContainsKey(uid))
                return false;

            return uidHasUpdated[uid];
        }

        public static void MarkUIDInPass(Guid uid)
        {
            uidsInPass.Add(uid);
        }

        public static void ClearUIDInPass()
        {
            uidsInPass.Clear();
        }

        public static bool ContainsUIDInPass(Guid uid)
        {
            return uidsInPass.Contains(uid);
        }


        public static void MarkUIDSerialized(Guid uid)
        {
            uidIsSerialized[uid] = true;
        }

        public static void MarkUIDNonSerialized(Guid uid)
        {
            uidIsSerialized[uid] = false;
        }

        public static bool UIDSerializationStateIsMarked(Guid uid)
        {
            return uidIsSerialized.ContainsKey(uid);
        }

        public static bool IsUIDSerialized(Guid uid)
        {
            return uidIsSerialized[uid];
        }

        public static void RegisterObject(Guid uid, object toStore)
        {
            if (uid.ToString() == "b706fe46-14bd-4b6b-a78f-ed51f74c5bff")
            {
                int s = 0;
            }

            if (toStore is RelationalFormatterHelper)
            {
                toStore = (toStore as RelationalFormatterHelper).OriginalReference;
            }

            Type toFind = toStore.GetType();

            if(!SerializedObjectManager.storedObjects.ContainsKey(toFind))
            {
                SerializedObjectManager.storedObjects.Add(toFind, new Dictionary<Guid, object>());
            }

            if (SerializedObjectManager.storedObjects[toFind].ContainsKey(uid))
            {
                SerializedObjectManager.storedObjects[toFind][uid] = toStore;
            }
            else
            {
                storedObjects[toFind].Add(uid, toStore);
            }
        }

        public static void UpdateRegistration(object oldObject, object newObject)
        {
            Guid keyToUpdate = GetRegisteredID(oldObject);
            if (keyToUpdate == Guid.Empty)
                return;

            SerializedObjectManager.RegisterObject(keyToUpdate, newObject);
        }

        public static object RetrieveObject(Type type, Guid uid)
        {
            if (type == null)
            {

                foreach (Type t in SerializedObjectManager.storedObjects.Keys)
                {
                    if (SerializedObjectManager.storedObjects[t].ContainsKey(uid))
                        return SerializedObjectManager.storedObjects[t][uid];

                }
            }
            else if(SerializedObjectManager.storedObjects.ContainsKey(type) &&
                SerializedObjectManager.storedObjects[type].ContainsKey(uid)) 
                return SerializedObjectManager.storedObjects[type][uid]; 

            

            return null;
        }

        //public static bool IsObjectRegistered(Guid uid)
        //{
        //    if (SerializedObjectManager.storedObjects.ContainsKey(uid))
        //        return true;

        //    return false;
        //}


        public static Guid GetRegisteredID(object obj)
        {
            Guid keyToUpdate = Guid.Empty;

            if (obj is RelationalFormatterHelper)
            {
                obj = (obj as RelationalFormatterHelper).OriginalReference;
            }

            Type objectType = obj.GetType();

            if (!storedObjects.ContainsKey(objectType))
            {
                return Guid.Empty;
            }

            if (objectType.IsValueType)
            {
                keyToUpdate = (from Guid key in storedObjects[objectType].Keys
                                   where storedObjects[objectType][key].Equals(obj)
                                   select key).FirstOrDefault();
            }
            else
            {

                keyToUpdate = (from Guid key in storedObjects[objectType].Keys
                               where ReferenceEquals(storedObjects[objectType][key], obj)
                               select key).FirstOrDefault();
            }

            return keyToUpdate;
        }

        public static void RegisterSerializationPair(ISerializable serializable, SerializationInfo info)
        {
            if (!storedSerializationPairs.ContainsKey(serializable))
            {
                storedSerializationPairs.Add(serializable, info);
            }
        }

        public static SerializationInfo GetSerializationInfo(ISerializable serializable)
        {
            if (!storedSerializationPairs.ContainsKey(serializable)) return null;
            return storedSerializationPairs[serializable];
        }


        public static void RegisterTranslationPair(ITranslator translator, object attachedObject)
        {
            if(!storedTranslationPairs.ContainsKey(attachedObject))
                storedTranslationPairs.Add(attachedObject, translator);
        }


        public static ITranslator GetTranslator(object attachedObject)
        {
            if (!storedTranslationPairs.ContainsKey(attachedObject))
                return null;
            return storedTranslationPairs[attachedObject];
        }
    }
}
