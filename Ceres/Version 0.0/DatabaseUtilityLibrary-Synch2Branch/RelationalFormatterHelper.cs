﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.Serialization
{
    public class RelationalFormatterHelper
    {
        private string propertyName;
        public string PropertyName
        {
            get { return propertyName; }
            set { propertyName = value; }
        }

        private object originalReference;
        public object OriginalReference
        {
            get { return originalReference; }
            set { originalReference = value; }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        public Type ObjectRealType { get; set; }

        private object val;
        public object Value
        {
            get { return val; }
            set
            {
                val = value;

                //Yank the types out
                List<string> ret = new List<string>();
                if (this.PropertyName == "DictionaryItems")
                {
                    if ((value as Array).Length == 2)
                    {
                        for (int i = 0; i < ((value as Array).GetValue(0) as Array).Length; i++)
                        {
                            string toAdd = ((value as Array).GetValue(0) as Array).GetValue(i).GetType().FullName;
                            toAdd += ":";
                            toAdd += ((value as Array).GetValue(1) as Array).GetValue(i).GetType().FullName;
                            ret.Add(toAdd);
                        }
                    }
                    //ret.Add(((value as Array).GetValue(0) as Array).GetValue(0).GetType().FullName);
                    //ret.Add(((value as Array).GetValue(1) as Array).GetValue(0).GetType().FullName);
                }
                else if (this.PropertyName == "ListItems")
                {
                    foreach (object sub in (value as Array))
                    {
                        ret.Add(sub.GetType().FullName);
                    }
                    //ret.Add((value as Array).GetValue(0).GetType().FullName);
                }

                this.OriginatingTypeNames = ret.ToArray();
            }
        }

        /// <summary>
        /// Used to store the "Originating Type" of a GUID-style item link in the database.
        /// Lists will have one originating type, Dictionaries will have two.
        /// </summary>
        public string[] OriginatingTypeNames { get; set; }

        public static string GetDBTableName()
        {
            return "";
        }

        public static string[] GetDBColumnNames()
        {
            return null;
        }

        public static Type[] GetDBColumnTypes()
        {
            return null;
        }
    }
}
