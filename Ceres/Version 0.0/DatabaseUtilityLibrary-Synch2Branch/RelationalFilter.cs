﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CeresBase.IO.Serialization
{
    public enum Comparison
    {
        Equal,
        LessThan,
        GreaterThan,
        LessThanEqual,
        GreaterThanEqual,
        NotEqual
    }

    public class Condition
    {
        public string PropertyName { get; set; }
        public Comparison Operator { get; set; }
        public object Value { get; set; }

        public Condition(string propertyName, Comparison op, object value)
        {
            this.PropertyName = propertyName;
            this.Operator = op;
            this.Value = value;
        }
    }

    public class RelationalFilter
    {
        public List<Guid> UUID { get; set; }

        public Type TargetClass { get; set; }

        public List<Condition> Conditions { get; set; }

        public string TargetHelperClassName { get; set; }

        /// <summary>
        /// Deserialize-time modification.  Force refreshing of objects that have already been deserialized, and update the reference
        /// in the ObjectManager to the new object.
        /// </summary>
        public bool ForceRefresh { get; set; }


        private bool deleteObjects = false;
        /// <summary>
        /// Serialize-time modification.  Delete from the DB all objects being serialized in this pass.  Has no effect on objects not already present
        /// in the database.  (Requires a read from DB to be effective, obviously!)
        /// </summary>
        public bool DeleteObjects
        {
            get
            {
                return this.deleteObjects;
            }
            set
            {
                this.deleteObjects = value;
            }
        }

        private bool compareAndUpdateCollections = false;
        /// <summary>
        /// Serialize-time modification.  Rather than overwriting collections (default), add any new values to the existing collection in the db.
        /// Modified members will be updated correctly.
        /// </summary>
        public bool CompareAndUpdateCollections
        {
            get
            {
                return this.compareAndUpdateCollections;
            }
            set
            {
                this.compareAndUpdateCollections = value;
            }
        }

        public RelationalFilter()
        {
            this.Conditions = new List<Condition>();
            this.ForceRefresh = false;
            this.UUID = new List<Guid>();
        }

        public RelationalFilter(Type targetClass, Guid uuid)
        {
            this.TargetClass = targetClass;
            this.Conditions = new List<Condition>();
            this.UUID = new List<Guid>();
            this.UUID.Add(uuid);
            this.ForceRefresh = false;
        }

        public RelationalFilter(Type targetClass, Condition condition)
        {
            this.TargetClass = targetClass;
            this.Conditions = new List<Condition>();
            this.Conditions.Add(condition);
            this.ForceRefresh = false;
            this.UUID = new List<Guid>();
        }

        public RelationalFilter(Type targetClass, Condition[] conditions)
        {
            this.TargetClass = targetClass;
            this.Conditions = new List<Condition>();
            this.UUID = new List<Guid>();
            foreach (Condition condition in conditions)
            {
                this.Conditions.Add(condition);
            }
            this.ForceRefresh = false;
        }

        public RelationalFilter(String helperClassName, Guid uuid)
        {
            this.TargetHelperClassName = helperClassName;
            this.UUID = new List<Guid>();
            this.UUID.Add(uuid);
            this.ForceRefresh = false;
        }

        public RelationalFilter(String helperClassName, List<Guid> uuids)
        {
            this.TargetHelperClassName = helperClassName;
            this.UUID = new List<Guid>();
            foreach (Guid thisID in uuids)
            {
                this.UUID.Add(thisID);
            }
            this.ForceRefresh = false;
        }

        public RelationalFilter(Type targetClass, List<Guid> uuids)
        {
            this.TargetClass = targetClass;
            this.Conditions = new List<Condition>();
            this.UUID = new List<Guid>();
            foreach (Guid thisID in uuids)
            {
                this.UUID.Add(thisID);
            }
            this.ForceRefresh = false;
        }
    }
}
