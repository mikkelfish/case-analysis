using System.Reflection;
using System.Runtime.CompilerServices;
using Razor.Attributes;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("CodeReflection")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("Copyright � CodeReflection 2004")]
[assembly: AssemblyTrademark("Trademark � CodeReflection 2004")]
[assembly: AssemblyCulture("")]		
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]

[assembly: SnapInExportedFromAssembly(typeof(Razor.SnapIns.AutoUpdateOptions.AutoUpdateOptionsSnapIn))]