/*
 * This file is a part of the Razor Framework.
 * 
 * Copyright (C) 2003 Mark (Code6) Belles 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * */

using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Razor.SnapIns
{
    /// <summary>
    /// Summary description for SnapInApplicationContext.
    /// </summary>
    //	[System.Diagnostics.DebuggerStepThrough()]
    public class SnapInApplicationContext : ApplicationContext
    {	

        protected FormList _topLevelWindows;
		
		//MBF 9/22/05
		//Store other non-top windows so we can clean them up if all top windows are closed
		//and also I don't trust the garbage collector if we were to just create a window
		//and let the object drift
		protected FormList _otherWindows; 

        /// <summary>
        /// Initializes a new instance of the SnapInApplicationContext class
        /// </summary>
        public SnapInApplicationContext()
        {
            _topLevelWindows = new FormList();		
			_otherWindows = new FormList();
        }		

        /// <summary>
        /// Adds a Form to the SnapInHostingEngine's ApplicationContext as a top level form
        /// </summary>
        /// <param name="form"></param>
        public virtual void AddTopLevelWindow(Form form)
        {
            if (form == null)
                throw new ArgumentNullException("form");

            // wire up to the form's closed event
            form.Closed += new EventHandler(OnTopLevelWindowClosed);

            // add the window to the list
            lock(_topLevelWindows)
            {
				//Mikkel Fishman 9/22/05
				//This will make it so when closing the first form that's added it always closes the program
				//(Alternate to stop this is to override OnMainFormClosed in this class, but there's no
				//point in adding a MainForm when the system doesn't really use it)
//                if (_topLevelWindows.Count == 0)
//                    base.MainForm = form;

                _topLevelWindows.Add(form);
            }
        }

		/// <summary>
		/// Add a non-top window to the manager. These windows will close when all top windows are closed
		/// so use this for tool windows, etc.
		/// </summary>
		/// <remarks>Mikkel Fishman 9/22/05</remarks>
		public virtual void AddOtherWindow(Form form)
		{
			if (form == null)
				throw new ArgumentNullException("form");
			
			form.Closed +=new EventHandler(OnOtherWindowClosed);
			lock(_otherWindows)
			{
				_otherWindows.Add(form);
			}

		}

        /// <summary>
        /// Removes a Form from the SnapInHostingEngine's ApplicationContext as a top level form
        /// </summary>
        /// <param name="form"></param>
        public virtual void RemoveTopLevelWindow(Form form)
        {
            if (form == null)
                throw new ArgumentNullException("form");

            // unwire from the form's closed event
            form.Closed -= new EventHandler(OnTopLevelWindowClosed);

            // lock the window list
            lock(_topLevelWindows)
            {
                // remove the window from the list
                _topLevelWindows.Remove(form);
		
                // if that was the last top level window
				if (_topLevelWindows.Count == 0)
				{
					//Dispose of each window that isn't a top window
					foreach(Form oform in _otherWindows)
					{
						oform.Dispose();
					}
				
					_otherWindows.Clear();

					// go ahead and exit the main thread
					this.ExitThread();
				}
            }
        }

		/// <summary>
		/// Remove a non-top window from the manager
		/// </summary>
		/// <remarks>Mikkel Fishman 9/22/05</remarks>
		public virtual void RemoveOtherWindow(Form form)
		{
			if (form == null)
				throw new ArgumentNullException("form");

			// unwire from the form's closed event
			form.Closed -= new EventHandler(OnOtherWindowClosed);

			lock(_otherWindows)
			{
				_otherWindows.Remove(form);
			}
		}

        /// <summary>
        /// Occurs when a top level window is closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnTopLevelWindowClosed(object sender, EventArgs e)
        {
            try
            {
                // snag the sender of the event
                Form form = (Form)sender;

                // remove the window from out list, if it's the last one this will exit the main thread
                this.RemoveTopLevelWindow(form);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

		/// <summary>
		/// Occurs when an auxiliary window is closed
		/// </summary>
		/// <remarks>Mikkel Fishman 9/22/05</remarks>
		protected virtual void OnOtherWindowClosed(object sender, EventArgs e)
		{
			try
			{
				// snag the sender of the event
				Form form = (Form)sender;

				// remove the window from out list, if it's the last one this will exit the main thread
				this.RemoveOtherWindow(form);
			}
			catch(Exception ex)
			{
				Debug.WriteLine(ex);
			}
		}



        /// <summary>
        /// Determines if the application context contains only one main form
        /// </summary>
        /// <returns></returns>
        public bool HasOnlyOneMainForm
        {
            get
            {
				//MBF 9/22/05
				//This property was not threadsafe because there was no lock statement
				lock(_topLevelWindows)
				{
					if (_topLevelWindows.Count == 1)
						return true;
					return false;
				}
            }
		}

	}	
}
