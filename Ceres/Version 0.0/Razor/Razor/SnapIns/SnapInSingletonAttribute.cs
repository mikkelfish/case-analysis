using System;
using System.Collections.Generic;
using System.Text;

namespace Razor.SnapIns
{
    [global::System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class SnapInSingletonAttribute : Attribute
    {
        
    }

}
