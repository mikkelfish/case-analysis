###############################################################################
# What is this? 
###############################################################################

This document will provide you the developer will some helpfull tips on configuring and a building your own applications built using the Razor Framework.

Thank you for downloading the Razor Framework.

###############################################################################
# Who wrote this stuff?
###############################################################################

Me, Mark (Code6) Belles for contributing some sweet stuff for you to use and learn from.

I had some help tho, here's my list of contributors that helped me think this stuff up. Without these crafty individuals Razor would suck very badly I'm quite sure. 

Pup, for being my right hand man, pointing out all the bad ideas and turning them into better more reasonable ones that make a lot more sense.

Dorf, for breaking lot of things and generally testing out things we overlooked. 

pArtiK, for telling me it was good stuff and that I should release it. Not to mention testing and patching up my threading and wizard ideas. 

Skinny, for listening to me gloat and/or bitch about Razor's progress for more time than he shoulda had too. Good roomates are hard to find.

Spunk, for being around to ride with when my head was ready to explode from excitement or frustration, and for keeping me honest at Foosball.

Send me a shout at markbelles@gmail.com if you want, or head over to my blogs/forums at http://mrbelles.brinkster.net and drop me a line. I enjoy getting feedback, it's my one true motivator.

###############################################################################
# Licences and Releases
###############################################################################

The Razor Framework is released under the LGPL. For more information on how this applies to you and your projects, check out the licence details here. 

http://www.gnu.org/copyleft/lesser.html

###############################################################################
# Support Options
###############################################################################

Need more help? Check out the project home at http://mrbelles.brinkster.net for the latest news and tips. Please direct your questions to the forums. Should you find yourself stuck, please read the articles on the Code Project, or the blog on my site, they should get you up to speed quickly.

###############################################################################
# FAQ
###############################################################################

Q: What does the Lesser GPL mean to me?

A: A whole lot of lingo just to say that you can use Razor in closed or open source projects, provided you give me credit for Razor, don't change my stuff and call it your own, and that you provide anyone that asks the Razor source code that you built your projects with. 

Pretty much don't be a bastard, give back when you can, and enjoy Razor however you want! :P


Q: How do I build this?

A: Build in this order, for minimized headaches after first downloading.

1. Razor Framework
2. Bootstrap
3. Hosting Engine
4. Window Positioning Engine
5. Application Window

Rinse and repeat for any other SnapIns you want to use that are included with the solution. There probably will be more than a few, don't freak out, just build those and you'll get up and running.


Q: Do I need to deploy those funky *.pdb files? 

A: No, those are only needed for debugging purposes. If you are releasing your app, just deploy the following files.

1. *.exe
2. *.dll
3. *.config
4. *.manifest


Q: I am really really stuck, I need help, and your articles, forums, and blog aren't cutting it. Will you help me personally?

A: Yes, but it will cost you. Contact me personally at above email address, and we'll work out the details. I work by the hour, and I don't work unless I am paid in advance. Pay-pal is the preferred payment method.


Q: Do you take contributions?

A: Heck ya! If you really want to thank me for my efforts, nothing says thanks more than cash money or promotion on your site or venue to the masses.