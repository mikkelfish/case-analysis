/*
* MATLAB Compiler: 4.4 (R2006a)
* Date: Wed May 17 15:39:29 2006
* Arguments: "-B" "macro_default" "-M" "-silentsetup" "-S" "-B"
* "dotnet:GersteinCleaner,GersteinCleanerclass,1.0,private" "-W"
* "dotnet:GersteinCleaner,GersteinCleanerclass,1.0,private" "-T" "link:lib" "-d"
* "C:/MatlabR2006a/work/GersteinCleaner/src" "C:/Documents and
* Settings/mbf4/Desktop/Matlab/CleanData.m" 
*/

using System;
using System.Reflection;

using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;


[assembly: System.Reflection.AssemblyVersion("1.0.*")]
#if SHARED
[assembly: System.Reflection.AssemblyKeyFile(@"")]
#endif

namespace GersteinCleaner
{
  /// <summary>
  /// The GersteinCleanerclass class provides a CLS compliant interface to the
  /// M-functions contained in the files:
  /// <newpara></newpara>
  /// C:/Documents and Settings/mbf4/Desktop/Matlab/CleanData.m
  /// <newpara></newpara>
  /// matlabrc.m
  /// <newpara></newpara>
  /// dirname.m
  /// <newpara></newpara>
  /// deployprint.m
  /// <newpara></newpara>
  /// printdlg.m
  /// </summary>
  /// <remarks>
  /// @Version 1.0
  /// </remarks>
  public class GersteinCleanerclass : IDisposable
    {
      #region Constructors

      /// <summary internal= "true">
      /// The static constructor instantiates and initializes the MATLAB Common Runtime
      /// instance.
      /// </summary>
      static GersteinCleanerclass()
        {
          Assembly assembly= Assembly.GetExecutingAssembly();

          string ctfFilePath= assembly.Location;

          int lastDelimeter= ctfFilePath.LastIndexOf(@"\");

          ctfFilePath= ctfFilePath.Remove(lastDelimeter, (ctfFilePath.Length - lastDelimeter));

          if (MWMCR.InitializeApplication(new string[]{}))
            {
              mcr= new MWMCR(MCRComponentState.MCC_GersteinCleaner_name_data,
                             MCRComponentState.MCC_GersteinCleaner_root_data,
                             MCRComponentState.MCC_GersteinCleaner_public_data,
                             MCRComponentState.MCC_GersteinCleaner_session_data,
                             MCRComponentState.MCC_GersteinCleaner_matlabpath_data,
                             MCRComponentState.MCC_GersteinCleaner_classpath_data,
                             MCRComponentState.MCC_GersteinCleaner_libpath_data,
                             MCRComponentState.MCC_GersteinCleaner_mcr_application_options,
                             MCRComponentState.MCC_GersteinCleaner_mcr_runtime_options,
                             MCRComponentState.MCC_GersteinCleaner_mcr_pref_dir,
                             MCRComponentState.MCC_GersteinCleaner_set_warning_state,
                             ctfFilePath, true);
            }
        }


      /// <summary>
      /// Constructs a new instance of the GersteinCleanerclass class.
      /// </summary>
      public GersteinCleanerclass()
        {
        }


      #endregion Constructors

      #region Finalize

      /// <summary internal= "true">
      /// Class destructor called by the CLR garbage collector.
      /// </summary>
      ~GersteinCleanerclass()
        {
          Dispose(false);
        }


      /// <summary>
      /// Frees the native resources associated with this object
      /// </summary>
      public void Dispose()
        {
          Dispose(true);

          GC.SuppressFinalize(this);
        }


      /// <summary internal= "true">
      /// Internal dispose function
      /// </summary>
      protected virtual void Dispose(bool disposing)
        {
          if (!disposed)
            {
              disposed= true;

              if (disposing)
                {
                  // Free managed resources;
                }

              // Free native resources
            }
        }


      #endregion Finalize

      #region Methods

      /// <summary>
      /// Provides a single output, 0-input interface to the CleanData M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// to use call:
      /// cleaned = CleanData(rawdata);        to clean the data without plotting
      /// cleaned = CleanData(rawdata,0);      to clean the data without plotting
      /// cleaned = CleanData(rawdata,1);      to clean the data with plotting
      /// rawdata is the original data of channels and time, in a 2-D matlab array.
      /// NOTE:	(1) a minimum of 3 channels is required
      /// (2) the longer dimension of rawdata will be treated as time.
      /// cleaned is a 2-D array, [time,channels+2] of cleaned data. The last
      /// two columns returned are the first and second principal component vectors.
      /// CleanData.m will clean the data array using a principal
      /// component analysis to find common signal across channels
      /// and remove it.  Edit the initialized value of the global variables in this
      /// file appropriately to optimize cleaning of your data.
      /// Questions?  jeff@mulab.physiol.upenn.edu
      /// george@mulab.physiol.upenn.edu
      /// </remarks>
      /// <returns>An MWArray containing the first output argument.</returns>
      ///
      public MWArray CleanData()
        {
          return mcr.EvaluateFunction("CleanData");
        }


      /// <summary>
      /// Provides a single output, 1-input interface to the CleanData M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// to use call:
      /// cleaned = CleanData(rawdata);        to clean the data without plotting
      /// cleaned = CleanData(rawdata,0);      to clean the data without plotting
      /// cleaned = CleanData(rawdata,1);      to clean the data with plotting
      /// rawdata is the original data of channels and time, in a 2-D matlab array.
      /// NOTE:	(1) a minimum of 3 channels is required
      /// (2) the longer dimension of rawdata will be treated as time.
      /// cleaned is a 2-D array, [time,channels+2] of cleaned data. The last
      /// two columns returned are the first and second principal component vectors.
      /// CleanData.m will clean the data array using a principal
      /// component analysis to find common signal across channels
      /// and remove it.  Edit the initialized value of the global variables in this
      /// file appropriately to optimize cleaning of your data.
      /// Questions?  jeff@mulab.physiol.upenn.edu
      /// george@mulab.physiol.upenn.edu
      /// </remarks>
      /// <param name="action">Input argument #1</param>
      /// <returns>An MWArray containing the first output argument.</returns>
      ///
      public MWArray CleanData(MWArray action)
        {
          return mcr.EvaluateFunction("CleanData", action);
        }


      /// <summary>
      /// Provides a single output, 2-input interface to the CleanData M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// to use call:
      /// cleaned = CleanData(rawdata);        to clean the data without plotting
      /// cleaned = CleanData(rawdata,0);      to clean the data without plotting
      /// cleaned = CleanData(rawdata,1);      to clean the data with plotting
      /// rawdata is the original data of channels and time, in a 2-D matlab array.
      /// NOTE:	(1) a minimum of 3 channels is required
      /// (2) the longer dimension of rawdata will be treated as time.
      /// cleaned is a 2-D array, [time,channels+2] of cleaned data. The last
      /// two columns returned are the first and second principal component vectors.
      /// CleanData.m will clean the data array using a principal
      /// component analysis to find common signal across channels
      /// and remove it.  Edit the initialized value of the global variables in this
      /// file appropriately to optimize cleaning of your data.
      /// Questions?  jeff@mulab.physiol.upenn.edu
      /// george@mulab.physiol.upenn.edu
      /// </remarks>
      /// <param name="action">Input argument #1</param>
      /// <param name="tdata">Input argument #2</param>
      /// <returns>An MWArray containing the first output argument.</returns>
      ///
      public MWArray CleanData(MWArray action, MWArray tdata)
        {
          return mcr.EvaluateFunction("CleanData", action, tdata);
        }


      /// <summary>
      /// Provides the standard 0-input interface to the CleanData M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// to use call:
      /// cleaned = CleanData(rawdata);        to clean the data without plotting
      /// cleaned = CleanData(rawdata,0);      to clean the data without plotting
      /// cleaned = CleanData(rawdata,1);      to clean the data with plotting
      /// rawdata is the original data of channels and time, in a 2-D matlab array.
      /// NOTE:	(1) a minimum of 3 channels is required
      /// (2) the longer dimension of rawdata will be treated as time.
      /// cleaned is a 2-D array, [time,channels+2] of cleaned data. The last
      /// two columns returned are the first and second principal component vectors.
      /// CleanData.m will clean the data array using a principal
      /// component analysis to find common signal across channels
      /// and remove it.  Edit the initialized value of the global variables in this
      /// file appropriately to optimize cleaning of your data.
      /// Questions?  jeff@mulab.physiol.upenn.edu
      /// george@mulab.physiol.upenn.edu
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public MWArray[] CleanData(int numArgsOut)
        {
          return mcr.EvaluateFunction(numArgsOut, "CleanData");
        }


      /// <summary>
      /// Provides the standard 1-input interface to the CleanData M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// to use call:
      /// cleaned = CleanData(rawdata);        to clean the data without plotting
      /// cleaned = CleanData(rawdata,0);      to clean the data without plotting
      /// cleaned = CleanData(rawdata,1);      to clean the data with plotting
      /// rawdata is the original data of channels and time, in a 2-D matlab array.
      /// NOTE:	(1) a minimum of 3 channels is required
      /// (2) the longer dimension of rawdata will be treated as time.
      /// cleaned is a 2-D array, [time,channels+2] of cleaned data. The last
      /// two columns returned are the first and second principal component vectors.
      /// CleanData.m will clean the data array using a principal
      /// component analysis to find common signal across channels
      /// and remove it.  Edit the initialized value of the global variables in this
      /// file appropriately to optimize cleaning of your data.
      /// Questions?  jeff@mulab.physiol.upenn.edu
      /// george@mulab.physiol.upenn.edu
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="action">Input argument #1</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public MWArray[] CleanData(int numArgsOut, MWArray action)
        {
          return mcr.EvaluateFunction(numArgsOut, "CleanData", action);
        }


      /// <summary>
      /// Provides the standard 2-input interface to the CleanData M-function.
      /// </summary>
      /// <remarks>
      /// M-Documentation:
      /// to use call:
      /// cleaned = CleanData(rawdata);        to clean the data without plotting
      /// cleaned = CleanData(rawdata,0);      to clean the data without plotting
      /// cleaned = CleanData(rawdata,1);      to clean the data with plotting
      /// rawdata is the original data of channels and time, in a 2-D matlab array.
      /// NOTE:	(1) a minimum of 3 channels is required
      /// (2) the longer dimension of rawdata will be treated as time.
      /// cleaned is a 2-D array, [time,channels+2] of cleaned data. The last
      /// two columns returned are the first and second principal component vectors.
      /// CleanData.m will clean the data array using a principal
      /// component analysis to find common signal across channels
      /// and remove it.  Edit the initialized value of the global variables in this
      /// file appropriately to optimize cleaning of your data.
      /// Questions?  jeff@mulab.physiol.upenn.edu
      /// george@mulab.physiol.upenn.edu
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return.</param>
      /// <param name="action">Input argument #1</param>
      /// <param name="tdata">Input argument #2</param>
      /// <returns>An Array of length "numArgsOut" containing the output
      /// arguments.</returns>
      ///
      public MWArray[] CleanData(int numArgsOut, MWArray action, MWArray tdata)
        {
          return mcr.EvaluateFunction(numArgsOut, "CleanData", action, tdata);
        }


      /// <summary>
      /// Provides an interface for the CleanData function in which the input and output
      /// arguments are specified as an array of MWArrays.
      /// </summary>
      /// <remarks>
      /// This method will allocate and return by reference the output argument
      /// array.<newpara></newpara>
      /// M-Documentation:
      /// to use call:
      /// cleaned = CleanData(rawdata);        to clean the data without plotting
      /// cleaned = CleanData(rawdata,0);      to clean the data without plotting
      /// cleaned = CleanData(rawdata,1);      to clean the data with plotting
      /// rawdata is the original data of channels and time, in a 2-D matlab array.
      /// NOTE:	(1) a minimum of 3 channels is required
      /// (2) the longer dimension of rawdata will be treated as time.
      /// cleaned is a 2-D array, [time,channels+2] of cleaned data. The last
      /// two columns returned are the first and second principal component vectors.
      /// CleanData.m will clean the data array using a principal
      /// component analysis to find common signal across channels
      /// and remove it.  Edit the initialized value of the global variables in this
      /// file appropriately to optimize cleaning of your data.
      /// Questions?  jeff@mulab.physiol.upenn.edu
      /// george@mulab.physiol.upenn.edu
      /// </remarks>
      /// <param name="numArgsOut">The number of output arguments to return</param>
      /// <param name= "argsOut">Array of MWArray output arguments</param>
      /// <param name= "argsIn">Array of MWArray input arguments</param>
      ///
      public void CleanData(int numArgsOut, ref MWArray[] argsOut, MWArray[] argsIn)
        {
          mcr.EvaluateFunction("CleanData", numArgsOut, ref argsOut, argsIn);
        }


      
      #endregion Methods

      #region Class Members

      private static MWMCR mcr= null;

      private bool disposed= false;

      #endregion Class Members
    }
}
