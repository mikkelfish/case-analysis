﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DBManager;
using System.Collections.ObjectModel;
using DatabaseUtilityLibrary;

namespace DBManager
{
    [AlwaysSerialize]
    public partial class Group : INotifyPropertyChanged
    {
        public Group()
        {
            //this.Users.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Users_CollectionChanged);
            //this.Administrators.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Administrators_CollectionChanged);
        }

        void Administrators_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.OnPropertyChanged("Admins");
        }

        void Users_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.OnPropertyChanged("Users");
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        protected void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler ev = PropertyChanged;
            if (ev != null)
            {
                ev(this, new PropertyChangedEventArgs(property));
            }
        }

        private ObservableCollection<User> users = new ObservableCollection<User>();
        public ObservableCollection<User> Users
        {
            get
            {
                return this.users;
            }
        }

        private ObservableCollection<User> admins = new ObservableCollection<User>();
        public ObservableCollection<User> Administrators
        {
            get
            {
                return this.admins;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
