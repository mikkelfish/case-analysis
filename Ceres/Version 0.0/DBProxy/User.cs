﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DatabaseUtilityLibrary;

namespace DBManager
{
    [AlwaysSerialize]
    public partial class User : INotifyPropertyChanged
    {
        public override string ToString()
        {
            return this.Name;
        }

        protected void OnPropertyChanged(string member)
        {
            PropertyChangedEventHandler ev = this.PropertyChanged;
            if (ev != null)
            {
                ev(this, new PropertyChangedEventArgs(member));
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
