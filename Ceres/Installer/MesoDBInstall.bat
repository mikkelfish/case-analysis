@echo off

echo Installing PostgreSQL...
msiexec /i postgresql-8.3-int.msi  /qr INTERNALLAUNCH=1 ADDLOCAL=server,nls,psql,pgadmin CREATESERVICEUSER=1 SERVICEDOMAIN="%COMPUTERNAME%" SERVICEACCOUNT="postgres83" SERVICEPASSWORD="postgres_service83" SUPERUSER="postgres" SUPERPASSWORD="postgres" BASEDIR="%ALLUSERSPROFILE%\Mesosoft\Database"

echo Setting up MesoDB...
set PGUSER=postgres
set PGPASSWORD=postgres
"%ALLUSERSPROFILE%\Mesosoft\Database\bin\psql" -f MesoDBPrep.sql -d postgres -U postgres