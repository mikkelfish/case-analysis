﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace PhysioConverter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dia = new OpenFileDialog();
            dia.Multiselect = true;
            if (dia.ShowDialog() == DialogResult.Cancel) return;

            this.listBox1.Items.AddRange(dia.FileNames);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (string file in this.listBox1.Items)
            {
                using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
                {
                    using (FileStream toWrite = new FileStream(file + ".text", FileMode.Create, FileAccess.Write))
                    {
                        using (StreamReader read = new StreamReader(stream))
                        {
                            using (StreamWriter write = new StreamWriter(toWrite))
                            {
                                string line = read.ReadLine();
                                line = read.ReadLine();

                                double first = 0;
                                bool done = false;

                                int count = 0;

                                while (line != null)
                                {
                                    line = line.Trim();

                                    if (count == 44)
                                    {
                                        int s = 0;
                                    }
                                    
                                    string[] splits = line.Split(' ');
                                    

                                    string[] minHour = splits[0].Split(':');
                                    int hours = 0;
                                    int minutes = 0;
                                    int seconds = 0;
                                    int mili = 0;

                                    int minIndex = 0;

                                    if (minHour.Length == 3)
                                    {
                                        hours = int.Parse(minHour[0]);
                                        minIndex = 1;
                                    }

                                    minutes = int.Parse(minHour[minIndex]);

                                    string rest = minHour[minIndex + 1];
                                    int periodIndex = rest.IndexOf('.');
                                    if (periodIndex == -1) continue;

                                     seconds = int.Parse(rest.Substring(0, periodIndex));
                                    mili = int.Parse(rest.Substring(periodIndex + 1));

                                    long time = hours*3600 + minutes * 60 + seconds;
                                    
                                    double realTime = time + mili/1000.0;
                                   

                                    if(done)
                                    {
                                        realTime -= first;
                                    }
                                    else
                                    {
                                        first = realTime;
                                        realTime = 0;
                                        done = true;
                                    }

                                    if (realTime < 0)
                                    {
                                        int s = 0;
                                    }

                                    string writeLine = realTime.ToString();
                                    write.WriteLine(writeLine);

                                    count++;
                                    line = read.ReadLine();
                                }
                            }
                        }

                    }
                }

            }
        }
    }
}
