function [I,taugrid,E1,E2]=MutInf(data,taumax,nbins)
%   MutInf.m
%   usage: [I,taugrid,E1,E2]=MutInf(data,taumax,nbins,method)
%   Mutual information of time-series data for different delays
%
%   Inputs:
%
%   data        time-series
%   taumax      maximum delay value
%   nbins       bins for histogram (default = 100)
%
%   Notes:      If taumax is one integer, mutual information is computed 
%               for all delay values up to taumax.  If taumax 
%
%   Outputs:
%
%   I           Mutual Information
%   taugrid     delay values
%   E1          Entropy of the time-series
%   E2          Entropies of the delayed time-series
%
%   Anatoly Zlotnik, June 19, 2006
%
%   [1] Zlotnik, A., "Algorithm Development for Estimation and Modeling 
%       Problems In Human EEG Analysis". M.S. Thesis, Case Western Reserve 
%       University; 2006