function [CofR2, CofR1, Pvalue, NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
 
%Titration of Chaos with added noise
%   [CofR2, CofR1, Pvalue, NoiseLimit]=titration(data,tit,kappa,degree,noise_step,pp,num_run)
%
%   This program will first check if the input data is nonlinear by Akaike cost function
%   and by an F-test significance level p<.05.  Titration can only be done if nonlinearity is
%   detected. 
%
%   Reference: Poon, C., Barahona, M. "Titration of chaos with added noise,"  Proc. Natl. Acad. Sci. 2000
%
%
%   Outputs: CofR2 = C(r) for the nonlinear estimate
%            CofR1 = C(r) for the linear estimate
%            Pvalue = the pvalue of F-test before titration
%            NoiseLimit = the minimum amount(%) of noise added that prevents nonlinear detection by Akaike and F-test
%
%   C(r) is the Akaike cost function and r is the number of polynomial terms
%   of the nonlinear or linear estimate of the time series using the 
%   Volterra-Wiener method developed by Mauricio Barahona and Chi-Sang Poon in Nature 1996.
%
%   Inputs: data = the discrete time series; 
%           tit = 1 for titration or tit = 0 for nonlinear detection only
%           kappa = the memory order of the Volterra series; 
%           degree = the nonlinear degree of the Volterra series; 
%           noise_step = the increment step size of added white noise in decimal (.01 for 1% of signal power); 
%           pp = flag to plot a figure (pp = 1 to plot; pp = 0 not to plot). 
%           num_run = the number of titration runs
%
%   
%   [CofR2, CofR1, Pvalue, NoiseLimit] = titration(data)
%           defaults to:
%                       tit = 1;    
%                       kappa = 6; 
%                       degree = 3;
%                       noise_step = .01; 1% of signal power
%                       pp = 1; 
%                       num_run=1.
%
%   During titration with added noise, NoiseLimit is averaged over 
%   multiple runs (specified by num_run). 
%   num_run should be greater or equal to 1. For larger values of num_run 
%   and smaller values of noise step, NoiseLimit will be more accurate but will increase 
%   processing time.
%
%
%   Code developed by Laurence Zapanta and Sung Il Kim 11/2003
%
%   "Statistics Toolbox for Matlab is needed for this code" 
%
%

if nargin<7
    num_run=1;
end
if nargin<6
    pp=1;
end
if nargin<5
    noise_step=.01;
end
if nargin<4
    degree=3;
end
if nargin<3
    kappa=6;
end
if nargin<2
    tit=1;
end


data_len=length(data);
data_std = std(data);
terms=factorial(kappa + degree)/(factorial(kappa)*factorial(degree));

%initialize
NL=0;
NoiseLimit=0;
%check if data length is twice the number of terms
if data_len>=(2*terms)
    
    %make data into a column vector
    [r,c]=size(data);
    if r>c
        data=data;
    else
        data=data';
    end
    
    %linear
    [C_lin , err_lin]=ckoraut2_lz(data,terms-1,1);%align data (terms-1)
    %nonlinear
    [C_non , err_non]=ckoraut2_lz(data,kappa,degree);
    
    CofR1=C_lin;
    CofR2=C_non;
    
    err_len=length(err_lin);
    
    %find minimum of C_lin and C_non and their indeces.
    %search begins at C_***(2), because C_***(1) is null.
    [C_lin_min, lin_index] = min(C_lin(2:end));
    [C_non_min, non_index] = min(C_non(2:end));
    
    %get variance of error vectors
    %add 1 to ***_index, because minimum search above begins at C_***(2)
    err_lin_var = var(err_lin(:,lin_index+1));
    err_non_var = var(err_non(:,non_index+1));
    
    if C_non_min < C_lin_min
        %ftest
        F=err_lin_var/err_non_var;
        pval=1-fcdf(F,err_len-1,err_len-1);
    else
        pval = 1;
    end
    
    Pvalue = pval;
    
    
    if pp==1
        figure
        subplot(2,1,1)
        plot(C_lin,'r-*')
        hold on
        plot(C_non,'b-*')
        
        
        if pval < .05       %Significance level
            linear = 0;     %Nonlinearity detected
            legend('Linear','Nonlinear',['Nonlinearity detected: pval = ' num2str(pval)])
        else
            legend('Linear','Nonlinear','Nonlinear is not better than linear')
        end
        
        xlabel('Number of terms (r)')
        ylabel('C(r)')
        Title('Titration of Chaos with added noise')
    end
    
    %Titration starts |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| 
    if tit==1
        
        if pval < .05
             
            n11 = 1;
            noise_start = 1;
            noise_addition = 3/noise_step;  %make noise addition limit to 300% of signal power
            back_search = 3;                %fast search for noise_level
            
            while n11 < num_run + 1, 
                
                for n10=noise_start:noise_addition,
                    
                    NL=noise_step*n10;
                    noise=randn(data_len,1)*NL*data_std;
                    noisy_data=data + noise;
                    
                    [C_lin , err_lin]=ckoraut2_lz(noisy_data,terms-1,1);
                    [C_non , err_non]=ckoraut2_lz(noisy_data,kappa,degree);
                    
                    [C_lin_min, lin_index] = min(C_lin(2:end));
                    [C_non_min, non_index] = min(C_non(2:end));
                    
                    Q = C_lin_min - C_non_min;
                    err1 = err_lin(:,lin_index+1);m1=mean(err1);s1=std(err1);
                    err2 = err_non(:,non_index+1);m2=mean(err2);s2=std(err2);
                    err_len = length(err1);                
                    
                    %Akaike and F-test:
                    %Akaike tests whether nonlinear model is better than linear. 
                    %F statistics tests the significance of nonlinearity.
                    %Null Hypothesis: Linear and nonlinear model is equivalent in their predition power. 
                    %Alternate Hypothesis:Nonlinear is better than linear.
                    
                    if Q>0
                        
                        F = var(err1) / var(err2);
                        F_pvalue = 1-fcdf(F,err_len - 1,err_len - 1);
                        
                        if 0.05 < F_pvalue %accept the null Hypothesis
                            noise_level(n11)=NL-noise_step;          
                            break;
                        end
                        
                    elseif Q<=0  %accept the null Hypothesis
                        noise_level(n11)=NL-noise_step;
                        break;
                    end
                    
                    %Limit noise addition to 300% of signal power
                    if n10 == noise_addition
                        sprintf('Adjust noise_addition higher')
                        break;
                    end
                    
                end
                
                % Adjust next noise_start below current noise_level by back_search 
                % in order to search next noise_level quickly 
                if noise_start ==1
                    if round(noise_level(n11)*(noise_step)^-1) > back_search
                        noise_start = round(noise_level(n11)*(noise_step)^-1 - back_search);
                    else
                        noise_start = 1;
                    end
                    
                else
                    %If nonlinearity is not detected, decrease noise level further until it detects 
                    %nonlinearity at least once. Afterwards, it returns to normal(forward) search by increasing NL
                    if round(noise_level(n11)*(noise_step)^-1) ==(noise_start - 1) 
                        n11 = n11 - 1;
                        if round(noise_start - back_search)<=0
                            noise_start = 1;
                        else
                            noise_start = round(noise_start - back_search);
                        end
                    else
                        if  round(noise_level(n11)*(noise_step)^-1) > back_search
                            noise_start = round(noise_level(n11)*(noise_step)^-1 - back_search);
                        else
                            noise_start = 1;
                        end     
                    end
                    
                end
                n11 = n11 + 1;
                pause(0.01);  

            end
            NoiseLimit = mean(noise_level)*100;
        else
            linear = 1;
            sprintf('Cannot do titration. Nonlinearity is not detected.')
        end
        
        %compute C(r) for noise limit NoiseLimit again for plotting 
        noise=randn(data_len,1)*(NoiseLimit/100)*data_std;
        noisy_data=data + noise;
        
        [C_lin , err_lin]=ckoraut2_lz(noisy_data,terms-1,1);
        [C_non , err_non]=ckoraut2_lz(noisy_data,kappa,degree);
        
        
        if pp==1
            subplot(2,1,2)
            plot(C_lin,'r-*')
            hold on
            plot(C_non,'b-*')
            legend('Linear','Nonlinear')
            if linear == 1
                legend('Linear','Nonlinear','Cannot do titration. Nonlinearity is not detected.')
            else
                 legend('Linear','Nonlinear',['Noise Limit = ' num2str(NoiseLimit) '%'])
            end
            
            xlabel('Number of terms (r)')
            ylabel('C(r)')
        end
    end
    
else
    CofR2 = 0;
    CofR1 = 0;
    Pvalue = [];
    sprintf('Error: Data must be twice the number of terms')
    
end





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    function [CofR , E_matrix]=ckoraut2_lz(Vect,kappa,degree)
%
%Code converted by Laurence Zapanta to a Matlab function 10/25/2003
%       [CofR, E_matrix]=ckoraur2_lz(vector,kappa,degree) outputs C(r) the Akaike 
%       parsimony principle of either the nonlinear or linear( depends on kappa and degree)  
%       estimate of the time series using the Volterra-Wiener method developed by 
%       Mauricio Barahona and Chi-Sang Poon 10/1/97.
%
%       E_matrix outputs a matrix containing error vectors for every r (# of polynomial terms). 
%       The error vector is the difference between the Volterra-Wiener estimate and the 
%       time series. E_vector can be used directly statistical analysis (t-test, F-test....etc). 
%       Vect is the input vector. Kappa is the memory. degree is the degree. 
%
%       For degree greater than 1, the code truncates the input Vector to match the same segment
%       predicted by the linear estimate (degree=1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Following lines added by Laurence

terms=prod((max(kappa,degree)+1):(kappa+degree))/prod(1:min(kappa,degree));
%truncate nonlinear to match the linear segment
if degree>1
    Vect=Vect(terms-kappa:end);
end
%make vect into a column vector
[r,c]=size(Vect);
if r>c
    y=Vect;
else
    y=Vect';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
%  developed by Mauricio Barahona (1993)
%  input file must contain scalar series(y) 
%  and it must be loaded in the system
%  file modified by Chi-Sang Poon 10/1/97 (redefined: terms, numsyn, and CofR)
%%% to run:
%%%  load datafile and set equal to y
%%%  run ckoraut2
%%%  output is CofR
%%%  for example:         ?load datafile.dat
%%%                        ?y=datafile;
%%%                        ?ckoraut2
%%%                        ?plot(CofR)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  xk(u,k) is a function defined to produce delayed by k
%  vectors of u which are used in the production of the Korenberg basis
%  
%  select(a,tol) picks component of vector a that are bigger than max(a)*tol
%
%  Input u  ---  Output y 
  
%%%% take the parameters of the system as input from the user
 
ymd=y(kappa+1:length(y));

%%%% Magnitudes of the calculation:
%%%%    numtot=number of points
%%%%    terms=number of polynomial terms

% Following line changed 10/1/97 from ckorauto.m

numterms=length(ymd);

%disp(' The total size of the series is...')
%disp(numterms)                 
                                       
%%%% take the number of points to be used for synthesis of the series from
%%%% the user
% Following line changed 10/1/97 from ckorauto.m
numsyn=numterms-1;

%%%% ysyn is the part of the time series we use to synthesize the Korenberg
%%%% basis
ysyn=ymd(1:numsyn);

%%%% ypred is the part of the time series that is going to be used for 
%%%% determining how accurate our Korenberg basis is in predicting future
%%%% values of the time series. 
ypred=ymd(numsyn+1:numterms);
numpred=numterms-numsyn;

%%%% These variables will be used elsewhere in the program
stdypred = std(ypred);
stdysyn = std(ysyn);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Dimensionalisation of matrices

ztot=zeros(numterms,terms);
zfirst=zeros(numsyn,terms);

zw=zeros(terms,terms);
beta=zeros(terms,terms);
wprim=zeros(terms,terms);

a=zeros(terms,1);
b=zeros(terms,1);
w2=zeros(terms,1);
dif=zeros(terms,1);
CofR = zeros(terms,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
E_matrix=zeros(length(ysyn),1);%added by Laurence 10/25/03
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
index=zeros(1,kappa);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   First term of the series calculated out of the loop

zfirst(:,1)=ones(numsyn,1);
ztot(:,1)=ones(numterms,1);
b(1)=mean(ysyn);
w2(1)=1;
index(1)=1;
dif(1)=ysyn'*ysyn/numsyn-b(1)^2*w2(1);
CofR(1) = 0;
wprim(1,1)=1;
identif(1)='C';
m=1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Rest of the terms

init=zeros(1,kappa);
initprev=zeros(1,kappa);

for i=1:degree,
   for k=0:kappa-1,

%%% set up an algorithm for covering all the possible terms in the
%%% Volterra series
       if i==1, 
          init(k+1)=1;
       elseif k==0, 
          init(k+1)=1+index(i-1);
       else
          init(k+1)=init(k)+initprev(kappa)-initprev(k)+1;
       end

%%% calculate the basis of the Volterra series (step 1 in Chapter 10)
%%% note a is the vector of Volterra coefficients
         
       for j=init(k+1):index(i),
           m=m+1;
           ztot(:,m)=ztot(:,j) .* y(kappa-k:numterms+kappa-k-1);
           zfirst(:,m)=ztot(1:numsyn,m);

           for pp=1:m-1,
                if pp==1,
                  zw(m,pp)=mean(zfirst(:,m));
                  beta(m,pp)=zw(m,pp)/w2(pp);
               else
                  zw(m,pp)=(zfirst(:,m)'*zfirst(:,pp))/numsyn-...
                            beta(pp,:)*zw(m,:)';
 
%%%  The selection and small numbers elimination processes are implemented
%%%  but not used since they do not improve the solution (at least for Ikeda)

                  if w2(pp)<= 1e-100,
                     beta(m,pp)=0;
                  else
                     beta(m,pp)=zw(m,pp)/w2(pp);
                  end

               end 
            end
           
            w2(m)= (zfirst(:,m)'*zfirst(:,m))/numsyn -(beta(m,:) .^2)*w2;
            yw=(ysyn'*zfirst(:,m))/numsyn - beta(m,:)*(b .* w2);
        
            if w2(m)<= 1e-100,
               b(m)=0;
            else
               b(m)=yw/w2(m);
            end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            wprim(m,m)=1;

            for tt=1:m-1
               for r=tt:m-1,
                  wprim(m,tt)=wprim(m,tt)-beta(m,r)*wprim(r,tt);
               end
            end
 
            a=wprim'*b;

            ycsyn=ztot(1:numsyn,:)*a;
            ersyn=ysyn-ycsyn;
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %following line added by Laurence Zapanta 10/25/03
            %outputs the error matrix
            
            E_matrix = [E_matrix ersyn];
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            CofR(m) = (ycsyn(1:numsyn) - ysyn)' * (ycsyn(1:numsyn) -ysyn);
            CofR(m) = CofR(m) / ( (ysyn - b(1))' * (ysyn - b(1)) );
            CofR(m) = sqrt(CofR(m));
% Following line changed 10/1/97 from ckorauto.m
            CofR(m) = log(CofR(m)) + m/numterms;



       end
    end

     initprev=init;
     index(i+1)=m;

end

 
return
