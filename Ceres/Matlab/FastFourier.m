function [freqs FFT] = FastFourier(data, frequency, numbins)
NFFT = numbins;
if(numbins == 0) 
    NFFT = 2^nextpow2(length(data)); % Next power of 2 from length of y
end
tempFT = fft(data,NFFT)/length(data);
FFT = abs(tempFT(1:NFFT/2)).^2;
freqs = frequency/2*linspace(0,1,NFFT/2);