function [PCA reformed] = runPCA(data, projectOnto, maxVectors)
    [Vecs,Vals,Psi] = pc_evectors(data,maxVectors);
    PCA = Vecs(:,projectOnto)' * data;
    reformed = Vecs(:,projectOnto) * PCA;