function output = decimaterev(idata,r,nfilt)

error(nargchk(2,3,nargin));
error(nargoutchk(0,1,nargout));

% Validate required inputs 
msg = validateinput(idata,r);
error(msg);

if fix(r) == 1
    output = idata;
    return
end

if nargin == 2
    nfilt = 8;
end

if nfilt > 13
    warning(generatemsgid('highorderIIRs'),...
        'IIR filters above order 13 may be unreliable.');
end

nd = length(idata);
nout = floor(nd/r)-r;

    rip = .05;	% passband ripple in dB
    [b,a] = cheby1(nfilt, rip, .8/r);
    while all(b==0) || (abs(filtmag_db(b,a,.8/r)+rip)>1e-6)
        nfilt = nfilt - 1;
        if nfilt == 0
            break
        end
        [b,a] = cheby1(nfilt, rip, .8/r);
    end
    if nfilt == 0
        error('Bad Chebyshev design, likely R is too big; try mult. decimation (R=R1*R2).')
    end

    % be sure to filter in both directions to make sure the filtered data has zero phase
    % make a data vector properly pre- and ap- pended to filter forwards and back
    % so end effects can be obliterated.
    odata = filtfilt(b,a,idata);
    nbeg = r - (r*nout - nd);
    for loop = 0:r-1
        tdata = odata(nbeg + loop:r:nout*r+loop);
        if loop == 0
            output = tdata;
        else
            output = [output;tdata];
        end
    end
    
%--------------------------------------------------------------------------
function H = filtmag_db(b,a,f)
%FILTMAG_DB Find filter's magnitude response in decibels at given frequency.

nb = length(b);
na = length(a);
top = exp(-j*(0:nb-1)*pi*f)*b(:);
bot = exp(-j*(0:na-1)*pi*f)*a(:);

H = 20*log10(abs(top/bot));

%--------------------------------------------------------------------------
function msg = validateinput(x,r)
% Validate 1st two input args: signal and decimation factor

msg = '';

if isempty(x) || issparse(x) || ~isa(x,'double'),
    msg.identifier = generatemsgid('invalidInput');
    msg.message = 'The input signal X must be a double-precision vector.';
    return;
end

if (abs(r-fix(r)) > eps) || (r <= 0)
    msg.identifier = generatemsgid('invalidR');    
    msg.message = 'Resampling rate R must be a positive integer.';
    return;
end

