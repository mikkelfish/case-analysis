%	ipca		- Reconstruction from Principal Components
%
%	[rcondata]	= ipca(pc_data,paxis);
%
%	_____INPUTS_____________________________________________________________
%	pc_data		projected data				(col vectors)
%			(assume sorted; kth row = kth pc)
%	paxis		principal axes				(col vectors)
%			(assume sorted col vectors)
%
%	_____OUTPUTS____________________________________________________________
%	rcondata	reconstructed data col vector		(col vectors)
%
%	_____SEE ALSO___________________________________________________________
%	pca
%
%	(C) 1998.07.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [rcondata]	= ipca(pc_data,paxis);

rcondata	= paxis*pc_data;
