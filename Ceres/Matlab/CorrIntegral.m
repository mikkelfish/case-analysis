function Cr = CorrIntegral(Data,m,r,Tau,W)
% Inputs
% Data : Input data 
% m    : Embedded dimension (a positive integer) (Typical value 2-8)
% r    : Tolerance 
% Tau  : Delay (a positive integer)
% W    : Theiler Window (a positive integer)
%
% Output
% Cr   : Correlation Integral 