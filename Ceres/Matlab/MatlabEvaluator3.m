function [output1 output2 output3] = MatlabEvaluator3(args, outputVar1, outputVar2, outputVar3)    
    eval(args);
    output1 = eval(outputVar1);
    output2 = eval(outputVar2);
    output3 = eval(outputVar3);