function [output1 output2 output3 output4] = MatlabEvaluator4(args, outputVar1, outputVar2, outputVar3, outputVar4)    
    eval(args);
    output1 = eval(outputVar1);
    output2 = eval(outputVar2);
    output3 = eval(outputVar3);
    output4 = eval(outputVar4);