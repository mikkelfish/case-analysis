function filtered = BandButterworthFilter(Data, SamplingRate, FilterCutOff, order)
FilterCutOff = FilterCutOff/SamplingRate;
[FilterB FilterA]=butter(order,FilterCutOff);
filtered=filtfilt(FilterB,FilterA,Data);