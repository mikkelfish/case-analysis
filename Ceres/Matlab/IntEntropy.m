function [HistEnt IntEnt]=IntEntropy(Data)
% This function calulate the interval entropy based on histogram and fitted
% probability distribution
%
% Ref: George N. Reeke, and Allan D. Coop "Estimating the temporal interval entropy of neuronal discharge"
%           Neural computation 16(2004) 941-970.
%
% Inputs
% Data    : Time interval between each event
%
% Outputs
% HistEnt  : Entropy value based on histogram
% IntEnt   : Entropy from distribution
% 
% Note ; This function requires Matlab optimization tool box.
%
% Example 
% Data  = rand(1,200);
% [HistEnt IntEnt]=IntEntropy(Data)
