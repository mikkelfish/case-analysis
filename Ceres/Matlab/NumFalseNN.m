function [Num1 Num2]=NumFalseNN(Data,m,Tau)
%
% This function calculates the numbers of false nearest neighborhood for the
% given embedded dimension and delay according to two criteria
% 
% Inputs
% Data : Input data (a vector)
% m    : embedded dimension (a positive integer) (Typical value 2-8)
% Tau  : Delay (a positive integer)
%
% Outpus
% Num1 : Number of false nearest neighborhood with first criteria
% Num2 : Number of false nearest neighborhood with second criteria
% 
% Note. Atol & Rtol have been selected as 10.
% 
% Ref
% "Determining embedded dimension for phase-space reconstruction using a 
% geometrical construction" M. B. Kannel, R. Brown, & H.D.I. Abarbanel