function [SD,efinal]=ItSurrDat(data,M)
%ItSurrDat.m
%usage: [SD,efinal]=ItSurrDat(data,M)
%Generates surrogate data set of the same length and with the same linear 
%statistical properties as data.  The distribution, autocorrelation, and 
%power spectrum are preserved, while the phase is iteratively modified.
%
%   Inputs:
%   data        time-series data
%   M           maximum iterations (default=50)
%
%   Outputs:
%   SD          surrogate data
%   efinal      error at final iteration
%
%   Copyright 05/06 Anatoly Zlotnik and Farhad Kaffashi