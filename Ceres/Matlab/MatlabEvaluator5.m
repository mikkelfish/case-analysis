function [output1 output2 output3 output4 output5] = MatlabEvaluator5(args,outputVar1, outputVar2, outputVar3, outputVar4, outputVar5)    
    eval(args);
    output1 = eval(outputVar1);
    output2 = eval(outputVar2);
    output3 = eval(outputVar3);
    output4 = eval(outputVar4);
    output5 = eval(outputVar5);