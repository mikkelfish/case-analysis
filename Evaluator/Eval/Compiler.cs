﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using Microsoft.CSharp;

namespace Ikriv.Eval
{
    public class Compiler : MarshalByRefObject // good for mocking
    {
        CodeGenerator _codeGenerator;
        CompilerImpl _compilerImpl;
        DomainUtil _domainUtil;

        List<string> _namespaces = new List<string>();
        List<string> _references = new List<string>();
        List<string> _definitions = new List<string>();
        IStackWalk _permissions = GetDefaultPermissions();

        public Compiler()
            :
            this(GetDotNet35CSharpProvider())
        {
        }

        public Compiler(CodeDomProvider provider)
            :
            this(new CompilerImpl(provider), new CodeGenerator(), new DomainUtil())
        {
        }

        internal Compiler(CompilerImpl compilerImpl, CodeGenerator codeGenerator, DomainUtil domainUtil)
        {
            _compilerImpl = compilerImpl;
            _codeGenerator = codeGenerator;
            _domainUtil = domainUtil;
        }

        public ReadOnlyCollection<string> Namespaces
        {
            get { return _namespaces.AsReadOnly(); }
        }

        public Compiler Using(string @namespace)
        {
            _namespaces.Add(@namespace);
            return this;
        }

        public Compiler Using(IEnumerable<string> namespaces)
        {
            _namespaces.AddRange(namespaces);
            return this;
        }

        public ReadOnlyCollection<string> References
        {
            get { return _references.AsReadOnly(); }
        }

        public Compiler Reference(string assembly)
        {
            _references.Add(assembly);
            return this;
        }

        public Compiler Reference(IEnumerable<string> assemblies)
        {
            _references.AddRange(assemblies);
            return this;
        }

        public Compiler ReferenceAllLoaded()
        {
            _references.AddRange(_domainUtil.GetNamesOfLoadedAssemblies());
            return this;
        }

        public ReadOnlyCollection<string> Definitions
        {
            get { return _definitions.AsReadOnly(); }
        }

        public Compiler Define(string source)
        {
            _definitions.Add(source);
            return this;
        }

        public Compiler Define(IEnumerable<string> sources)
        {
            _definitions.AddRange(sources);
            return this;
        }

        public IStackWalk Permissions
        {
            get { return _permissions; }
        }

        public Compiler SetPermissions(IStackWalk permissions)
        {
            _permissions = permissions;
            return this;
        }

        public void Clear()
        {
            _references.Clear();
            _definitions.Clear();
            _namespaces.Clear();
            _permissions = GetDefaultPermissions();
        }

        public object Eval(string expression)
        {
            return Eval(expression, null);
        }

        public object Eval(string expression, IDictionary<object, object> args)
        {
            var sources = _codeGenerator.GetSources(_code, expression, _namespaces, _definitions);
            _Assembly asm = _compilerImpl.Compile(sources, _references);
            var obj = (IComparer<IDictionary<object, object>>)asm.CreateInstance("Ikriv.Eval.GeneratedCode.Foo");
            var result = new Dictionary<object, object>();
            _permissions.PermitOnly();
            obj.Compare(args, result);
            return result[String.Empty];
        }

        private static CodeDomProvider GetDotNet35CSharpProvider()
        {
            return new CSharpCodeProvider(
                new Dictionary<string, string>() { {"CompilerVersion", "v3.5"} });
        }

        private static IStackWalk GetDefaultPermissions()
        {
            return new PermissionSet(PermissionState.None);
        }

        const string _code =
            "[assembly:System.Security.SecurityTransparent]\n" +
            "namespace Ikriv.Eval.GeneratedCode\n" +
            "{\n"+
            "   public class Foo : System.Collections.Generic.IComparer<System.Collections.Generic.IDictionary<object,object>>\n" +
            "   {\n" +
            "       public int Compare( System.Collections.Generic.IDictionary<object,object> args, System.Collections.Generic.IDictionary<object,object> result)\n" +
            "       {\n" +
            "           result[String.Empty] = ?;\n" +
            "           return 0;\n" +
            "       }\n" +
            "   }\n" +
            "}";
    }
}
