﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CSharp;
using System.Runtime.InteropServices;

namespace Ikriv.Eval
{
    internal class CompilerImpl : MarshalByRefObject
    {
        CodeDomProvider _provider;

        public CompilerImpl(CodeDomProvider provider)
        {
            _provider = provider;
        }

        public _Assembly Compile(IEnumerable<string> sources, IEnumerable<string> references)
        {
            CompilerParameters options = new CompilerParameters();
            options.GenerateInMemory = true;
            options.ReferencedAssemblies.AddRange(references.ToArray());
            return Check(_provider.CompileAssemblyFromSource(options, sources.ToArray()));
        }

        private static _Assembly Check(CompilerResults results)
        {
            if (results.Errors.Count > 0)
            {
                throw new CompilerException(results);
            }

            return results.CompiledAssembly;
        }

    }
}
