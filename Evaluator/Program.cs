﻿using System;
using Microsoft.CSharp;
using Ikriv.Eval;

namespace cseval
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage cseval expression");
            }
            else
            {
                try
                {
                    Console.WriteLine(new Compiler().Eval(args[0]));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
