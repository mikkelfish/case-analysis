﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace KatzExcel
{
    public class CheckWrapper : INotifyPropertyChanged
    {
        private bool check;
        public bool IsChecked 
        {
            get
            {
                return this.check;
            }
            set
            {
                this.check = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        private bool isReadOnly;
        public bool IsEnabled
        {
            get
            {
                return this.isReadOnly;
            }
            set
            {
                this.isReadOnly = value;
                this.OnPropertyChanged("IsEnabled");
            }
        }

        private object val;
        public object Value 
        {
            get
            {
                return this.val;
            }
            set
            {
                this.val = value;
                this.OnPropertyChanged("Value");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler ev = this.PropertyChanged;
            if (ev != null)
            {
                ev(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}
