﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace KatzExcel
{
    [ValueConversion(typeof(string), typeof(double[][]))]
    public class SegmentValueConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is double[][])) return "All";

            string toRet = "";
            double[][] segs = value as double[][];
            for (int i = 0; i < segs.Length; i++)
            {
                int minutes1 = (int)(segs[i][0] / 60);
                int seconds1 = (int)(segs[i][0] - minutes1 * 60);
                int minutes2 = (int)(segs[i][1] / 60);
                int seconds2 = (int)(segs[i][1] - minutes2 * 60);

                toRet += minutes1.ToString() + ":" + seconds1.ToString("D2") + "-" + 
                    minutes2.ToString() + ":" + seconds2.ToString("D2") + ",";
            }

            if (toRet.Length > 0)
            {
                toRet = toRet.Substring(0, toRet.Length - 1);
            }

            if (toRet == "-1.00--1.00") return "All";

            return toRet;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (!(value is string)) return value;

            string segmentStr = value as string;
            double[][] segs;
            if (segmentStr == "All")
            {
                segs = new double[1][] { new double[] { -1, -1 } };
            }
            else
            {

                string[] split = segmentStr.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                segs = new double[split.Length][];
                for (int i = 0; i < segs.Length; i++)
                {
                    segs[i] = new double[2];
                    string[] split2 = split[i].Split('-');
                    if (split2.Length != 2)
                        return null;

                    for (int j = 0; j < 2; j++)
                    {
                        double minutes = 0;
                        double seconds = 0;

                        string[] split3 = split2[j].Split(':');
                        if (!double.TryParse(split3[0], out minutes))
                            return null;
                        if (split3.Length == 2)
                        {
                            if (!double.TryParse(split3[1], out seconds))
                                return null;
                        }

                        segs[i][j] = minutes * 60.0 + seconds;

                    }
                }
            }

            return segs;
        }

        #endregion
    }
}
