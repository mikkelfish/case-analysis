﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Taramon.Exceller;
using System.Collections.ObjectModel;

namespace KatzExcel
{
    public class KatzBuxcoDataSet : ExcelEvaluation.ExcelDataSet
    {
        public string Subject { get; private set; }
        public string Site { get; private set; }
        public DateTime ReferenceTime { get; set; }
        public string File { get; set; }

        public double Length 
        {
            get
            {
                return this.timeInSeconds[this.timeInSeconds.Length - 1];
            }
             
        }

        private static readonly int[] spacing = new int[] { 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

        public static readonly bool[] IsEditable = new bool[] { false, false, false, true, true, true, true, true, true, true, true, true, 
            true, true, true, true, true, true, true, true, true, true, true, true };
        public static readonly string[] Fields =
            new string[]{
                "PeriodID",
                "Comment",
                "LogTime",
                "f",
                "TV",
                "AV",
                "MV",
                "Ti",
                "Te",
                "PIF", 
                "PEF",
                "RT",
                "EIP",
                "EEP",
                "dV",
                "EF50",
                "Rinx",
                "Comp",
                "Penh",
                "PAU",
                "Rpef",
                "RH",
                "Temp",
                "BodyTemp"
            };

        private double[] timeInSeconds;

        public double[] GetTimeSeries(double start, double end)
        {
            int index1 = -1;
            int index2 = -1;

            for (int i = 0; i < this.timeInSeconds.Length; i++)
            {
                if (index1 == -1)
                {
                    if (this.timeInSeconds[i] >= start)
                        index1 = i;
                }

                if (index2 == -1)
                {
                    if (this.timeInSeconds[i] >= end)
                        index2 = i;
                }
            }

            if (index1 == -1) return null;
            if (index2 == -1) index2 = this.timeInSeconds.Length - 1;

            double[] toRet = new double[index2 - index1];
            Array.Copy(this.timeInSeconds, index1, toRet, 0, toRet.Length);
            return toRet;
        }

        public double[][] GetData(double start, double end, string[] channels)
        {
            int index1 = -1;
            int index2 = -1;

            for (int i = 0; i < this.timeInSeconds.Length; i++)
            {
                if (index1 == -1)
                {
                    if (this.timeInSeconds[i] >= start)
                        index1 = i;
                }

                if (index2 == -1)
                {
                    if (this.timeInSeconds[i] >= end)
                        index2 = i;
                }
            }

            if (index1 == -1) return null;
            if (index2 == -1) index2 = this.timeInSeconds.Length - 1;

            double[][] toRet = new double[channels.Length][];
            for (int i = 0; i < toRet.Length; i++)
            {
                toRet[i] = new double[index2 - index1];
                ReadOnlyCollection<string> ro = base.GetFormattedData(channels[i]);
                for (int j = 0; j < toRet[i].Length; j++)
                {
                    toRet[i][j] = double.Parse(ro[j + index1]);
                }
            }

            return toRet;
        }

        public void Load(string file, string[] fields)
        {
            List<string> realFields = new List<string>(fields);

            if (!fields.Contains("Ti"))
            {
                realFields.Add("Ti");
            }

            if (!fields.Contains("Te"))
            {
                realFields.Add("Te");

            }

            int[] f = new int[realFields.Count];
            for (int i = 0; i < realFields.Count; i++)
            {
                for (int j = 0; j < KatzBuxcoDataSet.Fields.Length; j++)
                {
                    if (realFields[i] == KatzBuxcoDataSet.Fields[j])
                        f[i] = j;
                }
            }

            f = f.OrderBy(i => i).ToArray();
            this.Load(file, f);

        }

        public void Load(string file, int[] fields)
        {
            this.File = file;

            string basecell = "D";
            int firstField = fields[0];
            int toAdd = 0;
            for (int i = 0; i < firstField; i++)
            {
                toAdd += KatzBuxcoDataSet.spacing[i];
            }

            char newcell = (char)((int)basecell[0] + toAdd);
            if (newcell > 'Z')
            {
                basecell = "A" + (char)((int)'A' + ((int)newcell - (int)'Z' - 1));
            }
            else basecell = new string(newcell, 1);

            basecell += "2";

            int[] realSpacing = new int[fields.Length];
            string[] fieldNames = new string[fields.Length];
            for (int i = 0; i < fieldNames.Length; i++)
            {
                fieldNames[i] = KatzBuxcoDataSet.Fields[fields[i]];
            }

            int running = 0;
            int cur = fields[1];
            int read = 0;
            for (int i = firstField; i < spacing.Length; i++)
            {
                if (cur == i)
                {
                    realSpacing[read] = running;
                    running = spacing[i];
                    if (read + 2 < fields.Length)
                        cur = fields[read + 2];
                    else realSpacing[fields.Length - 1] = 1;
                    read++;
                }
                else running += spacing[i];
            }

            base.FillAdapter(fieldNames, file, "WBP_Compensated1_Data", basecell, ExcelEvaluation.ExcelDataOrientation.Horizontal, -1, realSpacing);

            using (ExcelManager manager = new ExcelManager())
            {
                manager.Open(file);
                this.Subject = manager.GetFormattedValue("A2").ToString();
                this.Site = manager.GetFormattedValue("B2").ToString();
                // this.ReferenceTime = DateTime.Parse(manager.GetFormattedValue("G2").ToString());
            }

            ReadOnlyCollection<string> ti = base.GetFormattedData("Ti");
            ReadOnlyCollection<string> te = base.GetFormattedData("Te");            
            this.timeInSeconds = new double[ti.Count];
            double totalTime = 0;
            for (int i = 0; i < this.timeInSeconds.Length; i++)
            {
                //string formatted = log[i];
                //if (formatted.Length == 9)
                //    formatted = "0" + formatted;
                //this.timeInSeconds[i] = TimeSpan.Parse(formatted).TotalSeconds;
                this.timeInSeconds[i] = totalTime;
                totalTime += double.Parse(ti[i]) + double.Parse(te[i]);
            }
        }

        public void Load(string file)
        {
            int[] fields = new int[KatzBuxcoDataSet.Fields.Length];
            for (int i = 0; i < fields.Length; i++)
            {
                fields[i] = i;
            }

            this.Load(file, fields);
        }
    }
}
