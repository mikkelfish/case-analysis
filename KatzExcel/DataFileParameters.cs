﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KatzExcel
{
    public class DataFileParameters
    {
        public string File { get; set; }

        public double[][] Segments
        {
            get;
            set;
        }
    }
}
