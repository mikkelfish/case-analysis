﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace KatzExcel
{
    public class SegmentValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value is string)
            {
                string str = value as string;
                if (str == "All") return new ValidationResult(true, "");
                string[] splits = str.Split(',');
                foreach (string split in splits)
                {
                    string[] split2 = split.Split('-');
                    if (split2.Length != 2)
                        return new ValidationResult(false, "Error in formatting. Formatting is 'time1-time1b,time2-time2b,...')");

                    double val1 = 0;
                    double val2 = 0;
                    bool ok = true;
                    for (int j = 0; j < 2; j++)
                    {
                        double minutes = 0;
                        double seconds = 0;

                        string[] split3 = split2[j].Split(':');
                        if (!double.TryParse(split3[0], out minutes))
                            ok = false;
                        if (split3.Length == 2)
                        {
                            if (!double.TryParse(split3[1], out seconds))
                                ok = false;
                        }

                        if (j == 0) val1 = minutes * 60 + seconds;
                        else val2 = minutes * 60 + seconds;
                    }


                    if (!ok)
                        return new ValidationResult(false, "Error in segment formatting");
                    if (val2 <= val1)
                        return new ValidationResult(false, "Times in segment must be ascending");
                   
                }

            }
            return new ValidationResult(true, "");
        }
    }
}
