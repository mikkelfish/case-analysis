﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace KatzExcel
{
    [ValueConversion(typeof(string), typeof(string))]
    public class FilenameValueConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if(value is string)
            {
                string val = value as string;
                int indexof = val.LastIndexOf('\\');
                if(indexof < 0) return val;

                return val.Substring(indexof + 1);
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
