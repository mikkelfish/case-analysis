﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Collections.ObjectModel;
using System.IO;
using PlottingLibrary;

namespace KatzExcel
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            this.Settings = SeriesSettings.GetDefaultSeriesSettings();

            InitializeComponent();

            
            for (int i = 0; i < KatzBuxcoDataSet.Fields.Length; i++)
            {
                this.checkers.Add(new CheckWrapper()
                {
                    IsChecked = !KatzBuxcoDataSet.IsEditable[i],
                    Value = KatzBuxcoDataSet.Fields[i],
                    IsEnabled = KatzBuxcoDataSet.IsEditable[i]
                });

                this.checkers[i].PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(Window1_PropertyChanged);
            }
        }

        void Window1_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            CheckWrapper wrapper = sender as CheckWrapper;
            if (wrapper == null) return;
            if (wrapper.IsChecked)
                this.Axes.Add(new Axis() { Label = wrapper.Value as string, Limit = new AxisLimit() });
            else this.axes.Remove(this.axes.Single(a => a.Label == wrapper.Value as string));
        }



        public SeriesSettings Settings
        {
            get { return (SeriesSettings)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Settings.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings", typeof(SeriesSettings), typeof(Window1));




        public int NumberSub
        {
            get { return (int)GetValue(NumberSubProperty); }
            set { SetValue(NumberSubProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NumberSub.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberSubProperty =
            DependencyProperty.Register("NumberSub", typeof(int), typeof(Window1), new PropertyMetadata(10), new ValidateValueCallback(validateMaximum));



        public double Tolerance
        {
            get { return (double)GetValue(ToleranceProperty); }
            set { SetValue(ToleranceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Tolerance.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ToleranceProperty =
            DependencyProperty.Register("Tolerance", typeof(double), typeof(Window1), new PropertyMetadata(0.1), new ValidateValueCallback(validateMaximum));




        public double MaximumWindow
        {
            get { return (double)GetValue(MaximumWindowProperty); }
            set { SetValue(MaximumWindowProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaximumWindow.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaximumWindowProperty =
            DependencyProperty.Register("MaximumWindow", typeof(double), typeof(Window1), new PropertyMetadata(30.0), new ValidateValueCallback(validateMaximum));




        public double MaxFreq
        {
            get { return (double)GetValue(MaxFreqProperty); }
            set { SetValue(MaxFreqProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaxFreq.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxFreqProperty =
            DependencyProperty.Register("MaxFreq", typeof(double), typeof(Window1), new PropertyMetadata(300.0), new ValidateValueCallback(validateMaximum));



        private static bool validateMaximum(object val)
        {
            double win = Convert.ToDouble(val);
            if (win <= 0) return false;

            return true;
        }

        private ObservableCollection<Axis> axes = new ObservableCollection<Axis>();
        public ObservableCollection<Axis> Axes { get { return this.axes; } }

        private ObservableCollection<DataFileParameters> files = new ObservableCollection<DataFileParameters>();
        public ObservableCollection<DataFileParameters> Files { get { return this.files; } }

        private ObservableCollection<CheckWrapper> checkers = new ObservableCollection<CheckWrapper>();
        public ObservableCollection<CheckWrapper> Checked { get { return this.checkers; } }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dia = new OpenFileDialog();
            if (dia.ShowDialog().Value)
            {
                KatzBuxcoDataSet ds = new KatzBuxcoDataSet();
                ds.Load(dia.FileName);                
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dia = new OpenFileDialog();
            dia.Multiselect = true;
            if (dia.ShowDialog().Value)
            {
                foreach (string str in dia.FileNames)
                {
                    if (!this.Files.Any(f => f.File == str))
                        this.files.Add(new DataFileParameters(){File=str});
                }
            }
        }

        private void addFolder(string folder, bool recurse)
        {
            string[] files = Directory.GetFiles(folder, "*.xls*");
            foreach (string str in files)
            {
                if (!this.files.Any(f => f.File == str))
                    this.files.Add(new DataFileParameters() { File = str });
            }

            if (recurse)
            {
                string[] subfolders = Directory.GetDirectories(folder);
                foreach (string fold in subfolders)
                {
                    this.addFolder(fold, recurse);
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = System.Windows.MessageBox.Show("Include subfolders?", "Include subfolders", MessageBoxButton.YesNoCancel);

            if(res== MessageBoxResult.Cancel)return;

            System.Windows.Forms.FolderBrowserDialog dia = new System.Windows.Forms.FolderBrowserDialog();
            if (dia.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string folder = dia.SelectedPath;
                this.addFolder(folder, res == MessageBoxResult.Yes);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            DataFileParameters[] selected = this.fileList.SelectedItems.Cast<DataFileParameters>().ToArray();
            foreach (DataFileParameters str in selected)
            {
                this.Files.Remove(str);
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            foreach (CheckWrapper check in this.checkers)
            {
                check.IsChecked = true;
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            foreach (CheckWrapper check in this.checkers)
            {
                check.IsChecked = false;
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Save graphs to source directories (Yes) or new destination (No)", "Where to save?", MessageBoxButton.YesNoCancel);
            if (res == MessageBoxResult.Cancel) return;

            string dir = "";
            if (res == MessageBoxResult.No)
            {
                System.Windows.Forms.FolderBrowserDialog dia = new System.Windows.Forms.FolderBrowserDialog();
                if (dia.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    dir = dia.SelectedPath;
                }
                else return;
            }

            foreach (DataFileParameters para in this.fileList.Items)
            {
                string[] tofill = this.checkers.Where(c => c.IsChecked).Select(s => s.Value).Cast<string>().ToArray();
                string[] toPlot = this.checkers.Where(c => c.IsEnabled && c.IsChecked).Select(s => s.Value).Cast<string>().ToArray();
                if (tofill.Length == this.checkers.Where(c => !c.IsEnabled).Count())
                {
                    MessageBox.Show("Please select at least one series to plot.");
                    return;
                }
                
                KatzBuxcoDataSet set = new KatzBuxcoDataSet();
                set.Load(para.File, tofill);

                string target = dir;
                if (target == "")
                {
                    target = set.File.Substring(0, set.File.LastIndexOf('\\') + 1) + "Results\\";
                    if (!Directory.Exists(target))
                        Directory.CreateDirectory(target);
                }

                Helpers.CreatePlots(target, new KatzBuxcoDataSet[] { set }, toPlot, this.axes.ToArray(),  this.Settings, 
                    new double[][][] { para.Segments }, this.MaximumWindow*60.0, this.NumberSub, this.Tolerance, this.MaxFreq/60.0);

            }

            MessageBox.Show("Plots created.");
        }

        
    }
}
