﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PlottingLibrary;
using System.IO;

namespace KatzExcel
{
    public static class Helpers
    {
        public static void CreatePlots(string dir, KatzBuxcoDataSet[] sets, string[] toPlot, Axis[] limits, SeriesSettings settings, double[][][] times, double maxWidth, int numDivs, double tolerance, double maxFreq)
        {
            for (int i = 0; i < sets.Length; i++)
            {
                double[][] t;
                if (times == null || times[i] == null || times[i].Length == 0)
                {
                    t = new double[][] { new double[]{ 0, sets[i].Length }};
                }
                else t = times[i];

                //Partition if needed
                List<double[]> realTimes = new List<double[]>();
                for (int j = 0; j < t.Length; j++)
                {
                    if (t[j][1] - t[j][0] < maxWidth)
                    {
                        realTimes.Add(t[j]);
                        continue;
                    }

                    double total = t[j][1] - t[j][0];
                    int numParts = (int)(total / maxWidth) + 1;
                    for (int k = 0; k < numParts; k++)
                    {
                        double[] newPart = new double[2];
                        newPart[0] = maxWidth*k + t[j][0];
                        newPart[1] = maxWidth*(k+1) + t[j][0];

                        if (newPart[1] > t[j][1])
                            newPart[1] = t[j][1];
                        realTimes.Add(newPart);
                    }

                }

                t = realTimes.ToArray();

                for (int j = 0; j < t.Length; j++)
                {
                    double[][] data = sets[i].GetData(t[j][0], t[j][1], toPlot);
                    if (data == null) return;

                    for (int k = 0; k < data.Length; k++)
                    {
                        LinePlot plot = new LinePlot() { Title = sets[i].Subject + " " + sets[i].Site};
                        plot.XAxis = new Axis() { Label = "Time", Units = "Minutes" };
                        plot.YAxis = new Axis() { Label = toPlot[k], Limit=limits.Single(l => l.Label == toPlot[k]).Limit };
                        plot.SeriesSettings.Add(settings);
                        
                        
                        string path = dir;
                        if (path != null && Directory.Exists(path))
                        {
                            if (path[path.Length - 1] != '\\')
                                path += "\\";

                            string filePart = sets[i].File.Substring(sets[i].File.LastIndexOf('\\') + 1);
                            filePart = filePart.Substring(0, filePart.IndexOf('.') - 1);
                            path += filePart + " Variable -- " + toPlot[k] + " Time -- " + t[j][0] + "-" + t[j][1];
                        }

                        double[] timesSer = sets[i].GetTimeSeries(t[j][0], t[j][1]);
                        
                        

                        double minTime = 1.0 / maxFreq;
                       

                        int numValidTimes = 0;
                        double averageTime = 0;

                        for (int l = 0; l < timesSer.Length - 1; l++)
                        {
                            double timeSpan = timesSer[l + 1] - timesSer[l];

                            if (timeSpan< minTime)
                            {
                                data[k][l] = 0;
                            }
                            else
                            {
                                numValidTimes++;
                                averageTime += timeSpan;
                            }                            
                        }

                        averageTime /= numValidTimes;

                        int numberApenas = 0;
                        double apenaAvg = 0;
                        double[] te = sets[i].GetData(t[j][0], t[j][1], new string[] { "Te" })[0];

                        for (int l = 0; l < te.Length - 1; l++)
                        {
                            if (te[l] > 2 * averageTime)
                            {
                                data[k][l] = 0;
                                numberApenas++;
                                apenaAvg += timesSer[l + 1] - timesSer[l];
                            }
                        }

                        apenaAvg /= numberApenas;
                        
                        SeriesData series = new SeriesData()
                        {
                            X = timesSer.Select(s => s / 60.0).ToArray(),
                            Y=data[k]
                        };

                        double[] statedData = data[k].Where(d => d != 0).ToArray();
                        double[][] stats;
                        bool[] byseg;
                        StatisticalAnalysis.StatisticsDelegate[] dels = new StatisticalAnalysis.StatisticsDelegate[3];
                        dels[0] = new StatisticalAnalysis.StatisticsDelegate(StatisticalAnalysis.Stats.Average);
                        dels[1] = new StatisticalAnalysis.StatisticsDelegate(StatisticalAnalysis.Stats.StandardDev);
                        dels[2] = new StatisticalAnalysis.StatisticsDelegate(StatisticalAnalysis.Stats.CoefficentOfVariation);
                        bool ok = StatisticalAnalysis.Stationarity.TestStationarity(statedData, numDivs, tolerance, dels, out stats, out byseg);


                        List<string> info = new List<string>();

                        info.Add("Average: " + stats[0][0].ToString("F"));
                        info.Add("Std Dev: " + stats[0][1].ToString("F"));
                        info.Add("CV: " + stats[0][2].ToString("F"));

                        info.Add("Is stationary: " + ok);

                        string notStationary = "Not stationary: ";
                        for (int l   = 0; l < byseg.Length; l++)
                        {
                            if (!byseg[l])
                                notStationary += (l + 1) + ",";
                        }

                        if (notStationary[notStationary.Length - 1] == ',')
                        {
                            notStationary = notStationary.Substring(0, notStationary.Length - 1);
                            info.Add(notStationary);
                        }

                        info.Add("Number Valid Breaths: " + statedData.Length + " Valid Percentage: " + ((double)(statedData.Length + numberApenas) / (double)timesSer.Length).ToString("F2"));
                        info.Add("Number Apenas: " + numberApenas.ToString() + " Average Apena: " + apenaAvg.ToString("F2")); 

                        plot.Information = info.ToArray();

                        plot.Plot(new string[] { path }, new SeriesData[]{series});
                    }
                }
            }
        }
    }
}
