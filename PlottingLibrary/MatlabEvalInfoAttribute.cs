﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlottingLibrary
{
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public sealed class MatlabEvalInfoAttribute : Attribute
    {
        public MatlabEvalInfoAttribute(string positionalString)
        {
            this.Text = positionalString;

        }

        public string Text { get; private set; }

    }
}
