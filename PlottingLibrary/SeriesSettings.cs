﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlottingLibrary
{
    public class SeriesSettings
    {
        public static SeriesSettings GetDefaultSeriesSettings()
        {
            SeriesSettings toRet = new SeriesSettings();
            toRet.Style = LineStyle.GetDefaultStyle();
            toRet.Label = "Default";
            return toRet;
        }

        public string Label { get; set; }
        public LineStyle Style { get; set; }
    }
}
