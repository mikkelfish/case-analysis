﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlottingLibrary
{
    public class SeriesData
    {
        public double[] X { get; set; }
        public double[] Y { get; set; }
        public double[] Z { get; set; }
    }
}
