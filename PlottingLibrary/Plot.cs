﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace PlottingLibrary
{
    public class LinePlot
    {
        public string Title { get; set; }
        public bool ShowLegend { get; set; }
        
        public Axis XAxis { get; set; }
        public Axis YAxis { get; set; }
        public Axis ZAxis { get; set; }

        private ObservableCollection<SeriesSettings> series = new ObservableCollection<SeriesSettings>();
        public ObservableCollection<SeriesSettings> SeriesSettings { get { return this.series; } }

        public string[] Information { get; set; }

        public void Plot(string[] path, SeriesData[] data)
        {
            for (int i = 0; i < path.Length; i++)
			{
                string styleStr = "";
                System.Reflection.FieldInfo field = this.series[i].Style.LineSpecifier.GetType().GetField(this.series[i].Style.LineSpecifier.ToString());

                MatlabEvalInfoAttribute attr = field.GetCustomAttributes(typeof(MatlabEvalInfoAttribute), true).Cast<MatlabEvalInfoAttribute>().ToArray()[0];
                styleStr += attr.Text;

                styleStr += LineStyle.GetColorMatlab(this.series[i].Style.LineColor);

                field = this.series[i].Style.MarkerStyle.GetType().GetField(this.series[i].Style.MarkerStyle.ToString());
                attr = field.GetCustomAttributes(typeof(MatlabEvalInfoAttribute), true).Cast<MatlabEvalInfoAttribute>().ToArray()[0];
                styleStr += attr.Text;

                string xlabel = this.XAxis.Label;
                if (this.XAxis.Units != null)
                    xlabel +=  " ("+this.XAxis.Units+")";

                string ylabel = this.YAxis.Label;
                if (this.YAxis.Label != null)
                    ylabel += " ("+this.YAxis.Units+")";

                double[] limits = new double[4];
                if (this.XAxis == null || this.XAxis.Limit == null || this.XAxis.Limit.Min == this.XAxis.Limit.Max)
                {
                    limits[0] = data[i].X.Min();
                    limits[1] = data[i].X.Max();
                }
                else
                {
                    limits[0] = this.XAxis.Limit.Min;
                    limits[1] = this.XAxis.Limit.Max;
                }
                if (this.YAxis == null || this.YAxis.Limit == null || this.YAxis.Limit.Min == this.YAxis.Limit.Max)
                {
                    limits[2] = data[i].Y.Min();
                    limits[3] = data[i].Y.Max();
                }
                else
                {
                    limits[2] = this.YAxis.Limit.Min;
                    limits[3] = this.YAxis.Limit.Max;
                }

                MatlabInterface.MakeGraph(path[i], this.Title, xlabel, ylabel, limits,
                    data[i].X, data[i].Y, styleStr, this.series[i].Style.Width, this.series[i].Style.MarkerSize,
                    LineStyle.GetColorMatlab(this.series[i].Style.MarkerFaceColor), LineStyle.GetColorMatlab(this.series[i].Style.MarkerEdgeColor), this.Information);			 
			}                
        }
    }
}
