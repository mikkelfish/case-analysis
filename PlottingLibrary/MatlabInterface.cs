﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathWorks.MATLAB.NET.Arrays;


namespace PlottingLibrary
{
    public static class MatlabInterface
    {
        private static object matlabLockable = new object();
        public static object MatlabLockable
        {
            get
            {
                return matlabLockable;
            }
        }

        private static evaluator.evaluatorclass matlab;
        public static evaluator.evaluatorclass Matlab
        {
            get
            {
                lock (matlabLockable)
                {
                    if (matlab == null)
                        matlab = new evaluator.evaluatorclass();

                    return matlab;
                }
            }
        }

        public static void MakeGraph(string path, string title, string xlabel, string ylabel, double[] lim,
            double[] x, double[] y, string lineStyle, double lineWidth, double markerSize, string markerColor, string markerEdgeColor, string[] info)
        {
            evaluator.evaluatorclass ev = Matlab;
            lock (MatlabLockable)
            {
                MWCharArray p = new MWCharArray(path);
                MWCharArray t = new MWCharArray(title);
                MWCharArray xl = new MWCharArray(xlabel);
                MWCharArray yl = new MWCharArray(ylabel);
                MWNumericArray limits = lim == null ? new MWNumericArray() : new MWNumericArray(lim);
                MWNumericArray xdata = new MWNumericArray(x);
                MWNumericArray ydata = new MWNumericArray(y);
                MWCharArray style = new MWCharArray(lineStyle);
                MWNumericArray lW = new MWNumericArray(lineWidth);
                MWNumericArray mS = new MWNumericArray(markerSize);
                MWCharArray mc = new MWCharArray(markerColor);
                MWCharArray mec = new MWCharArray(markerEdgeColor);
                MWCharArray infos = new MWCharArray(info);


                try
                {
                    ev.plotMatlab(0, p, t, xl, yl, limits, xdata, ydata, style, lW, mS, mc, mec, infos);
                }
                finally
                {
                    p.Dispose();
                    t.Dispose();
                    limits.Dispose();
                    xl.Dispose();
                    yl.Dispose();
                    xdata.Dispose();
                    ydata.Dispose();
                    style.Dispose();
                    lW.Dispose();
                    mS.Dispose();
                    mc.Dispose();
                    mec.Dispose();
                    infos.Dispose();
                }
            }

        }
    }
}
