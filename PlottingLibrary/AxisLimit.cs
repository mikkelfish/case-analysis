﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlottingLibrary
{
    public enum AxisType { Automatic, Manual };
    public class AxisLimit
    {
        public double Min { get; set; }
        public double Max { get; set; }
        public AxisType LimitType { get; set; }
    }
}
