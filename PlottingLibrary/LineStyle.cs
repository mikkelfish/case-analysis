﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PlottingLibrary
{
    public enum LineStyleSpecifier { 
        [MatlabEvalInfo("-")]Solid, 
        [MatlabEvalInfo("--")] Dashed, 
        [MatlabEvalInfo(":")] Dotted, 
        [MatlabEvalInfo("-.")] Dash_Dot, 
        [MatlabEvalInfo("")] None 
    };
    
    public enum MarkerStyle { 
       [MatlabEvalInfo("+")]Plus, 
       [MatlabEvalInfo("o")] Circle, 
       [MatlabEvalInfo("*")] Asterisk, 
       [MatlabEvalInfo(".")] Point, 
       [MatlabEvalInfo("x")] Cross, 
       [MatlabEvalInfo("s")] Square, 
       [MatlabEvalInfo("d")] Diamond, 
       [MatlabEvalInfo("^")] Upward, 
       [MatlabEvalInfo("v")] Downward, 
       [MatlabEvalInfo(">")] Right, 
       [MatlabEvalInfo("<")] Left, 
       [MatlabEvalInfo("p")] Pentagram, 
       [MatlabEvalInfo("h")] Hexagram,
       [MatlabEvalInfo("")] None
    };

    public class LineStyle
    {
        public static readonly Color[] CommonColors = new Color[]{
            Color.Black, Color.Red, Color.Blue, Color.Green, Color.Cyan, Color.Yellow, Color.Magenta, Color.White
        };

        public static string GetColorMatlab(Color c)
        {
            if (c == CommonColors[0])
                return "k";
            if (c == CommonColors[1])
                return "r";
            if (c == CommonColors[2])
                return "b";
            if (c == CommonColors[3])
                return "g";
            if (c == CommonColors[4])
                return "c";
            if (c == CommonColors[5])
                return "y";
            if (c == CommonColors[6])
                return "m";
            if (c == CommonColors[7])
                return "w";

            return "k";
        }

        private double width;
        public double Width 
        {
            get
            {
                return this.width;
            }
            set
            {
                if ((double)value < 0)
                    throw new Exception("Line width cannot be less than zero");
                this.width = value;
            }
        }

        public Color LineColor { get; set; }
        public Color MarkerEdgeColor { get; set; }
        public Color MarkerFaceColor { get; set; }

        private double markerSize;
        public double MarkerSize 
        {
            get
            {
                return this.markerSize;
            }
            set
            {
                if ((double)value < 0)
                    throw new Exception("Marker size cannot be less than zero");
                this.markerSize = value;
            }
        }
        
        public LineStyleSpecifier LineSpecifier { get; set; }
        public MarkerStyle MarkerStyle { get; set; }

        public static LineStyle GetDefaultStyle()
        {
            LineStyle toRet = new LineStyle();
            toRet.Width = 1;
            toRet.MarkerSize = 2;
            toRet.LineSpecifier = LineStyleSpecifier.None;
            toRet.MarkerStyle = MarkerStyle.Circle;
            toRet.LineColor = Color.Black;
            toRet.MarkerEdgeColor = Color.Black;
            toRet.MarkerFaceColor = Color.Black;
            
            return toRet;
        }
    }
}
