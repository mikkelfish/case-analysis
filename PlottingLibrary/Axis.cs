﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlottingLibrary
{
    public class Axis
    {
        public string Units { get; set; }
        public string Label { get; set; }
        public double[] Ticks { get; set; }
        public string[] TickLabels { get; set; }
        public AxisLimit Limit { get; set; }

        public override string ToString()
        {
            return this.Label;
        }
    }
}
