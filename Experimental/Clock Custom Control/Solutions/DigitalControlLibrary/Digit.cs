﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalControlLibrary
{
    public class Digit : Control
    {
        private byte lights;


        public Digit()
        {
            lights = 0;
        }

        public Digit(byte initialLights)
        {
            lights = initialLights;
        }

        public Digit(bool top, bool middle, bool bottom, bool topLeft, bool topRight, bool bottomLeft,
                        bool bottomRight)
        {
            byte runningValue = 0;

            if (top)
                runningValue += 1;
            if (topLeft)
                runningValue += 2;
            if (topRight)
                runningValue += 4;
            if (middle)
                runningValue += 8;
            if (bottomLeft)
                runningValue += 16;
            if (bottomRight)
                runningValue += 32;
            if (bottom)
                runningValue += 64;

            lights = runningValue;
        }


        



    }
}
