
#include "sampleEntropy.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

__constant__ int deviceJumpArray[JUMP_ARRAY_WIDTH];
__constant__ int deviceTauArray[TAU_ARRAY_WIDTH];

texture<float, 1, cudaReadModeElementType> texRef;
texture<unsigned int, 1, cudaReadModeElementType> resultTexRef;

extern "C"
void generateTauTable(int desiredM, int tauStart, int numberOfTaus)//, int** jumpToIndexArray, int** tausEffectedArray)
{
	tauStart++;
	int indexer = 0;
	int* jumpToIndexArray = (int*) malloc (JUMP_ARRAY_WIDTH * sizeof(int));
	int* tausAffectedArray = (int*) malloc (TAU_ARRAY_WIDTH * sizeof(int));

	memset(jumpToIndexArray,0,JUMP_ARRAY_WIDTH * sizeof(int));
	memset(tausAffectedArray,0,TAU_ARRAY_WIDTH * sizeof(int));

	for(int i = 1; i <= ((desiredM - 1) * (tauStart + numberOfTaus)); i++)
	{
		int tauIndex = 0;
		
		for(int j = 0; j < numberOfTaus; j++)
		{			
			int thisTau = tauStart + j;


			if(i % thisTau == 0)
			{
				if(tauIndex >= TAU_TABLE_DEPTH)
				{
					//printf("Tau precalculation table is only %i elements deep, insufficient depth to continue.",TAU_TABLE_DEPTH);
					exit(1);
				}

				if(i / thisTau < (desiredM - 1))
				{
					tausAffectedArray[indexer + JUMP_ARRAY_WIDTH * tauIndex] = thisTau;
					tauIndex++;
				}
				else if(i / thisTau == (desiredM - 1))
				{
					tausAffectedArray[indexer + JUMP_ARRAY_WIDTH * tauIndex] = (thisTau | 0x80000000);
					tauIndex++;
				}
			}
		}


		if (tauIndex > 0)
		{
			jumpToIndexArray[indexer] = i;
			indexer++;
		}
	}

	cudaMemcpyToSymbol(deviceJumpArray, jumpToIndexArray, JUMP_ARRAY_WIDTH * sizeof(int), 0);
	cudaMemcpyToSymbol(deviceTauArray, tausAffectedArray, TAU_ARRAY_WIDTH * sizeof(int), 0);

	free(jumpToIndexArray);
	free(tausAffectedArray);
}




//numberOfTaus MUST be 32 or less.
__global__ void basicSampleEntropy(unsigned int* result, int m, float r, int dataLength)
{	
	volatile __shared__ unsigned int scanBank[3072];

	register int tauApack = 0;
	register int tauBpack = 0;

	int loopI = 0; //*
	int loopJ = 0; //* 

	//int offI = 0;
	//int offJ = 0; 

	//int xCoordinate = blockIdx.x * threadCount + threadIdx.x; // can get rid of these
	//int yCoordinate = blockIdx.y; //get rid of these
	bool rightSide = true; //get rid of these

	//volatile int jump = 0; // get rid of this

	for(int jump = 0; jump < 3072 / 192; jump++)
	{
		scanBank[threadIdx.x + jump * 192] = 0;
	}


	if (/*xCoordinate*/ blockIdx.x * threadCount + threadIdx.x > /*yCoordinate*/ blockIdx.y)
	{
		loopI = /*yCoordinate*/ blockIdx.y;
		loopJ = /*xCoordinate*/ blockIdx.x * threadCount + threadIdx.x;
		rightSide = true; //formerely rightside
	}
	else
	{
		loopI = dataLength - /*yCoordinate*/blockIdx.y - 2; 
		loopJ = loopI + /*xCoordinate*/blockIdx.x * threadCount + threadIdx.x + 1;
		rightSide = false; //formerely rightside
	}

	/*offI = loopI;
	offJ = loopJ;*/

	__syncthreads();

	if (loopI < dataLength && loopJ < dataLength && (rightSide || (/*yCoordinate*/blockIdx.y + 1) != dataLength / 2.0) || dataLength % 2 == 1)
	{
		tauApack = 0;

		//Compare the first points and write to all taus
		float val1 = tex1Dfetch(texRef, loopI); //maybe remove?
 		float val2 = tex1Dfetch(texRef, loopJ); //maybe remove?
		int fail = val1 - val2 > r || val2 - val1 > r;
		
		if(fail) tauBpack = 0xFFFFFFFF;
		else tauBpack = 0;

		int offI = loopI + deviceJumpArray[0];
		int offJ = loopJ + deviceJumpArray[0];

		//Compare the rest of the points
		for(volatile int jump = 0; jump < (m - 1) * 32; jump++)
		{
			val1 = tex1Dfetch(texRef, offI); 
			val2 = tex1Dfetch(texRef, offJ);
			fail = offI >= dataLength  
				|| offJ >= dataLength 
				|| val1 - val2 > r 
				|| val2 - val1 > r;

			offI = loopI + deviceJumpArray[jump + 1];
			offJ = loopJ + deviceJumpArray[jump + 1];

			//Move to new function?
			int tauJump = 0;
			//int thisIndex = deviceTauArray[jump];
			int isLast = 0;
			int realIndex = deviceTauArray[jump];

			while(realIndex != 0)
			{		
				isLast = realIndex < 0;
				realIndex = (realIndex & 0x0FFFFFFF) - 1;
				tauBpack |= ((fail && !isLast) || ((!isLast || (isLast && jump == 0)) && (offJ + realIndex) >= dataLength)) << (realIndex % 32);
				tauApack |= ((fail && isLast) << (realIndex % 32));
				
				tauJump++;
				realIndex = deviceTauArray[JUMP_ARRAY_WIDTH * tauJump + jump];
			}
		}

		// using XOR (^) here to invert the bits to positive value (0 = false, 1 = true)
		tauBpack ^= 0xFFFFFFFF;
		tauApack ^= 0xFFFFFFFF;

		for(volatile int jump = 0; jump < 16; jump++)
		{
			scanBank[threadIdx.x * 16 + jump] = (
													(((tauBpack >> (jump*2 )) & 1) << 8) |
													(((tauBpack >> (jump*2 + 1 )) & 1) << 24) |
													(((tauApack >> (jump * 2)) & 1) & (tauBpack >> (jump*2) & 1)) |
													((((tauApack >> (jump * 2 + 1)) & 1) & (tauBpack >> (jump*2 + 1) & 1)) << 16)
												 );			
		}
	}

	__syncthreads();

	// Divide into banks
	//threadIdx.x / 12
	register unsigned int runningAdd = 0;
	for(volatile int jump = 0; jump < 16; jump++)
	{
		runningAdd += scanBank[((threadIdx.x / 16) * 256) + (threadIdx.x % 16) + (jump * 16)];
	}
	
	scanBank[((threadIdx.x / 16) * 256) + (threadIdx.x % 16)] = runningAdd;

	__syncthreads();

	runningAdd = 0;
	if(threadIdx.x < 16)
	{
		for(volatile int jump = 0; jump < 12; jump++)
		{
			runningAdd += scanBank[(threadIdx.x % 16) + (jump * 256)];
		}

		result[(blockIdx.x + (gridDim.x * blockIdx.y)) * 16 + threadIdx.x] = runningAdd;		
	}
	__syncthreads();
}

extern "C" 
void basicSampEn(unsigned int* result, int m, float r, int dataLength, dim3 grid, dim3 threads)
{
	basicSampleEntropy<<< grid, threads >>>(result, m, r, dataLength);
}


__global__ void combineValues(int dataLength, int*combineResult)
{
	int isB = blockIdx.y % 2;
	int thistau = (threadIdx.x % 16) * 2 + blockIdx.y/2;
	int whichSubBlock = threadIdx.x / 16;
	unsigned int value = 0;
	int offsetMask = 0xFF << (blockIdx.y * 8);
	int xSplit = (dataLength*16)/192;
	volatile unsigned int runningAdd = 0;
	volatile int jump = 0;

//	__syncthreads();


	for(jump = 0; jump < xSplit; jump++)
	{
		value = tex1Dfetch(resultTexRef, threadIdx.x + jump * 192);
		runningAdd += (value & offsetMask) >> (blockIdx.y * 8);
	}

	__syncthreads();


	//Clamp down to 16 threads for the leftovers
	if(threadIdx.x / 16 == 0)
	{
		for(jump = 0; jump < (dataLength*16 - (xSplit * 192))/16; jump++)
		{
			value = tex1Dfetch(resultTexRef, xSplit * 192 + (jump * 16) + (threadIdx.x % 16) );	
			runningAdd += (value & offsetMask) >> (blockIdx.y * 8);
		}
	}
	__syncthreads();

	//COMBINE_GRID_X * 4 * threadCount
	//Gross level - 4 splits, along blockIdx.y
	//next level - 16 sections of 12 * COMBINE_GRID_X length

	combineResult[whichSubBlock + thistau * 12 + isB * 32 * 12] = runningAdd;
}

extern "C"
void combineVals(int blockCount, int* combineResult, dim3 grid, dim3 threads)
{
	combineValues<<< grid, threads >>>(blockCount, combineResult);
}


int runSampleEntropy(float* data, int dataSize, int m, int beginTau, int endTau, float r, char* error, double* E, int device)
{
	char* errortxt = "testing";
	memcpy(error,errortxt,strlen(errortxt)+1);

	DLL_CALL(cudaSetDevice(device));
	

	int sharedChunkSize = (DATA_CHUNK_SIZE);
	int blockXDimension = (int)ceil((float)dataSize/threadCount);
	int blockYDimension = dataSize / 2;
	int blockCount = blockXDimension * blockYDimension;

	float* deviceData;
	unsigned int* deviceResult;
	int* combineDeviceResult;



	int totTaus = endTau - beginTau + 1;

	//double* finEs = (double*)malloc(sizeof(double) * totTaus);

	//CUT_SAFE_CALL( cutCreateTimer(&timer1));
	//CUT_SAFE_CALL( cutResetTimer(timer1) );
	//CUT_SAFE_CALL( cutStartTimer(timer1) );


	DLL_CALL(cudaMalloc((void**) &deviceData, sizeof(float) * dataSize));
	DLL_CALL(cudaMemcpy(deviceData, data, sizeof(float) * dataSize, cudaMemcpyHostToDevice));
	DLL_CALL(cudaBindTexture(NULL, texRef, deviceData, sizeof(float) * dataSize));


	DLL_CALL(cudaMalloc((void**) &deviceResult, sharedChunkSize * blockCount * sizeof(int)));
	DLL_CALL(cudaMalloc((void**) &combineDeviceResult, (COMBINE_GRID_X * 4 * threadCount * sizeof(int))));

	int outputSpot = 0;

	for(int startTau = beginTau - 1; startTau < endTau; startTau+=32)
	{
		//Generate the table for the taus in question
		generateTauTable(m, startTau, 32);
				
		//CALCULATE SAMPLE ENTROPY
		DLL_CALL(cudaMemset(deviceResult, 0, sharedChunkSize * blockCount * sizeof(int)) );

		dim3 threads(threadCount);
		dim3 grid(blockXDimension,blockYDimension);

		//basicSampleEntropy<<< grid, threads >>>(deviceResult, m, r, dataSize);
		basicSampEn(deviceResult, m, r, dataSize, grid, threads);
		cudaThreadSynchronize();      

		DLL_CALL(cudaBindTexture(0, resultTexRef, deviceResult, sharedChunkSize * blockCount * sizeof(int)));

		//END SAMPLE ENTROPY


		//COMBINE
		int* combineHostResult;

		combineHostResult = (int*) malloc (COMBINE_GRID_X * 4 * threadCount * sizeof(int));
		memset(combineHostResult, 0, (COMBINE_GRID_X * 4 * threadCount * sizeof(int)));
		
		DLL_CALL(cudaMemset(combineDeviceResult, 0, (COMBINE_GRID_X * 4 * threadCount * sizeof(int))));

		dim3 combineGrid(COMBINE_GRID_X,4);
		cudaThreadSynchronize();
		//combineValues<<< combineGrid, threads >>>(blockCount, combineDeviceResult);
		combineVals(blockCount, combineDeviceResult, combineGrid, threads);
		cudaThreadSynchronize();

		//Don't need the result texture so it can be unbound
		DLL_CALL(cudaUnbindTexture(resultTexRef));

		DLL_CALL(cudaMemcpy(combineHostResult, combineDeviceResult, (COMBINE_GRID_X * 4 * threadCount * sizeof(int)), cudaMemcpyDeviceToHost));
		DLL_CALL( cudaThreadSynchronize() );

		int tauA[32];
		int tauB[32];
		double e[32];
		for(int i = 0; i < 32; i++)
		{
			tauA[i] = 0;
			tauB[i] = 0;

			for(int j = 0; j < 12; j++)
			{
				tauA[i] += combineHostResult[i * 12 + j];
				tauB[i] += combineHostResult[i * 12 + j + 12 * 32];
			}

			double ratio = (double)tauA[i]/(double)tauB[i];
			e[i] = -log(ratio);

			E[outputSpot] = e[i];
			outputSpot++;
		}

		free(combineHostResult);
		//END COMBINE

	}

	DLL_CALL(cudaUnbindTexture(texRef));	
	DLL_CALL(cudaFree(deviceData));
	DLL_CALL(cudaFree(combineDeviceResult));
	DLL_CALL(cudaFree(deviceResult));	

	cudaError_t err = cudaGetLastError(); 
	if(err != cudaSuccess)
	{
		errortxt = (char*)cudaGetErrorString(err);
	}
	else errortxt = "";

	memcpy(error,errortxt,strlen(errortxt)+1);
	
	DLL_CALL(cudaThreadExit());
	
	return 0;
}

//using namespace std;
// 2000 points = 11000 blocks @ 192 int32 ea (768 bytes), 15 sets of 192 int32s with this many blocks
// will fit into 128 mb of global memory.
// 
// Global memory limitation determines how many taus to do at once?
// Datasize N,  (n/2) * (ceil(n/192)) = blockSize
// Number of simultaneous taus sT, (sT/5) * 192 * sizeof(int) * blockSize = total memory requirement
// can solve for memory requirement and set segmentation thresholds for loop batching of kernels








