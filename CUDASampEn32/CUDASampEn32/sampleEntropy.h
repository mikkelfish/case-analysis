#ifndef _SAMPLEENTROPY_H_
#define _SAMPLEENTROPY_H_

#  define DLL_CALL( call) do {                              \
	cudaError errres = cudaGetLastError();					\
    if( cudaSuccess != errres) {                            \
	errortxt = (char*)cudaGetErrorString(errres);			\
	sprintf(error,"Before Error %s %i", errortxt,__LINE__);				\
	return 0; }												\
	errres = call;                                          \
    if( cudaSuccess != errres) {                            \
		errortxt = (char*)cudaGetErrorString(errres);		\
		sprintf(error,"After Error %s %i", errortxt,__LINE__);			\
		return 0;												\
	} } while (0)
	
#  define DLL_CALL_RET( call, ret) do {                              \
	cudaError errres = cudaGetLastError();					\
    if( cudaSuccess != errres) {                            \
	errortxt = (char*)cudaGetErrorString(errres);			\
	sprintf(error,"Before Error %s %i", errortxt,__LINE__);				\
	return ret; }												\
	errres = call;                                          \
    if( cudaSuccess != errres) {                            \
		errortxt = (char*)cudaGetErrorString(errres);		\
		sprintf(error,"After Error %s %i", errortxt,__LINE__);			\
		return ret;												\
	} } while (0)


#define threadCount 192

//#define dataSize 2000
//#define tau 1
//#define tauRange1 1
//#define tauRange2 100
//#define M 3
//#define R 0.02512

#define TAU_TABLE_DEPTH 8

#define COMBINE_GRID_X 1

#define I_CHUNK_SIZE 16
#define J_CHUNK_SIZE 192

//These are set up for an M of 6 or lower.
#define JUMP_ARRAY_WIDTH 160
#define TAU_ARRAY_WIDTH 1280
#define MAXIMUM_M 6

#define NUM_BANKS 16
#define LOG_NUM_BANKS 4

// Define this to more rigorously avoid bank conflicts, even at the lower (root) levels of the tree
//#define ZERO_BANK_CONFLICTS 

#ifdef ZERO_BANK_CONFLICTS
#define CONFLICT_FREE_OFFSET(index) ((index) >> LOG_NUM_BANKS + (index) >> (2 * LOG_NUM_BANKS))
#else
#define CONFLICT_FREE_OFFSET(index) ((index) >> LOG_NUM_BANKS)
#endif

#ifdef CHECK_BANK_CONFLICTS
#define TEMP(index)   CUT_BANK_CHECKER(temp, index)
#else
#define TEMP(index)   temp[index]
#endif

//Machine warp size
//#ifndef __DEVICE_EMULATION__
//G80's warp size is 32 threads
#define WARP_LOG_SIZE 5
//#else
//Emulation currently doesn't execute threads in coherent groups of 32 threads,
//which effectively means warp size of 1 thread for emulation modes
//#define WARP_LOG_SIZE 5
//#endif

//Warps in thread block
#define  WARP_N 6
//Chunk size per block (number of threads)
#define DATA_CHUNK_SIZE 16
//Threads per block count
#define THREAD_N (WARP_N << WARP_LOG_SIZE)
//Per-block number of elements
#define BLOCK_MEMORY (WARP_N * THREAD_N)

extern "C"
{
void generateTauTable(int desiredM, int tauStart, int numberOfTaus);
void basicSampEn(unsigned int* result, int m, float r, int dataLength, dim3 grid, dim3 threads);
void combineVals(int blockCount, int* combineResult, dim3 grid, dim3 threads);
int runSampleEntropy(float* data, int dataSize, int m, int beginTau, int endTau, float r, char* error, double* E, int device);
}


#endif