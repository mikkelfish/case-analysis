#ifndef CUDASAMPEN_H
#define CUDASAMPEN_H


typedef struct 
{
	

	float* data;
	int device;
	double E[32];
	int dataLength;
	int startTau;
	int endTau;
	float r;
	int m;

	char error[256];
} SampEnBlock;

extern "C"
{
	__declspec(dllexport) void CUDAEntryCall(float* data, int dataSize, int m, float r, int beginTau, int endTau, double* E, char* error, bool multi);
	__declspec(dllexport) int Supports(int dev, char* error);

}

#endif