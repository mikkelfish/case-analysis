﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using MesosoftCommon.Resources;
using System.Reflection;
using System.Windows.Media;

namespace MesosoftCommon.ExtensionMethods
{
    public static class FrameworkElements
    {

        public static DependencyObject FindAncestor(this DependencyObject child, Type ancestorType, int ancestorLevel)
        {
            DependencyObject workingElement = LogicalTreeHelper.GetParent(child);
            if (workingElement == null) return null;
            int currentLevel = 0;

            while((workingElement.GetType() != ancestorType && !workingElement.GetType().IsSubclassOf(ancestorType)) 
                && currentLevel < ancestorLevel)
            {
                workingElement = LogicalTreeHelper.GetParent(workingElement);
                if (workingElement == null)
                {
                    if (ancestorLevel != int.MaxValue) return null;
                    else break;
                }
                
                if (workingElement.GetType() == ancestorType || workingElement.GetType().IsSubclassOf(ancestorType))
                    currentLevel++;
            }

            return workingElement;
        }

        public static object FindVisualAncestor(UIElement child, Type ancestorType)
        {
            DependencyObject elem = VisualTreeHelper.GetParent(child);
            while (elem != null)
            {
                if (ancestorType.IsInstanceOfType(elem))
                    return elem;
                elem = VisualTreeHelper.GetParent(elem);
            }
            return null;
        }

        public static T FindTopAncestor<T>(this DependencyObject child) where T : DependencyObject
        {
            return (FindTopAncestor(child, typeof(T)) as T);
        }

        public static DependencyObject FindTopAncestor(this DependencyObject child, Type ancestorType)
        {
            return FindAncestor(child, ancestorType, int.MaxValue);
        }

        public static T FindAncestor<T>(this DependencyObject child, int ancestorLevel) where T : DependencyObject
        {
            return (FindAncestor(child, typeof(T), ancestorLevel) as T);
        }

        public static T UnboxConvert<T>(this object obj)
        {
            MethodInfo info = typeof(T).GetMethod("op_Implicit", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic,
                null, new Type[] { obj.GetType() }, null);

            if (info == null) return default(T);
            return (T)info.Invoke(null, new object[] { obj });
        }

    }
}
