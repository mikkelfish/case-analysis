﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;

namespace MesosoftCommon.ExtensionMethods
{
    public static class CollectionFunctions
    {
        private static Dictionary<Type, MethodInfo> reentrancy = new Dictionary<Type,MethodInfo>();
        private static Dictionary<Type, PropertyInfo> itemsProp = new Dictionary<Type,PropertyInfo>();
        private static Dictionary<Type, MethodInfo> onPropChanged = new Dictionary<Type,MethodInfo>();
        private static Dictionary<Type, MethodInfo> onCollectionChanged = new Dictionary<Type,MethodInfo>();


        public static void AddRange<T>(this ObservableCollection<T> collection, T[] items)
        {
            if (!reentrancy.ContainsKey(typeof(T)))
            {
                reentrancy.Add(typeof(T),
                    typeof(ObservableCollection<T>).GetMethod("CheckReentrancy", BindingFlags.Instance | BindingFlags.NonPublic));
                itemsProp.Add(typeof(T),
                    typeof(ObservableCollection<T>).GetProperty("Items", BindingFlags.NonPublic | BindingFlags.Instance));
                onPropChanged.Add(typeof(T),
                    typeof(ObservableCollection<T>).GetMethod("OnPropertyChanged", BindingFlags.Instance | BindingFlags.NonPublic, null,
                        new Type[]{typeof(System.ComponentModel.PropertyChangedEventArgs)}, null));


                MethodInfo[] infos = typeof(ObservableCollection<T>).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(m => m.Name == "OnCollectionChanged").ToArray();
                onCollectionChanged.Add(typeof(T),infos[0]);                   
            }

            reentrancy[typeof(T)].Invoke(collection, null);
            foreach (T item in items)
            {
                IList<T> itemList = itemsProp[typeof(T)].GetValue(collection, null) as IList<T>;
                itemList.Add(item);
            }

            onPropChanged[typeof(T)].Invoke(collection, new object[] { new System.ComponentModel.PropertyChangedEventArgs("Count") });
            onPropChanged[typeof(T)].Invoke(collection, new object[] { new System.ComponentModel.PropertyChangedEventArgs("Items[]") });
            onCollectionChanged[typeof(T)].Invoke(collection, new object[]{
                new NotifyCollectionChangedEventArgs(
                System.Collections.Specialized.NotifyCollectionChangedAction.Add, items) });

            //this.CheckReentrancy();
            //foreach (T item in items)
            //{
            //    this.Items.Add(item);
            //}
            //this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("Count"));
            //this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("Item[]"));
            //this.OnCollectionChanged(
            //    new System.Collections.Specialized.NotifyCollectionChangedEventArgs(
            //        System.Collections.Specialized.NotifyCollectionChangedAction.Add,
            //        items));


        }
    }
}
