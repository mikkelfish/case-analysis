﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Reflection;
using MesosoftCommon.Development;
using System.Reflection.Emit;
using System.ComponentModel;
using MesosoftCommon.Utilities.Events;

namespace MesosoftCommon.AutoComplete
{
    /// <summary>
    /// Interaction logic for AutoCompleteControl.xaml
    /// </summary>
    public partial class AutoCompleteControl : Window
    {

        public event RoutedEventHandler<InterfaceControlChangedEventArgs> InterfaceControlChanged
        {
            add { AddHandler(InterfaceControlChangedEvent, value); }
            remove { RemoveHandler(InterfaceControlChangedEvent, value); }
        }

        public static readonly RoutedEvent InterfaceControlChangedEvent =
            EventManager.RegisterRoutedEvent("InterfaceControlChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler<InterfaceControlChangedEventArgs>), typeof(AutoCompleteControl));

        void RaiseInterfaceControlChangedEvent(object interfaceObject, bool isSet)
        {
            InterfaceControlChangedEventArgs newEventArgs = new InterfaceControlChangedEventArgs(AutoCompleteControl.InterfaceControlChangedEvent, interfaceObject);
            newEventArgs.IsSet = isSet;
            RaiseEvent(newEventArgs);
            this.wasElementSet = newEventArgs.IsSet;
        }
    
        public object EventSource
        {
            get { return (object)GetValue(EventSourceProperty); }
            set { SetValue(EventSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EventSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EventSourceProperty =
            DependencyProperty.Register("EventSource", typeof(object), typeof(AutoCompleteControl), 
            new PropertyMetadata(new PropertyChangedCallback(AutoCompleteControl.onEventSourceChanged)));


        public string EventName
        {
            get { return (string)GetValue(EventNameProperty); }
            set { SetValue(EventNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EventName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EventNameProperty =
            DependencyProperty.Register("EventName", typeof(string), typeof(AutoCompleteControl), 
            new UIPropertyMetadata("KeyDown",new PropertyChangedCallback(AutoCompleteControl.onEventNameChanged)));

        public AutoCompleteModes Mode
        {
            get { return (AutoCompleteModes)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Mode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register(
              "Mode",
              typeof(AutoCompleteModes),
              typeof(AutoCompleteControl),
            new PropertyMetadata(AutoCompleteModes.SuggestAppend)
            );

        public object FilterObject
        {
            get { return (object)GetValue(FilterStringProperty); }
            set { SetValue(FilterStringProperty, value); }
        }
        // Using a DependencyProperty as the backing store for FilterString.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FilterStringProperty =
            DependencyProperty.Register(
              "FilterObject",
              typeof(object),
              typeof(AutoCompleteControl),
              new PropertyMetadata(new PropertyChangedCallback(AutoCompleteControl.onFilterStringChanged))             
            );

        public IAutoCompleteSourceProvider Provider
        {
            get { return (IAutoCompleteSourceProvider)GetValue(ProviderProperty); }
            set { SetValue(ProviderProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Provider.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ProviderProperty =
            DependencyProperty.Register(
              "Provider",
              typeof(IAutoCompleteSourceProvider),
              typeof(AutoCompleteControl)
            );

        public object Source
        {
            get { return (object)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register(
              "Source",
              typeof(object),
              typeof(AutoCompleteControl)
            );

        public object Parameter
        {
            get { return (object)GetValue(ParameterProperty); }
            set { SetValue(ParameterProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Parameter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParameterProperty =
            DependencyProperty.Register(
              "Parameter",
              typeof(object),
              typeof(AutoCompleteControl)
            );

        public int MaxDisplayItems
        {
            get { return (int)GetValue(MaxDisplayItemsProperty); }
            set { SetValue(MaxDisplayItemsProperty, value); }
        }
        // Using a DependencyProperty as the backing store for MaxDisplayItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxDisplayItemsProperty =
            DependencyProperty.Register(
              "MaxDisplayItems",
              typeof(int),
              typeof(AutoCompleteControl),
              new PropertyMetadata(int.MaxValue)
            );

        private ObservableCollection<AutoCompleteItemPair> sourceItems = new ObservableCollection<AutoCompleteItemPair>();
        public ObservableCollection<AutoCompleteItemPair> SourceItems
        {
            get { return sourceItems; }
        }


        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }
        // Using a DependencyProperty as the backing store for SelectedIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register(
              "SelectedIndex",
              typeof(int),
              typeof(AutoCompleteControl)
            );

        public PopupLocationModes PopupLocationMode
        {
            get { return (PopupLocationModes)GetValue(PopupLocationModeProperty); }
            set { SetValue(PopupLocationModeProperty, value); }
        }
        // Using a DependencyProperty as the backing store for PopupLocationMode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PopupLocationModeProperty =
            DependencyProperty.Register(
              "PopupLocationMode",
              typeof(PopupLocationModes),
              typeof(AutoCompleteControl),
            new PropertyMetadata(PopupLocationModes.PopToOwner)
            );


        private static void onFilterStringChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AutoCompleteControl control = obj as AutoCompleteControl;
            if (control.EventSource == null)
            {
                BindingExpression ex = control.GetBindingExpression(e.Property);

                control.EventSource = ex.DataItem;
            }

            BindingExpression expression = control.GetBindingExpression(AutoCompleteControl.FilterStringProperty);
            if (expression == null) return;
           // if(expression.DataItem != control.CurrentElement)
                control.CurrentElement = expression.DataItem as FrameworkElement;
        }


        private static void onEventSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AutoCompleteControl auto = (obj as AutoCompleteControl);
            auto.attachHandler(e.OldValue, e.NewValue, auto.EventName, auto.EventName);
        }


        private static void onEventNameChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AutoCompleteControl auto = (obj as AutoCompleteControl);
            auto.attachHandler(auto.EventSource, auto.EventSource, (string)e.OldValue, (string)e.NewValue);
        }

        private bool wasElementSet = false;

        private FrameworkElement currentElement;
        private FrameworkElement CurrentElement
        {
            get { return currentElement; }
            set 
            { 
                currentElement = value;
                if(!wasElementSet) this.RaiseInterfaceControlChangedEvent(this.currentElement, false);
            }
        }

	
        public AutoCompleteControl()
        {
            InitializeComponent();
        }

        private bool canFillSourceItems()
        {
            if ((Provider == null))
                return false;
            return Provider.CanGetAutoCompleteItems(Source, Parameter);
        }

        private void fillSourceItems()
        {
            AutoCompleteItemPair[] newItems = null;

            this.SourceItems.Clear();
            newItems = Provider.GetAutoCompleteItems(this.Source, this.Parameter, null, this.MaxDisplayItems);
            for (int i = 0; i < newItems.Length; i++)
            {
                this.SourceItems.Add(newItems[i]);
            }
        }

        private bool suppressPopUp = false;
        public bool SuppressPopUp
        {
            get { return suppressPopUp; }
            set { suppressPopUp = value; }
        }	

        public new void Show()
        {


            if (this.suppressPopUp || this.IsVisible || this.currentElement == null || this.Provider == null || !this.canFillSourceItems()) return;

            this.fillSourceItems();
            if (this.sourceItems == null) return;

            this.Visibility = Visibility.Visible;


            this.Width = this.currentElement.ActualWidth;
            if (this.Width < 300) this.Width = 300;

            //locate the parent window and attach to it
            this.Owner = Window.GetWindow(CurrentElement);

            if (this.PopupLocationMode == PopupLocationModes.PopToMouse)
            {
                this.Left = System.Windows.Forms.Control.MousePosition.X;
                this.Top = System.Windows.Forms.Control.MousePosition.Y;
            }
            else
            {
                this.Owner.LocationChanged += new EventHandler(Owner_LocationChanged);
                this.currentElement.LayoutUpdated += new EventHandler(currentElement_LayoutUpdated);
            }

            this.currentElement.LostFocus += new RoutedEventHandler(currentElement_LostFocus);
            base.Show();
            this.currentElement.Focus();
        }

        void Owner_LocationChanged(object sender, EventArgs e)
        {
            if (this.CurrentElement == null) return;
            Point point = this.CurrentElement.PointToScreen(new Point(0,
                                                                this.currentElement.Height + this.currentElement.Margin.Top));
            this.Left = point.X;
            this.Top = point.Y;
            
        }

        void currentElement_LostFocus(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }


        private void currentElement_LayoutUpdated(object sender, EventArgs e)
        {
            if (this.CurrentElement == null) return;
            Point point = this.CurrentElement.PointToScreen(new Point(0,
                                                                this.currentElement.Height + this.currentElement.Margin.Top));
            this.Left = point.X;
            this.Top = point.Y;            
        }


        public new void Hide()
        {
            base.Hide();
            this.Visibility = Visibility.Hidden;
            this.currentElement.LayoutUpdated -= new EventHandler(currentElement_LayoutUpdated);

            this.currentElement.LostFocus -= new RoutedEventHandler(this.currentElement_LostFocus);
            if(this.Owner != null) this.Owner.LocationChanged -= new EventHandler(this.Owner_LocationChanged);

            this.RaiseInterfaceControlChangedEvent(this.currentElement, true);
            this.wasElementSet = false;

            //Todo : Unset the binding! (done?)
            //BindingOperations.ClearBinding(this, AutoCompleteControl.FilterStringProperty);

        }

       

        private void setSource(AutoCompleteItemPair obj)
        {
            Type type = CurrentElement.GetType();
            FieldInfo fieldInfo = type.GetField("TextProperty", BindingFlags.Static | BindingFlags.Public);
            BindingExpression bindingExpression = this.CurrentElement.GetBindingExpression((fieldInfo.GetValue(CurrentElement) as DependencyProperty));
            PropertyInfo propertyInfo = bindingExpression.DataItem.GetType().GetProperty(bindingExpression.ParentBinding.Path.Path);
            
            propertyInfo.SetValue(bindingExpression.DataItem, obj.SourceObject, null);
            bindingExpression.UpdateTarget();
        }


        public void SetSource(AutoCompleteItemPair obj)
        {
            this.setSource(obj);
            this.Hide();
        }

        //Fix this eventually
        #region Dynamic Code Generation

        private static ModuleBuilder builder;
        private static Dictionary<string, MethodInfo> constructedMethods;
        private static TypeBuilder constructedType;
        private static FieldBuilder field;
        private static Type builtType;

        static AutoCompleteControl()
        {
            AppDomain myDomain = System.Threading.Thread.GetDomain();
            AssemblyName myAsmName = new AssemblyName();
            myAsmName.Name = "RequestInformationAssembly";

            AssemblyBuilder myAsmBuilder = myDomain.DefineDynamicAssembly(
                           myAsmName,
                           AssemblyBuilderAccess.RunAndSave);

            AutoCompleteControl.builder = myAsmBuilder.DefineDynamicModule(typeof(AutoCompleteControl).Name + " Module", true);

            AutoCompleteControl.constructedMethods = new Dictionary<string, MethodInfo>();

            AutoCompleteControl.constructedType = builder.DefineType("DynamicHandlers", TypeAttributes.Public);
            AutoCompleteControl.field = constructedType.DefineField("control", typeof(AutoCompleteControl), FieldAttributes.Public);            
        }


        private Delegate dynamicDelegate;
        private void attachHandler(object oldTarget, object newTarget, string oldEvent, string newEvent)
        {


            if (oldTarget != null && dynamicDelegate != null && oldEvent != null)
            {
                EventInfo info = oldTarget.GetType().GetEvent(oldEvent);
                if (info == null) return;
                info.RemoveEventHandler(oldTarget, dynamicDelegate);
            }

            if (newTarget != null && newEvent != null)
            {
                EventInfo info = newTarget.GetType().GetEvent(newEvent);
                if (info == null) return;
                object source;
                this.createDelegate(info, out dynamicDelegate, out source);

                info.AddEventHandler(newTarget, dynamicDelegate);
            }
        }


        private void createDelegate(EventInfo info, out Delegate del, out object source)
        {
            source = null;
            MethodInfo toUse = null;

            if (!AutoCompleteControl.constructedMethods.ContainsKey(info.Name)) 
            {
                MethodInfo invoke = info.EventHandlerType.GetMethod("Invoke");
                ParameterInfo[] pars = invoke.GetParameters();
                Type[] parTypes = new Type[pars.Length];
                for (int i = 0; i < pars.Length; i++)
                {
                    parTypes[i] = pars[i].ParameterType;
                }

                MethodInfo show = typeof(AutoCompleteControl).GetMethod("Show");

                MethodBuilder method = AutoCompleteControl.constructedType.DefineMethod(info.Name + "Handler", MethodAttributes.Public, invoke.ReturnType, parTypes);
                ILGenerator methodIL = method.GetILGenerator();
                methodIL.Emit(OpCodes.Nop);
                methodIL.Emit(OpCodes.Ldarg_0);
                methodIL.Emit(OpCodes.Ldfld, field);
                methodIL.Emit(OpCodes.Callvirt, show);
                methodIL.Emit(OpCodes.Nop);
                methodIL.Emit(OpCodes.Ret);

                AutoCompleteControl.builtType = AutoCompleteControl.constructedType.CreateType();
                MethodInfo theMethod = AutoCompleteControl.builtType.GetMethod(info.Name + "Handler");

                AutoCompleteControl.constructedMethods.Add(info.Name, theMethod);

            }

            toUse = AutoCompleteControl.constructedMethods[info.Name];
            source = Activator.CreateInstance(AutoCompleteControl.builtType);
            del = Delegate.CreateDelegate(info.EventHandlerType, source, toUse);
            FieldInfo control = source.GetType().GetField("control");
            control.SetValue(source, this);
        }

        #endregion
    }

    public enum AutoCompleteModes
    {
        /// <summary>
        /// Automatic completion
        /// </summary>
        Append = 1,
        /// <summary>
        /// No behavior
        /// </summary>
        None = 2,
        /// <summary>
        /// Show Drop Down
        /// </summary>
        Suggest = 3,
        /// <summary>
        /// Drop down and automatic completion
        /// </summary>
        SuggestAppend = 4
    }

    public enum PopupLocationModes
    {
        /// <summary>
        /// Traditional style, popup under the owner
        /// </summary>
        PopToOwner = 1,
        /// <summary>
        /// Pop to the mouse
        /// </summary>
        PopToMouse = 2
    }
}

