﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Text;
using System.ComponentModel;
using MesosoftCommon.Utilities.Converters;

namespace MesosoftCommon.AutoComplete
{
    [TypeConverter(typeof(AutoCompleteItemPairConverter))]
    public class AutoCompleteItemPair
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private object sourceObject;
        public object SourceObject
        {
            get { return sourceObject; }
            set { sourceObject = value; }
        }

        public override string ToString()
        {
            return Name;
        }

        public AutoCompleteItemPair(string thisName, object thisSourceObject)
        {
            Name = thisName;
            SourceObject = thisSourceObject;
        }
    }
}
