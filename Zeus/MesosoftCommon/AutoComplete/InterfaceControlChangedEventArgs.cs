﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MesosoftCommon.AutoComplete
{
    public class InterfaceControlChangedEventArgs : RoutedEventArgs
    {
        private object interfaceObject;
        public object InterfaceObject
        {
            get { return interfaceObject; }
        }

        private bool isSet;

        public bool IsSet
        {
            get { return isSet; }
            set { isSet = value; }
        }
	

        public InterfaceControlChangedEventArgs(RoutedEvent ev, object interfaceObject) : base(ev)
        {
            this.interfaceObject = interfaceObject;
            this.isSet = false;
        }
	
    }
}
