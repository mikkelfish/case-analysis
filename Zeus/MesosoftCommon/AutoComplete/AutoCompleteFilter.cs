﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows;

namespace MesosoftCommon.AutoComplete
{
    public abstract class AutoCompleteFilter : FrameworkElement
    {
        public abstract CollectionViewSource CurrentSource { get; }
        public abstract void Filter(object obj, FilterEventArgs e);
        protected bool isEmpty;
        public bool IsEmpty
        {
            get { return isEmpty; }
        }
	
    }
}
