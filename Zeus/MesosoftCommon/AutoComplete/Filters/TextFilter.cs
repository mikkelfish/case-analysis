﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows;

namespace MesosoftCommon.AutoComplete.Filters
{

    public class TextFilter : AutoCompleteFilter
    {
        public string FilterText
        {
            get { return (string)GetValue(FilterTextProperty); }
            set { SetValue(FilterTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FilterText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FilterTextProperty =
            DependencyProperty.Register("FilterText", typeof(string), typeof(TextFilter), new PropertyMetadata(TextFilter.onFilterTextChanged));

        public FilterModes FilterMode
        {
            get { return (FilterModes)GetValue(FilterModeProperty); }
            set { SetValue(FilterModeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FilterMode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FilterModeProperty =
            DependencyProperty.Register("FilterMode", typeof(FilterModes), typeof(TextFilter), new UIPropertyMetadata(FilterModes.MatchBeginning));


        public const string FilterHandler = "Filter";

        private static void onFilterTextChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            TextFilter filter = obj as TextFilter;
            if (filter.curSource == null) return;
            filter.curSource.View.Refresh();

            if (filter.curSource.View.CurrentItem == null) filter.curSource.View.MoveCurrentToFirst();

            if (filter.FilterText.Length == 0) filter.isEmpty = true;
            else filter.isEmpty = false;
        }

        private CollectionViewSource curSource;
        public override CollectionViewSource CurrentSource
        {
            get { return curSource; }
        }
	

        public override void Filter(object obj, FilterEventArgs e)
        {
            if (this.curSource == null) this.curSource = (obj as CollectionViewSource);
            
            if (String.IsNullOrEmpty(this.FilterText))
            {
                e.Accepted = true;
                return;
            }

            string str = e.Item.ToString();
            if (String.IsNullOrEmpty(str))
            {
                e.Accepted = false;
                return;
            }

            bool ok = false;
            if (this.FilterMode == FilterModes.MatchAnywhere)
            {
                int index = str.IndexOf(this.FilterText, 0, StringComparison.InvariantCultureIgnoreCase);
                ok = index > -1;
            }
            else
            {

                ok = str.StartsWith(this.FilterText, StringComparison.InvariantCultureIgnoreCase);
            }
            e.Accepted = ok;
        }
    }

    public enum FilterModes
    {
        /// <summary>
        /// Filter matching is done only from the beginning of the string
        /// </summary>
        MatchBeginning = 1,
        /// <summary>
        /// Filter matching is done anywhere in the string
        /// </summary>
        MatchAnywhere = 2,
        /// <summary>
        /// Filter matching is done from the beginning of any tree component, IE: "S" matches General.System but not General.Passthrough
        /// </summary>
        MatchBeginningByTree = 3
    }

}
