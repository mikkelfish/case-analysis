﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.AutoComplete
{
    public interface IAutoCompleteCommunicator
    {
        AutoCompleteControl AutoComplete { get; set; }
    }
}
