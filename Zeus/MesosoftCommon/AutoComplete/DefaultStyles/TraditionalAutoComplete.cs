﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Data;
using System.Linq;
using MesosoftCommon.AutoComplete.Filters;

namespace MesosoftCommon.AutoComplete.DefaultStyles
{
    public class TraditionalAutoComplete : FrameworkElement
    {
        public CollectionViewSource View
        {
            get { return (CollectionViewSource)GetValue(ViewProperty); }
            set { SetValue(ViewProperty, value); }
        }

        // Using a DependencyProperty as the backing store for view.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ViewProperty =
            DependencyProperty.Register("View", typeof(CollectionViewSource), typeof(TraditionalAutoComplete));



        public AutoCompleteControl AutoComplete
        {
            get { return (AutoCompleteControl)GetValue(AutoCompleteProperty); }
            set { SetValue(AutoCompleteProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AutoComplete.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutoCompleteProperty =
            DependencyProperty.Register("AutoComplete", typeof(AutoCompleteControl), typeof(TraditionalAutoComplete), new PropertyMetadata(new PropertyChangedCallback(TraditionalAutoComplete.onAutoCompletePropertyChanged)));

        private static void onAutoCompletePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null)
            {
                AutoCompleteControl oldControl = e.OldValue as AutoCompleteControl;
                if (oldControl != null)
                {
                    BindingOperations.ClearBinding((obj as TraditionalAutoComplete).Filter, TextFilter.FilterTextProperty);
                }
            }

            AutoCompleteControl control = e.NewValue as AutoCompleteControl;

            if (control == null) return;
            Binding textBinding = new Binding();
            textBinding.Source = control;
            textBinding.Path = new PropertyPath("FilterObject");

            BindingOperations.SetBinding((obj as TraditionalAutoComplete).Filter, TextFilter.FilterTextProperty, textBinding);
        }

        public const string AttachEventHandler = "attachEvents";

        private void attachEvents(object sender, InterfaceControlChangedEventArgs e)
        {
            FrameworkElement ele = e.InterfaceObject as FrameworkElement;
            if (ele == null || this.View == null) return;

            if (!e.IsSet)
            {
                this.View.Filter += new FilterEventHandler(this.Filter.Filter);
                ele.PreviewKeyDown += new KeyEventHandler(this.PreviewKeyDownDelegate);
                ele.KeyDown += new KeyEventHandler(this.KeyDownDelegate);
                ele.LostFocus += new RoutedEventHandler(ele_LostFocus);
                e.IsSet = true;
            }
            else
            {
                this.View.Filter -= new FilterEventHandler(this.Filter.Filter);
                ele.PreviewKeyDown -= new KeyEventHandler(this.PreviewKeyDownDelegate);
                ele.KeyDown -= new KeyEventHandler(this.KeyDownDelegate);
                ele.LostFocus -= new RoutedEventHandler(ele_LostFocus);
                e.IsSet = false;
            }
        }

        private void ele_LostFocus(object sender, RoutedEventArgs e)
        {
            if(this.AutoComplete != null)
                this.AutoComplete.SuppressPopUp = false;
        }

        public const string MousePreviewHandler = "onMousePreview";
        private void onMousePreview(object sender, MouseEventArgs e)
        {
            if (this.AutoComplete == null) return;
            this.AutoComplete.SetSource((sender as ListBoxItem).Content as AutoCompleteItemPair);
        }

        public const string MouseOverHandler = "onMouseOver";
        private void onMouseOver(object sender, MouseEventArgs e)
        {
            this.View.View.MoveCurrentTo((sender as ListBoxItem).Content);
        }

        private AutoCompleteFilter filter;
        public AutoCompleteFilter Filter
        {
            get { return filter; }
            set { this.filter = value; }
        }

        public TraditionalAutoComplete()
        {
            this.filter = new TextFilter();
        }
	

        private void PreviewKeyDownDelegate(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space || e.Key == Key.Down || e.Key == Key.Up)
                this.KeyDownDelegate(sender, new KeyEventArgs(e.KeyboardDevice, e.InputSource, e.Timestamp, e.Key));
        }


        private void KeyDownDelegate(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                this.setSource();
                this.AutoComplete.Hide();
                e.Handled = true;             
            }
            else if (e.Key == Key.Down)
            {
                this.View.View.MoveCurrentToNext();
                if (this.View.View.IsCurrentAfterLast) this.View.View.MoveCurrentToLast();
             //   e.Handled = true;

            }
            else if (e.Key == Key.Up)
            {
                this.View.View.MoveCurrentToPrevious();
                if (this.View.View.IsCurrentBeforeFirst) this.View.View.MoveCurrentToFirst();
               // e.Handled = true;

            }
            else if (e.Key == Key.Cancel || e.Key == Key.Escape)
            {
                //Add a "HasBeenDismissed" flag and set it to true here, set it to false on initialize or when the input is cleared
                this.AutoComplete.Hide();
                this.AutoComplete.SuppressPopUp = true;
                e.Handled = true;
            
            }

        }

        private void setSource()
        {
           // if (this.SelectedIndex < 0 || this.SelectedIndex >= this.sourceItems.Count) return;
            AutoCompleteItemPair pair = this.View.View.CurrentItem as AutoCompleteItemPair;
            if (pair == null) return;
            this.AutoComplete.SetSource(pair);
        }


    }
}
