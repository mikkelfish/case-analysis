﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.AutoComplete
{
    public class AutoCompleteSourceItem
    {
        private IAutoCompleteSourceProvider provider;
        public IAutoCompleteSourceProvider Provider
        {
            get { return provider; }
            set { provider = value; }
        }

        private object source;
        public object Source
        {
            get { return source; }
            set { source = value; }
        }

        private object parameter;
        public object Parameter
        {
            get { return parameter; }
            set { parameter = value; }
        }
	
	
	
    }
}
