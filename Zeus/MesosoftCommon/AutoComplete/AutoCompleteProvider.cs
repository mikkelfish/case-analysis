﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Reflection;
using System.Windows.Data;
using MesosoftCommon.Utilities.Bindings;

namespace MesosoftCommon.AutoComplete
{
    public class AutoCompleteProvider : Decorator
    {

        public AutoCompleteProvider()
        {
            this.Loaded += new RoutedEventHandler(AutoCompleteProvider_Loaded);
        }

        private void AutoCompleteProvider_Loaded(object sender, RoutedEventArgs e)
        {
            FindBindings.FindBindingsRecursively(this.Parent, new FindBindings.FoundBindingCallbackDelegate(this.onBindingFound));
        }

        private void onBindingFound(FrameworkElement element, Binding binding, DependencyProperty dp)
        {
            if(!(binding is AutoCompleteBinding)) return;
            AutoCompleteBinding autoBinding = binding as AutoCompleteBinding;

            Binding filterText = new Binding();
            filterText.Source = element;
            filterText.Path = new PropertyPath(dp.Name);
            if(autoBinding.AutoControl != null) autoBinding.AutoControl.SetBinding(AutoComplete.AutoCompleteControl.FilterStringProperty, filterText);
        }
        
    }
}
