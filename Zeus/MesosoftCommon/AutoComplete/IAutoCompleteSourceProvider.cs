﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace MesosoftCommon.AutoComplete
{
    public interface IAutoCompleteSourceProvider
    {
        bool CanGetAutoCompleteItems(object source, object parameter);
        AutoCompleteItemPair[] GetAutoCompleteItems(object source,object parameter,CultureInfo culture,int maxItems);
    }
}
