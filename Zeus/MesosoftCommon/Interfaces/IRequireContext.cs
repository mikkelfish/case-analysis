﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.Interfaces
{
    public interface IRequireObjects
    {
        Type[] ObjectTypes{ get; }
        object[] Args { get; set; }
        object[] Instances { get; set; }
    }
}
