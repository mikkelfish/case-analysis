﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MesosoftCommon.Interfaces
{
    public interface IConstructorProvider
    {
        object[] ConstructionArgs { get; }
    }
}
