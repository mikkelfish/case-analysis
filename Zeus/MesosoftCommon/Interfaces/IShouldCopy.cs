﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.Interfaces
{
    public class IShouldCopy
    {
        public bool ShouldCopy { get; set; }
    }
}
