﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace MesosoftCommon.Interfaces
{
    public delegate void PreviewTrackingEventHandler(object sender, PreviewTrackingEventArgs e);
    public delegate void TrackingEventHandler(object sender, TrackingEventArgs e);

    public enum TrackingType
    {
        Undo,
        Redo,
        Added,
        Deleted
    }

    #region Event Args

    public class PreviewTrackingEventArgs : EventArgs
    {
        private bool cancel;
        public bool Cancel
        {
            get { return cancel; }
            set { cancel = value; }
        }

        private object target;
        public object Target
        {
            get { return target; }
        }

        private object val;
        public object Value
        {
            get { return val; }
        }


        private PropertyInfo info;
        public PropertyInfo Info
        {
            get { return info; }
        }

        private TrackingType trackType;
        public TrackingType TrackingType
        {
            get { return trackType; }
        }
	

        public PreviewTrackingEventArgs(object target, object value, string property, TrackingType track)
        {
            this.target = target;
            this.val = value;
            this.info = this.target.GetType().GetProperty(property);
            this.trackType = track;
        }

        public PreviewTrackingEventArgs(object target, object value, PropertyInfo property, TrackingType track)
        {
            this.target = target;
            this.val = value;
            this.info = property;
            this.trackType = track;
        }
	
    }

    public class TrackingEventArgs : EventArgs
    {
        private object target;
        public object Target
        {
            get { return target; }
        }

        private object val;
        public object Value
        {
            get { return val; }
        }


        private PropertyInfo info;
        public PropertyInfo Info
        {
            get { return info; }
        }

        private TrackingType trackType;
        public TrackingType TrackingType
        {
            get { return trackType; }
        }
	

        public TrackingEventArgs(object target, object value, string property, TrackingType track)
        {
            this.target = target;
            this.val = value;
            this.info = this.target.GetType().GetProperty(property);
            this.trackType = track;
        }

        public TrackingEventArgs(object target, object value, PropertyInfo property, TrackingType track)
        {
            this.target = target;
            this.val = value;
            this.info = property;
            this.trackType = track;
        }

    }

    #endregion

    public interface ITracksChanges
    {
        bool StartTracking();
        bool Undo();
        bool Redo();
        void StopTracking();

        event PreviewTrackingEventHandler PreviewTrackingEvent;
        event TrackingEventHandler TrackingEvent;
    }
}
