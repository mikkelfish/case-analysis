﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using MesosoftCommon.Utilities.Inspection;

namespace MesosoftCommon.Resources
{
    [ContentProperty("Value")] 
    public class UnifiedResourceEntry
    {
        internal bool isActive;
        public bool IsActive
        {
            get { return isActive; }
        }

        internal object entryKey;
        public object EntryKey
        {
            get { return entryKey; }
            //set { this.entryKey = value; }
        }

        private string uniqueName;
        public string UniqueName
        {
            get 
            { 
                return uniqueName; 
            }
            set 
            { 
                uniqueName = value; 
            }
        }

        internal UnifiedResourceDictionary dictionary;
        public UnifiedResourceDictionary Dictionary
        {
            get { return this.dictionary; }
        }
	
        public UnifiedResourceEntry ActiveEntry
        {
            get
            {
                UnifiedResourceEntry entry = UnifiedResourceManager.GetActiveEntry(this.entryKey, false);
                if (entry == null) return this;
                return entry;
            }
        }

        private object converterKey;
        public object ConverterKey
        {
            get { return converterKey; }
            set { converterKey = value; }
        }

        private object converterParameter;
        public object ConverterParameter
        {
            get { return converterParameter; }
            set { converterParameter = value; }
        }
	

        internal object val;
        public object Value
        {
            set { val = value; }
        }

        public object GetValue()
        {
            return FillInManager.GetObject(val,  null, FillInManager.FillInCopyMode.Leave);
        }

        public object GetValue(object[] args)
        {
            return FillInManager.GetObject(val, args, FillInManager.FillInCopyMode.Leave);
        }


        public void MakeActive()
        {
            UnifiedResourceManager.MakeActive(this);
        }
    }
}
