﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using MesosoftCommon.IO;
using MesosoftCommon.IO.Interfaces;
using MesosoftCommon.Controls;
using Microsoft.Win32;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Globalization;
using System.Collections;
using System.Windows.Markup;
using System.Windows.Data;
using Reflector.BamlViewer;

namespace MesosoftCommon.Resources
{
    public class DefaultUnifiedResourceManagerBehavior : FrameworkElement
    {
        public ListBox ListBox
        {
            get { return (ListBox)GetValue(ListBoxProperty); }
            set { SetValue(ListBoxProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ListBox.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ListBoxProperty =
            DependencyProperty.Register("ListBox", typeof(ListBox), typeof(DefaultUnifiedResourceManagerBehavior));




        public UnifiedResourceManager Manager
        {
            get { return (UnifiedResourceManager)GetValue(ManagerProperty); }
            set { SetValue(ManagerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Manager.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ManagerProperty =
            DependencyProperty.Register("Manager", typeof(UnifiedResourceManager), typeof(DefaultUnifiedResourceManagerBehavior));


	


        public const string RemoveItem = "removeItemHandler";
        private void removeItemHandler(object sender, RoutedEventArgs e)
        {
            if (this.ListBox != null)
            {
                IUniversalListCollection collection = this.ListBox.ItemsSource as IUniversalListCollection;

                object[] selected = new object[this.ListBox.SelectedItems.Count];
                this.ListBox.SelectedItems.CopyTo(selected, 0);
                for (int i = 0; i < selected.Length; i++)
                {
                    collection.Remove(selected[i]);
                }
            }
        }

        private Button addDictionaryButton;

        public const string AddDictionaryButtonClick = "addDictionaryHandler";
        private void addDictionaryHandler(object sender, RoutedEventArgs e)
        {
            SimpleChoicePopup popup = new SimpleChoicePopup(new string[] { "File", "Resource", "Web" });
            popup.SelectionMode = SelectionMode.Single;
            popup.Closed += new EventHandler(addDictionaryHandler_Closed);
            Button button = sender as Button;
            this.addDictionaryButton = button;
            Point point = button.PointToScreen(new Point(0, button.Height + button.Margin.Top));
            popup.Top = point.Y;
            popup.Left = point.X;
            popup.Show();
        }

        private string getResource( string val, string assemblyName, string key)
        {
            string toRet = null;
            if (val.Contains("UnifiedResourceDictionary"))
            {
                try
                {
                    string toLoad = val;
                    string[] splits = toLoad.Split(new string[] { "clr-namespace:" }, StringSplitOptions.RemoveEmptyEntries);
                    string newLoad = string.Empty;

                    newLoad += splits[0];
                    for (int i = 1; i < splits.Length; i++)
                    {
                        int index = splits[i].IndexOf('"');
                        string justThis = splits[i].Substring(0, index);

                        if (!justThis.Contains(';'))
                        {
                            justThis += ";assembly=" + assemblyName.Substring(0, assemblyName.IndexOf(','));
                        }

                        string otherPart = splits[i].Substring(index + 1);
                        newLoad += "clr-namespace:" + justThis + "\"" + otherPart;
                    }


                    using (Stream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(newLoad)))
                    {
                        object possibleDict = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(stream);
                        if (possibleDict is UnifiedResourceDictionary)
                        {
                            toRet = "Resource: " + key + " in " +
                                assemblyName.Substring(0, assemblyName.IndexOf(','));
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return toRet;
        }

        private void addDictionaryHandler_Closed(object sender, EventArgs e)
        {
            SimpleChoicePopup popup = sender as SimpleChoicePopup;
            if (popup.Result == null || popup.Result.Count == 0 || this.Manager == null) return;
            string res = popup.Result[0] as string;

            if (res == "File")
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.AddExtension = true;
                dialog.DefaultExt = ".xaml";

                if (dialog.ShowDialog() == false) return;

                if (!File.Exists(dialog.FileName)) return;
                this.Manager.URIs.Add(new Uri(dialog.FileName));

            }
            else if (res == "Resource")
            {
                SimpleChoicePopup resourcePopup = new SimpleChoicePopup();
                resourcePopup.SelectionMode = SelectionMode.Extended;
                resourcePopup.Closed += new EventHandler(resourcePopup_Closed);
                resourcePopup.Width = 600;

                List<string> paths = new List<string>();
                Assembly[] referencedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (Assembly assembly in referencedAssemblies)
                {
                    #region GetResources
                    string[] names = null;

                    try
                    {
                        names = assembly.GetManifestResourceNames();
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    if (names == null) continue;

                    foreach (string name in names)
                    {
                        //if (name.Contains("Resources.resources"))
                        //{
                            //Type t = assembly.GetType(name.Substring(0, name.LastIndexOf(".resources")));
                            //MemberInfo[] infos = t.GetMembers(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Sta

                        if (!name.Contains(".resources")) continue;

                            ResourceManager manager = new ResourceManager(name.Substring(0,name.LastIndexOf(".resources")), assembly);
                            try
                            {
                                ResourceSet set = manager.GetResourceSet(CultureInfo.CurrentCulture, true, true);
                                IDictionaryEnumerator en = set.GetEnumerator();
                                while (en.MoveNext())
                                {
                                    string toParse = null;
                                    if (en.Value is string)
                                    {
                                        toParse = en.Value as string;
                                    }
                                    else if ((en.Key is string) && (en.Key as string).Contains(".xaml") && en.Value is Stream)
                                    {
                                        using (StreamReader reader = new StreamReader(en.Value as Stream, true))
                                        {
                                            toParse = reader.ReadToEnd();
                                        }
                                        (en.Value as Stream).Dispose();
                                    }

                                    if (toParse != null)
                                    {
                                        string result = this.getResource(toParse, assembly.FullName, en.Key as string);
                                        if (result != null)
                                        {
                                            resourcePopup.Items.Add(result);
                                        }
                                    }
                                    
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                            
                        //}
                    }
                    #endregion

                    // Stream stream = assembly.GetManifestResourceStream("ZeusApp.Properties.Resources.resources");
                    //ResourceManager t = new ResourceManager(
                }

                Point point = this.addDictionaryButton.PointToScreen(new Point(0, 0));
                resourcePopup.Top = point.Y - popup.Height;
                resourcePopup.Left = point.X;
                resourcePopup.Show();
            }
            else
            {

            }
        }

        void resourcePopup_Closed(object sender, EventArgs e)
        {
            SimpleChoicePopup resources = sender as SimpleChoicePopup;
            if (resources.Result == null || resources.Result.Count == 0 || this.Manager == null) return;
            foreach (string res in resources.Result)
            {
                string remove = res.Remove(0, "Resource: ".Length);
                string[] split = remove.Split(new string[] { " in " }, StringSplitOptions.RemoveEmptyEntries);

                string finPath = "pack://application:,,,/" + split[1].Trim() + ";" + split[0];
                Uri uri = new Uri(finPath);
                this.Manager.URIs.Add(uri);
            }
        }
    }
}
