﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Reflection;
using System.Collections;

namespace MesosoftCommon.Resources
{
    public class UnifiedResourceDictionary : ResourceDictionary
    {    
        public void Unload()
        {
            UnifiedResourceManager.RemoveResourceDictionary(this);
        }

        private object dictionaryKey;
        public object DictionaryKey
        {
            get { return dictionaryKey; }
            set 
            { 
                if(dictionaryKey != null) return;
                dictionaryKey = value;
            }
        }

        public new UnifiedResourceEntry this[object key]
        {
            get
            {
                return base[key] as UnifiedResourceEntry;
            }
        }

        private string uniqueName;
        public string UniqueName
        {
            get { return uniqueName; }
            set { uniqueName = value; }
        }

        internal bool isActive;
        public bool IsActive
        {
            get { return isActive; }
        }

        internal Uri uri;
        public Uri URI 
        {
            get { return this.uri; } 
        }


        public UnifiedResourceDictionary ActiveDictionary
        {
            get
            {
                UnifiedResourceDictionary dict = UnifiedResourceManager.GetActiveDictionary(this.dictionaryKey, false);
                if (dict == null) return this;
                return dict;
            }
        }
	

        public void MakeActive()
        {
            UnifiedResourceManager.MakeActive(this);
        }
	
    }
}
