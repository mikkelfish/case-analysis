﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MesosoftCommon.IO;
using System.Collections.ObjectModel;
using MesosoftCommon.ExtensionMethods;
using System.Reflection;
using MesosoftCommon.Structures;
using System.ComponentModel;
using MesosoftCommon.Utilities.XAML;
using System.IO;
using System.Resources;
using System.Globalization;
using MesosoftCommon.Interfaces;

namespace MesosoftCommon.Resources
{
    /// <summary>
    /// Interaction logic for UnifiedResourceWindow.xaml
    /// </summary>

    public partial class UnifiedResourceManager : Window, INotifyPropertyChanged
    {

        #region DictionaryManagement
        private static Dictionary<object, List<UnifiedResourceDictionary>> allDictionaries =
                 new Dictionary<object, List<UnifiedResourceDictionary>>();
        
        public static void AddResourceDictionary(UnifiedResourceDictionary dictionary)
        {
            if (!UnifiedResourceManager.allDictionaries.ContainsKey(dictionary.DictionaryKey))
                allDictionaries.Add(dictionary.DictionaryKey, new List<UnifiedResourceDictionary>());

            if (!UnifiedResourceManager.allDictionaries[dictionary.DictionaryKey].Contains(dictionary))
            {
                UnifiedResourceManager.allDictionaries[dictionary.DictionaryKey].Add(dictionary);
                foreach (object obj in dictionary.Keys)
                {
                    UnifiedResourceEntry entry = null;
                    if (dictionary[obj] is UnifiedResourceEntry)
                        entry = dictionary[obj] as UnifiedResourceEntry;
                    else
                    {
                        entry = new UnifiedResourceEntry() { Value = dictionary[obj] };
                        dictionary.Remove(obj);
                        dictionary.Add(obj, entry);
                    }

                    entry.dictionary = dictionary;
                    entry.entryKey = obj;

                    UnifiedResourceManager.addResourceEntry(entry);
                }
            }
            dictionaryUpdated("Dictionary");
        }

        public static void MakeActive(UnifiedResourceDictionary dictionary)
        {
            bool found = false;
            foreach (UnifiedResourceDictionary dict in UnifiedResourceManager.allDictionaries[dictionary.DictionaryKey])
            {
                if (dict == dictionary)
                {
                    dict.isActive = true;
                    found = true;
                }
                else dict.isActive = false;
            }

            if (!found) return;
            UnifiedResourceManager.allDictionaries[dictionary.DictionaryKey].Remove(dictionary);
            UnifiedResourceManager.allDictionaries[dictionary.DictionaryKey].Insert(0, dictionary);
            dictionaryUpdated("Dictionary");
        }

        public static UnifiedResourceDictionary GetDictionary(object key, string uniqueName)
        {
            if (!UnifiedResourceManager.allDictionaries.ContainsKey(key))
                return null;

            return UnifiedResourceManager.allDictionaries[key].Find(
                delegate(UnifiedResourceDictionary dict)
                {
                    return dict.UniqueName == uniqueName;
                }
            );
        }

        public static UnifiedResourceDictionary[] GetDictionaries(object key)
        {
            if (!UnifiedResourceManager.allDictionaries.ContainsKey(key))
                return null;
            return UnifiedResourceManager.allDictionaries[key].ToArray();
        }

        public static UnifiedResourceDictionary GetActiveDictionary(object key, bool forceReturn)
        {
            if (!UnifiedResourceManager.allDictionaries.ContainsKey(key) ||
                  UnifiedResourceManager.allDictionaries[key].Count == 0)
                return null;
            if (UnifiedResourceManager.allDictionaries[key][0].isActive)
                return UnifiedResourceManager.allDictionaries[key][0];
            if (!forceReturn) return null;
            return UnifiedResourceManager.allDictionaries[key][0];
        }

        public static void RemoveResourceDictionary(UnifiedResourceDictionary dictionary)
        {
            if (dictionary == null || !UnifiedResourceManager.allDictionaries.ContainsKey(dictionary.DictionaryKey))
                return;
            UnifiedResourceManager.allDictionaries[dictionary.DictionaryKey].Remove(dictionary);
            if (UnifiedResourceManager.allDictionaries[dictionary.DictionaryKey].Count == 0)
            {
                UnifiedResourceManager.allDictionaries.Remove(dictionary.DictionaryKey);
            }

            foreach (UnifiedResourceEntry entry in dictionary.Values)
            {
                UnifiedResourceManager.removeEntry(entry);
            }

            dictionaryUpdated("Dictionary");


        }

        public static void RemoveResourceDictionary(object key, string uniqueName)
        {
            UnifiedResourceManager.RemoveResourceDictionary(UnifiedResourceManager.GetDictionary(key, uniqueName));            
        }        


        #endregion

        #region EntryManagement
        private static Dictionary<object, List<UnifiedResourceEntry>> allEntries =
          new Dictionary<object, List<UnifiedResourceEntry>>();

        private static void addResourceEntry(UnifiedResourceEntry entry)
        {
            if (!UnifiedResourceManager.allEntries.ContainsKey(entry.EntryKey))
                allEntries.Add(entry.EntryKey, new List<UnifiedResourceEntry>());
            UnifiedResourceManager.allEntries[entry.EntryKey].Add(entry);
            dictionaryUpdated("Entries");

        }

        public static void MakeActive(UnifiedResourceEntry entry)
        {

            bool found = false;
            foreach (UnifiedResourceEntry dict in UnifiedResourceManager.allEntries[entry.entryKey])
            {
                if (dict == entry)
                {
                    dict.isActive = true;
                    found = true;
                }
                else dict.isActive = false;
            }

            if (!found) return;
            UnifiedResourceManager.allEntries[entry.entryKey].Remove(entry);
            UnifiedResourceManager.allEntries[entry.entryKey].Insert(0, entry);
            dictionaryUpdated("Entries");

        }

        public static UnifiedResourceEntry GetActiveEntry(object key, bool forceReturn)
        {
            if (!UnifiedResourceManager.allEntries.ContainsKey(key) ||
                   UnifiedResourceManager.allEntries[key].Count == 0)
                return null;
            if (UnifiedResourceManager.allEntries[key][0].IsActive)
                return UnifiedResourceManager.allEntries[key][0];
            if (!forceReturn) return null;
            return UnifiedResourceManager.allEntries[key][0];
        }

        public static UnifiedResourceEntry GetEntry(object key, string uniqueName)
        {
            if (!UnifiedResourceManager.allEntries.ContainsKey(key))
                return null;

            return UnifiedResourceManager.allEntries[key].Find(
                delegate(UnifiedResourceEntry dict)
                {
                    return dict.UniqueName == uniqueName;
                }
            );
        }

        private static void removeEntry(UnifiedResourceEntry entry)
        {
            if (UnifiedResourceManager.allEntries.ContainsKey(entry.entryKey))
            {
                UnifiedResourceManager.allEntries[entry.entryKey].Remove(entry);

                if (UnifiedResourceManager.allEntries[entry.entryKey].Count == 0)
                {
                    UnifiedResourceManager.allEntries.Remove(entry.entryKey);
                }
            }
            dictionaryUpdated("Entries");
        }

        #endregion

        #region Statics

        private static List<UnifiedResourceManager> managers = new List<UnifiedResourceManager>();

        private static void dictionaryUpdated(string which)
        {
            string name = "Entries";
            if (which == "Dictionary")
            {
                name = "Dictionaries";
            }
            foreach (UnifiedResourceManager manager in managers)
            {
                manager.OnNotifyPropertyChanged(manager, name);
            }
        }       

        public static UnifiedResourceDictionary GetResource(string path)
        {
            string[] split = path.Split(';');
            string assemblyStr = split[0].Substring("pack://application:,,,/".Length);
            Assembly[] referencedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in referencedAssemblies)
            {
                string testName = assembly.FullName.Substring(0, assembly.FullName.IndexOf(','));
                if (testName != assemblyStr) continue;

                ResourceManager manager = null;
                if (split[1].Contains(".xaml"))
                {
                    manager = new ResourceManager(assemblyStr + ".g", assembly);
                }
                else
                {
                    manager = new ResourceManager(assemblyStr + ".Properties.Resources", assembly);
                }


                try
                {
                    ResourceSet set = manager.GetResourceSet(CultureInfo.CurrentCulture, true, true);
                    IDictionaryEnumerator en = set.GetEnumerator();
                    while (en.MoveNext())
                    {
                        if ((en.Key as string) == split[1])
                        {
                            string dictionaryString = null;
                            if (en.Value is string)
                            {
                                dictionaryString = en.Value as string;
                            }
                            else if ((en.Key is string) && (en.Key as string).Contains(".xaml") && en.Value is Stream)
                            {
                                using (StreamReader reader = new StreamReader(en.Value as Stream, true))
                                {
                                    dictionaryString = reader.ReadToEnd();
                                }
                                (en.Value as Stream).Dispose();
                            }

                            if (dictionaryString == null || !dictionaryString.Contains("UnifiedResourceDictionary"))
                            {
                                return null;
                            }


                            #region Load Dictionary

                            string[] splits = dictionaryString.Split(new string[] { "clr-namespace:" }, StringSplitOptions.RemoveEmptyEntries);
                            string newLoad = string.Empty;

                            newLoad += splits[0];
                            for (int i = 1; i < splits.Length; i++)
                            {
                                int index = splits[i].IndexOf('"');
                                string justThis = splits[i].Substring(0, index);

                                if (!justThis.Contains(';'))
                                {
                                    justThis += ";assembly=" + assembly.FullName.Substring(0, assembly.FullName.IndexOf(','));
                                }

                                string otherPart = splits[i].Substring(index + 1);
                                newLoad += "clr-namespace:" + justThis + "\"" + otherPart;
                            }


                            using (Stream stream = new MemoryStream(UTF8Encoding.Default.GetBytes(newLoad)))
                            {
                                object possibleDict = MesosoftCommon.Utilities.XAML.XAMLInputOutput.Load(stream);
                                return possibleDict as UnifiedResourceDictionary;
                            }

                            #endregion
                        }
                    }
                }
                catch
                {

                }
            }

            return null;
        }

        private static void loadDictionary(Uri uri)
        {
            if (uri.IsFile && File.Exists(uri.LocalPath))
            {
                using (FileStream stream = new FileStream(uri.LocalPath, FileMode.Open, FileAccess.Read))
                {
                    UnifiedResourceDictionary dict = XAMLInputOutput.Load(stream) as UnifiedResourceDictionary;
                    if (dict != null)
                    {
                        AddResourceDictionary(dict);
                        dict.uri = uri;                    
                    }

                }
            }
            else if(uri.AbsoluteUri.Contains("pack://application:,,,/"))
            {
                UnifiedResourceDictionary dict = GetResource(uri.AbsoluteUri);
                if (dict != null)
                {
                    AddResourceDictionary(dict);
                    dict.uri = uri;

                }
            }
        }

        static UnifiedResourceManager()
        {
            uris = 
                UniversalCollectionManager.CreateCollection<UniversalListCollection<UniversalItemWrapper<Uri>>>("UnifiedResourceManagerURIs", true);

            foreach (UniversalItemWrapper<Uri> uri in uris)
            {
                loadDictionary(uri);

            }

            uris.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(uris_CollectionChanged);

            
        }

        static void uris_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (UniversalItemWrapper<Uri> uri in e.NewItems)
                {
                    loadDictionary(uri.Value as Uri);
                }
            }
            else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (UniversalItemWrapper<Uri> uri in e.OldItems)
                {
                    UnifiedResourceDictionary dict = null;
                    foreach (List<UnifiedResourceDictionary> listDicts in allDictionaries.Values)
                    {
                        dict = listDicts.Find(testDict => testDict.uri == uri);
                        if (dict != null) break;
                    }
                    if (dict != null) RemoveResourceDictionary(dict);
                }
            }
            else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Replace)
            {

            }
            else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Reset)
            {

            }
        }

        #endregion

        #region Instance Stuff

        private void OnNotifyPropertyChanged(object sender, string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(sender, new PropertyChangedEventArgs(propertyName));
            }
        }

        public SimpleNode Dictionaries
        {
            get
            {
                SimpleNode root = new SimpleNode(null, "Root");
                foreach (object obj in UnifiedResourceManager.allDictionaries.Keys)
                {
                    SimpleNode keyNode = new SimpleNode(obj, obj.ToString());
                    keyNode.Parent = root;
                    for (int i = 0; i < UnifiedResourceManager.allDictionaries[obj].Count; i++ )
                    {
                        UnifiedResourceDictionary dict = UnifiedResourceManager.allDictionaries[obj][i];
                        SimpleNode dictNode = new SimpleNode(dict, dict.UniqueName ?? "Unified Resource Dictionary" + (i+1).ToString() );
                        dictNode.Parent = keyNode;
                        foreach (UnifiedResourceEntry val in dict.Values)
                        {
                            string valStr = "";
                            if(val.val != null) valStr = val.val.ToString();

                            if (val.ConverterKey != null)
                            {
                                UnifiedResourceEntry converter = GetActiveEntry(val.ConverterKey, true);
                                if (converter != null && converter.val is IValueConverter)
                                {
                                    valStr = (string)(converter.val as IValueConverter).Convert(val.val, typeof(string), val.ConverterParameter, CultureInfo.CurrentCulture);
                                }
                            }
                            else
                            {
                                UnifiedResourceEntry converter = GetActiveEntry(val.val.GetType(), true);
                                if (converter != null && converter.val is IValueConverter)
                                {
                                    valStr = (string)(converter.val as IValueConverter).Convert(val.val, typeof(string), val.ConverterParameter, CultureInfo.CurrentCulture);
                                }
                            }

                            SimpleNode valNode = new SimpleNode(val, val.EntryKey.ToString() + ": " + valStr) { Parent = dictNode };
                        }
                    }
                }

                return root;
            }
        }

        public SimpleNode Entries
        {
            get
            {
                SimpleNode root = new SimpleNode(null, "Root");
                foreach (object obj in UnifiedResourceManager.allEntries.Keys)
                {
                    SimpleNode keyNode = new SimpleNode(obj, obj.ToString());
                    keyNode.Parent = root;
                    for (int i = 0; i < UnifiedResourceManager.allEntries[obj].Count; i++)
                    {
                        UnifiedResourceEntry dict = UnifiedResourceManager.allEntries[obj][i];

                        string toDisplay = dict.UniqueName ?? "Unified Resource Entry" + (i + 1).ToString() + ": ";
                        if (dict.val != null) toDisplay += dict.val.ToString();
                        if (dict.ConverterKey != null)
                        {
                            UnifiedResourceEntry converter = GetActiveEntry(dict.ConverterKey, true);
                            if (converter != null && converter.val is IValueConverter)
                            {
                                toDisplay += (string)(converter.val as IValueConverter).Convert(dict.val, typeof(string), dict.ConverterParameter, CultureInfo.CurrentCulture);
                            }
                        }
                        else
                        {
                            UnifiedResourceEntry converter = GetActiveEntry(dict.val.GetType(), true);
                            if (converter != null && converter.val is IValueConverter)
                            {
                                toDisplay += (string)(converter.val as IValueConverter).Convert(dict.val, typeof(string), dict.ConverterParameter, CultureInfo.CurrentCulture);
                            }
                        }

                        SimpleNode dictNode = new SimpleNode(dict, toDisplay);
                        dictNode.Parent = keyNode;
                    }
                }
                return root;
            }
        }


        private static UniversalListCollection<UniversalItemWrapper<Uri>> uris;
        public UniversalListCollection<UniversalItemWrapper<Uri>> URIs
        {
            get
            {
                return uris;
            }
        }



        public bool DisplayDictionaries
        {
            get { return (bool)GetValue(DisplayDictionariesProperty); }
            set { SetValue(DisplayDictionariesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DisplayDictionaries.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayDictionariesProperty =
            DependencyProperty.Register("DisplayDictionaries", typeof(bool), typeof(UnifiedResourceManager), new PropertyMetadata(true));



        public UnifiedResourceManager()
        {
            this.Closed += new EventHandler(UnifiedResourceManager_Closed);
            this.Loaded += new RoutedEventHandler(UnifiedResourceManager_Loaded);

            InitializeComponent(); 
            
        }

        void UnifiedResourceManager_Loaded(object sender, RoutedEventArgs e)
        {
            UnifiedResourceManager.managers.Add((UnifiedResourceManager)sender);
        }

        void UnifiedResourceManager_Closed(object sender, EventArgs e)
        {
            UnifiedResourceManager.managers.Remove((UnifiedResourceManager)sender);
        }
    
#region INotifyPropertyChanged Members

public event PropertyChangedEventHandler  PropertyChanged;

#endregion

        #endregion
}
}
