﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.Utilities.Reflection;
using System.Reflection;
using MesosoftCommon.Interfaces;
using MesosoftCommon.Development;

namespace MesosoftCommon.CommonAddIns.BlindAssignmentAddIns
{
    [AddIns.HostAdapter]
    public class BlindAssignmentContractToViewHostAdapter : BlindAssignment
    {
        private class cacheInfo
        {
            public bool IsClassAssignable { get; set; }
            public Type[] InterfacesSupported { get; set; }
            public string[] PropertiesToIgnore { get; set; }
        }

        private Dictionary<Type, cacheInfo> cache = new Dictionary<Type, cacheInfo>();

        private BlindAssignmentContract contract;
        public BlindAssignmentContractToViewHostAdapter(BlindAssignmentContract contract)
        {
            this.contract = contract;
        }

        public override Type[] SupportsInterfaces
        {
            get { return this.contract.GetSupportedInterfaces; }
        }

        private cacheInfo buildCache(Type objectType)
        {
            if (cache.ContainsKey(objectType)) return cache[objectType];

            cacheInfo info = new cacheInfo();
            cache.Add(objectType, info);


            Type[] objectInterfaces = objectType.GetInterfaces();
            Type[] interfacesSupported = (from t in this.SupportsInterfaces
                                          where objectInterfaces.Contains<Type>(t)
                                          select t).ToArray<Type>();
            if (interfacesSupported.Length == 0)
            {
                info.IsClassAssignable = false;
                return info;
            }

            BlindAssignmentAttribute attr = Common.GetAttribute<BlindAssignmentAttribute>(objectType, true);
            if (attr != null)
            {
                info.IsClassAssignable = attr.IsAssignable;
                if (!info.IsClassAssignable)
                {
                    return info;
                }
            }
            else info.IsClassAssignable = true;


            List<Type> realInterfacesSupported = new List<Type>(interfacesSupported);
            foreach (Type t in interfacesSupported)
            {
                BlindAssignmentAttribute interfaceAttr = Common.GetAttribute<BlindAssignmentAttribute>(t, false);
                if (interfaceAttr != null && attr.IsAssignable == false)
                    realInterfacesSupported.Remove(t);
            }

            info.InterfacesSupported = realInterfacesSupported.ToArray();


            List<string> propertiesToIgnore = new List<string>();
            PropertyInfo[] properties = objectType.GetProperties();
            foreach (PropertyInfo prop in properties)
            {
                object[] attrs = prop.GetCustomAttributes(typeof(BlindAssignmentAttribute), true);
                if (attrs == null || attrs.Length == 0) continue;
                BlindAssignmentAttribute propAttr = attrs[0] as BlindAssignmentAttribute;
                if (propAttr.IsAssignable == false) propertiesToIgnore.Add(prop.Name);
            }

            info.PropertiesToIgnore = propertiesToIgnore.ToArray();

            return info;

        }

        public override bool CanAssign(object obj, object[] args, bool shouldCopy, out PreviewBlindAssignmentEventArgs options)
        {

            cacheInfo info = this.buildCache(obj.GetType());


            PreviewBlindAssignmentEventArgs pargs = new PreviewBlindAssignmentEventArgs(obj, args);
            pargs.ShouldCopy = shouldCopy;
            EventHandler<PreviewBlindAssignmentEventArgs> handler = PreviewAssigned;
            if (handler != null)
            {
                handler(this, pargs);
            }


            bool canAssign = this.contract.CanAssign(obj, pargs.Args.ToArray(), info.InterfacesSupported, info.PropertiesToIgnore, out shouldCopy);
            pargs.ShouldCopy = pargs.ShouldCopy || shouldCopy;

            if (canAssign == false)
            {
                options = null;
                return false;
            }           

            options = pargs;
            return true;
        }

        [Todo("Mikkel", "08/07/2007", Comments="What if it's not successful copy")]
        public override object Assign(object obj, PreviewBlindAssignmentEventArgs options)
        {
            cacheInfo info = this.buildCache(obj.GetType());

            if (options.ShouldCopy)
            {
                bool success;
                if (this.contract.CanCopy(obj))
                {
                    obj = this.contract.Copy(obj);
                }
                else obj = Common.CreateBestGuessCopy(obj, out success);

                //TODO what if it's not a successful copy!?
            }

            this.contract.Assign(obj, options.Args.ToArray(), info.InterfacesSupported, info.PropertiesToIgnore);


            BlindAssignmentEventArgs pargs = new BlindAssignmentEventArgs(obj, options.Args.ToArray());
            EventHandler<BlindAssignmentEventArgs> handler = Assigned;
            if (handler != null)
            {
                handler(this, pargs);
            }

            return obj;
        }

        public override event EventHandler<BlindAssignmentEventArgs> Assigned;

        public override event EventHandler<PreviewBlindAssignmentEventArgs> PreviewAssigned;
    }
}
