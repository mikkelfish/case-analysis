﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.BlindAssignmentAddIns
{
    public class PreviewBlindAssignmentEventArgs : EventArgs
    {
        public object Object { get; private set; }
        public List<object> Args{ get; private set; }
        public bool ShouldCopy { get; set; }

        public PreviewBlindAssignmentEventArgs(object obj, object[] args)
        {
            this.Object = obj;
            if(args != null) this.Args = new List<object>(args);
            this.Args = new List<object>();
        }
    }

    public class BlindAssignmentEventArgs : EventArgs
    {
        public object Object { get; private set; }
        public object[] Args { get; private set; }

        public BlindAssignmentEventArgs(object obj, object[] args)
        {
            this.Object = obj;
            this.Args = args;
            
        }
    }
}
