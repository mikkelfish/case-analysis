﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.BlindAssignmentAddIns
{
    [global::System.AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class BlindAssignmentAttribute : Attribute
    {
        public bool IsAssignable { get; private set; }
        public BlindAssignmentAttribute(bool canAssign)
        {
            this.IsAssignable = canAssign;
        }
    }
}
