﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.BlindAssignmentAddIns
{
    [AddIns.AddInView]
    public abstract class BlindAssignmentAddInView
    {
        public abstract Type[] GetSupportedInterfaces { get; }
        public abstract bool CanAssign(object obj, object[] args, Type[] interfacesSupported, string[] excludedProperties, out bool shouldCopy);
        public abstract void Assign(object obj, object[] args, Type[] interfacesSupported, string[] excludedProperties);

        public abstract bool CanCopy(object obj);
        public abstract object Copy(object obj);
    }
}
