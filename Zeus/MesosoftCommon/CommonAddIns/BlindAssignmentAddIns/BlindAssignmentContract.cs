﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.BlindAssignmentAddIns
{
    [AddIns.AddInContract]
    public interface BlindAssignmentContract : AddIns.Interfaces.IAddInContract
    {
        bool CanCopy(object obj);
        object Copy(object obj);


        Type[] GetSupportedInterfaces { get; }
        bool CanAssign(object obj, object[] args, Type[] interfacesSupported, string[] excludedProperties, out bool shouldCopy);
        void Assign(object obj, object[] args, Type[] interfacesSupported, string[] excludedProperties);
    }
}
