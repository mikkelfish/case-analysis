﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.BlindAssignmentAddIns
{
    [AddIns.HostView]
    public abstract class BlindAssignment
    {
        public abstract Type[] SupportsInterfaces{get;}
        public abstract bool CanAssign(object obj, object[] args, bool shouldCopy, out PreviewBlindAssignmentEventArgs options);
        public abstract object Assign(object obj, PreviewBlindAssignmentEventArgs options);

        public abstract event EventHandler<BlindAssignmentEventArgs> Assigned;
        public abstract event EventHandler<PreviewBlindAssignmentEventArgs> PreviewAssigned;
    }
}
