﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.BlindAssignmentAddIns
{
    [AddIns.AddInAdapter]
    public class BlindAssignmentAddInAdapter : AddIns.AddInAdapterBase, BlindAssignmentAddIns.BlindAssignmentContract
    {
        private BlindAssignmentAddInView view;
        public BlindAssignmentAddInAdapter(BlindAssignmentAddInView view)
        {
            this.view = view;
        }

        #region BlindAssignmentContract Members

        public Type[] GetSupportedInterfaces
        {
            get { return this.view.GetSupportedInterfaces; }
        }

        public bool CanAssign(object obj, object[] args, Type[] interfacesSupported, string[] excludedProperties, out bool shouldCopy)
        {
            return this.view.CanAssign(obj, args, interfacesSupported, excludedProperties, out shouldCopy);
        }

        public void Assign(object obj, object[] args, Type[] interfacesSupported, string[] excludedProperties)
        {
            this.view.Assign(obj, args, interfacesSupported, excludedProperties);
        }

        #endregion

        #region BlindAssignmentContract Members

        public bool CanCopy(object obj)
        {
            return this.CanCopy(obj);
        }

        public object Copy(object obj)
        {
            return this.Copy(obj);
        }

        #endregion
    }
}
