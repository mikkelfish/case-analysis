﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.ContextAddIns
{
    [AddIns.AddInContract]
    public interface IContextContract : AddIns.Interfaces.IAddInContract
    {
        Type[] ContextTypes { get; }
        object CreateContext(object key, object[] args);
        bool SupportsObject(object key, object[] args);
    }
}
