﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.ContextAddIns
{
    [AddIns.HostView]
    public abstract class ContextProvider
    {
        public abstract Type[] Types { get; }
        public abstract T GetContext<T>(object key, object[] args) where T : class;
        public abstract object GetContext(object key, object[] args);
        public abstract bool ContextSupportsKey(object key, object[] args);


    }
}
