﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.ContextAddIns
{
    [AddIns.AddInView]
    public abstract class ContextAddInView
    {
        public abstract Type[] Types { get; }
        public abstract bool SupportsObject(object key, object[] args);
        public abstract object GetContext(object key, object[] args);
    }
}
