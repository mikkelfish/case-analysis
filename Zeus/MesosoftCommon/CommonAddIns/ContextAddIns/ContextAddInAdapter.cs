﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.ContextAddIns
{
    [AddIns.AddInAdapter]
    public class ContextAddInAdapter : AddIns.AddInAdapterBase, ContextAddIns.IContextContract
    {
        private ContextAddInView view;

        public ContextAddInAdapter(ContextAddInView view)
        {
            this.view = view;
        }

        #region IContextContract Members

        public Type[] ContextTypes
        {
            get { return this.view.Types; }
        }

        public object CreateContext(object key, object[] args)
        {
            return this.view.GetContext(key, args);
        }

        public bool SupportsObject(object key, object[] args)
        {
            return this.view.SupportsObject(key, args);
        }

        #endregion
    }
}
