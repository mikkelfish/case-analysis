﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.CommonAddIns.ContextAddIns
{
    [AddIns.HostAdapter]
    public class ContextContractToViewHostAdapter : ContextProvider
    {
        private IContextContract contract;

        public ContextContractToViewHostAdapter(IContextContract contract)
        {
            this.contract = contract;
        }

        public override Type[] Types
        {
            get { return contract.ContextTypes; }
        }

        public override T GetContext<T>(object key, object[] args)
        {
            return this.contract.CreateContext(key, args) as T;
        }

        public override object GetContext(object key, object[] args)
        {
            return this.contract.CreateContext(key, args);
        }

        public override bool ContextSupportsKey(object key, object[] args)
        {
            return this.contract.SupportsObject(key, args);
        }
    }
}
