﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Kennedy.ManagedHooks;
//using System.Windows;

//namespace MesosoftCommon.Hooks
//{
//    public static class HookManager
//    {
//        private static MouseHook hook;
//        private static int counter = 0;
//        private static object lockable = new object();

//        private class hookentry
//        {
//            public object Owner { get; set; }
//            public bool Active { get; set; }

//            private List<MouseHook.MouseEventHandler> handlers = new List<MouseHook.MouseEventHandler>();
//            public List<MouseHook.MouseEventHandler> Handlers { get { return this.handlers; } }
//        }

//        private static List<hookentry> entries = new List<hookentry>();

//        static HookManager()
//        {
//            hook = new MouseHook();
//            hook.MouseEvent += new MouseHook.MouseEventHandler(hook_MouseEvent);
//            if (Application.Current == null)
//                System.Windows.Forms.Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
//            else
//                Application.Current.Exit += new ExitEventHandler(Current_Exit);
//        }

//        static void hook_MouseEvent(MouseEvents mEvent, System.Drawing.Point point)
//        {
//            lock (lockable)
//            {
//                foreach (hookentry entry in entries)
//                {
//                    if (!entry.Active)
//                        continue;
//                    foreach (MouseHook.MouseEventHandler handle in entry.Handlers)
//                    {
//                        handle(mEvent, point);
//                    }
//                }
//            }
//        }

//        static void Application_ApplicationExit(object sender, EventArgs e)
//        {
//            hook.UninstallHook();
//            hook.Dispose();
//        }

//        static void Current_Exit(object sender, ExitEventArgs e)
//        {
//            hook.UninstallHook();
//            hook.Dispose();
//        }

//        public static void AddHandler(object owner, MouseHook.MouseEventHandler handler)
//        {
//            hookentry entry = entries.SingleOrDefault(e => e.Owner == owner);
//            if (entry == null)
//            {
//                entry = new hookentry() { Owner = owner };
//                entries.Add(entry);
//            }

//            lock (lockable)
//            {
//                entry.Handlers.Add(handler);
//            }
//        }

//        public static void RemoveHandler(object owner, MouseHook.MouseEventHandler handler)
//        {
//            hookentry entry = entries.SingleOrDefault(e => e.Owner == owner);
//            if (entry == null)
//                throw new Exception("This owner has not been given handlers");

//            lock (lockable)
//            {
//                entry.Handlers.Remove(handler);
//            }

//        }

//        //public static event MouseHook.MouseEventHandler MouseEvent
//        //{
//        //    add
//        //    {
//        //        hook.MouseEvent += value;
//        //    }
//        //    remove
//        //    {
//        //        hook.MouseEvent -= value;
//        //    }
//        //}

//        public static void InstallHook(object owner)
//        {
//            hookentry entry = entries.SingleOrDefault(e => e.Owner == owner);
//            if (entry == null)
//            {
//                entry = new hookentry() { Owner = owner };
//                entries.Add(entry);
//            }

//            if (entry.Active)
//                throw new Exception("This hook has already been installed on the object");
            
//            lock (lockable)
//            {
//                entry.Active = true;
//                if (counter == 0)
//                {
//                    hook.InstallHook();
//                }

//                counter++;
                
//            }

//        }

//        public static void UninstallHook(object owner)
//        {
//            hookentry entry = entries.SingleOrDefault(e => e.Owner == owner);
//            if (entry == null || !entry.Active)
//                throw new Exception("This owner has not been installed");

//            lock (lockable)
//            {
//                counter--;

//                if (counter == 0)
//                {
//                    if (hook.IsHooked) 
//                        hook.UninstallHook();
//                }
//                entry.Active = false;
//            }
//        }
//    }
//}
