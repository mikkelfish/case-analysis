using System;
using System.Collections.Generic;
using System.Text;

namespace MesosoftCommon.Development
{
    /// <summary>
    /// Give a Ceres Development attribute the proper priority that you feel this problem/request for input
    /// contains
    /// </summary>
    public enum DevelopmentPriority
    {
        /// <summary>
        /// This should be used for all bugs that are a problem whenever the function is called
        /// or implementation that needs to be done to get the object to work
        /// </summary>
        Critical,
        /// <summary>
        /// This should be used when you want to mark where there should be exception handling 
        /// but you don't want to do it yet or it isn't decided how to handle it.
        /// </summary>
        Exception,
        /// <summary>
        /// This should be used primarily for optimization possibilities, but also for bugs 
        /// that appear in rare situations
        /// </summary>
        Low,
        /// <summary>
        /// This should be used only if you don't know whether something is correct or whether
        /// something can be done, and want someone else to look at it.
        /// </summary>
        Glance
    }

    /// <summary>
    /// Attribute applied to targets that need implementation or testing.
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public class TodoAttribute : DevelopmentAttribute
    {
        private DevelopmentPriority priority;
        private bool completed;

        /// <summary>
        /// People should remember to mark things as completed when the issue is resolved. Attributes
        /// will only be removed on version review.
        /// </summary>
        public bool Completed
        {
            get
            {
                return this.completed;
            }
            set
            {
                this.completed = value;
            }
        }

        private string completedPerson;
        /// <summary>
        /// The person that addressed the issue.
        /// </summary>
        public string CompletedByPerson
        {
            get { return completedPerson; }
            set { completedPerson = value; }
        }
	

        private string completedDate;
        /// <summary>
        /// The date that the issue was resolved.
        /// </summary>
        public string CompletedDate
        {
            get { return completedDate; }
            set { completedDate = value; }
        }
	

        /// <summary>
        /// Gets or sets the priority for the attribute
        /// </summary>
        public DevelopmentPriority Priority
        {
            get
            {
                return this.priority;
            }
            set
            {
                this.priority = value;
            }
        }

        /// <summary>
        /// Default priority is Critical. Be sure to change it if this is not applicable.
        /// </summary>
        /// <param name="author"></param>
        /// <param name="date"></param>
        public TodoAttribute(string author, string date)
            : base(author, date)
        {
            this.priority = DevelopmentPriority.Critical;
            this.completed = false;
        }
    }
}
