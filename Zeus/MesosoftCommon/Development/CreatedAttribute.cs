using System;
using System.Collections.Generic;
using System.Text;

namespace MesosoftCommon.Development
{
    /// <summary>
    /// Apply when you create an item
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public class CreatedAttribute : DevelopmentAttribute
    {
        #region Private Variables
        private DocumentedStage documented = DocumentedStage.None;
        #endregion

        #region Private Functions

        #endregion

        #region Properties
        /// <summary>
        /// What stage has this been documented
        /// </summary>
        public DocumentedStage DocumentedStatus
        {
            get
            {
                return this.documented;
            }
            set
            {
                this.documented = value;
            }
        }
        #endregion

        #region Constructors
        public CreatedAttribute(string author, string date)
            : base(author, date)
        {

        }
        #endregion

        #region Public Functions

        #endregion
    }
}
