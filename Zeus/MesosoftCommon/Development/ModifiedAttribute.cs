using System;
using System.Collections.Generic;
using System.Text;

namespace MesosoftCommon.Development
{
    /// <summary>
    /// Apply when you make a modification to an existing item that has been accepted in a prior form.
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public class ModifiedAttribute : DevelopmentAttribute
    {
        

        #region Private Variables
        private DocumentedStage documented = DocumentedStage.None;
        #endregion

        #region Private Functions

        #endregion

        #region Properties
        /// <summary>
        /// What stage has this been documented
        /// </summary>
        public DocumentedStage DocumentedStatus
        {
            get
            {
                return this.documented;
            }
            set
            {
                this.documented = value;
            }
        }
        #endregion

        #region Constructors
        public ModifiedAttribute(string author, string date)
            : base(author, date)
        {

        }
        #endregion

        #region Public Functions

        #endregion
    }
}
