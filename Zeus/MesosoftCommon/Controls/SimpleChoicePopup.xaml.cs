﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace MesosoftCommon.Controls
{
    /// <summary>
    /// Interaction logic for SimpleChoicePopup.xaml
    /// </summary>
    public partial class SimpleChoicePopup : System.Windows.Window
    {
        private ObservableCollection<object> items = new ObservableCollection<object>();
        public ObservableCollection<object> Items
        {
            get { return items; }
        }



        public SelectionMode SelectionMode
        {
            get { return (SelectionMode)GetValue(SelectionModeProperty); }
            set { SetValue(SelectionModeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectionMode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectionModeProperty =
            DependencyProperty.Register("SelectionMode", typeof(SelectionMode), typeof(SimpleChoicePopup));

    

        private IList obj;
        public IList Result
        {
            get 
            {
                return obj; 
            
            }
            set { obj = value; }
        }	

        public SimpleChoicePopup()
        {
            InitializeComponent();
        }

        public SimpleChoicePopup(ICollection items)
            : this()
        {
            foreach (object item in items)
            {
                this.items.Add(item);
            }
        }
    }
}
