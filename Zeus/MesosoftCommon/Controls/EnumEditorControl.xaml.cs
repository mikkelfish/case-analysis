﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MesosoftCommon.Controls
{
    /// <summary>
    /// Interaction logic for EnumEditorControl.xaml
    /// </summary>
    public partial class EnumEditorControl : UserControl
    {

        private SimpleChoicePopup popup = new SimpleChoicePopup();

        public Type EnumerationType
        {
            get { return (Type)GetValue(EnumerationTypeProperty); }
            set { SetValue(EnumerationTypeProperty, value); }
        }



        public bool AllowEditing
        {
            get { return (bool)GetValue(AllowEditingProperty); }
            set { SetValue(AllowEditingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowEditing.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowEditingProperty =
            DependencyProperty.Register("AllowEditing", typeof(bool), typeof(EnumEditorControl));



        // Using a DependencyProperty as the backing store for EnumerationType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnumerationTypeProperty =
            DependencyProperty.Register("EnumerationType", typeof(Type), typeof(EnumEditorControl), new UIPropertyMetadata(new DependencyPropertyChangedEventHandler(enumerationChanged)));

        private static void enumerationChanged(object o, DependencyPropertyChangedEventArgs e)
        {
            EnumEditorControl cont = o as EnumEditorControl;
            Type newType = e.NewValue as Type;

            if (newType.IsEnum)
            {
                throw new InvalidOperationException("This is not an enumeration");
            }

            
        }



        public EnumEditorControl()
        {
            InitializeComponent();
        }
    }
}
