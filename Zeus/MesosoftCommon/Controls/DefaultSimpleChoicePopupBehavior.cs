﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MesosoftCommon.ExtensionMethods;

namespace MesosoftCommon.Controls
{
    public class DefaultSimpleChoicePopupBehavior : FrameworkElement
    {
        public SimpleChoicePopup SimpleChoice
        {
            get { return (SimpleChoicePopup)GetValue(SimpleChoiceProperty); }
            set { SetValue(SimpleChoiceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SimpleChoice.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SimpleChoiceProperty =
            DependencyProperty.Register("SimpleChoice", typeof(SimpleChoicePopup), typeof(DefaultSimpleChoicePopupBehavior));



        public ListBox ListBox
        {
            get { return (ListBox)GetValue(ListBoxProperty); }
            set { SetValue(ListBoxProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ListBox.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ListBoxProperty =
            DependencyProperty.Register("ListBox", typeof(ListBox), typeof(DefaultSimpleChoicePopupBehavior));



        public const string MouseOver = "mouseOverHandler";
        private void mouseOverHandler(object sender, MouseEventArgs e)
        {
            if (this.SimpleChoice.SelectionMode != SelectionMode.Single) 
                return;
            this.ListBox.SelectedItem = (sender as ListBoxItem).Content;
        }

        public const string ItemClicked = "itemClickedHandler";
        private void itemClickedHandler(object sender, MouseEventArgs e)
        {
            if (this.SimpleChoice.SelectionMode != SelectionMode.Single) return;
            this.SimpleChoice.Result = this.ListBox.SelectedItems;
            this.SimpleChoice.Close();
        }

        public const string DeactivatedString = "lostFocusHandler";
        private void lostFocusHandler(object sender, EventArgs e)
        {
            if (sender == SimpleChoice)
            {
                if (this.SimpleChoice.IsVisible) 
                    this.SimpleChoice.Close();
            }
        }

        public const string AddClicked = "addClickedHandler";
        private void addClickedHandler(object sender, RoutedEventArgs e)
        {
            this.SimpleChoice.Result = this.ListBox.SelectedItems;
            this.SimpleChoice.Close();
        }

        public const string CloseClicked = "closeClickedHandler";
        private void closeClickedHandler(object sender, RoutedEventArgs e)
        {
            this.SimpleChoice.Close();
        }

    }
}
