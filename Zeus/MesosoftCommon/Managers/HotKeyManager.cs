﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MesosoftCommon.Layout.BasicPanels;

namespace MesosoftCommon.Managers
{
    public class HotKeyEventArgs : EventArgs
    {
        public FrameworkElement OriginatingObject { get; private set; }
    }

    public class HotKeyContext
    {
        public string ContextName { get; set; }
    }

    public static class HotKeyManager
    {
        private static List<Panel> managedPanels = new List<Panel>();
        private static List<int> currentEventArgs = new List<int>();
        private static List<Key> currentKeysDown = new List<Key>();
        private static object lockable = new object();
        private static MouseButtonEventArgs latestMouseEvent;

        static HotKeyManager()
        {
            EventManager.RegisterClassHandler(typeof(FrameworkElement), FrameworkElement.PreviewKeyDownEvent, new RoutedEventHandler(handlePreviewKeyDown));
            EventManager.RegisterClassHandler(typeof(FrameworkElement), FrameworkElement.PreviewKeyUpEvent, new RoutedEventHandler(handlePreviewKeyUp));
            EventManager.RegisterClassHandler(typeof(FrameworkElement), FrameworkElement.PreviewMouseDownEvent, new RoutedEventHandler(handlePreviewMouseDown));
            EventManager.RegisterClassHandler(typeof(FrameworkElement), FrameworkElement.PreviewMouseUpEvent, new RoutedEventHandler(handlePreviewMouseUp));   
        }

        public static void StartTracking()
        {

        }

        public static void EndTracking()
        {

        }

        private static void handlePreviewKeyDown(object sender, RoutedEventArgs e)
        {
            System.Windows.Input.KeyEventArgs ke = e as System.Windows.Input.KeyEventArgs;

            lock (lockable)
            {
                if (!currentEventArgs.Contains(e.GetHashCode()))
                {
                    if (ke.IsDown && !currentKeysDown.Contains(ke.Key))
                        currentKeysDown.Add(ke.Key);
                }
                else
                {
                    if (e.OriginalSource == e.Source)
                        currentEventArgs.Remove(e.GetHashCode());
                }
            }

            e.Handled = handleMatching();
        }

        private static void handlePreviewKeyUp(object sender, RoutedEventArgs e)
        {
            System.Windows.Input.KeyEventArgs ke = e as System.Windows.Input.KeyEventArgs;

            lock (lockable)
            {
                if (!currentEventArgs.Contains(e.GetHashCode()))
                {
                    if (ke.IsUp && currentKeysDown.Contains(ke.Key))
                        currentKeysDown.Remove(ke.Key);
                }
                else
                {
                    if (e.OriginalSource == e.Source)
                        currentEventArgs.Remove(e.GetHashCode());
                }
            }

            e.Handled = handleMatching();
        }

        private static void handlePreviewMouseDown(object sender, RoutedEventArgs e)
        {
            latestMouseEvent = e as System.Windows.Input.MouseButtonEventArgs;

            e.Handled = handleMatching();
        }

        private static void handlePreviewMouseUp(object sender, RoutedEventArgs e)
        {
            latestMouseEvent = e as System.Windows.Input.MouseButtonEventArgs;

            e.Handled = handleMatching();
        }

        private static bool handleMatching()
        {
            if (currentKeysDown.Count == 1)
            {
                if (latestMouseEvent != null)
                {
                    if ((currentKeysDown.Contains(Key.LeftShift) || currentKeysDown.Contains(Key.RightShift)) &&
                        latestMouseEvent.ChangedButton == MouseButton.Left && latestMouseEvent.LeftButton == MouseButtonState.Pressed)
                    {
                        //Dockstate on/off
                        if (MesoGrid.IsDragModeActive)
                        {
                            //MesoGrid.ChangeDockingStateOfLastElementClicked();
                            return false;
                        }
                    }
                    if (currentKeysDown.Contains(Key.LeftCtrl) && latestMouseEvent.ChangedButton == MouseButton.Left &&
                        latestMouseEvent.LeftButton == MouseButtonState.Pressed && latestMouseEvent.ClickCount == 2)
                    {
                        if (MesoGrid.IsResizeModeActive)
                        {
                            MesoGrid.MaximizeLastElementClicked();
                            return true;
                        }
                        else if (MesoGrid.IsDragModeActive)
                        {
                            MesoGrid.ChangeConstraintOfLastElementClicked();
                            return true;
                        }
                    }
                }
            }
            else if (currentKeysDown.Count == 2)
            {
                if (latestMouseEvent != null)
                {
                    if (currentKeysDown.Contains(Key.LeftShift) && currentKeysDown.Contains(Key.LeftCtrl) &&
                        latestMouseEvent.ChangedButton == MouseButton.Left && latestMouseEvent.LeftButton == MouseButtonState.Pressed)
                    {
                        //Enable movement between grids on this object.
                        if (MesoGrid.IsDragModeActive)
                        {
                            //taken out
                            return false;
                        }
                    }
                }

                if ((currentKeysDown.Contains(Key.LeftCtrl) || currentKeysDown.Contains(Key.RightCtrl)) && currentKeysDown.Contains(Key.D))
                {
                    MesoGrid.IsDragModeActive = !MesoGrid.IsDragModeActive;
                    return true;
                }
                else if ((currentKeysDown.Contains(Key.LeftCtrl) || currentKeysDown.Contains(Key.RightCtrl)) && currentKeysDown.Contains(Key.R))
                {
                    MesoGrid.IsResizeModeActive = !MesoGrid.IsResizeModeActive;
                    return true;
                }
                else if (currentKeysDown.Contains(Key.LeftCtrl) && currentKeysDown.Contains(Key.L))
                {
                    MesoGrid.IsLockingModeActive = !MesoGrid.IsLockingModeActive;
                    return true;
                }
            }
            else if (currentKeysDown.Count == 3)
            {
                if (currentKeysDown.Contains(Key.LeftCtrl) && currentKeysDown.Contains(Key.LeftShift) && currentKeysDown.Contains(Key.R))
                {
                    MesoGrid.IsResizeMarginMirrorRestraintActive = !MesoGrid.IsResizeMarginMirrorRestraintActive;
                    return true;
                }
            }

            return false;
        }

        public static void RegisterPanel(Panel panelToRegister, List<HotKeyContext> listOfContextsToRegisterTo)
        {

        }
    }
}
