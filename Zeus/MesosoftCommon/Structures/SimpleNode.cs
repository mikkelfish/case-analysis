﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MesosoftCommon.Structures
{
    public class SimpleNode
    {
        List<SimpleNode> childNodes = new List<SimpleNode>();
        public SimpleNode[] ChildNodes
        {
            get
            {
                return this.childNodes.ToArray();
            }
        }
        
        private string text;
        public string Text
        {
            get { return this.text; }
        }

        private object source;
        public object Source
        {
            get { return source; }
        }

        private SimpleNode parent;
        public SimpleNode Parent
        {
            get 
            { 
                return parent; 
            }
            set 
            {

                if (this.parent != null)
                {
                    this.parent.childNodes.Remove(this);
                }
                parent = value;
                this.parent.childNodes.Add(this);
            }
        }
	

        public SimpleNode()
        {
        }

        public SimpleNode(string text)
        {
            this.text = text;
        }

        public SimpleNode(object newSource)
        {
            this.source = newSource;
            this.text = newSource.ToString();
        }

        public SimpleNode(object newSource, string text)
        {
            this.source = newSource;
            this.text = text;
        }

        public override string ToString()
        {
            return this.text;
        }

        /// <summary>
        /// True if the node does not have a source (is a "Sourceless" node)
        /// </summary>
        public bool IsSourceless
        {
            get 
            {
                if (this.Source == null) return true;
                return false;
            }
        }

        /// <summary>
        /// Gets the child of the current node by its name.
        /// </summary>
        /// <param name="name">The string which is matched to the text value of the child</param>
        /// <returns>The child node or null</returns>
        public SimpleNode GetNamedChild(string name)
        {
            SimpleNode ret = null;
            foreach (SimpleNode thisNode in this.ChildNodes)
            {
                if (thisNode.text == name)
                {
                    ret = thisNode;
                    break;
                }
            }
            return ret;
        }

        /// <summary>
        /// Builds or nests an entire tree structure inside of this, defined by a string.  If a node in the string doesn't exist, it's created,
        ///  if it does exist then the remaining sub-string is built/nested inside of the existing node.
        /// </summary>
        /// <param name="sourceString">The string that defines the tree.</param>
        /// <param name="delimiter">The characters used to seperate node-levels in the sourceString.</param>
        /// <param name="nodeSource">The source backing for the terminal node in the sourceString.</param>
        /// <returns>The terminal SimpleNode generated.</returns>
        public SimpleNode BuildNodeTreeFromString(string sourceString, char[] delimiters, object nodeSource)
        {
            string[] splitString = sourceString.Split(delimiters, System.StringSplitOptions.RemoveEmptyEntries);
            SimpleNode currentNode = this;
            SimpleNode childNode = null;
            int i = 0;

            //First, iterate through all non-terminal nodes of the splitstring.
            for (; i < splitString.Length - 1; i++)
            {
                childNode = currentNode.GetNamedChild(splitString[i]);
                if (childNode == null)
                {
                    //Backing objects are not provided for these nodes, so instantiate them as sourceless nodes.
                    childNode = new SimpleNode(splitString[i]);
                    childNode.Parent = currentNode;
                 //   currentNode.ChildNodes.Add(childNode);
                }
                currentNode = childNode;
            }

            //Now handle the terminal node.  We don't care if it's a duplicate.
            childNode = new SimpleNode(nodeSource,splitString[i]);
            //currentNode.ChildNodes.Add(childNode);
            childNode.Parent = currentNode;

            return childNode;
        }
    }
}