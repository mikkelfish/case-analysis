using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace MesosoftCommon.Utilities.Binders
{
    /// <summary>
    /// Takes a member info and checks to see if there is an attached type conversion attribute specifying how to bind to the target.
    /// </summary>
    public class TypeConversionBinder : Binder
    {
        private MemberInfo info;
        public MemberInfo MemberInfo
        {
            get { return info; }
        }

        private ITypeDescriptorContext context;
        public ITypeDescriptorContext Context
        {
            get { return context; }
        }

        public TypeConversionBinder(MemberInfo info, ITypeDescriptorContext context)
        {            
            this.info = info;
            this.context = context;
        }

        private class BinderState
        {
            public object[] args;
        }

        public override FieldInfo BindToField(BindingFlags bindingAttr, FieldInfo[] match, object value, System.Globalization.CultureInfo culture)
        {
            if (match == null)
                throw new ArgumentNullException("match");
            // Get a field for which the value parameter can be converted to the specified field type.
            for (int i = 0; i < match.Length; i++)
                if (this.ChangeType(value, match[i].FieldType, culture) != null)
                    return match[i];
            return null;
        }

        public override MethodBase BindToMethod(BindingFlags bindingAttr, MethodBase[] match, ref object[] args, ParameterModifier[] modifiers, System.Globalization.CultureInfo culture, string[] names, out object state)
        {
            // Store the arguments to the method in a state object.
            BinderState myBinderState = new BinderState();
            object[] arguments = new Object[args.Length];
            args.CopyTo(arguments, 0);
            myBinderState.args = arguments;
            state = myBinderState;
            if (match == null)
                throw new ArgumentNullException();
            // Find a method that has the same parameters as those of the args parameter.
            for (int i = 0; i < match.Length; i++)
            {
                // Count the number of parameters that match.
                int count = 0;
                ParameterInfo[] parameters = match[i].GetParameters();
                // Go on to the next method if the number of parameters do not match.
                if (args.Length != parameters.Length)
                    continue;
                // Match each of the parameters that the user expects the method to have.
                for (int j = 0; j < args.Length; j++)
                {
                    // If the names parameter is not null, then reorder args.
                    if (names != null)
                    {
                        if (names.Length != args.Length)
                            throw new ArgumentException("names and args must have the same number of elements.");
                        for (int k = 0; k < names.Length; k++)
                            if (String.Compare(parameters[j].Name, names[k].ToString()) == 0)
                                args[j] = myBinderState.args[k];
                    }
                    // Determine whether the types specified by the user can be converted to the parameter type.
                    if (ChangeType(args[j], parameters[j].ParameterType, culture) != null)
                        count += 1;
                    else
                        break;
                }
                // Determine whether the method has been found.
                if (count == args.Length)
                    return match[i];
            }
            return null;
        }

        public override object ChangeType(object value, Type type, System.Globalization.CultureInfo culture)
        {
            if (value is Type) return value;
            object[] args = this.info.GetCustomAttributes(typeof(TypeConverterAttribute), true);
            if (args == null || args.Length == 0) return null;
            TypeConverterAttribute convertType = args[0] as TypeConverterAttribute;
            TypeConverter converter = (TypeConverter)Type.GetType(convertType.ConverterTypeName).InvokeMember("", BindingFlags.CreateInstance, null, null, null);
            if (!converter.CanConvertFrom(this.context, value.GetType())) return null;
            return converter.ConvertFrom(this.context, culture, value);
        }

        public override void ReorderArgumentArray(ref object[] args, object state)
        {
            // Return the args that had been reordered by BindToMethod.
            ((BinderState)state).args.CopyTo(args, 0);
        }

        public override MethodBase SelectMethod(BindingFlags bindingAttr, MethodBase[] match, Type[] types, ParameterModifier[] modifiers)
        {
            return null;
        }

        public override PropertyInfo SelectProperty(BindingFlags bindingAttr, PropertyInfo[] match, Type returnType, Type[] indexes, ParameterModifier[] modifiers)
        {
            return null;
        }
    }
}
