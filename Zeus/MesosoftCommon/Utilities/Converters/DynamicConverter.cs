﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Data;
using System.Windows;
using System.Windows.Controls;


namespace MesosoftCommon.Utilities.Converters
{
    public class DynamicConverter : FrameworkElement, IValueConverter
    {
        public IValueConverter Converter
        {
            get { return (IValueConverter)GetValue(ConverterProperty); }
            set { SetValue(ConverterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Converter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ConverterProperty =
            DependencyProperty.Register("Converter", typeof(IValueConverter), typeof(DynamicConverter));


        public Type ConvertBackType
        {
            get { return (Type)GetValue(ConvertBackTypeProperty); }
            set { SetValue(ConvertBackTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TargetType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ConvertBackTypeProperty =
            DependencyProperty.Register("ConvertBackType", typeof(Type), typeof(DynamicConverter));



        public object Parameter
        {
            get { return (object)GetValue(ParameterProperty); }
            set { SetValue(ParameterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Parameter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParameterProperty =
            DependencyProperty.Register("Parameter", typeof(object), typeof(DynamicConverter));



        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
             if (this.Converter == null) return value;

            
            return this.Converter.Convert(value, targetType, this.Parameter, culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (this.ConvertBackType == null || this.Converter == null) return value;
            return this.Converter.ConvertBack(value, this.ConvertBackType, this.Parameter, culture);
        }

        #endregion
    }
}
