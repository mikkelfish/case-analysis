﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Data;
using System.Windows.Controls;
using MesosoftCommon.Utilities.Reflection;

namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    [ValueConversion(typeof(ValidationRule),typeof(string))]
    public class ValidationRuleValueConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string ret = "";
            if (value == null) return null;
            if (Common.GetAbsoluteType(value) != typeof(ValidationRule) || targetType != typeof(string))
                throw new NotSupportedException("Cannot convert");

            if (value is IEnumerable<ValidationRule>)
            {
                foreach (ValidationRule val in (value as IEnumerable<ValidationRule>))
                {
                    ret += val.ToString();
                    ret += ",";
                }

                if (ret == String.Empty) return String.Empty;
                return ret.Substring(0,ret.Length - 1);
            }
            return (value as ValidationRule).ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException("Cannot convert back");
        }

        #endregion
    }
}
