﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows;

namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    [ValueConversion(typeof(UIElement),typeof(Point))]
    public class UIElementPointConverter : IValueConverter
    {
        #region IValueConverter Members

        public UIElement AncestorForPoint { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(parameter is string)) return null;
            if (AncestorForPoint == null) return null;

            UIElement element = value as UIElement;
            string param = parameter as string;

            Point basePoint = element.PointToScreen
            (
            new Point(
                        (element.RenderSize.Width / 2),
                        (element.RenderSize.Height / 2)
                     )
            );

            Point localPoint = AncestorForPoint.PointFromScreen(basePoint);

            if (param == "x" || param == "X")
                return localPoint.X;
            else if (param == "y" || param == "Y")
                return localPoint.Y;
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
