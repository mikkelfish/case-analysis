﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.ComponentModel;

namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    [ValueConversion(typeof(object), typeof(object))]
    public class TypeCoverterAsValueConverter : IValueConverter
    {
        public TypeConverter Converter { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (this.Converter == null) return null;
            if (!this.Converter.CanConvertFrom(value.GetType()))
                return null;
            object toRet = this.Converter.ConvertFrom(value);
            return toRet;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (this.Converter == null) return null;
            if (!this.Converter.CanConvertTo(targetType))
                return null;
            return this.Converter.ConvertTo(value, targetType);
        }
    }
}
