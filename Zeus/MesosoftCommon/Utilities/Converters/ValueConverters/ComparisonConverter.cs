﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows;

namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    [ValueConversion(typeof(double), typeof(string))]
    public class ComparisonConverter : DependencyObject, IValueConverter
    {


        public double CompareValue
        {
            get { return (double)GetValue(CompareValueProperty); }
            set { SetValue(CompareValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CompareValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CompareValueProperty =
            DependencyProperty.Register("CompareValue", typeof(double), typeof(ComparisonConverter));


        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double val = System.Convert.ToDouble(value);
            if (val == CompareValue) return "0";
            else if (val > CompareValue) return "1";
            return "-1";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is string) || targetType != typeof(double)) return null;
            double p;
            bool ok = double.TryParse(value as string, out p);
            if (ok) return p;
            return null;
        }

        public ComparisonConverter()
        {
        }

        #endregion
    }
}
