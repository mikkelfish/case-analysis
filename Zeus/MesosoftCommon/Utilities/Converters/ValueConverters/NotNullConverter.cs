﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    [ValueConversion(typeof(object), typeof(bool))]
    public class NotNullConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return false;
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception("Attempting to ConvertBack from bool to object through NotNullConverter - operation not supported.");
        }

        #endregion
    }
}
