﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Data;

namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    [ValueConversion(typeof(Type), typeof(string))]
    public class TypeValueConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is Type) || targetType != typeof(string)) return null;
            return (value as Type).FullName;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is string) || (targetType != typeof(Type) && targetType.BaseType != typeof(Type))) return null;
            return Type.GetType(value as string);
        }

        #endregion
    }
}
