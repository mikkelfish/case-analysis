﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Text.RegularExpressions;


namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    [ValueConversion(typeof(string), typeof(string))]
    [ValueConversion(typeof(string), typeof(bool))]
    public class RegexValueConverter : IValueConverter
    {
        private bool retIsMatch;
        public bool IsMatch
        {
            get { return retIsMatch; }
            set { retIsMatch = value; }
        }
	

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(string) && targetType!=typeof(object))
            {
                if (this.IsMatch) return false;                
                return string.Empty;
            }

            string valStr = (string)value;
            Regex regex = new Regex((parameter as string));
            if (!regex.IsMatch(valStr))
            {
                if (this.IsMatch) return false;
                return string.Empty;
            }

            if (this.IsMatch) return true;

            Match match = regex.Match(valStr);
            
            return match.Value;
        }



        #endregion

        #region IValueConverter Members


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
