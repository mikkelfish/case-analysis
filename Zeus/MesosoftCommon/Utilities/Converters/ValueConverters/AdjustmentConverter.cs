﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    public class AdjustmentConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                string toRun = parameter.ToString().Trim();
                char oper = toRun[0];
                double rest = double.Parse(toRun.Substring(1));

                Type origType = value.GetType();
                double val = System.Convert.ToDouble(value);

                switch (oper)
                {
                    case '+':
                        val += rest;
                        break;

                    case '-':
                        val -= rest;
                        break;

                    case '/':
                        val /= rest;
                        break;

                    case '*':
                        val *= rest;
                        break;

                    default:
                        return value;
                }

                return System.Convert.ChangeType(val, origType);
            }
            catch
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
