﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Collections;
using System.Globalization;
using System.Windows;

namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    [ValueConversion(typeof(string), typeof(bool))]
    public class PathNotPresentConverter : IValueConverter
    {

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(bool) && targetType != typeof(object)) return null;

            string path = value as string;
            Uri temp = new Uri(path);
            if (temp.IsFile)
            {
                return File.Exists(temp.LocalPath);
            }
            else if (path.Contains("pack://application:,,,/"))
            {
                string[] split = path.Split(';');
                string assemblyStr = split[0].Substring("pack://application:,,,/".Length);
                Assembly[] referencedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (Assembly assembly in referencedAssemblies)
                {
                    string testName = assembly.FullName.Substring(0, assembly.FullName.IndexOf(','));
                    if (testName != assemblyStr) continue;

                    ResourceManager manager = null;
                    if (split[1].Contains(".xaml"))
                    {
                        manager = new ResourceManager(assemblyStr + ".g", assembly);                       
                    }
                    else
                    {
                        manager = new ResourceManager(assemblyStr + ".Properties.Resources", assembly);
                    }

                    try
                    {
                        ResourceSet set = manager.GetResourceSet(CultureInfo.CurrentCulture, true, true);
                        IDictionaryEnumerator en = set.GetEnumerator();
                        while (en.MoveNext())
                        {
                            if ((en.Key as string) == split[1])
                            {
                                return true;
                            }
                        }
                        return false;
                    }
                    catch
                    {
                        return false;
                    }
                }
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool ok;
            bool toRet = bool.TryParse(value as string, out ok);
            if (ok) return toRet;
            return null;
        }

        #endregion
    }
}
