﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Collections;
using System.Reflection;
using MesosoftCommon.IO;

namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    //[ValueConversion(typeof(Variable[]), typeof()]
    public class DisplayHierarchyConverter : IValueConverter
    {
       

        #region IValueConverter Members

        private void buildTree(GenericDisplayItem parent, object val, List<string> paths, int level)
        {
            GenericDisplayItem thisOne = null;
            if (parent != null)
            {
                thisOne = parent.Children.SingleOrDefault(p => p.Description == paths[level]);
                if (thisOne == null)
                {
                    thisOne = new GenericDisplayItem() { Description = paths[level], Parent=parent };
                    parent.Children.Add(thisOne);
                }
            }
 

            if (level < paths.Count - 1)
                this.buildTree(thisOne, val, paths, level + 1);
            else thisOne.Value = val;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string hierarchyParse = parameter as string;
            if (hierarchyParse == null) return null;

            string[] splits = hierarchyParse.Split(',');

            List<GenericDisplayItem> roots = new List<GenericDisplayItem>();

            ICollection collection = value as ICollection;
            foreach (object obj in collection)
            {
                List<string> path = new List<string>();
                foreach (string split in splits)
                {

                    string para = split;
                    string splitchar = null;
                    if (split.Contains(':'))
                    {
                        para = split.Substring(0, split.IndexOf(':'));
                        splitchar = split.Substring(split.IndexOf(':') + 1);
                    }

                    string[] paraSplit = para.Split('.');
                    object curObj = obj;
                    string val = null;
                    for (int i = 0; i < paraSplit.Length; i++)
                    {
                        PropertyInfo info = curObj.GetType().GetProperty(paraSplit[i]);
                        if (info == null) throw new Exception("Error in parsing");

                        curObj = info.GetValue(curObj, null);
                        if (curObj != null)
                            val = curObj.ToString();
                    }

                    if (splitchar != null)
                    {
                        string[] splitval = val.Split(new string[] { splitchar }, StringSplitOptions.RemoveEmptyEntries);
                        path.AddRange(splitval);
                    }
                    else
                    {
                        path.Add(val);
                    }
                }

                GenericDisplayItem thisRoot = roots.SingleOrDefault(r => r.Description == path[0]);

                if(thisRoot == null)
                    thisRoot = new GenericDisplayItem() { Description = path[0] };


                this.buildTree(thisRoot, obj, path, 1);

                if(!roots.Contains(thisRoot))
                    roots.Add(thisRoot);
            }

            return roots.ToArray();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
