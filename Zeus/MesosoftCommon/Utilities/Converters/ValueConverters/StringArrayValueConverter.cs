﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Data;

namespace MesosoftCommon.Utilities.Converters.ValueConverters
{
    [ValueConversion(typeof(string[]), typeof(string))]
    public class StringArrayValueConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return null;
            if (value.GetType() != typeof(string[]) || targetType != typeof(string))
                throw new NotSupportedException("Cannot convert");

            string separator = "||";
            if (parameter != null && parameter.GetType() == typeof(string))
                separator = parameter as string;

            string toRet = "";
            string[] val = value as string[];
            foreach (string str in val)
            {
                toRet += str + separator;
            }
            if(toRet != "") toRet = toRet.Substring(0, toRet.Length - separator.Length);
            return toRet;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return null;
            if(value.GetType() != typeof(string) || targetType != typeof(string[]))
            {
                throw new NotSupportedException("Cannot convert back");
            }

            string separator = "||";
            if (parameter != null && parameter.GetType() == typeof(string))
                separator = parameter as string;

            string[] toRet = (value as string).Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries);
            return toRet;
        }

        #endregion
    }
}
