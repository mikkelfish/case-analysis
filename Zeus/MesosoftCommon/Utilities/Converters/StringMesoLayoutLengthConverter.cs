﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using MesosoftCommon.Layout.BasicPanels.SupportControls;

namespace MesosoftCommon.Utilities.Converters
{
    public class StringMesoLayoutLengthConverter : TypeConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (value == null)
                return null;
            else if (!(value is MesoLayoutLength))
                throw new Exception("Wrong type in StringMesoLayoutLengthConverter.");

            if (destinationType == typeof(string))
            {
                MesoLayoutLength length = (MesoLayoutLength)value;

                string ret = length.Value.ToString();
                if (length.LengthType == LayoutLengthType.Percent)
                    ret += '%';
                else if (length.LengthType == LayoutLengthType.Relative)
                    ret += '*';

                return ret;
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value == null)
                return null;
            else if (!(value is string))
                return base.ConvertFrom(context, culture, value);

            MesoLayoutLength ret = new MesoLayoutLength(0);
            string str = ((string)value).Trim();

            if (str == string.Empty)
                return ret;

            //Track the last character (which can be a type indicator, remember)
            //and the "lastIndex," used in the substring
            char lastChar = str[str.Length - 1];
            int lastIndex = str.Length;
            if (lastChar == '%' || lastChar == '*')
            {
                if (lastChar == '%')
                    ret.LengthType = LayoutLengthType.Percent;
                else if (lastChar == '*')
                    ret.LengthType = LayoutLengthType.Relative;

                lastIndex--;
            }

            //If there is only one character and it was a type indicator, this is fine for relative type.
            //"%" is meaningless and throws an exception.
            if (lastIndex == 0)
            {
                if (lastChar == '*')
                {
                    ret.Value = 1;
                    return ret;
                }
                else
                    throw new Exception("Unable to parse length : term too short!");
            }

            double val = 0;
            //Parse the numerical value.  Throw an exception if it can't be parsed correctly.
            if (double.TryParse(str.Substring(0, lastIndex), out val))
            {
                ret.Value = val;
                return ret;
            }

            throw new Exception("Unable to parse length : Couldn't parse numerical term : " + str.Substring(0, lastIndex) + ".");
        }
    }
}
