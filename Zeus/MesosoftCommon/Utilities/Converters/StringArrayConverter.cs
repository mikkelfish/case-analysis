using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace MesosoftCommon.Utilities.Converters
{
    /// <summary>
    /// Convert a string separated by "||" into an array of strings
    /// </summary>
    public class StringArrayConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string)) return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            return (value as string).Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}

