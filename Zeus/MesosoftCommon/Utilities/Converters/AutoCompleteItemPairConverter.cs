﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using MesosoftCommon.AutoComplete;

namespace MesosoftCommon.Utilities.Converters
{
    class AutoCompleteItemPairConverter : TypeConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (value != null)
                if (!(value is AutoCompleteItemPair))
                    throw new Exception("Wrong Type");

            if (destinationType == typeof(string))
            {
                return (value as AutoCompleteItemPair).ToString();                 
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
