using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace MesosoftCommon.Utilities.Converters
{
    public class ListedTypeConverter<T> : TypeConverter where T: TypeConverter, new()
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string)) return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            string[] parameters = (value as string).Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
            List<object> tempList = new List<object>();
            foreach (string str in parameters)
            {
                T converter = new T();
                object obj = converter.ConvertFrom(context, culture, str);
                tempList.Add(obj);
            }
            if (tempList.Count == 0) return null;
            Type firstType = tempList[0].GetType();
            bool convert = true;
            foreach (object obj in tempList)
            {
                if (obj == null)
                {
                    return null;
                }

                if (obj.GetType() != firstType)
                {
                    convert = false;
                    break;
                }
            }

            if (convert)
            {
                Type genList = typeof(List<>).MakeGenericType(firstType);
                object createdList = genList.InvokeMember("", BindingFlags.CreateInstance, null, null, null);
                MethodInfo method = genList.GetMethod("Add");
                foreach (object obj in tempList)
                {
                    method.Invoke(createdList, new object[] { obj });
                }
            //    method.Invoke(createdList, new object[] { tempList.ToArray() });
                return createdList;

            }
            return tempList;

        }
    }
}
