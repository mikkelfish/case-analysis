﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.Utilities.Settings
{
    public static class Paths
    {
        public static Uri DefaultPath
        {
            get
            {
                return new Uri(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\Mesosoft\\");
            }
        }
    }
}
