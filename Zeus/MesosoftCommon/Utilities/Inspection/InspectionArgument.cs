﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.Utilities.Inspection
{
    public class InspectionArgument
    {
        public string Name { get; private set; }
        public object Value { get; set; }
        public InspectionArgument(string name)
        {
            this.Name = name;
        }
    }
}
