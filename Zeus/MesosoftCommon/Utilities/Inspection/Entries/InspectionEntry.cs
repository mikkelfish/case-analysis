﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows;
using System.Reflection;
using System.ComponentModel;
using System.Windows.Controls;
using System.Globalization;
using System.Windows.Input;
using MesosoftCommon.AutoComplete;
using System.Collections;
using System.Collections.Specialized;

namespace MesosoftCommon.Utilities.Inspection
{
    public class InspectionEntry : IDataErrorInfo, INotifyPropertyChanged
    {
        public string Name
        {
            get { return this.PropertyInfo.Name; }
        }

        private IValueConverter converter;
        public IValueConverter Converters
        {
            get { return converter; }
            set { converter = value; }
        }

        public Type Type
        {
            get { return this.propInfo.PropertyType; }
        }

        public Type TypeOfValue
        {
            get
            {
                return this.Value.GetType();
            }
        }

        private IAutoCompleteSourceProvider autoProvider;
        public IAutoCompleteSourceProvider AutoProvider
        {
            get { return autoProvider; }
            set { autoProvider = value; }
        }

        private object autoSource;
        public object AutoSource
        {
            get { return autoSource; }
            set { autoSource = value; }
        }

        private object autoParameter;   
        public object AutoParameter
        {
            get { return autoParameter; }
            set { autoParameter = value; }
        }
	
	
	
        private PropertyInfo propInfo;
        public PropertyInfo PropertyInfo
        {
            get { return propInfo; }
        }

        private object boundObject;
        public object BoundObject
        {
            get { return boundObject; }
            set { boundObject = value; }
        }
	

        private object val;
        public object Value
        {
            get 
            { 
                return val; 
            }
            set 
            {
                val = value;
                if(this.PropertyInfo.CanWrite) this.PropertyInfo.SetValue(this.boundObject, this.val, null);
                this.OnPropertyChanged("Value");
            }
        }

        private ValidationRule[] validations;

        public ValidationRule[] Validations
        {
            get { return validations; }
            set { validations = value; }
        }
	

        public InspectionEntry(PropertyInfo info, object boundObject)
        {
            this.boundObject = boundObject;
            this.propInfo = info;
        }

        #region IDataErrorInfo Members

        public string Error
        {
            get { return this[String.Empty]; }
        }

        public string this[string columnName]
        {
            get 
            {
                if (columnName != "Value") return string.Empty;
                if (this.validations == null) return string.Empty;
                string toRet = string.Empty;
                foreach (ValidationRule rule in this.validations)
                {
                    ValidationResult res = rule.Validate(this.val, CultureInfo.CurrentCulture); 
                    if(!res.IsValid)
                        toRet += res.ErrorContent + " AND";
                }
                if (toRet == String.Empty) return String.Empty;
                return toRet.Substring(0, toRet.Length - " AND".Length);
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}
