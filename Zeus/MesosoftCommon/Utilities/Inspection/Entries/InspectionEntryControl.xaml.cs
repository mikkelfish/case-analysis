﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MesosoftCommon.Utilities.Events;
using System.Windows.Markup;
using MesosoftCommon.ExtensionMethods;
using MesosoftCommon.Utilities.Reflection;
using System.ComponentModel;
using System.Windows.Media.Effects;
using MesosoftCommon.AutoComplete;

namespace MesosoftCommon.Utilities.Inspection
{
    /// <summary>
    /// Interaction logic for InspectionEntryControl.xaml
    /// </summary>
    public partial class InspectionEntryControl : UserControl
    {

        public InspectionEntry Entry
        {
            get { return (InspectionEntry)GetValue(EntryProperty); }
            set { SetValue(EntryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Entry.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EntryProperty =
            DependencyProperty.Register("Entry", typeof(InspectionEntry), typeof(InspectionEntryControl), new PropertyMetadata(new PropertyChangedCallback(InspectionEntryControl.OnEntryPropertyChanged)));

        protected static void OnEntryPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            InspectionEntryControl self = (obj as InspectionEntryControl);
            self.DataContext = e.NewValue;

            self.IsEditable = true;
            //if (self.Entry.Value is Atom)
            //    self.IsEditable = false;
            //else
            //    self.IsEditable = true;
        }


        public bool IsEditable
        {
            get { return (bool)GetValue(IsEditableProperty); }
            set { SetValue(IsEditableProperty, value); }
        }
        // Using a DependencyProperty as the backing store for ButtonContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsEditableProperty =
            DependencyProperty.Register(
              "IsEditable",
              typeof(bool),
              typeof(InspectionEntryControl)
            );


        public InspectionEntryControl()
        {
            InitializeComponent();
        }
    }
}
