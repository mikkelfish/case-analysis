﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.AutoComplete;

namespace MesosoftCommon.Utilities.Inspection
{
    public class InspectionEditorAutoProvider : MesosoftCommon.AutoComplete.IAutoCompleteSourceProvider
    {
        #region IAutoCompleteSourceProvider Members

        public bool CanGetAutoCompleteItems(object source, object parameter)
        {
            if (!(source is InspectionEntry)) return false;
            InspectionEntry entry = source as InspectionEntry;
            if (entry.AutoProvider == null) return false;
            return entry.AutoProvider.CanGetAutoCompleteItems(entry.AutoSource, entry.AutoParameter);
        }

        public AutoCompleteItemPair[] GetAutoCompleteItems(object source, object parameter, System.Globalization.CultureInfo culture, int maxItems)
        {
            InspectionEntry entry = source as InspectionEntry;
            return entry.AutoProvider.GetAutoCompleteItems(entry.AutoSource, entry.AutoParameter, culture, maxItems);
        }

        #endregion
    }
}
