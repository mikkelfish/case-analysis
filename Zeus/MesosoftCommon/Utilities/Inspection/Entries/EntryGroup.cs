﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.Utilities.Inspection
{
    public class EntryGroupEntry
    {
        private string propertyName;
        public string PropertyName
        {
            get { return propertyName; }
            set { propertyName = value; }
        }

        private Type type;
        public Type TargetType
        {
            get { return type; }
            set { type = value; }
        }
	
	
    }

    public class EntryGroup
    {
        private string groupName;
        public string GroupName
        {
            get { return groupName; }
            set { groupName = value; }
        }

        private List<EntryGroupEntry> entries = new List<EntryGroupEntry>(); 
        public List<EntryGroupEntry> Entries
        {
            get { return entries; }
        }
	
	
    }
}
