﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using MesosoftCommon.AutoComplete;
using MesosoftCommon.Resources;

namespace MesosoftCommon.Utilities.Inspection
{
    public class EntryResource : UnifiedResourceEntry
    {
        private string groupName;
        public string GroupName
        {
            get { return groupName; }
            set { groupName = value; }
        }
	
        private Type targetType;
        public Type TargetType
        {
            get { return targetType; }
            set { targetType = value; }
        }

        private string propertyName;
        public string PropertyName
        {
            get { return propertyName; }
            set { propertyName = value; }
        }

        public bool IsEditable { get; set; }
        public Type OwnerType { get; set; }

        private IValueConverter converter;
        public IValueConverter Converter
        {
            get { return converter; }
            set { converter = value; }
        }

        private AutoCompleteSourceItem autoCompleteInfo;
        public AutoCompleteSourceItem AutoCompleteInfo
        {
            get { return autoCompleteInfo; }
            set { autoCompleteInfo = value; }
        }

        private List<ValidationRule> rules = new List<ValidationRule>();
        public List<ValidationRule> ValidationRules
        {
            get { return rules; }
        }	
    }
}
