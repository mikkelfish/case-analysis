﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OntologyCreation.Entries
{
    public interface IRequireOntologyEntry
    {
        OntologyEntry Entry { get; set; }
    }
}
