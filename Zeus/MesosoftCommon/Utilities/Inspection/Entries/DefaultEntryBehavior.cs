﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MesosoftCommon.ExtensionMethods;
using System.Windows.Data;

namespace MesosoftCommon.Utilities.Inspection
{
    public class DefaultEntryBehavior : FrameworkElement
    {


        public InspectionEntryControl EntryControl
        {
            get { return (InspectionEntryControl)GetValue(EntryControlProperty); }
            set { SetValue(EntryControlProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EntryControl.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EntryControlProperty =
            DependencyProperty.Register("EntryControl", typeof(InspectionEntryControl), typeof(DefaultEntryBehavior));

	

        public const string ButtonClickedHandler = "ButtonClicked";
        private void ButtonClicked(object sender, RoutedEventArgs e)
        {
            if (this.EntryControl.Entry.Value == null)
                return;

            if (!this.EntryControl.IsEditable)
            {
                InspectionDataDockPopup inspectWindow = new InspectionDataDockPopup();
                inspectWindow.Source = this.EntryControl.Entry.BoundObject;
                inspectWindow.Owner = Window.GetWindow(this.EntryControl);
                inspectWindow.Show();
            }
            else
            {
                InspectionObjectEditor editWindow = new InspectionObjectEditor();
                editWindow.Source = this.EntryControl.Entry.Value;
                editWindow.Owner = Window.GetWindow(this.EntryControl);
                editWindow.Show();
            }

        }

    }
}
