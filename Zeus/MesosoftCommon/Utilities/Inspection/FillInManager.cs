﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.CommonAddIns.BlindAssignmentAddIns;
using System.Reflection;
using System.Collections.Specialized;
using MesosoftCommon.AddIns;
using MesosoftCommon.AddIns.Attributes;
using MesosoftCommon.Utilities.Reflection;
using MesosoftCommon.Attributes;
using MesosoftCommon.Interfaces;

namespace MesosoftCommon.Utilities.Inspection
{
    public static class FillInManager
    {
        private static List<CommonAddIns.BlindAssignmentAddIns.BlindAssignment> assignments = new List<MesosoftCommon.CommonAddIns.BlindAssignmentAddIns.BlindAssignment>();

        private static List<EventHandler<PreviewBlindAssignmentEventArgs>> previewHandlers = new List<EventHandler<PreviewBlindAssignmentEventArgs>>();
        private static List<EventHandler<BlindAssignmentEventArgs>> assignmentHandlers = new List<EventHandler<BlindAssignmentEventArgs>>();


        static FillInManager()
        {
            Build(Assembly.GetEntryAssembly().Location.Substring(0, Assembly.GetEntryAssembly().Location.LastIndexOf('\\')), true, null);
        }

        private static void createAssignment(AddInToken<BlindAssignment> token, double version)
        {
            BlindAssignment assignment = token.CreateAddIn(MesosoftCommon.AddIns.AddInLoadingEnvironment.CurrentDomain, ref version, false);

            foreach (EventHandler<PreviewBlindAssignmentEventArgs> preview in previewHandlers)
            {
                assignment.PreviewAssigned += preview;
            }

            foreach (EventHandler<BlindAssignmentEventArgs> assgnHandler in assignmentHandlers)
            {
                assignment.Assigned += assgnHandler;
            }

            assignments.Add(assignment);
        }

        public static void Build(string path, bool includeBase, string[] otherAddIns)
        {
            AddIns.AddInManager.BuildAll(path, includeBase, otherAddIns);
            AddIns.AddInToken<BlindAssignment>[] tokens = AddIns.AddInManager.GetAddIns<BlindAssignment>();
            foreach (AddIns.AddInToken<BlindAssignment> assgn in tokens)
            {
                createAssignment(assgn, double.MaxValue);
            }
        }

        public static void ClearFillIns()
        {
            assignments.Clear();
        }

        public static void ClearHandlers()
        {
            foreach (BlindAssignment blind in assignments)
            {
                foreach (EventHandler<PreviewBlindAssignmentEventArgs> preview in previewHandlers)
                {
                    blind.PreviewAssigned -= preview;
                }

                foreach (EventHandler<BlindAssignmentEventArgs> handler in assignmentHandlers)
                {
                    blind.Assigned -= handler;
                }
            }


            previewHandlers.Clear();
            assignmentHandlers.Clear();
        }

        public static void ClearAll()
        {
            ClearHandlers();
            ClearFillIns();
        }

        public static void AddUniversalHandlers(EventHandler<PreviewBlindAssignmentEventArgs> previewHandler, EventHandler<BlindAssignmentEventArgs> assignmentHandler)
        {
            if (previewHandler != null)
                previewHandlers.Add(previewHandler);
            if (assignmentHandler != null)
                assignmentHandlers.Add(assignmentHandler);


            foreach (BlindAssignment assgn in assignments)
            {
                if (previewHandler != null) assgn.PreviewAssigned += previewHandler;
                if (assignmentHandler != null) assgn.Assigned += assignmentHandler;
            }
        }

        public static void RemoveUniversalHandlers(EventHandler<PreviewBlindAssignmentEventArgs> previewHandler, EventHandler<BlindAssignmentEventArgs> assignmentHandler)
        {
            if (previewHandler != null)
                previewHandlers.Remove(previewHandler);
            if (assignmentHandler != null)
                assignmentHandlers.Remove(assignmentHandler);

            foreach (BlindAssignment assgn in assignments)
            {
                if (previewHandler != null) assgn.PreviewAssigned -= previewHandler;
                if (assignmentHandler != null) assgn.Assigned -= assignmentHandler;
            }
        }


        public static object GetObject(object obj, object[] args, FillInCopyMode copyMode, EventHandler<PreviewBlindAssignmentEventArgs>[] previewHandlers, EventHandler<BlindAssignmentEventArgs>[] assignmentHandlers)
        {
            foreach (BlindAssignment assgn in assignments)
            {
                if (previewHandlers != null)
                {
                    foreach (EventHandler<PreviewBlindAssignmentEventArgs> pHandler in previewHandlers)
                    {
                        assgn.PreviewAssigned += pHandler;
                    }
                }

                if (assignmentHandlers != null)
                {
                    foreach (EventHandler<BlindAssignmentEventArgs> handler in assignmentHandlers)
                    {
                        assgn.Assigned += handler;
                    }
                }

                PreviewBlindAssignmentEventArgs options;      
     
                bool shouldCopy = false;
                if (copyMode == FillInCopyMode.Copy)
                    shouldCopy = true;
                else if (copyMode == FillInCopyMode.LetObjectDecide)
                {
                    if (obj is IShouldCopy)
                    {
                        shouldCopy = (obj as IShouldCopy).ShouldCopy;
                    }
                    else if (obj is ICloneable)
                        shouldCopy = true;                    
                }

                bool can = assgn.CanAssign(obj, args, shouldCopy, out options);                
                if (can) obj = assgn.Assign(obj, options);

                if (previewHandlers != null)
                {
                    foreach (EventHandler<PreviewBlindAssignmentEventArgs> pHandler in previewHandlers)
                    {
                        assgn.PreviewAssigned -= pHandler;
                    }
                }

                if (assignmentHandlers != null)
                {
                    foreach (EventHandler<BlindAssignmentEventArgs> handler in assignmentHandlers)
                    {
                        assgn.Assigned -= handler;
                    }
                }
            }

            return obj;
        }

        public static object GetObject(object obj, object[] args, FillInCopyMode copyMode)
        {
            return GetObject(obj, args, copyMode, null, null);
        }

        public enum FillInCopyMode { Copy, Leave, LetObjectDecide };
    }
}
