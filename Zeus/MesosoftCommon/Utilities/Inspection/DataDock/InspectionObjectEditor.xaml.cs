﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using MesosoftCommon.Utilities.Reflection;
using MesosoftCommon.Structures;
using System.ComponentModel;
using MesosoftCommon.ExtensionMethods;

namespace MesosoftCommon.Utilities.Inspection
{
    /// <summary>
    /// Interaction logic for InspectionObjectEditor.xaml
    /// </summary>
    public partial class InspectionObjectEditor : System.Windows.Window, INotifyPropertyChanged
    {

        public object Source
        {
            get { return (object)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register(
              "Source",
              typeof(object),
              typeof(InspectionObjectEditor),
              new PropertyMetadata(sourcePropertyChanged)
            );
        protected static void sourcePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            InspectionObjectEditor self = obj as InspectionObjectEditor;
            self.SourceCollection.Clear();
            string identifier;
            
            if (e.NewValue == null)
            {
                self.Title = "Null";
                return;
            }

            if (self.Source.GetType().IsGenericType) identifier = self.Source.GetType().GetGenericArguments()[0].Name + " Collection";
            else identifier = self.Source.GetType().Name;

            self.Title = identifier;

            //check to see if the source is a collection
            if (e.NewValue is IList)
            {
                foreach (object item in (e.NewValue as IList))
                {
                    self.SourceCollection.Add(item);
                }

                self.InternalIsACollection = true;

            }
            else
            {
                self.InternalIsACollection = false;
                //The value is singular, so just link it over
                self.dataDockSource = self.Source;
            }

            Type[] currentDomainAtoms = self.retrieveTypes.GetTypesPresentInRunningAssembly(e.NewValue.GetType(), false);
            for (int i = 0; i < currentDomainAtoms.Length; i++)
            {
                self.typesList.Add(currentDomainAtoms[i]);
            }
        }

        private ObservableCollection<object> sourceCollection = new ObservableCollection<object>();
        public ObservableCollection<object> SourceCollection
        {
            get { return sourceCollection; }
        }
	

        public void AddItemToSource(object newItem)
        {
            this.SourceCollection.Add(newItem);
        }

        public void SaveSourceCollectionToSource()
        {
            if (!(this.IsACollection)) return;
            (this.Source as IList).Clear();
            foreach (object item in SourceCollection)
            {
                (this.Source as IList).Add(item);
            }
        }

        private object dataDockSource;
        public object DataDockSource
        {
            get 
            { 
                return dataDockSource; 
            }
            set 
            { 
                dataDockSource = value;

                if (value == null)
                {
                    this.TypeSelector.SelectedIndex = -1;
                    this.notifyPropertyChanged("DataDockSource");
                    return;
                }

                if (this.typesList.Contains(value.GetType())) this.TypeSelector.SelectedIndex = this.typesList.IndexOf(value.GetType());
                else this.TypeSelector.SelectedIndex = -1;
                this.notifyPropertyChanged("DataDockSource");
            }
        }

        private SimpleNode rootNode;
        private SimpleNode InternalRootNode
        {
            get
            {
                return this.rootNode;
            }
            set
            {
                this.rootNode = value;
                this.notifyPropertyChanged("RootNode");
            }
        }

        public SimpleNode RootNode
        {
            get { return this.InternalRootNode; }
        }

        private bool isACollection = false;
        private bool InternalIsACollection
        {
            get
            {
                return this.isACollection;
            }
            set
            {
                this.isACollection = value;
                this.notifyPropertyChanged("IsACollection");
            }
        }

        public bool IsACollection
        {
            get { return this.InternalIsACollection; }
        }


        private ObservableCollection<Type> typesList = new ObservableCollection<Type>();
        public ObservableCollection<Type> TypesList
        {
            get { return typesList; }
            set { typesList = value; }
        }

        private RetrieveLoadedTypes retrieveTypes = new RetrieveLoadedTypes();

        public InspectionObjectEditor()
        {
            InitializeComponent();

            this.SourceCollection.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(SourceCollection_CollectionChanged);
        }

        void SourceCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            string identifier;

            if (this.Source.GetType().IsGenericType) identifier = this.Source.GetType().GetGenericArguments()[0].Name + " Collection";
            else identifier = this.Source.GetType().Name;

            this.InternalIsACollection = true;

            //generate the Node-set to use for the tree
            SimpleNode topNode = new SimpleNode(identifier);
            foreach (object value in (this.SourceCollection))
            {
                SimpleNode child = new SimpleNode(value);
                child.Parent = topNode;
                //topNode.ChildNodes.Add(child);
            }
            SimpleNode newRoot = new SimpleNode();
            topNode.Parent = newRoot;
           // newRoot.ChildNodes.Add(topNode);
            this.InternalRootNode = newRoot;
            
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void notifyPropertyChanged(string name)
        {
            PropertyChangedEventHandler handle = this.PropertyChanged;
            if (handle == null) return;
            handle(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
