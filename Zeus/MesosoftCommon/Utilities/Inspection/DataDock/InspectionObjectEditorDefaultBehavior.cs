﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;
using MesosoftCommon.Structures;
using System.Windows.Controls;
using System.Collections;
using System.Collections.Specialized;

namespace MesosoftCommon.Utilities.Inspection
{
    public class InspectionObjectEditorDefaultBehavior : FrameworkElement
    {
        public const string MouseDownHandler = "MouseClickOnTreeSelectorItem";
        public const string NewButtonClickHandler = "NewButtonClicked";
        public const string AcceptButtonClickHandler = "AcceptButtonClicked"; 

        public void MouseClickOnTreeSelectorItem(Object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (!(sender is TreeView)) return;

            TreeView treeViewSender = sender as TreeView;

            if (!(treeViewSender.SelectedItem is SimpleNode)) return;
            this.TargetSource = (treeViewSender.SelectedItem as SimpleNode).Source;
        }

        public void NewButtonClicked(Object sender, RoutedEventArgs e)
        {
            if (this.InstantiatingType == null) return;
            object newAddition = Activator.CreateInstance(this.InstantiatingType);
            this.CallingObject.AddItemToSource(newAddition);
        }

        public void AcceptButtonClicked(Object sender, RoutedEventArgs e)
        {
            this.CallingObject.SaveSourceCollectionToSource();
        }


        public object TargetSource
        {
            get { return (object)GetValue(TargetSourceProperty); }
            set { SetValue(TargetSourceProperty, value); }
        }
        // Using a DependencyProperty as the backing store for TargetSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TargetSourceProperty =
            DependencyProperty.Register(
              "TargetSource",
              typeof(object),
              typeof(InspectionObjectEditorDefaultBehavior)
            );


        public object CollectionSource
        {
            get { return (object)GetValue(CollectionSourceProperty); }
            set { SetValue(CollectionSourceProperty, value); }
        }
        // Using a DependencyProperty as the backing store for CollectionSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CollectionSourceProperty =
            DependencyProperty.Register(
              "CollectionSource",
              typeof(object),
              typeof(InspectionObjectEditorDefaultBehavior)
            );


        public InspectionObjectEditor CallingObject
        {
            get { return (InspectionObjectEditor)GetValue(CallingObjectProperty); }
            set { SetValue(CallingObjectProperty, value); }
        }
        // Using a DependencyProperty as the backing store for CallingObject.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CallingObjectProperty =
            DependencyProperty.Register(
              "CallingObject",
              typeof(InspectionObjectEditor),
              typeof(InspectionObjectEditorDefaultBehavior)
            );



        public Type InstantiatingType
        {
            get { return (Type)GetValue(InstantiatingTypeProperty); }
            set { SetValue(InstantiatingTypeProperty, value); }
        }
        // Using a DependencyProperty as the backing store for InstantiatingType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InstantiatingTypeProperty =
            DependencyProperty.Register(
              "InstantiatingType",
              typeof(Type),
              typeof(InspectionObjectEditorDefaultBehavior)
            );
    }
}
