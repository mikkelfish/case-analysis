﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MesosoftCommon.Utilities.Inspection
{
    /// <summary>
    /// Interaction logic for InspectionDataDockPopup.xaml
    /// </summary>

    public partial class InspectionDataDockPopup : Window
    {
        public object Source
        {
            get { return (object)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register(
                "Source",
                typeof(object),
                typeof(InspectionDataDockPopup),
                new PropertyMetadata(sourcePropertyChanged)
        );
        protected static void sourcePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            InspectionDataDockPopup self = obj as InspectionDataDockPopup;
            if (e.NewValue == null)
                self.TitleString = "Null";
            else
                self.TitleString = e.NewValue.ToString();
        }


        public string TitleString
        {
            get { return (string)GetValue(TitleStringProperty); }
            set { SetValue(TitleStringProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Name.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleStringProperty =
            DependencyProperty.Register(
              "Name",
              typeof(string),
              typeof(InspectionDataDockPopup)
            );


        public InspectionDataDockPopup()
        {
            InitializeComponent();
        }
    }
}
