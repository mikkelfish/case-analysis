﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using MesosoftCommon.Utilities.Reflection;
using System.Reflection;
using System.ComponentModel;
using MesosoftCommon.Utilities.Validations;
using MesosoftCommon.Resources;

namespace MesosoftCommon.Utilities.Inspection
{
    /// <summary>
    /// Interaction logic for InspectionDataDock.xaml
    /// </summary>
    public partial class InspectionDock : UserControl
    {
        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register(
                "Orientation",
                typeof(Orientation),
                typeof(InspectionDock),
                new PropertyMetadata(Orientation.Vertical)
            );
        public Orientation Orientation
        {
            get 
            {
                return (Orientation)GetValue(OrientationProperty); 
            }
            set
            {
                SetValue(OrientationProperty, value);
            }
        }
	
        public object Source
        {
            get { return (object)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register(
                "Source",
                typeof(object),
                typeof(InspectionDock),
                new PropertyMetadata(new PropertyChangedCallback(InspectionDock.sourcePropertyChanged))
        );

        protected static ValidationRule[] getValidations(InspectionDock obj, InspectionEntry entry)
        {
            List<ValidationRule> allValidations = new List<ValidationRule>();
            ValidationRule[] validations = ValidationInfoAttribute.CreateValidations(entry.PropertyInfo, false);
            if (validations != null && validations.Length != 0)
            {
                foreach (ValidationRule rule in validations)
                {
                    ValidationRule realRule = (ValidationRule)FillInManager.GetObject(rule, new object[] { obj }, FillInManager.FillInCopyMode.LetObjectDecide);
                    allValidations.Add(realRule);
                }
            }
            return allValidations.ToArray();
        }

        protected static void sourcePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //Clear entries BEFORE the null check, we always clear the entries regardless of the state of the Source.
            InspectionDock dobj = (obj as InspectionDock);
            foreach (InspectionEntryControl child in dobj.Entries)
                dobj.RemoveLogicalChild(child);            
            dobj.Entries.Clear();
            

            if (e.NewValue == null) return;

            PropertyInfo[] properties = (PropertyInfo[])Common.GetMembersWithAttribute<PropertyInfo, BrowsableAttribute>(e.NewValue.GetType(), true);
            foreach (PropertyInfo info in properties)
            {
                InspectionEntry entry = new InspectionEntry(info, e.NewValue);

                entry.Value = info.GetValue(e.NewValue, null);

                #region Get Stuff From Attributes

                entry.Validations = InspectionDock.getValidations(dobj, entry);

                #endregion

                #region Get Stuff From Dictionaries

                //First, see if there is an entry that targets the type and the name and owner type
                UnifiedResourceDictionary[] entryDictionaries = UnifiedResourceManager.GetDictionaries(typeof(EntryResource));



                //Then see if there is an entry that targets the group

                #endregion



                #region Get Stuff From Defaults

                UnifiedResourceDictionary[] dictionary = UnifiedResourceManager.GetDictionaries("ValueConverters");

                if (dictionary != null)
                {
                    Type type = info.PropertyType;
                    //if (type.IsGenericType)
                    //    type = type.GetGenericArguments()[0];
                    //if (type.IsArray)
                    //    type = type.GetElementType();

                    IValueConverter resource = null;
                    while (resource == null)
                    {
                        // resource = dobj.TryFindResource(type) as IValueConverter;

                        foreach (UnifiedResourceDictionary dict in dictionary)
                        {
                            if (dict.Contains(type))
                            {
                                resource = dict[type].GetValue(new object[]{dobj}) as IValueConverter;
                            }
                        }


                        if (resource == null)
                            type = type.BaseType;
                        if (type == typeof(object))
                            break;
                    }
                    entry.Converters = resource;
                }
                #endregion


                InspectionEntryControl control = new InspectionEntryControl();
                control.Entry = entry;
                dobj.AddLogicalChild(control);
                dobj.Entries.Add(control);
            }
        }

        private ObservableCollection<InspectionEntryControl> entries = new ObservableCollection<InspectionEntryControl>();
        public ObservableCollection<InspectionEntryControl> Entries
        {
            get { return entries; }
        }

        public InspectionDock()
        {
            InitializeComponent();

        }
    }


}
