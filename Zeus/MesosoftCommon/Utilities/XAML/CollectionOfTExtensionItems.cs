using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Markup;
using System.Collections;

namespace MesosoftCommon.Utilities.XAML
{
    public class ListOfTItem : MarkupExtension
    {
        private string fromRes;
        public string ResourceKey
        {
            get { return fromRes; }
            set { fromRes = value; }
        }
	

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return ListOfTExtension.Current.Resources[this.fromRes];
        }
    }

    //public class ObservableOfTItem : MarkupExtension
    //{
    //    private string fromRes;
    //    public string ResourceKey
    //    {
    //        get { return fromRes; }
    //        set { fromRes = value; }
    //    }


    //    public override object ProvideValue(IServiceProvider serviceProvider)
    //    {
    //        return ObservableOfTExtension.Current.Resources[this.fromRes];
    //    }
    //}
}
