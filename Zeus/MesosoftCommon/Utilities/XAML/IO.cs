﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Windows;

namespace MesosoftCommon.Utilities.XAML
{
    public static class XAMLInputOutput
    {
        private static ParserContext createParserContext()
        {
            ParserContext parserContext = new ParserContext();

            Assembly[] allAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<NamespaceMapEntry> namespaces = new List<NamespaceMapEntry>();
            List<string> assemblies = new List<string>();

            foreach (Assembly assm in allAssemblies)
            {
                object[] attrs = assm.GetCustomAttributes(typeof(XmlnsDefinitionAttribute), true);
                foreach (XmlnsDefinitionAttribute attr in attrs)
                {
                    string assemName = attr.AssemblyName;
                    if (assemName == null)
                        assemName = assm.FullName;
                    namespaces.Add(new NamespaceMapEntry(attr.XmlNamespace, assemName, attr.ClrNamespace));
                    assemblies.Add(assemName);
                }
            }


            XamlTypeMapper mapper = new XamlTypeMapper(assemblies.ToArray(), namespaces.ToArray());
            parserContext.XamlTypeMapper = mapper;
            return parserContext;
        }

        public static void SaveFrameworkElement(FrameworkElement element)
        {

        }


        public static string Save(object obj)
        {
            XAMLEncapsulator capsule = new XAMLEncapsulator();
            bool ok = capsule.Encapsulate(obj);            
            return System.Windows.Markup.XamlWriter.Save(capsule);
        }

        public static void Save(object obj, XamlDesignerSerializationManager manager)
        {
            XAMLEncapsulator capsule = new XAMLEncapsulator();
            bool ok = capsule.Encapsulate(obj);
            System.Windows.Markup.XamlWriter.Save(capsule, manager);
        }

        public static void Save(object obj, Stream stream)
        {
            XAMLEncapsulator capsule = new XAMLEncapsulator();
            bool ok = capsule.Encapsulate(obj);
            System.Windows.Markup.XamlWriter.Save(capsule, stream);
            //string test = System.Windows.Markup.XamlWriter.Save(capsule);
        }

        public static void Save(object obj, TextWriter writer)
        {
            XAMLEncapsulator capsule = new XAMLEncapsulator();
            bool ok = capsule.Encapsulate(obj);
            System.Windows.Markup.XamlWriter.Save(capsule, writer);
        }

        public static void Save(object obj, XmlWriter writer)
        {
            XAMLEncapsulator capsule = new XAMLEncapsulator();
            bool ok = capsule.Encapsulate(obj);
            System.Windows.Markup.XamlWriter.Save(capsule, writer);
        }

        public static object Load(string path)
        {
            if (!File.Exists(path)) return null;
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                ParserContext context = createParserContext();
                return Load(stream, context);
            }
        }

        public static object Load(Stream stream)
        {
            ParserContext context = createParserContext();
            object obj = System.Windows.Markup.XamlReader.Load(stream, context);
            if (!(obj is XAMLEncapsulator)) return obj;
            return (obj as XAMLEncapsulator).CreateObject();
        }

        public static object Load(Stream stream, ParserContext context)
        {
            object obj = System.Windows.Markup.XamlReader.Load(stream, context);
            if (!(obj is XAMLEncapsulator)) return obj;
            return (obj as XAMLEncapsulator).CreateObject();
        }

        public static object Load(XmlReader reader)
        {
            object obj = System.Windows.Markup.XamlReader.Load(reader);
            if (!(obj is XAMLEncapsulator)) return obj;
            return (obj as XAMLEncapsulator).CreateObject();
        }

    }
}
