﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace MesosoftCommon.Utilities.XAML
{
    public static class TypeConstants
    {
        public static Type StringArrayType
        {
            get
            {
                return typeof(string[]);
            }
        }

        public static Type ValidationRuleType
        {
            get
            {
                return typeof(ValidationRule);
            }
        }
    }
}
