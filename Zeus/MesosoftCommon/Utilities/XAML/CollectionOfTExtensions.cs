using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Markup;
using System.Collections;
using System.Collections.ObjectModel;

namespace MesosoftCommon.Utilities.XAML
{
    //
    // MarkupExtension that creates a List<T>
    //
    [ContentProperty("Items")]
    public class ListOfTExtension : CollectionOfTExtensionBase<IList>
    {
        public static ListOfTExtension Current
        {
            get
            {
                return current as ListOfTExtension;
            }
        }

        protected override Type GetCollectionType(Type typeArgument)
        {

            return typeof(List<>).MakeGenericType(typeArgument);

        }

    }

    //[ContentProperty("Items")]
    //public class ObservableOfTExtension : CollectionOfTExtensionBase<IList>
    //{
    //    public static ObservableOfTExtension Current
    //    {
    //        get
    //        {
    //            return current as ObservableOfTExtension;
    //        }
    //    }

    //    protected override Type GetCollectionType(Type typeArgument)
    //    {

    //        return typeof(ObservableCollection<>).MakeGenericType(typeArgument);

    //    }

    //}
}
