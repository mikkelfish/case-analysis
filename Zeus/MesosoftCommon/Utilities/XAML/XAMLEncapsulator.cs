﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Windows.Markup;
using System.Collections;
using MesosoftCommon.Interfaces;

namespace MesosoftCommon.Utilities.XAML
{
    public class XAMLEncapsulatorHelper
    {
        private string propertyName;

        public string PropertyName
        {
            get { return propertyName; }
            set { propertyName = value; }
        }

        private object val;

        public object Value
        {
            get { return val; }
            set { val = value; }
        }

        private bool selfReference;

        public bool SelfReference
        {
            get { return selfReference; }
            set { selfReference = value; }
        }
	
    }

    public class XAMLEncapsulatorTypeHelper
    {
        private Type type;
        public Type Type
        {
            get { return type; }
            set { type = value; }
        }

        private List<XAMLEncapsulatorTypeHelper> typeParameters = new List<XAMLEncapsulatorTypeHelper>();
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<XAMLEncapsulatorTypeHelper> TypeParameters
        {
            get { return typeParameters; }
        }

        private string nestedName;
        public string NestedName
        {
            get { return nestedName; }
            set { nestedName = value; }
        }
	

        public void Setup(Type type)
        {
            bool isNest = false;
            if (type.IsNested)
            {
                isNest = true;
                this.type = type.DeclaringType;
                this.nestedName = type.Name;
            }

            if (!type.IsGenericType)
            {
                if(!isNest) this.type = type;
                return;
            }



            if (!isNest) this.type = type.GetGenericTypeDefinition();
            Type[] args = type.GetGenericArguments();
            foreach (Type para in args)
            {
                XAMLEncapsulatorTypeHelper help = new XAMLEncapsulatorTypeHelper();
                help.Setup(para);
                TypeParameters.Add(help);
            }
        }

        public Type CreateType()
        {
            Type toRet = this.type;
            if (nestedName != null)
            {
                toRet = this.type.GetNestedType(nestedName, BindingFlags.Public | BindingFlags.NonPublic);
            }

            if (this.typeParameters.Count == 0) return toRet;

            Type[] paras = new Type[this.typeParameters.Count];
            for (int i = 0; i < paras.Length; i++)
            {
                paras[i] = this.typeParameters[i].CreateType();
            }

            return toRet.MakeGenericType(paras);
        }
	
    }

    public class XAMLEncapsulator
    {
        public bool IsGeneric
        {
            get { return this.type.TypeParameters.Count != 0; }
        }
	
        private List<XAMLEncapsulatorHelper> nonGeneric = new List<XAMLEncapsulatorHelper>();
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<XAMLEncapsulatorHelper> NonGenerics
        {
            get { return nonGeneric; }
        }

        private List<XAMLEncapsulatorHelper> generics = new List<XAMLEncapsulatorHelper>();
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<XAMLEncapsulatorHelper> Generics
        {
            get { return generics; }
        }

        public XAMLEncapsulator()
        {

        }

        private XAMLEncapsulatorTypeHelper type;
        public XAMLEncapsulatorTypeHelper Type
        {
            get { return type; }
            set { this.type = value; }
        }

        private List<object> consArgs = new List<object>();
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<object> ConstructionArgs
        {
            get { return consArgs; }
        }

        private bool needsToBeEncapsulated(object obj)
        {
            if (obj == null) return false;

            bool ok = (!(obj is XAMLEncapsulatorHelper) || !(obj as XAMLEncapsulatorHelper).SelfReference) && obj.GetType().IsGenericType;

            if (obj.GetType().GetInterface("IConstructorProvider") != null)
            {
                ok = true;
            }

            if (ok || !obj.GetType().IsPublic) return true;

            PropertyInfo[] infos = obj.GetType().GetProperties();
            foreach (PropertyInfo info in infos)
            {
                DesignerSerializationVisibilityAttribute[] attr =
                   (DesignerSerializationVisibilityAttribute[])info.GetCustomAttributes(typeof(DesignerSerializationVisibilityAttribute), true);
                
                if (!info.CanWrite && (attr.Length == 0 || attr[0].Visibility == DesignerSerializationVisibility.Hidden)) continue;


                if (info.PropertyType.GetInterface("IDictionary") != null ||
                    info.PropertyType.GetInterface("IList") != null ||
                    info.PropertyType.GetInterface("IConstructorProvider") != null)
                    return true;

                object val = info.GetValue(obj, null);
                if (needsToBeEncapsulated(val)) return true;
            }

            return false;

        }

        private object getObject(object obj)
        {
            bool needs = this.needsToBeEncapsulated(obj);
            if (!needs) return obj;
            XAMLEncapsulator capsule = new XAMLEncapsulator();
            bool ok = capsule.Encapsulate(obj);
            if (!ok) return null;
            return capsule;
        }

        private object revealObject(object obj)
        {
            if (obj is XAMLEncapsulator)
            {
                return (obj as XAMLEncapsulator).CreateObject();
            }
            return obj;
        }

        private void writeDictionary(IDictionary dict, string name)
        {
            List<object> keyObjs = new List<object>();
            foreach (object key in dict.Keys)
            {
                keyObjs.Add(this.getObject(key));
            }

            List<object> itemsObjs = new List<object>();
            foreach (object item in dict.Values)
            {
                itemsObjs.Add(this.getObject(item));
            }

            this.nonGeneric.Add(new XAMLEncapsulatorHelper() { PropertyName = name, Value = new object[] { keyObjs.ToArray(), itemsObjs.ToArray() } });
        }


	
        public bool Encapsulate(object obj)
        {
            if (obj == null) return false;
 
            this.type = new XAMLEncapsulatorTypeHelper();
            this.type.Setup(obj.GetType());
                
            if (obj is IList)
            {
                IList enumerable = (obj as IList);
                List<object> items = new List<object>();
                foreach (object item in enumerable)
                {
                    object real = this.getObject(item);
                    if (real == null) continue;
                    items.Add(real);
                }
                this.nonGeneric.Add(new XAMLEncapsulatorHelper() { PropertyName = "ItemsInObject", Value = items.ToArray() });
            }
            else if (obj is IDictionary)
            {
                this.writeDictionary(obj as IDictionary, "DictionaryItemsInObject");
            }

            if (obj.GetType().IsEnum)
            {
                this.nonGeneric.Add(new XAMLEncapsulatorHelper() { PropertyName = "Name", Value = obj.ToString() });
            }

            PropertyInfo[] infos = obj.GetType().GetProperties();
            foreach (PropertyInfo info in infos)
            {
                DesignerSerializationVisibilityAttribute[] attr = 
                    (DesignerSerializationVisibilityAttribute[])info.GetCustomAttributes(typeof(DesignerSerializationVisibilityAttribute), true);
                if (!info.CanWrite && (attr.Length == 0 || attr[0].Visibility == DesignerSerializationVisibility.Hidden)) continue;

                if (attr.Length != 0 && attr[0].Visibility == DesignerSerializationVisibility.Hidden) continue;

                ParameterInfo[] para = info.GetIndexParameters();
                if (para.Length != 0)
                {
                    continue;
                }
                else
                {
                    object val = info.GetValue(obj, null);

                    if (obj == val)
                    {
                        val = new XAMLEncapsulatorHelper();
                        (val as XAMLEncapsulatorHelper).SelfReference = true;
                    }

                    if (val is IDictionary)
                    {
                        this.writeDictionary(val as IDictionary, info.Name);
                        continue;
                    }

                    bool needs = this.needsToBeEncapsulated(val);
                    if (needs)
                    {
                        XAMLEncapsulator capsule = new XAMLEncapsulator();
                        bool ok = capsule.Encapsulate(val);
                        if (!ok) continue;
                        this.generics.Add(new XAMLEncapsulatorHelper() { PropertyName = info.Name, Value = capsule });
                    }
                    else this.nonGeneric.Add(new XAMLEncapsulatorHelper() { PropertyName = info.Name, Value = val });
                }
            }

            if (obj is IConstructorProvider)
            {
                if ((obj as IConstructorProvider).ConstructionArgs != null)
                {
                    foreach (object arg in (obj as IConstructorProvider).ConstructionArgs)
                    {
                        this.consArgs.Add(this.getObject(arg));
                    }
                }
            }

            return true;
        }

        public object CreateObject()
        {
            Type realType = this.Type.CreateType();
            object obj = null;

            if (realType.IsEnum)
            {
                return Enum.Parse(realType, this.nonGeneric[0].Value as string);
            }

            if (this.consArgs == null || this.consArgs.Count == 0)
                obj = Activator.CreateInstance(realType);
            else
            {
                object[] realArgs = new object[this.consArgs.Count];
                for (int i = 0; i < realArgs.Length; i++)
                {
                    realArgs[i] = this.revealObject(this.consArgs[i]);
                }
                
                obj = Activator.CreateInstance(realType, realArgs);
            }

            foreach (XAMLEncapsulatorHelper helper in this.nonGeneric)
            {
                PropertyInfo info = realType.GetProperty(helper.PropertyName);
                if (helper.PropertyName == "DictionaryItemsInObject" || (info != null && info.PropertyType.GetInterface("IDictionary") != null))
                {
                    object[] keysAndItems = helper.Value as object[];
                    object[] keys = keysAndItems[0] as object[];
                    object[] items = keysAndItems[1] as object[];

                    IDictionary dictionary = null;
                    if (info != null)
                        dictionary = info.GetValue(obj, null) as IDictionary;
                    else dictionary = obj as IDictionary;
                    for (int i = 0; i < keys.Length; i++)
                    {
                        dictionary.Add(this.revealObject(keys[i]), this.revealObject(items[i]));
                    }
                }
                else if (helper.PropertyName == "ItemsInObject")
                {
                    IList enumerable = (obj as IList);
                    if (enumerable == null) continue;
                    object[] items = helper.Value as object[];
                    for (int i = 0; i < items.Length; i++)
                    {
                        if (items[i] is XAMLEncapsulator)
                        {
                            enumerable.Add((items[i] as XAMLEncapsulator).CreateObject());
                        }
                        else enumerable.Add(items[i]);
                    }
                }
                else
                {
                    if (info == null) continue;

                    if (info.CanWrite)
                    {
                        if (helper.Value is XAMLEncapsulatorHelper &&
                            (helper.Value as XAMLEncapsulatorHelper).SelfReference)
                        {
                            info.SetValue(obj, obj, null);
                        }
                        else info.SetValue(obj, helper.Value, null);
                    }
                    else if (helper.Value is IEnumerable && info.PropertyType.GetInterface("IList") != null)
                    {
                        MethodInfo add = info.PropertyType.GetMethod("Add");
                        object listObj = info.GetValue(obj, null);
                        foreach (object item in (helper.Value as IEnumerable))
                        {
                            add.Invoke(listObj, new object[] { this.revealObject(item) });
                        }
                    }
                }
            }

            foreach (XAMLEncapsulatorHelper helper in this.generics)
            {
                PropertyInfo info = realType.GetProperty(helper.PropertyName);
                object genObj = (helper.Value as XAMLEncapsulator).CreateObject();
               if(info.CanWrite) info.SetValue(obj, genObj, null);
                else if (genObj is IEnumerable && info.PropertyType.GetInterface("IList") != null)
                {
                    MethodInfo add = info.PropertyType.GetMethod("Add");
                    object listObj = info.GetValue(obj, null);
                    foreach (object item in (genObj as IEnumerable))
                    {
                        add.Invoke(listObj, new object[] { this.revealObject(item) });
                    }
                }
            }
            return obj;
        }
    }
}
