using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Markup;
using System.Windows;

namespace MesosoftCommon.Utilities.XAML
{
    public class FileLocationMarkup : MarkupExtension
    {
        private string location;
        public string Location
        {
            get { return location; }
            set { location = value; }
        }



        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            string toRet = this.location;
            string appDir = System.Reflection.Assembly.GetEntryAssembly().Location;
            appDir = appDir.Substring(0, appDir.LastIndexOf('\\'));
            toRet = toRet.Replace("$RUNDIR",appDir);
            toRet = toRet.Replace(':', '|');
            toRet = toRet.Replace('\\', '/');
            Uri uri = new Uri("file:///" + toRet);
            return uri;
        }
    }
}
