﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using MesosoftCommon.ExtensionMethods;
using MesosoftCommon.Utilities.Reflection;
using System.Reflection;
using System.Globalization;
using System.Text.RegularExpressions;

namespace MesosoftCommon.Utilities.XAML
{
    public static class XamlFrameworkElementManager
    {
        private class XamlFrameworkEntry
        {
            public int TempID { get; set; }
            public Guid UniversalID { get; set; }
            public Type Type { get; set; }

            public string NativeXAML { get; set; }

            //private Dictionary<int, string> children = new Dictionary<int, string>();
            //public Dictionary<int, string> Children
            //{
            //    get
            //    {
            //        return this.children;
            //    }
            //}

            private List<int> childIds = new List<int>();
            public List<int> ChildIds { get { return this.childIds; } }

            private List<string> childXaml = new List<string>();
            public List<string> ChildXaml { get { return this.childXaml; } }


        }

        private static Regex regex = new Regex("<[^>]*>", RegexOptions.Compiled);


        private static List<XamlFrameworkEntry> entries = new List<XamlFrameworkEntry>();

        private static bool isSub(Match m, ref string property)
        {
            string match = m.Groups[0].Value;
            if (!match.Contains('.')) return false;

            int subIndex = match.IndexOf(' ');
            if (subIndex == -1) return true;

            string sub = match.Substring(0, subIndex);
            
            bool isSub = sub.Contains('.');
            if (isSub)
                property = sub.Substring(sub.IndexOf('.') + 1);
            return isSub;
        }

        private static bool isClosed(Match m)
        {
            string test = m.Value.Replace(" ", "");
            if (test[0] == '<' && test[1] == '/') return true;
            return false;
        }

        private static bool isOnly(Match m)
        {
            string match = m.Value;
            if (match[match.Length - 1] == '/') return true;
            return false;
        }

        public static void RegisterElements(Window window)
        {
            if (entries.Exists(entry => entry.TempID == window.GetHashCode())) return;

            if (!(window.Content is Panel)) return;

            string built = CompiledResourceManagement.GetXamlOrBamlResource(window.GetType().Name, Assembly.GetAssembly(window.GetType()), null, CultureInfo.CurrentCulture);
            if (built == null) return;
            
            XamlFrameworkEntry windowEntry = new XamlFrameworkEntry() { TempID = window.GetHashCode(), Type = window.GetType(), UniversalID = Guid.NewGuid() };

            //The initial match
            Match match = regex.Match(built);
            match = match.NextMatch();

            string throwAway = null;

            //Skip till we get to the panel
            while (isSub(match, ref throwAway) || isClosed(match))
            {
                match = match.NextMatch();
            }

            Stack<string> namesStack = new Stack<string>();

            buildWindow(window.Content, match, windowEntry);
           
            //Panel panel = window.Content as Panel;
            //windowEntry.ChildIds.Add(panel.GetHashCode());
            //windowEntry.ChildXaml.Add(match);

            //match = match.NextMatch();

            //foreach (UIElement ele in panel.Children)
            //{
            //    buildWindow(ele, match, windowEntry);
            //}
        }

        private static void buildWindow(object curElement, Match match, XamlFrameworkEntry entry)
        {
            entry.ChildIds.Add(curElement.GetHashCode());

            string total = match.Value;            
            match = match.NextMatch();

            string prop = null;
            while (isSub(match, ref prop) || isClosed(match))
            {
                total += match.Value;
                match = match.NextMatch();                 
            }

            entry.ChildXaml.Add(total);


            if (curElement is Panel)
            {
                Panel pan = curElement as Panel;
                foreach (UIElement ele in pan.Children)
                {
                    buildWindow(curElement, match, entry);
                }
            }
        }
    }
}
