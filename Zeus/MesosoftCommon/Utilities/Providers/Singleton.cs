using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace MesosoftCommon.Utilities.Providers
{
    public sealed class Singleton<T>
    {
        Singleton() { }

        public static T Instance
        {
            get
            {
                return SingletonCreator.instance;
            }
        }

        class SingletonCreator
        {
            static SingletonCreator() 
            {
                    Type type = typeof(T);
                    ConstructorInfo cons = type.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, Type.EmptyTypes, null);
                    instance = (T)cons.Invoke(null);
            }
            internal static readonly T instance;
        }
    }
}
