//using System;
//using System.Collections.Generic;
//using System.Text;
//using MesosoftCommon.Development;
////using Razor.SnapIns;

//namespace MesosoftCommon.Utilities.Providers
//{
//    public static class Provide
//    {
//        /// <summary>
//        /// Create Ceres Plugins of the given (or subclassed off) the given type
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <returns></returns>
//        [Created("Mikkel", "12/03/06", DocumentedStatus=DocumentedStage.None, Version="0.0")]
//        public static T[] GetPlugins<T> () where T:ISnapIn 
//        {
//            SnapInDescriptor[] descs =
//                            SnapInHostingEngine.Instance.FindDescriptorChainByType(typeof(T));
//            List<T> toRet = new List<T>();
//            foreach (SnapInDescriptor plugin in descs)
//            {
//                toRet.Add((T)plugin.SnapIn);
//            }
//            return toRet.ToArray();
//        }

//        /// <summary>
//        /// Create Ceres Plugins of the given type
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <returns></returns>
//        [Created("Mikkel", "12/03/06", DocumentedStatus = DocumentedStage.None, Version = "0.0")]
//        public static T GetPlugin<T>() where T : ISnapIn
//        {
//            return (T)SnapInHostingEngine.Instance.FindDescriptorByType(typeof(T)).SnapIn;
//        }
//    }
//}
