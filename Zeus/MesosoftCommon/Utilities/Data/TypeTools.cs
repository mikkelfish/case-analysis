﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;

namespace MesosoftCommon.Utilities.Data
{
    public static class TypeTools
    {
        /// <summary>
        /// Convert a source to numeric target type.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="baseTarget">Pass in base type. For instance, if you want to convert to double(s) just pass in double as type, and if the source is an array it will return double[].</param>
        /// <returns></returns>
        public static object ConvertNumbersToType(object source, Type baseTarget)
        {
            if (source == null) return null;

            Type sourceType = source.GetType();

       
            if (sourceType == baseTarget)
                return source;

            if(baseTarget == null) return source;

            string sourceName = sourceType.Name;
            int index = sourceType.Name.IndexOf('[');
            if (index > 0)
            {
                sourceName = sourceName.Substring(0, index);
            }

            string baseName = baseTarget.Name;
            index = baseName.IndexOf('[');
            if (index > 0)
            {
                baseName = baseName.Substring(0, index);
            }

            if (!(sourceName.ToLower() == "int32" || sourceName.ToLower() == "double" || sourceName.ToLower() == "single" || sourceName.ToLower() == "string") ||
                !(baseName.ToLower() == "int32" || baseName.ToLower() == "double" || baseName.ToLower() == "single" || baseName.ToLower() == "string"))
                return source;

            if (source is Array)
            {
                string arraypart = sourceType.Name.Substring(sourceType.Name.IndexOf('['));

                string convertString = baseTarget.FullName.Substring(0, baseTarget.FullName.LastIndexOf('.') +1 ) + baseName;
                Type convertType = Type.GetType(convertString + arraypart);

                int arrayRank = convertType.GetArrayRank();
                object[] toPass = new object[arrayRank];
                for (int i = 0; i < toPass.Length; i++)
                {
                    toPass[i] = (source as Array).GetLength(i);
                }

                IList toRet = convertType.InvokeMember("", System.Reflection.BindingFlags.CreateInstance, null, null, toPass) as IList;

                string convertMethodStr = "ToDouble";
                if (baseName == "Int32")
                    convertMethodStr = "ToInt32";
                else if (baseName == "Single")
                    convertMethodStr = "ToSingle";
                
                MethodInfo convertMethod = typeof(Convert).GetMethod(convertMethodStr, new Type[]{typeof(object)});
                if (toPass.Length == 1)
                {
                    for (int i = 0; i < (int)toPass[0]; i++)
                    {
                            toRet[i] = convertMethod.Invoke(null, new object[] { (source as IList)[i] });
  
                    }
                }
                else if (toPass.Length == 2)
                {
                    for (int i = 0; i < (int)toPass[0]; i++)
                    {
                        for (int j = 0; j < (int)toPass[1]; j++)
                        {
                            
                        }
                    }
                }
                else if (toPass.Length == 3)
                {

                }
                else throw new Exception("This function only supports conversion for arrays up to rank of 3");

                return toRet;
            }


            if (baseTarget == typeof(double))
            {
                return Convert.ToDouble(source);
            }

            if (baseTarget == typeof(float))
            {
                return Convert.ToSingle(source);
            }

            return Convert.ToInt32(source);
        }
    }
}
