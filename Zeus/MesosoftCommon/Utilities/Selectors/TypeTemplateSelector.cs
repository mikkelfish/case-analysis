﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace MesosoftCommon.Utilities.Selectors
{
    public class TypeTemplateSelector : DataTemplateSelector
    {
        public Type Type1 { get; set; }

        private DataTemplate template1;
        public DataTemplate Template1
        {
            get
            {
                return this.template1;
            }
            set
            {
                this.template1 = value;
                if (this.Type1 == null && this.template1.DataType != null)
                {
                    this.Type1 = this.template1.DataType as Type;
                }
            }
        }

        public Type Type2 { get; set; }
        private DataTemplate template2;
        public DataTemplate Template2
        {
            get
            {
                return this.template2;
            }
            set
            {
                this.template2 = value;
                if (this.Type2 == null && this.template2.DataType != null)
                {
                    this.Type2 = this.template2.DataType as Type;
                }
            }
        }
        
        public Type Type3 { get; set; }
        private DataTemplate template3;
        public DataTemplate Template3
        {
            get
            {
                return this.template3;
            }
            set
            {
                this.template3 = value;
                if (this.Type3 == null && this.template3.DataType != null)
                {
                    this.Type3 = this.template3.DataType as Type;
                }
            }
        }
        
        public Type Type4 { get; set; }
        private DataTemplate template4;
        public DataTemplate Template4
        {
            get
            {
                return this.template4;
            }
            set
            {
                this.template4 = value;
                if (this.Type4 == null && this.template4.DataType != null)
                {
                    this.Type4 = this.template4.DataType as Type;
                }
            }
        }

        public Type Type5 { get; set; }
        private DataTemplate template5;
        public DataTemplate Template5
        {
            get
            {
                return this.template5;
            }
            set
            {
                this.template5 = value;
                if (this.Type5 == null && this.template5.DataType != null)
                {
                    this.Type5 = this.template5.DataType as Type;
                }
            }
        }
        
        public Type Type6 { get; set; }
        private DataTemplate template6;
        public DataTemplate Template6
        {
            get
            {
                return this.template6;
            }
            set
            {
                this.template6 = value;
                if (this.Type6 == null && this.template6.DataType != null)
                {
                    this.Type6 = this.template6.DataType as Type;
                }
            }
        }

        public Type Type7 { get; set; }
        private DataTemplate template7;
        public DataTemplate Template7
        {
            get
            {
                return this.template7;
            }
            set
            {
                this.template7 = value;
                if (this.Type7 == null && this.template7.DataType != null)
                {
                    this.Type7 = this.template7.DataType as Type;
                }
            }
        }

        public Type Type8 { get; set; }
        private DataTemplate template8;
        public DataTemplate Template8
        {
            get
            {
                return this.template8;
            }
            set
            {
                this.template8 = value;
                if (this.Type8 == null && this.template8.DataType != null)
                {
                    this.Type8 = this.template8.DataType as Type;
                }
            }
        }

        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            Type type = item.GetType();

            if (type == this.Type1) return this.Template1;
            if (type == this.Type2) return this.Template2;
            if (type == this.Type3) return this.Template3;
            if (type == this.Type4) return this.Template4;
            if (type == this.Type5) return this.Template5;
            if (type == this.Type6) return this.Template6;
            if (type == this.Type7) return this.Template7;
            if (type == this.Type8) return this.Template8;

            return base.SelectTemplate(item, container);


        }
    }
}
