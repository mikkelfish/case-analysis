﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.Utilities.Text
{
    public static class TextUtilities
    {
        private class stringCreator
        {
            public char[] buffer = new char[10000];
            public int bufferspot = 0;
            public string createdString = "";
        }

        private static Dictionary<string, stringCreator> stringCreations = new Dictionary<string, stringCreator>();
        public static string AddStringToCreate(string name)
        {
            if (!stringCreations.ContainsKey(name))
            {
                stringCreations.Add(name, new stringCreator());
                return null;
            }

            stringCreator toRet = stringCreations[name];
            stringCreations[name] = new stringCreator();
            return toRet.createdString + new string(toRet.buffer, 0, toRet.bufferspot);
        }

        public static void ClearString(string name)
        {
            if (!stringCreations.ContainsKey(name))
            {
                stringCreations.Add(name, new stringCreator());
                return;
            }

            stringCreations[name] = new stringCreator();
        }

        public static string GetCreatedString(string name)
        {
            if (!stringCreations.ContainsKey(name))
            {
                return null;
            }

            stringCreator toRet = stringCreations[name];
            string next = new string(toRet.buffer, 0, toRet.bufferspot);
            toRet.bufferspot = 0;
            toRet.createdString += next;

            return toRet.createdString;
        }

        public static void AddToString(string name, string toAdd)
        {
            if (!stringCreations.ContainsKey(name))
            {
                throw new Exception("String " + name + " has not been created");
            }

            stringCreator create = stringCreations[name];
            if (create.bufferspot + toAdd.Length < create.buffer.Length)
            {
                toAdd.CopyTo(0, create.buffer, create.bufferspot, toAdd.Length);
                create.bufferspot += toAdd.Length;
            }
            else
            {
                create.createdString += new string(create.buffer, 0, create.bufferspot);
                create.bufferspot = 0;
                if (toAdd.Length < create.buffer.Length)
                {
                    toAdd.CopyTo(0, create.buffer, create.bufferspot, toAdd.Length);
                    create.bufferspot += toAdd.Length;
                }
                else create.createdString += toAdd;
            }
        }



    }
}
