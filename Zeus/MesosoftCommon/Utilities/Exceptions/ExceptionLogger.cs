﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MesosoftCommon.Utilities.Exceptions
{
    public static class ExceptionLogger
    {
        public static readonly string WriteDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData) + "\\Mesosoft\\Logs\\";
        public static void WriteException(Exception ex, string message)
        {
            if (!Directory.Exists(WriteDirectory))
            {
                Directory.CreateDirectory(WriteDirectory);
            }

            Exception curEx = ex;

            string file = DateTime.Now.ToString().Replace('/', '-').Replace(':', '-') + ".txt";

            using (FileStream stream = new FileStream(WriteDirectory + file, FileMode.CreateNew, FileAccess.Write))
            {
                using (StreamWriter write = new StreamWriter(stream))
                {
                    while (curEx != null)
                    {
                        writeEx(curEx, message, write);
                        write.Write("\n\nNEXT\n");
                        curEx = curEx.InnerException;
                    }
                    

                }
            }
        }

        public static void WriteCustomExceptionMessage(string toWrite)
        {
            if (!Directory.Exists(WriteDirectory))
            {
                Directory.CreateDirectory(WriteDirectory);
            }

            string file = DateTime.Now.ToString().Replace('/', '-').Replace(':', '-') + ".txt";
            

            using (FileStream stream = new FileStream(WriteDirectory + file, FileMode.CreateNew, FileAccess.Write))
            {
                using (StreamWriter write = new StreamWriter(stream))
                {
                    write.Write(toWrite);
                }
            }
        }

        private static void writeEx(Exception ex, string message, StreamWriter write)
        {
            write.WriteLine(message);
            write.WriteLine(ex.Message);
            write.WriteLine(ex.StackTrace);
        }
        
    }
}
