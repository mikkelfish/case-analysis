﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Linq;

namespace MesosoftCommon.Utilities.Reflection
{
    public class RetrieveLoadedTypes
    {
        private static Dictionary<string, Type> typeAssociations = new Dictionary<string, Type>();

        public static Type GetType(string fullname)
        {
            if (fullname == null) 
                return null;

            if (typeAssociations.ContainsKey(fullname)) return typeAssociations[fullname];

           // Type toRet = null;
            foreach (Assembly assm in AppDomain.CurrentDomain.GetAssemblies())
            {
                Type toRet = assm.GetType(fullname);



                if (toRet != null)
                {
                    typeAssociations.Add(fullname, toRet);
                    return toRet;
                }
            }

            return null;
            //RetrieveLoadedTypes retriever = new RetrieveLoadedTypes();
            //Type[] types = retriever.GetTypesPresentInRunningAssembly(true);
            //return types.SingleOrDefault(t => t.FullName == fullname);
        }

        private static Dictionary<string, List<Type>> typesPresentInRunningAssembly = new Dictionary<string,List<Type>>();

        public RetrieveLoadedTypes()
        {
            
        }

        public void PopulateTypesPresentInRunningAssembly()
        {
            Assembly[] referencedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<Type> currentAssemblyTypes = new List<Type>();

            for (int i = 0; i < referencedAssemblies.Length; i++)
            {
                Type[] typeList = referencedAssemblies[i].GetTypes();

                for (int j = 0; j < typeList.Length; j++)
                {
                    currentAssemblyTypes.Add(typeList[j]);
                }
            }
            typesPresentInRunningAssembly.Add(AppDomain.CurrentDomain.FriendlyName, currentAssemblyTypes);
        }

        public Type[] GetTypesPresentInRunningAssembly(bool includeAbstractTypes)
        {
            if(!typesPresentInRunningAssembly.ContainsKey(AppDomain.CurrentDomain.FriendlyName)) this.PopulateTypesPresentInRunningAssembly();
            
            if (!includeAbstractTypes)
            {
                List<Type> ret = new List<Type>();

                foreach (Type currentType in typesPresentInRunningAssembly[AppDomain.CurrentDomain.FriendlyName])
                {
                    if (!currentType.IsAbstract) ret.Add(currentType);
                }
                return ret.ToArray();
            }
            return typesPresentInRunningAssembly[AppDomain.CurrentDomain.FriendlyName].ToArray();
        }

        public Type[] GetTypesPresentInRunningAssembly(Type targetType, bool includeAbstractTypes)
        {
            Type theTargetType = targetType;
            if (targetType.IsGenericType)
                theTargetType = targetType.GetGenericArguments()[0];
            if (targetType.IsArray)
                theTargetType = targetType.GetElementType();

            List<Type> ret = new List<Type>();
            if (!typesPresentInRunningAssembly.ContainsKey(AppDomain.CurrentDomain.FriendlyName)) this.PopulateTypesPresentInRunningAssembly();

            foreach (Type currentType in typesPresentInRunningAssembly[AppDomain.CurrentDomain.FriendlyName])
            {
                if (currentType.IsSubclassOf(theTargetType) && (includeAbstractTypes || !currentType.IsAbstract)) ret.Add(currentType);
            }

            return ret.ToArray();
        }
	
    }
}
