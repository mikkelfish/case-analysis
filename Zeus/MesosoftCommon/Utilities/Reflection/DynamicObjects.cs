﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection.Emit;
using System.Reflection;
using System.Drawing.Design;

namespace MesosoftCommon.Utilities.Reflection
{
    public static class DynamicObjects
    {
        private static ModuleBuilder builder;
        private static int id = 0;
        private static readonly object lockable = new object();
        private static Dictionary<int, Type> constructedTypes;
        private static Dictionary<string, Type> enumTypes = new Dictionary<string, Type>();


        static DynamicObjects()
        {
            AppDomain myDomain = System.Threading.Thread.GetDomain();
            AssemblyName myAsmName = new AssemblyName();
            myAsmName.Name = "DynamicTypeAssembly";

            AssemblyBuilder myAsmBuilder = myDomain.DefineDynamicAssembly(
                           myAsmName,
                           AssemblyBuilderAccess.RunAndSave);

            builder = myAsmBuilder.DefineDynamicModule("DynamicModule", true);

            constructedTypes = new Dictionary<int, Type>();
        }

        private static int getHashKey(Type[] types, string[] names, string[] descriptions)
        {
            string str = "";
            for (int i = 0; i < types.Length; i++)
            {
                str += types[i].Name + names[i];
                if(descriptions != null) 
                    str += descriptions[i];
            }
            return str.GetHashCode();
        }

        public static Type CreateEnum(string[] names)
        {
            int[] toPass = new int[names.Length];
            for (int i = 0; i < toPass.Length; i++)
            {
                toPass[i] = i;
            }
            return CreateEnum<int>(names, toPass);
        }

        public static Type CreateEnum<T>(string[] names, T[] values)
        {
            string name = "DynamicTypes";
            //  int hashCode = getHashKey(types, names, descriptions, possibleAssignTypes);
            string hash = "";
            foreach (string nameStr in names)
            {
                hash += nameStr;
            }


            Type constructedType = null;
            lock (lockable)
            {
                if (enumTypes.ContainsKey(hash))
                {
                    constructedType = enumTypes[hash];
                }

                name = name + id.ToString();
                id++;
            }

            if (constructedType == null)
            {
                EnumBuilder ebuilder = builder.DefineEnum(name, TypeAttributes.Public, typeof(T));
                for (int i = 0; i < names.Length; i++)
                {
                    ebuilder.DefineLiteral(names[i], values[i]);
                }
                constructedType = ebuilder.CreateType();
                lock (lockable)
                {
                    enumTypes.Add(hash, constructedType);
                }
            }
            return constructedType;
        }

        public static object CreateObject(Type[] types, string[] categories, string[] names, string[] descriptions, Type[] editors,
            object[] beginningValues)
        {
            bool[] allFalse = new bool[types.Length];
            return CreateObject(types, categories, names, descriptions, editors, beginningValues, allFalse);
        }

        public static object CreateObject(Type[] types, string[] categories, string[] names, string[] descriptions, Type[] editors,
            object[] beginningValues, bool[] isReadOnly)
        {
            string name = "DynamicTypes";
            int hashCode = getHashKey(types, names, descriptions);
            Type constructedType = null;
            lock (lockable)
            {
                if (constructedTypes.ContainsKey(hashCode))
                {
                    constructedType = constructedTypes[hashCode];
                }

                name = name + id.ToString();
                id++;
            }

            if (constructedType == null)
            {
                #region Create Type

                TypeBuilder ivTypeBld = builder.DefineType(name, TypeAttributes.Public);
                for (int i = 0; i < types.Length; i++)
                {
                    Type requestInfoType = types[i];

                    FieldBuilder field = ivTypeBld.DefineField(names[i] + "field", requestInfoType, FieldAttributes.Public);
                    string propertyName = char.ToUpper(names[i][0]) + names[i].Substring(1);
                    PropertyBuilder prop = ivTypeBld.DefineProperty(propertyName, System.Reflection.PropertyAttributes.None,
                        requestInfoType, null);
                    

                    if (descriptions != null)
                    {
                        if (descriptions[i] != null)
                        {
                            ConstructorInfo cInfo = typeof(System.ComponentModel.DescriptionAttribute).GetConstructor(new Type[] { typeof(string) });
                            CustomAttributeBuilder propDesc = new CustomAttributeBuilder(cInfo, new object[] { descriptions[i] });
                            prop.SetCustomAttribute(propDesc);
                        }
                    }

                    if (categories != null)
                    {
                        if (categories[i] != null)
                        {
                            ConstructorInfo catInfo = typeof(System.ComponentModel.CategoryAttribute).GetConstructor(new Type[] { typeof(string) });
                            CustomAttributeBuilder catDesc = new CustomAttributeBuilder(catInfo, new object[] { categories[i] });
                            prop.SetCustomAttribute(catDesc);
                        }
                    }


                    if (editors != null && editors[i] != null)
                    {
                        ConstructorInfo eInfo = typeof(System.ComponentModel.EditorAttribute).GetConstructor(new Type[] { typeof(Type), typeof(Type) });
                        CustomAttributeBuilder eDesc = new CustomAttributeBuilder(eInfo, new object[] { editors[i], typeof(UITypeEditor) });
                        prop.SetCustomAttribute(eDesc);
                    }
 
 

                    // Define the 'get_Greeting' method.
                    MethodBuilder getMethod = ivTypeBld.DefineMethod("get_" + propertyName,
                       MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName,
                       requestInfoType, null);
                    // Generate IL code for 'get_Greeting' method.
                    ILGenerator methodIL = getMethod.GetILGenerator();
                    methodIL.Emit(OpCodes.Ldarg_0);
                    methodIL.Emit(OpCodes.Ldfld, field);
                    methodIL.Emit(OpCodes.Ret);
                    prop.SetGetMethod(getMethod);

                    if (!isReadOnly[i])
                    {
                        MethodBuilder setMethod = ivTypeBld.DefineMethod("set_" + propertyName,
                           MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName,
                           typeof(void), new Type[] { requestInfoType });

                        // Generate IL code for set_Greeting method.
                        methodIL = setMethod.GetILGenerator();
                        methodIL.Emit(OpCodes.Ldarg_0);
                        methodIL.Emit(OpCodes.Ldarg_1);
                        methodIL.Emit(OpCodes.Stfld, field);
                        methodIL.Emit(OpCodes.Ret);
                        prop.SetSetMethod(setMethod);
                    }
                }
                constructedType = ivTypeBld.CreateType();
                lock (lockable)
                {
                    constructedTypes.Add(hashCode, constructedType);
                }

                #endregion
            }

            object obj = constructedType.InvokeMember("", BindingFlags.CreateInstance, null, null, null);

            if (beginningValues != null)
            {
                for (int i = 0; i < beginningValues.Length; i++)
                {
                    if (isReadOnly[i])
                    {
                        FieldInfo info = obj.GetType().GetField(names[i] + "field");
                        info.SetValue(obj, beginningValues[i]);
                    }
                    else
                    {
                        string propertyName = char.ToUpper(names[i][0]) + names[i].Substring(1);
                        PropertyInfo info = obj.GetType().GetProperty(propertyName);
                        info.SetValue(obj, beginningValues[i], null);
                    }
                }
            }
            return obj;
        }
    }
}
