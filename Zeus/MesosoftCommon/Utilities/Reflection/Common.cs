using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Globalization;
using MesosoftCommon.Attributes;
using MesosoftCommon.Development;
using MesosoftCommon.Interfaces;


namespace MesosoftCommon.Utilities.Reflection
{
    public static class Common
    {
        private static Dictionary<string, ConstructorInfo> genericInfos = new Dictionary<string, ConstructorInfo>();

        /// <summary>
        /// Create an object from a generic type
        /// </summary>
        /// <param name="genericType"></param>
        /// <param name="parameterTypes"></param>
        /// <param name="constructorArgs"></param>
        /// <returns></returns>
        public static object CreateGenericObject(Type genericType, Type[] parameterTypes, params object[] constructorArgs)
        {
            string typeString = genericType.FullName;
            foreach (Type type in parameterTypes)
            {
                typeString += type.FullName;
            }

            if (constructorArgs != null)
            {
                foreach (object o in constructorArgs)
                {
                    typeString += o.GetType().FullName;
                }
            }

            ConstructorInfo info = null;
            if (genericInfos.ContainsKey(typeString))
            {
                info = genericInfos[typeString];
            }

            if (info == null)
            {
                Type constructedType = genericType.MakeGenericType(parameterTypes); //TODO ALL SORTS OF STUFF
                if (constructorArgs != null)
                {
                    Type[] constructorTypes = new Type[constructorArgs.Length];
                    for (int i = 0; i < constructorTypes.Length; i++)
                    {
                        constructorTypes[i] = constructorArgs[i].GetType();
                    }
                    info = constructedType.GetConstructor(constructorTypes); //TODO MORE SORTS OF STUFF
                    //return info.Invoke(constructorArgs);
                }
                else info = constructedType.GetConstructor(Type.EmptyTypes);

                genericInfos.Add(typeString, info);
            }

            // ConstructorInfo info2 = 


            return info.Invoke(constructorArgs);

        }

        /// <summary>
        /// Run a generic function dynamically
        /// </summary>
        /// <param name="function"></param>
        /// <param name="parameterTypes"></param>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static object RunGenericMethod(Delegate function, Type[] parameterTypes, object obj, object[] args)
        {
            MethodInfo info;
            //if (methodInfos.Contains(function.Method))
            //    info = (MethodInfo)methodInfos[function.Method];
            //else
            //{
            info = function.Method.MakeGenericMethod(parameterTypes);
            //    methodInfos.Add(function.Method, info);
            //}

            return info.Invoke(obj, args);
        }

        /// <summary>
        /// Run a generic function dynamically
        /// </summary>
        /// <param name="function"></param>
        /// <param name="parameterTypes"></param>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static object RunGenericMethod(MethodInfo function, Type[] parameterTypes, object obj, object[] args)
        {
            MethodInfo info;
            //if (methodInfos.Contains(function.ToString() + parameterTypes.ToString()))
            //    info = (MethodInfo)methodInfos[function];
            //else
            //{
            info = function.MakeGenericMethod(parameterTypes);
            //    methodInfos.Add(function.ToString() + parameterTypes.ToString(), info);
            //}

            return info.Invoke(obj, args);
        }

        ///// <summary>
        ///// Create Ceres Plugins of the given (or subclassed off) the given type
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <returns></returns>
        //[CeresCreated("Mikkel", "03/14/06")]
        //public static T[] CreatePlugins<T>()
        //{
        //    SnapInDescriptor[] descs =
        //                    SnapInHostingEngine.Instance.FindDescriptorChainByType(typeof(T));
        //    ArrayList toRet = new ArrayList();
        //    foreach (SnapInDescriptor plugin in descs)
        //    {
        //        toRet.Add(plugin.SnapIn);
        //    }
        //    return (T[])toRet.ToArray(typeof(T));
        //}

        /// <summary>
        /// Create interfaces of the given (or subclassed off) the given type from all the loaded plugins
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T[] CreateInterfaces<T>(object[] constructorArgs)
        {
            //SnapInDescriptor[] descs =
            //                SnapInHostingEngine.Instance.FindDescriptorChainByType(typeof(Plugins.CeresPluginBase));

            List<T> toRet = new List<T>();

            Type[] argTypes;

            if (constructorArgs != null && constructorArgs.Length != 0)
            {
                argTypes = new Type[constructorArgs.Length];
                for (int i = 0; i < argTypes.Length; i++)
                {
                    argTypes[i] = constructorArgs[i].GetType();
                }
            }
            else argTypes = Type.EmptyTypes;


            foreach (Assembly assm in AppDomain.CurrentDomain.GetAssemblies())
            {
                Type[] types = assm.GetTypes();
                foreach (Type t in types)
                {
                    Type inter = t.GetInterface(typeof(T).ToString(), true);
                    if (inter == null || t.IsAbstract) continue;
                    ConstructorInfo info = t.GetConstructor(argTypes);
                    if (info == null) continue;
                    T obj = (T)info.Invoke(constructorArgs);
                    if (obj != null) toRet.Add(obj);
                }
            }


            //foreach (SnapInDescriptor plugin in descs)
            //{
            //    Type[] types = plugin.Type.Assembly.GetTypes();
            //    foreach (Type t in types)
            //    {
            //        Type inter = t.GetInterface(typeof(T).ToString(), true);
            //        if (inter == null || t.IsAbstract) continue;
            //        ConstructorInfo info = t.GetConstructor(argTypes);
            //        if (info == null) continue;
            //        T obj = (T)info.Invoke(constructorArgs);
            //        if (obj != null) toRet.Add(obj);
            //    }
            //}

            return toRet.ToArray();
        }

        ///// <summary>
        ///// Get an attribute of the type given from the passed object
        ///// </summary>
        ///// <typeparam name="TAttribute"></typeparam>
        ///// <param name="obj"></param>
        ///// <returns></returns>
        //[CeresCreated("Mikkel", "03/14/06")]
        //public static TAttribute GetAttribute<TAttribute>(object obj) where TAttribute : Attribute
        //{
        //    return Utilities.GetAttribute<TAttribute>(obj, true);
        //}
        /// <summary>
        /// Get an attribute of the type given from the passed object
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="obj"></param>
        /// <param name="inherit"></param>
        /// <returns></returns>
        //[CeresCreated("Mikkel", "03/14/06")]
        //public static TAttribute GetAttribute<TAttribute>(object obj, bool inherit) where TAttribute : Attribute
        //{
        //    return Utilities.GetAttribute<TAttribute>(obj.GetType(), inherit);
        //}

        public static TAttribute GetAttribute<TAttribute>(Type type, bool inherit) where TAttribute : Attribute
        {
            object[] attrs = type.GetCustomAttributes(typeof(TAttribute), inherit);
            if (attrs == null || attrs.Length == 0) return null;
            return (TAttribute)attrs[0];
        }

        /// <summary>
        /// Given an type that is an array, return the unarrayed type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Type UnarrayType(Type type)
        {
            string name = type.FullName;
            int index = name.IndexOf('[');
            if (index < 0) return type;
            name = name.Substring(0, index);
            return Type.GetType(name);
        }

        /// <summary>
        /// Get all attributes of the given type from the object
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static TAttribute[] GetAttributes<TAttribute>(object obj) where TAttribute : Attribute
        {
            return GetAttributes<TAttribute>(obj, true);
        }
        /// <summary>
        /// Get all attributes of the given type from the object
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="obj"></param>
        /// <param name="inherit"></param>
        /// <returns></returns>
        public static TAttribute[] GetAttributes<TAttribute>(object obj, bool inherit) where TAttribute : Attribute
        {
            object[] attrs = obj.GetType().GetCustomAttributes(typeof(TAttribute), inherit);
            if (attrs == null || attrs.Length == 0) return null;
            return (TAttribute[])attrs;
        }



        /// <summary>
        /// Return all the fields that have the given attribute in the object.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="obj"></param>
        /// <param name="fields"></param>
        /// <param name="attributes"></param>
        public static FieldInfo[] GetFieldsWithAttribute<TAttribute>(object obj, out TAttribute[] attributes) where TAttribute : Attribute
        {
            FieldInfo[] members = obj.GetType().GetFields(BindingFlags.Instance | BindingFlags.Static |
                 BindingFlags.Public | BindingFlags.NonPublic);
            List<FieldInfo> list = new List<FieldInfo>();
            List<TAttribute> attrList = new List<TAttribute>();
            foreach (FieldInfo info in members)
            {
                object[] attrs = info.GetCustomAttributes(typeof(TAttribute), true);
                if (attrs == null || attrs.Length == 0) continue;
                list.Add(info);
                attrList.Add((TAttribute)attrs[0]);
            }
            attributes = attrList.ToArray();
            return list.ToArray();
        }

        public static FieldInfo[] GetFieldsWithAttribute<TAttribute>(object obj) where TAttribute : Attribute
        {
            TAttribute[] th;
            return GetFieldsWithAttribute<TAttribute>(obj, out th);
        }

        /// <summary>
        /// Get all the functions that have the given attribute from the given type
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="type"></param>
        /// <returns></returns>
        public static MethodInfo[] GetFunctionsWithAttribute<TAttribute>(Type type) where TAttribute : Attribute
        {
            TAttribute[] throwAway;
            return GetFunctionsWithAttribute<TAttribute>(type, out throwAway);
        }

        /// <summary>
        /// Get all the functions that have the given attribute from the given type
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="type"></param>
        /// <param name="attrsList"></param>
        /// <returns></returns>
        public static MethodInfo[] GetFunctionsWithAttribute<TAttribute>(Type type, out TAttribute[] attrsList) where TAttribute : Attribute
        {
            MethodInfo[] methods = type.GetMethods(BindingFlags.Instance |
                BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            List<MethodInfo> list = new List<MethodInfo>();
            List<TAttribute> attr = new List<TAttribute>();
            foreach (MethodInfo info in methods)
            {
                object[] attrs = info.GetCustomAttributes(typeof(TAttribute), true);
                if (attrs == null || attrs.Length == 0) continue;
                foreach (TAttribute at in attrs) attr.Add(at);
                list.Add(info);
            }

            attrsList = attr.ToArray();
            return list.ToArray();
        }


        public static object CreateBestGuessCopy(object obj, out bool copySuccessful)
        {
            object toRet = obj;
            if (obj is ICloneable)
            {
                toRet = (obj as ICloneable).Clone();
            }
            else
            {
                object copy = null;

                SafeForBlindCopyAttribute attr = Common.GetAttribute<SafeForBlindCopyAttribute>(obj.GetType(), false);
                if (attr == null)
                {
                    copySuccessful = false;
                    return obj;
                }

                if (obj is IConstructorProvider)
                {
                    IConstructorProvider provider = obj as IConstructorProvider;
                    Type[] consTypes = Type.GetTypeArray(provider.ConstructionArgs);
                    ConstructorInfo cons = provider.GetType().GetConstructor(consTypes);
                    if (cons == null)
                        throw new Exception("Could not copy object.");
                    copy = cons.Invoke(provider.ConstructionArgs);
                }
                else
                {
                    ConstructorInfo cons = obj.GetType().GetConstructor(Type.EmptyTypes);
                    if (cons == null)
                        throw new Exception("Could not copy object.");
                    copy = cons.Invoke(null);
                }

                PropertyInfo[] props = copy.GetType().GetProperties();
                foreach (PropertyInfo prop in props)
                {
                    if (!prop.CanRead || !prop.CanWrite) continue;
                    ParameterInfo[] paras = prop.GetIndexParameters();
                    if (paras.Length == 0) continue;

                    prop.SetValue(copy, prop.GetValue(obj, null), null);
                }

                toRet = copy;
            }

            copySuccessful = true;
            return toRet;
        }

        /// <summary>
        /// This is used to set a property's value using reflection. If the property can be written, it sets it directly; if not, it looks to see if it has FieldLinkedAttribute and sets the underlying field.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="target"></param>
        /// <param name="value"></param>
        /// <param name="binder"></param>
        /// <param name="index"></param>
        /// <param name="culture"></param>
        public static void SetPropertyOrUnderlying(PropertyInfo info, object target, object value, Binder binder, object[] index, CultureInfo culture)
        {
            if (culture == null)
                culture = CultureInfo.CurrentCulture;

            if (info.CanWrite)
            {
                info.SetValue(target, value, BindingFlags.SetProperty, binder, null, culture);
            }
            else
            {
                object[] fields = info.GetCustomAttributes(typeof(FieldLinkedAttribute), false);
                if (fields != null && fields.Length > 0)
                {
                    FieldLinkedAttribute fieldAttr = fields[0] as FieldLinkedAttribute;
                    FieldInfo field = info.DeclaringType.GetField(fieldAttr.FieldName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
                    if (field != null)
                    {
                        field.SetValue(target, value, BindingFlags.SetField, binder, culture);
                    }
                }
            }
        }


        public static object GetPropertyOrUnderlying(PropertyInfo info, object target, object[] index)
        {
            if (info.CanWrite)
            {
               return info.GetValue(target, index);
            }
            else
            {
                object[] fields = info.GetCustomAttributes(typeof(FieldLinkedAttribute), false);
                if (fields != null && fields.Length > 0)
                {
                    FieldLinkedAttribute fieldAttr = fields[0] as FieldLinkedAttribute;
                    FieldInfo field = info.DeclaringType.GetField(fieldAttr.FieldName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
                    if (field != null)
                    {
                       return field.GetValue(target);
                    }
                }
            }
            return null;
        }

        public static object RunGenericMethod(string functionName, Type[] parameterTypes, object obj, object[] args)
        {
            MethodInfo info = obj.GetType().GetMethod(functionName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            if (info == null) return null;
            return RunGenericMethod(info, parameterTypes, obj, args);
        }

        //public static object RunGenericMethod(MethodInfo function, Type[] parameterTypes, object obj, object[] args)
        //{
        //    MethodInfo info;
        //    //if (methodInfos.Contains(function.ToString() + parameterTypes.ToString()))
        //    //    info = (MethodInfo)methodInfos[function];
        //    //else
        //    //{
        //    info = function.MakeGenericMethod(parameterTypes);
        //    //    methodInfos.Add(function.ToString() + parameterTypes.ToString(), info);
        //    //}

        //    return info.Invoke(obj, args);
        //}

        public static Type[] GetTypesFromAssembly(Assembly assembly, Type attributeType, Type searchType, bool includeAbstract)
        {
            Type[] types = assembly.GetTypes();
            List<Type> toRet = new List<Type>();
            foreach (Type type in types)
            {
                if(searchType != null)
                    if (!type.IsSubclassOf(searchType) && type != searchType) continue;

                if (attributeType != null)
                {
                    object[] attrs = type.GetCustomAttributes(attributeType, true);
                    if (attrs == null || attrs.Length == 0) continue;
                }

                if (includeAbstract || !type.IsAbstract)
                    toRet.Add(type);
            }
            return toRet.ToArray();
        }

        public static Type[] GetTypesFromAssembly(Assembly assembly, Type searchType, bool includeAbstract)
        {
            return GetTypesFromAssembly(assembly, null, searchType, includeAbstract);
        }

        //public static TAttribute GetAttribute<TAttribute>(Type type, bool inherit) where TAttribute : Attribute
        //{
        //    object[] attrs = type.GetCustomAttributes(typeof(TAttribute), inherit);
        //    if (attrs == null || attrs.Length == 0) return null;
        //    return (TAttribute)attrs[0];
        //}

        public static TMember[] GetMembersWithAttribute<TMember, TAttribute>(Type type, bool inherit, BindingFlags flags, out TAttribute[][] attrs)
            where TMember : MemberInfo
            where TAttribute : Attribute
        {
            MemberInfo[] members = type.GetMembers(flags);
            List<TMember> methods = new List<TMember>();
            List<TAttribute[]> attrsL = new List<TAttribute[]>();
            foreach (MemberInfo info in members)
            {
                Type typeCheck = info.GetType();
                if (typeCheck.Name.ToLower().Contains("runtime"))
                    typeCheck = typeCheck.BaseType;
                if (typeCheck != typeof(TMember)) continue;
                object[] objs = info.GetCustomAttributes(typeof(TAttribute), true);
                if (objs == null || objs.Length == 0) continue;
                methods.Add((TMember)info);
                TAttribute[] attrA = new TAttribute[objs.Length];
                for (int i = 0; i < attrA.Length; i++)
                {
                    attrA[i] = (TAttribute)objs[i];
                }
                attrsL.Add(attrA);
            }
            attrs = attrsL.ToArray();
            return methods.ToArray();
        }

        public static TMember[] GetMembersWithAttribute<TMember, TAttribute>(Type type, bool inherit, BindingFlags flags)
            where TMember : MemberInfo
            where TAttribute : Attribute
        {
            TAttribute[][] attrs;
            return Common.GetMembersWithAttribute<TMember, TAttribute>(type, inherit, flags, out attrs);
        }

        public static TMember[] GetMembersWithAttribute<TMember, TAttribute>(Type type, bool inherit)
            where TMember : MemberInfo
            where TAttribute : Attribute
        {
            TAttribute[][] attrs;
            return Common.GetMembersWithAttribute<TMember, TAttribute>(type, inherit, 
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static, out attrs);
        }

        /// <summary>
        /// Find the type, pulling it out from arrays or generics as necessary.
        /// </summary>
        /// <param name="target">Object to find the type on.</param>
        /// <returns>The "absolute" type of the target.</returns>
        public static Type GetAbsoluteType(object target)
        {
            Type type = target.GetType();
            if (type.IsGenericType)
                type = type.GetGenericArguments()[0];
            if (type.IsArray)
                type = type.GetElementType();
            return type;
        }
    }
}
