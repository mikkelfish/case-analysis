﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Reflection;
using System.Globalization;
using System.Collections;
using System.IO;
using Reflector.BamlViewer;

namespace MesosoftCommon.Utilities.Reflection
{
    public static class CompiledResourceManagement
    {
        public static ResourceSet GetResourceSet(Assembly assm, string resourceSetName, CultureInfo culture)
        {
            if (resourceSetName == null)
            {
                resourceSetName = assm.FullName.Substring(0, assm.FullName.IndexOf(',')) + ".g";
            }

            try
            {
                string[] s = assm.GetManifestResourceNames();
                ResourceManager manager = new ResourceManager(resourceSetName, assm);
                ResourceSet set = manager.GetResourceSet(culture, true, true);
                return set;
            }
            catch
            {
                return null;
            }

        }

        public static object GetResourceStream(ref string resourceName, Assembly assm, string resourceSetName, CultureInfo culture)
        {
            ResourceSet set = GetResourceSet(assm, resourceSetName, culture);
            if (set == null) return null;
            if (resourceName[0] == '$') return set.GetObject(resourceName);
            else
            {
                string tempName = resourceName.ToLower();
                var found = from DictionaryEntry possibleResource in set
                        where possibleResource.Key is string
                         && (possibleResource.Key as string).Contains(tempName)
                        select new{possibleResource.Key, possibleResource.Value};

                foreach (var f in found)
                {
                    resourceName = f.Key as string;
                    return f.Value;
                }

                return null;
            }
        }

        public static object[] GetResourceStreams(string filter, Assembly assm, string resourceSetName, CultureInfo culture)
        {
            ResourceSet set = GetResourceSet(assm, resourceSetName, culture);
            if (set == null) return null;
            var foundResources = from DictionaryEntry possibleResource in set
                                 where possibleResource.Key is string &&
                                    (possibleResource.Key as string).Contains(filter)
                                 select possibleResource.Value;
            return foundResources.ToArray<object>();
        }

        private static string getBamlToXml(Stream stream)
        {
            BamlTranslator trans = new BamlTranslator(stream);
            return trans.ToString();
        }

        public static string GetXamlOrBamlResource(string resourceName, Assembly assm, string resourceSetName, CultureInfo culture)
        {
            string toRet = null;
            using (Stream stream = GetResourceStream(ref resourceName, assm, resourceSetName, culture) as Stream)
            {
                if (stream == null) return null;
                if (resourceName.Contains(".xaml"))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        toRet = reader.ReadToEnd();
                    }
                }
                else if(resourceName.Contains(".baml"))
                {
                    toRet = getBamlToXml(stream);
                }
            }
            return toRet;
        }

        public static string[] GetXamlOrBamlResources(string filter, Assembly assm, string resourceSetName, CultureInfo culture)
        {
            ResourceSet set = GetResourceSet(assm, resourceSetName, culture);
            if (set == null) return null;

            var foundXamlResources = from DictionaryEntry possibleResource in set
                                 where possibleResource.Key is string &&
                                    possibleResource.Value is Stream &&
                                    (possibleResource.Key as string).Contains(filter) &&
                                    (possibleResource.Key as string).Contains(".xaml")
                                 select possibleResource.Value;

            var foundBamlResources = from DictionaryEntry possibleResource in set
                                     where possibleResource.Key is string &&
                                        possibleResource.Value is Stream &&
                                        (possibleResource.Key as string).Contains(filter) &&
                                        (possibleResource.Key as string).Contains(".baml")
                                     select possibleResource.Value;

            List<string> loaded = new List<string>();
            foreach (Stream stream in foundXamlResources)
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    loaded.Add(reader.ReadToEnd());
                }
                stream.Dispose();
            }

            foreach (Stream stream in foundBamlResources)
            {
                loaded.Add(getBamlToXml(stream));
                stream.Dispose();
            }

            return loaded.ToArray();
        }
    }
}
