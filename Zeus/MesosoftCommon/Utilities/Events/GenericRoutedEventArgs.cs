﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MesosoftCommon.Utilities.Events
{
    public delegate void RoutedEventHandler<T>(object obj, T args) where T:RoutedEventArgs;
}
