﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Reflection;
using System.Windows.Markup;
using MesosoftCommon.ExtensionMethods;
using System.Windows.Controls;
using System.Windows.Data;
using System.Collections;
using System.Drawing;

namespace MesosoftCommon.Utilities.Events
{
    public class DynamicEventSetter : FrameworkElement, IDisposable
    {
        public class SetterEntry
        {
            private object obj;
            public object Object
            {
                get { return obj; }
                set { obj = value; }
            }

            private FrameworkElement parent;
            public FrameworkElement Parent
            {
                get { return parent; }
                set { parent = value; }
            }
	

            private string eventName;
            public string EventName
            {
                get { return eventName; }
                set { eventName = value; }
            }	
        }

        private static Dictionary<DependencyObject, List<DynamicEventSetter>> setters =
            new Dictionary<DependencyObject, List<DynamicEventSetter>>();

        private static Dictionary<string, List<SetterEntry>> objectsToSet = new Dictionary<string, List<SetterEntry>>();
        private static Dictionary<object, Dictionary<DynamicEventSetter, List<Delegate>>> attachedHandlers =
            new Dictionary<object, Dictionary<DynamicEventSetter, List<Delegate>>>();

        public static Dictionary<DynamicEventSetter, List<Delegate>> GetDynamicSettersOnObject(object obj)
        {
            if (!attachedHandlers.ContainsKey(obj)) return null;
            return attachedHandlers[obj];
        }
	

        public static string GetDynamicEventName(DependencyObject obj)
        {
            return (string)obj.GetValue(DynamicEventNameProperty);
        }

        public static void SetDynamicEventName(DependencyObject obj, string value)
        {
            obj.SetValue(DynamicEventNameProperty, value);
        }

        // Using a DependencyProperty as the backing store for DynamicEventName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DynamicEventNameProperty =
            DependencyProperty.RegisterAttached("DynamicEventName", typeof(string), typeof(DynamicEventSetter), 
            new UIPropertyMetadata(new PropertyChangedCallback(onDynamicEventNameChanged)));

        private static void onDynamicEventNameChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            SetterEntry entry = new SetterEntry() { Object = obj, Parent = (FrameworkElement)(obj as FrameworkElement).Parent, EventName = e.NewValue as string };
            associateObject(entry);
        }


        public static List<SetterEntry> GetStyles(DependencyObject obj)
        {
            return (List<SetterEntry>)obj.GetValue(StylesProperty);
        }

        public static void SetStyles(DependencyObject obj, List<SetterEntry> value)
        {
            obj.SetValue(StylesProperty, value);
        }

        // Using a DependencyProperty as the backing store for Styles.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StylesProperty =
            DependencyProperty.RegisterAttached("Styles", typeof(List<SetterEntry>), typeof(DynamicEventSetter),
            new UIPropertyMetadata(new PropertyChangedCallback(stylesChanged)));


        private static void stylesChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            List<SetterEntry> vals = e.NewValue as List<SetterEntry>;
            if (vals == null) return;
            foreach (SetterEntry entry in vals)
            {
                associateObject(entry);
            }
        }

        private static RoutedEvent getRoutedEvent(string name, Type target)
        {
            RoutedEvent[] events = EventManager.GetRoutedEvents();
            //if (this.EventOwner != null)
            //    events = EventManager.GetRoutedEventsForOwner(this.EventOwner);

            if (events == null) return null;
            foreach (RoutedEvent ev in events)
            {
                if (ev.Name == name)
                {
                    return ev;
                }
            }

            if (target != null)
            {
                events = EventManager.GetRoutedEventsForOwner(target);

                if (events != null)
                {
                    foreach (RoutedEvent ev in events)
                    {
                        if (ev.Name == name)
                        {
                            return ev;
                        }
                    }
                }
                else
                {
                    FieldInfo info = target.GetField(name + "Event", BindingFlags.Public | BindingFlags.Static);
                    if (info != null) return (RoutedEvent)info.GetValue(null);
                }
            }

            return null;
        }

        private Delegate getDelegate(Type handleType)
        {
            if (this.Source is Type)
            {
                MethodInfo info = (this.Source as Type).GetMethod(this.MethodName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
                return Delegate.CreateDelegate(handleType, info);
            }
             
            MethodInfo info2 = this.Source.GetType().GetMethod(this.MethodName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            return Delegate.CreateDelegate(handleType, this.Source, info2);
        }

        private static void associateObject(SetterEntry pentry)
        {
            object obj = pentry.Object;
            FrameworkElement parent = pentry.Parent;

            if (parent == null)
            {
                if (obj is Control)
                    parent = obj as FrameworkElement;
                else return;
            }
            else if (parent is FrameworkElement)
            {
                parent = parent.FindTopAncestor<Control>();
            }

            parent.Unloaded += new RoutedEventHandler(Parent_Unloaded);


            string val = pentry.EventName;

            if (val == null) return;
            string[] parse = val.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string entryStr in parse)
            {
                string[] ev = entryStr.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                string setterName = ev[0].Trim();
                string eventName = ev[1].Trim();

                SetterEntry entry = new SetterEntry() { EventName = eventName, Object = pentry.Object, Parent = pentry.Parent };

                DynamicEventSetter setter = null;

                if (setters.ContainsKey(parent))
                {
                    setter = setters[parent].Find(
                        delegate(DynamicEventSetter set)
                        {
                            return set.DynamicName == setterName;
                        }
                    );
                }

              //  SetterEntry entry = new SetterEntry() { Object = obj, EventName = eventName, Parent=parent };
                if (setter == null)
                {
                    if (!objectsToSet.ContainsKey(setterName))
                    {
                        objectsToSet.Add(setterName, new List<SetterEntry>());
                    }
                    objectsToSet[setterName].Add(entry);
                }
                else
                {
                    DynamicEventSetter.setEvent(setter, entry);
                }
            }
        }

        private static void setEvent(DynamicEventSetter setter, SetterEntry entry)
        {
            if (entry.Object is Style)
            {
                Style style = entry.Object as Style;
                if (style == null) return;

                RoutedEvent foundEvent = getRoutedEvent(entry.EventName, style.TargetType);


                EventSetter evsetter = new EventSetter();
                evsetter.Event = foundEvent;
                evsetter.Handler = setter.getDelegate(foundEvent.HandlerType);

                if (style.IsSealed)
                {
                    Style newStyle = new Style(style.TargetType, style);
                    newStyle.Setters.Add(evsetter);
                    (entry.Parent as FrameworkElement).Resources.Add(newStyle, style.TargetType);
                }
                else style.Setters.Add(evsetter);
            }
            else
            {
                EventInfo ev = entry.Object.GetType().GetEvent(entry.EventName);
                Delegate handler = setter.getDelegate(ev.EventHandlerType);
                ev.AddEventHandler(entry.Object, handler);

                if (!attachedHandlers.ContainsKey(entry.Object))
                {
                    attachedHandlers.Add(entry.Object, new Dictionary<DynamicEventSetter, List<Delegate>>());
                }

                if (!attachedHandlers[entry.Object].ContainsKey(setter))
                {
                    attachedHandlers[entry.Object].Add(setter, new List<Delegate>());
                }

                attachedHandlers[entry.Object][setter].Add(handler);
            }
        }

        static void Parent_Unloaded(object sender, RoutedEventArgs e)
        {
            if (setters.ContainsKey(sender as DependencyObject))
            {
                List<DynamicEventSetter> setterList = setters[sender as DependencyObject];

                DynamicEventSetter[] toRemove = new DynamicEventSetter[setterList.Count];
                setterList.CopyTo(toRemove);
                foreach (DynamicEventSetter setter in toRemove)
                {
                    setter.Dispose();
                }
                setters.Remove(sender as DependencyObject);
            }
        }

        private string dynamicEventName;
        public string DynamicName
        {
            get { return dynamicEventName; }
            set
            {                 
                dynamicEventName = value;
                this.somethingChanged();
            }
        }
	
        public object Source
        {
            get { return (object)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(object), typeof(DynamicEventSetter), 
            new PropertyMetadata(dependencyChanged));

        private static void dependencyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
           (obj as DynamicEventSetter).somethingChanged();
        }


        public string MethodName
        {
            get { return (string)GetValue(MethodNameProperty); }
            set { SetValue(MethodNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MethodName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MethodNameProperty =
            DependencyProperty.Register("MethodName", typeof(string), typeof(DynamicEventSetter),
            new PropertyMetadata(dependencyChanged));

        private void somethingChanged()
        {
            if (this.MethodName == null || this.Source == null || this.DynamicName == null) return;

            DependencyObject parent = this.FindTopAncestor<Control>();
            if (!setters.ContainsKey(parent))
                setters.Add(parent, new List<DynamicEventSetter>());
            setters[parent].Add(this);
           
            if (objectsToSet.ContainsKey(dynamicEventName))
            {
                List<SetterEntry> list = objectsToSet[dynamicEventName];
                foreach (SetterEntry entry in list)
                {
                    DynamicEventSetter.setEvent(this, entry);
                }

                objectsToSet.Remove(dynamicEventName);
            }
        }

        public DynamicEventSetter()
        {

        }

        #region IDisposable Members

        public void Dispose()
        {
            DependencyObject parent = this.FindTopAncestor<Control>();         
            if (parent == null) return;
            if (setters.ContainsKey(parent))
            {
                setters[parent].Remove(this);
            }
        }

        #endregion
    }
}
