﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;
using System.Windows;

namespace MesosoftCommon.Utilities.Events
{
    [MarkupExtensionReturnType(typeof(List<DynamicEventSetter.SetterEntry>))]
    public class DynamicEventStyleExtension : MarkupExtension
    {
        private string val;
        public string Value
        {
            get { return val; }
            set { value = val; }
        }
	

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            IXamlTypeResolver resolver = (IXamlTypeResolver)serviceProvider.GetService(typeof(IXamlTypeResolver));
            IProvideValueTarget target = (IProvideValueTarget)serviceProvider.GetService(typeof(IProvideValueTarget));
            if (resolver == null || val == null) return new List<DynamicEventSetter.SetterEntry>();
            
            FrameworkElement element = target.TargetObject as FrameworkElement;
            List<DynamicEventSetter.SetterEntry> toRet = new List<DynamicEventSetter.SetterEntry>();

            string[] parse = val.Split(new string[] { ")" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string p in parse)
            {
                int index = p.IndexOf('(');
                string styleStr = p.Substring(0, index);

                Style style = null;
                if (styleStr.Contains("Type:"))
                {
                    string stringType = styleStr.Substring("Type:".Length);
                    Type type = resolver.Resolve(stringType);
                    style = (Style)element.TryFindResource(type);
                }
                else
                {
                    style = (Style)element.TryFindResource(styleStr);
                }

                DynamicEventSetter.SetterEntry entry = new DynamicEventSetter.SetterEntry()
                                                       {
                                                           Object = style,
                                                           Parent = (FrameworkElement)target.TargetObject,
                                                           EventName = p.Substring(index + 1)
                                                       };

                if (style != null)
                    toRet.Add(entry);
            }

            return toRet;
        }

        public DynamicEventStyleExtension()
        {

        }

        public DynamicEventStyleExtension(string val)
        {
            this.val = val;
        }
    }
}
