﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.ComponentModel;

namespace MesosoftCommon.Utilities.Validations
{
    public class NotNullValidator : ValidationRule
    {
        public override string ToString()
        {
            return "Not Null Validator";
        }

        [Browsable(true)]
        private string extraInfo;
        public string ExtraInfo
        {
            get { return extraInfo; }
            set { extraInfo = value; }
        }
	


        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value == null) return new ValidationResult(false, "Value cannot be null. " + this.extraInfo);
            return new ValidationResult(true, null);
        }
    }
}
