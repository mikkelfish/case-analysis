﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Reflection;

namespace MesosoftCommon.Utilities.Validations
{
    public class ComparisonValidator : ValidationRule 
    {
        public enum Comparison { Equal, NotEqual, GreaterThan, LessThan, GreaterThanEqual, LessThanEqual };

        public Comparison Operator { get; set; }
        public object CompareTo { get; set; }
        public string Path { get; set; }

        public bool TreatNonComparableAsSuccess { get; set; }

        public string Message { get; set; }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value == null) return new ValidationResult(false, "Value is null. " + this.Message);
            if (!(value is IComparable))
            {
                if (this.TreatNonComparableAsSuccess)
                    return new ValidationResult(true, null);
                return new ValidationResult(false, "Value is not Comparable. " + this.Message);
            }

            object toCompare = this.CompareTo;
            if (this.Path != null && this.Path != "")
            {
                string[] splits = this.Path.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < splits.Length; i++)
                {
                    PropertyInfo info = toCompare.GetType().GetProperty(splits[i]);
                    if (info == null)
                    {
                        return new ValidationResult(false, "Path not valid. " + this.Message);
                    }
                    toCompare = info.GetValue(toCompare, null);
                }
            }


            IComparable comparable = toCompare as IComparable;
            if (comparable == null)
            {
                if (this.TreatNonComparableAsSuccess)
                    return new ValidationResult(true, null);
                return new ValidationResult(false, "Value is not Comparable. " + this.Message);
            }

            int compare = (value as IComparable).CompareTo(comparable);

            if (this.Operator == Comparison.Equal)
            {
                if (compare == 0)
                {
                    return new ValidationResult(true, null);
                }

                return new ValidationResult(false, "Value not equal to " + comparable.ToString() + ". " + this.Message);
            }
            else if (this.Operator == Comparison.GreaterThan)
            {
                if (compare > 0)
                {
                    return new ValidationResult(true, null);
                }
                return new ValidationResult(false, "Value not greater than " + comparable.ToString() + ". " + this.Message);
            }
            else if (this.Operator == Comparison.GreaterThanEqual)
            {
                if (compare >= 0)
                {
                    return new ValidationResult(true, null);
                }
                return new ValidationResult(false, "Value not greater than or equal to " + comparable.ToString() + ". " + this.Message);
            }
            else if (this.Operator == Comparison.LessThan)
            {
                if (compare < 0)
                {
                    return new ValidationResult(true, null);
                }

                return new ValidationResult(false, "Value not less than " + comparable.ToString() + ". " + this.Message);
            }
            else if (this.Operator == Comparison.LessThanEqual)
            {
                if (compare <= 0)
                {
                    return new ValidationResult(true, null);
                }
                return new ValidationResult(false, "Value not less than or equal to " + comparable.ToString() + ". " + this.Message);
            }

            if (compare != 0)
            {
                return new ValidationResult(true, null);
            }

            return new ValidationResult(false, "Value is not unequal to " + comparable.ToString() + ". " + this.Message);

        }
    }
}
