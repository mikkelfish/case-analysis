﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace MesosoftCommon.Utilities.Validations
{
    public class MultiValidation : ValidationRule 
    {
        public ValidationRule[] Rules { get; set; }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            bool isValid = true;
            string errors = "";

            if (this.Rules == null)
                return new ValidationResult(true, null);

            foreach (ValidationRule rule in this.Rules)
            {
                ValidationResult res = rule.Validate(value, cultureInfo);
                if (!res.IsValid)
                {
                    isValid = false;
                    errors += res.ErrorContent.ToString() + "\n";
                }
            }

            if (isValid)
            {
                return new ValidationResult(true, null);
            }

            return new ValidationResult(false, errors);
        }
    }
}
