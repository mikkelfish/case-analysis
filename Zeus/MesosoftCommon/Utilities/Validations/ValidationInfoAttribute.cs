﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Windows.Controls;

namespace MesosoftCommon.Utilities.Validations
{
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
    public class ValidationInfoAttribute : Attribute
    {
        private Type type;
        public Type ValidationType
        {
            get { return type; }
        }

        private object[] constructorArgs;
        public object[] ConstructorArguments
        {
            get { return constructorArgs; }
        }

        public ValidationInfoAttribute(Type validationType, params object[] constructorArgs)
        {
            this.type = validationType;
            this.constructorArgs = constructorArgs;
        }

        public static ValidationRule CreateValidation(ValidationInfoAttribute validator, ValidationInfoPropertyAttribute[] properties, bool throwOnError)
        {
            if (validator == null) return null;

            object valid = Activator.CreateInstance(validator.type, BindingFlags.Public | BindingFlags.Instance, null, validator.ConstructorArguments, null);

            foreach (ValidationInfoPropertyAttribute attr in properties)
            {
                if (attr.ValidationType != validator.type)
                {
                    if (throwOnError)
                    {
                        throw new ArgumentException("Property attribute not of same validator type as the validator attribute");
                    }
                    return null;
                }

                PropertyInfo info = attr.ValidationType.GetProperty(attr.PropertyName);
                if (info == null && throwOnError)
                {
                    throw new ArgumentException("Property with name " + attr.PropertyName + " not found in type.");
                }

                if(info != null) info.SetValue(valid, attr.Value, null);
            }

            return valid as ValidationRule;
        }

        public static ValidationRule[] CreateValidations(MemberInfo member, bool throwOnError)
        {
            object[] validators = member.GetCustomAttributes(typeof(ValidationInfoAttribute), true);
            if (validators == null || validators.Length == 0) return null;
            Dictionary<Type, List<ValidationInfoPropertyAttribute>> propsDictionary = new Dictionary<Type, List<ValidationInfoPropertyAttribute>>();

            object[] props = member.GetCustomAttributes(typeof(ValidationInfoPropertyAttribute), true);
            foreach (ValidationInfoPropertyAttribute prop in props)
            {
                if (!propsDictionary.ContainsKey(prop.ValidationType))
                {
                    propsDictionary.Add(prop.ValidationType, new List<ValidationInfoPropertyAttribute>());
                }
                propsDictionary[prop.ValidationType].Add(prop);

            }

            List<ValidationRule> toRet = new List<ValidationRule>();
            foreach (ValidationInfoAttribute valid in validators)
            {
                if (!propsDictionary.ContainsKey(valid.ValidationType))
                {
                    propsDictionary.Add(valid.ValidationType, new List<ValidationInfoPropertyAttribute>());
                }

                toRet.Add(ValidationInfoAttribute.CreateValidation(valid, propsDictionary[valid.ValidationType].ToArray(), throwOnError));
            }

            return toRet.ToArray();            
        }
    }
}
