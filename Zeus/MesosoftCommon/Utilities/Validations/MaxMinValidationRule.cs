﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace MesosoftCommon.Utilities.Validations
{
    public class MaxMinValidationRule : ValidationRule
    {
        public double Min { get; set; }
        public double Max { get; set; }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            try
            {
                double val = Convert.ToDouble(value);
                if (val >= this.Min && val <= this.Max) return new ValidationResult(true, null);
                if (val < this.Min) return new ValidationResult(false, "Less than " + this.Min);
                 return new ValidationResult(false, "Greater than " + this.Max);
            }
            catch
            {
                return new ValidationResult(false, "Couldn't convert to double");
            }
        }
    }
}
