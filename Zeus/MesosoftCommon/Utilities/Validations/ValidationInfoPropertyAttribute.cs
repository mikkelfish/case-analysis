﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MesosoftCommon.Utilities.Validations
{
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
    public sealed class ValidationInfoPropertyAttribute : Attribute
    {
        private Type type;

        public Type ValidationType
        {
            get { return type; }
        }

        private string propertyName;

        public string PropertyName
        {
            get { return propertyName; }
        }

        private object val;

        public object Value
        {
            get { return val; }
        }

        public ValidationInfoPropertyAttribute(Type validationType, string propertyName, object val)
        {
            this.val = val;
            this.propertyName = propertyName;
            this.type = validationType;
        }	
    }

}
