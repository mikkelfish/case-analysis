﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Data;
using System.Windows.Controls;
using System.Reflection;

namespace MesosoftCommon.Utilities.Validations
{
    public class ValidatorObject : FrameworkElement
    {
        public const string PressEnterHandler = "OnPressEnter";

        protected void OnPressEnter(object obj, KeyEventArgs e)
        {            
            if (e.Key != Key.Enter) return;
            this.Update(obj);
        }

        private void Update(object obj)
        {
            if(this.TargetType == null || this.PropertyPath == null || !(obj is FrameworkElement)) return;
            FieldInfo info = this.TargetType.GetField(this.PropertyPath + "Property");
            if (info == null || ! (typeof(DependencyProperty).IsAssignableFrom(info.FieldType))) return;

            BindingExpression ex = (obj as FrameworkElement).GetBindingExpression((DependencyProperty)info.GetValue(null));
            if (ex == null) return;
            ex.UpdateSource();

        }

        public Type TargetType
        {
            get { return (Type)GetValue(TargetTypeProperty); }
            set { SetValue(TargetTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TargetType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TargetTypeProperty =
            DependencyProperty.Register("TargetType", typeof(Type), typeof(ValidatorObject));



        public string PropertyPath
        {
            get { return (string)GetValue(PropertyPathProperty); }
            set { SetValue(PropertyPathProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PropertyPath.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PropertyPathProperty =
            DependencyProperty.Register("PropertyPath", typeof(string), typeof(ValidatorObject));


    }
}
