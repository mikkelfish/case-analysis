using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using MesosoftCommon.Development;
using System.ComponentModel;

namespace MesosoftCommon.Utilities.Validations
{
    public class StringLengthValidator : ValidationRule
    {
        public override string ToString()
        {
            return "String Length Validator";
        }

        private int minLength = 0;
        /// <summary>
        /// The minimum length for a valid input.
        /// </summary>
        [Browsable(true)]
        public int MinimumLength
        {
            get { return minLength; }
            set 
            {
                if (minLength < 0) throw new ArgumentException("Maximum length cannot be less than 0.");              
                minLength = value; 
            }
        }

        
        private int maxLength = int.MaxValue;
        /// <summary>
        /// The maximum length for a valid input.
        /// </summary>
        [Browsable(true)]
        public int MaximumLength
        {
            get { return maxLength; }
            set 
            {
                if (maxLength <= 0) throw new ArgumentException("Maximum length cannot be less than 1.");
                maxLength = value; 
            }
        }

        public StringLengthValidator()
        {
            
        }

        public StringLengthValidator(int minLength, int maxLength) : this()
        {
            this.minLength = minLength;
            this.maxLength = maxLength;
        }

        /// <summary>
        /// Overriden function of ValidationRule.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        [Todo("Mikkel", "12/05/06", Comments="Think about whether this should support any types other than string.", Version="0.0", Priority=DevelopmentPriority.Glance)]
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value.GetType() != typeof(string) && value.GetType() != typeof(string[])) return new ValidationResult(false, "Value is not of type string.");
            string str = String.Empty;
            if (value is string) str = value as string;
            else
            {
                string[] array = value as string[];
                foreach (string s in array)
                {
                    str += s + "||";
                }
            }

            if (str.Length < this.minLength) return new ValidationResult(false, "Length is less than " + minLength.ToString());
            if (str.Length > this.maxLength) return new ValidationResult(false, "Length is greater than " + maxLength.ToString());
            return new ValidationResult(true, null);
        }
    }
}
