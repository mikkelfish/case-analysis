﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MesosoftCommon.IO;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Media.Animation;
using System.Windows.Markup;

namespace MesosoftCommon.Layout
{
    /// <summary>
    /// Interaction logic for WindowDock.xaml
    /// Name property must be unique for dockContent data to persist.
    /// </summary>
    public partial class WindowDock : System.Windows.Controls.UserControl
    {
        enum DockSide
        {
            Top,
            Bottom,
            Left,
            Right
        }

        class DockContent : System.Windows.Controls.Grid
        {
            private Rectangle contentPanel = new Rectangle();
            private DockContentTopBar topBar = new DockContentTopBar();
            public GridCellResizer Resizer = new GridCellResizer();

            public int MyProperty { get; set; }

            public static int DockContentCounter = 0;

            public string RegisteredName { get; set; }

            public Panel SourcePanel { get; set; }
            public BitmapImage ImageTag { get; set; }

            [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
            public Window DockableWindow { get; set; }

            private bool isDocked = false;
            public bool IsDocked
            {
                get { return isDocked; }
                set { isDocked = value; }
            }

            private bool isPinned = false;
            public bool IsPinned
            {
                get { return isPinned; }
                set { isPinned = value; }
            }

            public bool IsExpanding { get; set; }
            public bool IsExpanded { get; set; }

            public string DockTitle { get; set; }

            private DockSide dockedSide;
            public DockSide DockedSide
            {
                get
                {
                    return dockedSide;
                }
                set
                {
                    dockedSide = value;
                    switch (value)
                    {
                        case DockSide.Top:
                            this.SourcePanel.VerticalAlignment = VerticalAlignment.Bottom;
                            break;
                        case DockSide.Bottom:
                            this.SourcePanel.VerticalAlignment = VerticalAlignment.Top;
                            break;
                        case DockSide.Left:
                            this.SourcePanel.HorizontalAlignment = HorizontalAlignment.Right;
                            break;
                        case DockSide.Right:
                            this.SourcePanel.HorizontalAlignment = HorizontalAlignment.Left;
                            break;
                        default:
                            break;
                    }
                    dockedSideChanged();
                }
            }

            public Point FloatingWindowCoordinate { get; set; }
            public Type WindowType { get; set; }
            public WindowStyle OriginalWindowStyle { get; set; }
            public ContextMenu OriginalWindowContextMenu { get; set; }
            public Window OriginalWindow { get; set; }

            public DockContent(Window window, WindowDock dock, DockSide side)
            {
                //Store the original values for the window
                Point newCoordinate = new Point(window.Left, window.Top);
                this.FloatingWindowCoordinate = newCoordinate;
                this.WindowType = window.GetType();
                this.OriginalWindowStyle = window.WindowStyle;
                this.OriginalWindowContextMenu = window.ContextMenu;

                //Locate the window's content and remove it from the window
                Panel content = window.Content as Panel;
                if (content == null) return;
                MethodInfo method = window.GetType().GetMethod("RemoveLogicalChild", BindingFlags.NonPublic | BindingFlags.Instance);
                method.Invoke(window, new object[] { content });

                //Keep track of the original window (in case it's restored) and hide it
                this.OriginalWindow = window;
                window.Hide();

                //Reference the content and place it at the second-lowest z-index of the grid (lowest z-index is always reserved for the 
                //grid's content pane)
                this.SourcePanel = content;
                dock.ParentGrid.Children.Insert(1, content);

                //Alter grid layout depending on the side it's docked to, then dock it.

                this.IsDocked = true;
                this.IsPinned = true;
            }

            public DockContent(Panel panel, INameScope nameScope)
            {
                this.SourcePanel = panel;

                topBar.TextValue = "DockContent Test Text";

                this.Background = new SolidColorBrush(System.Windows.Media.Colors.DarkGray);
                topBar.Margin = new Thickness(1, 1, 1, 0);
                panel.Margin = new Thickness(1, 0, 1, 1);

                contentPanel.Fill = new SolidColorBrush(System.Windows.Media.Colors.White);
                contentPanel.Margin = new Thickness(1);

                this.Children.Add(contentPanel);
                this.Children.Add(topBar);
                this.Children.Add(SourcePanel);
                this.Children.Add(Resizer);

                this.IsDocked = true;
                this.IsPinned = true;
                this.IsExpanded = false;
                this.IsExpanding = false;

                NameScope.SetNameScope(this, nameScope);
                this.RegisteredName = "DockContent" + DockContentCounter;
                this.RegisterName(this.RegisteredName, this);
                DockContentCounter++;

                Resizer.GridCellResized += new RoutedEventHandler(resizer_GridCellResized);
            }

            void resizer_GridCellResized(object sender, RoutedEventArgs e)
            {
                if (Resizer.TargetCellType == GridCellResizer.CellType.Column)
                {
                    this.MaxWidth = Resizer.TargetColumn.Width.Value;
                    
                }
                else
                {
                    this.MaxHeight = Resizer.TargetRow.Height.Value;
                }
                this.UpdateLayout();
            }

            private void dockedSideChanged()
            {
                RowDefinition rowDefinition;
                ColumnDefinition columnDefinition;
                
                this.RowDefinitions.Clear();
                this.ColumnDefinitions.Clear();

                if (this.DockedSide == DockSide.Bottom)
                {
                    rowDefinition = new RowDefinition();
                    rowDefinition.Height = new GridLength(dockContentResizerSize);
                    this.RowDefinitions.Add(rowDefinition);
                }

                rowDefinition = new RowDefinition();
                rowDefinition.Height = new GridLength(dockContentBarHeight);
                this.RowDefinitions.Add(rowDefinition);

                rowDefinition = new RowDefinition();
                rowDefinition.Height = new GridLength(1, GridUnitType.Star);
                this.RowDefinitions.Add(rowDefinition);

                if (this.DockedSide == DockSide.Top)
                {
                    rowDefinition = new RowDefinition();
                    rowDefinition.Height = new GridLength(dockContentResizerSize);
                    this.RowDefinitions.Add(rowDefinition);
                }

                if (this.DockedSide == DockSide.Right)
                {
                    columnDefinition = new ColumnDefinition();
                    columnDefinition.Width = new GridLength(dockContentResizerSize);
                    this.ColumnDefinitions.Add(columnDefinition);
                }

                columnDefinition = new ColumnDefinition();
                columnDefinition.Width = new GridLength(1, GridUnitType.Star);
                this.ColumnDefinitions.Add(columnDefinition);

                if (this.DockedSide == DockSide.Left)
                {
                    columnDefinition = new ColumnDefinition();
                    columnDefinition.Width = new GridLength(dockContentResizerSize);
                    this.ColumnDefinitions.Add(columnDefinition);
                }

                switch (this.DockedSide)
                {
                    case DockSide.Top:
                        Grid.SetRow(topBar, 0);
                        Grid.SetRow(contentPanel, 1);
                        Grid.SetRow(SourcePanel, 1);
                        Grid.SetRow(Resizer, 2);

                        Resizer.TargetCellType = GridCellResizer.CellType.Row;
                        
                        break;
                    case DockSide.Bottom:
                        Grid.SetRow(topBar, 1);
                        Grid.SetRow(contentPanel, 2);
                        Grid.SetRow(SourcePanel, 2);
                        Grid.SetRow(Resizer, 0);

                        Resizer.TargetCellType = GridCellResizer.CellType.Row;
                        
                        break;
                    case DockSide.Left:
                        Grid.SetRow(topBar, 0);
                        Grid.SetRow(contentPanel, 1);
                        Grid.SetRow(SourcePanel, 1);
                        Grid.SetRow(Resizer, 0);
                        Grid.SetRowSpan(Resizer, 2);
                        Grid.SetColumn(topBar, 0);
                        Grid.SetColumn(contentPanel, 0);
                        Grid.SetColumn(SourcePanel, 0);
                        Grid.SetColumn(Resizer, 1);

                        Resizer.TargetCellType = GridCellResizer.CellType.Column;
                        
                        break;
                    case DockSide.Right:
                        Grid.SetRow(topBar, 0);
                        Grid.SetRow(contentPanel, 1);
                        Grid.SetRow(SourcePanel, 1);
                        Grid.SetRow(Resizer, 0);
                        Grid.SetRowSpan(Resizer, 2);
                        Grid.SetColumn(topBar, 1);
                        Grid.SetColumn(contentPanel, 1);
                        Grid.SetColumn(SourcePanel, 1);
                        Grid.SetColumn(Resizer, 0);

                        Resizer.TargetCellType = GridCellResizer.CellType.Column;
                        
                        break;
                    default:
                        break;
                }
            }

            public void UpdateResizer(Grid parentGrid)
            {
                if (parentGrid == null)
                {
                    Resizer.TargetColumn = null;
                    Resizer.TargetRow = null;
                    return;
                }

                if (Resizer.TargetCellType == GridCellResizer.CellType.Row)
                {
                    Resizer.TargetColumn = null;
                    if (!IsPinned)
                    {
                        if (DockedSide == DockSide.Top)
                        {
                            Resizer.TargetRow = parentGrid.RowDefinitions[1];
                            Resizer.ResizeFromSide = GridCellResizer.ResizeSide.Bottom;
                        }
                        else if (DockedSide == DockSide.Bottom)
                        {
                            Resizer.TargetRow = parentGrid.RowDefinitions[3];
                            Resizer.ResizeFromSide = GridCellResizer.ResizeSide.Top;
                        }
                    }
                    else
                    {
                        if (DockedSide == DockSide.Top)
                        {
                            Resizer.TargetRow = parentGrid.RowDefinitions[Grid.GetRow(this)];
                            Resizer.ResizeFromSide = GridCellResizer.ResizeSide.Bottom;
                        }
                        else if (DockedSide == DockSide.Bottom)
                        {
                            Resizer.TargetRow = parentGrid.RowDefinitions[Grid.GetRow(this)];
                            Resizer.ResizeFromSide = GridCellResizer.ResizeSide.Top;
                        }
                    }
                }
                else if (Resizer.TargetCellType == GridCellResizer.CellType.Column)
                {
                    Resizer.TargetRow = null;
                    if (!IsPinned)
                    {
                        if (DockedSide == DockSide.Left)
                        {
                            Resizer.TargetColumn = parentGrid.ColumnDefinitions[1];
                            Resizer.ResizeFromSide = GridCellResizer.ResizeSide.Right;
                        }
                        else if (DockedSide == DockSide.Right)
                        {
                            Resizer.TargetColumn = parentGrid.ColumnDefinitions[3];
                            Resizer.ResizeFromSide = GridCellResizer.ResizeSide.Left;
                        }
                    }
                    else
                    {
                        if (DockedSide == DockSide.Left)
                        {
                            Resizer.TargetColumn = parentGrid.ColumnDefinitions[Grid.GetColumn(this)];
                            Resizer.ResizeFromSide = GridCellResizer.ResizeSide.Right;
                        }
                        else if (DockedSide == DockSide.Right)
                        {
                            Resizer.TargetColumn = parentGrid.ColumnDefinitions[Grid.GetColumn(this)];
                            Resizer.ResizeFromSide = GridCellResizer.ResizeSide.Left;
                        }
                    }
                }
            }
        }
      
        private const int sidePanelSize = 22;
        private const int sideButtonMargin = 10;
        private const int dockContentBarHeight = 17;
        private const int dockContentResizerSize = 5;
        private const double expandTimeSpan = 0.10;
        private const double collapseTimeSpan = 0.10;
        private const double collapseDelay = 1.0;
        private const double expandDelay = 0.25;

        //Use an "active animation" variable to ensure that the dock is only animating one property at a time.
        //(And to try to get a handle on the bloody animation's behavior)
        private Storyboard activeAnimation = new Storyboard();

        /// <summary>
        /// When on, restricts docking per side to a maximum of half the available width.  On by default.
        /// </summary>
        public bool UseCenterRestrictedDocks
        {
            get { return (bool)GetValue(UseCenterRestrictedDocksProperty); }
            set { SetValue(UseCenterRestrictedDocksProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UseCenterRestrictedDocks.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UseCenterRestrictedDocksProperty =
            DependencyProperty.Register(
              "UseCenterRestrictedDocks", typeof(bool),
              typeof(WindowDock),
              new UIPropertyMetadata(true)
            );

        private bool isLayoutPaused = false;
        public bool IsLayoutPaused 
        {
            get
            {
                return isLayoutPaused;
            }
            set
            {
                isLayoutPaused = value;
            }
        }

        private double minimumContentWidth = 100.0;
        public double MinimumContentWidth
        {
            get
            {
                return minimumContentWidth;
            }
            set
            {
                minimumContentWidth = value;
            }
        }

        private double  minimumContentHeight = 100.0;
        public double  MinimumContentHeight
        {
            get
            {
                return minimumContentHeight;
            }
            set
            {
                minimumContentHeight = value;
            }
        }

        public void TestLeftExpand()
        {
        }

        public void SlideLeftExpand()
        {
        }

        Color color = new Color();

        public Color ColorShifter()
        {
            byte shiftByte = 49;
            byte brightPassByte = 120;

            //Full solids
            color.A = 255;

            if (ParentGrid.Children.Count % 3 == 0)
            {
                if (color.R <= (byte)206)
                    color.R = (byte)(color.R + shiftByte);
                else
                    color.R = (byte)((color.R + shiftByte) - 255);
            }
            else if (ParentGrid.Children.Count % 3 == 1)
            {
                if (color.G <= (byte)206)
                    color.G = (byte)(color.G + shiftByte);
                else
                    color.G = (byte)((color.G + shiftByte) - 255);
            }
            else if (ParentGrid.Children.Count % 3 == 2)
            {
                if (color.B <= (byte)206)
                    color.B = (byte)(color.B + shiftByte);
                else
                    color.B = (byte)((color.B + shiftByte) - 255);
            }

            //do a post-shift brightness correction
            if ((color.R < brightPassByte) && (color.G < brightPassByte) && (color.B < brightPassByte))
            {
                Color ret = new Color();
                ret.A = color.A;
                ret.R = color.R;
                ret.B = color.B;
                ret.G = color.G;

                if (ParentGrid.Children.Count % 3 == 0)
                {
                    ret.R += brightPassByte;
                }
                else if (ParentGrid.Children.Count % 3 == 1)
                {
                    ret.G += brightPassByte;
                }
                else if (ParentGrid.Children.Count % 3 == 2)
                {
                    ret.B += brightPassByte;
                }

                return ret;
            }
            return color;
        }


        public void AddLeft()
        {
            Button test1 = new Button();
            Button test2 = new Button();
            Button test3 = new Button();

            test1.Content = "1";
            test2.Content = "2";
            test3.Content = "3";

            test1.Height = 23;
            test2.Height = 33;
            test3.Height = 43;
            
            StackPanel stack1 = new StackPanel();
            stack1.Orientation = Orientation.Vertical;
            stack1.Width = 50.0;
            stack1.Children.Add(test1);
            stack1.Children.Add(test2);
            stack1.Children.Add(test3);

            DockContent dc1 = new DockContent(stack1,NameScope.GetNameScope(this));
            dc1.DockedSide = DockSide.Left;

            this.ParentGrid.Children.Add(dc1);

            dc1.Background = new SolidColorBrush(ColorShifter());

            dc1.Width = dc1.SourcePanel.Width + dockContentResizerSize;
            dc1.UpdateLayout(); 
            this.updateDockLayout();
            this.ParentGrid.UpdateLayout();

            dc1.UpdateResizer(this.ParentGrid);
            dc1.Resizer.GridCellResized += new RoutedEventHandler(Resizer_GridCellResized);
        }

        public void AddRight()
        {
            Button test1 = new Button();
            Button test2 = new Button();
            Button test3 = new Button();

            test1.Content = "1";
            test2.Content = "2";
            test3.Content = "3";

            test1.Height = 23;
            test2.Height = 33;
            test3.Height = 43;

            StackPanel stack1 = new StackPanel();
            stack1.Orientation = Orientation.Vertical;
            stack1.Width = 30.0;
            stack1.Children.Add(test1);
            stack1.Children.Add(test2);
            stack1.Children.Add(test3);

            DockContent dc1 = new DockContent(stack1,NameScope.GetNameScope(this));
            dc1.DockedSide = DockSide.Right;

            this.ParentGrid.Children.Add(dc1);

            dc1.Background = new SolidColorBrush(ColorShifter());

            dc1.Width = dc1.SourcePanel.Width + dockContentResizerSize;
            dc1.UpdateLayout();
            this.updateDockLayout(); 
            this.ParentGrid.UpdateLayout();

            dc1.UpdateResizer(this.ParentGrid);
            dc1.Resizer.GridCellResized += new RoutedEventHandler(Resizer_GridCellResized);
        }

        public void AddTop()
        {
            Button test1 = new Button();
            Button test2 = new Button();
            Button test3 = new Button();

            test1.Content = "1";
            test2.Content = "2";
            test3.Content = "3";

            test1.Width = 23;
            test2.Width = 33;
            test3.Width = 43;

            StackPanel stack1 = new StackPanel();
            stack1.Orientation = Orientation.Horizontal;
            stack1.Height = 25.0;
            stack1.Children.Add(test1);
            stack1.Children.Add(test2);
            stack1.Children.Add(test3);

            DockContent dc1 = new DockContent(stack1, NameScope.GetNameScope(this));
            dc1.DockedSide = DockSide.Top;

            this.ParentGrid.Children.Add(dc1);

            dc1.Background = new SolidColorBrush(ColorShifter());

            dc1.Height = dc1.SourcePanel.Height + dockContentResizerSize + dockContentBarHeight;
            dc1.UpdateLayout();
            this.updateDockLayout();
            this.ParentGrid.UpdateLayout();

            dc1.UpdateResizer(this.ParentGrid);
            dc1.Resizer.GridCellResized += new RoutedEventHandler(Resizer_GridCellResized);
        }

        public void AddBottom()
        {
            Button test1 = new Button();
            Button test2 = new Button();
            Button test3 = new Button();

            test1.Content = "1";
            test2.Content = "2";
            test3.Content = "3";

            test1.Width = 23;
            test2.Width = 33;
            test3.Width = 43;

            StackPanel stack1 = new StackPanel();
            stack1.Orientation = Orientation.Horizontal;
            stack1.Height = 55.0;
            stack1.Children.Add(test1);
            stack1.Children.Add(test2);
            stack1.Children.Add(test3);

            DockContent dc1 = new DockContent(stack1, NameScope.GetNameScope(this));
            dc1.DockedSide = DockSide.Bottom;

            this.ParentGrid.Children.Add(dc1);

            dc1.Background = new SolidColorBrush(ColorShifter());

            dc1.Height = dc1.SourcePanel.Height + dockContentResizerSize + dockContentBarHeight;
            dc1.UpdateLayout();
            this.updateDockLayout();
            this.ParentGrid.UpdateLayout();

            dc1.UpdateResizer(this.ParentGrid);
            dc1.Resizer.GridCellResized += new RoutedEventHandler(Resizer_GridCellResized);
        }

        public void AddLeftButton()
        {
            Button test1 = new Button();
            Button test2 = new Button();
            Button test3 = new Button();

            test1.Content = (LeftDock.Children.Count / 2 + 1).ToString();
            test2.Content = "Meh";
            test3.Content = "Feh";

            test1.Height = 23;
            test2.Height = 33;
            test3.Height = 43;

            StackPanel stack1 = new StackPanel();
            stack1.Orientation = Orientation.Vertical;
            stack1.Width = 150.0;
            stack1.Children.Add(test1);
            stack1.Children.Add(test2);
            stack1.Children.Add(test3);

            DockContent dc1 = new DockContent(stack1, NameScope.GetNameScope(this));
            dc1.DockedSide = DockSide.Left;
            dc1.DockTitle = "Left Button" + (LeftDock.Children.Count/2 + 1) ;

            Grid.SetColumn(dc1, 1);
            Grid.SetRowSpan(dc1, 3);
            dc1.HorizontalAlignment = HorizontalAlignment.Left;
            dc1.VerticalAlignment = VerticalAlignment.Top;

            dc1.UpdateLayout();

            this.LeftDock.Children.Add(dc1);

            this.updateSideButtonsLayout(this.LeftDock);
            this.OuterGrid.UpdateLayout();

            dc1.MaxWidth = dc1.SourcePanel.ActualWidth + 2;
            dc1.Width = 0;
            dc1.Visibility = Visibility.Hidden;
            dc1.IsPinned = false;

            dc1.UpdateResizer(this.OuterGrid);
            dc1.Resizer.GridCellResized += new RoutedEventHandler(Resizer_GridCellResized);
        }

        public void AddRightButton()
        {
            Button test1 = new Button();
            Button test2 = new Button();
            Button test3 = new Button();

            test1.Content = (RightDock.Children.Count / 2 + 1).ToString();
            test2.Content = "Meh";
            test3.Content = "Feh";

            test1.Height = 23;
            test2.Height = 33;
            test3.Height = 43;

            StackPanel stack1 = new StackPanel();
            stack1.Orientation = Orientation.Vertical;
            stack1.Width = 150.0;
            stack1.Children.Add(test1);
            stack1.Children.Add(test2);
            stack1.Children.Add(test3);

            DockContent dc1 = new DockContent(stack1, NameScope.GetNameScope(this));
            dc1.DockedSide = DockSide.Right;
            dc1.DockTitle = "Right Button" + (RightDock.Children.Count / 2 + 1);

            Grid.SetColumn(dc1, 0);
            Grid.SetRowSpan(dc1, 3);
            dc1.HorizontalAlignment = HorizontalAlignment.Right;
            dc1.VerticalAlignment = VerticalAlignment.Top;

            dc1.UpdateLayout();

            this.RightDock.Children.Add(dc1);

            this.updateSideButtonsLayout(this.RightDock);
            this.OuterGrid.UpdateLayout();

            dc1.MaxWidth = dc1.SourcePanel.ActualWidth + 2;
            dc1.Width = 0;
            dc1.Visibility = Visibility.Hidden;
            dc1.IsPinned = false;

            dc1.UpdateResizer(this.OuterGrid);
            dc1.Resizer.GridCellResized += new RoutedEventHandler(Resizer_GridCellResized);
        }
        public void AddTopButton()
        {
            Button test1 = new Button();
            Button test2 = new Button();
            Button test3 = new Button();

            test1.Content = (TopDock.Children.Count / 2 + 1).ToString();
            test2.Content = "Meh";
            test3.Content = "Feh";

            test1.Width = 23;
            test2.Width = 33;
            test3.Width = 43;

            StackPanel stack1 = new StackPanel();
            stack1.Orientation = Orientation.Horizontal;
            stack1.Height = 150.0;
            stack1.Children.Add(test1);
            stack1.Children.Add(test2);
            stack1.Children.Add(test3);

            DockContent dc1 = new DockContent(stack1, NameScope.GetNameScope(this));
            dc1.DockedSide = DockSide.Top;
            dc1.DockTitle = "Top Button" + (TopDock.Children.Count / 2 + 1);

            Grid.SetRow(dc1, 1);
            Grid.SetColumnSpan(dc1, 3);
            dc1.HorizontalAlignment = HorizontalAlignment.Left;
            dc1.VerticalAlignment = VerticalAlignment.Top;

            dc1.UpdateLayout();

            this.TopDock.Children.Add(dc1);

            this.updateSideButtonsLayout(this.TopDock);
            this.OuterGrid.UpdateLayout();

            dc1.MaxHeight = dc1.SourcePanel.ActualHeight + dockContentBarHeight + 2;
            dc1.Height = 0;
            dc1.Visibility = Visibility.Hidden;
            dc1.IsPinned = false;

            dc1.UpdateResizer(this.OuterGrid);
            dc1.Resizer.GridCellResized += new RoutedEventHandler(Resizer_GridCellResized);
        }
        public void AddBottomButton()
        {
            Button test1 = new Button();
            Button test2 = new Button();
            Button test3 = new Button();

            test1.Content = (BottomDock.Children.Count / 2 + 1).ToString();
            test2.Content = "Meh";
            test3.Content = "Feh";

            test1.Width = 23;
            test2.Width = 33;
            test3.Width = 43;

            StackPanel stack1 = new StackPanel();
            stack1.Orientation = Orientation.Horizontal;
            stack1.Height = 150.0;
            stack1.Children.Add(test1);
            stack1.Children.Add(test2);
            stack1.Children.Add(test3);

            DockContent dc1 = new DockContent(stack1, NameScope.GetNameScope(this));
            dc1.DockedSide = DockSide.Bottom;
            dc1.DockTitle = "Bottom Button" + (BottomDock.Children.Count / 2 + 1);

            Grid.SetRow(dc1, 0);
            Grid.SetColumnSpan(dc1, 3);
            dc1.HorizontalAlignment = HorizontalAlignment.Left;
            dc1.VerticalAlignment = VerticalAlignment.Bottom;

            dc1.UpdateLayout();

            this.BottomDock.Children.Add(dc1);

            this.updateSideButtonsLayout(this.BottomDock);
            this.OuterGrid.UpdateLayout();

            dc1.MaxHeight = dc1.SourcePanel.ActualHeight + dockContentBarHeight + 2;
            dc1.Width = 0;
            dc1.Visibility = Visibility.Hidden;
            dc1.IsPinned = false;

            dc1.UpdateResizer(this.OuterGrid);
            dc1.Resizer.GridCellResized += new RoutedEventHandler(Resizer_GridCellResized);
        }

        void Resizer_GridCellResized(object sender, RoutedEventArgs e)
        {
            GridCellResizer resizer = sender as GridCellResizer;
            DockContent source = resizer.Parent as DockContent;

            if (!source.IsPinned)
            {
                if (resizer.TargetCellType == GridCellResizer.CellType.Column)
                    oneKeyFrameLinearDoubleAnimate(0, 0, DockContent.WidthProperty, source.MaxWidth, source.RegisteredName, activeAnimation, this);
                else
                    oneKeyFrameLinearDoubleAnimate(0, 0, DockContent.HeightProperty, source.MaxHeight, source.RegisteredName, activeAnimation, this);
            }
            else
            {
                //For pinned objects a little fixup has to be done since the situation is a bit more complex
                //This will be cleaned up in the Custom Grid update
                if (resizer.TargetCellType == GridCellResizer.CellType.Column)
                {
                    source.MaxWidth = resizer.TargetColumn.ActualWidth;
                    source.Width = source.MaxWidth;

                    foreach (DockContent content in (source.Parent as Grid).Children)
                    {
                        if (Grid.GetColumn(content) == (source.Parent as Grid).ColumnDefinitions.IndexOf(resizer.ComplementaryTargetColumn))
                        {
                            content.MaxWidth = resizer.ComplementaryTargetColumn.ActualWidth;
                            content.Width = content.MaxWidth;
                        }
                    }
                }
                else
                {
                    source.MaxHeight = resizer.TargetRow.ActualHeight;
                    source.Height = source.MaxHeight;

                    foreach (DockContent content in (source.Parent as Grid).Children)
                    {
                        if (Grid.GetRow(content) == (source.Parent as Grid).RowDefinitions.IndexOf(resizer.ComplementaryTargetRow))
                        {
                            content.MaxHeight = resizer.ComplementaryTargetRow.ActualHeight;
                            content.Height = content.MaxHeight;
                        }
                    }
                }
            }
        }

        public WindowDock()
        {
            InitializeComponent();

            // Create a NameScope for this page so that
            // Storyboards can be used.
            NameScope.SetNameScope(this, new NameScope());
            activeAnimation.Completed += new EventHandler(translationStoryboard_Completed);
        }

        private RowDefinition getRowDefinition(double value, bool isStar)
        {
            RowDefinition ret = new RowDefinition();
            if (isStar)
                ret.Height = new GridLength(value, GridUnitType.Star);
            else
            {
                ret.Height = new GridLength(value);
                ret.MaxHeight = value;
                ret.MinHeight = value;
            }
            return ret;
        }

        private ColumnDefinition getColumnDefinition(double value, bool isStar)
        {
            ColumnDefinition ret = new ColumnDefinition();
            if (isStar)
                ret.Width = new GridLength(value, GridUnitType.Star);
            else
            {
                ret.Width = new GridLength(value);
                ret.MaxWidth = value;
                ret.MinWidth = value;
            }
            return ret;
        }

        private void updateSideButtonsLayout()
        {
            this.updateSideButtonsLayout(this.LeftDock);
            this.updateSideButtonsLayout(this.TopDock);
            this.updateSideButtonsLayout(this.RightDock);
            this.updateSideButtonsLayout(this.BottomDock);
        }

        private void updateSideButtonsLayout(Grid sideToUpdate)
        {
            List<Button> buttons = new List<Button>();
            int firstButtonIndex = 0;

            //Clean up old row/column definitions
            if (sideToUpdate == LeftDock || sideToUpdate == RightDock)
                sideToUpdate.RowDefinitions.Clear();
            else if (sideToUpdate == TopDock || sideToUpdate == BottomDock)
                sideToUpdate.ColumnDefinitions.Clear();

            foreach (object obj in sideToUpdate.Children)
            {
                if (obj.GetType() != typeof(DockContent))
                {
                    if (obj.GetType() == typeof(Button) && firstButtonIndex == 0)
                        firstButtonIndex = sideToUpdate.Children.IndexOf(obj as Button);
                    continue;
                }

                DockContent content = obj as DockContent;

                Button dockButton = new Button();
                dockButton.Content = content.DockTitle;
                dockButton.Tag = content.Tag;
                dockButton.Style = FindResource(MesosoftStyleLibrary.TestDictionary.SideButtonDefaultKey) as Style;
                
                //we call Measure directly so that we can hijack the DesiredSize
                //This is done because the grid will not properly enforce the width
                dockButton.Measure(new Size(8000, 8000));
                dockButton.Width = dockButton.DesiredSize.Width;
                
                //Handle row/column shenannigans
                switch (content.DockedSide)
                {
                    case DockSide.Top:
                        Grid.SetRow(dockButton, 0);
                        dockButton.VerticalContentAlignment = VerticalAlignment.Top;
                        dockButton.Margin = new Thickness(0, 0, 0, 2);
                        sideToUpdate.ColumnDefinitions.Add(getColumnDefinition(dockButton.Width + WindowDock.sideButtonMargin, false));
                        Grid.SetColumn(dockButton, sideToUpdate.ColumnDefinitions.Count - 1);
                        Grid.SetColumnSpan(content, sideToUpdate.Children.Count + 1);
                        break;
                    case DockSide.Bottom:
                        Grid.SetRow(dockButton, 1);
                        dockButton.VerticalContentAlignment = VerticalAlignment.Bottom;
                        dockButton.Margin = new Thickness(0, 2, 0, 0);
                        sideToUpdate.ColumnDefinitions.Add(getColumnDefinition(dockButton.Width + WindowDock.sideButtonMargin, false));
                        Grid.SetColumn(dockButton, sideToUpdate.ColumnDefinitions.Count - 1);
                        Grid.SetColumnSpan(content, sideToUpdate.Children.Count + 1);
                        break;
                    case DockSide.Left:
                        Grid.SetColumn(dockButton, 0);
                        dockButton.HorizontalContentAlignment = HorizontalAlignment.Left;
                        dockButton.Margin = new Thickness(0, 0, 2, 0);
                        sideToUpdate.RowDefinitions.Add(getRowDefinition(dockButton.Width + WindowDock.sideButtonMargin, false));
                        Grid.SetRow(dockButton, sideToUpdate.RowDefinitions.Count - 1);
                        Grid.SetRowSpan(content, sideToUpdate.Children.Count + 1);
                        break;
                    case DockSide.Right:
                        Grid.SetColumn(dockButton, 1);
                        dockButton.HorizontalContentAlignment = HorizontalAlignment.Right;
                        dockButton.Margin = new Thickness(2, 0, 0, 0);
                        sideToUpdate.RowDefinitions.Add(getRowDefinition(dockButton.Width + WindowDock.sideButtonMargin, false));
                        Grid.SetRow(dockButton, sideToUpdate.RowDefinitions.Count - 1);
                        Grid.SetRowSpan(content, sideToUpdate.Children.Count + 1);
                        break;
                    default:
                        break;
                }

                dockButton.MouseEnter += new MouseEventHandler(dockButton_MouseEnter);
                dockButton.MouseLeave += new MouseEventHandler(dockButton_MouseLeave);
                //These delegates are added and never removed so this might be bad
                content.MouseEnter += new MouseEventHandler(content_MouseEnter);
                content.MouseLeave += new MouseEventHandler(content_MouseLeave);

                buttons.Add(dockButton);
            }

            //Fix up the row/column endcaps
            if (sideToUpdate == LeftDock || sideToUpdate == RightDock)
                sideToUpdate.RowDefinitions.Add(getRowDefinition(1, true));
            else if (sideToUpdate == TopDock || sideToUpdate == BottomDock)
                sideToUpdate.ColumnDefinitions.Add(getColumnDefinition(1, true));

            //Clean out old buttons
            sideToUpdate.Children.RemoveRange(firstButtonIndex, sideToUpdate.Children.Count - firstButtonIndex - 1);

            foreach (Button button in buttons)
            {
                sideToUpdate.Children.Add(button);
            }
        }

        void content_MouseLeave(object sender, MouseEventArgs e)
        {
            DockContent source = sender as DockContent;

            if (!source.Resizer.IsDragInProcess)
                this.collapseSidePanel(source);
        }

        void content_MouseEnter(object sender, MouseEventArgs e)
        {
            DockContent source = sender as DockContent;

            this.expandSidePanel(source);
        }

        void dockButton_MouseLeave(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;

            //Locate the related DockContent
            //Since Buttons are generated in the same order as the contents they are generated from, it is easy to locate the appropriate content.
            Grid buttonParentGrid = (Grid)button.Parent;
            DockContent source = buttonParentGrid.Children[(buttonParentGrid.Children.IndexOf(button) - (buttonParentGrid.Children.Count / 2))] as DockContent;

            this.collapseSidePanel(source);
        }

        void dockButton_MouseEnter(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;

            //Locate the related DockContent
            //Since Buttons are generated in the same order as the contents they are generated from, it is easy to locate the appropriate content.
            Grid buttonParentGrid = (Grid)button.Parent;
            DockContent source = buttonParentGrid.Children[(buttonParentGrid.Children.IndexOf(button) - (buttonParentGrid.Children.Count / 2))] as DockContent;

            this.expandSidePanel(source);
        }

        void collapseSidePanel(DockContent source)
        {
            // Create a DoubleAnimationUsingKeyFrames to animate the sizeAnimation.
            DoubleAnimationUsingKeyFrames sizeAnimation = new DoubleAnimationUsingKeyFrames();
            sizeAnimation.Duration = TimeSpan.FromSeconds(collapseTimeSpan);
            DependencyProperty propertyToAnimate;

            if (source.DockedSide == DockSide.Left)
            {
                propertyToAnimate = DockContent.WidthProperty;
                source.Height = ParentGrid.ActualHeight;
            }
            else if (source.DockedSide == DockSide.Right)
            {
                propertyToAnimate = DockContent.WidthProperty;
                source.Height = ParentGrid.ActualHeight;
            }
            else if (source.DockedSide == DockSide.Top)
            {
                propertyToAnimate = DockContent.HeightProperty;
                source.Width = ParentGrid.ActualWidth;
            }
            else //if (source.DockedSide == DockSide.Bottom) - last enum, leave it as else to catch assignment of propertyToAnimate
            {
                propertyToAnimate = DockContent.HeightProperty;
                source.Width = ParentGrid.ActualWidth;
            }

            source.IsExpanding = false;
            oneKeyFrameLinearDoubleAnimate(collapseTimeSpan, collapseDelay, propertyToAnimate, 0, source.RegisteredName, activeAnimation, this);
        }

        void expandSidePanel(DockContent source)
        {
            bool expandedPanelIsPresent = false;

            //Move this panel to the top and make sure all the others are below it
            foreach (UIElement element in OuterGrid.Children)
            {
                Grid.SetZIndex(element, OuterGrid.Children.IndexOf(element));
            }
            Grid.SetZIndex((source.Parent as UIElement), OuterGrid.Children.Count);

            //Move the source element itself to the top (within its panel)
            foreach (UIElement element in (source.Parent as Panel).Children)
            {
                Grid.SetZIndex(element, (source.Parent as Panel).Children.IndexOf(element));
                if (element is DockContent && (element as DockContent).IsExpanded )
                    expandedPanelIsPresent = true;
            }
            Grid.SetZIndex(source, (source.Parent as Panel).Children.Count);

            source.Visibility = Visibility.Visible;
            DependencyProperty propertyToAnimate;
            double valueToAnimateTo;

            if (source.DockedSide == DockSide.Left)
            {
                OuterGrid.ColumnDefinitions[1].MaxWidth = source.MaxWidth;
                OuterGrid.ColumnDefinitions[1].Width = new GridLength(source.MaxWidth);
                source.Height = ParentGrid.ActualHeight;
                propertyToAnimate = DockContent.WidthProperty;
                valueToAnimateTo = source.MaxWidth;
            }
            else if (source.DockedSide == DockSide.Right)
            {
                OuterGrid.ColumnDefinitions[3].MaxWidth = source.MaxWidth;
                OuterGrid.ColumnDefinitions[3].Width = new GridLength(source.MaxWidth);
                source.Height = ParentGrid.ActualHeight;
                propertyToAnimate = DockContent.WidthProperty;
                valueToAnimateTo = source.MaxWidth;
            }
            else if (source.DockedSide == DockSide.Top)
            {
                OuterGrid.RowDefinitions[1].MaxHeight = source.MaxHeight;
                OuterGrid.RowDefinitions[1].Height = new GridLength(source.MaxHeight);
                source.Width = ParentGrid.ActualWidth;
                propertyToAnimate = DockContent.HeightProperty;
                valueToAnimateTo = source.MaxHeight;
            }
            else //if (source.DockedSide == DockSide.Bottom) - only four enums for DockSide and all the others are already covered!
            {
                OuterGrid.RowDefinitions[3].MaxHeight = source.MaxHeight;
                OuterGrid.RowDefinitions[3].Height = new GridLength(source.MaxHeight);
                source.Width = ParentGrid.ActualWidth;
                propertyToAnimate = DockContent.HeightProperty;
                valueToAnimateTo = source.MaxHeight;
            }

            BottomDock.UpdateLayout();
            OuterGrid.UpdateLayout();

            //If there's an active animation with animation elements, push it to the end of its animation and skip the delay on the expand.
            if ((activeAnimation.Children.Count > 0) && expandedPanelIsPresent)
            {
                activeAnimation.SkipToFill(this);
                source.IsExpanding = true;
                //activeAnimation.BeginTime = new TimeSpan(0);
                oneKeyFrameLinearDoubleAnimate(0, 0, propertyToAnimate, valueToAnimateTo, source.RegisteredName, activeAnimation, this);
            }
            else
            {
                source.IsExpanding = true;
                oneKeyFrameLinearDoubleAnimate(expandTimeSpan, expandDelay, propertyToAnimate, valueToAnimateTo, source.RegisteredName, activeAnimation, this);
            }
        }

        private void oneKeyFrameLinearDoubleAnimate(double durationInSeconds, double delayInSeconds, DependencyProperty propertyToAnimate,
                                                    double targetValue, string registeredName, Storyboard storyboardToAnimateIn,
                                                    FrameworkElement containingObject)
        {
            // Create a DoubleAnimationUsingKeyFrames to animate the sizeAnimation.
            DoubleAnimationUsingKeyFrames sizeAnimation = new DoubleAnimationUsingKeyFrames();
            sizeAnimation.Duration = TimeSpan.FromSeconds(durationInSeconds);

            sizeAnimation.KeyFrames.Add(new LinearDoubleKeyFrame(targetValue, KeyTime.FromTimeSpan(TimeSpan.FromSeconds(durationInSeconds))));
            Storyboard.SetTargetProperty(sizeAnimation, new PropertyPath(propertyToAnimate));
            Storyboard.SetTargetName(sizeAnimation, registeredName);

            storyboardToAnimateIn.Children.Clear();
            storyboardToAnimateIn.Children.Add(sizeAnimation);

            storyboardToAnimateIn.BeginTime = new TimeSpan(0, 0, 0, 0, (int)(delayInSeconds * 1000));
            storyboardToAnimateIn.Begin(this, true);
        }

        void translationStoryboard_Completed(object sender, EventArgs e)
        {
            DockContent content = this.OuterGrid.FindName(Storyboard.GetTargetName((sender as ClockGroup).Timeline.Children[0])) as DockContent;

            if (content.IsExpanding)
            {
                content.IsExpanded = true;
                return;
            }

            if ((content.DockedSide == DockSide.Top || content.DockedSide == DockSide.Bottom) && content.Height < content.MaxHeight)
                content.Visibility = Visibility.Hidden;
            else if ((content.DockedSide == DockSide.Left || content.DockedSide == DockSide.Right) && content.Width < content.MaxWidth)
                content.Visibility = Visibility.Hidden;

            content.IsExpanded = false;
        }

        private void updateDockLayout()
        {
            int leftElements = 0;
            int rightElements = 0;
            int topElements = 0;
            int bottomElements = 0;

            double availableWidth = this.ParentGrid.ActualWidth - this.MinimumContentWidth;
            double availableHeight = this.ParentGrid.ActualHeight - this.MinimumContentHeight;

            if (this.UseCenterRestrictedDocks)
            {
                availableHeight /= 2;
                availableWidth /= 2;
            }

            //First pass - count the number of pinned elements for the col/row span assignments in the second pass
            foreach (DockContent content in this.ParentGrid.Children)
            {
                if (!content.IsPinned) continue;

                switch (content.DockedSide)
                {
                    case DockSide.Top:
                        topElements++;
                        break;
                    case DockSide.Bottom:
                        bottomElements++;
                        break;
                    case DockSide.Left:
                        leftElements++;
                        break;
                    case DockSide.Right:
                        rightElements++;
                        break;
                    default:
                        break;
                }
            }

            //Clear the Row/Col defs for layout.
            this.ParentGrid.ColumnDefinitions.Clear();
            this.ParentGrid.RowDefinitions.Clear();

            //Precalculate the "mid" columns and rows for the second pass
            int midColumns = leftElements + rightElements;
            int midRows = topElements + bottomElements;

            int leftRunningCount = 0;
            int topRunningCount = 0;
            int rightRunningCount = 0;
            int bottomRunningCount = 0;

            //Make the content row/column def to pin the others around
            this.ParentGrid.ColumnDefinitions.Add(getColumnDefinition(1, true));
            this.ParentGrid.RowDefinitions.Add(getRowDefinition(1, true));

            //Iterate through the DockContents in the ParentGrid, they are presented in the order they were pinned.
            foreach (DockContent content in this.ParentGrid.Children)
            {
                if (!content.IsPinned) continue;

                switch (content.DockedSide)
                {
                    case DockSide.Top:
                        this.ParentGrid.RowDefinitions.Insert(topRunningCount, getRowDefinition(content.ActualHeight, false));

                        Grid.SetRow(content, topRunningCount);
                        Grid.SetColumn(content, leftRunningCount);
                        Grid.SetRowSpan(content, 1);
                        Grid.SetColumnSpan(content, ((leftElements - leftRunningCount) + 1 + (rightElements - rightRunningCount)));

                        topRunningCount++;
                        break;
                    case DockSide.Bottom:
                        this.ParentGrid.RowDefinitions.Insert((topRunningCount + 1), getRowDefinition(content.ActualHeight, false));

                        Grid.SetRow(content, (topElements + bottomElements) - bottomRunningCount);
                        Grid.SetColumn(content, leftRunningCount);
                        Grid.SetRowSpan(content, 1);
                        Grid.SetColumnSpan(content, ((leftElements - leftRunningCount) + 1 + (rightElements - rightRunningCount)));

                        bottomRunningCount++;
                        break;
                    case DockSide.Left:
                        this.ParentGrid.ColumnDefinitions.Insert(leftRunningCount, getColumnDefinition(content.ActualWidth, false));

                        Grid.SetRow(content, topRunningCount);
                        Grid.SetColumn(content, leftRunningCount);
                        Grid.SetRowSpan(content, ((topElements - topRunningCount) + 1 + (bottomElements - bottomRunningCount)));
                        Grid.SetColumnSpan(content, 1);

                        leftRunningCount++;
                        break;
                    case DockSide.Right:
                        this.ParentGrid.ColumnDefinitions.Insert((leftRunningCount + 1), getColumnDefinition(content.ActualWidth, false));

                        Grid.SetRow(content, topRunningCount);
                        Grid.SetColumn(content, (leftElements + rightElements) - rightRunningCount);
                        Grid.SetRowSpan(content, ((topElements - topRunningCount) + 1 + (bottomElements - bottomRunningCount)));
                        Grid.SetColumnSpan(content, 1);

                        rightRunningCount++;
                        break;
                    default:
                        break;
                }
            }

            //Third and final pass, to update resizers.
            foreach (DockContent content in this.ParentGrid.Children)
            {
                //Update the resizer with the appropriate info
                content.UpdateResizer(this.ParentGrid);
            }
        }

        static void NamePropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //WindowDock self = obj as WindowDock;
            //string name = e.NewValue as string;

            //if (UniversalCollectionManager.ContainsCollection(name))
            //    self.dockContentList = UniversalCollectionManager.CreateCollection<UniversalListCollection<UniversalItemWrapper<DockContent>>>(name);
            //else
            //{
            //    self.dockContentList.Settings.UpdateSourceOnExit = true;
            //    UniversalCollectionManager.RegisterCollection(name, self.dockContentList);
            //}
        }

        static WindowDock()
        {         
        }
    }
}
