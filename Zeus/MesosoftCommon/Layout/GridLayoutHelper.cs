﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;
using MesosoftCommon.Layout.BasicPanels;

namespace MesosoftCommon.Layout
{
    class MarginHelper
    {
        #region Standard Rect-style Identifiers (Top/Bottom/Left/Right etc...)
        private double top = double.NaN;
        public double Top
        {
            get { return top; }
            set { top = value; }
        }

        private double bottom = double.NaN;
        public double Bottom
        {
            get { return bottom; }
            set { bottom = value; }
        }

        private double left = double.NaN;
        public double Left
        {
            get { return left; }
            set { left = value; }
        }

        private double right = double.NaN;
        public double Right
        {
            get { return right; }
            set { right = value; }
        }
        #endregion //Standard Rect-style Identifiers (Top/Bottom/Left/Right etc...)

        /// <summary>
        /// Sets the current margin stored in the MarginHelper (via top/bottom/left/right) to a FrameworkElement.
        /// Margin thicknesses set to Double.NaN indicates a thickness that is not to be modified.
        /// </summary>
        /// <param name="element">The FrameworkElement to set the current margin on.</param>
        public void SetThisMarginOnElement(FrameworkElement element)
        {
            Thickness newMargin = new Thickness();
            newMargin.Top = double.IsNaN(this.Top) ? element.Margin.Top : this.Top;
            newMargin.Bottom = double.IsNaN(this.Bottom) ? element.Margin.Bottom : this.Bottom;
            newMargin.Left = double.IsNaN(this.Left) ? element.Margin.Left : this.Left;
            newMargin.Right = double.IsNaN(this.Right) ? element.Margin.Right : this.Right;
            element.Margin = newMargin;
        }

        public static void ApplyPointToElementAsMargin(FrameworkElement element, Point point, double heightOfCell, double widthOfCell,
            bool resizeToConstraint)
        {
            Thickness newMargin = new Thickness();
            double verticalMarginSize = heightOfCell - element.ActualHeight;
            double horizontalMarginSize = widthOfCell - element.ActualWidth;

            if (element.VerticalAlignment == VerticalAlignment.Stretch || element.VerticalAlignment == VerticalAlignment.Center)
            {
                newMargin.Top = ((point.Y * 2) - verticalMarginSize);
                newMargin.Bottom = (verticalMarginSize - (point.Y * 2));
            }
            else
            {

            }

            if (element.HorizontalAlignment == HorizontalAlignment.Stretch || element.HorizontalAlignment == HorizontalAlignment.Center)
            {
                newMargin.Left = ((point.X * 2) - horizontalMarginSize);
                newMargin.Right = ((horizontalMarginSize - point.X) * 2);
            }
            else
            {

            }

            element.Margin = newMargin;
        }

        public static Point GetPointFromMargin(FrameworkElement element, double heightOfCell, double widthOfCell)
        {
            double y = 0;
            double x = 0;

            double verticalMarginSize = heightOfCell - element.ActualHeight;
            double horizontalMarginSize = widthOfCell - element.ActualWidth;

            if (element.VerticalAlignment == VerticalAlignment.Stretch || element.VerticalAlignment == VerticalAlignment.Center)
            {
                if (element.Margin.Top != 0)
                    y = (element.Margin.Top + verticalMarginSize) / 2;
                else if (element.Margin.Bottom != 0)
                    y = ((element.Margin.Bottom - verticalMarginSize) / -2);
                else
                    y = verticalMarginSize / 2;
            }
            else if (element.VerticalAlignment == VerticalAlignment.Top)
                y = element.Margin.Top / 2;
            else if (element.VerticalAlignment == VerticalAlignment.Bottom)
                y = verticalMarginSize - element.Margin.Bottom;

            if (element.HorizontalAlignment == HorizontalAlignment.Stretch || element.HorizontalAlignment == HorizontalAlignment.Center)
            {
                if (element.Margin.Left != 0)
                    x = (element.Margin.Left + horizontalMarginSize) / 2;
                else if (element.Margin.Right != 0)
                    x = ((element.Margin.Right - horizontalMarginSize) / -2);
                else
                    x = horizontalMarginSize / 2;
            }
            else if (element.HorizontalAlignment == HorizontalAlignment.Left)
                x = element.Margin.Left / 2;
            else if (element.HorizontalAlignment == HorizontalAlignment.Right)
                x = horizontalMarginSize - element.Margin.Right;

            return new Point(x, y);
        }
    }

    public class GridLayoutHelper
    {
        #region Grid Source specifiers
        private MesoGrid source;
        /// <summary>
        /// Get or Set the Grid source
        /// </summary>
        public MesoGrid Source
        {
            get { return source; }
            set { source = value; }
        }

        private int row;
        /// <summary>
        /// Get or Set the Grid Row for the coordinate.  This defines the row of the "subgrid," used elsewhere in the helper.
        /// </summary>
        public int Row
        {
            get { return row; }
            set { row = value; }
        }

        private int column;
        /// <summary>
        /// Get or Set the Grid Column for the coordinate.  This defines the column of the "subgrid," used elsewhere in the helper.
        /// </summary>
        public int Column
        {
            get { return column; }
            set { column = value; }
        }
        #endregion //Grid Source specifiers

        #region Standard Rect-style Identifiers (Top/Bottom/Left/Right etc...)
        /// <summary>
        /// Gets the y-axis position of the top of the source row/column (Y).
        /// </summary>
        public double Top
        {
            get
            {
                if (source == null) return 0;
                double runningLimit = 0;
                for (int i = 0; i < this.row; i++)
                {
                    runningLimit += this.source.RowDefinitions[i].ActualLength;
                }
                return runningLimit;
            }
        }

        /// <summary>
        /// Gets the y-axis position of the bottom of the source row/column (Y + ActualHeight).
        /// </summary>
        public double Bottom
        {
            get
            {
                if (source == null) return 0;
                double runningLimit = 0;
                for (int i = 0; i <= this.row; i++)
                {
                    runningLimit += this.source.RowDefinitions[i].ActualLength;
                }
                return runningLimit;
            }
        }

        /// <summary>
        /// Gets the x-axis position of the left of the source row/column (X).
        /// </summary>
        public double Left
        {
            get
            {
                if (source == null) return 0;
                double runningLimit = 0;
                for (int i = 0; i < this.column; i++)
                {
                    runningLimit += this.source.ColumnDefinitions[i].ActualLength;
                }
                return runningLimit;
            }
        }

        /// <summary>
        /// Gets the x-axis position of the right of the source row/column (X + ActualWidth).
        /// </summary>
        public double Right
        {
            get
            {
                if (source == null) return 0;
                double runningLimit = 0;
                for (int i = 0; i <= this.column; i++)
                {
                    runningLimit += this.source.ColumnDefinitions[i].ActualLength;
                }
                return runningLimit;
            }
        }

        /// <summary>
        /// Gets the actual width of the source row/column (Right - Left).
        /// </summary>
        public double ActualWidth
        {
            get
            {
                return this.Right - this.Left;
            }
        }

        /// <summary>
        /// Gets the actual height of the source row/column (Bottom - Top).
        /// </summary>
        public double ActualHeight
        {
            get
            {
                return this.Bottom - this.Top;
            }
        }
        #endregion //Standard Rect-style Identifiers (Top/Bottom/Left/Right etc...)

        public void PositionElementWithinGrid(FrameworkElement element, Point desiredTopLeftCoordinateOnGrid, bool enforceBoundaries,
            HorizontalAlignment desiredHorizontalAlignment, VerticalAlignment desiredVerticalAlignment, bool forceHelperRowCol)
        {
            //Ignore requests to position an element that is not a direct child of the grid.
            if (!this.Source.Children.Contains(element)) return;

            //set up a MarginHelper
            MarginHelper marginHelper = new MarginHelper();

            //Translate the desiredTopLeftCoordinateOnGrid point into a coordinate local to the subgrid that is currently selected (0,0 by default)
            Point topLeftCoordinateOnSubGrid = new Point();

            //If boundaries aren't enforced, there are a few concerns.  Primarily - bounds or not, the element must always stay on the grid.
            if (!enforceBoundaries)
            {
                //Handle Right and Bottom bounds
                double xOverflow = (desiredTopLeftCoordinateOnGrid.X + element.ActualWidth) - this.Source.ActualWidth;
                if (xOverflow > 0) desiredTopLeftCoordinateOnGrid.X -= xOverflow;
                double yOverflow = (desiredTopLeftCoordinateOnGrid.Y + element.ActualHeight) - this.Source.ActualHeight;
                if (yOverflow > 0) desiredTopLeftCoordinateOnGrid.Y -= yOverflow;

                //Handle Left and Top bounds
                if (desiredTopLeftCoordinateOnGrid.X < 0) desiredTopLeftCoordinateOnGrid.X = 0;
                if (desiredTopLeftCoordinateOnGrid.Y < 0) desiredTopLeftCoordinateOnGrid.Y = 0;
            }

            topLeftCoordinateOnSubGrid.X = desiredTopLeftCoordinateOnGrid.X - this.Left;
            topLeftCoordinateOnSubGrid.Y = desiredTopLeftCoordinateOnGrid.Y - this.Top;

            //If boundaries are being enforced, restrain the translated point by the bounds of the subgrid
            if (enforceBoundaries)
            {
                if (topLeftCoordinateOnSubGrid.X < 0) topLeftCoordinateOnSubGrid.X = 0;
                else if ((topLeftCoordinateOnSubGrid.X + element.ActualWidth) > this.ActualWidth) topLeftCoordinateOnSubGrid.X = this.ActualWidth - element.ActualWidth;
                if (topLeftCoordinateOnSubGrid.Y < 0) topLeftCoordinateOnSubGrid.Y = 0;
                else if ((topLeftCoordinateOnSubGrid.Y + element.ActualHeight) > this.ActualHeight) topLeftCoordinateOnSubGrid.Y = this.ActualHeight - element.ActualHeight;
            }

            //Note: Negative margins are used instead of positive ones because positive margins push their container out instead of flowing
            //outside of it.  This isn't a concern when boundaries are enforced but becomes an issue if they aren't.

            //Handle the X-axis margins by orientation style
            //if width is non-specified within Stretch, it stretches to fill the whole axis, so nothing needs to be done
            //Otherwise it exhibits the same behavior as a centered element.
            if ((desiredHorizontalAlignment == HorizontalAlignment.Stretch && !Double.IsNaN(element.Width)) ||
                    desiredHorizontalAlignment == HorizontalAlignment.Center)
            {
                //In a centered element, the range of offsets goes from 0 to (GridCoordinateWidth - element width)
                //This effectively halves the size of a margin offset in this mode!
                //Find the horizontalMargin in terms of the change in X-coord from the centered location.
                double horizontalMargin = topLeftCoordinateOnSubGrid.X - ((this.ActualWidth - element.ActualWidth) / 2);

                //Multiply by 2 to account for the offset halving, and keep the margin number negative.
                if (horizontalMargin <= 0)
                {
                    marginHelper.Left = horizontalMargin * 2;
                    marginHelper.Right = 0;
                }
                else
                {
                    marginHelper.Right = horizontalMargin * -2;
                    marginHelper.Left = 0;
                }
            }
            else
            {
                double horizontalMargin = topLeftCoordinateOnSubGrid.X;
                if (desiredHorizontalAlignment == HorizontalAlignment.Left)
                {
                    //If the desired position is too far to the right (outside a non-enforced boundary), horizontal alignment must be switched
                    //to right.
                    if ((horizontalMargin + element.ActualWidth) > this.ActualWidth)
                    {
                        element.HorizontalAlignment = HorizontalAlignment.Right;
                        marginHelper.Right = this.ActualWidth - (horizontalMargin + element.ActualWidth);
                        marginHelper.Left = 0;
                    }
                    else
                    {
                        element.HorizontalAlignment = HorizontalAlignment.Left;
                        marginHelper.Left = horizontalMargin;
                        marginHelper.Right = 0;
                    }
                }
                else if (desiredHorizontalAlignment == HorizontalAlignment.Right)
                {
                    if (horizontalMargin < 0)
                    {
                        element.HorizontalAlignment = HorizontalAlignment.Left;
                        marginHelper.Left = horizontalMargin;
                        marginHelper.Right = 0;
                    }
                    else
                    {
                        element.HorizontalAlignment = HorizontalAlignment.Right;
                        marginHelper.Right = this.ActualWidth - (horizontalMargin + element.ActualWidth);
                        marginHelper.Left = 0;
                    }
                }
            }

            //Handle the Y-axis margins by orientation style
            if ((desiredVerticalAlignment == VerticalAlignment.Stretch && !Double.IsNaN(element.Height)) ||
                    desiredVerticalAlignment == VerticalAlignment.Center)
            {
                double verticalMargin = topLeftCoordinateOnSubGrid.Y - ((this.ActualHeight - element.ActualHeight) / 2);

                if (verticalMargin <= 0)
                {
                    marginHelper.Top = verticalMargin * 2;
                    marginHelper.Bottom = 0;
                }
                else
                {
                    marginHelper.Bottom = verticalMargin * -2;
                    marginHelper.Top = 0;
                }
            }
            else
            {
                double verticalMargin = topLeftCoordinateOnSubGrid.Y;
                if (desiredVerticalAlignment == VerticalAlignment.Top)
                {
                    if ((verticalMargin + element.ActualHeight) > this.ActualHeight)
                    {
                        element.VerticalAlignment = VerticalAlignment.Bottom;
                        marginHelper.Bottom = this.ActualHeight - (verticalMargin + element.ActualHeight);
                        marginHelper.Top = 0;
                    }
                    else
                    {
                        element.VerticalAlignment = VerticalAlignment.Top;
                        marginHelper.Top = verticalMargin;
                        marginHelper.Bottom = 0;
                    }
                }
                else if (desiredVerticalAlignment == VerticalAlignment.Bottom)
                {
                    if (verticalMargin < 0)
                    {
                        element.VerticalAlignment = VerticalAlignment.Top;
                        marginHelper.Top = verticalMargin;
                        marginHelper.Bottom = 0;
                    }
                    else
                    {
                        element.VerticalAlignment = VerticalAlignment.Bottom;
                        marginHelper.Bottom = this.ActualHeight - (verticalMargin + element.ActualHeight);
                        marginHelper.Top = 0;
                    }
                }
            }
            marginHelper.SetThisMarginOnElement(element);
            
            if (forceHelperRowCol)
            {
                //Force the proper row and column onto the element
                MesoGrid.SetRow(element, Row);
                MesoGrid.SetColumn(element, Column);
            }
        }

        public static GridLayoutHelper LocateMouseOnGrid(MesoGrid hostGrid)
        {
            if (!hostGrid.IsMouseOver)
                return null;

            GridLayoutHelper ret = new GridLayoutHelper();
            Point mousePosition = Mouse.GetPosition(hostGrid);
            double runningLimit = 0;
            int i = 0;
            int j = 0;

            for (i = 0; i < hostGrid.ColumnDefinitions.Count - 1; i++)
            {
                runningLimit += hostGrid.ColumnDefinitions[i].ActualLength;
                if (mousePosition.X < runningLimit)
                {
                    ret.Column = i;
                    break;
                }
            }
            //Catch the grid edge in the final column
            if (i == hostGrid.ColumnDefinitions.Count - 1)
            {
                ret.Column = i;
            }

            runningLimit = 0;
            for (j = 0; j < hostGrid.RowDefinitions.Count - 1; j++)
            {
                runningLimit += hostGrid.RowDefinitions[j].ActualLength;
                if (mousePosition.Y < runningLimit)
                {
                    ret.Row = j;
                    break;
                }
            }
            //Catch the grid edge in the final row
            if (j == hostGrid.RowDefinitions.Count - 1)
            {
                ret.Row = j;
            }

            ret.Source = hostGrid;
            return ret;
        }

        public override string ToString()
        {
            string ret = "Row : " + this.Row + " Col : " + this.Column + ";";
            return ret;
            
        }
    }
 }

