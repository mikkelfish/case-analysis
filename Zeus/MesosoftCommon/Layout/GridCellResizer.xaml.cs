﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.IO;
using Kennedy.ManagedHooks;
using MesosoftCommon.Adorners;

namespace MesosoftCommon.Layout
{
    /// <summary>
    /// Interaction logic for GridCellResizer.xaml
    /// </summary>
    public partial class GridCellResizer : UserControl, INotifyPropertyChanged
    {
        static public readonly RoutedEvent GridCellResizedEvent = 
            EventManager.RegisterRoutedEvent("GridCellResized", RoutingStrategy.Bubble, typeof(RoutedEventHandler),
            typeof(GridCellResizer));
        /// <summary>
        /// Occurs after the Grid Cell has been resized.
        /// </summary>
        public event RoutedEventHandler GridCellResized
        {
            add
            {
                AddHandler(GridCellResizedEvent, value);
            }
            remove
            {
                RemoveHandler(GridCellResizedEvent, value);
            }
        }

        static public readonly RoutedEvent DragBegunEvent =
            EventManager.RegisterRoutedEvent("DragBegun", RoutingStrategy.Bubble, typeof(RoutedEventHandler),
            typeof(GridCellResizer));
        /// <summary>
        /// Occurs when the Resizer is being dragged (when the mouse button is depressed over it).
        /// </summary>
        public event RoutedEventHandler DragBegun
        {
            add
            {
                AddHandler(DragBegunEvent, value);
            }
            remove
            {
                RemoveHandler(DragBegunEvent, value);
            }
        }

        static public readonly RoutedEvent DragEndedEvent =
            EventManager.RegisterRoutedEvent("DragEnded", RoutingStrategy.Bubble, typeof(RoutedEventHandler),
            typeof(GridCellResizer));
        /// <summary>
        /// Occurs when the Resizer is no longer being dragged, but before the Grid Cell is resized.
        /// </summary>
        public event RoutedEventHandler DragEnded
        {
            add
            {
                AddHandler(DragEndedEvent, value);
            }
            remove
            {
                RemoveHandler(DragEndedEvent, value);
            }
        }

        static private Color backgroundDefault = Color.FromRgb(229, 229, 215);
        static private SolidColorBrush hatchedBrush = new SolidColorBrush(Color.FromArgb(125, 29, 29, 15));
        private AdornerLayer hatchedLayer;

        private Point cursorOffsetFromTopLeft;

        private Cursor splitHorizontal = new Cursor(Directory.GetCurrentDirectory() + 
                                                "\\Layout\\Resources\\SplitHorizontal.cur");
        private Cursor splitVertical = new Cursor(Directory.GetCurrentDirectory() +
                                        "\\Layout\\Resources\\SplitVertical.cur");

        public enum CellType { Row, Column };
        public enum ResizeType { Split, Absolute };
        public enum ResizeSide { Left, Top, Right, Bottom, Null };

        public ResizeType TargetResizeType { get; set; }

        private ResizeSide resizeFromSide;
        public ResizeSide ResizeFromSide
        {
            get
            {
                return resizeFromSide;
            }
            set
            {
                resizeFromSide = value;
            }
        }

        private CellType targetCellType;
        public CellType TargetCellType
        {
            get
            {
                return targetCellType;
            }
            set
            {
                targetCellType = value;

                if (targetCellType == CellType.Column)
                    this.Cursor = this.splitVertical;
                else if (targetCellType == CellType.Row)
                    this.Cursor = this.splitHorizontal;
            }
        }

        public bool ResizerIsValid(ref string errorString)
        {
            if ((TargetCellType == CellType.Column && (ResizeFromSide == ResizeSide.Bottom || ResizeFromSide == ResizeSide.Top))
                 || (TargetCellType == CellType.Row && (ResizeFromSide == ResizeSide.Left || ResizeFromSide == ResizeSide.Right)))
            {
                errorString = "Cell type and Resize side mismatch : " + TargetCellType + " and " + ResizeFromSide + ".";
                return false;
            }
            return true;
        }

        public bool IsDragInProcess { get; set; }
     //   private MouseHook mouseHook;
        private Point originalCursorLocation;
        private Point currentCursorLocation;
        private double originalDefinitionSize;

        public ColumnDefinition TargetColumn { get; set; }
        public RowDefinition TargetRow { get; set; }

        public ColumnDefinition ComplementaryTargetColumn { get; set; }
        public RowDefinition ComplementaryTargetRow { get; set; }

        private Brush backgroundColor;
        public Brush BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                this.notifyPropertyChanged("BackgroundColor");
            }
        }

        public GridCellResizer()
        {
            InitializeComponent();

            BackgroundColor = new SolidColorBrush(GridCellResizer.backgroundDefault);
            IsDragInProcess = false;
            TargetResizeType = ResizeType.Split;
            //mouseHook = new MouseHook();
            //mouseHook.MouseEvent += new MouseHook.MouseEventHandler(mouseHook_MouseEvent);
            //Application.Current.Exit += new ExitEventHandler(Current_Exit);
        }

        static GridCellResizer()
        {
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void notifyPropertyChanged(string name)
        {
            PropertyChangedEventHandler handle = this.PropertyChanged;
            if (handle == null) return;
            handle(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        private Grid findParentGrid()
        {
            Grid ret;

            if (TargetCellType == CellType.Column && TargetColumn != null)
                ret = TargetColumn.Parent as Grid;
            else if (TargetCellType == CellType.Row && TargetRow != null)
                ret = TargetRow.Parent as Grid;
            else
                throw new Exception("GridCellResizer " + this.Name + " had a null target.");

            return ret;
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string errorstring = "";

            if (!ResizerIsValid(ref errorstring))
                throw new Exception(errorstring);

            IsDragInProcess = false;
            
            Grid parentGrid = findParentGrid();

            if (hatchedLayer == null)
                hatchedLayer = AdornerLayer.GetAdornerLayer(this);

            if (TargetCellType == CellType.Column && TargetColumn != null)
                originalDefinitionSize = TargetColumn.ActualWidth;
            else if (TargetCellType == CellType.Row && TargetRow != null)
                originalDefinitionSize = TargetRow.ActualHeight;
            else
                throw new Exception("GridCellResizer " + this.Name + " had a null target.");

            //Don't bother doing a null check here - if we've managed to find a Row or Columndefinition without a Grid parent, there are
            //much more serious problems afoot!
            originalCursorLocation = e.GetPosition(parentGrid);

            e.Handled = true;

            //mouseHook.InstallHook();
            //Update the hatchedrect's size here, in case the window was resized
            //hatchedRect.Height = this.ActualHeight;
            //hatchedRect.Width = this.ActualWidth;
            //gridHelper = GridLayoutHelper.LocateMouseOnGrid(parentGrid);
            
            cursorOffsetFromTopLeft = e.GetPosition(this);

            currentCursorLocation = e.GetPosition(parentGrid);
            Point cursorLocation = currentCursorLocation;
            cursorLocation.X -= cursorOffsetFromTopLeft.X;
            cursorLocation.Y -= cursorOffsetFromTopLeft.Y;

            HatchedOverlayAdorner hatchedOverlayAdorner = new HatchedOverlayAdorner(this);
            hatchedOverlayAdorner.UseRenderSize = true;
            hatchedLayer.Add(hatchedOverlayAdorner);

            hatchedOverlayAdorner.MouseUp += parentGrid_MouseUp;

            //Attach events to the grid that the element is being dragged in
            parentGrid.PreviewMouseMove += parentGrid_PreviewMouseMove;
            parentGrid.MouseUp += parentGrid_MouseUp;

            IsDragInProcess = true;

            RoutedEventArgs dragBegunEventArgs = new RoutedEventArgs(DragBegunEvent, this);
            RaiseEvent(dragBegunEventArgs);
        }

        private void parentGrid_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            //Correct for the situation where the grid is left and the mouse is released.
            if (IsDragInProcess && e.LeftButton == MouseButtonState.Released)
            {
                endDrag();
            }

            if (!IsDragInProcess) return;

            Grid parentGrid = findParentGrid();

            currentCursorLocation = e.GetPosition(parentGrid);
            Point newCursorLocation = currentCursorLocation;
            newCursorLocation.X -= cursorOffsetFromTopLeft.X;
            newCursorLocation.Y -= cursorOffsetFromTopLeft.Y;

            HatchedOverlayAdorner adorner = ((hatchedLayer.GetAdorners(this))[0] as HatchedOverlayAdorner);

            if (TargetCellType == CellType.Column)
            {
                newCursorLocation.Y = originalCursorLocation.Y - cursorOffsetFromTopLeft.Y;
                adorner.Offset = new Point(newCursorLocation.X - originalCursorLocation.X, 0);
            }
            else
            {
                newCursorLocation.X = originalCursorLocation.X - cursorOffsetFromTopLeft.X;
                adorner.Offset = new Point(0, newCursorLocation.Y - originalCursorLocation.Y);
            }

            adorner.InvalidateVisual();
        }

        private void endDrag()
        {
            IsDragInProcess = false;

            bool complementaryDefAltered = false;

            RoutedEventArgs dragEndedEventArgs = new RoutedEventArgs(DragEndedEvent, this);
            RaiseEvent(dragEndedEventArgs);

            Grid parentGrid = findParentGrid();

            HatchedOverlayAdorner adorner = ((hatchedLayer.GetAdorners(this))[0] as HatchedOverlayAdorner);
            hatchedLayer.Remove(adorner);

            parentGrid.PreviewMouseMove -= parentGrid_PreviewMouseMove;
            parentGrid.MouseUp -= parentGrid_MouseUp;

            if (TargetCellType == CellType.Column && TargetColumn != null)
            {
                double newWidth;
                double newComplementaryWidth;

                if (this.ResizeFromSide == ResizeSide.Left)
                {
                    if (ComplementaryTargetColumn == null && TargetResizeType == ResizeType.Split)
                    {
                        //Default to the column next to the target column (-1), throw an exception if this columndef doesn't exist
                        if (parentGrid.ColumnDefinitions.IndexOf(TargetColumn) - 1 < 0)
                            throw new Exception("GridCellResizer " + this.Name + " had a null complementary target and the "
                                    + "default could not be assigned.");

                        ComplementaryTargetColumn =
                            parentGrid.ColumnDefinitions[parentGrid.ColumnDefinitions.IndexOf(TargetColumn) - 1];
                    }

                    newWidth = TargetColumn.ActualWidth - (currentCursorLocation.X - originalCursorLocation.X);
                    newComplementaryWidth = ComplementaryTargetColumn.ActualWidth +
                            (currentCursorLocation.X - originalCursorLocation.X);
                }
                else if (this.ResizeFromSide == ResizeSide.Right)
                {
                    if (ComplementaryTargetColumn == null && TargetResizeType == ResizeType.Split)
                    {
                        //Default to the column next to the target column (+1), throw an exception if this columndef doesn't exist
                        if (parentGrid.ColumnDefinitions.IndexOf(TargetColumn) + 1 >= parentGrid.ColumnDefinitions.Count)
                            throw new Exception("GridCellResizer " + this.Name + " had a null complementary target and the "
                                    + "default could not be assigned.");

                        ComplementaryTargetColumn =
                            parentGrid.ColumnDefinitions[parentGrid.ColumnDefinitions.IndexOf(TargetColumn) + 1];
                    }

                    newWidth = TargetColumn.ActualWidth + (currentCursorLocation.X - originalCursorLocation.X);
                    newComplementaryWidth = ComplementaryTargetColumn.ActualWidth -
                            (currentCursorLocation.X - originalCursorLocation.X);
                }
                else
                    throw new Exception("Invalid TargetType/ResizeSide pair: " + TargetCellType.ToString() + " : " + ResizeFromSide.ToString());

                TargetColumn.Width = new GridLength(newWidth);
                //Hack to prevent overallocation of size when working with a Star complementary column
                TargetColumn.MaxWidth = newWidth;

                if (TargetResizeType == ResizeType.Split && !ComplementaryTargetColumn.Width.IsStar)
                {
                    ComplementaryTargetColumn.Width = new GridLength(newComplementaryWidth);
                    complementaryDefAltered = true;
                }
            }
            else if (TargetCellType == CellType.Row && TargetRow != null)
            {
                double newHeight;
                double newComplementaryHeight;

                if (this.ResizeFromSide == ResizeSide.Top)
                {
                    if (ComplementaryTargetRow == null && TargetResizeType == ResizeType.Split)
                    {
                        //Default to the row next to the target row (-1), throw an exception if this rowdef doesn't exist
                        if (parentGrid.RowDefinitions.IndexOf(TargetRow) - 1 < 0)
                            throw new Exception("GridCellResizer " + this.Name + " had a null complementary target and the "
                                    + "default could not be assigned.");

                        ComplementaryTargetRow =
                            parentGrid.RowDefinitions[parentGrid.RowDefinitions.IndexOf(TargetRow) - 1];
                    }

                    newHeight = TargetRow.ActualHeight - (currentCursorLocation.Y - originalCursorLocation.Y);
                    newComplementaryHeight = ComplementaryTargetRow.ActualHeight +
                            (currentCursorLocation.Y - originalCursorLocation.Y);
                }
                else if (this.ResizeFromSide == ResizeSide.Bottom)
                {
                    if (ComplementaryTargetRow == null && TargetResizeType == ResizeType.Split)
                    {
                        //Default to the row next to the target row (+1), throw an exception if this rowdef doesn't exist
                        if (parentGrid.RowDefinitions.IndexOf(TargetRow) + 1 >= parentGrid.RowDefinitions.Count)
                            throw new Exception("GridCellResizer " + this.Name + " had a null complementary target and the "
                                    + "default could not be assigned.");

                        ComplementaryTargetRow =
                            parentGrid.RowDefinitions[parentGrid.RowDefinitions.IndexOf(TargetRow) + 1];
                    }

                    newHeight = TargetRow.ActualHeight + (currentCursorLocation.Y - originalCursorLocation.Y);
                    newComplementaryHeight = ComplementaryTargetRow.ActualHeight -
                            (currentCursorLocation.Y - originalCursorLocation.Y);
                }
                else
                    throw new Exception("Invalid TargetType/ResizeSide pair: " + TargetCellType.ToString() + " : " + ResizeFromSide.ToString());

                TargetRow.Height = new GridLength(newHeight);
                //Hack to prevent overallocation of size when working with a Star complementary row
                TargetRow.MaxHeight = newHeight;

                if (TargetResizeType == ResizeType.Split && !ComplementaryTargetRow.Height.IsStar)
                {
                    ComplementaryTargetRow.Height = new GridLength(newComplementaryHeight);
                    complementaryDefAltered = true;
                }
            }
            else
                throw new Exception("GridCellResizer " + this.Name + " had a null target.");

            parentGrid.UpdateLayout();

            RoutedEventArgs gridCellResizedEventArgs = new RoutedEventArgs(GridCellResizedEvent, this);
            RaiseEvent(gridCellResizedEventArgs);
        }

        private void parentGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            endDrag();
        }

        //void globalHookMouseUp()
        //{

        //    //Uninstall the hook
        //    mouseHook.UninstallHook();
        //}

        //void mouseHook_MouseEvent(MouseEvents mEvent, System.Drawing.Point point)
        //{
        //    if (mEvent == MouseEvents.LeftButtonUp || mEvent == MouseEvents.RightButtonUp)
        //    {
        //        this.globalHookMouseUp();
        //    }
        //}

        //void Current_Exit(object sender, ExitEventArgs e)
        //{
        //    //Dispose the mouseHook
        //    if (mouseHook != null)
        //    {
        //        mouseHook.Dispose();
        //        mouseHook = null;
        //    }
        //}
    }
}
