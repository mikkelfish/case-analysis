﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.ComponentModel;
using MesosoftCommon.Utilities.Converters;

namespace MesosoftCommon.Layout.BasicPanels.SupportControls
{
    public enum LayoutLengthType
    {
        Pixel, Percent, Relative
    }

    public class MesoLayoutLength
    {
        public MesoLayoutDefinition ParentDefinition { get; set; }

        private double value;
        public double Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
                if (ParentDefinition != null)
                    ParentDefinition.InvalidateParentGridMetrics();
            }
        }

        private LayoutLengthType lengthType;
        public LayoutLengthType LengthType
        {
            get
            {
                return lengthType;
            }
            set
            {
                lengthType = value;
                if (ParentDefinition != null)
                    ParentDefinition.InvalidateParentGridMetrics();
            }
        }

        public bool IsRelative
        {
            get { return LengthType == LayoutLengthType.Relative; }
        }
        public bool IsPercent
        {
            get { return LengthType == LayoutLengthType.Percent; }
        }
        public bool IsPixel
        {
            get { return LengthType == LayoutLengthType.Pixel; }
        }
        public bool IsZero
        {
            get { return Value == 0; }
        }
        public bool IsNonAbsolute
        {
            get { return (IsRelative || IsPercent); }
        }

        public override bool Equals(object oCompare)
        {
            if (oCompare is MesoLayoutLength)
            {
                MesoLayoutLength length = (MesoLayoutLength)oCompare;
                if (this.LengthType == length.LengthType)
                    return this.Value == length.Value;
                return false;
            }
            return false;
        } 

        public static bool operator ==(MesoLayoutLength length1, MesoLayoutLength length2)
        {
            if ((object)length1 == null)
            {
                if ((object)length2 == null)
                    return true;
                return false;
            }
            return length1.Equals(length2);
        }

        public static bool operator !=(MesoLayoutLength length1, MesoLayoutLength length2)
        {
            if ((object)length1 == null)
            {
                if ((object)length2 == null)
                    return false;
                return true;
            }
            return !length1.Equals(length2);
        }

        public override int GetHashCode()
        {
            return (((int)this.Value) + (int)this.LengthType);
        }

        public MesoLayoutLength(double pixels)
        {
            this.Value = pixels;
            this.LengthType = LayoutLengthType.Pixel;
        }
        public MesoLayoutLength(double value, LayoutLengthType type)
        {
            this.Value = value;
            this.LengthType = type;
        }
    }

    public class MesoLayoutDefinition : FrameworkContentElement
    {
        public MesoLayoutDefinition()
        {
            this.Index = -1;
        }

        [TypeConverter(typeof(StringMesoLayoutLengthConverter))]
        public MesoLayoutLength Length
        {
            get
            {
                if ((MesoLayoutLength)GetValue(LengthProperty) == null)
                    return new MesoLayoutLength(0);
                return (MesoLayoutLength)GetValue(LengthProperty); 
            }
            set { SetValue(LengthProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Length.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LengthProperty =
            DependencyProperty.Register(
              "Length", typeof(MesoLayoutLength),
              typeof(MesoLayoutDefinition),
              new PropertyMetadata(new PropertyChangedCallback(MesoLayoutDefinition.onIsLengthDependencyPropertyChanged))
            );
        private static void onIsLengthDependencyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MesoLayoutDefinition self = d as MesoLayoutDefinition;
            self.InvalidateParentGridMetrics();

            MesoLayoutLength newLength = e.NewValue as MesoLayoutLength;
            newLength.ParentDefinition = self;
        }

        public void InvalidateParentGridMetrics()
        {
            MesoGrid parentGrid = this.Parent as MesoGrid;

            if (parentGrid != null)
                parentGrid.InvalidateMetrics();
        }

        private double actualLength;
        public double ActualLength
        {
            get
            {
                if ((this.InParentLogicalTree) && this.thisMetricIsValid())
                    return actualLength;
                return 0;
            }
            internal set
            {
                actualLength = value;
            }
        }

        private bool thisMetricIsValid()
        {
            if (((MesoGrid)this.Parent).metricsForContainingCollectionAreValid(this))
                return true;
            return false;
        }

        internal double minimumLength;

        public int Index { get; set; }

        public double Offset { get; internal set; }

        public bool InParentLogicalTree
        {
            get
            {
                return (this.Index != -1);
            }
        }
    }
}
