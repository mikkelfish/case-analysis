﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Controls;
using System.Windows;
using System.Reflection;
using System.Windows.Data;

namespace MesosoftCommon.Layout.BasicPanels.SupportControls
{
    public enum LayoutDefinitionType
    {
        Undefined,
        Row,
        Column
    }

    public class MesoLayoutDefinitionCollection : IList, IList<MesoLayoutDefinition>
    {
        internal struct Enumerator : IEnumerator<MesoLayoutDefinition>, IDisposable, IEnumerator
        {
            private MesoLayoutDefinitionCollection collection;
            private int index;
            private int version;
            private object currentElement;
            internal Enumerator(MesoLayoutDefinitionCollection collection)
            {
                this.collection = collection;
                this.index = -1;
                this.version = (this.collection != null) ? this.collection.version : -1;
                this.currentElement = collection;
            }

            public bool MoveNext()
            {
                if (this.collection != null)
                {
                    this.PrivateValidate();
                    if (this.index < (this.collection.Count - 1))
                    {
                        this.index++;
                        this.currentElement = this.collection[this.index];
                        return true;
                    }
                    this.currentElement = this.collection;
                    this.index = this.collection.Count;
                }
                return false;
            }

            object IEnumerator.Current
            {
                get
                {
                    if (this.currentElement != this.collection)
                    {
                        return this.currentElement;
                    }
                    if (this.index == -1)
                    {
                        throw new Exception("Enumerator not started.");
                    }
                    throw new Exception("Enumerator reached end.");
                }
            }
            public MesoLayoutDefinition Current
            {
                get
                {
                    if (this.currentElement != this.collection)
                    {
                        return (MesoLayoutDefinition)this.currentElement;
                    }
                    if (this.index == -1)
                    {
                        throw new Exception("Enumerator not started.");
                    }
                    throw new Exception("Enumerator reached end.");
                }
            }
            public void Reset()
            {
                if (this.collection != null)
                {
                    this.PrivateValidate();
                    this.currentElement = this.collection;
                    this.index = -1;
                }
            }

            public void Dispose()
            {
                this.currentElement = null;
            }

            private void PrivateValidate()
            {
                if (this.currentElement == null)
                {
                    throw new Exception("Enumerator collection is disposed.");
                }
                if (this.version != this.collection.version)
                {
                    throw new Exception("Enumerator version changed.");
                }
            }
        }

        private MesoGrid owner;
        protected MesoGrid Owner
        {
            get { return owner; }
        }
        private List<MesoLayoutDefinition> internalList = new List<MesoLayoutDefinition>();
        private bool metricsAreValid;
        public bool MetricsAreValid
        {
            get { return metricsAreValid; }
        }

        private int version;

        public LayoutDefinitionType DefinitionType { get; set; }
        internal bool subMetricsAreValid;
        private double totalPercent;
        private double totalStar;

        public bool ContainsRelativeDefinitions
        {
            get
            {
                if (subMetricsAreValid)
                    return (totalStar > 0);

                foreach (MesoLayoutDefinition definition in internalList)
                {
                    if (definition.Length.LengthType == LayoutLengthType.Relative)
                        return true;
                }
                return false;
            }
        }

        

        public MesoLayoutDefinitionCollection(MesoGrid parent)
        {
            this.owner = parent;
            this.collectionModified();
        }

        /// <summary>
        /// Counts up the sub-metric values :
        /// totalPercent - Total value of all percentage values requested (may not exceed 100%)
        /// totalStar - Total value of all star values requested
        /// </summary>
        internal void calculateSubMetrics()
        {
            totalPercent = 0;
            totalStar = 0;
            for (int i = 0; i < internalList.Count; i++)
            {             
                MesoLayoutDefinition definition = internalList[i];
                if (definition.Length == null)
                    throw new Exception("Null length found in a MesoLayoutDefinition.");

                //BindingExpression ex = BindingOperations.GetBindingExpression(this.Owner, FrameworkElement.TagProperty);

                if (definition.Length.LengthType == LayoutLengthType.Percent)
                {
                    totalPercent += definition.Length.Value;
                }
                else if (definition.Length.LengthType == LayoutLengthType.Relative)
                {
                    totalStar += definition.Length.Value;
                }
            }

            if (totalPercent > 100)
            {
                throw new Exception("MesoGrid exception : Total percent requested over 100 during Minimum size calculations.");
            }

            subMetricsAreValid = true;
        }

        internal double calculateMinimumSize(double[] minimumSizes)
        {
            double ret = 0;
            double nonPercentTotal = 0;

            //(Pass 1 : accumulate relative metrics)
            if (!subMetricsAreValid)
                calculateSubMetrics();

            double starMultiplier = 0;
            double percentMultiplier = 0;

            //Pass 2 : identify minimum star and percentage unit multipliers
            for (int i = 0; i < internalList.Count; i++)
            {
                MesoLayoutDefinition definition = internalList[i];

                if (minimumSizes[i] == 0)
                    continue;

                if (definition.Length.LengthType == LayoutLengthType.Percent)
                {
                    //The multiplier for percentages simply defines the value of 1% in minimum terms.
                    double thisMultiplier = minimumSizes[i] / definition.Length.Value;
                    if (thisMultiplier > percentMultiplier)
                        percentMultiplier = thisMultiplier;
                }
                else
                {
                    if (definition.Length.LengthType == LayoutLengthType.Relative)
                    {
                        //The multiplier for relatives(star-values) defines the total minimum value for ALL relative types.
                        //(Note that "definition.Length.Value/totalStar" will be a fractional value unless there is only
                        //one relative type in the definition collection)
                        double thisMultiplier = minimumSizes[i] / (definition.Length.Value / totalStar);
                        if (thisMultiplier > starMultiplier)
                            starMultiplier = thisMultiplier;
                    }
                    else
                        nonPercentTotal += minimumSizes[i];
                }
            }

            //The total minimum across all star values is defined by the starMultiplier, so add it into the nonPercentTotal.
            nonPercentTotal += starMultiplier;

            //Check the percentMultiplier against the implied multiplier from the non-percent minimums, move it up if necessary
            double impliedMultiplier = nonPercentTotal / (100 - totalPercent );
            if (impliedMultiplier > percentMultiplier)
                percentMultiplier = impliedMultiplier;

            //Pass 3 : count up the minimums to find a return value
            for (int i = 0; i < internalList.Count; i++)
            {
                MesoLayoutDefinition definition = internalList[i];

                if (definition.Length.LengthType == LayoutLengthType.Pixel)
                {
                    //Pixel types do not have non-explicit minimums.
                    ret += minimumSizes[i];
                    definition.minimumLength = minimumSizes[i];
                }
                else if (definition.Length.LengthType == LayoutLengthType.Percent)
                {
                    // value is in percent (34 = 34%, etc) and percentMultiplier is in length per 1%
                    ret += definition.Length.Value * percentMultiplier;
                    definition.minimumLength = definition.Length.Value * percentMultiplier;
                }
                else if (definition.Length.LengthType == LayoutLengthType.Relative)
                {
                    // value is in number of parts of the whole (totalStar), starMultiplier is the value of totalStar/totalStar (1)
                    ret += (definition.Length.Value/totalStar) * starMultiplier;
                    definition.minimumLength = (definition.Length.Value / totalStar) * starMultiplier;
                }
            }
            return ret;
        }

        internal void updateMetrics(double totalSize, bool safeMode)
        {
            double[] definitionSizes = new double[internalList.Count];
            double runningSize = 0;
            double minimumSizeTotal = 0;
            double adjustedStaticSizeTotal = 0;
            double requestedPercentTotal = 0;
            double minimumPercentTotal = 0;
            double requestedPixelTotal = 0;
            double minimumPixelTotal = 0;
            double minimumRelativeTotal = 0;
            int minimumPixelCount = 0;
            int minimumPercentCount = 0;

            //(Pass 1 : accumulate relative metrics)
            if (!subMetricsAreValid)
                calculateSubMetrics();            

            //Pass 2 : count up the minimum request, and the minimum-adjusted static request numbers
            for (int i = 0; i < internalList.Count; i++)
            {
                MesoLayoutDefinition definition = internalList[i];

                minimumSizeTotal += definition.minimumLength;

                if (definition.Length.LengthType == LayoutLengthType.Pixel)
                {
                    if (definition.Length.Value > definition.minimumLength)
                    {
                        requestedPixelTotal += definition.Length.Value;
                        adjustedStaticSizeTotal += definition.Length.Value;
                    }
                    else
                    {
                        requestedPixelTotal += definition.minimumLength;
                        adjustedStaticSizeTotal += definition.minimumLength;
                    }

                    minimumPixelTotal += definition.minimumLength;

                    if (definition.minimumLength > 0)
                        minimumPixelCount++;
                }
                else if (definition.Length.LengthType == LayoutLengthType.Percent)
                {
                    if (((definition.Length.Value / 100) * totalSize) > definition.minimumLength)
                        adjustedStaticSizeTotal += ((definition.Length.Value / 100) * totalSize);
                    else
                        adjustedStaticSizeTotal += definition.minimumLength;
                    requestedPercentTotal += ((definition.Length.Value / 100) * totalSize);
                    minimumPercentTotal += definition.minimumLength;

                    if (definition.minimumLength > 0)
                        minimumPercentCount++;
                }
                else if (definition.Length.LengthType == LayoutLengthType.Relative)
                {
                    //TODO : Fix this!
                    adjustedStaticSizeTotal += definition.minimumLength;
                    minimumRelativeTotal += definition.minimumLength;
                }
            }

            //minimumSizeTotal = (minimumPixelTotal + minimumPercentTotal);

            //Throw an exception if safeMode is off and not enough space has been provided to fulfill minimums.
            if (minimumSizeTotal > totalSize && !safeMode)
            {
                throw new Exception("MesoGrid exception : Grid alotted less than minimum required size.");
            }
            //If safe mode is on, degrade gracefully.
            else if (minimumSizeTotal > totalSize && safeMode)
            {
                throw new Exception("Help me, I'm melting, I'm meeeeelting!");
            }
            //If the minimum is fullfilled but space has to be reallocated because more space has been requested than is available...
            else if (adjustedStaticSizeTotal > totalSize)
            {
                double spareSpaceAboveMinimum = totalSize - minimumSizeTotal;

                resolvePercentages(ref spareSpaceAboveMinimum, requestedPercentTotal, minimumPercentTotal,totalSize, minimumPercentCount);
                resolvePixels(ref spareSpaceAboveMinimum, requestedPixelTotal, minimumPixelTotal, minimumPixelCount);             
            }
            //Finally, handle the case where more than enough space has been allocated
            else if (adjustedStaticSizeTotal <= totalSize)
            {
                for (int i = 0; i < internalList.Count; i++)
                {
                    MesoLayoutDefinition definition = internalList[i];

                    if (definition.Length.LengthType == LayoutLengthType.Percent)
                        definition.ActualLength = ((definition.Length.Value / 100) * totalSize);
                    else if (definition.Length.LengthType == LayoutLengthType.Pixel)
                        definition.ActualLength = definition.Length.Value;
                    else if (definition.Length.LengthType == LayoutLengthType.Relative)
                    {
                        //Tack on the minimumLength because minLength is taken into account inside of the adjustedStaticSizeTotal!
                        definition.ActualLength = ((definition.Length.Value / totalStar) * (totalSize - adjustedStaticSizeTotal)) + definition.minimumLength;
                    }
                }
            }

            this.metricsAreValid = true;

            //Run down the line and arrange the offset values
            for (int i = 0; i < internalList.Count; i++)
            {
                MesoLayoutDefinition definition = internalList[i];

                definition.Offset = runningSize;
                runningSize += definition.ActualLength;
            }
        }

        private void resolvePercentages(ref double spareSpaceAboveMinimum, double requestedPercentTotal, double minimumPercentTotal,
                                        double totalSize, int minimumPercentCount)
        {
            if (spareSpaceAboveMinimum <= 0)
                return;

            if (spareSpaceAboveMinimum >= (requestedPercentTotal - minimumPercentTotal))
            {
                spareSpaceAboveMinimum -= (requestedPercentTotal - minimumPercentTotal);
                for (int i = 0; i < internalList.Count; i++)
                {
                    MesoLayoutDefinition definition = internalList[i];

                    if (definition.Length.LengthType == LayoutLengthType.Percent)
                        definition.ActualLength = ((definition.Length.Value / 100) * totalSize);
                }
            }
            else
            {
                for (int i = 0; i < internalList.Count; i++)
                {
                    MesoLayoutDefinition definition = internalList[i];

                    if (definition.Length.LengthType == LayoutLengthType.Percent)
                        definition.ActualLength = definition.minimumLength + ((definition.Length.Value / 100) * spareSpaceAboveMinimum);
                }

                spareSpaceAboveMinimum = 0;
            }
        }

        private void resolvePixels(ref double spareSpaceAboveMinimum, double requestedPixelTotal, double minimumPixelTotal, int minimumPixelCount)
        {
            if (spareSpaceAboveMinimum <= 0)
                return;

            if (spareSpaceAboveMinimum >= (requestedPixelTotal - minimumPixelTotal))
            {
                spareSpaceAboveMinimum -= (requestedPixelTotal - minimumPixelTotal);
                for (int i = 0; i < internalList.Count; i++)
                {
                    MesoLayoutDefinition definition = internalList[i];

                    if (definition.Length.LengthType == LayoutLengthType.Pixel)
                        definition.ActualLength = definition.Length.Value;
                }
            }
            else
            {
                for (int i = 0; i < internalList.Count; i++)
                {
                    MesoLayoutDefinition definition = internalList[i];

                    if (definition.Length.LengthType == LayoutLengthType.Pixel)
                    {
                        definition.ActualLength = (definition.minimumLength == 0 ?
                            ((definition.Length.Value / (requestedPixelTotal - minimumPixelTotal)) * spareSpaceAboveMinimum) :
                            definition.minimumLength);
                    }
                }
                spareSpaceAboveMinimum = 0;
            }
        }

        public double GetLengthOfSpan(int index, int span)
        {
            //This indicates a bad span request
            if (index + span > internalList.Count)
            {
                throw new Exception("Bad span request!  A span was requested for index " + index + " with a span of " + span +
                    " when the definition collection only has " + internalList.Count + " elements!");
            }

            //Time for a little optimization (woo!)  We can use the offsets for everything but the last index in the definition collection,
            //and this is faster than iterating across what could theoretically be hundreds of indexes.
            if (index + span == internalList.Count)
            {
                return (internalList[index + span - 1].Offset - internalList[index].Offset + internalList[index + span - 1].ActualLength);
            }
            else
            {
                return (internalList[index + span].Offset - internalList[index].Offset);
            }
        }

        public double TotalStaticSize
        {
            get
            {
                if (!this.MetricsAreValid)
                    return 0;

                double ret = 0;

                foreach (MesoLayoutDefinition definition in internalList)
                {
                    if (definition.Length.LengthType != LayoutLengthType.Relative)
                        ret += definition.ActualLength;
                }

                return ret;
            }
        }

        public void InvalidateMetrics()
        {
            this.metricsAreValid = false;
            this.subMetricsAreValid = false;
        }

        protected void collectionModified()
        {
            this.InvalidateMetrics();
            this.version++;
        }

        private void verifyWriteAccess()
        {
            if (this.IsReadOnly)
                throw new Exception("Attempt to write to a ReadOnly MesoLayoutDefinitionCollection.");
        }

        private void validateValueForAddition(object value)
        {
            MesoLayoutDefinition def = definitionFromObject(value);
            if (def.Parent != null)
            {
                throw new Exception("Attempted to add a definition that is already a child of another collection.");
            }
        }

        private void validateValueForRemoval(object value)
        {
            MesoLayoutDefinition def = definitionFromObject(value);
            if (def.Parent != this.Owner)
            {
                throw new Exception("Attempted to remove a definition that isn't owned by this grid.");
            }
        }

        private void connectChildToOwner(MesoLayoutDefinition child, int index)
        {
            child.Index = index;
            MethodInfo mi = (typeof(MesoGrid)).GetMethod("AddLogicalChild", BindingFlags.NonPublic|BindingFlags.Instance);
            mi.Invoke(Owner, new object[] { child });
        }

        private void disconnectChildFromOwner(MesoLayoutDefinition child)
        {
            child.Index = -1;
            MethodInfo mi = (typeof(MesoGrid)).GetMethod("RemoveLogicalChild", BindingFlags.NonPublic | BindingFlags.Instance);
            mi.Invoke(Owner, new object[] { child });            
        }

        private MesoLayoutDefinition definitionFromObject(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            MesoLayoutDefinition def = value as MesoLayoutDefinition;

            if (def == null)
                throw new Exception("Null value or wrong type.");
            return def;
        }

        #region IList Members

        public int Add(object value)
        {
            this.Add(value as MesoLayoutDefinition);

            return internalList.Count - 1;
        }

        public void Clear()
        {
            foreach (MesoLayoutDefinition def in internalList)
            {
                this.disconnectChildFromOwner(def);
            }
            internalList.Clear();
            this.collectionModified();
        }

        public bool Contains(object value)
        {
            MesoLayoutDefinition def = this.definitionFromObject(value);

            return internalList.Contains(def);
        }

        public int IndexOf(object value)
        {
            MesoLayoutDefinition def = this.definitionFromObject(value);

            return internalList.IndexOf(def);
        }

        public void Insert(int index, object value)
        {
            this.Insert(index, (value as MesoLayoutDefinition));
        }

        public bool IsFixedSize
        {
            get { return this.IsReadOnly; }
        }

        public bool IsReadOnly
        {
            get 
            {
                if (!this.owner.MeasureOverrideInProgress)
                    return this.owner.ArrangeOverrideInProgress;
                return true;
            }
        }

        public void Remove(object value)
        {
            this.validateValueForRemoval(value);
            this.disconnectChildFromOwner(value as MesoLayoutDefinition);
            internalList.Remove(value as MesoLayoutDefinition);
            this.collectionModified();
        }

        public void RemoveAt(int index)
        {
            this.validateValueForRemoval(internalList[index]);
            this.disconnectChildFromOwner(internalList[index]);
            internalList.RemoveAt(index);
            this.collectionModified();
        }

        object IList.this[int index]
        {
            get
            {
                //if (index < 0 || index >= internalList.Count)
                //    throw new Exception("Index out of range.");

                //return internalList[index];
                return this[index];
            }
            set
            {
                //this.verifyWriteAccess();
                //this.validateValueForAddition(value);
                //if (index < 0 || index >= internalList.Count)
                //    throw new Exception("Index out of range.");

                //this.disconnectChildFromOwner(internalList[index]);
                //this.connectChildToOwner(value as MesoLayoutDefinition,index);
                //internalList[index] = value as MesoLayoutDefinition;
                //this.collectionModified();
                this[index] = (MesoLayoutDefinition)value;
            }
        }

        #endregion

        #region ICollection Members

        public void CopyTo(Array array, int index)
        {
            MesoLayoutDefinition[] def = array as MesoLayoutDefinition[];

            if (def == null)
                throw new Exception("Null value or wrong type.");

            internalList.CopyTo(def, index);
            this.collectionModified();
        }

        public int Count
        {
            get { return internalList.Count; }
        }

        public bool IsSynchronized
        {
            //Not threadsafe!
            get { return false; }
        }

        public object SyncRoot
        {
            get { return this; }
        }

        #endregion

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        #endregion

        #region IList<MesoLayoutDefinition> Members

        public int IndexOf(MesoLayoutDefinition item)
        {
            if ((item != null) && (item.Parent == this.Owner))
            {
                return item.Index;
            }
            return -1;
        }

        public void Insert(int index, MesoLayoutDefinition item)
        {
            this.verifyWriteAccess();
            if (index < 0 || index > internalList.Count)
                throw new Exception("Index out of range.");

            this.validateValueForAddition(item);
            internalList.Insert(index, item);
            this.connectChildToOwner(item,index);
            this.collectionModified();
        }

        public MesoLayoutDefinition this[int index]
        {
            get
            {
                if (index < 0 || index >= internalList.Count)
                    throw new Exception("Index out of range.");

                return internalList[index];
            }
            set
            {
                this.verifyWriteAccess();
                this.validateValueForAddition(value);
                if (index < 0 || index >= internalList.Count)
                    throw new Exception("Index out of range.");

                this.disconnectChildFromOwner(internalList[index]);
                this.connectChildToOwner(value,index);
                internalList[index] = value;
                this.collectionModified();
            }
        }

        #endregion

        #region ICollection<MesoLayoutDefinition> Members

        public void Add(MesoLayoutDefinition item)
        {
            this.verifyWriteAccess();
            this.validateValueForAddition(item);
            internalList.Add(item);
            this.connectChildToOwner(item,internalList.Count - 1);
            this.collectionModified();           
        }

        public bool Contains(MesoLayoutDefinition item)
        {
            return internalList.Contains(item);
        }

        public void CopyTo(MesoLayoutDefinition[] array, int arrayIndex)
        {
            internalList.CopyTo(array, arrayIndex);
            this.collectionModified();
        }

        public bool Remove(MesoLayoutDefinition item)
        {
            this.validateValueForRemoval(item);
            this.disconnectChildFromOwner(item);
            internalList.Remove(item);
            this.collectionModified();
            return true;
        }

        #endregion

        #region IEnumerable<MesoLayoutDefinition> Members

        IEnumerator<MesoLayoutDefinition> IEnumerable<MesoLayoutDefinition>.GetEnumerator()
        {
            return new Enumerator(this);
        }

        #endregion
    }
}
