﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using MesosoftCommon.Layout.BasicPanels.SupportControls;
using System.Windows;
using System.Windows.Media;
using System.Collections;
using Kennedy.ManagedHooks;
using System.Windows.Input;
using System.Windows.Shapes;
using MesosoftCommon.Development;
using System.Windows.Data;
using System.ComponentModel;
using MesosoftCommon.Managers;
using System.Windows.Documents;
using MesosoftCommon.Adorners;

namespace MesosoftCommon.Layout.BasicPanels
{
    public enum DockSide
    {
        Top,
        Right,
        Bottom,
        Left
    }

    public enum DockAlignment
    {
        Near,
        Center,
        Far
    }

    public class MesoGrid : Panel, INotifyPropertyChanged
    {
        // "Basic MesoGrid Components" denotes the methods and members that make up the basic MesoGrid.  MesoGrid
        // at its core mimics the standard Grid.
        #region Basic MesoGrid Components

        protected override IEnumerator LogicalChildren
        {
            get
            {
                List<object> toRet = new List<object>();
                IEnumerator e = base.LogicalChildren;
                e.Reset();
                while (e.MoveNext())
                {
                    toRet.Add(e.Current);
                }

                toRet.AddRange(this.rowDefinitions.ToArray());
                toRet.AddRange(this.columnDefinitions.ToArray());
                
                return toRet.GetEnumerator();
            }
        }

        public static int GetColumn(DependencyObject obj)
        {
            return (int)obj.GetValue(ColumnProperty);
        }
        public static void SetColumn(DependencyObject obj, int value)
        {
            obj.SetValue(ColumnProperty, value);
        }
        // Using a DependencyProperty as the backing store for Column.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ColumnProperty =
            DependencyProperty.RegisterAttached(
              "Column",
              typeof(int),
              typeof(MesoGrid),
              new UIPropertyMetadata(0, new PropertyChangedCallback(MesoGrid.onDefinitionAttachedPropertyChanged)),
              new ValidateValueCallback(MesoGrid.isIntValueNotNegative)
            );

        public static int GetRow(DependencyObject obj)
        {
            return (int)obj.GetValue(RowProperty);
        }
        public static void SetRow(DependencyObject obj, int value)
        {
            obj.SetValue(RowProperty, value);
        }
        // Using a DependencyProperty as the backing store for Row.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RowProperty =
            DependencyProperty.RegisterAttached(
              "Row",
              typeof(int),
              typeof(MesoGrid),
              new UIPropertyMetadata(0, new PropertyChangedCallback(MesoGrid.onDefinitionAttachedPropertyChanged)),
              new ValidateValueCallback(MesoGrid.isIntValueNotNegative)
            );

        public static int GetColumnSpan(DependencyObject obj)
        {
            return (int)obj.GetValue(ColumnSpanProperty);
        }
        public static void SetColumnSpan(DependencyObject obj, int value)
        {
            obj.SetValue(ColumnSpanProperty, value);
        }
        // Using a DependencyProperty as the backing store for ColumnSpan.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ColumnSpanProperty =
            DependencyProperty.RegisterAttached(
              "ColumnSpan",
              typeof(int),
              typeof(MesoGrid),
              new UIPropertyMetadata(1, new PropertyChangedCallback(MesoGrid.onDefinitionAttachedPropertyChanged)),
              new ValidateValueCallback(MesoGrid.isIntValueGreaterThanZero)
            );

        public static int GetRowSpan(DependencyObject obj)
        {
            return (int)obj.GetValue(RowSpanProperty);
        }
        public static void SetRowSpan(DependencyObject obj, int value)
        {
            obj.SetValue(RowSpanProperty, value);
        }
        // Using a DependencyProperty as the backing store for RowSpan.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RowSpanProperty =
            DependencyProperty.RegisterAttached(
              "RowSpan",
              typeof(int),
              typeof(MesoGrid),
              new UIPropertyMetadata(1, new PropertyChangedCallback(MesoGrid.onDefinitionAttachedPropertyChanged)),
              new ValidateValueCallback(MesoGrid.isIntValueGreaterThanZero)
            );

        //IsConstrained : this means the object is held within gridspace, not freespace.  Constrained objects may not anchor themselves
        //but can be used as anchors.
        public static bool GetIsConstrained(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsConstrainedProperty);
        }
        public static void SetIsConstrained(DependencyObject obj, bool value)
        {
            obj.SetValue(IsConstrainedProperty, value);
        }
        // Using a DependencyProperty as the backing store for IsConstrained.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsConstrainedProperty =
            DependencyProperty.RegisterAttached(
              "IsConstrained",
              typeof(bool),
              typeof(MesoGrid),
              new UIPropertyMetadata(true, new PropertyChangedCallback(MesoGrid.onDefinitionAttachedPropertyChanged))
            );

        private static void onDefinitionAttachedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Visual reference = d as Visual;
            if (reference != null)
            {
                MesoGrid parent = VisualTreeHelper.GetParent(reference) as MesoGrid;
                if (parent != null)
                {
                    //Don't invalidateMetrics here - row/col defs are not effected!
                    //parent.InvalidateMetrics();
                    parent.InvalidateMeasure();
                    parent.validateAttachedProperties(e);
                }
            }
        }

        private void validateAttachedProperties(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == MesoGrid.RowProperty)
            {
                if ((int)e.NewValue >= RowDefinitions.Count)
                    throw new Exception("Row attached property out of bounds.");
            }
            else if (e.Property == MesoGrid.ColumnProperty)
            {
                if ((int)e.NewValue >= ColumnDefinitions.Count)
                    throw new Exception("Column attached property out of bounds.");
            }
            else if (e.Property == MesoGrid.RowSpanProperty)
            {
                if ((int)e.NewValue > RowDefinitions.Count)
                    throw new Exception("RowSpan attached property out of bounds.");
            }
            else if (e.Property == MesoGrid.ColumnSpanProperty)
            {
                if ((int)e.NewValue > ColumnDefinitions.Count)
                    throw new Exception("ColumnSpan attached property out of bounds.");
            }
        }

        private static bool isIntValueGreaterThanZero(object value)
        {
            return (((int)value) > 0);
        }

        private static bool isIntValueNotNegative(object value)
        {
            return (((int)value) >= 0);
        }

        public static bool GetIsDraggable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsDraggableProperty);
        }
        public static void SetIsDraggable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsDraggableProperty, value);
        }
        // Using a DependencyProperty as the backing store for IsDraggable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsDraggableProperty =
            DependencyProperty.RegisterAttached(
                "IsDraggable",
                typeof(bool),
                typeof(MesoGrid),
                new UIPropertyMetadata(true)
            );

        //IsBackground : this means that the object is a "background" object and may not be moved, docked to, etc.  It should also always be below non-background
        //objects.
        //This is intended primarily for background images.
        public static bool GetIsBackground(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsBackgroundProperty);
        }
        public static void SetIsBackground(DependencyObject obj, bool value)
        {
            obj.SetValue(IsBackgroundProperty, value);
        }
        // Using a DependencyProperty as the backing store for IsBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsBackgroundProperty =
            DependencyProperty.RegisterAttached(
                "IsBackground",
                typeof(bool),
                typeof(MesoGrid),
                new UIPropertyMetadata(false,new PropertyChangedCallback(MesoGrid.onIsBackgroundAttachedPropertyChanged))
            );

        private static void onIsBackgroundAttachedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            
        }

        private MesoLayoutDefinitionCollection rowDefinitions;
        public MesoLayoutDefinitionCollection RowDefinitions
        {
            get
            {
                return rowDefinitions;
            }
        }

        private MesoLayoutDefinitionCollection columnDefinitions;
        public MesoLayoutDefinitionCollection ColumnDefinitions
        {
            get
            {
                return columnDefinitions;
            }
        }

        public bool SafeMode { get; set; }

        public override void EndInit()
        {
            //Here we have to cover "default" row/col defs.  If a defCollection has 0 elements, this really means it has one
            //element, set to "*".
            if (rowDefinitions.Count == 0)
            {
                MesoLayoutDefinition def = new MesoLayoutDefinition();
                def.Length = new MesoLayoutLength(1, LayoutLengthType.Relative);
                def.Offset = 0.0;

                rowDefinitions.Add(def);
            }

            if (columnDefinitions.Count == 0)
            {
                MesoLayoutDefinition def = new MesoLayoutDefinition();
                def.Length = new MesoLayoutLength(1, LayoutLengthType.Relative);
                def.Offset = 0.0;

                columnDefinitions.Add(def);
            }

            base.EndInit();
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            this.InvalidateMetrics();
            base.OnRenderSizeChanged(sizeInfo);
        }

        public bool RowsAreValid
        {
            get
            {
                return this.rowDefinitions.MetricsAreValid;
            }
        }
        
        public bool ColumnsAreValid
        {
            get
            {
                return this.columnDefinitions.MetricsAreValid;
            }
        }

        public bool MetricsInvalid
        {
            get
            {
                if (!this.ColumnsAreValid || !this.RowsAreValid)
                    return true;
                return false;
            }
        }

        internal bool metricsForContainingCollectionAreValid(object obj)
        {
            if (this.columnDefinitions.Contains(obj))
                return this.columnDefinitions.MetricsAreValid;
            else if (this.rowDefinitions.Contains(obj))
                return this.rowDefinitions.MetricsAreValid;

            return false;
        }

        private bool measureOverrideInProgress;
        public bool MeasureOverrideInProgress
        {
            get
            {
                return measureOverrideInProgress;
            }
        }

        private bool arrangeOverrideInProgress;
        public bool ArrangeOverrideInProgress
        {
            get
            {
                return arrangeOverrideInProgress;
            }
        }

        public void InvalidateRows()
        {
            this.rowDefinitions.InvalidateMetrics();
        }

        public void InvalidateColumns()
        {
            this.columnDefinitions.InvalidateMetrics();
        }

        public void InvalidateMetrics()
        {
            this.columnDefinitions.InvalidateMetrics();
            this.rowDefinitions.InvalidateMetrics();
        }

        internal void calculateMinimumSize()
        {
            double[] minRowSizes = new double[rowDefinitions.Count];
            double[] minColumnSizes = new double[columnDefinitions.Count];

            foreach (object child in this.Children)
            {
                //Do not process minimum sizes on objects that are not MesoGrids.
                if (!(child is MesoGrid))
                    continue;

                MesoGrid grid = (child as MesoGrid);

                //Do not process MesoGrids which are in the freespace coordinate system.
                if (!MesoGrid.GetIsConstrained(grid))
                    continue;

                grid.calculateMinimumSize();

                if (MesoGrid.GetRowSpan(grid) == 1 && minRowSizes[MesoGrid.GetRow(grid)] < grid.MinHeight)
                    minRowSizes[MesoGrid.GetRow(grid)] = grid.MinHeight;

                if (MesoGrid.GetColumnSpan(grid) == 1 && minColumnSizes[MesoGrid.GetColumn(grid)] < grid.MinWidth)
                    minColumnSizes[MesoGrid.GetColumn(grid)] = grid.MinWidth;
            }

            double rowMinimum = rowDefinitions.calculateMinimumSize(minRowSizes);
            double columnMinimum = columnDefinitions.calculateMinimumSize(minColumnSizes);

            if (rowMinimum > MinHeight)
                MinHeight = rowMinimum;
            if (columnMinimum > MinWidth)
                MinWidth = columnMinimum;
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            try
            {
                this.arrangeOverrideInProgress = true;

                if (!RowsAreValid)
                    RowDefinitions.updateMetrics(arrangeSize.Height, false);

                if (!ColumnsAreValid)
                    ColumnDefinitions.updateMetrics(arrangeSize.Width, false);

                UIElementCollection internalChildren = base.InternalChildren;
                for (int i = 0; i < internalChildren.Count; i++)
                {
                    UIElement element = internalChildren[i];
                    //the element can't be null and has to be constrained to gridspace to be arranged in it.
                    if (element != null && MesoGrid.GetIsConstrained(element))
                    {
                        int columnIndex = MesoGrid.GetColumn(element);
                        int rowIndex = MesoGrid.GetRow(element);
                        int columnSpan = MesoGrid.GetColumnSpan(element);
                        int rowSpan = MesoGrid.GetRowSpan(element);
                        Rect arrangeRect = new Rect((columnIndex == 0) ? 0 : this.ColumnDefinitions[columnIndex].Offset,
                                                    (rowIndex == 0) ? 0 : this.RowDefinitions[rowIndex].Offset,
                                                    this.ColumnDefinitions.GetLengthOfSpan(columnIndex, columnSpan),
                                                    this.RowDefinitions.GetLengthOfSpan(rowIndex, rowSpan));
                        element.Arrange(arrangeRect);
                    }
                    //Nonconstrained elements that are not null are arranged in freespace.
                    //Docked elements ... (fill this in)
                    else if (element != null && !MesoGrid.GetIsConstrained(element))
                    {
                        UIElement dockingTarget;
                        String dockingTargetName = MesoGrid.GetDockTarget(element);
                        if (dockingTargetName == "")
                            dockingTarget = null;
                        else
                        {
                            DependencyObject dockingTargetObj = (LogicalTreeHelper.FindLogicalNode(this, dockingTargetName) as UIElement);
                            if (dockingTargetObj is UIElement)
                                dockingTarget = dockingTargetObj as UIElement;
                            else
                                dockingTarget = null;
                        }
                         
                        if (dockingTarget == null)
                        {
                            Rect arrangeRect = new Rect(MesoGrid.GetXCoordinate(element), MesoGrid.GetYCoordinate(element),
                                                        element.DesiredSize.Width, element.DesiredSize.Height);
                            element.Arrange(arrangeRect);
                        }
                        else
                        {
                            if (!this.InternalChildren.Contains(dockingTarget))
                                throw new Exception("Attempted to dock a child element at index " + i + " to an element that does"
                                                  + " not belong to the parent MesoGrid, " + this.Name + ".");

                            Vector targetOffset = VisualTreeHelper.GetOffset(dockingTarget);
                            Size targetSize = dockingTarget.RenderSize;
                            Rect arrangeRect = new Rect(0,0,element.DesiredSize.Width,element.DesiredSize.Height);

                            switch (MesoGrid.GetDockSide(element))
                            {
                                case DockSide.Top:
                                    arrangeRect.Y = targetOffset.Y - element.DesiredSize.Height;
                                    if (MesoGrid.GetDockAlignment(element) == DockAlignment.Near)
                                        arrangeRect.X = targetOffset.X;
                                    else if (MesoGrid.GetDockAlignment(element) == DockAlignment.Center)
                                        arrangeRect.X = targetOffset.X + ((targetSize.Width - element.DesiredSize.Width) / 2);
                                    else if (MesoGrid.GetDockAlignment(element) == DockAlignment.Far)
                                        arrangeRect.X = targetOffset.X + (targetSize.Width - element.DesiredSize.Width);
                                    break;

                                case DockSide.Right:
                                    arrangeRect.X = targetOffset.X + targetSize.Width;
                                    
                                    if (MesoGrid.GetDockAlignment(element) == DockAlignment.Near)
                                        arrangeRect.Y = targetOffset.Y;
                                    else if (MesoGrid.GetDockAlignment(element) == DockAlignment.Center)
                                        arrangeRect.Y = targetOffset.Y + ((targetSize.Height - element.DesiredSize.Height) / 2);
                                    else if (MesoGrid.GetDockAlignment(element) == DockAlignment.Far)
                                        arrangeRect.Y = targetOffset.Y + (targetSize.Height - element.DesiredSize.Height);
                                    break;

                                case DockSide.Bottom:
                                    arrangeRect.Y = targetOffset.Y + targetSize.Height;
                                    if (MesoGrid.GetDockAlignment(element) == DockAlignment.Near)
                                        arrangeRect.X = targetOffset.X;
                                    else if (MesoGrid.GetDockAlignment(element) == DockAlignment.Center)
                                        arrangeRect.X = targetOffset.X + ((targetSize.Width - element.DesiredSize.Width) / 2);
                                    else if (MesoGrid.GetDockAlignment(element) == DockAlignment.Far)
                                        arrangeRect.X = targetOffset.X + (targetSize.Width - element.DesiredSize.Width);
                                    break;

                                case DockSide.Left:
                                    arrangeRect.X = targetOffset.X - element.DesiredSize.Width;
                                    
                                    
                                    if (MesoGrid.GetDockAlignment(element) == DockAlignment.Near)
                                        arrangeRect.Y = targetOffset.Y;
                                    else if (MesoGrid.GetDockAlignment(element) == DockAlignment.Center)
                                        arrangeRect.Y = targetOffset.Y + ((targetSize.Height - element.DesiredSize.Height) / 2);
                                    else if (MesoGrid.GetDockAlignment(element) == DockAlignment.Far)
                                        arrangeRect.Y = targetOffset.Y + (targetSize.Height - element.DesiredSize.Height);
                                    break;

                                default:
                                    break;
                            }

                            arrangeRect.X += MesoGrid.GetXCoordinate(element);
                            arrangeRect.Y += MesoGrid.GetYCoordinate(element);

                            element.Arrange(arrangeRect);
                        }
                    }
                }
            }
            finally
            {   
                this.arrangeOverrideInProgress = false;       
            }
            return arrangeSize;
        }

        protected override Size MeasureOverride(Size constraint)
        {
            Size ret = new Size(0,0);
            try
            {
                this.measureOverrideInProgress = true;

                if (MetricsInvalid)
                    this.calculateMinimumSize();

                if (!RowsAreValid)
                    RowDefinitions.updateMetrics(constraint.Height, false);

                if (!ColumnsAreValid)
                    ColumnDefinitions.updateMetrics(constraint.Width, false);

                if (double.IsPositiveInfinity(constraint.Width) && ColumnDefinitions.ContainsRelativeDefinitions)
                {
                    //Don't handle non-constrained measures with relative definitions
                    throw new Exception("Constraint of PositiveInfinity is not supported for measures on MesoGrids containing relative definitions.");
                }
                else if (ColumnDefinitions.ContainsRelativeDefinitions)
                {
                    //the grid contains relative definitions so request back exactly the constraint
                    ret.Width = constraint.Width;
                }
                else
                {
                    //no relative definitions to soak up excess space, request only the static values.
                    ret.Width = ColumnDefinitions.TotalStaticSize;
                }

                if (double.IsPositiveInfinity(constraint.Height) && ColumnDefinitions.ContainsRelativeDefinitions)
                {
                    //Don't handle non-constrained measures with relative definitions
                    throw new Exception("Constraint of PositiveInfinity is not supported for measures on MesoGrids containing relative definitions.");
                }
                else if (RowDefinitions.ContainsRelativeDefinitions)
                {
                    //the grid contains relative definitions so request back exactly the constraint
                    ret.Height = constraint.Height;
                }
                else
                {
                    //no relative definitions to soak up excess space, request only the static values.
                    ret.Height = RowDefinitions.TotalStaticSize;
                }

                //Last we have to call measure on all children, constrained by their row and column.
                UIElementCollection internalChildren = base.InternalChildren;
                for (int i = 0; i < internalChildren.Count; i++)
                {
                    UIElement element = internalChildren[i];
                    //the element can't be null and has to be constrained to gridspace to be measured in it.
                    if (element != null && MesoGrid.GetIsConstrained(element))
                    {
                        int columnIndex = MesoGrid.GetColumn(element);
                        int rowIndex = MesoGrid.GetRow(element);
                        int columnSpan = MesoGrid.GetColumnSpan(element);
                        int rowSpan = MesoGrid.GetRowSpan(element);
                        Size measureSize = new Size(this.ColumnDefinitions.GetLengthOfSpan(columnIndex, columnSpan),
                                                    this.RowDefinitions.GetLengthOfSpan(rowIndex, rowSpan));
                        element.Measure(measureSize);
                    }
                    //Nonconstrainted elements that are not null are arranged in freespace.
                    //Docked elements ... (fill this in)
                    else if (element != null && !MesoGrid.GetIsConstrained(element))
                    {
                        element.Measure(constraint);
                    }
                }
            }
            finally
            {
                this.measureOverrideInProgress = false;
            }
            
            return ret;
        }

        protected override void OnVisualChildrenChanged(DependencyObject visualAdded, DependencyObject visualRemoved)
        {
            //Don't invalidateMetrics here - row/col defs are not effected!
            //this.InvalidateMetrics();
            base.OnVisualChildrenChanged(visualAdded, visualRemoved);

            if (visualAdded != null && visualAdded is FrameworkElement)
            {
                (visualAdded as FrameworkElement).PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(MesoGrid_PreviewMouseLeftButtonDown);
                (visualAdded as FrameworkElement).PreviewMouseMove += new System.Windows.Input.MouseEventHandler(MesoGrid_PreviewMouseMove);
            }
            else if (visualRemoved != null && visualRemoved is FrameworkElement)
            {
                (visualRemoved as FrameworkElement).PreviewMouseLeftButtonDown -= new System.Windows.Input.MouseButtonEventHandler(MesoGrid_PreviewMouseLeftButtonDown);
                (visualRemoved as FrameworkElement).PreviewMouseMove -= new System.Windows.Input.MouseEventHandler(MesoGrid_PreviewMouseMove);
            }
        }

        private static MesoGrid gridMouseIsOver;
        private static MesoGrid GridMouseIsOver
        {
            get
            {
                return gridMouseIsOver;
            }
            set
            {
                if (value == gridMouseIsOver) return;

                AdornerLayer al = null;

                if (gridMouseIsOver != null)
                {
                    al = AdornerLayer.GetAdornerLayer(gridMouseIsOver);
                }

                if (al != null)
                {
                    Adorner[] toRemoveArray = al.GetAdorners(gridMouseIsOver);
                    if (toRemoveArray != null)
                    {
                        foreach (Adorner toRemove in toRemoveArray)
                        {
                            if (toRemove is MesoGridIsLockedStateAdorner)
                                al.Remove(toRemove);
                        }
                    }
                }

                al = null;

                if (value != null)
                {
                    al = AdornerLayer.GetAdornerLayer(value);
                }
                if (al != null)
                    al.Add(new MesoGridIsLockedStateAdorner(value));
                

                gridMouseIsOver = value;
            }
        }

        void MesoGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if (!MesoGrid.IsLockingModeActive) return;

            if (e.OriginalSource is MesoGrid)
            {
                MesoGrid.GridMouseIsOver = e.OriginalSource as MesoGrid;
                e.Handled = true;
            }
            else
            {
                MesoGrid parent = (MesoGrid.FindParentManagedPanel(e.OriginalSource as DependencyObject) as MesoGrid);
                if (parent != null)
                {
                    MesoGrid.GridMouseIsOver = parent;
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// This method ONLY exists to clean up the adorner layer.  Don't put actual mouse click handling behavior here,
        /// that goes in PreviewMouseDown.
        /// </summary>
        /// <param name="sender">standard event sender</param>
        /// <param name="e">standard MouseButtonEventArgs</param>
        void MesoGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = false;

            if (!MesoGrid.IsLockingModeActive) return;

            AdornerLayer al = null;

            if (e.OriginalSource is MesoGrid)
            {
                (e.OriginalSource as MesoGrid).IsLocked = !(e.OriginalSource as MesoGrid).IsLocked;
                al = AdornerLayer.GetAdornerLayer(e.OriginalSource as MesoGrid);
                e.Handled = true;
            }
            else
            {
                MesoGrid parent = (MesoGrid.FindParentManagedPanel(e.OriginalSource as DependencyObject) as MesoGrid);
                if (parent != null)
                {
                    parent.IsLocked = !parent.IsLocked;
                    al = AdornerLayer.GetAdornerLayer(parent);
                    e.Handled = true;
                }
            }

            if (al != null)
                al.Update();
        }

        #endregion

        // "MesoGrid Freespace Components" denotes the methods and members that add the Freespace (non-constrained) layer
        // to the MesoGrid.  Objects within Freespace may be docked to other objects.
        #region MesoGrid Freespace Components

        public static double GetXCoordinate(DependencyObject obj)
        {
            return (double)obj.GetValue(XCoordinateProperty);
        }
        public static void SetXCoordinate(DependencyObject obj, double value)
        {
            obj.SetValue(XCoordinateProperty, value);
        }
        // Using a DependencyProperty as the backing store for XCoordinate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty XCoordinateProperty =
            DependencyProperty.RegisterAttached(
              "XCoordinate",
              typeof(double),
              typeof(MesoGrid),
              new UIPropertyMetadata(0.0)
            );

        public static double GetYCoordinate(DependencyObject obj)
        {
            return (double)obj.GetValue(YCoordinateProperty);
        }
        public static void SetYCoordinate(DependencyObject obj, double value)
        {
            obj.SetValue(YCoordinateProperty, value);
        }
        // Using a DependencyProperty as the backing store for YCoordinate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty YCoordinateProperty =
            DependencyProperty.RegisterAttached(
              "YCoordinate",
              typeof(double),
              typeof(MesoGrid),
              new UIPropertyMetadata(0.0)
            );

        public static void ChangeConstraintOfLastElementClicked()
        {
            MesoGrid lastGridClickedIn = MesoGrid.FindParentManagedPanel(MesoGrid.lastElementClicked) as MesoGrid;
            if (lastGridClickedIn == null) return;
            if (lastGridClickedIn.IsLocked) return;
            if (MesoGrid.lastElementClicked == null) return;

            if (MesoGrid.GetIsConstrained(MesoGrid.lastElementClicked))
            {
                MesoLayoutDefinition hostRow = lastGridClickedIn.RowDefinitions[MesoGrid.GetRow(MesoGrid.lastElementClicked)];
                MesoLayoutDefinition hostColumn = lastGridClickedIn.ColumnDefinitions[MesoGrid.GetColumn(MesoGrid.lastElementClicked)];

                Point pointFromMargin = MarginHelper.GetPointFromMargin(MesoGrid.lastElementClicked, hostRow.ActualLength, hostColumn.ActualLength);

                MesoGrid.SetXCoordinate(MesoGrid.lastElementClicked, hostColumn.Offset + pointFromMargin.X);
                MesoGrid.SetYCoordinate(MesoGrid.lastElementClicked, hostRow.Offset + pointFromMargin.Y);
                //MesoGrid.SetXCoordinate(MesoGrid.lastElementClicked, hostColumn.Offset + MesoGrid.lastElementClicked.Margin.Left);
                //MesoGrid.SetYCoordinate(MesoGrid.lastElementClicked, hostRow.Offset + MesoGrid.lastElementClicked.Margin.Top);
                MesoGrid.lastElementClicked.Margin = new Thickness(0);
                MesoGrid.SetIsConstrained(MesoGrid.lastElementClicked, false);
            }
            else
            {
                //GridLayoutHelper glh = GridLayoutHelper.LocateMouseOnGrid(lastGridClickedIn);
                //glh.PositionElementWithinGrid(MesoGrid.lastElementClicked, new Point(MesoGrid.GetXCoordinate(MesoGrid.lastElementClicked),
                //                                                                     MesoGrid.GetYCoordinate(MesoGrid.lastElementClicked)),
                //                              lastGridClickedIn.IsBoundaryEnforced, MesoGrid.lastElementClicked.HorizontalAlignment,
                //                              MesoGrid.lastElementClicked.VerticalAlignment, true);
                //MesoGrid.SetIsConstrained(MesoGrid.lastElementClicked, true);
                ////handling for a docked element - detach its docking
                //MesoGrid.SetDockTarget(MesoGrid.lastElementClicked, "");
            }

            lastGridClickedIn.InvalidateArrange();
            lastGridClickedIn.UpdateLayout();
        }

        #endregion

        // "MesoGrid Dragging Components" denotes the methods and members that add object dragging to the MesoGrid.
        #region MesoGrid Dragging Components

        public static readonly RoutedEvent DraggingStartedEvent = EventManager.RegisterRoutedEvent(
            "DraggingStarted", RoutingStrategy.Bubble, typeof(DraggingRoutedEventHandler), typeof(MesoGrid));
        public event DraggingRoutedEventHandler DraggingStarted
        {
            add { AddHandler(DraggingStartedEvent, value); }
            remove { RemoveHandler(DraggingStartedEvent, value); }
        }
        protected virtual void RaiseDraggingStartedEvent()
        {
            DraggingRoutedEventArgs args = new DraggingRoutedEventArgs(DraggingStartedEvent,
                                                            MesoGrid.elementBeingDragged, MesoGrid.GetIsConstrained(MesoGrid.elementBeingDragged),
                                                            MesoGrid.GetXCoordinate(MesoGrid.elementBeingDragged), MesoGrid.GetYCoordinate(MesoGrid.elementBeingDragged),
                                                            MesoGrid.GetRow(MesoGrid.elementBeingDragged), MesoGrid.GetColumn(MesoGrid.elementBeingDragged),
                                                            MesoGrid.elementBeingDragged.Margin, MesoGrid.IsDragModeActive, MesoGrid.IsResizeModeActive);
            RaiseEvent(args);
        }


        public static readonly RoutedEvent DraggingEndedEvent = EventManager.RegisterRoutedEvent(
            "DraggingEnded", RoutingStrategy.Bubble, typeof(DraggingRoutedEventHandler), typeof(MesoGrid));
        public event DraggingRoutedEventHandler DraggingEnded
        {
            add { AddHandler(DraggingEndedEvent, value); }
            remove { RemoveHandler(DraggingEndedEvent, value); }
        }
        protected virtual void RaiseDraggingEndedEvent()
        {
            DraggingRoutedEventArgs args = new DraggingRoutedEventArgs(DraggingEndedEvent,
                                                            MesoGrid.elementBeingDragged, MesoGrid.GetIsConstrained(MesoGrid.elementBeingDragged),
                                                            MesoGrid.GetXCoordinate(MesoGrid.elementBeingDragged), MesoGrid.GetYCoordinate(MesoGrid.elementBeingDragged),
                                                            MesoGrid.GetRow(MesoGrid.elementBeingDragged), MesoGrid.GetColumn(MesoGrid.elementBeingDragged),
                                                            MesoGrid.elementBeingDragged.Margin, MesoGrid.IsDragModeActive, MesoGrid.IsResizeModeActive);
            RaiseEvent(args);
        }

        public static readonly RoutedEvent DraggingActiveEvent = EventManager.RegisterRoutedEvent(
            "DraggingActive", RoutingStrategy.Bubble, typeof(DraggingRoutedEventHandler), typeof(MesoGrid));
        public event DraggingRoutedEventHandler DraggingActive
        {
            add { AddHandler(DraggingActiveEvent, value); }
            remove { RemoveHandler(DraggingActiveEvent, value); }
        }
        protected virtual void RaiseDraggingActiveEvent()
        {
            DraggingRoutedEventArgs args = new DraggingRoutedEventArgs(DraggingActiveEvent,
                                                            MesoGrid.elementBeingDragged, MesoGrid.GetIsConstrained(MesoGrid.elementBeingDragged),
                                                            MesoGrid.GetXCoordinate(MesoGrid.elementBeingDragged), MesoGrid.GetYCoordinate(MesoGrid.elementBeingDragged),
                                                            MesoGrid.GetRow(MesoGrid.elementBeingDragged), MesoGrid.GetColumn(MesoGrid.elementBeingDragged),
                                                            MesoGrid.elementBeingDragged.Margin, MesoGrid.IsDragModeActive, MesoGrid.IsResizeModeActive);
            RaiseEvent(args);
        }

        public bool ChildrenCanBeDragged { get; set; }
        public bool IsBoundaryEnforced { get; set; }
        public static bool IsDragInProcess { get; protected set; }

        private bool useDragHighlight = true;
        public bool UseDragHighlight
        {
            get
            {
                return useDragHighlight;
            }
            set
            {
                useDragHighlight = value;
            }
        }



        public static bool GetDraggingBringsToFront(DependencyObject obj)
        {
            return (bool)obj.GetValue(DraggingBringsToFrontProperty);
        }
        public static void SetDraggingBringsToFront(DependencyObject obj, bool value)
        {
            obj.SetValue(DraggingBringsToFrontProperty, value);
        }
        // Using a DependencyProperty as the backing store for DraggingBringsToFront.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DraggingBringsToFrontProperty =
            DependencyProperty.RegisterAttached(
                "DraggingBringsToFront",
                typeof(bool),
                typeof(MesoGrid),
                new UIPropertyMetadata(true));

        public static UIElement GetDragGripTarget(DependencyObject obj)
        {
            return (UIElement)obj.GetValue(DragGripTargetProperty);
        }
        public static void SetDragGripTarget(DependencyObject obj, UIElement value)
        {
            obj.SetValue(DragGripTargetProperty, value);
        }
        // Using a DependencyProperty as the backing store for DragGripTarget.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DragGripTargetProperty =
            DependencyProperty.RegisterAttached(
                "DragGripTarget",
                typeof(UIElement),
                typeof(MesoGrid),
                new UIPropertyMetadata(null)
            );



        public static UIElement GetResizeGripTarget(DependencyObject obj)
        {
            return (UIElement)obj.GetValue(ResizeGripTargetProperty);
        }
        public static void SetResizeGripTarget(DependencyObject obj, UIElement value)
        {
            obj.SetValue(ResizeGripTargetProperty, value);
        }
        // Using a DependencyProperty as the backing store for ResizeGripTarget.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ResizeGripTargetProperty =
            DependencyProperty.RegisterAttached(
                "ResizeGripTarget", 
                typeof(UIElement), 
                typeof(MesoGrid), 
                new UIPropertyMetadata(null)
            );

        public static bool IsDragModeActive { get; set; }

        private static Point originalCursorLocation;
        private static MesoGrid gridBeingDraggedIn;
        private static FrameworkElement elementBeingDragged;
        public static FrameworkElement ElementBeingDragged
        {
            get
            {
                return MesoGrid.elementBeingDragged;
            }
        }
        private static Point originalCursorLocationOnElementBeingDragged;
        public static Point OriginalCursorLocationOnElementBeingDragged
        {
            get
            {
                return originalCursorLocationOnElementBeingDragged;
            }
        }
        private static HorizontalAlignment elementBeingDraggedOriginalHorizontalAlignment;
        private static VerticalAlignment elementBeingDraggedOriginalVerticalAlignment;
        private static GridLayoutHelper elementBeingDraggedOriginalGridCoordinate;
        private static Thickness elementBeingDraggedOriginalMargin;
        private static double elementBeingDraggedOriginalXCoordinate;
        private static double elementBeingDraggedOriginalYCoordinate;
        private static double elementBeingDraggedOpacity;
        private static Rectangle gridHighlight;
        private static Brush skyBlueBrush = new SolidColorBrush(
                                            Color.FromArgb(System.Drawing.Color.LightBlue.A, System.Drawing.Color.LightBlue.R,
                                            System.Drawing.Color.LightBlue.G, System.Drawing.Color.LightBlue.B));
        private Dictionary<UIElement, UIElement> heightOrderMap = new Dictionary<UIElement, UIElement>();
        private bool isLocked = false;
        public bool IsLocked
        {
            get
            {
                return isLocked;
            }
            set
            {
                isLocked = value;
                this.OnPropertyChanged("IsLocked");
            }
        }
        private static bool isLockingModeActive;
        public static bool IsLockingModeActive
        {
            get
            {
                return isLockingModeActive;
            }
            set
            {
                isLockingModeActive = value;
                if (!value)
                    MesoGrid.GridMouseIsOver = null;
            }
        }
        private static FrameworkElement lastElementClicked;
        private static bool dragWithDockingPoints;

        private static GridLayoutHelper gridCoordinateMouseIsOver;
        public static GridLayoutHelper InternalGridCoordinateMouseIsOver
        {
            get
            {
                if (!MesoGrid.IsDragModeActive)
                    return null;
                return MesoGrid.gridCoordinateMouseIsOver;
            }
            set
            {
                GridLayoutHelper oldValue = MesoGrid.gridCoordinateMouseIsOver;
                if (!MesoGrid.IsDragModeActive)
                    MesoGrid.gridCoordinateMouseIsOver = null;
                else
                    MesoGrid.gridCoordinateMouseIsOver = value;
                if (oldValue != MesoGrid.gridCoordinateMouseIsOver)
                    MesoGrid.gridCoordinateMouseIsOverChanged(MesoGrid.gridCoordinateMouseIsOver);
            }
        }
        [Todo("Ian", "7/18/07", Comments = "Replace temporary hardcoded style with dynamic style once Unified Resource Dictionary is complete.")]
        private static void gridCoordinateMouseIsOverChanged(GridLayoutHelper value)
        {
            MesoGrid hostGrid = MesoGrid.gridBeingDraggedIn;

            if (value == null)
            {
                if (hostGrid != null)
                {
                    //if the host grid or gridcoordinate has been lost, it means the gridHighlight needs to be taken off of the hostgrid.
                    MesoGrid.gridBeingDraggedIn.Children.Remove(MesoGrid.gridHighlight);
                }
                return;
            }

            if (hostGrid.UseDragHighlight)
            {
                #region Temporary Code (Hardcoded style)
                //TEMPORARY CODE: Hardcoded style until the unified resource library is complete
                //gridHighlight is a rectangle that's instantiated once and then moved around and resized as necessary to highlight the grid
                if (MesoGrid.gridHighlight == null)
                {
                    MesoGrid.gridHighlight = new Rectangle();
                    MesoGrid.gridHighlight.Fill = MesoGrid.skyBlueBrush;
                }
                #endregion

                if (!hostGrid.Children.Contains(MesoGrid.gridHighlight) && hostGrid.heightOrderMap.ContainsKey(MesoGrid.elementBeingDragged))
                {
                    int newIndex = hostGrid.Children.IndexOf(hostGrid.heightOrderMap[MesoGrid.elementBeingDragged]) - 1;
                    if (newIndex < 0)
                        newIndex = 0;
                    hostGrid.Children.Insert(newIndex, MesoGrid.gridHighlight);
                }

                //Put the highlight rectangle on the grid
                MesoGrid.SetRow(MesoGrid.gridHighlight, value.Row);
                MesoGrid.SetColumn(MesoGrid.gridHighlight, value.Column);
            }

            //Move the element to its new grid location, mind the spans
            if (value.Row + MesoGrid.GetRowSpan(MesoGrid.elementBeingDragged) <= MesoGrid.gridBeingDraggedIn.RowDefinitions.Count)
                MesoGrid.SetRow(MesoGrid.elementBeingDragged, value.Row);
            if(value.Column + MesoGrid.GetColumnSpan(MesoGrid.elementBeingDragged) <= MesoGrid.gridBeingDraggedIn.ColumnDefinitions.Count)
                MesoGrid.SetColumn(MesoGrid.elementBeingDragged, value.Column);

            MesoGrid.gridBeingDraggedIn.InvalidateArrange();
            MesoGrid.gridBeingDraggedIn.UpdateLayout();
        }


        static void MesoGrid_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            e.Handled = false;

            if (MesoGrid.IsLockingModeActive) return;

            //Make sure it's a drag or a resize
            if (!MesoGrid.IsDragModeActive && !MesoGrid.IsResizeModeActive) return;       

            MesoGrid.IsDragInProcess = false;  
            
            //Find the Grid that is being dragged in and the element being dragged...
            MesoGrid.gridBeingDraggedIn = MesoGrid.FindParentManagedPanel(e.OriginalSource as DependencyObject) as MesoGrid;
            if (MesoGrid.gridBeingDraggedIn == null) return;

            //MesoGrid.elementBeingDragged = MesoGrid.FindManagedChild(e.OriginalSource as DependencyObject, MesoGrid.gridBeingDraggedIn);
            //if (MesoGrid.elementBeingDragged == null) return;

            MesoGrid.elementBeingDragged = MesoGrid.FindParentDragGripObject(e.OriginalSource as DependencyObject, MesoGrid.gridBeingDraggedIn);
            if (MesoGrid.elementBeingDragged != null)
            {
                if (!Dragging_PreviewMouseLeftButtonDown(sender, e))
                    return;
            }
            else
            {
                MesoGrid.elementBeingDragged = MesoGrid.FindParentResizeGripObject(e.OriginalSource as DependencyObject, MesoGrid.gridBeingDraggedIn);
                if (MesoGrid.elementBeingDragged == null) return;
                else
                {
                    if (!Resizing_PreviewMouseLeftButtonDown(sender, e))
                        return;
                }
            }

            MesoGrid.originalCursorLocation = e.GetPosition(MesoGrid.gridBeingDraggedIn);
            MesoGrid.lastElementClicked = MesoGrid.elementBeingDragged;
            MesoGrid.originalCursorLocationOnElementBeingDragged = Mouse.GetPosition(MesoGrid.elementBeingDragged);
            MesoGrid.gridBeingDraggedIn.BringToFrontOfActivePanel(MesoGrid.elementBeingDragged);

            MesoGrid.elementBeingDraggedOriginalHorizontalAlignment = MesoGrid.elementBeingDragged.HorizontalAlignment;
            MesoGrid.elementBeingDraggedOriginalVerticalAlignment = MesoGrid.elementBeingDragged.VerticalAlignment;

            //MesoGrid.gridBeingDraggedIn.MouseUp += new MouseButtonEventHandler(globalHookMouseUp);

            
            if (MesoGrid.IsResizeModeActive)
            {
                MesoGrid.Resizing_MesoGrid_PreviewMouseLeftButtonDown(sender, e);
            }

            if (MesoGrid.dragWithDockingPoints)
            {
                MesoGrid.buildChildBoundingBoxes();
            }


            
           // Hooks.HookManager.InstallHook(typeof(MesoGrid));

            MesoGrid.IsDragInProcess = true;
            MesoGrid.gridBeingDraggedIn.CaptureMouse();
            MesoGrid.gridBeingDraggedIn.RaiseDraggingStartedEvent();
            MesoGrid.elementBeingDragged.IsHitTestVisible = false;

            e.Handled = true;
        }

        static bool Dragging_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            //And now before doing any further checks, make sure the element being dragged isn't a drag grip
            if (MesoGrid.GetDragGripTarget(MesoGrid.elementBeingDragged) != null)
            {
                //If it is a drag grip, we need to adjust the grid being dragged in and the element being dragged.
                MesoGrid.elementBeingDragged = MesoGrid.GetDragGripTarget(MesoGrid.elementBeingDragged) as FrameworkElement;
                MesoGrid.gridBeingDraggedIn = MesoGrid.FindParentManagedPanel(MesoGrid.elementBeingDragged.Parent as DependencyObject) as MesoGrid;
                if (MesoGrid.gridBeingDraggedIn == null) return false;
            }
            else
            {
                //Quit out if we're not dragging a grip (disable non-grip drags)
                return false;
            }

            if (MesoGrid.gridBeingDraggedIn.IsLocked) return false;
            if (!MesoGrid.gridBeingDraggedIn.ChildrenCanBeDragged) return false;
            if (MesoGrid.GetIsDraggable(MesoGrid.elementBeingDragged) == false) return false;

            //if (MesoGrid.elementBeingDragged == null) return;


            MesoGrid.IsDragModeActive = true;
            MesoGrid.IsResizeModeActive = false;

            //If in constrained space, treat it as a grid type operation
            if (MesoGrid.GetIsConstrained(MesoGrid.elementBeingDragged))
            {
                MesoGrid.elementBeingDraggedOriginalGridCoordinate = GridLayoutHelper.LocateMouseOnGrid(MesoGrid.gridBeingDraggedIn);
                MesoGrid.elementBeingDraggedOriginalMargin = MesoGrid.elementBeingDragged.Margin;
            }
            else
            {
                MesoGrid.elementBeingDraggedOriginalXCoordinate = MesoGrid.GetXCoordinate(MesoGrid.elementBeingDragged);
                MesoGrid.elementBeingDraggedOriginalYCoordinate = MesoGrid.GetYCoordinate(MesoGrid.elementBeingDragged);
            }



            #region Temporary Code (Hardcoded style)
            //TEMPORARY CODE: Hardcoded style until the unified resource library is complete
            if (MesoGrid.elementDockingMap.ContainsKey(MesoGrid.elementBeingDragged))
            {
                MesoGrid.elementBeingDraggedOpacity = MesoGrid.elementBeingDragged.Opacity;
                MesoGrid.recursiveOpacityAlteration(MesoGrid.elementBeingDragged, 0.5);
            }
            else
            {
                MesoGrid.elementBeingDraggedOpacity = MesoGrid.elementBeingDragged.Opacity;
                MesoGrid.elementBeingDragged.Opacity = MesoGrid.elementBeingDraggedOpacity * 0.5;
            }
            #endregion



            return true;
        }

        static bool Resizing_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //And now before doing any further checks, make sure the element being dragged isn't a drag grip
            if (MesoGrid.GetResizeGripTarget(MesoGrid.elementBeingDragged) != null)
            {
                //If it is a drag grip, we need to adjust the grid being dragged in and the element being dragged.
                MesoGrid.elementBeingDragged = MesoGrid.GetResizeGripTarget(MesoGrid.elementBeingDragged) as FrameworkElement;
                MesoGrid.gridBeingDraggedIn = MesoGrid.FindParentManagedPanel(MesoGrid.elementBeingDragged.Parent as DependencyObject) as MesoGrid;
                if (MesoGrid.gridBeingDraggedIn == null) return false;
            }
            else
            {
                //Quit out if we're not dragging a grip (disable non-grip drags)
                return false;
            }

            if (MesoGrid.gridBeingDraggedIn.IsLocked) return false;

            MesoGrid.IsResizeModeActive = true;
            MesoGrid.IsDragModeActive = false;

            return true;
        }

        static void recursiveOpacityAlteration(FrameworkElement targetElement, double desiredOpacity)
        {
            if (MesoGrid.elementDockingMap.ContainsKey(targetElement))
            {
                foreach (FrameworkElement element in MesoGrid.elementDockingMap[targetElement])
                {
                    element.Opacity = desiredOpacity;
                    if(targetElement != element)
                        MesoGrid.recursiveOpacityAlteration(element, desiredOpacity);
                }
            }
        }

        static void MesoGrid_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (MesoGrid.IsLockingModeActive) return;

            if (!MesoGrid.IsDragModeActive && !MesoGrid.IsResizeModeActive) return;

            //A drag must be in process and must know what it's dragging, and what panel it's in, to do anything here.
            if (!MesoGrid.IsDragInProcess || MesoGrid.elementBeingDragged == null || MesoGrid.gridBeingDraggedIn == null) return;

            //If the grid is locked, no elements within it may be moved.
            if (MesoGrid.gridBeingDraggedIn.IsLocked) return;

            //Can't move docked objects other than by undocking them.
            if (MesoGrid.GetDockTarget(MesoGrid.elementBeingDragged) != "" && !MesoGrid.dragWithDockingPoints) return;

            Point cursorLocation = Mouse.GetPosition(MesoGrid.gridBeingDraggedIn);

            if (MesoGrid.IsDragModeActive)
            {
                if (MesoGrid.GetIsConstrained(MesoGrid.elementBeingDragged))
                {
                    MesoGrid.InternalGridCoordinateMouseIsOver = GridLayoutHelper.LocateMouseOnGrid(MesoGrid.gridBeingDraggedIn);

                    if (MesoGrid.InternalGridCoordinateMouseIsOver == null) return;

                    //If both alignments are stretch and there are no margins, there's nothing to do here
                    if (MesoGrid.elementBeingDragged.HorizontalAlignment == HorizontalAlignment.Stretch &&
                        MesoGrid.elementBeingDragged.VerticalAlignment == VerticalAlignment.Stretch &&
                        Double.IsNaN(MesoGrid.elementBeingDragged.Height) &&
                        Double.IsNaN(MesoGrid.elementBeingDragged.Width) &&
                        MesoGrid.elementBeingDragged.Margin.Top == 0 &&
                        MesoGrid.elementBeingDragged.Margin.Bottom == 0 &&
                        MesoGrid.elementBeingDragged.Margin.Left == 0 &&
                        MesoGrid.elementBeingDragged.Margin.Right == 0) return;

                    Point desiredNewTopLeftCoordinate = Mouse.GetPosition(MesoGrid.gridBeingDraggedIn);
                    desiredNewTopLeftCoordinate.X -= MesoGrid.originalCursorLocationOnElementBeingDragged.X;
                    desiredNewTopLeftCoordinate.Y -= MesoGrid.originalCursorLocationOnElementBeingDragged.Y;

                    MesoGrid.InternalGridCoordinateMouseIsOver.PositionElementWithinGrid(MesoGrid.elementBeingDragged,
                            desiredNewTopLeftCoordinate, MesoGrid.gridBeingDraggedIn.IsBoundaryEnforced,
                            MesoGrid.elementBeingDraggedOriginalHorizontalAlignment,
                            MesoGrid.elementBeingDraggedOriginalVerticalAlignment, false);
                }
                else
                {
                    double newXCoordinate = MesoGrid.elementBeingDraggedOriginalXCoordinate + (cursorLocation.X - MesoGrid.originalCursorLocation.X);
                    double newYCoordinate = MesoGrid.elementBeingDraggedOriginalYCoordinate + (cursorLocation.Y - MesoGrid.originalCursorLocation.Y);

                    if (MesoGrid.gridBeingDraggedIn.IsBoundaryEnforced)
                    {
                        if (newXCoordinate < 0)
                            newXCoordinate = 0;
                        else if (newXCoordinate > (MesoGrid.gridBeingDraggedIn.ActualWidth - MesoGrid.elementBeingDragged.ActualWidth))
                            newXCoordinate = (MesoGrid.gridBeingDraggedIn.ActualWidth - MesoGrid.elementBeingDragged.ActualWidth);
                        if (newYCoordinate < 0)
                            newYCoordinate = 0;
                        else if (newYCoordinate > (MesoGrid.gridBeingDraggedIn.ActualHeight - MesoGrid.elementBeingDragged.ActualHeight))
                            newYCoordinate = (MesoGrid.gridBeingDraggedIn.ActualHeight - MesoGrid.elementBeingDragged.ActualHeight);
                    }

                    MesoGrid.SetXCoordinate(MesoGrid.elementBeingDragged, newXCoordinate);
                    MesoGrid.SetYCoordinate(MesoGrid.elementBeingDragged, newYCoordinate);

                    if (MesoGrid.dragWithDockingPoints)
                        MesoGrid.snapToDockingPointIfApplicable(new Point(newXCoordinate, newYCoordinate));
                }
            }
            else if (MesoGrid.IsResizeModeActive)
            {
                MesoGrid.Resizing_MesoGrid_PreviewMouseMove(sender, e);
            }

            MesoGrid.gridBeingDraggedIn.RaiseDraggingActiveEvent();
            MesoGrid.gridBeingDraggedIn.InvalidateArrange();
            MesoGrid.gridBeingDraggedIn.UpdateLayout();
        }

        private static void globalHookMouseUp(object sender, MouseEventArgs e)
        {
            if (!MesoGrid.IsDragModeActive && !MesoGrid.IsResizeModeActive) return;

            //A drag must be in process and must know what it's dragging, and what panel it's in, to do anything here.
            if (!MesoGrid.IsDragInProcess || MesoGrid.elementBeingDragged == null || MesoGrid.gridBeingDraggedIn == null) return;

            if (MesoGrid.gridBeingDraggedIn.IsLocked) return;

            //Always call this, since we want to resize an object to fit if it's too large for its cell
            MesoGrid.Resizing_globalHookMouseUp();

            if (MesoGrid.dragWithDockingPoints)
                MesoGrid.Docking_globalHookMouseUp();

            //Don't do the opacity alteration on Resize, since resize does not alter opacity when it begins
            if (MesoGrid.elementBeingDragged != null && !MesoGrid.IsResizeModeActive)
            {
                #region Temporary Code (Hardcoded style)
                //TEMPORARY CODE: Hardcoded style until the unified resource library is complete
                if (MesoGrid.elementDockingMap.ContainsKey(MesoGrid.elementBeingDragged))
                {
                    MesoGrid.recursiveOpacityAlteration(MesoGrid.elementBeingDragged, MesoGrid.elementBeingDraggedOpacity);
                }
                else
                {
                    MesoGrid.elementBeingDragged.Opacity = MesoGrid.elementBeingDraggedOpacity;
                }
                #endregion
            }

            MesoGrid.gridBeingDraggedIn.ReleaseMouseCapture();


            MesoGrid.gridBeingDraggedIn.RaiseDraggingEndedEvent();
            if (!(MesoGrid.GetDraggingBringsToFront(MesoGrid.elementBeingDragged)))
                MesoGrid.gridBeingDraggedIn.setIndexOnActivePanel(MesoGrid.elementBeingDragged);
            MesoGrid.InternalGridCoordinateMouseIsOver = null;
            //MesoGrid.elementBeingDragged.MouseUp -= new MouseButtonEventHandler(globalHookMouseUp);
            MesoGrid.elementBeingDragged.IsHitTestVisible = true;
            MesoGrid.elementBeingDragged = null;
            MesoGrid.gridBeingDraggedIn = null;
            MesoGrid.IsDragInProcess = false;



            //Uninstall the hook
           // Hooks.HookManager.UninstallHook(typeof(MesoGrid));
        }

        //static void mouseHook_MouseEvent(MouseEvents mEvent, System.Drawing.Point point)
        //{
        //    if (mEvent == MouseEvents.LeftButtonUp || mEvent == MouseEvents.RightButtonUp)
        //    {
        //        MesoGrid.globalHookMouseUp();
        //    }
        //}



        public static FrameworkElement FindManagedChild(DependencyObject depObj, Panel managedPanel)
        {
            while (depObj != null)
            {
                FrameworkElement elem = depObj as FrameworkElement;
                FrameworkElement parent;
                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.
                if (depObj is System.Windows.Media.Visual || depObj is System.Windows.Media.Media3D.Visual3D)
                    parent = System.Windows.Media.VisualTreeHelper.GetParent(depObj) as FrameworkElement;
                else
                    parent = LogicalTreeHelper.GetParent(depObj) as FrameworkElement;

                if ((parent != null) && (parent is MesoGrid) && (parent == managedPanel))
                    break;
                depObj = parent;
            }
            return depObj as FrameworkElement;
        }

        public static FrameworkElement FindParentDragGripObject(DependencyObject depObj, Panel managedPanel)
        {
            while (depObj != null)
            {
                FrameworkElement elem = depObj as FrameworkElement;
                FrameworkElement parent;
                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.

                UIElement uie = GetDragGripTarget(elem);
                if (uie != null) return depObj as FrameworkElement;

                if (depObj is System.Windows.Media.Visual || depObj is System.Windows.Media.Media3D.Visual3D)
                    parent = System.Windows.Media.VisualTreeHelper.GetParent(depObj) as FrameworkElement;
                else
                    parent = LogicalTreeHelper.GetParent(depObj) as FrameworkElement;

                if ((parent != null) && (parent is MesoGrid) && (parent == managedPanel))
                    break;
                depObj = parent;
            }
            return null;
        }

        public static FrameworkElement FindParentResizeGripObject(DependencyObject depObj, Panel managedPanel)
        {
            while (depObj != null)
            {
                FrameworkElement elem = depObj as FrameworkElement;
                FrameworkElement parent;
                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.

                UIElement uie = GetResizeGripTarget(elem);
                if (uie != null) return depObj as FrameworkElement;

                if (depObj is System.Windows.Media.Visual || depObj is System.Windows.Media.Media3D.Visual3D)
                    parent = System.Windows.Media.VisualTreeHelper.GetParent(depObj) as FrameworkElement;
                else
                    parent = LogicalTreeHelper.GetParent(depObj) as FrameworkElement;

                if ((parent != null) && (parent is MesoGrid) && (parent == managedPanel))
                    break;
                depObj = parent;
            }
            return null;
        }
        

        public static Panel FindParentManagedPanel(DependencyObject depObj)
        {
            while (depObj != null)
            {
                FrameworkElement elem = depObj as FrameworkElement;
                if ((elem != null) && (elem is MesoGrid))
                    break;

                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.
                if (depObj is System.Windows.Media.Visual || depObj is System.Windows.Media.Media3D.Visual3D)
                    depObj = System.Windows.Media.VisualTreeHelper.GetParent(depObj);
                else
                    depObj = LogicalTreeHelper.GetParent(depObj);
            }
            return depObj as Panel;
        }

        public void BringToFrontOfActivePanel(UIElement element)
        {
            if (element is MesoGrid)
            {
                return;
            }

            //Add a height order map entry for replacement in the grid arrangement, if it's not already mapped
            if (!this.heightOrderMap.ContainsKey(element))
            {
                if (this.Children.Count > this.Children.IndexOf(element) + 1)
                    this.heightOrderMap.Add(element, (this.Children[this.Children.IndexOf(element) + 1]));
                else
                    this.heightOrderMap.Add(element, null);
            }

            this.Children.Remove(element);
            this.Children.Add(element);
        }

        private void setIndexOnActivePanel(UIElement element)
        {
            if (!this.Children.Contains(element))
                throw new Exception("Attempted to move an index that doesn't belong to this grid.");

            //index the height order map and replace the element where it came from in the grid stack.
            if (this.heightOrderMap.ContainsKey(element) && this.Children.Contains(this.heightOrderMap[element]))
            {
                this.Children.Remove(element);
                this.Children.Insert(this.Children.IndexOf(this.heightOrderMap[element]), element);
            }
        }

        public static bool ElementCanBeMovedToNewGrid(UIElement element)
        {
            //Can't select a dockedElement
            if (MesoGrid.GetDockTarget(element) != "") return false;

            MesoGrid owningGrid = MesoGrid.FindParentManagedPanel(element) as MesoGrid;

            //The element must have an owning grid and that grid must not be locked.
            if (owningGrid == null) return false;
            if (owningGrid.IsLocked) return false;

            return true;
        }

        public bool CanReceiveNewElement
        {
            get
            {
                if (IsLocked)
                    return false;

                return true;
            }
        }

        public Point GetNonConstrainedPoint(Point constrainedPoint, int row, int column)
        {
            Point ret = new Point(this.ColumnDefinitions[column].Offset + constrainedPoint.X, this.RowDefinitions[row].Offset + constrainedPoint.Y);

            return ret;
        }

        #endregion

        // "MesoGrid Resizing Components" denotes the methods and members that add object resizing to the MesoGrid.
        #region MesoGrid Resizing Components

        public static bool IsResizeModeActive { get; set; }
        private static double elementBeingDraggedOriginalHeight;
        private static double elementBeingDraggedOriginalWidth;
        private static Point originalCoordinateWithinCell = new Point();
        public static bool IsResizeMarginMirrorRestraintActive { get; set; }

        private static void Resizing_MesoGrid_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MesoGrid.elementBeingDraggedOriginalHeight = MesoGrid.elementBeingDragged.ActualHeight;
            MesoGrid.elementBeingDraggedOriginalWidth = MesoGrid.elementBeingDragged.ActualWidth;

            MesoLayoutDefinition hostRow = MesoGrid.gridBeingDraggedIn.RowDefinitions[MesoGrid.GetRow(MesoGrid.elementBeingDragged)];
            MesoLayoutDefinition hostColumn = MesoGrid.gridBeingDraggedIn.ColumnDefinitions[MesoGrid.GetColumn(MesoGrid.elementBeingDragged)];

            originalCoordinateWithinCell.X = (MesoGrid.originalCursorLocation.X - hostColumn.Offset) -
                MesoGrid.originalCursorLocationOnElementBeingDragged.X;
            originalCoordinateWithinCell.Y = (MesoGrid.originalCursorLocation.Y - hostRow.Offset) -
                MesoGrid.originalCursorLocationOnElementBeingDragged.Y;
        }

        private static int testIter = 0;

        private static void Resizing_MesoGrid_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (testIter > 100)
            {
                int j = 0;
            }

            testIter++;

            Point newCursorLocation = Mouse.GetPosition(MesoGrid.gridBeingDraggedIn);

            double newWidth = MesoGrid.elementBeingDraggedOriginalWidth + (newCursorLocation.X - MesoGrid.originalCursorLocation.X);
            double newHeight = MesoGrid.elementBeingDraggedOriginalHeight + (newCursorLocation.Y - MesoGrid.originalCursorLocation.Y);

            //if (MesoGrid.elementBeingDragged.VerticalAlignment == VerticalAlignment.Stretch ||
            //    MesoGrid.elementBeingDragged.VerticalAlignment == VerticalAlignment.Center)
            //    newHeight = MesoGrid.elementBeingDraggedOriginalHeight + (newCursorLocation.Y - MesoGrid.originalCursorLocation.Y)*2;
            //else
            //    newHeight = MesoGrid.elementBeingDraggedOriginalHeight + (newCursorLocation.Y - MesoGrid.originalCursorLocation.Y);

            //if (MesoGrid.elementBeingDragged.HorizontalAlignment == HorizontalAlignment.Stretch ||
            //    MesoGrid.elementBeingDragged.HorizontalAlignment == HorizontalAlignment.Center)
            //    newWidth = MesoGrid.elementBeingDraggedOriginalWidth + (newCursorLocation.X - MesoGrid.originalCursorLocation.X)*2;
            //else
            //    newWidth = MesoGrid.elementBeingDraggedOriginalWidth + (newCursorLocation.X - MesoGrid.originalCursorLocation.X);

            MesoGrid.elementBeingDragged.Width = (newWidth >= 0 ? newWidth : 0);
            MesoGrid.elementBeingDragged.Height = (newHeight >= 0 ? newHeight : 0);

            MesoLayoutDefinition hostRow = MesoGrid.gridBeingDraggedIn.RowDefinitions[MesoGrid.GetRow(MesoGrid.elementBeingDragged)];
            MesoLayoutDefinition hostColumn = MesoGrid.gridBeingDraggedIn.ColumnDefinitions[MesoGrid.GetColumn(MesoGrid.elementBeingDragged)];

            //Resize Margin Mirror Restraint dictates that the top and left margins be respected on the bottom and right side by the resize.
            if (MesoGrid.IsResizeMarginMirrorRestraintActive)
            {
                if (MesoGrid.elementBeingDragged.Width > (hostColumn.ActualLength - (MesoGrid.elementBeingDragged.Margin.Left * 2)))
                    MesoGrid.elementBeingDragged.Width = (hostColumn.ActualLength - (MesoGrid.elementBeingDragged.Margin.Left * 2));
                if (MesoGrid.elementBeingDragged.Height > (hostRow.ActualLength - (MesoGrid.elementBeingDragged.Margin.Top * 2)))
                    MesoGrid.elementBeingDragged.Height = (hostRow.ActualLength - (MesoGrid.elementBeingDragged.Margin.Top * 2));
            }

            if (MesoGrid.gridBeingDraggedIn.IsBoundaryEnforced)
            {
                if (MesoGrid.GetIsConstrained(MesoGrid.elementBeingDragged))
                {
                    //Center and Stretch elements use double-sized Width/Height coordinates for some ridiculous reason, account for it here
                    if (MesoGrid.elementBeingDragged.HorizontalAlignment == HorizontalAlignment.Stretch ||
                        MesoGrid.elementBeingDragged.HorizontalAlignment == HorizontalAlignment.Center)
                    {
                        double lesserOfWidths = MesoGrid.elementBeingDragged.Width;
                        
                        //Catch the left side collisions
                        if (MesoGrid.elementBeingDragged.Width - MesoGrid.elementBeingDraggedOriginalWidth >
                                MesoGrid.originalCoordinateWithinCell.X * 2)
                        {
                            double thisWidth = MesoGrid.originalCoordinateWithinCell.X * 2 + MesoGrid.elementBeingDraggedOriginalWidth;
                            if (thisWidth < lesserOfWidths)
                                lesserOfWidths = thisWidth;
                        }
                        //Catch the right side collisions
                        if (MesoGrid.elementBeingDragged.Width - MesoGrid.elementBeingDraggedOriginalWidth >
                            hostColumn.ActualLength * 2 - MesoGrid.elementBeingDraggedOriginalWidth * 2 - MesoGrid.originalCoordinateWithinCell.X * 2)
                        {
                            double thisWidth =
                                hostColumn.ActualLength * 2 - MesoGrid.elementBeingDraggedOriginalWidth - MesoGrid.originalCoordinateWithinCell.X * 2;
                            if (thisWidth < lesserOfWidths)
                                lesserOfWidths = thisWidth;
                        }

                        MesoGrid.elementBeingDragged.Width = lesserOfWidths;
                    }
                    else
                    {
                        if (MesoGrid.elementBeingDragged.Width + MesoGrid.originalCoordinateWithinCell.X > hostColumn.ActualLength)
                            MesoGrid.elementBeingDragged.Width = hostColumn.ActualLength - MesoGrid.originalCoordinateWithinCell.X;
                    }

                    //Center and Stretch elements use double-sized Width/Height coordinates for some ridiculous reason, account for it here
                    if (MesoGrid.elementBeingDragged.VerticalAlignment == VerticalAlignment.Stretch ||
                        MesoGrid.elementBeingDragged.VerticalAlignment == VerticalAlignment.Center)
                    {
                        double lesserOfHeights = MesoGrid.elementBeingDragged.Height;

                        //Catch the top side collisions
                        if (MesoGrid.elementBeingDragged.Height - MesoGrid.elementBeingDraggedOriginalHeight >
                                MesoGrid.originalCoordinateWithinCell.Y * 2)
                        {
                            double thisHeight = MesoGrid.originalCoordinateWithinCell.Y * 2 + MesoGrid.elementBeingDraggedOriginalHeight;
                            if (thisHeight < lesserOfHeights)
                                lesserOfHeights = thisHeight;
                        }
                        //Catch the bottom side collisions
                        if (MesoGrid.elementBeingDragged.Width - MesoGrid.elementBeingDraggedOriginalHeight >
                            hostRow.ActualLength * 2 - MesoGrid.elementBeingDraggedOriginalHeight * 2 - MesoGrid.originalCoordinateWithinCell.Y * 2)
                        {
                            double thisHeight =
                                hostRow.ActualLength * 2 - MesoGrid.elementBeingDraggedOriginalHeight - MesoGrid.originalCoordinateWithinCell.Y * 2;
                            if (thisHeight < lesserOfHeights)
                                lesserOfHeights = thisHeight;
                        }

                        MesoGrid.elementBeingDragged.Height = lesserOfHeights;
                    }
                    else
                    {
                        if (MesoGrid.elementBeingDragged.Height + MesoGrid.originalCoordinateWithinCell.Y > hostRow.ActualLength)
                            MesoGrid.elementBeingDragged.Height = hostRow.ActualLength - MesoGrid.originalCoordinateWithinCell.Y;
                    }
                }
                else
                {
                    if (MesoGrid.elementBeingDragged.Width + MesoGrid.elementBeingDraggedOriginalXCoordinate > 
                                MesoGrid.gridBeingDraggedIn.ActualWidth)
                        MesoGrid.elementBeingDragged.Width = MesoGrid.gridBeingDraggedIn.ActualWidth - 
                                MesoGrid.elementBeingDraggedOriginalXCoordinate;
                    if (MesoGrid.elementBeingDragged.Height + MesoGrid.elementBeingDraggedOriginalYCoordinate > 
                                MesoGrid.gridBeingDraggedIn.ActualHeight)
                        MesoGrid.elementBeingDragged.Height = MesoGrid.gridBeingDraggedIn.ActualHeight - 
                                MesoGrid.elementBeingDraggedOriginalYCoordinate;
                }
            }
        }

        private static void Resizing_globalHookMouseUp()
        {
            if (MesoGrid.gridBeingDraggedIn.IsBoundaryEnforced)
            {
                if (MesoGrid.GetIsConstrained(MesoGrid.elementBeingDragged))
                {
                    MesoLayoutDefinition hostRow = MesoGrid.gridBeingDraggedIn.RowDefinitions[MesoGrid.GetRow(MesoGrid.elementBeingDragged)];
                    MesoLayoutDefinition hostColumn = MesoGrid.gridBeingDraggedIn.ColumnDefinitions[MesoGrid.GetColumn(MesoGrid.elementBeingDragged)];

                    MarginHelper mh = new MarginHelper();

                    if (MesoGrid.elementBeingDragged.Width > hostColumn.ActualLength)
                    {
                        MesoGrid.elementBeingDragged.Width = hostColumn.ActualLength;
                        mh.Left = 0;
                        mh.Right = 0;
                    }
                    if (MesoGrid.elementBeingDragged.Height > hostRow.ActualLength)
                    {
                        MesoGrid.elementBeingDragged.Height = hostRow.ActualLength;
                        mh.Top = 0;
                        mh.Bottom = 0;
                    }

                    mh.SetThisMarginOnElement(MesoGrid.elementBeingDragged);
                }
            }
        }

        public static void MaximizeLastElementClicked()
        {
            MesoGrid lastGridClickedIn = MesoGrid.FindParentManagedPanel(MesoGrid.lastElementClicked) as MesoGrid;
            if (lastGridClickedIn == null) return;
            if (lastGridClickedIn.IsLocked) return;

            if (MesoGrid.GetIsConstrained(MesoGrid.lastElementClicked))
            {
                MesoLayoutDefinition hostRow = lastGridClickedIn.RowDefinitions[MesoGrid.GetRow(MesoGrid.lastElementClicked)];
                MesoLayoutDefinition hostColumn = lastGridClickedIn.ColumnDefinitions[MesoGrid.GetColumn(MesoGrid.lastElementClicked)];

                if (lastGridClickedIn.IsBoundaryEnforced)
                {
                    MesoGrid.lastElementClicked.Margin = new Thickness(0);
                    MesoGrid.lastElementClicked.Width = hostColumn.ActualLength;
                    MesoGrid.lastElementClicked.Height = hostRow.ActualLength;
                }
                else
                {
                    MesoGrid.lastElementClicked.Margin = new Thickness((-1 * hostColumn.Offset), (-1 * hostRow.Offset),
                                                                    ((hostColumn.Offset + hostColumn.ActualLength) - lastGridClickedIn.ActualWidth),
                                                                    ((hostRow.Offset + hostRow.ActualLength) - lastGridClickedIn.ActualHeight));
                    MesoGrid.lastElementClicked.Width = lastGridClickedIn.ActualWidth;
                    MesoGrid.lastElementClicked.Height = lastGridClickedIn.ActualHeight;
                }
            }
            else
            {
                MesoGrid.SetXCoordinate(MesoGrid.lastElementClicked, 0);
                MesoGrid.SetYCoordinate(MesoGrid.lastElementClicked, 0);
                MesoGrid.lastElementClicked.Height = lastGridClickedIn.ActualHeight;
                MesoGrid.lastElementClicked.Width = lastGridClickedIn.ActualWidth;
            }

            lastGridClickedIn.InvalidateArrange();
            lastGridClickedIn.UpdateLayout();
        }
        #endregion

        // "MesoGrid Docking Components" denotes the methods and members that add object docking to the MesoGrid.
        #region MesoGrid Docking Components

        public static String GetDockTarget(DependencyObject obj)
        {
            return (String)obj.GetValue(DockTargetProperty);
        }
        public static void SetDockTarget(DependencyObject obj, String value)
        {
            obj.SetValue(DockTargetProperty, value);
        }
        // Using a DependencyProperty as the backing store for DockingTarget.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DockTargetProperty =
            DependencyProperty.RegisterAttached(
              "DockTarget",
              typeof(String),
              typeof(MesoGrid),
              new UIPropertyMetadata("", new PropertyChangedCallback(MesoGrid.onDockTargetChanged))
            );
        private static void onDockTargetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            DependencyObject obj = MesoGrid.FindParentManagedPanel(d) as DependencyObject;

            if ((e.OldValue as string) != "" && e.OldValue != null)
            {
                DependencyObject dockingTargetObj = (LogicalTreeHelper.FindLogicalNode(obj, e.OldValue as string));
                MesoGrid.removeDockTargetReferenceFromTree(d, dockingTargetObj);
            }
            if ((e.NewValue as string) != "" && e.NewValue != null)
            {
                DependencyObject dockingTargetObj = (LogicalTreeHelper.FindLogicalNode(obj, e.NewValue as string));
                MesoGrid.addDockTargetReferenceToTree(d, dockingTargetObj);
            }
        }

        private static void removeDockTargetReferenceFromTree(DependencyObject depSelf, DependencyObject depTarget)
        {
            FrameworkElement self = depSelf as FrameworkElement;
            FrameworkElement target = depTarget as FrameworkElement;

            if (self == null || target == null)
                throw new Exception("Attempted to remove a dock target of or from an object which was not a frameworkelement.");

            if (!(MesoGrid.elementDockingMap.ContainsKey(self) && MesoGrid.elementDockingMap.ContainsKey(target)))
                throw new Exception("Attempted to remove a dock target but the element or its target were not present in the docking map.");

            foreach (FrameworkElement element in MesoGrid.elementDockingMap[self])
            {
                if (MesoGrid.elementDockingMap[target].Contains(element))
                    MesoGrid.elementDockingMap[target].Remove(element);
            }
        }

        private static void addDockTargetReferenceToTree(DependencyObject depSelf, DependencyObject depTarget)
        {
            FrameworkElement self = depSelf as FrameworkElement;
            FrameworkElement target = depTarget as FrameworkElement;

            if (self == null || target == null)
                throw new Exception("Attempted to add a dock target to or from an object which was not a frameworkelement.");

            if (!MesoGrid.elementDockingMap.ContainsKey(self))
            {
                MesoGrid.elementDockingMap.Add(self, new List<FrameworkElement>());
                MesoGrid.elementDockingMap[self].Add(self);
            }

            if (!MesoGrid.elementDockingMap.ContainsKey(target))
            {
                MesoGrid.elementDockingMap.Add(target, new List<FrameworkElement>());
                MesoGrid.elementDockingMap[target].Add(target);
                foreach (FrameworkElement element in MesoGrid.elementDockingMap[self])
                {
                    MesoGrid.elementDockingMap[target].Add(element);
                }
            }
            else
            {
                foreach (FrameworkElement element in MesoGrid.elementDockingMap[self])
                {
                    if (!MesoGrid.elementDockingMap[target].Contains(element))
                        MesoGrid.elementDockingMap[target].Add(element);
                }
            }
        }

        public static DockSide GetDockSide(DependencyObject obj)
        {
            return (DockSide)obj.GetValue(DockSideProperty);
        }
        public static void SetDockSide(DependencyObject obj, DockSide value)
        {
            obj.SetValue(DockSideProperty, value);
        }
        // Using a DependencyProperty as the backing store for DockSide.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DockSideProperty =
            DependencyProperty.RegisterAttached(
              "DockSide",
              typeof(DockSide),
              typeof(MesoGrid),
              new UIPropertyMetadata(DockSide.Right)
            );

        public static DockAlignment GetDockAlignment(DependencyObject obj)
        {
            return (DockAlignment)obj.GetValue(DockAlignmentProperty);
        }
        public static void SetDockAlignment(DependencyObject obj, DockAlignment value)
        {
            obj.SetValue(DockAlignmentProperty, value);
        }
        // Using a DependencyProperty as the backing store for DockAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DockAlignmentProperty =
            DependencyProperty.RegisterAttached(
              "DockAlignment",
              typeof(DockAlignment),
              typeof(MesoGrid),
              new UIPropertyMetadata(DockAlignment.Center)
            );

        private static bool isDoubleValueGreaterThanOrEqualToZero(object value)
        {
            return (((double)value) >= 0.0);
        }

        private static Dictionary<FrameworkElement, List<FrameworkElement>> elementDockingMap = new Dictionary<FrameworkElement, List<FrameworkElement>>();


        public static void ChangeDockingStateOfLastElementClicked()
        {
            MesoGrid lastGridClickedIn = MesoGrid.FindParentManagedPanel(MesoGrid.lastElementClicked) as MesoGrid;
            if (lastGridClickedIn == null) return;
            if (lastGridClickedIn.IsLocked) return;

            //Obviously we don't care about the docking state of constrained elements, since constrained elements cannot be docked.
            if (! MesoGrid.GetIsConstrained(MesoGrid.lastElementClicked))
            {
                if (MesoGrid.GetDockTarget(MesoGrid.lastElementClicked) != "")
                {
                    //We have to locate an appropriate X and Y coordinate for the newly un-docked element.
                    //MesoGrid.originalCursorLocationOnElementBeingDragged
                    Point positionOnElement = Mouse.GetPosition(MesoGrid.lastElementClicked);
                    Point positionInGrid = Mouse.GetPosition(lastGridClickedIn);

                    MesoGrid.SetDockTarget(MesoGrid.lastElementClicked, "");

                    //Update the actual coordinate as well as the "original" coordinate so dragging can carry on normally.
                    MesoGrid.SetXCoordinate(MesoGrid.lastElementClicked, positionInGrid.X - positionOnElement.X);
                    MesoGrid.SetYCoordinate(MesoGrid.lastElementClicked, positionInGrid.Y - positionOnElement.Y);
                    MesoGrid.elementBeingDraggedOriginalXCoordinate = positionInGrid.X - positionOnElement.X;
                    MesoGrid.elementBeingDraggedOriginalYCoordinate = positionInGrid.Y - positionOnElement.Y;
                }
                else
                {
                    //If not already in docking mode, enter it and rebuild the bounding boxes for snap-to boxes
                    if (!MesoGrid.dragWithDockingPoints)
                    {
                        MesoGrid.dragWithDockingPoints = true;
                        MesoGrid.buildChildBoundingBoxes();
                    }
                }

                lastGridClickedIn.InvalidateArrange();
                lastGridClickedIn.UpdateLayout();
            }
        }
        
        private static Dictionary<UIElement, Rect> childBoundingBoxes  = new Dictionary<UIElement, Rect>();
        private const double dockingSnapDistance = 8.0;

        private static void buildChildBoundingBoxes()
        {
            childBoundingBoxes.Clear();

            if (MesoGrid.gridBeingDraggedIn == null) return;
            
            foreach (UIElement element in MesoGrid.gridBeingDraggedIn.Children)
            {
                Point topLeft = element.TranslatePoint(new Point(0, 0), MesoGrid.gridBeingDraggedIn);
                childBoundingBoxes.Add(element,new Rect(topLeft, element.RenderSize));
            }
        }

        private static UIElement newDockTarget = null;
        private static DockSide newDockSide = DockSide.Bottom;
        private static DockAlignment newDockAlignment = DockAlignment.Center;
        private static Point newDockPoint = new Point(0, 0);

        private static bool isElementInDockingTreeOfOtherElement(UIElement element,UIElement otherElement)
        {
            //Recursive method...

            if (MesoGrid.elementDockingMap.ContainsKey(otherElement as FrameworkElement))
            {
                foreach (FrameworkElement internalElement in MesoGrid.elementDockingMap[otherElement as FrameworkElement])
                {
                    if (element == internalElement)
                        return true;
                    if (internalElement != otherElement && isElementInDockingTreeOfOtherElement(element, internalElement))
                        return true;
                }
            }

            return false;
        }

        private static void snapToDockingPointIfApplicable(Point desiredCoordinate)
        {
            UIElement closestDockTarget = null;
            double closestDockTargetDistance = MesoGrid.dockingSnapDistance;
            DockSide closestDockSide = DockSide.Bottom;
            DockAlignment closestDockAlignment = DockAlignment.Center;

            double candidateDockTargetDistance = MesoGrid.dockingSnapDistance;
            DockSide candidateDockSide = DockSide.Bottom;
            DockAlignment candidateDockAlignment = DockAlignment.Center;

            Rect activeRect = new Rect(desiredCoordinate, MesoGrid.elementBeingDragged.RenderSize);
            
            //Docking occurs primarily on the DockingSide - Left pairs with Right, Top pairs with Bottom.
            foreach (UIElement elementKey in MesoGrid.childBoundingBoxes.Keys)
	        {
                if (MesoGrid.isElementInDockingTreeOfOtherElement(elementKey, MesoGrid.elementBeingDragged)) continue;
                
                Rect elementRect = MesoGrid.childBoundingBoxes[elementKey];
                
                if (distanceIsSmallerOrEqual(activeRect, elementRect, ref candidateDockSide, ref candidateDockTargetDistance,
                            ref candidateDockAlignment))
                {
                    //ignore the element if the side it would dock on is concealed
                    if (elementIsConcealed(elementKey, elementRect, activeRect, candidateDockSide))
                        continue;

                    //ignore the element if it is nameless.
                    if (elementKey as FrameworkElement == null || (elementKey as FrameworkElement).Name == "")
                        continue;

                    closestDockTargetDistance = candidateDockTargetDistance;
                    closestDockSide = candidateDockSide;
                    closestDockAlignment = candidateDockAlignment;

                    closestDockTarget = elementKey;
                    
                }
	        }
            
                        
            if (closestDockTarget != MesoGrid.newDockTarget || closestDockSide != MesoGrid.newDockSide)
            {
                AdornerLayer al = null;

                if (newDockTarget != null)
                {
                    al = AdornerLayer.GetAdornerLayer(MesoGrid.newDockTarget);
                }

                if (al != null)
                {
                    Adorner[] toRemoveArray = al.GetAdorners(MesoGrid.newDockTarget);
                    if (toRemoveArray != null)
                    {
                        foreach (Adorner toRemove in toRemoveArray)
                        {
                            if (toRemove is DockingTargetAdorner)
                                al.Remove(toRemove);
                        }
                    }
                }

                al = null;

                if (closestDockTarget != null)
                {
                    al = AdornerLayer.GetAdornerLayer(closestDockTarget);
                }

                if (al != null)
                    al.Add(new DockingTargetAdorner(closestDockTarget, closestDockSide));

                
            }

            MesoGrid.newDockTarget = closestDockTarget;
            MesoGrid.newDockSide = closestDockSide;

            if (closestDockTarget == null) return;

            //Now place the element appropriately if it has acquired a dock target.

            if (closestDockSide == DockSide.Bottom)
            {
                MesoGrid.SetYCoordinate(MesoGrid.elementBeingDragged, MesoGrid.childBoundingBoxes[closestDockTarget].Bottom);
            }
            else if (closestDockSide == DockSide.Top)
            {
                MesoGrid.SetYCoordinate(MesoGrid.elementBeingDragged, MesoGrid.childBoundingBoxes[closestDockTarget].Top 
                                                                      - MesoGrid.elementBeingDragged.ActualHeight);
            }
            else if (closestDockSide == DockSide.Right)
            {
                MesoGrid.SetXCoordinate(MesoGrid.elementBeingDragged, MesoGrid.childBoundingBoxes[closestDockTarget].Right);
            }
            else if (closestDockSide == DockSide.Left)
            {
                MesoGrid.SetXCoordinate(MesoGrid.elementBeingDragged, MesoGrid.childBoundingBoxes[closestDockTarget].Left
                                                                      - MesoGrid.elementBeingDragged.ActualWidth);
            }

            //Now see if one of the sides is close enough for an "alignment" snap, and which one is closest if there
            //are multiple ones
            double nearGap;
            double farGap;
            double centerGap;

            Rect dockTarget = MesoGrid.childBoundingBoxes[closestDockTarget];

            if (closestDockSide == DockSide.Left || closestDockSide == DockSide.Right)
            {
                nearGap = Math.Abs(dockTarget.Top - activeRect.Top);
                farGap = Math.Abs(dockTarget.Bottom - activeRect.Bottom);
                centerGap = Math.Abs((dockTarget.Top + dockTarget.Height / 2) - (activeRect.Top + activeRect.Height / 2));
            }
            else
            {
                nearGap = Math.Abs(dockTarget.Left - activeRect.Left);
                farGap = Math.Abs(dockTarget.Right - activeRect.Right);
                centerGap = Math.Abs((dockTarget.Left + dockTarget.Width / 2) - (activeRect.Left + activeRect.Width / 2));
            }


            if (nearGap <= dockingSnapDistance || farGap <= dockingSnapDistance || centerGap <= dockingSnapDistance)
            {
                //If there's a gap match, then the desiredOffset is 0,0.
                MesoGrid.newDockPoint.X = 0;
                MesoGrid.newDockPoint.Y = 0;

                //TIE PRIORITY : In the event of a "tie" between distances, Center alignment trumps the other two,
                //               and near trumps far.  This is arbitrary.
                if (centerGap <= nearGap && centerGap <= farGap)
                {
                    closestDockAlignment = DockAlignment.Center;
                    if (closestDockSide == DockSide.Left || closestDockSide == DockSide.Right)
                        MesoGrid.SetYCoordinate(MesoGrid.elementBeingDragged,
                            (dockTarget.Top + dockTarget.Height / 2 - activeRect.Height / 2));
                    else
                        MesoGrid.SetXCoordinate(MesoGrid.elementBeingDragged,
                            (dockTarget.Left + dockTarget.Width / 2 - activeRect.Width / 2));
                }
                else if (nearGap <= farGap)
                {
                    closestDockAlignment = DockAlignment.Near;
                    if (closestDockSide == DockSide.Left || closestDockSide == DockSide.Right)
                        MesoGrid.SetYCoordinate(MesoGrid.elementBeingDragged, dockTarget.Top);
                    else
                        MesoGrid.SetXCoordinate(MesoGrid.elementBeingDragged, dockTarget.Left);
                }
                else
                {
                    closestDockAlignment = DockAlignment.Far;
                    if (closestDockSide == DockSide.Left || closestDockSide == DockSide.Right)
                        MesoGrid.SetYCoordinate(MesoGrid.elementBeingDragged, dockTarget.Bottom - activeRect.Height);
                    else
                        MesoGrid.SetXCoordinate(MesoGrid.elementBeingDragged, dockTarget.Right - activeRect.Width);
                }

                MesoGrid.newDockAlignment = closestDockAlignment;
            }
            else
            {
                //If there's no match, it means the offset should be figured out based on side

                //Track the desiredOffset (handled in the X,Y coordinate)
                Point topLeft = new Point(0, 0);
                topLeft.X = MesoGrid.GetXCoordinate(MesoGrid.elementBeingDragged);
                topLeft.Y = MesoGrid.GetYCoordinate(MesoGrid.elementBeingDragged);

                MesoGrid.newDockAlignment = DockAlignment.Near;

                if (closestDockSide == DockSide.Left || closestDockSide == DockSide.Right)
                {
                    MesoGrid.newDockPoint.Y = topLeft.Y - dockTarget.Top;
                    MesoGrid.newDockPoint.X = 0;
                }
                else
                {
                    MesoGrid.newDockPoint.X = topLeft.X - dockTarget.Left;
                    MesoGrid.newDockPoint.Y = 0;
                }
            }
        }

        private static void Docking_globalHookMouseUp()
        {
            AdornerLayer al = null;

            if (MesoGrid.newDockTarget != null)
            {
                al = AdornerLayer.GetAdornerLayer(MesoGrid.newDockTarget);
            }

            if (al != null)
            {
                Adorner[] toRemoveArray = al.GetAdorners(MesoGrid.newDockTarget);
                if (toRemoveArray != null)
                {
                    foreach (Adorner toRemove in toRemoveArray)
                    {
                        if (toRemove is DockingTargetAdorner)
                            al.Remove(toRemove);
                    }
                }
            }

            FrameworkElement target = MesoGrid.newDockTarget as FrameworkElement;

            if (target != null)
            {
                if (target.Name == "")
                    throw new Exception("Attempted to dock to a target with no name.  It felt good to be out of the rain.");

                MesoGrid.SetDockTarget(MesoGrid.elementBeingDragged, target.Name);
                MesoGrid.SetDockAlignment(MesoGrid.elementBeingDragged, MesoGrid.newDockAlignment);
                MesoGrid.SetDockSide(MesoGrid.elementBeingDragged, MesoGrid.newDockSide);
                MesoGrid.SetXCoordinate(MesoGrid.elementBeingDragged, MesoGrid.newDockPoint.X);
                MesoGrid.SetYCoordinate(MesoGrid.elementBeingDragged, MesoGrid.newDockPoint.Y);
            }

            MesoGrid.dragWithDockingPoints = false;
            MesoGrid.newDockTarget = null;
            MesoGrid.newDockPoint.X = 0;
            MesoGrid.newDockPoint.Y = 0;
        }

        private static double sideFromRect(Rect rect, DockSide side)
        {
            if (side == DockSide.Bottom)
                return rect.Bottom;
            else if (side == DockSide.Left)
                return rect.Left;
            else if (side == DockSide.Right)
                return rect.Right;
            else if (side == DockSide.Top)
                return rect.Top;
            else
                return 0.0;
        }

        //need a way to construct a line from a rect side for occlusion testing
        private static Rect lineSizeRectFromRectAndSide(Rect rect, DockSide side)
        {
            if (side == DockSide.Bottom)
                return new Rect(rect.Left, rect.Bottom, rect.Width, 0);
            else if (side == DockSide.Left)
                return new Rect(rect.Left, rect.Top, 0, rect.Height);
            else if (side == DockSide.Right)
                return new Rect(rect.Right, rect.Top, 0, rect.Height);
            else if (side == DockSide.Top)
                return new Rect(rect.Left, rect.Top, rect.Width, 0);
            else
                return new Rect();
        }

        private static DockSide oppositeOf(DockSide side)
        {
            if (side == DockSide.Bottom)
                return DockSide.Top;
            else if (side == DockSide.Left)
                return DockSide.Right;
            else if (side == DockSide.Right)
                return DockSide.Left;
            else if (side == DockSide.Top)
                return DockSide.Bottom;
            else
                return DockSide.Bottom;
        }

        private static bool elementIsConcealed(UIElement element, Rect elementRect, 
            Rect draggedRect, DockSide side)
        {
            Rect lineSizeRect = lineSizeRectFromRectAndSide(draggedRect, oppositeOf(side));


            foreach (UIElement childElement in MesoGrid.gridBeingDraggedIn.Children)
            {
                //If the childElement in question has a lower Zindex or comes before the element,
                //it cannot conceal the element anyway.
                if ((GetZIndex(childElement) <= GetZIndex(element)) &&
                    MesoGrid.gridBeingDraggedIn.Children.IndexOf(childElement) <=
                    MesoGrid.gridBeingDraggedIn.Children.IndexOf(element))
                    continue;

                //If the bounding box is contained completely inside another for the side in question, that
                //side is concealed.
                if ((lineSizeRect.Left > MesoGrid.childBoundingBoxes[childElement].Left &&
                     lineSizeRect.Right < MesoGrid.childBoundingBoxes[childElement].Right) &&
                    (lineSizeRect.Bottom < MesoGrid.childBoundingBoxes[childElement].Bottom &&
                     lineSizeRect.Top > MesoGrid.childBoundingBoxes[childElement].Top))
                    return true;
            }

            return false;
        }

        private static bool rectsShareInteriorSpace(Rect rect1, Rect rect2, DockSide side)
        {
            return true;               
                    
        }

        private static bool distanceIsSmallerOrEqual(Rect draggedRect, Rect rect2, ref DockSide side, ref double distance,
                                                     ref DockAlignment alignment)
        {
            bool ret = false;

            //Simplify boolean expression!
            if (Math.Abs(draggedRect.Top - rect2.Bottom) <= distance && 
                    ((draggedRect.Left > rect2.Left && draggedRect.Left < rect2.Right) || 
                      (draggedRect.Right < rect2.Right && draggedRect.Right > rect2.Left) || 
                      (draggedRect.Left < rect2.Left && draggedRect.Right > rect2.Right)))
            {
                side = DockSide.Bottom;
                distance = Math.Abs(draggedRect.Top - rect2.Bottom);
                ret = true;
            }
            if (Math.Abs(draggedRect.Right - rect2.Left) <= distance && 
                    ((draggedRect.Top > rect2.Top && draggedRect.Top < rect2.Bottom) || 
                      (draggedRect.Bottom < rect2.Bottom && draggedRect.Bottom > rect2.Top)))
            {
                side = DockSide.Left;
                distance = Math.Abs(draggedRect.Right - rect2.Left);
                ret = true;
            }
            if (Math.Abs(draggedRect.Left - rect2.Right) <= distance &&
                    ((draggedRect.Top > rect2.Top && draggedRect.Top < rect2.Bottom) ||
                      (draggedRect.Bottom < rect2.Bottom && draggedRect.Bottom > rect2.Top)))
            {
                side = DockSide.Right;
                distance = Math.Abs(draggedRect.Left - rect2.Right);
                ret = true;
            }
            if (Math.Abs(draggedRect.Bottom - rect2.Top) <= distance && 
                    ((draggedRect.Left > rect2.Left && draggedRect.Left < rect2.Right) || 
                      (draggedRect.Right < rect2.Right && draggedRect.Right > rect2.Left)))
            {
                side = DockSide.Top;
                distance = Math.Abs(draggedRect.Bottom - rect2.Top);
                ret = true;
            }

            return ret;
        }

        #endregion

        // "MesoGrid Animation Components" denotes the methods and members that help make animation easier within or on a MesoGrid.
        #region Mesogrid Animation Components
        private static int mesoGridCounter;

        private string registeredName;
        public string RegisteredName 
        {
            get
            {
                return registeredName;
            }
        }

        #endregion


        #region Constructors
        public MesoGrid()
        {
            rowDefinitions = new MesoLayoutDefinitionCollection(this);
            columnDefinitions = new MesoLayoutDefinitionCollection(this);
            SafeMode = false;
            this.MouseMove += new MouseEventHandler(MesoGrid_MouseMove);
            this.MouseDown += new MouseButtonEventHandler(MesoGrid_MouseDown);
            this.MouseUp += new MouseButtonEventHandler(globalHookMouseUp);
            //Name Registration for the MesoGrid
            //NameScope.SetNameScope(this, new NameScope());
            this.registeredName = "MesoGrid" + MesoGrid.mesoGridCounter;
          //  this.RegisterName(this.RegisteredName, this);
            MesoGrid.mesoGridCounter++;

            this.Loaded += new RoutedEventHandler(MesoGrid_Loaded);
        }

        void MesoGrid_Loaded(object sender, RoutedEventArgs e)
        {
            //Hooks.HookManager.MouseEvent += new MouseHook.MouseEventHandler(mouseHook_MouseEvent);


            //if (MesoGrid.mouseHook == null)
            //{
            //    MesoGrid.mouseHook = new MouseHook();
                //MesoGrid.mouseHook.MouseEvent += new MouseHook.MouseEventHandler(mouseHook_MouseEvent);
            //}
        }

        static MesoGrid()
        {
            //MesoGrid.mouseHook = new MouseHook();
            //MesoGrid.mouseHook.MouseEvent += new MouseHook.MouseEventHandler(mouseHook_MouseEvent);
           // Hooks.HookManager.AddHandler(typeof(MesoGrid), new MouseHook.MouseEventHandler(mouseHook_MouseEvent));

            MesoGrid.IsDragModeActive = true;
            MesoGrid.originalCoordinateWithinCell = new Point();
            HotKeyManager.RegisterPanel(null, null);
            MesoGrid.dragWithDockingPoints = false;
            MesoGrid.mesoGridCounter = 0;
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
