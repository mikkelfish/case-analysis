﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MesosoftCommon.Layout.BasicPanels
{
    public delegate void DraggingRoutedEventHandler(object sender, DraggingRoutedEventArgs e);

    public class DraggingRoutedEventArgs : RoutedEventArgs
    {
        private FrameworkElement elementBeingDragged;
        public FrameworkElement ElementBeingDragged
        {
            get
            {
                return elementBeingDragged;
            }
        }

        private bool constrainedSpaceDrag;
        public bool ConstrainedSpaceDrag
        {
            get
            {
                return constrainedSpaceDrag;
            }
        }

        private double xCoordinate;
        public double XCoordinate
        {
            get
            {
                return xCoordinate;
            }
        }

        private double yCoordinate;
        public double YCoordinate
        {
            get
            {
                return yCoordinate;
            }
        }

        private int row;
        public int Row
        {
            get
            {
                return row;
            }
        }

        private int column;
        public int Column
        {
            get
            {
                return column;
            }
        }

        private Thickness margin;
        public Thickness Margin
        {
            get
            {
                return margin;
            }
        }

        private bool isADrag;
        public bool IsADrag
        {
            get
            {
                return isADrag;
            }
        }

        private bool isAResize;
        public bool IsAResize
        {
            get
            {
                return isAResize;
            }
        }

        public DraggingRoutedEventArgs(RoutedEvent routedEvent, FrameworkElement elementBeingDragged, bool constrainedSpaceDrag, double xCoordinate,
                                        double yCoordinate, int row, int column, Thickness margin, bool isDrag, bool isResize) : base(routedEvent)
        {
            this.elementBeingDragged = elementBeingDragged;
            this.constrainedSpaceDrag = constrainedSpaceDrag;
            this.xCoordinate = xCoordinate;
            this.yCoordinate = yCoordinate;
            this.row = row;
            this.column = column;
            this.margin = margin;
            this.isADrag = isDrag;
            this.isAResize = isResize;
        }
    }
}
