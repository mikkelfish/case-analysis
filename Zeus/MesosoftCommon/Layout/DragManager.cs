﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using MesosoftCommon.Development;
using System.Windows.Input;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.IO;
using Kennedy.ManagedHooks;

namespace MesosoftCommon.Layout
{
    public class DragManager : FrameworkElement
    {
        static private Dictionary<Panel, List<FrameworkElement>> nonDraggableObjects = new Dictionary<Panel, List<FrameworkElement>>();
        static private List<Panel> managedPanels = new List<Panel>();

        static private Brush skyBlueBrush = new SolidColorBrush(
                                            Color.FromArgb(System.Drawing.Color.LightBlue.A, System.Drawing.Color.LightBlue.R,
                                            System.Drawing.Color.LightBlue.G, System.Drawing.Color.LightBlue.B));
        static private Rectangle gridHighlight;
        static private Cursor dropIcon;

        //static private MouseHook mouseHook;

        static private Grid gridBeingDraggedIn;
        static private GridLayoutHelper elementBeingDraggedOriginalGridCoordinate;

        static private Point originalCursorLocation;
        static private double originalHorizontalOffset;
        static private double originalVerticalOffset;
        static private bool useTopForCanvas;
        static private bool useLeftForCanvas;
        static private Point originalCursorLocationOnElementBeingDragged;
        static private HorizontalAlignment elementBeingDraggedOriginalHorizontalAlignment;
        static private VerticalAlignment elementBeingDraggedOriginalVerticalAlignment;

        static private Thickness elementBeingDraggedOriginalMargin;

        static private double elementBeingDraggedOpacity;
        static private Brush elementBeingDraggedOverBackground;

        #region Styles
        static private Style elementBeingDraggedOverStyle = null;
        public Style ElementBeingDraggedOverStyle
        {
            get { return DragManager.elementBeingDraggedOverStyle; }
            set { DragManager.elementBeingDraggedOverStyle = value; }
        }

        #endregion //Styles


        public static bool GetEnforceBoundariesOnDrag(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnforceBoundariesOnDragProperty);
        }
        public static void SetEnforceBoundariesOnDrag(DependencyObject obj, bool value)
        {
            obj.SetValue(EnforceBoundariesOnDragProperty, value);
        }
        // Using a DependencyProperty as the backing store for EnforceBoundariesOnDrag.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnforceBoundariesOnDragProperty =
            DependencyProperty.RegisterAttached(
              "EnforceBoundariesOnDrag",
              typeof(bool),
              typeof(DragManager),
              new PropertyMetadata(true)
            );

        static private bool isDragModeActive = false;
        static public bool IsDragModeActive
        {
            get { return DragManager.isDragModeActive; }
            set { DragManager.isDragModeActive = value; }
        }

        static private Panel panelBeingDraggedIn;
        public Panel PanelBeingDraggedIn
        {
            get { return DragManager.panelBeingDraggedIn; }
            set { DragManager.panelBeingDraggedIn = value; }
        }

        static private FrameworkElement elementBeingDragged;
        public static FrameworkElement ElementBeingDragged
        {
            get
            {
                if (!DragManager.IsDragModeActive)
                    return null;
                return DragManager.elementBeingDragged;
            }
            protected set
            {
                if (DragManager.elementBeingDragged != null)
                    DragManager.elementBeingDragged.ReleaseMouseCapture();

                if (!DragManager.IsDragModeActive)
                    DragManager.elementBeingDragged = null;
                else
                {
                    DragManager.elementBeingDragged = value;
                    DragManager.elementBeingDragged.CaptureMouse();
                }
            }
        }

        static private FrameworkElement elementBeingDraggedOver;
        private static FrameworkElement InternalElementBeingDraggedOver
        {
            get 
            {
                if (!DragManager.IsDragModeActive)
                    return null;
                return DragManager.elementBeingDraggedOver; 
            }
            set
            {
                FrameworkElement oldValue = DragManager.elementBeingDraggedOver;
                if (!DragManager.IsDragModeActive)
                    DragManager.elementBeingDraggedOver = null;
                else
                    DragManager.elementBeingDraggedOver = value;
                
                if(!(oldValue == DragManager.elementBeingDraggedOver))
                    DragManager.elementBeingDraggedOverChanged(oldValue, DragManager.elementBeingDraggedOver);
            }
        }
        [Todo("Ian", "7/18/07", Comments = "Replace temporary hardcoded style with dynamic style once Unified Resource Dictionary is complete.")]
        private static void elementBeingDraggedOverChanged(FrameworkElement oldValue, FrameworkElement newValue)
        {
            #region Temporary Code (Hardcoded style)
            //TEMPORARY CODE: Hardcoded style until the unified resource library is complete
            Control oldControl = oldValue as Control;
            Control newControl = newValue as Control;
            Panel oldPanel = oldValue as Panel;
            Panel newPanel = newValue as Panel;

            if (oldControl != null)
                oldControl.Background = DragManager.elementBeingDraggedOverBackground;
            if (oldPanel != null)
                oldPanel.Background = DragManager.elementBeingDraggedOverBackground;

            if (newControl != null)
            {
                DragManager.elementBeingDraggedOverBackground = newControl.Background;
                Style s = DragManager.elementBeingDraggedOverStyle;
                newControl.Background = DragManager.skyBlueBrush;
            }
            if (newPanel != null)
            {
                DragManager.elementBeingDraggedOverBackground = newPanel.Background;

                newPanel.Background = DragManager.skyBlueBrush;
            }
            #endregion
        }

        static private GridLayoutHelper gridCoordinateMouseIsOver;
        private static GridLayoutHelper InternalGridCoordinateMouseIsOver
        {
            get
            {
                if (!DragManager.IsDragModeActive)
                    return null;
                return DragManager.gridCoordinateMouseIsOver;
            }
            set
            {
                GridLayoutHelper oldValue = DragManager.gridCoordinateMouseIsOver;
                if (!DragManager.IsDragModeActive)
                    DragManager.gridCoordinateMouseIsOver = null;
                else
                    DragManager.gridCoordinateMouseIsOver = value;
                if (!(oldValue == DragManager.gridCoordinateMouseIsOver))
                    DragManager.gridCoordinateMouseIsOverChanged(DragManager.gridCoordinateMouseIsOver);
            }
        }
        [Todo("Ian", "7/18/07", Comments = "Replace temporary hardcoded style with dynamic style once Unified Resource Dictionary is complete.")]
        private static void gridCoordinateMouseIsOverChanged(GridLayoutHelper value)
        {
            Grid hostGrid = DragManager.panelBeingDraggedIn as Grid;

            if (hostGrid == null || value == null)
            {
                if (DragManager.gridBeingDraggedIn != null)
                {
                    //if the host grid or gridcoordinate has been lost, it means the gridHighlight needs to be taken off of the hostgrid.
                    DragManager.gridBeingDraggedIn.Children.Remove(DragManager.gridHighlight);
                    DragManager.gridBeingDraggedIn = null;
                }
                return;
            }

            #region Temporary Code (Hardcoded style)
            //TEMPORARY CODE: Hardcoded style until the unified resource library is complete
            //gridHighlight is a rectangle that's instantiated once and then moved around and resized as necessary to highlight the grid
            if (DragManager.gridHighlight == null)
            {
                DragManager.gridHighlight = new Rectangle();
                gridHighlight.Fill = DragManager.skyBlueBrush;
            }
            #endregion

            if (!hostGrid.Children.Contains(DragManager.gridHighlight))
                hostGrid.Children.Insert(0, DragManager.gridHighlight);

            //Put the highlight rectangle on the grid
            Grid.SetRow(DragManager.gridHighlight, value.Row);
            Grid.SetColumn(DragManager.gridHighlight, value.Column);

            //Move the element to its new grid location
            Grid.SetRow(DragManager.elementBeingDragged, value.Row);
            Grid.SetColumn(DragManager.elementBeingDragged, value.Column);
        }

        static private bool isDragInProcess = false;
        public static bool IsDragInProcess
        {
            get { return DragManager.isDragInProcess; }
            set { DragManager.isDragInProcess = value; }
        }
	
        public static bool GetIsDraggable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsDraggableProperty);
        }
        public static void SetIsDraggable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsDraggableProperty, value);
        }
        // Using a DependencyProperty as the backing store for IsDraggable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsDraggableProperty =
            DependencyProperty.RegisterAttached(
              "IsDraggable",
              typeof(bool),
              typeof(MesosoftCommon.Layout.DragManager),
              new PropertyMetadata(new PropertyChangedCallback(DragManager.isDraggablePropertyChanged))
            );
        protected static void isDraggablePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            bool newValue = (bool)e.NewValue;
            if (obj is Panel)
            {
                Panel callingPanel = obj as Panel;
                //When a Panel is set to be Draggable, it's added to the managedPanels list, and a callback is added to handle
                //hit detection on mousedownpreview.
                if ((newValue == true) && !(DragManager.managedPanels.Contains(callingPanel)))
                {
                    DragManager.managedPanels.Add(callingPanel);

                    callingPanel.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(callingPanel_PreviewMouseLeftButtonDown);
                    callingPanel.PreviewMouseMove += new System.Windows.Input.MouseEventHandler(callingPanel_PreviewMouseMove);
                }
                if ((newValue == false) && (DragManager.managedPanels.Contains(callingPanel)))
                {
                    DragManager.managedPanels.Remove(callingPanel);
                    callingPanel.PreviewMouseLeftButtonDown -= new System.Windows.Input.MouseButtonEventHandler(callingPanel_PreviewMouseLeftButtonDown);
                }
            }
            else if(obj is FrameworkElement)
            {
                FrameworkElement callingElement = obj as FrameworkElement;
                Panel parentPanel = DragManager.FindParentPanel(callingElement);
                //If a non-panel's Draggable property is being altered, it must belong to a Draggable panel for there to be any effect.
                //The parent (draggable) panel is located and the element is added or removed from the nonDraggableObjects Dictionary.
                if(parentPanel != null)
                {
                    if ((newValue == true) && (DragManager.nonDraggableObjects.ContainsKey(parentPanel))
                        && (DragManager.nonDraggableObjects[parentPanel].Contains(callingElement)))
                        DragManager.nonDraggableObjects[parentPanel].Remove(callingElement);
                    if (newValue == false) 
                    {
                        if(!(DragManager.nonDraggableObjects.ContainsKey(parentPanel)))
                            DragManager.nonDraggableObjects.Add(parentPanel,new List<FrameworkElement>());
                        if (!(DragManager.nonDraggableObjects[parentPanel].Contains(callingElement)))
                            DragManager.nonDraggableObjects[parentPanel].Add(callingElement);
                    }
                }
            }
        }


        [Todo("Ian", "7/18/07", Comments = "Replace temporary hardcoded style with dynamic style once Unified Resource Dictionary is complete.")]
        static void callingPanel_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!(DragManager.IsDragModeActive)) return;
            
            DragManager.isDragInProcess = false;

            DragManager.panelBeingDraggedIn = DragManager.FindParentManagedPanel(e.Source as DependencyObject);
            if (DragManager.panelBeingDraggedIn == null) return;

            DragManager.originalCursorLocation = e.GetPosition(DragManager.panelBeingDraggedIn);

            DragManager.gridBeingDraggedIn = (DragManager.panelBeingDraggedIn as Grid);
            DragManager.elementBeingDragged = DragManager.FindManagedChild(e.Source as DependencyObject, DragManager.panelBeingDraggedIn);
            if (DragManager.elementBeingDragged == null) return;

            DragManager.originalCursorLocationOnElementBeingDragged = Mouse.GetPosition(DragManager.elementBeingDragged);
            DragManager.elementBeingDraggedOriginalHorizontalAlignment = DragManager.elementBeingDragged.HorizontalAlignment;
            DragManager.elementBeingDraggedOriginalVerticalAlignment = DragManager.elementBeingDragged.VerticalAlignment;

            if (DragManager.panelBeingDraggedIn.GetType().FullName == "System.Windows.Controls.Canvas")
            {
                // Get the element's offsets from the four sides of the Canvas.
                double left = Canvas.GetLeft(DragManager.elementBeingDragged);
                double right = Canvas.GetRight(DragManager.elementBeingDragged);
                double top = Canvas.GetTop(DragManager.elementBeingDragged);
                double bottom = Canvas.GetBottom(DragManager.elementBeingDragged);

                // Calculate the offset deltas and determine for which sides
                // of the Canvas to adjust the offsets.
                DragManager.originalHorizontalOffset = resolveOffset(left, right, out DragManager.useLeftForCanvas);
                DragManager.originalVerticalOffset = resolveOffset(top, bottom, out DragManager.useTopForCanvas);

                DragManager.bringToFrontOfActivePanel(DragManager.elementBeingDragged);
            }

            if (DragManager.panelBeingDraggedIn.GetType().FullName == "System.Windows.Controls.Grid")
            {
                //DragManager.elementBeingDraggedOriginalGridCoordinate = GridLayoutHelper.LocateMouseOnGrid(DragManager.gridBeingDraggedIn);

                //Store the OriginalMargin
                DragManager.elementBeingDraggedOriginalMargin = DragManager.elementBeingDragged.Margin;

                DragManager.bringToFrontOfActivePanel(DragManager.elementBeingDragged);
            }

            e.Handled = true;

            #region Temporary Code (Hardcoded style)
            //TEMPORARY CODE: Hardcoded style until the unified resource library is complete
            DragManager.elementBeingDraggedOpacity = DragManager.elementBeingDragged.Opacity;
            DragManager.elementBeingDragged.Opacity = 0.5;
            #endregion

            //Install the hook
           // DragManager.mouseHook.InstallHook();

            DragManager.elementBeingDragged.MouseUp += new MouseButtonEventHandler(elementBeingDragged_MouseUp);
            DragManager.elementBeingDragged.CaptureMouse();


            DragManager.isDragInProcess = true;
        }

        static void elementBeingDragged_MouseUp(object sender, MouseButtonEventArgs e)
        {
            globalHookMouseUp();
        }



        private static double resolveOffset(double side1, double side2, out bool useSide1)
        {
            // If the Canvas.Left and Canvas.Right attached properties 
            // are specified for an element, the 'Left' value is honored.
            // The 'Top' value is honored if both Canvas.Top and 
            // Canvas.Bottom are set on the same element. If one 
            // of those attached properties is not set on an element, 
            // the default value is Double.NaN.
            useSide1 = true;
            double result;
            if (Double.IsNaN(side1))
            {
                if (Double.IsNaN(side2))
                {
                    // Both sides have no value, so set the
                    // first side to a value of zero.
                    result = 0;
                }
                else
                {
                    result = side2;
                    useSide1 = false;
                }
            }
            else
            {
                result = side1;
            }
            return result;
        }

        [Todo("Ian", "7/18/07", Comments = "Replace temporary hardcoded style with dynamic style once Unified Resource Dictionary is complete.")]
        static void callingPanel_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (!(DragManager.IsDragModeActive)) return;

            //A drag must be in process and must know what it's dragging, and what panel it's in, to do anything here.
            if (!DragManager.isDragInProcess || DragManager.elementBeingDragged == null || DragManager.panelBeingDraggedIn == null)
                return;

            #region Canvas Handling
            //A canvas doesn't really behave like other panels since it doesn't have predefined "drop zones."
            //Therefore, move the object itself when it is dragged (ie: no snapping to a drop zone).
            if (DragManager.panelBeingDraggedIn.GetType().FullName == "System.Windows.Controls.Canvas")
            {
                // Get the position of the mouse cursor, relative to the Canvas.
                Point cursorLocation = Mouse.GetPosition(DragManager.panelBeingDraggedIn);

                // These values will store the new offsets of the drag element.
                double newHorizontalOffset, newVerticalOffset;

                #region Calculate Offsets

                // Determine the horizontal offset.
                if (DragManager.useLeftForCanvas)
                    newHorizontalOffset = DragManager.originalHorizontalOffset + (cursorLocation.X - DragManager.originalCursorLocation.X);
                else
                    newHorizontalOffset = DragManager.originalHorizontalOffset - (cursorLocation.X - DragManager.originalCursorLocation.X);

                // Determine the vertical offset.
                if (DragManager.useTopForCanvas)
                    newVerticalOffset = DragManager.originalVerticalOffset + (cursorLocation.Y - DragManager.originalCursorLocation.Y);
                else
                    newVerticalOffset = DragManager.originalVerticalOffset - (cursorLocation.Y - DragManager.originalCursorLocation.Y);

                #endregion // Calculate Offsets

                if (DragManager.GetEnforceBoundariesOnDrag(DragManager.panelBeingDraggedIn))
                {
                    #region Verify Drag Element Location

                    // Get the bounding rect of the drag element.
                    Rect elemRect = DragManager.CalculateDragElementRect(newHorizontalOffset, newVerticalOffset);

                    //
                    // If the element is being dragged out of the viewable area, 
                    // determine the ideal rect location, so that the element is 
                    // within the edge(s) of the canvas.
                    //
                    bool leftAlign = elemRect.Left < 0;
                    bool rightAlign = elemRect.Right > DragManager.panelBeingDraggedIn.ActualWidth;

                    if (leftAlign)
                        newHorizontalOffset = DragManager.useLeftForCanvas ? 0 : DragManager.panelBeingDraggedIn.ActualWidth - elemRect.Width;
                    else if (rightAlign)
                        newHorizontalOffset = DragManager.useLeftForCanvas ? DragManager.panelBeingDraggedIn.ActualWidth - elemRect.Width : 0;

                    bool topAlign = elemRect.Top < 0;
                    bool bottomAlign = elemRect.Bottom > DragManager.panelBeingDraggedIn.ActualHeight;

                    if (topAlign)
                        newVerticalOffset = DragManager.useTopForCanvas ? 0 : DragManager.panelBeingDraggedIn.ActualHeight - elemRect.Height;
                    else if (bottomAlign)
                        newVerticalOffset = DragManager.useTopForCanvas ? DragManager.panelBeingDraggedIn.ActualHeight - elemRect.Height : 0;

                    #endregion // Verify Drag Element Location
                }

                #region Move Drag Element

                if (DragManager.useLeftForCanvas)
                    Canvas.SetLeft(DragManager.elementBeingDragged, newHorizontalOffset);
                else
                    Canvas.SetRight(DragManager.elementBeingDragged, newHorizontalOffset);

                if (DragManager.useTopForCanvas)
                    Canvas.SetTop(DragManager.elementBeingDragged, newVerticalOffset);
                else
                    Canvas.SetBottom(DragManager.elementBeingDragged, newVerticalOffset);

                #endregion // Move Drag Element
                return;
            }
            #endregion

            #region Grid Handling
            //A grid is still similar to a canvas in its handling, though it does have "drop zones."
            //Within a given "drop zone," the elementBeingDragged can move around (by altering its margins).
            //Snap to the Grid Row/Column when the boundaries are crossed, and move highlighting...
            if (DragManager.panelBeingDraggedIn.GetType().FullName == "System.Windows.Controls.Grid")
            {
                //DragManager.InternalGridCoordinateMouseIsOver = GridLayoutHelper.LocateMouseOnGrid(DragManager.gridBeingDraggedIn);

                //If both alignments are stretch and there are no margins, there's nothing to do here
                if (DragManager.elementBeingDragged.HorizontalAlignment == HorizontalAlignment.Stretch &&
                    DragManager.elementBeingDragged.VerticalAlignment == VerticalAlignment.Stretch &&
                    Double.IsNaN(DragManager.elementBeingDragged.Height) &&
                    Double.IsNaN(DragManager.elementBeingDragged.Width)&&
                    DragManager.elementBeingDragged.Margin.Top == 0 &&
                    DragManager.elementBeingDragged.Margin.Bottom == 0 &&
                    DragManager.elementBeingDragged.Margin.Left == 0 &&
                    DragManager.elementBeingDragged.Margin.Right == 0) return;


                Point desiredNewTopLeftCoordinate = Mouse.GetPosition(DragManager.gridBeingDraggedIn);
                desiredNewTopLeftCoordinate.X -= DragManager.originalCursorLocationOnElementBeingDragged.X;
                desiredNewTopLeftCoordinate.Y -= DragManager.originalCursorLocationOnElementBeingDragged.Y;

                DragManager.InternalGridCoordinateMouseIsOver.PositionElementWithinGrid(DragManager.elementBeingDragged,
                        desiredNewTopLeftCoordinate, DragManager.GetEnforceBoundariesOnDrag(DragManager.gridBeingDraggedIn)
                        , DragManager.elementBeingDraggedOriginalHorizontalAlignment,
                        DragManager.elementBeingDraggedOriginalVerticalAlignment, false);

                return;
            }
            #endregion

            #region StackPanel/WrapPanel handling
            if (DragManager.panelBeingDraggedIn.GetType().FullName == "System.Windows.Controls.StackPanel" ||
                    DragManager.panelBeingDraggedIn.GetType().FullName == "System.Windows.Controls.WrapPanel")
            {
                //The remaining two panel types, StackPanel and WrapPanel, behave in a far more restrictive fashion.
                //Margins aren't managed by the dragManager here, so placement is purely based on the drop zones.
                //Uniquely of the three sub-types, Stack and WrapPanels care what element they are over.

                bool found = false;

                //Determine what element is currently being dragged over
                foreach (FrameworkElement child in DragManager.panelBeingDraggedIn.Children)
                {
                    if (child.IsMouseOver)
                    {
                        DragManager.InternalElementBeingDraggedOver = child;
                        found = true;
                        Mouse.SetCursor(DragManager.dropIcon);                        
                        break;
                    }
                }
                if (!found) Mouse.SetCursor(Cursors.Arrow);
            }
            #endregion
        }


        /// <summary>
        /// Returns a Rect which describes the bounds of the element being dragged.
        /// </summary>
        private static Rect CalculateDragElementRect(double newHorizOffset, double newVertOffset)
        {
            Size elemSize = DragManager.elementBeingDragged.RenderSize;

            double x, y;

            if (DragManager.useLeftForCanvas)
                x = newHorizOffset;
            else
                x = DragManager.panelBeingDraggedIn.ActualWidth - newHorizOffset - elemSize.Width;

            if (DragManager.useTopForCanvas)
                y = newVertOffset;
            else
                y = DragManager.panelBeingDraggedIn.ActualHeight - newVertOffset - elemSize.Height;

            Point elemLoc = new Point(x, y);

            return new Rect(elemLoc, elemSize);
        }

        static void globalHookMouseUp()
        {
            if (!(DragManager.IsDragModeActive)) return;
            //Don't do any position shifting if the ElementBeingDragged is the same as the ElementBeingDraggedOver
            if ((DragManager.elementBeingDragged != DragManager.elementBeingDraggedOver) &&
                (DragManager.elementBeingDragged != null))
            {
                //Here code in position shifting based on which type of panel the element is being dragged in
                switch (DragManager.panelBeingDraggedIn.GetType().FullName)
                {
                    case "System.Windows.Controls.Canvas":
                        break;
                    case "System.Windows.Controls.Grid":
                        break;
                    case "System.Windows.Controls.StackPanel":
                    case "System.Windows.Controls.WrapPanel":
                        if (DragManager.elementBeingDraggedOver == null) break;

                        DragManager.moveIndex(DragManager.panelBeingDraggedIn.Children, DragManager.panelBeingDraggedIn.Children.IndexOf(elementBeingDragged),
                             DragManager.panelBeingDraggedIn.Children.IndexOf(elementBeingDraggedOver));
                        break;
                    default:
                        break;
                }
            }

            //"Drag ended" actions, do these regardless of whether or not the state changed.
            if (DragManager.elementBeingDragged != null) DragManager.elementBeingDragged.Opacity = DragManager.elementBeingDraggedOpacity;
            DragManager.InternalGridCoordinateMouseIsOver = null;
            DragManager.elementBeingDragged.MouseUp -= new MouseButtonEventHandler(elementBeingDragged_MouseUp);
            DragManager.elementBeingDragged.ReleaseMouseCapture();
            DragManager.elementBeingDragged = null;
            DragManager.panelBeingDraggedIn = null;
            DragManager.InternalElementBeingDraggedOver = null;
            DragManager.isDragInProcess = false;

            //Uninstall the hook
           // DragManager.mouseHook.UninstallHook();
        }

        static void moveIndex(UIElementCollection collection, int indexToMove, int indexToMoveTo)
        {
            UIElement elementToMove = collection[indexToMove];

            if ((indexToMove < 0) || (indexToMoveTo < 0) || (indexToMove > (collection.Count - 1)) || (indexToMoveTo > (collection.Count - 1)))
                throw new Exception("MesosoftCommon.Layout.DragManager:moveIndex attempted to access indexes outside collection range!");

            collection.RemoveAt(indexToMove);
            if (indexToMoveTo == collection.Count)
                collection.Add(elementToMove);
            else
                collection.Insert(indexToMoveTo, elementToMove);
        }

        private static void bringToFrontOfActivePanel(UIElement element)
        {
            #region Calculate Z-Indici
            // Determine the Z-Index for the target UIElement.
            int elementNewZIndex = -1;

            foreach (UIElement elem in DragManager.panelBeingDraggedIn.Children)
                if (elem.Visibility != Visibility.Collapsed)
                    ++elementNewZIndex;

            int elementCurrentZIndex = Canvas.GetZIndex(element);
            #endregion // Calculate Z-Indici

            #region Update Z-Indici
            // Update the Z-Index of every UIElement in the Canvas.
            foreach (UIElement childElement in DragManager.panelBeingDraggedIn.Children)
            {
                if (childElement == element)
                    Canvas.SetZIndex(element, elementNewZIndex);
                else
                {
                    int zIndex = Canvas.GetZIndex(childElement);

                    // Only modify the z-index of an element if it is  
                    // in between the target element's old and new z-index.
                    if (elementCurrentZIndex < zIndex)
                    {
                        Canvas.SetZIndex(childElement, zIndex - 1);
                    }
                }
            }
            #endregion // Update Z-Indici
        }

        /// <summary>
        /// Walks up the visual tree starting with the specified DependencyObject, looking for a Panel which is the parent.
        /// If the Panel found is not managed by the DragManager, return null.
        /// </summary>
        /// <param name="depObj">
        /// A DependencyObject from which the search begins.
        /// </param>
        public static Panel FindParentPanel(DependencyObject depObj)
        {
            while (depObj != null)
            {
                FrameworkElement elem = depObj as FrameworkElement;
                if ((elem != null) && (elem is Panel))
                {
                    if (!(DragManager.managedPanels.Contains(elem as Panel)))
                        depObj = null;
                    break;
                }

                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.
                if (depObj is System.Windows.Media.Visual || depObj is System.Windows.Media.Media3D.Visual3D)
                    depObj = System.Windows.Media.VisualTreeHelper.GetParent(depObj);
                else
                    depObj = LogicalTreeHelper.GetParent(depObj);
            }
            return depObj as Panel;
        }

        public static Panel FindParentManagedPanel(DependencyObject depObj)
        {
            while (depObj != null)
            {
                FrameworkElement elem = depObj as FrameworkElement;
                if ((elem != null) && (elem is Panel) && DragManager.managedPanels.Contains(elem as Panel))
                    break;

                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.
                if (depObj is System.Windows.Media.Visual || depObj is System.Windows.Media.Media3D.Visual3D)
                    depObj = System.Windows.Media.VisualTreeHelper.GetParent(depObj);
                else
                    depObj = LogicalTreeHelper.GetParent(depObj);
            }
            return depObj as Panel;
        }

        public static FrameworkElement FindManagedChild(DependencyObject depObj, Panel managedPanel)
        {
            while (depObj != null)
            {
                FrameworkElement elem = depObj as FrameworkElement;
                FrameworkElement parent;
                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.
                if (depObj is System.Windows.Media.Visual || depObj is System.Windows.Media.Media3D.Visual3D)
                    parent = System.Windows.Media.VisualTreeHelper.GetParent(depObj) as FrameworkElement;
                else
                    parent = LogicalTreeHelper.GetParent(depObj) as FrameworkElement;

                if ((parent != null) && (parent is Panel) && DragManager.managedPanels.Contains(parent as Panel))
                    break;
                depObj = parent;
            }
            return depObj as FrameworkElement;
        }

        static DragManager()
        {

            //DragManager.mouseHook = new MouseHook();
            //DragManager.mouseHook.MouseEvent += new MouseHook.MouseEventHandler(mouseHook_MouseEvent);
           // Application.Current.Exit += new ExitEventHandler(Current_Exit);

            //Temporary hardcoded icons get initialized here
            if (DragManager.dropIcon == null)
            {
                DragManager.dropIcon = new Cursor(Directory.GetCurrentDirectory() + "\\Layout\\Resources\\DragOver.cur");
            }
        }

        //static void mouseHook_MouseEvent(MouseEvents mEvent, System.Drawing.Point point)
        //{
        //    if (mEvent == MouseEvents.LeftButtonUp || mEvent == MouseEvents.RightButtonUp)
        //    {
        //        DragManager.globalHookMouseUp();
        //    }
        //}

        //static void Current_Exit(object sender, ExitEventArgs e)
        //{
        //    //Dispose the mouseHook
        //    if (mouseHook != null)
        //    {
        //        mouseHook.Dispose();
        //        mouseHook = null;
        //    }
        //}
    }
}
