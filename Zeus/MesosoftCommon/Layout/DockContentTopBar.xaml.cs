﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace MesosoftCommon.Layout
{
    /// <summary>
    /// Interaction logic for DockContentTopBar.xaml
    /// </summary>
    public partial class DockContentTopBar : UserControl, INotifyPropertyChanged
    {
        private Color buttonBackgroundNotFocused = Color.FromRgb(236, 233, 216);
        private Color buttonBackgroundFocused = Color.FromRgb(156, 182, 231);
        private Color buttonBorderNotFocused = Color.FromRgb(140, 134, 123);
        private Color buttonBorderFocused = Color.FromRgb(60, 90, 170);
        private Color gridBackgroundFocusedGradientTop = Color.FromRgb(59, 128, 237);
        private Color gridBackgroundFocusedGradientBottom = Color.FromRgb(49, 106, 197);
        private Color gridBackgroundNotFocused = Color.FromRgb(204, 199, 186);


        private string textValue;
        public string TextValue
        {
            get
            {
                return textValue;
            }
            set
            {
                textValue = value;
                this.notifyPropertyChanged("TextValue");
            }
        }

        private bool groupIsFocused;
        public bool GroupIsFocused
        {
            get
            {
                return groupIsFocused;
            }
            set
            {
                groupIsFocused = value;

                if (groupIsFocused)
                {
                    ToggleRotate = 0;
                    ButtonBackgroundColor = new SolidColorBrush(this.buttonBackgroundFocused);
                    ButtonBorderColor = new SolidColorBrush(this.buttonBorderFocused);
                    GeometryColor = new SolidColorBrush(Colors.White);
                    GridBackground = new LinearGradientBrush(this.gridBackgroundFocusedGradientTop, this.gridBackgroundFocusedGradientBottom, 0);
                }
                else
                {
                    ToggleRotate = 0;
                    ButtonBackgroundColor = new SolidColorBrush(this.buttonBackgroundNotFocused);
                    ButtonBorderColor = new SolidColorBrush(this.buttonBorderNotFocused);
                    GeometryColor = new SolidColorBrush(Colors.Black);
                    GridBackground = new SolidColorBrush(this.gridBackgroundNotFocused);
                }
            }
        }

        public bool IsPinned
        {
            get { return (bool)GetValue(IsPinnedProperty); }
            set { SetValue(IsPinnedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsPinned.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsPinnedProperty =
            DependencyProperty.Register(
              "IsPinned", typeof(bool),
              typeof(DockContentTopBar),
              new PropertyMetadata(true,new PropertyChangedCallback(DockContentTopBar.isPinnedChanged))
            );

        private static void isPinnedChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            bool isPinned = (bool)e.NewValue;

            if (!(obj is DockContentTopBar)) return;

            DockContentTopBar self = obj as DockContentTopBar;

            if (isPinned)
                self.ToggleRotate = 0;
            else
                self.ToggleRotate = 90;
            self.notifyPropertyChanged("ToggleRotate");
        }

        public Double ToggleRotate { get; set; }

        private SolidColorBrush buttonBackgroundColor;
        public SolidColorBrush ButtonBackgroundColor
        {
            get
            {
                return buttonBackgroundColor;
            }
            set
            {
                buttonBackgroundColor = value;
                this.notifyPropertyChanged("ButtonBackgroundColor");
            }
        }
        private SolidColorBrush buttonBorderColor;
        public SolidColorBrush ButtonBorderColor
        {
            get
            {
                return buttonBorderColor;
            }
            set
            {
                buttonBorderColor = value;
                this.notifyPropertyChanged("ButtonBorderColor");
            }
        }

        private SolidColorBrush geometryColor;
        public SolidColorBrush GeometryColor
        {
            get
            {
                return geometryColor;
            }
            set
            {
                geometryColor = value;
                this.notifyPropertyChanged("GeometryColor");
            }
        }

        private Brush gridBackground;
        public Brush GridBackground
        {
            get
            {
                return gridBackground;
            }
            set
            {
                gridBackground = value;
                this.notifyPropertyChanged("GridBackground");
            }
        }

        public DockContentTopBar()
        {
            InitializeComponent();
            ToggleRotate = 0;
            ButtonBackgroundColor = new SolidColorBrush(this.buttonBackgroundNotFocused);
            ButtonBorderColor = new SolidColorBrush(this.buttonBorderNotFocused);
            GeometryColor = new SolidColorBrush(Colors.Black);
            GridBackground = new SolidColorBrush(this.gridBackgroundNotFocused);
        }

        public void SetGroupFocus(object sender, RoutedEventArgs e)
        {
            if (this.IsFocused || this.OptionButton.IsFocused || this.PinButton.IsFocused || this.CloseButton.IsFocused
                || this.TopGrid.IsFocused || this.Text.IsFocused )
                this.GroupIsFocused = true;
            else
                this.GroupIsFocused = false;
        }

        public void GetFocus(object sender, MouseEventArgs e)
        {
            this.Focus();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void notifyPropertyChanged(string name)
        {
            PropertyChangedEventHandler handle = this.PropertyChanged;
            if (handle == null) return;
            handle(this, new PropertyChangedEventArgs(name));
        }

        #endregion

    }
}
