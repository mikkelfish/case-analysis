﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.AddIns
{
    [global::System.AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class AddInAttribute : AddInBaseAttribute
    {
        public bool RequiresInstanceInformation { get; set; }
        public string Name { get; private set; }
        public double Version { get; private set; }

        public AddInAttribute(string name, double version)
        {
            this.Name = name;
            this.Version = version;
        }
    }
}
