﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.AddIns.Attributes
{
    [global::System.AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
    public sealed class RequireAddInVersionAttribute : Attribute
    {
        public string Name { get; private set; }
        public double Version { get; private set; }

        public RequireAddInVersionAttribute(string name, double version)
        {
            this.Name = name;
            this.Version = version;
        }
    }
}
