﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.AddIns
{
    [global::System.AttributeUsage(AttributeTargets.Interface, Inherited = true, AllowMultiple = false)]
    public class AddInContractAttribute : AddInBaseAttribute
    {

    }
}
