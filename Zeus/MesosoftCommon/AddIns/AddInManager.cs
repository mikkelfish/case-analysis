﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.IO;
using MesosoftCommon.Utilities.XAML;
using MesosoftCommon.Utilities.Settings;
using System.IO;
using System.Reflection;
using MesosoftCommon.Utilities.Reflection;
using MesosoftCommon.Development;
using MesosoftCommon.AddIns.Interfaces;

namespace MesosoftCommon.AddIns
{
    public static class AddInManager
    {
        private class addInInfo
        {
            public string Path { get; set; }
            public double Version { get; set; }
            public bool CanEncapsulate { get; set; }
            public bool RequiresInstance { get; set; }

            public Guid Supports { get; set; }
            public Guid SubclassedOff { get; set; }


            public string Name { get; set; }
            public Guid ID { get; set; }

            public string CLRType { get; set; }
        }

        private static UniversalListCollection<UniversalItemWrapper<addInInfo>> addins =
            UniversalCollectionManager.CreateCollection<UniversalListCollection<UniversalItemWrapper<addInInfo>>>("FoundAddIns");
        private static UniversalListCollection<UniversalItemWrapper<addInInfo>> other =
            UniversalCollectionManager.CreateCollection<UniversalListCollection<UniversalItemWrapper<addInInfo>>>("Other");

        private static bool completedHash = false;
        private static Dictionary<Type, List<object>> hashedTokens = new Dictionary<Type, List<object>>();

        private static Dictionary<Type, IAddInContract> staticAddInAdapters = new Dictionary<Type, IAddInContract>();

        private static void foundType(Type type, string path)
        {
            object[] attrs = type.GetCustomAttributes(typeof(AddInBaseAttribute), false);
            if(attrs.Length != 1) return;
            AddInBaseAttribute attr = attrs[0] as AddInBaseAttribute;

            addInInfo info = null;

            if (attr is AddInAttribute)
            {
                AddInAttribute addInAttr = attr as AddInAttribute;

                info =
                    (from addIn in addins
                     where addIn.Value.Name == addInAttr.Name &&
                         addIn.Value.Version == addInAttr.Version
                     select addIn.Value).SingleOrDefault<addInInfo>();


                if (info == null)
                {
                    info = new addInInfo()
                    {
                        Version = addInAttr.Version,
                        Name = addInAttr.Name,
                        ID = Guid.NewGuid()
                    };

                    addins.Add(info);
                }

                info.RequiresInstance = addInAttr.RequiresInstanceInformation;

            }
            else
            {
                info =
                    (from foundInfo in other
                     where foundInfo.Value.Name == type.FullName
                     select foundInfo.Value).SingleOrDefault<addInInfo>();
                if (info == null)
                {
                    info = new addInInfo() { Name = type.FullName, ID = Guid.NewGuid() };
                    other.Add(info);
                }
            }

            info.Path = path;
            info.CLRType = type.AssemblyQualifiedName;

            if (attr is HostAdapterAttribute)
            {
                HostViewAttribute hostAttr = Common.GetAttribute<HostViewAttribute>(type.BaseType, false);

                //TODO LOG IF FAILURE
                if (hostAttr != null)
                {
                    #region Get Support and Subclass

                    addInInfo view =
                        (from potentialView in other
                         where potentialView.Value.Name == type.BaseType.FullName
                         select potentialView.Value).SingleOrDefault<addInInfo>();
                    if (view == null)
                    {
                        view = new addInInfo() { Name = type.BaseType.FullName, ID = Guid.NewGuid() };
                        other.Add(view);
                    }
                    info.SubclassedOff = view.ID;


                    addInInfo contractInfo = null;
                    ConstructorInfo[] infos = type.GetConstructors();
                    foreach (ConstructorInfo consInfo in infos)
                    {
                        ParameterInfo[] paras = consInfo.GetParameters();
                        if (paras.Length != 1) continue;

                        ParameterInfo contract = paras[0];

                        AddInContractAttribute contractAttr = Common.GetAttribute<AddInContractAttribute>(contract.ParameterType, true);
                        if (contractAttr == null) continue;

                        if (contractInfo != null) continue; //TODO LOG THAT THERE ARE MULTIPLE CONSTRUCTORS THAT SATISFY CONSTRAINTS

                        contractInfo =
                        (from potentialContract in other
                         where potentialContract.Value.Name == contract.ParameterType.FullName
                         select potentialContract.Value).SingleOrDefault<addInInfo>();

                        if (contractInfo == null)
                        {
                            contractInfo = new addInInfo() { Name = contract.ParameterType.FullName, ID = Guid.NewGuid() };
                            other.Add(contractInfo);
                        }
                    }

                    info.Supports = contractInfo.ID; //TODO LOG IF NULL
                    #endregion
                }

            }
            else if (attr is AddInAdapterAttribute)
            {
                AddInContractAttribute contractAttr = null;
                Type[] interfaces = type.GetInterfaces();
                Type interfaceType = null;
                foreach (Type inter in interfaces)
                {
                    contractAttr = Common.GetAttribute<AddInContractAttribute>(inter, false);
                    if (contractAttr != null)
                    {
                        interfaceType = inter;
                        break;
                    }
                }
                
                
                //TODO LOG IF NULL
                if (contractAttr != null)
                {
                    #region Get Support and Subclass

                    addInInfo contract =
                        (from potentialContract in other
                         where potentialContract.Value.Name == interfaceType.FullName
                         select potentialContract.Value).SingleOrDefault<addInInfo>();
                    if (contract == null)
                    {
                        contract = new addInInfo() { Name = interfaceType.FullName, ID = Guid.NewGuid() };
                        other.Add(contract);
                    }
                    info.SubclassedOff = contract.ID;


                    addInInfo viewInfo = null;
                    ConstructorInfo[] infos = type.GetConstructors();
                    foreach (ConstructorInfo consInfo in infos)
                    {
                        ParameterInfo[] paras = consInfo.GetParameters();
                        if (paras.Length != 1) continue;

                        ParameterInfo view = paras[0];
                        AddInViewAttribute viewAttr = Common.GetAttribute<AddInViewAttribute>(view.ParameterType, true);
                        if (viewAttr == null) continue;
                        
                        if (viewInfo != null) continue; //TODO LOG THAT THERE ARE MULTIPLE CONSTRUCTORS THAT SATISFY CONSTRAINTS

                        viewInfo =
                        (from potentialView in other
                         where potentialView.Value.Name == view.ParameterType.FullName
                         select potentialView.Value).SingleOrDefault<addInInfo>();

                        if (viewInfo == null)
                        {
                            viewInfo = new addInInfo() { Name = view.ParameterType.FullName, ID = Guid.NewGuid() };
                            other.Add(viewInfo);
                        }
                    }

                    info.Supports = viewInfo.ID; //TODO LOG IF NULL

                    #endregion
                }
            }
            else if (attr is AddInAttribute)
            {
                AddInViewAttribute viewAttr = Common.GetAttribute<AddInViewAttribute>(type.BaseType, false);
                //LOG FAILURE
                if (viewAttr != null)
                {
                    #region Get Subclass

                    addInInfo view =
                       (from potentialView in other
                        where potentialView.Value.Name == type.BaseType.FullName
                        select potentialView.Value).SingleOrDefault<addInInfo>();
                    if (view == null)
                    {
                        view = new addInInfo() { Name = type.BaseType.FullName, ID = Guid.NewGuid() };
                        other.Add(view);
                    }
                    info.SubclassedOff = view.ID;

                    #endregion
                }
            }

        }

        private static void build(string path, bool includeBase, string[] otherAddInPaths, bool throwOnRepeat, bool addInsOnly)
        {
            completedHash = false;
            hashedTokens.Clear();

            if (includeBase)
            {
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (Assembly assembly in assemblies)
                {
                    Type[] allTypes = Common.GetTypesFromAssembly(assembly, typeof(AddInBaseAttribute), null, true);
                    foreach (Type type in allTypes)
                    {
                        foundType(type, assembly.Location);
                    }
                }
            }

            if (path[path.Length - 1] != '\\') path += '\\';

            AppDomain tempDomain = AppDomain.CreateDomain("TempDomain");
            for (int i = 0; i < 6; i++)
            {
                if (i < 5 && addInsOnly)
                {
                    i = 5;
                    continue;
                }

                string fileName = "AddInAdapters";
                if (i == 1) fileName = "AddInViews";
                else if (i == 2) fileName = "Contracts";
                else if (i == 3) fileName = "HostAdapters";
                else if (i == 4) fileName = "HostViews";
                else if (i == 5) fileName = "AddIns";

                if (!Directory.Exists(path + fileName)) continue;

                string[] hostFiles = Directory.GetFiles(path + fileName, ".dll", SearchOption.AllDirectories);
                foreach (string hostFile in hostFiles)
                {
                    AssemblyName name = AssemblyName.GetAssemblyName(hostFile);
                    Assembly assembly = tempDomain.Load(name);
                    Type[] allTypes = Common.GetTypesFromAssembly(assembly, typeof(AddInBaseAttribute), null, true);
                    foreach (Type type in allTypes)
                    {
                        foundType(type, assembly.Location);
                    }
                }
            }

            if (otherAddInPaths != null)
            {
                foreach (string addInPath in otherAddInPaths)
                {
                    string[] hostFiles = Directory.GetFiles(addInPath, ".dll", SearchOption.AllDirectories);
                    foreach (string hostFile in hostFiles)
                    {
                        AssemblyName name = AssemblyName.GetAssemblyName(hostFile);
                        Assembly assembly = tempDomain.Load(name);
                        Type[] allTypes = Common.GetTypesFromAssembly(assembly, typeof(AddInBaseAttribute), null, true);
                        foreach (Type type in allTypes)
                        {
                            foundType(type, assembly.Location);
                        }
                    }
                }
            }

            AppDomain.Unload(tempDomain);
        }

        public static void BuildAll(string path, bool includeBase, string[] otherAddInPaths)
        {
            BuildAll(path, includeBase, otherAddInPaths, false);
        }

        public static void BuildAll(string path, bool includeBase, string[] otherAddInPaths, bool throwOnRepeat)
        {
            build(path, includeBase, otherAddInPaths, throwOnRepeat, false);
        }

        public static void BuildAddIns(string path, bool includeBase, string[] otherAddInPaths, bool throwOnRepeat)
        {
            build(path, includeBase, otherAddInPaths, throwOnRepeat, true);
        }

        public static AddInToken<T>[] GetAddIns<T>()
            where T:class
        {
            if(completedHash && hashedTokens.ContainsKey(typeof(T)))
            {
                AddInToken<T>[] toRet = new AddInToken<T>[hashedTokens[typeof(T)].Count];
                return hashedTokens[typeof(T)].ConvertAll<AddInToken<T>>(v => v as AddInToken<T>).ToArray();
            }


            List<AddInToken<T>> foundAddIns = new List<AddInToken<T>>();

            //First, get the ID of the view
            Guid viewID =
                (from view in other
                 where view.Value.Name == typeof(T).FullName
                 select view.Value.ID).SingleOrDefault<Guid>();

            if (viewID == Guid.Empty) return null;

            //Next, get all adapters subclassed off the view
            addInInfo[] viewAdapters =
                (from adapter in other
                 where adapter.Value.SubclassedOff == viewID
                 select adapter.Value).ToArray<addInInfo>();

            //Foreach adapter, complete the chain
            foreach (addInInfo viewAdapter in viewAdapters)
            {
                //Get contract
                addInInfo contract =
                    (from possibleContract in other
                     where possibleContract.Value.ID == viewAdapter.Supports
                     select possibleContract.Value).SingleOrDefault<addInInfo>();


                if (contract == null) continue; //TODO LOG

                addInInfo[] addInAdapters =
                    (from addInAdapter in other
                     where addInAdapter.Value.SubclassedOff == contract.ID
                     select addInAdapter.Value).ToArray<addInInfo>();

                foreach (addInInfo addInAdapter in addInAdapters)
                {
                    addInInfo addInView =
                        (from possibleView in other
                         where possibleView.Value.ID == addInAdapter.Supports
                         select possibleView.Value).SingleOrDefault<addInInfo>();

                    if (addInView == null) continue; //TODO LOG

                    //Get addins
                    var addInQuery =
                         from addIn in addins
                         where addIn.Value.SubclassedOff == addInView.ID
                         select addIn.Value;

                    foreach (addInInfo addin in addInQuery)
                    {
                        AddInToken<T> token = foundAddIns.Find(add => add.Name == addin.Name);
                        if (token == null) token = new AddInToken<T>(addin.Name);
                        token.addChain(addin.Version, new Guid[]{
                            viewID, viewAdapter.ID, contract.ID, addInAdapter.ID, addInView.ID, addin.ID});
                        foundAddIns.Add(token);
                    }

                }
            }

            completedHash = true;

            List<object> hashList = new List<object>(foundAddIns.ToArray());
            hashedTokens.Add(typeof(T), hashList);

            return foundAddIns.ToArray();

        }

        public static void ClearAllCache()
        {
            completedHash = false;
            staticAddInAdapters.Clear();
            hashedTokens.Clear();
            addins.Clear();
            other.Clear();
        }

        private static Type loadType(addInInfo info)
        {
            Type testType = Type.GetType(info.CLRType);
            bool needToLoadType = true;
            if (testType != null)
            {
                if (testType.Assembly.Location == info.Path)
                {
                    return testType;
                }
            }

            AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(info.Path));
            return Type.GetType(info.CLRType);
        }

        internal static T createAddIn<T>(Guid[] ids, AddInLoadingEnvironment environment)
            where T:class
        {
            //First, make sure everything on the view side is loaded into this appdomain
            addInInfo hostView = other.FirstOrDefault<UniversalItemWrapper<addInInfo>>(host => host.Value.ID == ids[0]);
            addInInfo hostAdapter = other.FirstOrDefault<UniversalItemWrapper<addInInfo>>(adapter => adapter.Value.ID == ids[1]);            
            addInInfo contract = other.FirstOrDefault<UniversalItemWrapper<addInInfo>>(cont => cont.Value.ID == ids[2]);
            addInInfo addInAdapter = other.FirstOrDefault<UniversalItemWrapper<addInInfo>>(adapter => adapter.Value.ID == ids[3]);
            addInInfo addInView = other.FirstOrDefault<UniversalItemWrapper<addInInfo>>(view => view.Value.ID == ids[4]);
            addInInfo addIn = addins.FirstOrDefault<UniversalItemWrapper<addInInfo>>(addin => addin.Value.ID == ids[5]);

            //View doesn't need to be loaded because obviously it is already passed in as T
            Type hostAdapterType = loadType(hostAdapter);
            Type contractType = loadType(contract);
       
            Type addInAdapterType = null;
            Type addInViewType = null;
            Type addInType = null;
            //Depending on the requested enviornment, we're going to load up the addin in a location
            if (environment == AddInLoadingEnvironment.CurrentDomain)
            {
                addInAdapterType = loadType(addInAdapter);
                addInViewType = loadType(addInView);
                addInType = loadType(addIn);
            }
            else
            {

            }

            object addInObject = null;
            IAddInContract addInAdapterObject = null;
            //First, create the addin
            if (addIn.RequiresInstance)
            {
                addInObject = addInType.InvokeMember("", BindingFlags.CreateInstance, null, null, null);

                ConstructorInfo adapterInfo = addInAdapterType.GetConstructor(new Type[] { addInViewType });
                addInAdapterObject = (IAddInContract)adapterInfo.Invoke(new object[] { addInObject });

                addInAdapterObject.RegisterInstanceAddIn();
            }
            else
            {
                if (staticAddInAdapters.ContainsKey(addInType))
                {
                    addInAdapterObject = staticAddInAdapters[addInType];
                }
                else
                {
                    addInObject = addInType.InvokeMember("", BindingFlags.CreateInstance, null, null, null);

                    ConstructorInfo adapterInfo = addInAdapterType.GetConstructor(new Type[] { addInViewType });
                    addInAdapterObject = (IAddInContract)adapterInfo.Invoke(new object[] { addInObject });

                    staticAddInAdapters.Add(addInType, addInAdapterObject);
                }
                addInAdapterObject.RegisterAddIn();
            }

            ConstructorInfo constructor = hostAdapterType.GetConstructor(new Type[] { contractType });
            if (constructor == null) return null;
            T hostAdapterObject = (T)constructor.Invoke(new object[] { addInAdapterObject });

            return hostAdapterObject;

        }
    }
}
