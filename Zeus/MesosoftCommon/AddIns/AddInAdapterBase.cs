﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.AddIns
{
    public abstract class AddInAdapterBase : Interfaces.IAddInContract
    {
        #region IAddInContract Members

        public bool EnforceVersionSecurity
        {
            get;
            set;
        }

        public void RegisterAddIn()
        {
        }

        public void UnregisterAddIn()
        {
        }

        public void RegisterInstanceAddIn()
        {
        }

        public void UnregisterInstanceAddIn()
        {
        }

        #endregion
    }
}
