﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.AddIns
{
    public class AddInToken<T> 
        where T:class
    {
        public string Name { get; private set; }

        public double[] AvailableVersions 
        { 
            get
            {
                return IDChain.Keys.ToArray<double>();
            } 
        }

        private Dictionary<double, Guid[]> idChain = new Dictionary<double, Guid[]>();
        internal Dictionary<double, Guid[]> IDChain { get { return this.idChain; } }

        internal AddInToken(string name)
        {
            this.Name = name;
        }

        internal void addChain(double version, Guid[] idChain)
        {
            this.IDChain.Add(version, idChain);   
        }

        public T CreateAddIn(AddInLoadingEnvironment environment)
        {
            return AddInManager.createAddIn<T>(this.IDChain[this.IDChain.Keys.Max()], environment);
        }

        public T CreateAddIn(AddInLoadingEnvironment environment, ref double preferredVersion, bool throwOnVersionError)
        {
            if (preferredVersion == double.MaxValue) preferredVersion = this.idChain.Keys.Max();

            bool inThere = this.IDChain.ContainsKey(preferredVersion);
            if (!inThere)
            {
                if (throwOnVersionError) throw new Exception("Version not present.");
                else preferredVersion = this.IDChain.Keys.Max();
            }

            return AddInManager.createAddIn<T>(this.IDChain[preferredVersion], environment);
        }
    }
}
