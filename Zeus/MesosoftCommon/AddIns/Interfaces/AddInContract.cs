﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.AddIns.Interfaces
{
    public interface IAddInContract
    {
        /// <summary>
        /// Called from host side
        /// </summary>
        bool EnforceVersionSecurity { get; set; }
 
        
        /// <summary>
        /// Called from Manager
        /// </summary>
        void RegisterAddIn();
        /// <summary>
        /// Called from Manager
        /// </summary>
        void UnregisterAddIn();
        /// <summary>
        /// Called from Manager
        /// </summary>
        void RegisterInstanceAddIn();
        /// <summary>
        /// Called from Manager
        /// </summary>
        void UnregisterInstanceAddIn();
    }
}
