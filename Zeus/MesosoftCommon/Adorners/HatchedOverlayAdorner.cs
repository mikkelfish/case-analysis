﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows;

namespace MesosoftCommon.Adorners
{
    public class HatchedOverlayAdorner : Adorner
    {
        static private SolidColorBrush hatchedBrush = new SolidColorBrush(Color.FromArgb(125, 29, 29, 15));

        public Point Offset { get; set; }
        public bool UseRenderSize { get; set; }

        // Be sure to call the base class constructor.
        public HatchedOverlayAdorner(UIElement adornedElement)
            : base(adornedElement) 
        { 
        }

        // A common way to implement an adorner's rendering behavior is to override the OnRender
        // method, which is called by the layout system as part of a rendering pass.
        protected override void OnRender(DrawingContext drawingContext)
        {

            // Get a rectangle that represents the desired size of the rendered element
            // after the rendering pass.  This will be used to draw at the corners of the 
            // adorned element.
            Rect adornedElementRect;

            if (UseRenderSize)
                adornedElementRect = new Rect(this.AdornedElement.RenderSize);
            else
                adornedElementRect = new Rect(this.AdornedElement.DesiredSize);

            adornedElementRect.Offset(Offset.X, Offset.Y);

            // Some arbitrary drawing implements.
            Pen renderPen = new Pen(hatchedBrush, 0);

            drawingContext.DrawRectangle(hatchedBrush, renderPen, adornedElementRect);
        }

    }
}
