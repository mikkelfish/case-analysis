﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows;
using MesosoftCommon.Layout.BasicPanels;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Globalization;

namespace MesosoftCommon.Adorners
{
    class MesoGridIsLockedStateAdorner : Adorner
    {
        public MesoGridIsLockedStateAdorner(UIElement adornedElement)
            : base(adornedElement)
        {
            this.IsHitTestVisible = false;
        }

        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            if (!(this.AdornedElement is MesoGrid))
            {
                base.OnRender(drawingContext);
                return;
            }

            MesoGrid adornedMesoGrid = this.AdornedElement as MesoGrid;
            Rect adornedElementRect = new Rect(this.AdornedElement.DesiredSize);
            Rect innerRect = new Rect(this.AdornedElement.DesiredSize);

            if (innerRect.Width > 20 && innerRect.Height > 20)
            {
                innerRect.X += 4;
                innerRect.Y += 4;
                innerRect.Height -= 8;
                innerRect.Width -= 8;
            }

            SolidColorBrush penBrush = new SolidColorBrush(Colors.Black);
            penBrush.Opacity = 0.7;
            Pen pen = new Pen(penBrush, 3);
            pen.DashStyle = new DashStyle();
            pen.DashStyle.Dashes = new DoubleCollection();
            pen.DashStyle.Dashes.Add(2);
            pen.DashStyle.Dashes.Add(8);
            pen.DashCap = PenLineCap.Square;

            NameScope.SetNameScope(this, new NameScope());

            SolidColorBrush renderBrush = new SolidColorBrush();
            ColorAnimation colorAnimation;

            if (adornedMesoGrid.IsLocked)
                colorAnimation = new ColorAnimation(Color.FromRgb(70, 0, 0), Color.FromRgb(230, 0, 0), new Duration(TimeSpan.FromSeconds(0.75)));
            else
                colorAnimation = new ColorAnimation(Color.FromRgb(0, 120, 0), Color.FromRgb(0, 230, 0), new Duration(TimeSpan.FromSeconds(0.75)));

            renderBrush.Opacity = 0.5;

            this.RegisterName("renderBrush", renderBrush);

            //ColorAnimation colorAnimation = new ColorAnimation(Colors.Red, Colors.Sienna, new Duration(new TimeSpan(0, 0, 1)));
            colorAnimation.RepeatBehavior = RepeatBehavior.Forever;
            colorAnimation.AutoReverse = true;

            Storyboard sb = new Storyboard();
            sb.Children.Add(colorAnimation);
            Storyboard.SetTargetName(colorAnimation, "renderBrush");
            Storyboard.SetTargetProperty(colorAnimation, new PropertyPath(SolidColorBrush.ColorProperty));

            drawingContext.DrawRectangle(renderBrush, null, adornedElementRect);
            drawingContext.DrawRectangle(null, pen, innerRect);

            sb.Begin(this);
        }
    }
}
