﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows;
using MesosoftCommon.Layout.BasicPanels;

namespace MesosoftCommon.Adorners
{
    class DockingTargetAdorner : Adorner
    {
        public DockSide side;

        public DockingTargetAdorner(UIElement adornedElement, DockSide dockSide)
            : base(adornedElement)
        {
            this.IsHitTestVisible = false;
            this.side = dockSide;
        }

        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            Rect adornedElementRect = new Rect(this.AdornedElement.DesiredSize);
            SolidColorBrush penBrush = new SolidColorBrush(Colors.Red);
            Pen pen = new Pen(penBrush,4);
            drawingContext.DrawLine(pen, adornedElementRect.TopLeft, adornedElementRect.BottomRight);
            drawingContext.DrawLine(pen, adornedElementRect.TopRight, adornedElementRect.BottomLeft);
            
            Point dockPointer = new Point();
            Point dockTransform = new Point();

            if (this.side == DockSide.Bottom)
            {
                dockPointer.X = adornedElementRect.Width / 2;
                dockPointer.Y = adornedElementRect.Bottom - 2;
                dockTransform.X = adornedElementRect.Width / 2;
                dockTransform.Y = adornedElementRect.Bottom - 6;
            }
            else if (this.side == DockSide.Left)
            {
                dockPointer.X = adornedElementRect.Left + 2;
                dockPointer.Y = adornedElementRect.Height / 2;
                dockTransform.X = adornedElementRect.Left + 6;
                dockTransform.Y = adornedElementRect.Height / 2;
            }
            else if (this.side == DockSide.Right)
            {
                dockPointer.X = adornedElementRect.Right - 2;
                dockPointer.Y = adornedElementRect.Height / 2;
                dockTransform.X = adornedElementRect.Right - 6;
                dockTransform.Y = adornedElementRect.Height / 2;
            }
            else if (this.side == DockSide.Top)
            {
                dockPointer.X = adornedElementRect.Width / 2;
                dockPointer.Y = adornedElementRect.Top + 2;
                dockTransform.X = adornedElementRect.Width / 2;
                dockTransform.Y = adornedElementRect.Top + 6;
            }

            drawingContext.DrawLine(pen, dockPointer, dockTransform);
        }
    }
}
