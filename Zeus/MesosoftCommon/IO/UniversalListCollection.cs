﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using System.Collections.Specialized;
using MesosoftCommon.ExtensionMethods;
using System.Reflection;

namespace MesosoftCommon.IO
{
    public class UniversalListCollection<T> : UniversalCollection<T>, Interfaces.IUniversalListCollection<T>
        where T : class, INotifyPropertyChanged, System.ComponentModel.INotifyPropertyChanging
    {

        #region Constructors
        public UniversalListCollection(IUniversalListImplementation<T> implementation)
            : base(implementation)
        {

        }

        public UniversalListCollection()
        {
            this.Implementation = UniversalCollectionManager.GetDefaultImplementation<IUniversalListImplementation<T>, T>();
        }
            
        #endregion

        private IUniversalListImplementation<T> listImp;

        protected override IUniversalCollectionImplementation<T> Implementation
        {
            get
            {
                return base.Implementation;
            }
            set
            {
                base.Implementation = value;
                this.listImp = base.Implementation as IUniversalListImplementation<T>;
            }
        }

        #region Private wrappers
        private int indexOf(T item)
        {
            return this.listImp.IndexOf(item);
        }

        private void insert(int index, T item)
        {
            this.listImp.Insert(index, item);
            this.OnCollectionChanged(
                new NotifyCollectionChangedEventArgs(
                        System.Collections.Specialized.NotifyCollectionChangedAction.Add,
                        item,
                        index));
        }

        private void removeAt(int index)
        {
            T obj = this.listImp[index];
            this.listImp.RemoveAt(index);
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, obj, index));
        }

        private void setValue(int index, T value)
        {
            object obj = this.listImp[index];
            this.listImp[index] = value;
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, new object[]{value}, new object[]{obj},  index));
        }
        #endregion

        #region IList<T> Members


        public int IndexOf(T item)
        {
            return this.indexOf(item);
        }

        public void Insert(int index, T item)
        {
            this.insert(index, item);
        }

        public void RemoveAt(int index)
        {
            this.removeAt(index);
        }

        public T this[int index]
        {
            get
            {
                return this.listImp[index];
            }
            set
            {
                this.setValue(index, value);
            }
        }

        #endregion

        #region IEnumerable Members

        public new System.Collections.IEnumerator GetEnumerator()
        {
            IEnumerator<T> enumer = this.listImp.GetEnumerator();
            Type realType = typeof(T).GetGenericTypeDefinition();
            bool unwrap = false;
            PropertyInfo info = null;
            if (realType == typeof(UniversalItemWrapper<>))
            {
                unwrap = true;
                info = typeof(T).GetProperty("Value");
            }

            while (enumer.MoveNext())
            {
                if (unwrap)
                {
                    yield return info.GetValue(enumer.Current, null);
                }
                else yield return enumer.Current;
            }
        }

        #endregion

        #region IList Members

        public int Add(object value)
        {
            if (value.GetType() != typeof(T))
            {
                value = value.UnboxConvert<T>();
            }
            base.Add((T)value);
            return this.Count;
        }

        public bool Contains(object value)
        {
            if (value.GetType() != typeof(T))
            {
                value = value.UnboxConvert<T>();
            }
            return base.Contains((T)value);
        }

        public int IndexOf(object value)
        {
            if (value.GetType() != typeof(T))
            {
                value = value.UnboxConvert<T>();
            }
            return this.indexOf((T)value);
        }

        public void Insert(int index, object value)
        {
            if (value.GetType() != typeof(T))
            {
                value = value.UnboxConvert<T>();
            }
            this.insert(index, (T)value);
        }

        public bool IsFixedSize
        {
            get { return false; }
        }

        public void Remove(object value)
        {
            if (value.GetType() != typeof(T))
            {
                value = value.UnboxConvert<T>();
            }
            base.Remove((T)value);
        }

        object System.Collections.IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                if (value.GetType() != typeof(T))
                {
                    value = value.UnboxConvert<T>();
                }
                this.setValue(index, (T)value);
            }
        }

        #endregion

        #region ICollection Members

        public void CopyTo(Array array, int index)
        {
            this.CopyTo(array as T[], index);
        }

        public bool IsSynchronized
        {
            get { return false; }
        }

        public object SyncRoot
        {
            get { return false; }
        }

        #endregion
    }
}
