﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace MesosoftCommon.IO
{
    public class GenericDisplayItem : INotifyPropertyChanged
    {
         public string Description { get; set; }

         private ObservableCollection<GenericDisplayItem> children = new ObservableCollection<GenericDisplayItem>();
         public ObservableCollection<GenericDisplayItem> Children { get { return this.children; } }


         private GenericDisplayItem parent;
         public GenericDisplayItem Parent 
         {
             get
             {
                 return this.parent;
             }
             set
             {
                 this.parent = value;
                 if (this.parent != null)
                 {
                     this.isChecked = this.parent.isChecked;
                 }
             }
         }

         public string Path
         {
             get
             {

                 if (this.Parent == null) return "\\" + this.Description;
                 return this.Parent.Path + "\\" + this.Description;
             }
         }

            public object Value { get; set; }

            private bool isChecked = false;
            public bool IsChecked 
            {
                get
                {
                    return this.isChecked;
                }
                set
                {
                    this.isChecked = value;
                    foreach (GenericDisplayItem item in this.children)
                    {
                        item.IsChecked = this.isChecked;
                    }

                    this.OnPropertyChanged("IsChecked");
                }
            }

            #region INotifyPropertyChanged Members

            public event PropertyChangedEventHandler PropertyChanged;

            protected void OnPropertyChanged(string displayName)
            {
                PropertyChangedEventHandler handler = this.PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(displayName));
                }
            }

            #endregion
    }
}
