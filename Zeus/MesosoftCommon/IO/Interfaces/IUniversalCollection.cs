﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Collections;
using System.Transactions;
using MesosoftCommon.Interfaces;

namespace MesosoftCommon.IO
{
    public interface IUniversalCollection : INotifyCollectionChanged, IQueryable, IEnlistmentNotification, ITracksChanges, IQueryProvider
    {
        bool UpdateSourceOnExit { get; set; }
        bool IsVolatile { get; }
        bool InTransaction { get; set; }
        bool CanPersist { get; }
        bool ShouldPersist { get; set; }
        UniversalCollectionSettings Settings { get; }

        void UpdateSource();
        void UpdateSource(UniversalCollectionSettings settings);
        Guid StartSimpleTransaction();
        Guid TryStartSimpleTransaction(out bool successful);
        void Rollback(Guid transaction);
        void EndSimpleTransaction(Guid transaction);
    }
}
