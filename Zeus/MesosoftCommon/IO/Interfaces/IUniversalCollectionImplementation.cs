﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.IO
{
    public interface IUniversalCollectionImplementation
    {
        void Commit();
        void Rollback();
        bool Prepare();
        void InDoubt();

        void StartTransaction();
        void EndTransaction();

        void UpdateSource();
        void UpdateSource(UniversalCollectionSettings settings);

        void LoadFromSource();
        void LoadFromSource(UniversalCollectionSettings settings);

        bool CanPersist { get; }
        bool ShouldPersist { get; set; }

        event ReportExceptionHandler Error;

        UniversalCollectionSettings Settings { get; set; }
    }
}
