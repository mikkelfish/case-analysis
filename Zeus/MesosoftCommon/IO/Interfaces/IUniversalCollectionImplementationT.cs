﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.ComponentModel;

namespace MesosoftCommon.IO
{
    public delegate void ReportExceptionHandler(object sender, Exception e);

    public interface IUniversalCollectionImplementation<T> : ICollection<T>, IUniversalCollectionImplementation
        where T : class, INotifyPropertyChanged, INotifyPropertyChanging
    {
      
    }
}
