﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MesosoftCommon.IO.Interfaces
{
    public interface IUniversalListCollection<T> : IUniversalListCollection, IList<T>
        where T : class, INotifyPropertyChanged, System.ComponentModel.INotifyPropertyChanging
    {
    }
}
