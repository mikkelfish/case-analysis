﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MesosoftCommon.IO
{
    public interface IUniversalListImplementation<T> : IUniversalCollectionImplementation<T>, IList<T>
        where T : class, INotifyPropertyChanged, System.ComponentModel.INotifyPropertyChanging
    {
    }
}
