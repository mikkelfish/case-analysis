﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hardcodet.Wpf.GenericTreeView;

namespace MesosoftCommon.IO
{
    public class GenericDisplayHierarchyTree : TreeViewBase<GenericDisplayItem>
    {
        public override ICollection<GenericDisplayItem> GetChildItems(GenericDisplayItem parent)
        {
            return parent.Children;
        }

        public override string GetItemKey(GenericDisplayItem item)
        {
            return item.Path;
        }

        public override GenericDisplayItem GetParentItem(GenericDisplayItem item)
        {
            return item.Parent;
        }
    }
}
