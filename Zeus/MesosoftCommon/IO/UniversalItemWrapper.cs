﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;

namespace MesosoftCommon.IO
{
    public class UniversalItemWrapper<T> : INotifyPropertyChanged, INotifyPropertyChanging
    {
        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private T val;
        public T Value
        {
            get { return val; }
            set 
            {
                this.OnPropertyChanging("Value");
                val = value;
                this.OnPropertyChanged("Value");
            }
        }

        //public static explicit operator T(UniversalItemWrapper<T> item)  // explicit byte to digit conversion operator
        //{
        //    if (item == null) return default(T);
        //    return item.Value;
        //}

        //public static explicit operator UniversalItemWrapper<T>(T item)  // explicit byte to digit conversion operator
        //{
        //    if (item == null) return new UniversalItemWrapper<T>();
        //    return new UniversalItemWrapper<T>() { Value = item };
        //}

        public static implicit operator T(UniversalItemWrapper<T> item)  // explicit byte to digit conversion operator
        {
            if (item == null) return default(T);
            return item.Value;
        }

        public static implicit operator UniversalItemWrapper<T>(T item)  // explicit byte to digit conversion operator
        {
            if (item == null) return new UniversalItemWrapper<T>();
            return new UniversalItemWrapper<T>() { Value = item };
        }


        protected void OnPropertyChanging(string propertyName)
        {
            PropertyChangingEventHandler handler = this.PropertyChanging;
            if (handler != null)
            {
                Delegate[] ds = handler.GetInvocationList();

                handler(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                Delegate[] ds = handler.GetInvocationList();

                handler(this, new PropertyChangedEventArgs(propertyName));
            }

        }
	
    }
}
