﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.Utilities.XAML;
using MesosoftCommon.Interfaces;

namespace MesosoftCommon.IO
{
    public abstract class UniversalCollectionSettings : IConstructorProvider
    {
        private Uri uri;
        public Uri URI
        {
            get { return uri; }
            set { uri = value; }
        }

        private Type backingType;
        public Type BackingType
        {
            get { return backingType; }
        }

        private bool updateSourceOnExit;
        public bool UpdateSourceOnExit
        {
            get { return updateSourceOnExit; }
            set { updateSourceOnExit = value; }
        }
	

        public UniversalCollectionSettings(Type backingType)
        {
            this.backingType = backingType;
        }

        #region IXAMLConstructorProvider Members

        public virtual object[] ConstructionArgs
        {
            get { return new object[] { this.backingType }; }
        }

        #endregion
    }
}
