﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MesosoftCommon.Utilities.XAML;
using System.IO;
using MesosoftCommon.IO.UCImplementations;
using System.ComponentModel;
using MesosoftCommon.Utilities.Settings;

namespace MesosoftCommon.IO
{
    public static class UniversalCollectionManager
    {
        private static Dictionary<string, IUniversalCollection> initializedCollections
            = new Dictionary<string, IUniversalCollection>();

        private static string collectionManagerPath
        {
            get
            {
                return UniversalCollectionManager.ManagerPath + "CollectionManager.xaml";
            }
        }

        private static string ManagerPath = Paths.DefaultPath.LocalPath + "UniversalCollections\\";

        static UniversalCollectionManager()
        {

            Application.Current.Exit += new ExitEventHandler(Current_Exit);

            if (!Directory.Exists(ManagerPath))
                Directory.CreateDirectory(ManagerPath);

            if (File.Exists(UniversalCollectionManager.collectionManagerPath))
            {
                using (FileStream stream = new FileStream(UniversalCollectionManager.collectionManagerPath, FileMode.Open, FileAccess.Read))
                {
                    registered = (Dictionary<string, UniversalCollectionSettings> )XAMLInputOutput.Load(stream);
                }
            }
        }

        private static int temporaryCount = 0;



        static void Current_Exit(object sender, ExitEventArgs e)
        {
            foreach (string key in initializedCollections.Keys)
            {
                if (initializedCollections[key].UpdateSourceOnExit)
                {
                    initializedCollections[key].UpdateSource();
                }
            }

            using (Stream stream = new FileStream(UniversalCollectionManager.collectionManagerPath, FileMode.Create, FileAccess.ReadWrite))
            {
                XAMLInputOutput.Save(registered, stream);
            }
        }

        private static Dictionary<string, UniversalCollectionSettings> registered = new Dictionary<string, UniversalCollectionSettings>();

        public static void RegisterCollection(string name, IUniversalCollection collection)
        {
            if (collection.Settings.URI.AbsolutePath.Contains("Temporary"))
            {
                collection.Settings.URI = new Uri(UniversalCollectionManager.ManagerPath + name + ".xaml");
            }
            registered.Add(name, collection.Settings);
        }

        public static void DeregisterCollection(string name)
        {
            if (!registered.ContainsKey(name))
                return;
            registered.Remove(name);
        }

        public static void DeleteCollection(string name)
        {

        }

        private static IUniversalCollection createCollection<T>(string name)
        {
            if (!registered.ContainsKey(name)) return null;
            UniversalCollectionSettings setting = registered[name];
            IUniversalCollectionImplementation implementation = (IUniversalCollectionImplementation)Activator.CreateInstance(setting.BackingType);
            implementation.Settings = setting;
            implementation.LoadFromSource();

            //Type[] paramType = setting.BackingType.GetGenericArguments();
           //Type toCreate = typeof(UniversalCollection<>).MakeGenericType(paramType);
            return (IUniversalCollection)Activator.CreateInstance(typeof(T), implementation);
        }

        private static IUniversalCollection createCollection<TCollection>(string name, bool saveOnExit)
            where TCollection : IUniversalCollection
        {
            if (UniversalCollectionManager.initializedCollections.ContainsKey(name))
                return UniversalCollectionManager.initializedCollections[name];

            IUniversalCollection toRet = null;
            if (!registered.ContainsKey(name))
            {
                toRet = (IUniversalCollection)Activator.CreateInstance(typeof(TCollection));
                RegisterCollection(name, toRet);
            }
            else toRet = createCollection<TCollection>(name);

            UniversalCollectionManager.initializedCollections.Add(name, toRet);


            toRet.UpdateSourceOnExit = saveOnExit;
            return toRet;
        }

        public static TCollection CreateCollection<TCollection>(string name, bool saveOnExit)
            where TCollection : IUniversalCollection
        {
            return (TCollection)createCollection<TCollection>(name, saveOnExit);
        }

        public static TCollection CreateCollection<TCollection>(string name)
            where TCollection : IUniversalCollection
        {
            return (TCollection)createCollection<TCollection>(name, true);
        }

        public static T GetDefaultImplementation<T, U>()
            where U : class, INotifyPropertyChanged, INotifyPropertyChanging
            where T : IUniversalCollectionImplementation<U>
        {
            IUniversalCollectionImplementation<U> imp = null;

            if (typeof(T) == typeof(IUniversalCollectionImplementation<U>))
            {
                imp = new UniversalMemoryImplementation<U>() { Settings = new UniversalMemoryImplementationSettings<U>() { URI = new Uri(UniversalCollectionManager.ManagerPath + "Temporary " + temporaryCount + ".xaml") } };
            }
            else if (typeof(T) == typeof(IUniversalListImplementation<U>))
            {
                imp = new UniversalMemoryImplementation<U>() { Settings = new UniversalMemoryImplementationSettings<U>() { URI = new Uri(UniversalCollectionManager.ManagerPath + "Temporary " + temporaryCount + ".xaml") } }; 
            }
            
            temporaryCount++;
            return (T)imp;
        }

        public static bool ContainsCollection(string name)
        {
            return registered.ContainsKey(name);
        }
    }
}
