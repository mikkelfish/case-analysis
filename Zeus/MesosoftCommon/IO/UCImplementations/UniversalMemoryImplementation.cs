﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Data.Linq;
using System.Xml;
using System.Windows.Markup;
using System.IO;
using System.Collections;
using MesosoftCommon.Utilities.XAML;

namespace MesosoftCommon.IO.UCImplementations
{
    public class UniversalMemoryImplementationSettings<T> : UniversalCollectionSettings
        where T : class, INotifyPropertyChanged, System.ComponentModel.INotifyPropertyChanging
    {
        public UniversalMemoryImplementationSettings()
            : base(typeof(UniversalMemoryImplementation<T>))
        {

        }

        public override object[] ConstructionArgs
        {
            get
            {
                return null;
            }
        }
    }

    public class UniversalMemoryImplementation<T> : IUniversalListImplementation<T>
        where T : class, INotifyPropertyChanged, System.ComponentModel.INotifyPropertyChanging
    {
        private class changedEntry
        {
            private T obj;
            public T Object
            {
                get { return obj; }
                set { obj = value; }
            }

            private object origValue;

            public object OrigValue
            {
                get { return origValue; }
                set { origValue = value; }
            }

            private PropertyInfo info;
            public PropertyInfo Info
            {
                get { return info; }
                set { info = value; }
            }

            public override bool Equals(object obj)
            {
                return this.obj == obj;
            }

            public override int GetHashCode()
            {
                return this.obj.GetHashCode();
            }
        }

        public UniversalMemoryImplementation()
        {
            this.currentList = this.realList;
        }

        private UniversalMemoryImplementationSettings<T> settings;
        public UniversalCollectionSettings Settings
        {
            get { return this.settings; }
            set { this.settings = (UniversalMemoryImplementationSettings<T>)value; }
        }
	

        private bool backWithXML = true;
        public bool ShouldPersist
        {
            get { return backWithXML; }
            set { backWithXML = value; }
        }

        public bool CanPersist
        {
            get { return true; }
        }	

        private List<changedEntry> changed = new List<changedEntry>();

        private ObservableCollection<T> temporaryList = new ObservableCollection<T>();
        private ObservableCollection<T> realList = new ObservableCollection<T>();

        private ObservableCollection<T> currentList;
        
        private bool isTransactionOccuring = false;

        void item_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (!this.isTransactionOccuring) return;

            int index = this.changed.FindIndex(
                delegate(changedEntry ent)
                {
                    return ent == sender;
                }
              );
            changedEntry entry = null;
            if (index == -1)
            {
                PropertyInfo info = sender.GetType().GetProperty(e.PropertyName);
                entry = new UniversalMemoryImplementation<T>.changedEntry() { Object = sender as T, Info = info, OrigValue = info.GetValue(sender, null) };
                this.changed.Add(entry);
            }
        }


        public IEnumerator<T> GetEnumerator()
        {
            return this.currentList.GetEnumerator();
        }

        public void StartTransaction()
        {
            this.isTransactionOccuring = true;

         //   this.temporaryList.
            this.temporaryList = new ObservableCollection<T>(this.realList.ToList<T>());
            this.currentList = this.temporaryList;
        }

        public void EndTransaction()
        {
            this.isTransactionOccuring = false;

        }

        public void UpdateSource()
        {
            if (this.ShouldPersist)
            {
               // string directory = this.settings.URI.AbsolutePath.Substring(0, this.settings.URI.AbsolutePath.LastIndexOf(

                using (Stream stream = new FileStream(this.settings.URI.LocalPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(this.realList, stream);
                }
            }
        }

        public void UpdateSource(UniversalCollectionSettings settings)
        {
            if (this.ShouldPersist)
            {
                using (Stream stream = new FileStream(this.settings.URI.LocalPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    MesosoftCommon.Utilities.XAML.XAMLInputOutput.Save(this.realList, stream);
                }
            }
        }

        public void LoadFromSource()
        {
            this.LoadFromSource(this.settings);
        }

        public void LoadFromSource(UniversalCollectionSettings settings)
        {
            this.temporaryList.Clear();

            if (File.Exists(settings.URI.LocalPath))
            {
                using (Stream stream = new FileStream(this.settings.URI.LocalPath, FileMode.Open, FileAccess.Read))
                {
                    this.realList = (ObservableCollection<T>)XAMLInputOutput.Load(stream);
                }
            }
            this.currentList = this.realList;
        }

        private void done()
        {
            this.currentList = this.realList;
            this.changed.Clear();
            this.temporaryList.Clear();
        }


        #region IUniversalCollectionImplementation<T> Members

        public void Commit()
        {
            try
            {
                this.realList.Clear();
                foreach (T item in this.temporaryList)
                {
                    this.realList.Add(item);
                }
                this.UpdateSource();
            }
            catch (Exception ex)
            {
                int s = 0;
            }
            finally
            {
                this.done();
            }
        }

        public void Rollback()
        {

            for (int i = 0; i < this.changed.Count; i++)
            {
                changedEntry entry = this.changed[i];
                entry.Info.SetValue(entry.Object, entry.OrigValue, null);
            }

            this.done();
        }

        public bool Prepare()
        {
            return true;
        }



        public void InDoubt()
        {
            
        }

        public event ReportExceptionHandler Error;

        #endregion

        #region IList<T> Members

        public int IndexOf(T item)
        {
            return this.currentList.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            item.PropertyChanging += new PropertyChangingEventHandler(item_PropertyChanging);

            this.currentList.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            this.currentList[index].PropertyChanging -= new PropertyChangingEventHandler(item_PropertyChanging);
            this.currentList.RemoveAt(index);
        }

        public T this[int index]
        {
            get
            {
                return this.currentList[index];
            }
            set
            {
                this.currentList[index] = value;
            }
        }

        #endregion

        #region ICollection<T> Members

        public void Add(T item)
        {
            item.PropertyChanging += new PropertyChangingEventHandler(item_PropertyChanging);
            this.currentList.Add(item);
        }

        public void Clear()
        {
            foreach (T item in this.currentList)
            {
                item.PropertyChanging -= new PropertyChangingEventHandler(item_PropertyChanging);
            }

            this.currentList.Clear();
        }

        public bool Contains(T item)
        {
            return this.currentList.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            this.currentList.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.currentList.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            item.PropertyChanging -= new PropertyChangingEventHandler(item_PropertyChanging); 
            return this.currentList.Remove(item);
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.currentList.GetEnumerator();
        }

        #endregion
    }
}
