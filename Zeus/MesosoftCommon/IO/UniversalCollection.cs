﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Transactions;
using System.Reflection;
using System.Collections.ObjectModel;
using MesosoftCommon.Interfaces;

namespace MesosoftCommon.IO
{
    public class UniversalCollection<T> : IUniversalCollection, ICollection<T>, IQueryable<T>
        where T:class,INotifyPropertyChanged, INotifyPropertyChanging
    {
        public bool UpdateSourceOnExit
        {
            get { return this.Settings.UpdateSourceOnExit; }
            set { this.Settings.UpdateSourceOnExit = value; }
        }

        protected bool isVolatile;
        public bool IsVolatile
        {
            get { return isVolatile; }
        }

        private bool inTransaction;
        public bool InTransaction
        {
            get { return inTransaction; }
            set { inTransaction = value; }
        }

        public bool CanPersist
        {
            get { return this.implementation.CanPersist; }
        }

        public bool ShouldPersist
        {
            get { return this.implementation.ShouldPersist; }
            set { this.implementation.ShouldPersist = value; }
        }

        public UniversalCollectionSettings Settings
        {
            get { return this.implementation.Settings; }
        }
	

        private IUniversalCollectionImplementation<T> implementation;
        protected virtual IUniversalCollectionImplementation<T> Implementation
        {
            get
            {
                return this.implementation;
            }
            set
            {
                this.implementation = value;
            }
        }

        protected Dictionary<Guid, CommittableTransaction> transactions = new Dictionary<Guid, CommittableTransaction>();


        #region Constructors
        public UniversalCollection()
        {
            this.implementation = UniversalCollectionManager.GetDefaultImplementation<IUniversalCollectionImplementation<T>, T>();
        }

        public UniversalCollection(IUniversalCollectionImplementation<T> implementation)
        {
            this.Implementation = implementation;
        }
        #endregion

        #region Undo Tracking

        public event PreviewTrackingEventHandler PreviewTrackingEvent;
        public event TrackingEventHandler TrackingEvent;

        private class trackingObject<U>
        {
            private U item;
            public U Item
            {
                get { return item; }
                set { item = value; }
            }

            private PropertyInfo propertyInfo;
            public PropertyInfo PropertyInfo
            {
                get { return propertyInfo; }
                set { propertyInfo = value; }
            }

            private object val;
            public object Value
            {
                get { return val; }
                set { val = value; }
            }

            private bool added;
            public bool Added
            {
                get { return added; }
                set { added = value; }
            }

            private bool deleted;
            public bool Deleted
            {
                get { return deleted; }
                set { deleted = value; }
            }	
        }


        private bool isTracking;
        public bool IsTracking
        {
            get { return isTracking; }
        }


        private List<trackingObject<T>> undoStack = new List<trackingObject<T>>();
        private List<trackingObject<T>> redoStack = new List<trackingObject<T>>();

        public bool StartTracking()
        {
            if (this.inTransaction) return false;
            this.isTracking = true;
            return true;
        }

        private bool preview(trackingObject<T> item, TrackingType trackType)
        {
            PreviewTrackingEventHandler handler = this.PreviewTrackingEvent;
            PreviewTrackingEventArgs e = new PreviewTrackingEventArgs(item.Item, item.Value, item.PropertyInfo, trackType);
            if (handler != null)
            {
                handler(this, e);
            }

            return e.Cancel;
        }

        private void track(trackingObject<T> item, TrackingType trackType)
        {
            TrackingEventHandler handler = this.TrackingEvent;
            if (handler != null)
            {
                handler(this, new TrackingEventArgs(item.Item, item.Value, item.PropertyInfo, trackType));
            }
        }

        public bool Undo()
        {
            if (!this.isTracking) return false;
            if (this.undoStack.Count == 0) return false;

            int backAmount = 2;
            //if (this.redoStack.Count == 0) backAmount++;

            if (this.undoStack.Count - backAmount < 0) return false;

            trackingObject<T> item = this.undoStack[this.undoStack.Count-backAmount];

            if (preview(item, TrackingType.Undo)) return false;

            trackingObject<T> moveItem = this.undoStack[this.undoStack.Count - 1];

            this.undoStack.RemoveAt(this.undoStack.Count - 1);
            this.redoStack.Add(moveItem);
           
            
            item.PropertyInfo.SetValue(item.Item, item.Value, null);

            this.track(item, TrackingType.Undo);

            return true;
        }

        public bool Redo()
        {
            if (!this.isTracking) return false;
            if (this.redoStack.Count == 0) return false;
            trackingObject<T> item = this.redoStack[this.redoStack.Count - 1];

            if (preview(item, TrackingType.Redo)) return false;

            this.redoStack.RemoveAt(this.redoStack.Count - 1);
            this.undoStack.Add(item);
            item.PropertyInfo.SetValue(item.Item, item.Value, null);

            this.track(item, TrackingType.Redo);

            return true;
        }

        public void StopTracking()
        {
            this.isTracking = false;
            this.undoStack.Clear();
            this.redoStack.Clear();
        }

        void tracking_PropertyChanging(object sender, PropertyChangedEventArgs e)
        {
            if (!this.isTracking) return;
            PropertyInfo info = sender.GetType().GetProperty(e.PropertyName);
            if (info != null)
            {
                T item = sender as T;

                trackingObject<T> obj = new UniversalCollection<T>.trackingObject<T>(){
                                            Item=item,PropertyInfo=info,Value=info.GetValue(item,null)};

                if ((this.undoStack.Count > 0 && obj.Value == this.undoStack[this.undoStack.Count - 1].Value) ||
                    (this.redoStack.Count > 0 && obj.Value == this.redoStack[this.redoStack.Count - 1].Value))
                    return;

                this.redoStack.Clear();
                this.undoStack.Add(obj);
            }
        }



        #endregion

        public virtual void UpdateSource()
        {
            if (this.isTracking || this.inTransaction) return;

            this.implementation.UpdateSource();
        }

        public virtual void UpdateSource(UniversalCollectionSettings settings)
        {
            if (this.isTracking || this.inTransaction) return;
            this.implementation.UpdateSource(settings);
        }


        public virtual Guid StartSimpleTransaction()
        {
            if (this.isTracking)
            {
                throw new Exception("Cannot begin transaction while in Tracking Mode.");
            }


            bool success;
            Guid guid = this.TryStartSimpleTransaction(out success);

            return guid;            
        }

        public virtual Guid TryStartSimpleTransaction(out bool successful)
        {
            if (this.isTracking)
            {
                successful = false;
                return Guid.Empty;
            }

            Guid toRet = Guid.NewGuid();
            this.transactions.Add(toRet, new CommittableTransaction());
            if (this.isVolatile) this.transactions[toRet].EnlistVolatile(this, EnlistmentOptions.None);
            else this.transactions[toRet].EnlistDurable(toRet, this, EnlistmentOptions.None);
            Transaction.Current = this.transactions[toRet];
            this.implementation.StartTransaction();
            this.inTransaction = true;

            successful = true;
            return toRet;
        }

        public virtual void Rollback(Guid transaction)
        {
            if (!this.inTransaction) return;
            this.implementation.EndTransaction();
            this.inTransaction = false;
            this.transactions[transaction].Rollback();
        }

        public virtual void EndSimpleTransaction(Guid transaction)
        {
            if (!this.inTransaction) return;
            this.inTransaction = false;
            this.implementation.EndTransaction();
            this.transactions[transaction].Commit();
            Transaction.Current = null;
        }

        #region ICollection<T> Members

        protected void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public void Add(T item)
        {
            item.PropertyChanged += new PropertyChangedEventHandler(transaction_PropertyChanged);
            item.PropertyChanged += new PropertyChangedEventHandler(tracking_PropertyChanging);
            this.implementation.Add(item);

            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        void transaction_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        public void Clear()
        {            
            this.implementation.Clear();
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public bool Contains(T item)
        {
            return this.implementation.Contains(item);            
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            this.implementation.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.implementation.Count; }
        }

        public bool IsReadOnly
        {
            get { return this.implementation.IsReadOnly; }
        }

        public bool Remove(T item)
        {
            bool removed = this.implementation.Remove(item);
            if (removed)
            {
                this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
            }
            return removed;
        }

        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return this.implementation.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

        #region INotifyCollectionChanged Members

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion

        #region IQueryable<T> Members

        public IQueryable<TElement> CreateQuery<TElement>(System.Linq.Expressions.Expression expression)
        {
            IQueryable<TElement> ele = this.implementation.AsQueryable<T>().Provider.CreateQuery<TElement>(expression);
            return ele;
        }

        public TResult Execute<TResult>(System.Linq.Expressions.Expression expression)
        {
            return this.implementation.AsQueryable<T>().Provider.Execute<TResult>(expression);
          //  return this.implementation.Execute<TResult>(expression);
        }

        #endregion

        #region IQueryable Members

        public IQueryable CreateQuery(System.Linq.Expressions.Expression expression)
        {
            if (this.implementation is IQueryable)
            {
                return this.CreateQuery<T>(expression);
            }

            return this.implementation.AsQueryable();

        }

        public Type ElementType
        {
            get { return typeof(T); }
        }

        public object Execute(System.Linq.Expressions.Expression expression)
        {
            return this.Execute<T>(expression);
        }

        public System.Linq.Expressions.Expression Expression
        {
            get { return System.Linq.Expressions.Expression.Constant(this.implementation.AsQueryable<T>()); }
        }

        #endregion

        #region IEnlistmentNotification Members

        public virtual void Commit(Enlistment enlistment)
        {
            this.implementation.Commit();
            enlistment.Done();
           // this.transactions.Remove(Transaction.Current.TransactionInformation.DistributedIdentifier);
           // Transaction.Current = null;
        }

        public virtual void InDoubt(Enlistment enlistment)
        {
            this.implementation.InDoubt();
        }

        public virtual void Prepare(PreparingEnlistment preparingEnlistment)
        {
            bool ok = this.implementation.Prepare();
            if (!ok)
            {
                preparingEnlistment.ForceRollback();
            }
            else preparingEnlistment.Prepared();
        }

        public virtual void Rollback(Enlistment enlistment)
        {
            this.implementation.Rollback();
            enlistment.Done();
         //   this.transactions.Remove(Transaction.Current.TransactionInformation.DistributedIdentifier);
          //  Transaction.Current = null;
        }

        #endregion


        #region IQueryable Members


        public IQueryProvider Provider
        {
            get { return this; }
        }

        #endregion
    }
}
