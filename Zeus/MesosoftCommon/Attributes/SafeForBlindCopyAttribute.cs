﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MesosoftCommon.Attributes
{
    [global::System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class SafeForBlindCopyAttribute : Attribute
    {
       
    }
}
