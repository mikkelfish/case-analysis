using System;
using System.Collections.Generic;
using System.Text;
using MesosoftCommon.Development;

namespace MesosoftCommon.Attributes
{
    /// <summary>
    /// Attribute used to link a property and its associated field. This allows implementation to work on the Property level, but then have the ability to set the underlying field if the Property can't write.
    /// </summary>
    [Created("Mikkel", "12/05/06", DocumentedStatus=DocumentedStage.None)]
    [global::System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class FieldLinkedAttribute : Attribute
    {
        readonly string fieldName;
        public string FieldName
        {
            get
            {
                return this.fieldName;
            }
        }

        public FieldLinkedAttribute(string fieldName)
        {
            this.fieldName = fieldName;
        }

    }

}
