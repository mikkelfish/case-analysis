﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MesosoftCommon.DataStructures;
using System.Windows.Threading;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using MesosoftCommon.ExtensionMethods;

namespace MesosoftCommon.Threading
{
    public static class ThreadSafeBinding
    {
        private interface iBoundItem
        {
            int Count { get; set; }
            IList List { get; }
            Type TypeBound { get; }
            string Name { get; set; }
        }

        private class boundItem<T> : iBoundItem
        {
            public int Count { get; set; }

            private BeginInvokeObservableCollection<T> list;
            public BeginInvokeObservableCollection<T> List { get { return this.list; } }

            public boundItem(Dispatcher dispatch, string name)
            {
                this.list = new BeginInvokeObservableCollection<T>(dispatch);
                this.Name = name;
            }

            public Type TypeBound
            {
                get
                {
                    return typeof(T);
                }
            }

            public string Name { get; set; }

            #region iBoundItem Members

            IList iBoundItem.List
            {
                get { return this.List; }
            }

            #endregion
        }

        private static Dictionary<string, IList> collections = new Dictionary<string, IList>();
        private static Dictionary<Dispatcher, List<iBoundItem>> boundLists = new Dictionary<Dispatcher, List<iBoundItem>>();
        private static object lockable = new object();

        public static void RegisterCollection<T>(ObservableCollection<T> collection, string name)
        {
            if(!collections.ContainsKey(name + "$" + typeof(T)))
            {
                collection.CollectionChanged += new NotifyCollectionChangedEventHandler(collection_CollectionChanged);
                collections.Add(name + "$" + typeof(T), collection);
            }
        }

        static void collection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            string name = null;
            foreach (string key in collections.Keys)
            {
                if (collections[key] == sender)
                {
                    name = key.Split('$')[0];
                    break;
                }
            }

            if (name == null) return;

            Type type = sender.GetType().GetGenericArguments()[0];

            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (Dispatcher d in boundLists.Keys)
                {
                    iBoundItem item = boundLists[d].FirstOrDefault(it => it.Name == name && it.TypeBound == type);

                    //MesosoftCommon.ExtensionMethods.CollectionFunctions.AddRange(item.List, 

                    foreach (object obj in e.NewItems)
                    {
                        item.List.Add(obj);
                    }
                }
            }

            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (Dispatcher d in boundLists.Keys)
                {
                    iBoundItem item = boundLists[d].FirstOrDefault(it => it.Name == name && it.TypeBound == type);

                    foreach (object obj in e.OldItems)
                    {
                        item.List.Remove(obj);
                    }
                }
            }

            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Reset)
            {
                List<object> newItems = new List<object>();
                lock (lockable)
                {
                    foreach (object o in (sender as IList))
                    {
                        newItems.Add(o);
                    }
                }

                foreach (Dispatcher d in boundLists.Keys)
                {
                    iBoundItem item = boundLists[d].FirstOrDefault(it => it.Name == name && it.TypeBound == type);
                    item.List.Clear();

                    foreach (object o in newItems)
                    {
                        item.List.Add(o);
                    }
                }
            }
        }

        public static BeginInvokeObservableCollection<T> BindToManager<T>(Dispatcher dispatch, string name)
        {
            if (!collections.ContainsKey(name + "$" + typeof(T)))
            {
                throw new InvalidOperationException("Name/type hasn't been registered");
            }

            string key = name + "$" + typeof(T);

            List<T> toAdd = new List<T>();
            bool needsAdd = false;
            iBoundItem item = null;
            lock (lockable)
            {

                if (!boundLists.ContainsKey(dispatch))
                {
                    boundLists.Add(dispatch, new List<iBoundItem>());
                }

                item = boundLists[dispatch].FirstOrDefault(b => b.Name == name && b.TypeBound == typeof(T));
                if (item == null)
                {
                    item = new boundItem<T>(dispatch, name);
                    boundLists[dispatch].Add(item);
                    foreach (T obj in collections[key])
                    {
                        toAdd.Add(obj);
                    }

                    needsAdd = true;
                }

              

            }

            //Add stuff outside a lock cause who knows how long it could take to reflect updates
            if (needsAdd)
            {
                foreach (T obj in toAdd)
                {
                    item.List.Add(obj);
                }
            }

        

            return item.List as BeginInvokeObservableCollection<T>;
        }

        public static void RemoveBindingToManager<T>(Dispatcher dispatch, string name)
        {
            lock (lockable)
            {
                iBoundItem item = boundLists[dispatch].FirstOrDefault(b => b.Name == name && b.TypeBound == typeof(T));

                if (item == null) return;

                item.Count--;

                if (boundLists[dispatch].All(it => it.Count == 0))
                {
                    boundLists.Remove(dispatch);
                }
            }
        }
    }
}
