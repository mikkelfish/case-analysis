﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MesosoftStyleLibrary
{
    public class TestDictionary
    {
        public static object BlackishKey
        {
            get
            {
                return new ComponentResourceKey(typeof(TestDictionary), "Blackish");
            }
        }

        public static object ToolValidKey
        {
            get
            {
                return new ComponentResourceKey(typeof(TestDictionary), "ToolValid");
            }
        }

        public static object DragManagerPanelBeingDraggedInDefaultKey
        {
            get
            {
                return new ComponentResourceKey(typeof(TestDictionary), "DragManagerPanelBeingDraggedInDefault");
            }
        }

        public static object DragManagerElementBeingDraggedDefaultKey
        {
            get
            {
                return new ComponentResourceKey(typeof(TestDictionary), "DragManagerElementBeingDraggedDefault");
            }
        }

        public static object DragManagerElementBeingDraggedOverDefaultKey
        {
            get
            {
                return new ComponentResourceKey(typeof(TestDictionary), "DragManagerElementBeingDraggedOverDefault");
            }
        }

        public static object DragManagerDragElementDefaultKey
        {
            get
            {
                return new ComponentResourceKey(typeof(TestDictionary), "DragManagerDragElementDefault");
            }
        }

        public static object SideButtonDefaultKey
        {
            get
            {
                return new ComponentResourceKey(typeof(TestDictionary), "SideButtonDefault");
            }
        }

        public static object DockedContentTopBarDefaultKey
        {
            get
            {
                return new ComponentResourceKey(typeof(TestDictionary), "DockedContentTopBarDefault");
            }
        }

        public static object DockedContentTopButtonDefaultKey
        {
            get
            {
                return new ComponentResourceKey(typeof(TestDictionary), "DockedContentTopButtonDefault");
            }
        }
    }
}
