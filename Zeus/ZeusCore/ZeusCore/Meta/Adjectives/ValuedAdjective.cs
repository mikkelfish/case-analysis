﻿using System;
using System.Collections.Generic;
using System.Text;
using MesosoftCommon.Attributes;
using ZeusCore.Instance;

namespace ZeusCore.Meta
{
    public class ValuedAdjective : Adjective
    {
        private List<DomainInstance> domains = new List<DomainInstance>();
        [FieldLinked("domains")]
        public override List<ZeusCore.Instance.DomainInstance> Domains
        {
            get
            {
                return this.domains;
            }
        }
    }
}
