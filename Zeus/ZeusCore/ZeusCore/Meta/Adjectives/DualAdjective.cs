﻿using System;
using System.Collections.Generic;
using System.Text;
using MesosoftCommon.Attributes;
using ZeusCore.Instance;
using ZeusCore.Components;
using MesosoftCommon.Development;

namespace ZeusCore.Meta
{
    [Todo("Mikkel", "6/21/07", Comments="Make ThreadSafe", Priority=DevelopmentPriority.Critical)]
    public class DualAdjective : Adjective
    {
        private static DomainInstance dualInstance;

        private List<DomainInstance> domains = new List<DomainInstance>();
        public override List<ZeusCore.Instance.DomainInstance> Domains
        {
            get 
            {
                if (this.domains.Count == 0)
                {
                    if (DualAdjective.dualInstance == null)
                    {
                        dualInstance = ZeusContext.CurrentlyLoadingContext.GetEngine<Domain>().CreateInstance("Dual", "System") as DomainInstance;
                    }
                    this.domains.Add(dualInstance);
                }
                return this.domains; 
            }
        }
    }
}
