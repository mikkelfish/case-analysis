using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using ZeusCore.Instance;
using MesosoftCommon.Attributes;
using System.Windows.Markup;

namespace ZeusCore.Meta
{

    public abstract class Adjective : Atom
    {
        [Required]
        public abstract List<DomainInstance> Domains
        {
            get;
        }
	
    }
}
