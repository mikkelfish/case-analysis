using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace ZeusCore.Meta
{
    public class Category : Atom
    {
        private static Category system;
        public static Category System
        {
            get { return system; }
        }

        static Category()
        {
            Category.system = new Category();
            Category.system.Name = "System";
            Category.system.Description = "System";
            Category.system.Owner = Entity.System;
            Category.system.Keywords = new string[] { "None" };
        }

        public override string FullName
        {
            get 
            {
                if (this.Parent == null) return this.Name;
                return this.Parent.FullName + "." + this.Name; 
            }
        }

        private Category parentCategory;
        /// <summary>
        /// The parent category.
        /// </summary>
        [TypeConverter(typeof(Converters.MetaEngineConverter<Category>))]        
        public Category Parent
        {
            get { return parentCategory; }
            set 
            { 
                this.parentCategory = value;
                this.parentCategory.addChildCategory(this);
            }
        }

        private List<Category> children = new List<Category>();
        public Category[] Children
        {
            get
            {
                return children.ToArray();
            }
        }

        private void addChildCategory(Category child)
        {
            children.Add(child);
        }
	
    }
}
