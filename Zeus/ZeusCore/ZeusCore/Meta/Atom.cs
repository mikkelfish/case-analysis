using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using ZeusCore.Utilities;
using System.Reflection;
using MesosoftCommon.Utilities.Converters;
using MesosoftCommon.Utilities.Validations;
using ZeusCore.Validations;

namespace ZeusCore.Meta
{
    [ZeusVocabulary]
    public abstract class Atom
    {
        //private Engines.Engine<Atom> engine;
        //public Engines.Engine<Atom> Engine
        //{
        //    get { return engine; }
        //    set { engine = value; }
        //}
	

        public const string MissingTargetString = "&&MISSING&&";

        public static bool VerifyAtom(Atom atom)
        {
            bool valid = true;
            PropertyInfo[] props = atom.GetType().GetProperties();
            foreach (PropertyInfo info in props)
            {
                object val = info.GetValue(atom, null);
                if (val == null)
                {
                    object[] objs = info.GetCustomAttributes(typeof(RequiredAttribute), true);
                    if (objs != null && objs.Length > 0) valid = false;
                }
                else if (val is Atom)
                {
                    if ((val as Atom).Name.Contains(Atom.MissingTargetString))
                        valid = false;
                }                
            }

            return valid;
        }

        public override string ToString()
        {
            return this.FullName;
        }

        public const int MaxSmallLength = 64;
        public const int MaxMediumLength = 128;
        public const int MaxLargeLength = 512;

        private string name;
        /// <summary>
        /// Gets or sets the name of the atom.
        /// </summary>
       // [ValidationInfo(typeof(ZeusUniqueValidation))]
        //[ValidationInfoProperty(typeof(ZeusUniqueValidation), "ExtraMessage", " Please change the name or category of the Atom.")]
        [ValidationInfo(typeof(StringLengthValidator), 1, Atom.MaxSmallLength)]
        [Required]
        [Browsable(true)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string[] keyWords;
        /// <summary>
        /// Gets or sets an array of keywords describing the atom.
        /// </summary>
        [ValidationInfo(typeof(StringLengthValidator), 1, Atom.MaxLargeLength)]        
        [TypeConverter(typeof(StringArrayConverter))]
        [DefaultValue("None")]
        [Browsable(true)]
        [Required]
        public string[] Keywords
        {
            get { return this.keyWords; }
            set { this.keyWords = value; }
        }

        private string description;
        /// <summary>
        /// Gets or sets the description of the atom.
        /// </summary>
        [ValidationInfo(typeof(StringLengthValidator), 1, Atom.MaxLargeLength)]        
        [DefaultValue("None")]
        [Required]
        [Browsable(true)]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// Gets a string that represents the full unique name of the atom.
        /// </summary>
        public virtual string FullName 
        {
            get 
            {
                if (this.category == null) 
                    return string.Empty;
                return this.category.FullName + "." + this.Name; 
            }
        }

        private Entity owner;
        
        [TypeConverter(typeof(Converters.MetaEngineConverter<Entity>))]        
        [Required]
        [Browsable(true)]
        [ValidationInfo(typeof(NotNullValidator))]
        [ValidationInfoProperty(typeof(NotNullValidator),"ExtraInfo","Entity not found in system. Please try again or add entity.")]
        public Entity Owner
        {
            get { return owner; }
            set { owner = value; }
        }


        private Category category;
        /// <summary>
        /// The category that the adjective belongs to.
        /// </summary>
        [DefaultValue(Engines.Engine<Category>.DefaultString)]
        [Browsable(true)]
        [TypeConverter(typeof(Converters.MetaEngineConverter<Category>))]
        [Required]
        public Category Category
        {
            get
            {
                if (this.GetType() == typeof(Category)) 
                    return this as Category;
                return category;
            }
            set
            {
                if (this.GetType() != typeof(Category))
                    category = value;
            }
        }
	
	
    }
}
