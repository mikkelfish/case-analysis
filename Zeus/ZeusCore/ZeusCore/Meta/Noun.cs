using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using ZeusCore.Instance;
using System.Collections.ObjectModel;

namespace ZeusCore.Meta
{
    public class Noun : Atom
    {
        private List<AdjectiveInstance> adjectives = new List<AdjectiveInstance>();
        public List<AdjectiveInstance> Adjectives
        {
            get { return adjectives; }
        }

    }
}
