using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Markup;
using ZeusCore.Engines;
using MesosoftCommon.Utilities.Converters;
using MesosoftCommon.Attributes;

namespace ZeusCore.Meta
{
    [ContentProperty("Description")] 
    public class Domain : Atom
    {
        private Type dataType;
        /// <summary>
        /// Gets or sets the underlying type of the domain.
        /// </summary>        
        [DefaultValue(typeof(Object))]
        [Required]
        [Browsable(true)]        
        public Type DataType
        {
            get { return this.dataType; }
            set { this.dataType = value; }
        }

        private List<ValidationRule> validations = new List<ValidationRule>();
        /// <summary>
        /// Gets optional validation rules that apply to all members of this domain.
        /// </summary>
        [Browsable(true)]
        public List<ValidationRule> Validations
        {
            get { return this.validations; }
        }

    }
}
