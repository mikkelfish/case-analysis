﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using ZeusCore.Engines;


namespace ZeusCore.Meta
{
    [CustomZeusEngineAttribute(typeof(EntityEngine))]
    public class Entity : Atom
    {
        private DateTime creationDate;
        public DateTime CreationDate
        {
            get { return creationDate; }
            set { creationDate = value; }
        }

        private DateTime lastLogin;
        public DateTime LastLogin
        {
            get { return lastLogin; }
            set { lastLogin = value;}
        }

        private string password;
        [Required]
        [DefaultValue("")]
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public override string FullName
        {
            get { return this.Name; }
        }

        private static Entity system;

        public static Entity System
        {
            get { return system; }
        }

        static Entity()
        {
            Entity.system = new Entity();
            Entity.system.Name = "System";
            Entity.system.Password = "";
            Entity.system.Owner = Entity.system;
            Entity.system.Description = "The system entity is used as the owner of all basic stuff integral to Zeus.";
            Entity.system.Category = Category.System;
            Entity.system.Keywords = new string[] { "None" };
        }
	
    }
}
