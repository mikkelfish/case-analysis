using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Markup;
using System.Reflection;
using System.IO;
using MesosoftCommon.Utilities.Binders;
using System.ComponentModel;
using ZeusCore.Meta;
using System.Globalization;
using System.Collections;
using MesosoftCommon.Attributes;
using ZeusCore.Components;

namespace ZeusCore.Utilities
{
    public static class XAML
    {
        private static Dictionary<Type, PropertyInfo[]> props = new Dictionary<Type, PropertyInfo[]>();

        private static bool fillItemWithDefaults(ITypeDescriptorContext context, object item)
        {
            if (!props.ContainsKey(item.GetType()))
            {
                XAML.props.Add(item.GetType(), item.GetType().GetProperties());
            }

            PropertyInfo destObject = context.GetType().GetProperty("DestinationObject");
            if(destObject != null)
            {
                destObject.SetValue(context, item, null);
            }

            bool error = false;
            foreach (PropertyInfo info in XAML.props[item.GetType()])
            {
                PropertyInfo destType = context.GetType().GetProperty("Destination");
                if (destType != null)
                {
                    destType.SetValue(context, info, null);
                }

                object obj = info.GetValue(item, null);
                if (obj is ICollection)
                {
                    if ((obj as ICollection).Count == 0)
                    {
                        obj = null;
                    }
                    else
                    {
                        foreach (object itemInList in (obj as ICollection))
                        {
                            object[] objs = itemInList.GetType().GetCustomAttributes(typeof(ZeusVocabularyAttribute), true);
                            if (objs != null && objs.Length > 0) error = XAML.fillItemWithDefaults(context, itemInList);
                        }
                    }
                }

                if (obj == null)
                {
                    object[] objs = info.GetCustomAttributes(typeof(DefaultValueAttribute), true);
                    if (objs != null && objs.Length > 0)
                    {
                        TypeConversionBinder binder = new TypeConversionBinder(info, context);
                        MesosoftCommon.Utilities.Reflection.Common.SetPropertyOrUnderlying(info, item, (objs[0] as DefaultValueAttribute).Value, binder, null, CultureInfo.CurrentCulture);
                        obj = MesosoftCommon.Utilities.Reflection.Common.GetPropertyOrUnderlying(info, item, null);
                        
                    }

                    object[] req = info.GetCustomAttributes(typeof(RequiredAttribute), true);
                    if (req != null && req.Length > 0)
                    {
                        if (obj == null)
                        {
                            error = true;
                        }
                    }
                }
            }

            return error;
        }

        public static void ReadXAML<T>(ITypeDescriptorContext context, Stream xamlStream, out T[] validItems, out T[] invalidItems)
        {          
            ParserContext parserContext = new ParserContext();
            object[] attrs = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(XmlnsDefinitionAttribute), true);
            List<NamespaceMapEntry> namespaces = new List<NamespaceMapEntry>();
            List<string> assemblies = new List<string>();
            foreach (XmlnsDefinitionAttribute attr in attrs)
            {
                namespaces.Add(new NamespaceMapEntry(attr.XmlNamespace, attr.AssemblyName, attr.ClrNamespace));
                assemblies.Add(attr.AssemblyName);
            }

            XamlTypeMapper mapper = new XamlTypeMapper(assemblies.ToArray(), namespaces.ToArray());
            parserContext.XamlTypeMapper = mapper;
            List<T> errorItems = new List<T>();
            List<T> goodItems = new List<T>();

            List<T> readItems = (List<T>)XamlReader.Load(xamlStream, parserContext);
            if (readItems != null)
            {
                foreach (T item in readItems)
                {
                    if (fillItemWithDefaults(context, item))
                    {
                        errorItems.Add(item);
                    }
                }

                foreach (T item in readItems)
                {
                    if (!errorItems.Contains(item))
                    {
                        goodItems.Add(item);
                    }
                }
            }

            validItems = goodItems.ToArray();
            invalidItems = errorItems.ToArray();
        }
    }
}
