using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using ZeusCore.Engines;
using ZeusCore.Meta;
using ZeusCore.Components;
using System.Reflection;

namespace ZeusCore.Converters
{
    public class EngineConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (context == null) return false;
            if (context.Instance == null) return false;
            if (!(context.Instance is Component)) return false;
            if (!((context.Instance as Component).Container is ZeusContext)) return false;

            if (sourceType == typeof(string)) return true;
            return false;
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (context == null) return null;
            if (context.Instance == null) return null;
            if (!(context.Instance is Component)) return null;
            if (!((context.Instance as Component).Container is ZeusContext)) return null;
            ZeusContext zeus = (context.Instance as Component).Container as ZeusContext;
            
            MethodInfo info = typeof(ZeusContext).GetMethod("GetEngine");
            PropertyInfo curType = context.GetType().GetProperty("Destination");
            PropertyInfo dest = (PropertyInfo)curType.GetValue(context, null);
            object engine = MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod(info, new Type[] { dest.PropertyType }, zeus, null);
            if (engine == null) return null;

            MethodInfo findByname = engine.GetType().GetMethod("FindByFullName");

            string val = value as string;
            if (val.Contains(Utilities.Constants.DefaultValuePropertyPrefix))
            {
                string linkage = val.Substring(Utilities.Constants.DefaultValuePropertyPrefix.Length);
                PropertyInfo link = dest.ReflectedType.GetProperty(linkage);
                PropertyInfo destObject = context.GetType().GetProperty("DestinationObject");
                object destObj = destObject.GetValue(context, null);
                val = link.GetValue(destObj, null).ToString();
            }
            
            return findByname.Invoke(engine, new object[] { val });
        }
    }
}
