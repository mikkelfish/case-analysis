using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using ZeusCore.Engines;
using ZeusCore.Meta;
using ZeusCore.Components;
using System.Reflection;

namespace ZeusCore.Converters
{
    public class MetaEngineConverter<T> : TypeConverter where T:Atom, new()
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string)) return true;
            return false;
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            ZeusContext zcontext = null;
            if (context != null && context.Instance != null && (context.Instance is Component) && ((context.Instance as Component).Container is ZeusContext))
                zcontext = (context.Instance as Component).Container as ZeusContext;
            else zcontext = ZeusContext.CurrentlyLoadingContext;

            if (zcontext == null) return null;
            Engine<T> engine = ZeusContext.CurrentlyLoadingContext.GetEngine<T>();
            T toRet = engine.FindByFullName((string)value);
            if (toRet == null)
            {
                toRet = (T)Activator.CreateInstance(typeof(T));
                toRet.Name = Atom.MissingTargetString + (string)value;
            }
            return toRet;
        }
    }
}
