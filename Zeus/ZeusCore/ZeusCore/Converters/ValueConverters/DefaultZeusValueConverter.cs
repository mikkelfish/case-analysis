﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Data;
using System.Reflection;
using ZeusCore.Meta;
using System.Globalization;
using System.ComponentModel;
using ZeusCore.Engines;

namespace ZeusCore.Converters.ValueConverters
{
    [ValueConversion(typeof(Atom), typeof(string))]
    public class DefaultZeusValueConverter : IValueConverter
    {
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MethodInfo method = this.GetType().GetMethod("getFromEngine",BindingFlags.Instance|BindingFlags.NonPublic);

            Atom ret = (Atom)MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod(method, new Type[] { targetType },
                this, new object[] { value, parameter as ZeusCore.Components.ZeusContext });
            if (ret == null)
                return null;                
            return ret;
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return String.Empty;
            Atom ret = value as Atom;
            if(ret == null)
                return null;
            return (value as Atom).FullName;
        }

        private ZeusCore.Meta.Atom getFromEngine<T>(string value, ZeusCore.Components.ZeusContext context) where T:Atom, new()
        {
            if (context == null) return null;
            Engine<T> engine = context.GetEngine<T>();
            T toRet = engine.FindByFullName(value);
            if (toRet == null)
            {
                return null;
            }
            return toRet;

            //ZeusCore.Converters.MetaEngineConverter<T> metaConv = new ZeusCore.Converters.MetaEngineConverter<T>();

            //if (!metaConv.CanConvertFrom(typeof(string)))
            //    return null;
            //return (Atom)metaConv.ConvertFrom(value);
        }
    }
}
