﻿using System;
using System.Collections.Generic;
using System.Text;
using ZeusCore.Meta;
using ZeusCore.Components;

namespace ZeusCore.Engines
{
    public class EntityEngine : Engine<Entity>
    {
        public EntityEngine()
            : base()
        {
         //   this.items.Add(Entity.System);
        }

        protected override void catchItemsBeforeInsertion(ref Entity[] validItems, ref Entity[] invalidItems)
        {
            base.catchItemsBeforeInsertion(ref validItems, ref invalidItems);
            foreach (Entity entity in validItems)
            {
                entity.LastLogin = DateTime.Now;
            }
        }
    }
}
