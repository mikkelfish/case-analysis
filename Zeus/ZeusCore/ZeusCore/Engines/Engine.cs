using System;
using System.Collections.Generic;
using System.Text;
using ZeusCore.Meta;
using System.IO;
using System.Windows.Markup;
using System.Reflection;
using System.ComponentModel;
using System.Globalization;
using MesosoftCommon.Utilities.Binders;
using MesosoftCommon.Development;
using ZeusCore.Components;
using System.Windows.Forms;
using ZeusCore.Properties;
using ZeusCore.Instance;

namespace ZeusCore.Engines
{
    /// <summary>
    /// A generic abstract base class for creating engines that keep track of atoms.
    /// </summary>
    /// <typeparam name="T">The type of atom that the engine keeps track of.</typeparam>
    [Created("Mikkel", "12/03/06", DocumentedStatus=DocumentedStage.None, Version="0.0")]
    [Todo("Mikkel", "12/03/06", Comments="Add events to fire upon loading the XAML file to communicate the valid and invalid items.",Version="0.0")]
    public class Engine<T> : Component where T:Atom
    {
        private PropertyInfo[] properties;

        private Dictionary<string, List<object[]>> dependencies = new Dictionary<string, List<object[]>>();
        protected void registerMissing(T dependency, object invalidAtom, PropertyInfo property)
        {
            string key = dependency.Name;
            if (!this.dependencies.ContainsKey(key))
            {
                this.dependencies.Add(key, new List<object[]>());
            }
            this.dependencies[key].Add(new object[] { invalidAtom, property });
        }

        /// <summary>
        /// Gets the string that's passed to the engine to ask for its default item.
        /// </summary>
        public const string DefaultString = "&&DEFAULT&&";

        private T defaultItem;
        /// <summary>
        /// Gets the default item specified by the engine.
        /// </summary>
        public T Default 
        {
            get
            {
                if (this.defaultItem != null) return this.defaultItem;
                else if (this.Items.Length > 0) return this.Items[0];
                return null;
            }
            set
            {
                this.defaultItem = value;
            }
        }

        /// <summary>
        /// Gets an array of files that the engine should load upon instantiation.
        /// </summary>
        protected virtual string[] DefaultXAMLFiles 
        {
            get
            {
                return new string[] { Resources.ResourceManager.GetString(typeof(T).Name) };
            }
        }

        private bool defaultsLoaded = false;

        private bool loadDefaults = true;
        /// <summary>
        /// Gets or sets whether to load the default meta files when the engine is linked to a context.
        /// </summary>
        public bool LoadDefaults
        {
            get { return loadDefaults; }
            set { loadDefaults = value; }
        }
	

        [Todo("Mikkel", "12/09/06", Comments="Make sure that only ZeusContexts try to assign the engine a Site")]
        public override ISite Site
        {
            get
            {
                return base.Site;
            }
            set
            {
                base.Site = value;
            }
        }

        protected List<T> items;
        //Gets an array of the valid items the engine is aware of.
        public T[] Items
        {
            get { return items.ToArray(); }
        }

        protected List<T> invalidItems;
        /// <summary>
        /// Gets an array of the invalid items the engine is aware of.
        /// </summary>
        public T[] InvalidItems
        {
            get { return invalidItems.ToArray(); }
        }

        /// <summary>
        /// Gets an array of all items that have the passed name.
        /// </summary>
        /// <param name="name">The name of the item to search for.</param>
        /// <returns>Array of items that represented by the passed string or null if no items match.</returns>
        public T[] FindByName(string name)
        {
            if (name == Engine<T>.DefaultString) return new T[] { this.Default };
            T[] toRet = this.items.FindAll(
                delegate(T item)
                {
                    return item.Name == name;
                }
            ).ToArray();

            if (toRet.Length == 0) return null;
            return toRet;
        }

        /// <summary>
        /// Gets the item that has the passed full name.
        /// </summary>
        /// <param name="name">String representing the full name (or DefaultString) to search for.</param>
        /// <returns>Item that has full name or null if none match.</returns>
        public T FindByFullName(string fullName)
        {
            if (fullName == Engine<T>.DefaultString) return this.Default;
            return this.items.Find(
                delegate(T item)
                {
                    return item.FullName == fullName;
                }
            );
        }

        public AtomInstance<T> CreateInstance(string fullName)
        {
            T atom = this.FindByFullName(fullName);
            //if (atom == null)
            //{
            //    atom = (T)Activator.CreateInstance(typeof(T));
            //    toRet.Name = Atom.MissingTargetString + fullName;
            //    return null;
            //}

            Type instanceType = Type.GetType(typeof(T).Name + "Instance");
            AtomInstance<T> obj = (AtomInstance<T>)Activator.CreateInstance(instanceType);
            PropertyInfo meta = instanceType.GetProperty("MetaBase");

            if (atom.FullName.Contains(Atom.MissingTargetString))
            {

            }
            else
            {
                MesosoftCommon.Utilities.Reflection.Common.SetPropertyOrUnderlying(meta, obj, atom, null, null, CultureInfo.CurrentCulture);
                obj.Category = atom.Category;
            }
            return obj;
        }

        public AtomInstance<T> CreateInstance(string name, string categoryName)
        {
            return CreateInstance(categoryName + "." + name);
        }

        public Engine()
        {
            this.items = new List<T>();
            this.invalidItems = new List<T>();
            this.properties = typeof(T).GetProperties();

            PropertyInfo systemProp = typeof(T).GetProperty("System", BindingFlags.Static | BindingFlags.Public);
            if (systemProp != null)
            {
                object obj = systemProp.GetValue(null, null);
                this.items.Add((T)obj);
            }
        }

        /// <summary>
        /// Load the default XAML stream(s)
        /// </summary>
        private void loadDefaultStreams()
        {
            if (this.defaultsLoaded || !this.loadDefaults) return;
            foreach (string file in this.DefaultXAMLFiles)
            {
                if(file != null) 
                    this.LoadItemsFromStream(file);
            }
            this.defaultsLoaded = true;
        }


        /// <summary>
        /// Loads the items from an XAML stream into the engine and checks whether they are valid or not.
        /// </summary>
        /// <param name="xamlFile">Filename of XAML markup for List{T}.</param>
        public void LoadItemsFromStream(string xamlFile)
        {
            object test = Resources.ResourceManager.GetObject(xamlFile);
            Stream stream = null;
            if (test != null)
            {                
                stream = new MemoryStream(UTF8Encoding.Default.GetBytes((string)test));
            }
            else stream = new FileStream(xamlFile, FileMode.Open, FileAccess.Read);
            this.LoadItemsFromStream(stream);
            stream.Close();
            
        }

        /// <summary>
        /// Loads the items from an XAML stream into the engine and checks whether they are valid or not.
        /// </summary>
        /// <param name="xamlStream">Stream that contains valid XAML markup for List{T}."</param>        
        [Todo("Mikkel", "12/03/06", Comments="Fire events informing of valid and invalid items.", Version="0.0")]
        [Todo("Mikkel", "05/17/07", Comments="Make thread safe", Version="0.0")]
        public void LoadItemsFromStream(Stream xamlStream)
        {
            T[] vItems;
            T[] inItems;
            ZeusContext.CurrentlyLoadingContext = (ZeusContext)this.Site.Container;
            Utilities.XAML.ReadXAML<T>(this.GetDescriptor(), xamlStream, out vItems, out inItems);
            ZeusContext.CurrentlyLoadingContext = null;

            this.catchItemsBeforeInsertion(ref vItems, ref inItems);

            this.invalidItems.AddRange(inItems);
            this.addValidItems(vItems);

            //TODO Add event to shout to the heavens about the invalid and valid items that were read.

        }

        protected void dependenciesCompleted(T atom)
        {
            this.invalidItems.Remove(atom);
            this.addValidItems(new T[] { atom });
        }

        private void addValidItem(T item)
        {
            //item.Engine = this as Engine<T>;
            this.items.Add(item);
            if (this.dependencies.ContainsKey(Atom.MissingTargetString + item.FullName))
            {
                List<object[]> deps = this.dependencies[Atom.MissingTargetString + item.FullName];
                foreach (object[] obj in deps)
                {
                    Atom target = obj[0] as Atom;
                    PropertyInfo info = obj[1] as PropertyInfo;
                    MesosoftCommon.Utilities.Reflection.Common.SetPropertyOrUnderlying(info, target, item, null, null, CultureInfo.CurrentCulture);
                    if (Atom.VerifyAtom(target))
                    {
                        MethodInfo getEngine = typeof(ZeusContext).GetMethod("GetEngine");
                        object engine = MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod(getEngine,
                            new Type[] { target.GetType() }, this.Site.Container, null);
                        MethodInfo dep = engine.GetType().GetMethod("dependenciesCompleted", BindingFlags.NonPublic | BindingFlags.Instance);
                        dep.Invoke(engine, new object[] { target });
                    }
                }
                this.dependencies.Remove(Atom.MissingTargetString + item.FullName);
            }
        }

        private void addValidItems(T[] validItems)
        {
            foreach (T item in validItems)
            {
                this.addValidItem(item);
            }
        }

        [Todo("Mikkel", "5/17/07", Comments="All sorts of exception handling.")]
        public void AddItem(T item)
        {
            bool valid = true;
            if (this.FindByFullName(item.FullName) != null) return;
            foreach (PropertyInfo prop in this.properties)
            {
                object[] attrs = prop.GetCustomAttributes(typeof(RequiredAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    object val = prop.GetValue(item, null);
                    if (val == null) valid = false;
                }
            }

            T[] validItems = new T[0];
            T[] invalidItems = new T[0];
            if (valid)
            {
                validItems = new T[] { item };
            }
            else invalidItems = new T[] { item };
            this.catchItemsBeforeInsertion(ref validItems, ref invalidItems);
        }

        protected virtual void catchItemsBeforeInsertion(ref T[] validItems, ref T[] invalidItems)
        {
            List<T> validRet = new List<T>();
            List<T> invalidRet = new List<T>();
            foreach (T item in validItems)
            {
                bool valid = true;
                foreach (PropertyInfo prop in this.properties)
                {
                    if (prop.PropertyType.IsSubclassOf(typeof(Atom)))
                    {
                        Atom val = (Atom) prop.GetValue(item, null);
                        if (val != null && val.Name.Contains(Atom.MissingTargetString))
                        {
                            if (val.FullName.Substring(Atom.MissingTargetString.Length) == item.FullName)
                            {
                                prop.SetValue(item, item, null);
                            }
                            else
                            {

                               MethodInfo getEngine = typeof(ZeusContext).GetMethod("GetEngine");
                                object engine = MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod(getEngine, new Type[] { prop.PropertyType },
                                    (this.Site.Container as ZeusContext), null);
                                MethodInfo method = engine.GetType().GetMethod("registerMissing", BindingFlags.Instance | BindingFlags.NonPublic);
                                method.Invoke(engine, new object[] { val, item, prop });
                                valid = false;
                            }
                        }
                    }
                }
                if (valid) validRet.Add(item);
                else invalidRet.Add(item);
            }

            foreach (T item in invalidItems)
            {
                invalidRet.Add(item);
            }

            validItems = validRet.ToArray();
            invalidItems = invalidRet.ToArray();
        }

        public ITypeDescriptorContext GetDescriptor()
        {
            return new EngineTypeDescriptor(this);
        }

        protected class EngineTypeDescriptor : ITypeDescriptorContext
        {
            private Engine<T> engine;
            private PropertyInfo destType;

            public PropertyInfo Destination
            {
                get { return this.destType; }
                set { this.destType = value; }
            }

            private object destObject;

            public object DestinationObject
            {
                get { return destObject; }
                set { destObject = value; }
            }
	

            public Type EngineType
            {

                get
                {
                    return typeof(T);
                }
            }
            
            public EngineTypeDescriptor(Engine<T> engine)
            {
                this.engine = engine;
            }

            #region ITypeDescriptorContext Members

            public IContainer Container
            {
                get { return this.engine.Site.Container; }
            }

            public object Instance
            {
                get { return this.engine; }
            }

            public void OnComponentChanged()
            {

            }

            public bool OnComponentChanging()
            {
                return false;
            }

            public PropertyDescriptor PropertyDescriptor
            {
                get 
                {
                    return null;
                }
            }

            #endregion

            #region IServiceProvider Members

            public object GetService(Type serviceType)
            {
                return this.engine.Site.GetService(serviceType);
            }

            #endregion

        }
        
    }
}
