﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZeusCore.Engines
{
    [global::System.AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    sealed class CustomZeusEngineAttribute : Attribute
    {
        private Type engineType;

        public Type EngineType
        {
            get { return engineType; }
        }
	      
        public CustomZeusEngineAttribute(Type engineType)
        {
            this.engineType = engineType;
        }
     
    }

}
