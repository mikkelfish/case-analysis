﻿using System;
using System.Collections.Generic;
using System.Text;
using MesosoftCommon.CommonAddIns.ContextAddIns;
using ZeusCore.Components;

namespace ZeusCore.AddIns
{
    [MesosoftCommon.AddIns.AddIn("ZeusContextAddIn", 1.0, RequiresInstanceInformation=false)]
    public class ZeusContextAddIn : ContextAddInView
    {
        private static Dictionary<object, ZeusContext> contexts = new Dictionary<object, ZeusContext>();

        public override Type[] Types
        {
            get { return new Type[]{typeof(ZeusContext)}; }
        }

        public override bool SupportsObject(object key, object[] args)
        {
            return true;
        }

        public override object GetContext(object key, object[] args)
        {
            if (!contexts.ContainsKey(key))
            {
                contexts.Add(key, new ZeusContext());
            }

            return contexts[key];
        }
    }
}
