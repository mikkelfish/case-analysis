﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZeusCore.Interfaces;
using MesosoftCommon.AddIns;
using MesosoftCommon.CommonAddIns.ContextAddIns;
using ZeusCore.Components;
using MesosoftCommon.Utilities.Inspection;

namespace ZeusCore.AddIns
{
    [MesosoftCommon.AddIns.AddIn("ZeusBlindAddin", 1.0)]
    public class ZeusBlindAddIn : MesosoftCommon.CommonAddIns.BlindAssignmentAddIns.BlindAssignmentAddInView
    {
        public const string ZeusInspectionName = "ZeusContextArgument";

        private ContextProvider contextAddin;

        public ZeusBlindAddIn()
        {
            AddInToken<ContextProvider>[] providers = AddInManager.GetAddIns<ContextProvider>();
            foreach (AddInToken<ContextProvider> provider in providers)
            {
                if (provider.Name != "ZeusContextAddIn") continue;
                contextAddin = provider.CreateAddIn(AddInLoadingEnvironment.CurrentDomain);
                if (!contextAddin.Types.Contains<Type>(typeof(ZeusContext))) continue;
                break;
            }
        }

        public override Type[] GetSupportedInterfaces
        {
            get { return new Type[]{typeof(IRequireZeusContext)}; }
        }

        public override bool CanAssign(object obj, object[] pargs, Type[] interfacesSupported, string[] excludedProperties, out bool shouldCopy)
        {
            shouldCopy = false;


            if (!(obj is IRequireZeusContext) || excludedProperties.Contains<string>("Context") || pargs == null) return false;

            InspectionArgument arg = pargs.SingleOrDefault<object>(o => (o is InspectionArgument) && (o as InspectionArgument).Name == ZeusBlindAddIn.ZeusInspectionName) as InspectionArgument;
            if (arg == null) return false;

            object key = arg.Value;
            object[] args = null;
            if (arg.Value is object[] && (arg.Value as object[])[1] is object[])
            {
                key = (arg.Value as object[])[0];
                args = (arg.Value as object[])[1] as object[];
            }

            return contextAddin.ContextSupportsKey(key, args);
        }

        public override void Assign(object obj, object[] pargs, Type[] interfacesSupported, string[] excludedProperties)
        {
            InspectionArgument arg = pargs.SingleOrDefault<object>(o => (o is InspectionArgument) && (o as InspectionArgument).Name == ZeusBlindAddIn.ZeusInspectionName) as InspectionArgument;
            if (arg == null) return;

            object key = arg.Value;
            object[] args = null;
            if (arg.Value is object[] && (arg.Value as object[])[1] is object[])
            {
                key = (arg.Value as object[])[0];
                args = (arg.Value as object[])[1] as object[];
            }

            (obj as IRequireZeusContext).Context = contextAddin.GetContext<ZeusContext>(key, args);

        }

        public override bool CanCopy(object obj)
        {
            return false;
        }

        public override object Copy(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
