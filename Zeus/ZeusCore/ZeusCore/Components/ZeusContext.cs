using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using ZeusCore.Engines;
using System.Windows.Forms;
using System.ComponentModel.Design;
using ZeusCore.Meta;
using MesosoftCommon.Development;
using System.Reflection;

namespace ZeusCore.Components
{
    public class ZeusContext : Container
    {
        private static ZeusContext currentlyLoadingContext;
        public static ZeusContext CurrentlyLoadingContext
        {
            get
            {
                return ZeusContext.currentlyLoadingContext;
            }
            set
            {
                ZeusContext.currentlyLoadingContext = value;
            }
        }

        protected class ZeusContextSite : ISite
        {
            private ZeusContext context;

            public ZeusContextSite(ZeusContext context, IComponent component, bool designMode)
            {
                this.context = context;
                this.component = component;
                this.designMode = designMode;
            }

            #region ISite Members
            private IComponent component;
            public IComponent Component
            {
                get { return this.component; }
            }

            public IContainer Container
            {
                get { return this.context; }
            }

            private bool designMode;
            public bool DesignMode
            {
                get { return this.designMode; }
            }

            string name;
            public string Name
            {
                get
                {
                    return this.name;
                }
                set
                {
                    this.name = value;
                }
            }

            #endregion

            #region IServiceProvider Members

            public object GetService(Type serviceType)
            {
               return this.context.GetService(serviceType);
            }

            #endregion
        }

       // private ComponentCollection collection;

        private ServiceContainer services = new ServiceContainer();
        public IServiceContainer Services
        {
            get
            {
                return this.services;
            }
        }

        protected override ISite CreateSite(IComponent component, string name)
        {
            ZeusContextSite site = new ZeusContextSite(this, component, false);
            site.Name = name;
            return site;
        }

        protected override object GetService(Type service)
        {
            object toRet = this.services.GetService(service);
            if (toRet != null) return toRet;
            return base.GetService(service);
        }

        [Todo("Mikkel", "12/09/06", Comments="Check to make sure there is only one type of each engine")]
        public override void Add(IComponent component)
        {
            //component.GetType().
            Type compareType = component.GetType();
            bool isEngine = false;

            while (compareType != compareType.BaseType)
            {
                if (compareType.IsGenericType)
                {
                    if (compareType.GetGenericTypeDefinition() == typeof(Engine<>))
                    {
                        isEngine = true;
                        break;
                    }
                }
                else compareType = compareType.BaseType;
            }

         

            if (isEngine)
            {
                base.Add(component, compareType.GetGenericArguments()[0].Name);
                
            }
            else base.Add(component);
        }

        private void loadDefault(IComponent component)
        {
            Type compareType = component.GetType();
            bool isEngine = false;

            while (compareType != compareType.BaseType)
            {
                if (compareType.IsGenericType)
                {
                    if (compareType.GetGenericTypeDefinition() == typeof(Engine<>))
                    {
                        isEngine = true;
                        break;
                    }
                }
                else compareType = compareType.BaseType;
            }

            if (isEngine)
            {
                MethodInfo method = compareType.GetMethod("loadDefaultStreams", BindingFlags.Instance | BindingFlags.NonPublic);
                if (method != null) method.Invoke(component, null);
            }
        }	

        public Engine<T> GetEngine<T>() where T:Atom, new()
        {
            string lookFor = typeof(T).Name;
            return this.Components[lookFor] as Engine<T>;
        }

        public object[] GetEngineItems<T>() where T : Atom, new()
        {
            string lookFor = typeof(T).Name;
            Engine<T> engine = this.Components[lookFor] as Engine<T>;
            return engine.Items;
        }

        public object[] GetEngineItemsByType(Type engineType)
        {
            object ret = MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod("GetEngineItems", new Type[] { engineType }, this, null);
            return (ret as object[]);
        }

        public ZeusContext()
        {
           

            Type[] types = MesosoftCommon.Utilities.Reflection.Common.GetTypesFromAssembly(this.GetType().Assembly, typeof(Atom), false);
            foreach (Type type in types)
            {
                object[] attrs = type.GetCustomAttributes(typeof(CustomZeusEngineAttribute), true);
                IComponent engine = null;
                if (attrs != null && attrs.Length > 0)
                {
                    engine = (IComponent)Activator.CreateInstance((attrs[0] as CustomZeusEngineAttribute).EngineType);
                }
                else
                {
                    Type engineGeneric = typeof(Engine<>).MakeGenericType(type);
                    engine = (IComponent)Activator.CreateInstance(engineGeneric);
                }
                this.Add(engine);
            }

            foreach (IComponent engine in this.Components)
            {
                this.loadDefault(engine);
            }
        }
    }
}
