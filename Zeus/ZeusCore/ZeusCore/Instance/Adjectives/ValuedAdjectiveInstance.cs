using System;
using System.Collections.Generic;
using System.Text;
using ZeusCore.Meta;
using System.ComponentModel;
using MesosoftCommon.Development;

namespace ZeusCore.Instance
{
    public class AdjectiveInstance : AtomInstance<ValuedAdjective>
    {
        private bool valueRequired;
        /// <summary>
        /// Specifies whether a value is required for the parent object to be considered valid.
        /// </summary>
        [DefaultValue(false)]
        [Required]
        [Browsable(true)]
        public bool ValueRequired
        {
            get { return valueRequired; }
            set { valueRequired = value; }
        }

        private bool searchable;
        /// <summary>
        /// Specifies whether this adjective should be added to the runtime searching engine.
        /// </summary>
        [DefaultValue(false)]
        [Required]
        [Browsable(true)]        
        public bool Searchable
        {
            get { return searchable; }
            set { searchable = value; }
        }



        private object val;
        /// <summary>
        /// The value of the adjective
        /// </summary>
        [Todo("Mikkel", "12/05/06", Comments="Add logic to check the domain to validate object.", Version="0.0")]
        [Browsable(true)]        
        public object Value
        {
            get { return val; }
            set { val = value; }
        }
      	
    }
}
