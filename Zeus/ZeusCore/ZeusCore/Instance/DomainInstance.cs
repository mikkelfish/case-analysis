using System;
using System.Collections.Generic;
using System.Text;
using ZeusCore.Meta;
using MesosoftCommon.Development;
using System.Windows.Controls;
using System.ComponentModel;

namespace ZeusCore.Instance
{
    /// <summary>
    /// Domain instance is used to add some extra information to the meta domain so it can be used in an actual application.
    /// </summary>
    [Created("Mikkel", "12/06/06", Version = "0.0", DocumentedStatus = DocumentedStage.None)]
    public class DomainInstance : AtomInstance<Domain>
    {
        private List<ValidationRule> extraValidations = new List<ValidationRule>();
        /// <summary>
        /// Instance specific validations to be applied on top of the meta validations
        /// </summary>
        [Created("Mikkel", "12/06/06", Version = "0.0", DocumentedStatus = DocumentedStage.None)]        
        public List<ValidationRule> ExtraValidations
        {
            get { return extraValidations; }
            set { this.extraValidations = value; }
        }

        private object dataType;
        /// <summary>
        /// Instance specific data type. (Note: only valid if it constricts the datatype from the meta definition.)
        /// </summary>
        [Created("Mikkel", "12/06/06", Version="0.0", DocumentedStatus=DocumentedStage.None)]
        [Todo("Mikkel", "12/05/06", Comments="Come back and implement some logic to make sure that we are constraining types so the instance type can be converted to meta type but not visa versa",
            Version="0.0")]
        public object DataType
        {
            get 
            {
                if (dataType == null && this.MetaBase != null)
                    return this.MetaBase.DataType;
                return dataType; 
            }
            set { dataType = value; }
        }	
    }
}
