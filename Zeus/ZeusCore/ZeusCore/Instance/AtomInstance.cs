using System;
using System.Collections.Generic;
using System.Text;
using ZeusCore.Meta;
using MesosoftCommon.Attributes;
using System.ComponentModel;
using ZeusCore.Utilities;

namespace ZeusCore.Instance
{
    [ZeusVocabulary]
    public abstract class AtomInstance<T> where T: Atom
    {
        private T metaBase=null;
        /// <summary>
        /// The meta element the instance is based on.
        /// </summary>
        [TypeConverter(typeof(Converters.EngineConverter))]
        [DefaultValue(Constants.DefaultValuePropertyPrefix + "FullName")]
        [FieldLinked("metaBase")]
        [Required]
        public T MetaBase
        {
            get { return metaBase; }
        }

        private string name;
        /// <summary>
        /// The name of the instance (and thus the name of the meta template).
        /// </summary>
        [Required]
        public string Name
        {
            get { return name; }
            set 
            { 
                name = value;
            }
        }

        private string notes;
        /// <summary>
        /// Any instance specific notes that might be useful to know.
        /// </summary>
        [DefaultValue("")]
        [Required]
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }

        private object tag;
        /// <summary>
        /// An optional argument used to store instance data.
        /// </summary>
        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        /// <summary>
        /// Gets a string that represents the full unique name of the atom.
        /// </summary>
        public virtual string FullName 
        {
            get 
            {
                if (this.category == null) return null;
                return this.Category.FullName + "." + this.Name; 
            }
        }

        private Category category;
        /// <summary>
        /// The category that the domain belongs to.
        /// </summary>
        [DefaultValue(Engines.Engine<Category>.DefaultString)]
        [TypeConverter(typeof(Converters.MetaEngineConverter<Category>))]
        [Required]
        public Category Category
        {
            get 
            {
                if (typeof(T) != typeof(Category)) return category;
                return this.MetaBase as Category;  
            }
            set 
            {
                if (typeof(T) != typeof(Category)) category = value;
            }
        }

    }
}
