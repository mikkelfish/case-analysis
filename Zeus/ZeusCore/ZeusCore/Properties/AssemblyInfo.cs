﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Markup;
//using Razor.Attributes;

[assembly: XmlnsDefinition("ZeusXAML", "ZeusCore.Meta", AssemblyName = "ZeusCore")]
[assembly: XmlnsDefinition("ZeusXAML", "ZeusCore.Instance", AssemblyName = "ZeusCore")]
[assembly: XmlnsDefinition("ZeusXAML-Converters", "ZeusCore.Converters", AssemblyName = "ZeusCore")]
[assembly: XmlnsDefinition("ZeusXAML-Converters", "ZeusCore.Converters.ValueConverters", AssemblyName = "ZeusCore")]
[assembly: XmlnsDefinition("ZeusXAML-Utilities", "ZeusCore.Utilities", AssemblyName = "ZeusCore")]
[assembly: XmlnsDefinition("Mesosoft-Utilities", "MesosoftCommon.Utilities.XAML", AssemblyName = "MesosoftCommon")]
[assembly: XmlnsDefinition("Mesosoft-Converters", "MesosoftCommon.Utilities.Converters", AssemblyName = "MesosoftCommon")]
[assembly: XmlnsDefinition("Mesosoft-ValueConverters", "MesosoftCommon.Utilities.Converters.ValueConverters", AssemblyName = "MesosoftCommon")]
[assembly: XmlnsDefinition("Mesosoft-Validators", "MesosoftCommon.Utilities.Validations", AssemblyName = "MesosoftCommon")]
[assembly: XmlnsDefinition("System", "System", AssemblyName = "mscorlib")]
[assembly: XmlnsDefinition("System-Collections", "System.Collections", AssemblyName = "mscorlib")]
[assembly: XmlnsPrefix("ZeusXAML", "uzeus")]

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ZeusCore")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("ZeusCore")]
[assembly: AssemblyCopyright("Copyright ©  2006")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e1a9b3dc-8422-4080-b171-594c9659c362")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.2")]
[assembly: AssemblyFileVersion("1.0.0.2")]

//[assembly: SnapInExportedFromAssembly(typeof(ZeusCore.ZeusCorePlugin))]

