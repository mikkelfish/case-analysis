﻿using System;
using System.Collections.Generic;
using System.Text;
using ZeusCore.Components;
using ZeusCore.Meta;
using MesosoftCommon.AutoComplete;

namespace ZeusCore.Interfaces
{
    public class ZeusAutoCompleteAtomProvider : IAutoCompleteSourceProvider, IRequireZeusContext
    {
        #region IAutoCompleteSourceProvider Members

        public bool CanGetAutoCompleteItems(object source, object parameter)
        {
            Type type = parameter as Type;
            if(type == null || !(source is ZeusContext) || !(type.IsSubclassOf(typeof(Atom))))
                return false;
            return true;
        }

        public AutoCompleteItemPair[] GetAutoCompleteItems(object source, object parameter,
            System.Globalization.CultureInfo culture, int maxItems)
        {
            ZeusContext context = source as ZeusContext;
            Type type = parameter as Type;

            return (AutoCompleteItemPair[])MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod("genericGetAutoCompleteItems", new Type[] { type }, this,
                new object[] { source, parameter, culture, maxItems });
            
            //throw new Exception("The method or operation is not implemented.");
        }

        private AutoCompleteItemPair[] genericGetAutoCompleteItems<T>(ZeusContext context, Type type,
            System.Globalization.CultureInfo culture, int maxItems) where T : Atom, new()
        {
            ZeusCore.Engines.Engine<T> engine = context.GetEngine<T>();
            AutoCompleteItemPair[] toRet = null;

            if (engine.Items.Length > maxItems)
                toRet = new AutoCompleteItemPair[maxItems];
            else
                toRet = new AutoCompleteItemPair[engine.Items.Length];

            for (int i = 0; i < toRet.Length; i++)
            {
                toRet[i] = new AutoCompleteItemPair(engine.Items[i].FullName,engine.Items[i]);
            }
            return toRet;
        }
        #endregion

        #region IRequireZeusContext Members

        public ZeusContext Context
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
