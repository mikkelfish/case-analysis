﻿using System;
using System.Collections.Generic;
using System.Text;
using MesosoftCommon.Interfaces;

namespace ZeusCore.Interfaces
{
    public interface IRequireZeusContext
    {
        ZeusCore.Components.ZeusContext Context {get; set;}
    }
}
