﻿using System;
using System.Collections.Generic;
using System.Text;
using ZeusCore.Meta;
using MesosoftCommon.Interfaces;

namespace ZeusCore.Interfaces
{
    public interface IRequireAtom : IRequireObjects
    {
        Atom Atom { get; set; }
    }
}
