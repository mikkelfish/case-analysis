﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using MesosoftCommon.Layout;

namespace OntologyCreation
{
    /// <summary>
    /// Interaction logic for BarbiesDragNDockPlayhouse.xaml
    /// </summary>

    public partial class BarbiesDragNDockPlayhouse : Window, INotifyPropertyChanged
    {
        private bool dragIsActive;
        public bool DragIsActive
        {
            get { return dragIsActive; }
            set
            {
                dragIsActive = value;
                this.notifyPropertyChanged("DragIsActive");
            }
        }

        private bool enforceBoundaries = true;
        public bool EnforceBoundaries
        {
            get { return enforceBoundaries; }
            set 
            { 
                enforceBoundaries = value;
                this.notifyPropertyChanged("EnforceBoundaries");
            }
        }
	

        public BarbiesDragNDockPlayhouse()
        {
            InitializeComponent();
        }

        public void DragToggleButtonClick(object sender, RoutedEventArgs e)
        {
            this.DragIsActive = !this.DragIsActive;
            DragManager.IsDragModeActive = this.DragIsActive;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void notifyPropertyChanged(string name)
        {
            PropertyChangedEventHandler handle = this.PropertyChanged;
            if (handle == null) return;
            handle(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
