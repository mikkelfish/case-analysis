﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Globalization;
using MesosoftStyleLibrary;

namespace OntologyCreation.Templates
{
    /// <summary>
    /// Interaction logic for AtomEditLine.xaml
    /// </summary>
    public partial class AtomEditLine : System.Windows.Controls.UserControl, IDataErrorInfo, INotifyPropertyChanged
    {
        public AtomEditLine()
        {

            InitializeComponent();

            object st = this.TryFindResource(new ComponentResourceKey(typeof(TestDictionary), "ToolValid"));
            this.textBoxEntry.DataContext = this;
            this.DataContext = this;
            this.textBlockLabel.DataContext = this;
        }

        public string EditName
        {
            get { return (string)this.GetValue(EditNameProperty); }
            set { this.SetValue(EditNameProperty, value); }
        }
        public static readonly DependencyProperty EditNameProperty = DependencyProperty.Register(
          "EditName", typeof(string), typeof(AtomEditLine), new PropertyMetadata("Blank"));



        public ValidationRule DataValidator
        {
            get { return (ValidationRule)GetValue(DataValidatorProperty); }
            set { SetValue(DataValidatorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DataValidator.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataValidatorProperty =
            DependencyProperty.Register("DataValidator", typeof(ValidationRule), typeof(AtomEditLine), new UIPropertyMetadata(new MesosoftCommon.Utilities.Validations.StringLengthValidator(1,20)));

        private string inputString = "";
        public string InputString
        {
            get 
            {
                return this.inputString;
               // return (string)GetValue(InputStringProperty); 
            }
            set 
            { 
               // SetValue(InputStringProperty, value);
                this.inputString = value;
                this.OnPropertyChange("InputString");
            }
        }

        // Using a DependencyProperty as the backing store for InputString.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty InputStringProperty =
        //    DependencyProperty.Register("InputString", typeof(string), typeof(AtomEditLine), new UIPropertyMetadata(""));




        #region IDataErrorInfo Members

        public string Error
        {
            get { return this[null]; }
        }

        public string this[string columnName]
        {
            get 
            {
                if (columnName != "InputString") return "";
                ValidationResult result = this.DataValidator.Validate(this.InputString, CultureInfo.CurrentUICulture);
                if (result.IsValid) return "";
                return result.ErrorContent.ToString();
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChange(string propertyName)
        {
            if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
