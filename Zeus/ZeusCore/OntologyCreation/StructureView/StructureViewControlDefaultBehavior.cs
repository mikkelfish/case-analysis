﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using ZeusCore.Components;
using System.Windows.Controls;
using MesosoftCommon.Structures;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows.Data;
using MesosoftCommon.Utilities.Inspection;

namespace OntologyCreation.StructureView
{
    public class StructureViewControlDefaultBehavior : FrameworkElement
    {
        public const string MouseDoubleClickHandler = "MouseDoubleClickOnTree";

        public void MouseDoubleClickOnTree(Object sender, MouseButtonEventArgs e)
        {
            BindingExpression be = GetBindingExpression(SourceProperty);
            if (!(sender is TreeView)) return;
            if ((this.Source == null) || (this.Context == null)) return;

            TreeView treeViewSender = sender as TreeView;
            if (!(treeViewSender.SelectedItem is SimpleNode)) return;

            InspectionDataDockPopup newView = new InspectionDataDockPopup();
            newView.Source = (this.Source as SimpleNode).Source;
           // newView.Context = this.Context;
            newView.Title = (this.Source as SimpleNode).Text;
            newView.Show();
        }


        public object Source
        {
            get { return (object)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register(
              "Source",
              typeof(object),
              typeof(StructureViewControlDefaultBehavior)
            );

        public ZeusContext Context
        {
            get { return (ZeusContext)GetValue(ContextProperty); }
            set { SetValue(ContextProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Context.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContextProperty =
            DependencyProperty.Register(
              "Context",
              typeof(ZeusContext),
              typeof(StructureViewControlDefaultBehavior)
            );
    }
}
