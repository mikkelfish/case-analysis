﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using MesosoftCommon.Structures;
using MesosoftCommon.Utilities.Reflection;
using System.ComponentModel;
using ZeusCore.Components;
using ZeusCore.Meta;
using MesosoftCommon.Utilities.Events;

namespace OntologyCreation.StructureView
{
    /// <summary>
    /// Interaction logic for StructureViewControl.xaml
    /// </summary>
    public partial class StructureViewControl : System.Windows.Controls.UserControl, INotifyPropertyChanged
    {
        private SimpleNode rootNode;
        private SimpleNode InternalRootNode
        {
            get { return rootNode; }
            set 
            { 
                rootNode = value;
                this.notifyPropertyChanged("RootNode");
            }
        }
        public SimpleNode RootNode
        {
            get { return this.InternalRootNode; }
        }

        public ZeusContext Context
        {
            get { return (ZeusContext)GetValue(ContextProperty); }
            set { SetValue(ContextProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Context.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContextProperty =
            DependencyProperty.Register(
              "Context",
              typeof(ZeusContext),
              typeof(StructureViewControl)
            );

        public ObservableCollection<Type> Source
        {
            get { return (ObservableCollection<Type>)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register(
              "Source",
              typeof(ObservableCollection<Type>),
              typeof(StructureViewControl),
              new PropertyMetadata(sourcePropertyChanged)
            );
        protected static void sourcePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {

            StructureViewControl self = obj as StructureViewControl;
            Type[] newSource = (e.NewValue as ObservableCollection<Type>).ToArray<Type>();

            string identifier;

            if ((e.NewValue == null)||(newSource.Length <= 0)||(self.Context == null)) return;

            if (e.NewValue.GetType().IsGenericType) identifier = e.NewValue.GetType().GetGenericArguments()[0].Name + " Collection";
            else identifier = e.NewValue.GetType().Name;

            SimpleNode topNode = new SimpleNode("TreeView: Meta-Types");

            foreach (Type type in newSource)
            {
                SimpleNode subNode = new SimpleNode(type.Name);

                object[] items = self.Context.GetEngineItemsByType(type);
                foreach (object item in items)
                {
                    subNode.BuildNodeTreeFromString(item.ToString(), new char[1] { '.' }, item);
                }
                subNode.Parent = topNode;
                //topNode.ChildNodes.Add(subNode);
            }

            self.InternalRootNode = new SimpleNode();
            //If there's only one child node, go ahead and collapse upward so the tree doesn't have two single-member layers
            if (topNode.ChildNodes.Length == 1)
            {
                //self.InternalRootNode.ChildNodes.Add(topNode.ChildNodes[0]);
                topNode.ChildNodes[0].Parent = self.InternalRootNode;
            }
            else
            {
                //self.InternalRootNode.ChildNodes.Add(topNode);
                topNode.Parent = self.InternalRootNode;
            }
        }

        private RetrieveLoadedTypes retrieveTypes = new RetrieveLoadedTypes();


        public StructureViewControl()
        {
            InitializeComponent();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void notifyPropertyChanged(string name)
        {
            PropertyChangedEventHandler handle = this.PropertyChanged;
            if (handle == null) return;
            handle(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
