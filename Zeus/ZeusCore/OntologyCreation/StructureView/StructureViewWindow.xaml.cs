﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ZeusCore.Components;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace OntologyCreation.StructureView
{
    /// <summary>
    /// Interaction logic for StructureView.xaml
    /// </summary>

    public partial class StructureViewWindow : Window, INotifyPropertyChanged
    {
        private ZeusContext context;
        public ZeusContext Context
        {
            get { return context; }
            set
            {
                context = value;
                this.notifyPropertyChanged("Context");
            }
        }

        private ObservableCollection<Type> source = new ObservableCollection<Type>();
        public ObservableCollection<Type> Source
        {
            get { return source; }
            set 
            { 
                source = value;
                this.notifyPropertyChanged("Source");
            }
        }
	
        public StructureViewWindow()
        {
            InitializeComponent();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void notifyPropertyChanged(string name)
        {
            PropertyChangedEventHandler handle = this.PropertyChanged;
            if (handle == null) return;
            handle(this, new PropertyChangedEventArgs(name));
        }


        #endregion
    }
}
