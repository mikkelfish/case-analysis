﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;

namespace OntologyCreation.MainWindow
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {

        public ZeusCore.Components.ZeusContext Context
        {
            get { return (ZeusCore.Components.ZeusContext)GetValue(ContextProperty); }
            set { SetValue(ContextProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Context.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContextProperty =
            DependencyProperty.Register(
              "Context",
              typeof(ZeusCore.Components.ZeusContext),
              typeof(OntologyCreation.MainWindow.MainWindow)
            ); 

        public MainWindow()
        {
            InitializeComponent();
            
        }

        

        protected override IEnumerator LogicalChildren
        {
            get
            {
                return base.LogicalChildren;
            }
        }

        protected override void OnVisualChildrenChanged(DependencyObject visualAdded, DependencyObject visualRemoved)
        {
            base.OnVisualChildrenChanged(visualAdded, visualRemoved);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
        }

        protected override void OnStateChanged(EventArgs e)
        {
            base.OnStateChanged(e);
        }
    }
}
