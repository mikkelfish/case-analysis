﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace OntologyCreation.MainWindow
{
    public class MainWindowDefaultBehavior : FrameworkElement
    {
        public Window ParentWindow
        {
            get { return (Window)GetValue(ParentWindowProperty); }
            set { SetValue(ParentWindowProperty, value); }
        }
        // Using a DependencyProperty as the backing store for ParentWindow.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParentWindowProperty =
            DependencyProperty.Register(
              "ParentWindow",
              typeof(Window),
              typeof(MainWindowDefaultBehavior)
            );


        public const string WindowLoadedHandler = "WindowLoaded";

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
