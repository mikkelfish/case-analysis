﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using MesosoftCommon.Utilities.Reflection;
using MesosoftCommon.Development;
using System.Collections.ObjectModel;
using ZeusCore.Components;
using ZeusCore.Meta;
using System.ComponentModel;
using ZeusCore.Interfaces;
using MesosoftCommon.Utilities.Inspection;

namespace OntologyCreation
{
    /// <summary>
    /// Interaction logic for AtomEditor.xaml
    /// </summary>
    public partial class AtomEditor : UserControl, IRequireZeusContext
    {
        static AtomEditor()
        {
            //OntologyControl.ContextProperty.OverrideMetadata(typeof(AtomEditor), new PropertyMetadata(new PropertyChangedCallback(AtomEditor.contextPropertyChanged)));
        }

        private ObservableCollection<Type> metaObjects = new ObservableCollection<Type>();
        public ObservableCollection<Type> MetaObjects
        {
            get
            {
                return this.metaObjects;
            }
        }

       
        private ObservableCollection<Atom> metaItems = new ObservableCollection<Atom>();
        public ObservableCollection<Atom> MetaItems
        {
            get
            {
                return this.metaItems;
            }
        }

        public static readonly DependencyProperty SelectedTypeProperty =
          DependencyProperty.Register(
          "SelectedType",
          typeof(Type),
          typeof(AtomEditor), new PropertyMetadata(new PropertyChangedCallback(AtomEditor.selectedTypePropertyChanged)));
        public Type SelectedType
        {
            get
            {
                return (Type)GetValue(SelectedTypeProperty);
            }
            set
            {
                SetValue(SelectedTypeProperty, value);
                Common.RunGenericMethod("setMetaItems", new Type[] { value }, this, null);
            }
        }

        public static readonly DependencyProperty ItemToEditProperty =
           DependencyProperty.Register(
           "ItemToEdit",
           typeof(Atom),
           typeof(AtomEditor));
        public ZeusCore.Meta.Atom ItemToEdit
        {
            get
            {
                return (Atom)GetValue(ItemToEditProperty);
            }
            set
            {
                SetValue(ItemToEditProperty, value);
            }
        }

        public AtomEditor()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(AtomEditor_Loaded);
        }

        void AtomEditor_Loaded(object sender, RoutedEventArgs e)
        {
            MesosoftCommon.Utilities.Inspection.FillInManager.GetObject(this, null, FillInManager.FillInCopyMode.Leave);
        }        

        [Todo("Ian", "06/18/2007",Comments="Implement exception handling",Priority=DevelopmentPriority.Exception)]
        private void setMetaObjects()
        {
            Type[] types = Common.GetTypesFromAssembly(Context.GetType().Assembly, typeof(ZeusCore.Meta.Atom), false);
            foreach (Type type in types)
            {
                this.metaObjects.Add(type);
            }
        }

        [Todo("Ian", "06/19/2007", Comments = "Implement exception handling", Priority = DevelopmentPriority.Exception)]
        private void setMetaItems<T>() where T:Atom, new()
        {
            if (Context == null)
                return;
            Atom[] atoms = this.Context.GetEngine<T>().Items;
            if (this.metaItems.Count != 0) this.metaItems.Clear();
            foreach (Atom atom in atoms)
            {
                this.metaItems.Add(atom);
            }
        }
 
        public void modifyClicked(object sender, EventArgs e)
        {
            MessageBox.Show("Button Clicked", "Clickies");
        }

        protected static void selectedTypePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MesosoftCommon.Utilities.Reflection.Common.RunGenericMethod("setMetaItems", new Type[] { (Type)e.NewValue }, obj, null);
        }

        //protected static void contextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    (obj as AtomEditor).setMetaObjects();
        //}

        #region IRequireZeusContext Members
        private ZeusContext context;
        public ZeusContext Context
        {
            get
            {
                return this.context;
            }
            set
            {
                this.context = value;
                this.setMetaObjects();
            }
        }

        #endregion
    }
}
