﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Reflection;
using ZeusCore.Meta;

namespace OntologyCreation
{
    /// <summary>
    /// Interaction logic for OntologyFieldEditor.xaml
    /// </summary>
    public partial class OntologyFieldEditor : System.Windows.Controls.UserControl
    {
        public static readonly DependencyProperty ZeusFieldProperty =
            DependencyProperty.Register(
                "ZeusField",
                typeof(ZeusCore.Meta.Atom),
                typeof(OntologyFieldEditor)
            );
        public ZeusCore.Meta.Atom ZeusField
        {
            get
            {
                return (Atom)GetValue(ZeusFieldProperty);
            }
            set
            {
                SetValue(ZeusFieldProperty, value);
            }
        }

        public OntologyFieldEditor()
        {
            InitializeComponent();
        }
    }
}
