﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using MesosoftCommon.Utilities.Events;
using System.Reflection;
using MesosoftCommon.Resources;
using MesosoftCommon.Utilities.Reflection;
using ZeusCore.Meta;
using MesosoftCommon.ExtensionMethods;
using System.Collections.ObjectModel;
using OntologyCreation.StructureView;
using MesosoftCommon.IO;
using System.Windows.Media.Effects;
using MesosoftCommon.Utilities.XAML;
using System.Windows.Markup;
using System.IO;
using System.Resources;
using System.Globalization;
using Reflector.BamlViewer;


namespace OntologyCreation
{
    /// <summary>
    /// Interaction logic for OntologyEditor.xaml
    /// </summary>



    public partial class OntologyEditor : Window
    {
        public static readonly DependencyProperty ContextProperty =
            DependencyProperty.Register(
                "Context",
                typeof(ZeusCore.Components.ZeusContext),
                typeof(OntologyCreation.OntologyEditor)
            );
        public ZeusCore.Components.ZeusContext Context
        {
            get 
            { 
                return (ZeusCore.Components.ZeusContext)GetValue(ContextProperty); 
            }
            set 
            {
                SetValue(ContextProperty, value);
            }
        }

        public void test(Object sender, MouseButtonEventArgs e)
        {
            //int i = 0;
            e.Handled = false;
        }
 
        public OntologyEditor()
        {            
            InitializeComponent();
        }

        private void ChangeBack(object sender, RoutedEventArgs e)
        {
            //if (this.Background == Brushes.Black) this.Background = Brushes.White;
            //else this.Background = Brushes.Black;

         

            MesosoftCommon.Resources.UnifiedResourceManager win = new UnifiedResourceManager();
            win.Show();
        }

        private void InvokeTreeView(object sender, RoutedEventArgs e)
        {
            XamlFrameworkElementManager.RegisterElements(this);
           
            //StructureViewWindow newView = new StructureViewWindow();
            //ObservableCollection<Type> source = new ObservableCollection<Type>();
            //source.Add(atomSelection.SelectedType);
            //newView.Context = this.Context;
            //newView.Source = source;
            //newView.Owner = this;
            //newView.Show();
        }

        private void CallBarbiesHouse(Object sender, RoutedEventArgs e)
        {
            //BarbiesDragNDockPlayhouse yayBarbie = new BarbiesDragNDockPlayhouse();
            //yayBarbie.Owner = this;
            //yayBarbie.Show();
            //SimpleGraph sg = new SimpleGraph();
            //sg.Show();
        }

        private void unifiedResource(object sender, RoutedEventArgs e)
        {
            UnifiedResourceManager manager = new UnifiedResourceManager();
            manager.Show();
        }
    }
}
