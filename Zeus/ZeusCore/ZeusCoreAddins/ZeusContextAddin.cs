﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn;
using CommonAddInViews;
using ZeusCore.Components;
using System.Reflection;

namespace ZeusCoreAddins
{
    [AddIn("ZeusContextProvider")]
    public class ZeusContextAddin : ContextAddInView
    {
        private Dictionary<object, ZeusContext> contexts =
            new Dictionary<object, ZeusContext>();

        public override Type[] ContextTypes
        {
            get { return new Type[] { typeof( ZeusContext) }; }
        }

        public override object CreateContext(object key, object[] args)
        {
            
            if (contexts.ContainsKey(key)) return contexts[key];
            
            contexts.Add(key, new ZeusContext());
            return contexts[key];
        }

        public override bool SupportsObject(object key, object[] args)
        {
            return true;
        }
    }
}
