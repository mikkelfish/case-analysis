﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace MesosoftBits.BasicConverters.ValueConverters
{
    [ValueConversion(typeof(bool[]), typeof(bool))]
    public class BooleanOrGroupConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool ret = false;

            foreach (object value in values)
            {
                if (value.GetType() == typeof(Boolean))
                {
                    if ((bool)(value))
                    {
                        ret = true;
                        break;
                    }
                }
            }
            return ret;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception("Cannot convert back from a boolean to a boolean group.");
        }

        #endregion
    }
}
