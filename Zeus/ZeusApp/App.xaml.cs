﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Xml;
using MesosoftCommon.IO;
using System.Collections.ObjectModel;
using ZeusCore.Components;
using System.Reflection;
using MesosoftCommon.AddIns;
using MesosoftCommon.CommonAddIns.ContextAddIns;
using MesosoftCommon.Resources;
using ZeusCore.Interfaces;
using MesosoftCommon.Utilities.Inspection;
using MesosoftCommon.CommonAddIns.BlindAssignmentAddIns;

namespace ZeusApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ContextProvider zeusContextProvider;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            UniversalListCollection<UniversalItemWrapper<int>> s = new UniversalListCollection<UniversalItemWrapper<int>>();
            s.StartTracking();
            s.Add(4);
            s.Add(5);
            s[1].Value = 2;
            s.Undo();

            s.StopTracking();
            
            Guid g = s.StartSimpleTransaction();
            s.EndSimpleTransaction(g);

            OntologyCreation.OntologyEditor mainWindow = new OntologyCreation.OntologyEditor();
            this.MainWindow = mainWindow;
            this.MainWindow.Closed += new EventHandler(MainWindow_Closed);


            MesosoftCommon.AddIns.AddInManager.BuildAll(Assembly.GetEntryAssembly().Location.Substring(0, Assembly.GetEntryAssembly().Location.LastIndexOf('\\')), true, null);
            AddInToken<ContextProvider>[] addIns = AddInManager.GetAddIns<ContextProvider>();
            foreach (AddInToken<ContextProvider> view in addIns)
            {
                ContextProvider vi = view.CreateAddIn(AddInLoadingEnvironment.CurrentDomain);
                if (vi.Types.Contains<Type>(typeof(ZeusContext)))
                {
                    mainWindow.Context = vi.GetContext<ZeusContext>(AppDomain.CurrentDomain, null);
                    zeusContextProvider = vi;
                }
            }

            FillInManager.AddUniversalHandlers(new EventHandler<PreviewBlindAssignmentEventArgs>(this.giveContextInformation), null);

            mainWindow.Show();
        }

        private void giveContextInformation(object sender, PreviewBlindAssignmentEventArgs e)
        {
            e.Args.Add(new InspectionArgument(ZeusCore.AddIns.ZeusBlindAddIn.ZeusInspectionName) { Value = AppDomain.CurrentDomain });
        }


        void MainWindow_Closed(object sender, EventArgs e)
        {
            this.Shutdown();
        }
    }
}
