﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonHostViews
{
    public abstract class ContextProvider
    {
        public abstract Type[] Types { get; }
        public abstract T GetContext<T>(object key, object[] args) where T : class;
        public abstract object GetContext(object key, object[] args);
        public abstract bool ContextSupportsKey(object key, object[] args);

        public abstract object GetContextByLookup(object key, IRequireContext toAssign, bool setContext);
        public abstract T GetContextByLookup<T>(object key, object[] args) where T : class;
    }
}
