﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonHostViews
{
    public interface IRequireContext
    {
        Type ContextType { get; }
        object[] Args { get; set; }
        object ContextInstance { get; set; }
    }
}
