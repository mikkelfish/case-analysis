﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn.Contract;
using System.AddIn.Pipeline;

namespace CommonContracts
{
    [AddInContract]
    public interface IContextContract : IContract
    {
        Type[] ContextTypes { get; }
        object CreateContext(object key, object[] args);
        bool SupportsObject(object key, object[] args);
    }
}
