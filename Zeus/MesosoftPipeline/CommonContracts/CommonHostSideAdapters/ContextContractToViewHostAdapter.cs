﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn.Pipeline;
using CommonHostViews;
using CommonContracts;
using System.Reflection;
using System.AddIn.Hosting;
using System.Collections.ObjectModel;

namespace CommonHostSideAdapters
{
    [HostAdapter]
    public class ContextContractToViewHostAdapter : ContextProvider
    {
        private static List<ContextProvider> allProviders;

        private static void refreshProviders()
        {
            string loc = Assembly.GetEntryAssembly().Location;
            string addInRoot = loc.Substring(0, loc.LastIndexOf("\\"));
            addInRoot += "\\PipeLine";
            allProviders = new List<ContextProvider>();
            AddInStore.Rebuild(addInRoot);
            Collection<AddInToken> tokens = AddInStore.FindAddIns(typeof(ContextProvider), addInRoot);
            foreach (AddInToken token in tokens)
            {
                //Is this ok?
                allProviders.Add(token.Activate<ContextProvider>(AddInSecurityLevel.FullTrust));
            }

        }

        private IContextContract contract;
        private ContractHandle handle;

        public ContextContractToViewHostAdapter(IContextContract contract)
        {
            this.contract = contract;
            this.handle = new ContractHandle(contract);
        }

        public override Type[] Types
        {
            get { return this.contract.ContextTypes; }
        }

        public override T GetContext<T>(object key, object[] args)
        {
            return this.contract.CreateContext(key, args) as T;
        }

        public override object GetContext(object key, object[] args)
        {
            return this.contract.CreateContext(key, args);
        }

        public override bool ContextSupportsKey(object key, object[] args)
        {
            return this.contract.SupportsObject(key, args);
        }

        public override object GetContextByLookup(object key, IRequireContext toAssign, bool setContext)
        {
            if (allProviders == null)
                refreshProviders();
            foreach (ContextProvider provider in allProviders)
            {
                if(!provider.Types.Contains<Type>(toAssign.ContextType)) continue;
                if (!provider.ContextSupportsKey(key, toAssign.Args)) continue;

                object context = provider.GetContext(key, toAssign.Args);
                if (setContext)
                {
                    toAssign.ContextInstance = context;
                }
                return context;
            }
            return null;
        }

        public override T GetContextByLookup<T>(object key, object[] args)
        {
            if (allProviders == null)
                refreshProviders();
            foreach (ContextProvider provider in allProviders)
            {
                if (!provider.Types.Contains<Type>(typeof(T))) continue;
                if (!provider.ContextSupportsKey(key, args)) continue;

                object context = provider.GetContext(key, args);
                return context as T;
            }
            return null;
        }
    }
}
