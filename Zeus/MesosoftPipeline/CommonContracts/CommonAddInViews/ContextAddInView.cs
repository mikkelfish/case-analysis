﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn.Pipeline;

namespace CommonAddInViews
{
    [AddInBase]
    public abstract class ContextAddInView
    {
        public abstract Type[] ContextTypes { get; }
        public abstract object CreateContext(object key, object[] args);
        public abstract bool SupportsObject(object key, object[] args);
    }
}
