﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn.Pipeline;
using CommonAddInViews;
using CommonContracts;

namespace CommonAddInSideAdapters
{
    [AddInAdapter]
    public class ContextAddInSideAdapter : ContractBase, IContextContract
    {
        private ContextAddInView view;

        public ContextAddInSideAdapter(ContextAddInView view)
        {
            this.view = view;
        }

        #region IContextContract Members

        public Type[] ContextTypes
        {
            get { return this.view.ContextTypes; }
        }

        public object CreateContext(object key, object[] args)
        {
            return this.view.CreateContext(key, args);
        }

        public bool SupportsObject(object key, object[] args)
        {
            return this.view.SupportsObject(key, args);
        }

        #endregion
    }
}
